<?php

namespace App\Services;

use App\Events\Recruitment\RecruitmentCreated;
use App\Events\Recruitment\RecruitmentDeleted;
use App\Events\Recruitment\RecruitmentUpdated;
use App\Models\Recruitment;
use App\Exceptions\GeneralException;
use App\Services\BaseService;
use Exception;
use Illuminate\Support\Facades\DB;

/**
 * Class RecruitmentService.
 */
class RecruitmentService extends BaseService
{
    /**
     * RecruitmentService constructor.
     *
     * @param  Recruitment  $recruitment
     */
    public function __construct(Recruitment $recruitment)
    {
        $this->model = $recruitment;
    }

    /**
     * @param  array  $data
     *
     * @return Recruitment
     * @throws GeneralException
     * @throws \Throwable
     */
    public function store(array $data = []): Recruitment
    {
        DB::beginTransaction();
        try {
            $dataT = [
                'salary_start' => isset($data['salary_start']) ? (int)$data['salary_start'] : 0,
                'salary_end' => isset($data['salary_end']) ? (int)$data['salary_end'] : 0,
                'career_id' => isset($data['career_id']) ? $data['career_id'] : null,
                'address_id' => isset($data['address_id']) ? $data['address_id'] : null,
                'degree_id' => isset($data['degree_id']) ? $data['degree_id'] : null,
                'department_id' => isset($data['department_id']) ? $data['department_id'] : null,
                'expired' => isset($data['expired']) ? date('Y-m-d', strtotime(str_replace('/', '-', $data['expired']))) : null,
                'active' => isset($data['active']) ? $data['active'] : 1,
                'sort' => isset($data['sort']) ? $data['sort'] : 0,
                'vi' => [
                    'title' => isset($data['title']) ? $data['title'] : null,
                    'slug' => isset($data['slug']) ? $data['slug'] : null,
                    'description' => isset($data['description']) ? $data['description'] : null,
                    'content' => isset($data['content']) ? $data['content'] : null,
                    'rank' => isset($data['rank']) ? $data['rank'] : null,
                    'form' => isset($data['form']) ?  $data['form'] : null,
                    'location' => isset($data['location']) ?  $data['location'] : null,
                    'contact' => isset($data['contact']) ?  $data['contact'] : null,
                    'note' => isset($data['note']) ?  $data['note'] : null,
                ],
                'en' => [
                    'title' => isset($data['title_en']) ? $data['title_en'] : null,
                    'slug' => isset($data['slug_en']) ? $data['slug_en'] : null,
                    'description' => isset($data['description_en']) ? $data['description_en'] : null,
                    'content' => isset($data['content_en']) ? $data['content_en'] : null,
                    'rank' => isset($data['rank_en']) ? $data['rank_en'] : null,
                    'form' => isset($data['form']) ? $data['form'] : [],
                    'location' => isset($data['location']) ? $data['location'] : null,
                    'contact' => isset($data['contact']) ? $data['contact'] : null,
                    'note' => isset($data['note']) ? $data['note'] : null,
                ],
            ];
            $recruitment = $this->model::create($dataT);
        } catch (Exception $e) {
            DB::rollBack();

            throw new GeneralException(__('There was a problem creating the recruitment.'));
        }

        event(new RecruitmentCreated($recruitment));

        DB::commit();

        return $recruitment;
    }

    /**
     * @param  Recruitment  $recruitment
     * @param  array  $data
     *
     * @return Recruitment
     * @throws GeneralException
     * @throws \Throwable
     */
    public function update(Recruitment $recruitment, array $data = []): Recruitment
    {
        DB::beginTransaction();
        try {
            $locale = session()->get('locale') ?? env('APP_LANGUAGE');
            $lang = $locale == 'vi' ? 'en' : 'vi';
            $dataT = [
                'salary_start' => isset($data['salary_start']) ? (int)$data['salary_start'] : $recruitment->salary_start,
                'salary_end' => isset($data['salary_end']) ? (int)$data['salary_end'] : $recruitment->salary_end,
                'career_id' => isset($data['career_id']) ? $data['career_id'] : $recruitment->career_id,
                'address_id' => isset($data['address_id']) ? $data['address_id'] : $recruitment->address_id,
                'degree_id' => isset($data['degree_id']) ? $data['degree_id'] : $recruitment->degree_id,
                'department_id' => isset($data['department_id']) ? $data['department_id'] : $recruitment->department_id,
                'expired' => isset($data['expired']) ? date('Y-m-d', strtotime(str_replace('/', '-', $data['expired']))) : $recruitment->expired,
                'active' => isset($data['active']) ? $data['active'] : $recruitment->active,
                'sort' => isset($data['sort']) ? $data['sort'] : $recruitment->sort,
                $locale => [
                    'title' => isset($data['title']) ? $data['title'] : $recruitment->title,
                    'slug' => isset($data['slug']) ? $data['slug'] : $recruitment->slug,
                    'description' => isset($data['description']) ? $data['description'] : $recruitment->description,
                    'content' => isset($data['content']) ? $data['content'] : $recruitment->content,
                    'rank' => isset($data['rank']) ? $data['rank'] : $recruitment->rank,
                    'form' => isset($data['form']) ?  $data['form'] : $recruitment->form,
                    'location' => isset($data['location']) ?  $data['location'] : $recruitment->location,
                    'contact' => isset($data['contact']) ?  $data['contact'] : $recruitment->contact,
                    'note' => isset($data['note']) ?  $data['note'] : $recruitment->note,
                ],
                $lang => [
                    'title' => $recruitment->translate($lang)->title,
                    'slug' => $recruitment->translate($lang)->slug,
                    'description' => $recruitment->translate($lang)->description,
                    'content' => $recruitment->translate($lang)->content,
                    'rank' => $recruitment->translate($lang)->rank,
                    'form' => isset($data['form']) ?  $data['form'] : $recruitment->form,
                    'location' => isset($data['location']) ?  $data['location'] : $recruitment->location,
                    'contact' => isset($data['contact']) ?  $data['contact'] : $recruitment->contact,
                    'note' => isset($data['note']) ?  $data['note'] : $recruitment->note,
                ],
            ];
            $recruitment->update($dataT);
        } catch (Exception $e) {
            DB::rollBack();

            throw new GeneralException(__('There was a problem updating the recruitment.'));
        }

        event(new RecruitmentUpdated($recruitment));

        DB::commit();

        return $recruitment;
    }

    /**
     * @param  Recruitment  $recruitment
     *
     * @return bool
     * @throws GeneralException
     */
    public function destroy(Recruitment $recruitment): bool
    {
        if ($this->deleteById($recruitment->id)) {
            event(new RecruitmentDeleted($recruitment));

            return true;
        }

        throw new GeneralException(__('There was a problem deleting the recruitment.'));
    }

    public function getRecruitmentTypical($limit = 3) {
        return $this->model->query()->orderBy('sort', 'asc')->limit($limit)->get();
    }

    //lấy tất cả tuyển dụng theo category
    public function getRecruitmentByCategory($address_id = null, $career_id = null, $wage = [], $keyword = null) {
        $query = $this->model->query();
        if($address_id) $query = $query->where('address_id', $address_id);
        if($career_id) $query = $query->where('career_id', $career_id);
        if($wage) $query = $query->where('salary_start', ">=", $wage[0])->where('salary_end', "<=", $wage[1]);
        if($keyword) $query = $query->where(function($builder) use($keyword){
            $builder->orWhereTranslationLike('title', "%$keyword%");
            $builder->orWhereTranslationLike('description','like', "%$keyword%");
        });
        return $query->paginate(6);
    }

    public function getRecruitmentBySlug($slug) {
        $recruitment = $this->model->query()->whereTranslationLike('slug', $slug)->first();
        return $recruitment;
    }
}
