<?php

namespace App\Services;

use App\Events\Contact\ContactCreated;
use App\Events\Contact\ContactDeleted;
use App\Events\Contact\ContactUpdated;
use App\Models\Contact;
use App\Exceptions\GeneralException;
use App\Services\BaseService;
use Exception;
use Illuminate\Support\Facades\DB;

/**
 * Class ContactService.
 */
class ContactService extends BaseService
{
    /**
     * ContactService constructor.
     *
     * @param  Contact  $contact
     */
    public function __construct(Contact $contact)
    {
        $this->model = $contact;
    }

    /**
     * @param  array  $data
     *
     * @return Contact
     * @throws GeneralException
     * @throws \Throwable
     */
    public function store(array $data = []): Contact
    {
        DB::beginTransaction();
        try {
            $contact = $this->model::create($data);
        } catch (Exception $e) {
            DB::rollBack();

            throw new GeneralException(__('There was a problem creating the '.$type.'.'));
        }

        event(new ContactCreated($contact));

        DB::commit();

        return $contact;
    }

    /**
     * @param  Contact  $contact
     * @param  array  $data
     *
     * @return Contact
     * @throws GeneralException
     * @throws \Throwable
     */
    public function update(Contact $contact, array $data = []): Contact
    {
        DB::beginTransaction();
        try {
            $contact->update($data);
        } catch (Exception $e) {
            DB::rollBack();

            throw new GeneralException(__('There was a problem updating the contact.'));
        }

        event(new ContactUpdated($contact));

        DB::commit();

        return $contact;
    }

    /**
     * @param  Contact  $contact
     *
     * @return bool
     * @throws GeneralException
     */
    public function destroy(Contact $contact): bool
    {
        if ($this->deleteById($contact->id)) {
            event(new ContactDeleted($contact));

            return true;
        }

        throw new GeneralException(__('There was a problem deleting the contact.'));
    }
}
