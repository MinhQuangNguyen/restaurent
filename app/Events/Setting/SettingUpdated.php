<?php

namespace App\Events\Setting;

use App\Models\Setting;
use Illuminate\Queue\SerializesModels;

/**
 * Class SettingUpdated.
 */
class SettingUpdated
{
    use SerializesModels;

    /**
     * @var
     */
    public $setting;

    /**
     * @param $setting
     */
    public function __construct(Setting $setting)
    {
        $this->setting = $setting;
    }
}
