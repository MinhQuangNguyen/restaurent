<?php

namespace App\Events\Menu;

use App\Models\Menu;
use Illuminate\Queue\SerializesModels;

/**
 * Class MenuUpdated.
 */
class MenuUpdated
{
    use SerializesModels;

    /**
     * @var
     */
    public $menu;

    /**
     * @param $menu
     */
    public function __construct(Menu $menu)
    {
        $this->menu = $menu;
    }
}
