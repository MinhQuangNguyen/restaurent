<?php

namespace App\Events\Menu;

use App\Models\Menu;
use Illuminate\Queue\SerializesModels;

/**
 * Class MenuDeleted.
 */
class MenuDeleted
{
    use SerializesModels;

    /**
     * @var
     */
    public $menu;

    /**
     * @param $menu
     */
    public function __construct(Menu $menu)
    {
        $this->menu = $menu;
    }
}
