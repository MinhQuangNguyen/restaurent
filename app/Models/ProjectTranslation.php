<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProjectTranslation extends Model
{
    protected $fillable = ['name', 'address', 'slug', 'description', 'overviews', 'positions', 'utilities', 'images'];
    public $timestamps = false;

    protected $casts = [
        'overviews' => 'json',
        'positions' => 'json',
        'utilities' => 'json',
        'images' => 'json'
    ];

    public function getOptionsAttribute($value)
    {
        return unserialize($value);
    }
}
