<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Astrotomic\Translatable\Translatable;

class Slide extends Model
{
    use Translatable;

    public $translatedAttributes = ['title', 'content', 'link'];

    protected $fillable = ['category_id', 'image', 'sort'];
    protected $guarded = [];

    public function getOptionsAttribute($value)
    {
        return unserialize($value);
    }
}
