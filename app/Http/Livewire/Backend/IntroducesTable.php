<?php

namespace App\Http\Livewire\Backend;

use App\Models\Menu;
use Illuminate\Database\Eloquent\Builder;
use Rappasoft\LaravelLivewireTables\TableComponent;
use Rappasoft\LaravelLivewireTables\Traits\HtmlComponents;
use Rappasoft\LaravelLivewireTables\Views\Column;

/**
 * Class IntroducesTable.
 */
class IntroducesTable extends TableComponent
{
    use HtmlComponents;

    /**
     * @var string
     */
    public $sortField = 'sort';

    /**
     * @var array
     */
    protected $options = [
        'bootstrap.container' => false,
        'bootstrap.classes.table' => 'table table-striped',
    ];

    /**
     * @return Builder
     */
    public function query(): Builder
    {
        return Menu::where('key', 'introduce')->whereNotNull('parent_id')->orderBy('sort', 'asc');
    }

    /**
     * @return array
     */
    public function columns(): array
    {
        return [
            Column::make(__('Name'), 'name')
                ->searchable(function ($builder, $term) {
                    return $builder->whereTranslationLike('name', '%'.$term.'%');
                })
                ->sortable(function ($builder, $direction) {
                    $locale = session()->get('locale') ?? env('APP_LANGUAGE');
                    return $builder->select('menus.*')->join('menu_translations as t', function ($query) use($locale) {
                        $query->on('menus.id', '=', 't.menu_id')
                            ->where('t.locale', '=', $locale);
                    })->orderBy('t.name', $direction);
                })
                ->format(function (Menu $model) {
                    return view('backend.introduce.includes.name', ['introduce' => $model]);
                }),
            Column::make(__('Link'), 'link')
                ->searchable(function ($builder, $term) {
                    return $builder->orWhereTranslationLike('link', '%'.$term.'%');
                })
                ->sortable(function ($builder, $direction) {
                    $locale = session()->get('locale') ?? env('APP_LANGUAGE');
                    return $builder->select('menus.*')->join('menu_translations as t', function ($query) use($locale) {
                        $query->on('menus.id', '=', 't.menu_id')
                            ->where('t.locale', '=', $locale);
                    })->orderBy('t.link', $direction);
                })
                ->format(function (Menu $model) {
                    return view('backend.introduce.includes.link', ['introduce' => $model]);
                }),
            Column::make(__('CreatedAt'), 'created_at')
                ->sortable()
                ->format(function (Menu $model) {
                    return date('d/m/Y', strtotime($model->created_at));
                }),
            Column::make(__('Actions'))
                ->format(function (Menu $model) {
                    return view('backend.introduce.includes.actions', ['introduce' => $model]);
                }),
        ];
    }
}
