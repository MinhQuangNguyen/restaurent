<?php

namespace App\Http\Controllers\Backend;

use App\Http\Requests\Backend\Menu\StoreMenuRequest;
use App\Http\Requests\Backend\Menu\EditMenuRequest;
use App\Http\Requests\Backend\Menu\UpdateMenuRequest;
use App\Http\Requests\Backend\Menu\DeleteMenuRequest;
use App\Models\Menu;
use App\Services\MenuService;
use App\Services\CategoryService;
use App\Http\Controllers\Controller;

/**
 * Class MenuController.
 */
class MenuController extends Controller
{
    /**
     * @var MenuService
     */
    protected $menuService;

    /**
     * MenuController constructor.
     *
     * @param  MenuService  $menuService
     * @param  CategoryService  $categoryService
     */
    public function __construct(MenuService $menuService, CategoryService $categoryService)
    {
        $this->menuService = $menuService;
        $this->categoryService = $categoryService;
    }

    /**
     * @return \Illuminate\View\View
     */
    public function index()
    {
        return view('backend.menu.index');
    }

    /**
     * @return mixed
     */
    public function create()
    {
        return view('backend.menu.create')
            ->withParents($this->menuService->getParents())
            ->withCategories($this->categoryService->getAllCategories());
    }

    /**
     * @param  StoreMenuRequest  $request
     *
     * @return mixed
     * @throws \App\Exceptions\GeneralException
     * @throws \Throwable
     */
    public function store(StoreMenuRequest $request)
    {
        $this->menuService->store($request->validated());

        return redirect()->route('admin.menu.index')->withFlashSuccess(__('The menu was successfully created.'));
    }

    /**
     * @param  EditMenuRequest  $request
     * @param  Menu  $menu
     *
     * @return mixed
     */
    public function edit(EditMenuRequest $request, Menu $menu)
    {
        return view('backend.menu.edit')
            ->withMenu($menu)
            ->withParents($this->menuService->getParents($menu->id))
            ->withCategories($this->categoryService->getAllCategories());
    }

    /**
     * @param  UpdateMenuRequest  $request
     * @param  Menu  $menu
     *
     * @return mixed
     * @throws \App\Exceptions\GeneralException
     * @throws \Throwable
     */
    public function update(UpdateMenuRequest $request, Menu $menu)
    {
        $this->menuService->update($menu, $request->validated());

        return redirect()->route('admin.menu.index')->withFlashSuccess(__('The menu was successfully updated.'));
    }

    /**
     * @param  DeleteMenuRequest  $request
     * @param  Menu  $menu
     *
     * @return mixed
     * @throws \Exception
     */
    public function destroy(DeleteMenuRequest $request, Menu $menu)
    {
        $this->menuService->destroy($menu);

        return redirect()->route('admin.menu.index')->withFlashSuccess(__('The menu was successfully deleted.'));
    }
}
