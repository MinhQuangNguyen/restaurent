<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Services\CategoryService;
use App\Services\PostService;
use App\Models\Category;
use Illuminate\Http\Request;

/**
 * Class NewsController.
 */
class DevilController extends Controller
{
    /**
     * NewsController constructor.
     *
     * @param  PostService  $postService
     * @param  CategoryService  $categoryService
     */
    public function __construct(PostService $postService, CategoryService $categoryService)
    {
        $this->postService = $postService;
        $this->categoryService = $categoryService;
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        $posts = $this->postService->getPostByCategory();
        return view('frontend.devil.index', [
            'posts' => $posts
        ]);
    }

    public function detail(Request $request, $slug)
    {
        $post = $this->postService->getPostBySlug($slug);
        $category = $this->categoryService->where('id', $post->category_id)->where('active', 1)->first();
        $title = $category->type === Category::TYPE_LIBRARY ? $post->type === 0 ? 'Hình ảnh' : 'Video' : $category->name;
        $slides = [
            [
                'title' => $title,
                'content' => $post->title,
                'image' => $post->banner,
            ]
        ];

        $news_relates = $this->postService->getPostRelate($post->category_id, $post->id, $post->type);
        $random_post = array_rand(json_decode(json_encode($news_relates), true));
        
        return view('frontend.devil.detail', [
            'slides' => json_decode(json_encode($slides)),
            'post' => $post,
            'news_relates' => $news_relates,
            'random_post' => $random_post
        ]);
    }
}
