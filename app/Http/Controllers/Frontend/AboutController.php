<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Services\MenuService;
use App\Services\SettingService;


/**
 * Class AboutController.
 */
class AboutController extends Controller
{
    public function __construct(MenuService $menuService, SettingService $settingService)
    {
        $this->menuService = $menuService;
        $this->settingService = $settingService;
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(){
        $setting = $this->menuService->getByKey('introduce');
        $about = ['name' => '', 'content' => ''];
        if($setting) $about = [
            'name' => $setting[0]->name,
            'content' => $setting[0]->content
        ];

        // dd($about->content);

        return view('frontend.about.index', [
            "about" => $setting[0]
        ]);
    }

    // public function vision()
    // {
    //     return view('frontend.about.vision');
    // }

    // public function strategy()
    // {
    //     return view('frontend.about.strategy');
    // }

    // public function mission()
    // {
    //     return view('frontend.about.mission');
    // }

    // public function coreValues()
    // {
    //     return view('frontend.about.coreValues');
    // }

    // public function brand()
    // {
    //     return view('frontend.about.brand');
    // }

    // public function organizationalChart()
    // {
    //     return view('frontend.about.organizationalChart');
    // }
}
