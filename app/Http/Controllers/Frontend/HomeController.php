<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Services\CategoryService;
use App\Services\ProjectService;
use App\Services\SettingService;
use App\Models\Category;
use App\Services\PostService;

/**
 * Class HomeController.
 */
class HomeController extends Controller
{
    /**
     * ProjectController constructor.
     *
     * @param  CategoryService  $categoryService
     * @param  ProjectService  $projectService
     * @param  SettingService  $settingService
     */
    public function __construct(CategoryService $categoryService, ProjectService $projectService, SettingService $settingService, PostService $postService)
    {
        $this->categoryService = $categoryService;
        $this->projectService = $projectService;
        $this->settingService = $settingService;
        $this->postService = $postService;
    }
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $projectCategory = $this->categoryService->getByType([Category::TYPE_PROJECT]);
        $slides = $this->categoryService->getSlideByKey('homepage-top');
        $projects = $this->projectService->getProjectOutstanding();
        $partners = $this->categoryService->getSlideByKey('homepage-footer');
        $newCategory = $this->categoryService->getByType([Category::TYPE_NEW, Category::TYPE_LIBRARY]);
        $setting = $this->settingService->getSettingByKey('future');
        $future = ['name' => '', 'description' => ''];
        if($setting) $future = [
            'name' => $setting->name,
            'description' => $setting->value
        ];

        $posts = $this->postService->getPostHighLight();

        return view('frontend.home.index', [
            "projectCategory" => $projectCategory,
            "slides" => $slides,
            "projects" => $projects,
            "partners" => $partners,
            "newCategory" => $newCategory,
            "future" => $future,
            "posts" => $posts
        ]);
    }
}
