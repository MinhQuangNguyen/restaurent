<?php

namespace App\Http\Requests\Backend\Post;

use App\Models\Post;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

/**
 * Class UpdatePostRequest.
 */
class UpdatePostRequest extends FormRequest
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => ['required', 'max:100'],
            'author' => ['required', 'max:100'],
            'tagser' => ['required', 'max:100'],
            'description_short' => ['max:255'],
            'content' => ['max:10000000'],
            'slug' => ['max:255'],
            'type' => ['required', Rule::in([Post::TYPE_NEW_IMAGE, Post::TYPE_VIDEO])],
            'image' => ['image', 'mimes:jpeg,jpg,png,gif', 'max:10000'],
            'sort' => ['nullable', 'numeric'],
            'active' => ['required', 'numeric'],
            'category_id' => ['required', 'numeric'],
            'is_highlight' => ['nullable', 'numeric'],
            'link' => ['max:10000000'],
        ];
    }
}
