<?php

use App\Http\Controllers\Frontend\HomeController;
use App\Http\Controllers\Frontend\AboutController;
use App\Http\Controllers\Frontend\NewsController;
use App\Http\Controllers\Frontend\CustomerController;
use App\Http\Controllers\Frontend\AnimalsController;
use App\Http\Controllers\Frontend\DarkAngelController;
use App\Http\Controllers\Frontend\WarriorController;
use App\Http\Controllers\Frontend\DevilController;
use App\Http\Controllers\Frontend\LastimeController;
use App\Http\Controllers\Frontend\FallenController;
use App\Http\Controllers\Frontend\ContactController;

use Tabuna\Breadcrumbs\Trail;

/*
 * Frontend Controllers
 * All route names are prefixed with 'frontend.'.
 */

Route::get('/', [HomeController::class, 'index'])
    ->name('index')
    ->breadcrumbs(function (Trail $trail) {
        $trail->push(__('Home'), route('frontend.index'));
    });

Route::get('/404', function () { abort(404);});


Route::get('/tieu-su', [AboutController::class, 'index']);
Route::get('/about', [AboutController::class, 'index']);


Route::get('/tatto', [NewsController::class, 'index']);
Route::get('/tatto', [NewsController::class, 'index']);


// Route::get('/tin-tuc', function () {
//     return redirect('/tin-tuc');
// });
// Route::get('/news', function () {
//     return redirect('/news');
// });


Route::get('/lien-he', [ContactController::class, 'index'])->name('lien-he.index');
Route::get('/contact', [ContactController::class, 'index'])->name('contact.index');
Route::post('/contact', [ContactController::class, 'store'])->name('contact.store');

Route::get('/khach-hang', [CustomerController::class, 'index'])->name('khach-hang.index');
Route::get('/customer', [CustomerController::class, 'index'])->name('customer.index');

Route::get('/animals', [AnimalsController::class, 'index'])->name('animals.index');

Route::get('/dark-angel', [DarkAngelController::class, 'index'])->name('angel.index');

Route::get('/ultimate-warrior', [WarriorController::class, 'index'])->name('warrior.index');

Route::get('/devil-fairy', [DevilController::class, 'index'])->name('devil.index');

Route::get('/the-last-time', [LastimeController::class, 'index'])->name('lastime.index');

Route::get('/fallen-angel', [FallenController::class, 'index'])->name('fallen.index');

Route::get('terms', [TermsController::class, 'index'])
    ->name('pages.terms')
    ->breadcrumbs(function (Trail $trail) {
        $trail->parent('frontend.index')
            ->push(__('Terms & Conditions'), route('frontend.pages.terms'));
    });
