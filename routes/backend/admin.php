<?php

use App\Http\Controllers\Backend\DashboardController;
use App\Http\Controllers\Backend\MenuController;
use App\Http\Controllers\Backend\IntroduceController;
use App\Http\Controllers\Backend\ProjectController;
use App\Http\Controllers\Backend\CategoryController;
use App\Http\Controllers\Backend\FieldController;
use App\Http\Controllers\Backend\PostController;
use App\Http\Controllers\Backend\ContactController;
use App\Http\Controllers\Backend\SettingController;
use App\Http\Controllers\Backend\BannerController;
use App\Http\Controllers\Backend\RecruitmentController;
use App\Http\Controllers\Backend\ImageController;
use Tabuna\Breadcrumbs\Trail;
use App\Models\Menu;
use App\Models\Project;
use App\Models\Category;
use App\Models\Post;
use App\Models\Contact;
use App\Models\Setting;
use App\Models\Recruitment;

// All route names are prefixed with 'admin.'.
Route::redirect('/', '/admin/dashboard', 301);
Route::get('dashboard', [DashboardController::class, 'index'])
    ->name('dashboard')
    ->breadcrumbs(function (Trail $trail) {
        $trail->push(__('Home'), route('admin.dashboard'));
    });

Route::group([
    'prefix' => 'menu',
    'as' => 'menu.',
    'middleware' => 'permission:admin.access.menu.list'
], function () {
    Route::get('/', [MenuController::class, 'index'])
        ->name('index')
        ->breadcrumbs(function (Trail $trail) {
            $trail->parent('admin.dashboard')
                ->push(__('Menu Management'), route('admin.menu.index'));
        });
    Route::get('create', [MenuController::class, 'create'])
        ->name('create')
        ->breadcrumbs(function (Trail $trail) {
            $trail->parent('admin.menu.index')
                ->push(__('Create Menu'), route('admin.menu.create'));
        });

    Route::post('/', [MenuController::class, 'store'])->name('store');

    Route::group(['prefix' => '{menu}'], function () {
        Route::get('edit', [MenuController::class, 'edit'])
            ->name('edit')
            ->breadcrumbs(function (Trail $trail, Menu $menu) {
                $trail->parent('admin.menu.index')
                    ->push(__('Editing :menu', ['menu' => $menu->name]), route('admin.menu.edit', $menu));
            });

        Route::patch('/', [MenuController::class, 'update'])->name('update');
        Route::delete('/', [MenuController::class, 'destroy'])->name('destroy');
    });
});

Route::group([
    'prefix' => 'introduce',
    'as' => 'introduce.',
    'middleware' => 'permission:admin.access.introduce.list'
], function () {
    Route::get('/', [IntroduceController::class, 'index'])
        ->name('index')
        ->breadcrumbs(function (Trail $trail) {
            $trail->parent('admin.dashboard')
                ->push(__('Introduce Management'), route('admin.introduce.index'));
        });

    Route::get('create', [IntroduceController::class, 'create'])
        ->name('create')
        ->breadcrumbs(function (Trail $trail) {
            $trail->parent('admin.introduce.index')
                ->push(__('Create Introduce'), route('admin.introduce.create'));
        });

    Route::post('/', [IntroduceController::class, 'store'])->name('store');

    Route::group(['prefix' => '{introduce}'], function () {
        Route::get('edit', [IntroduceController::class, 'edit'])
            ->name('edit')
            ->breadcrumbs(function (Trail $trail, Menu $introduce) {
                $trail->parent('admin.introduce.index')
                    ->push(__('Editing :introduce', ['introduce' => $introduce->name]), route('admin.introduce.edit', $introduce));
            });

        Route::patch('/', [IntroduceController::class, 'update'])->name('update');
        Route::delete('/', [IntroduceController::class, 'destroy'])->name('destroy');
    });
});

Route::group([
    'prefix' => 'project',
    'as' => 'project.',
    'middleware' => 'permission:admin.access.project.list'
], function () {
    Route::get('/', [ProjectController::class, 'index'])
        ->name('index')
        ->breadcrumbs(function (Trail $trail) {
            $trail->parent('admin.dashboard')
                ->push(__('Project Management'), route('admin.project.index'));
        });

    Route::get('create', [ProjectController::class, 'create'])
        ->name('create')
        ->breadcrumbs(function (Trail $trail) {
            $trail->parent('admin.project.index')
                ->push(__('Create Project'), route('admin.project.create'));
        });

    Route::post('/', [ProjectController::class, 'store'])->name('store');

    Route::group(['prefix' => '{project}'], function () {
        Route::get('edit', [ProjectController::class, 'edit'])
            ->name('edit')
            ->breadcrumbs(function (Trail $trail, Project $project) {
                $trail->parent('admin.project.index')
                    ->push(__('Editing :project', ['project' => $project->name]), route('admin.project.edit', $project));
            });

        Route::patch('/', [ProjectController::class, 'update'])->name('update');
        Route::delete('/', [ProjectController::class, 'destroy'])->name('destroy');
    });
});

Route::group([
    'prefix' => 'field',
    'as' => 'field.',
    'middleware' => 'permission:admin.access.field.list'
], function () {
    Route::get('/', [FieldController::class, 'index'])
        ->name('index')
        ->breadcrumbs(function (Trail $trail) {
            $trail->parent('admin.dashboard')
                ->push(__('Field Management'), route('admin.field.index'));
        });

    Route::get('create', [FieldController::class, 'create'])
        ->name('create')
        ->breadcrumbs(function (Trail $trail) {
            $trail->parent('admin.field.index')
                ->push(__('Create Field'), route('admin.field.create'));
        });

    Route::post('/', [FieldController::class, 'store'])->name('store');

    Route::group(['prefix' => '{field}'], function () {
        Route::get('edit', [FieldController::class, 'edit'])
            ->name('edit')
            ->breadcrumbs(function (Trail $trail, Category $field) {
                $trail->parent('admin.field.index')
                    ->push(__('Editing :field', ['field' => $field->name]), route('admin.field.edit', $field));
            });

        Route::patch('/', [FieldController::class, 'update'])->name('update');
        Route::delete('/', [FieldController::class, 'destroy'])->name('destroy');
    });
});

Route::group([
    'prefix' => 'category',
    'as' => 'category.',
    'middleware' => 'permission:admin.access.category.list'
], function () {
    Route::get('/', [CategoryController::class, 'index'])
        ->name('index')
        ->breadcrumbs(function (Trail $trail) {
            $trail->parent('admin.dashboard')
                ->push(__('Category Management'), route('admin.category.index'));
        });

    Route::get('create', [CategoryController::class, 'create'])
        ->name('create')
        ->breadcrumbs(function (Trail $trail) {
            $trail->parent('admin.category.index')
                ->push(__('Create Category'), route('admin.category.create'));
        });

    Route::post('/', [CategoryController::class, 'store'])->name('store');

    Route::group(['prefix' => '{category}'], function () {
        Route::get('edit', [CategoryController::class, 'edit'])
            ->name('edit')
            ->breadcrumbs(function (Trail $trail, Category $category) {
                $trail->parent('admin.category.index')
                    ->push(__('Editing :category', ['category' => $category->name]), route('admin.category.edit', $category));
            });

        Route::patch('/', [CategoryController::class, 'update'])->name('update');
        Route::delete('/', [CategoryController::class, 'destroy'])->name('destroy');
    });
});

Route::group([
    'prefix' => 'post',
    'as' => 'post.',
    'middleware' => 'permission:admin.access.post.list'
], function () {
    Route::get('/', [PostController::class, 'index'])
        ->name('index')
        ->breadcrumbs(function (Trail $trail) {
            $trail->parent('admin.dashboard')
                ->push(__('Post Management'), route('admin.post.index'));
        });

    Route::get('create', [PostController::class, 'create'])
        ->name('create')
        ->breadcrumbs(function (Trail $trail) {
            $trail->parent('admin.post.index')
                ->push(__('Create Post'), route('admin.post.create'));
        });

    Route::post('/', [PostController::class, 'store'])->name('store');

    Route::group(['prefix' => '{post}'], function () {
        Route::get('edit', [PostController::class, 'edit'])
            ->name('edit')
            ->breadcrumbs(function (Trail $trail, Post $post) {
                $trail->parent('admin.post.index')
                    ->push(__('Editing :post', ['post' => $post->title]), route('admin.post.edit', $post));
            });

        Route::patch('/', [PostController::class, 'update'])->name('update');
        Route::delete('/', [PostController::class, 'destroy'])->name('destroy');
    });
});

Route::group([
    'prefix' => 'banner',
    'as' => 'banner.',
    'middleware' => 'permission:admin.access.banner.list'
], function () {
    Route::get('/', [BannerController::class, 'index'])
        ->name('index')
        ->breadcrumbs(function (Trail $trail) {
            $trail->parent('admin.dashboard')
                ->push(__('Banner Management'), route('admin.banner.index'));
        });

    Route::group(['prefix' => '{banner}'], function () {
        Route::get('edit', [BannerController::class, 'edit'])
            ->name('edit')
            ->breadcrumbs(function (Trail $trail, Category $banner) {
                $trail->parent('admin.banner.index')
                    ->push(__('Editing :banner', ['banner' => $banner->name]), route('admin.banner.edit', $banner));
            });

        Route::patch('/', [BannerController::class, 'update'])->name('update');
    });
});

Route::group([
    'prefix' => 'recruitment',
    'as' => 'recruitment.',
    'middleware' => 'permission:admin.access.recruitment.list'
], function () {
    Route::get('/', [RecruitmentController::class, 'index'])
        ->name('index')
        ->breadcrumbs(function (Trail $trail) {
            $trail->parent('admin.dashboard')
                ->push(__('Recruitment Management'), route('admin.recruitment.index'));
        });

    Route::get('create', [RecruitmentController::class, 'create'])
        ->name('create')
        ->breadcrumbs(function (Trail $trail) {
            $trail->parent('admin.recruitment.index')
                ->push(__('Create Recruitment'), route('admin.recruitment.create'));
        });

    Route::post('/', [RecruitmentController::class, 'store'])->name('store');

    Route::group(['prefix' => '{recruitment}'], function () {
        Route::get('edit', [RecruitmentController::class, 'edit'])
            ->name('edit')
            ->breadcrumbs(function (Trail $trail, Recruitment $recruitment) {
                $trail->parent('admin.recruitment.index')
                    ->push(__('Editing :recruitment', ['recruitment' => $recruitment->title]), route('admin.recruitment.edit', $recruitment));
            });

        Route::patch('/', [RecruitmentController::class, 'update'])->name('update');
        Route::delete('/', [RecruitmentController::class, 'destroy'])->name('destroy');
    });
});

Route::group([
    'prefix' => 'contact',
    'as' => 'contact.',
    'middleware' => 'permission:admin.access.contact.list'
], function () {
    Route::get('/', [ContactController::class, 'index'])
        ->name('index')
        ->breadcrumbs(function (Trail $trail) {
            $trail->parent('admin.dashboard')
                ->push(__('Contact Management'), route('admin.contact.index'));
        });

    Route::group(['prefix' => '{contact}'], function () {
        Route::get('detail', [ContactController::class, 'detail'])
            ->name('detail')
            ->breadcrumbs(function (Trail $trail, Contact $contact) {
                $trail->parent('admin.contact.index')
                    ->push(__('Detail :contact', ['contact' => $contact->title]), route('admin.contact.detail', $contact));
            });

        Route::delete('/', [ContactController::class, 'destroy'])->name('destroy');
    });
});

Route::group([
    'prefix' => 'setting',
    'as' => 'setting.',
    'middleware' => 'permission:admin.access.setting.list'
], function () {
    Route::get('/', [SettingController::class, 'index'])
        ->name('index')
        ->breadcrumbs(function (Trail $trail) {
            $trail->parent('admin.dashboard')
                ->push(__('Setting'), route('admin.setting.index'));
        });

    Route::group(['prefix' => '{setting}'], function () {
        Route::get('edit', [SettingController::class, 'edit'])
            ->name('edit')
            ->breadcrumbs(function (Trail $trail, Setting $setting) {
                $trail->parent('admin.contact.index')
                    ->push(__('Editing :setting', ['setting' => $setting->title]), route('admin.setting.edit', $setting));
            });

        Route::patch('/', [SettingController::class, 'update'])->name('update');
        Route::delete('/', [SettingController::class, 'destroy'])->name('destroy');
    });
});

Route::post('image/storeMedia', [ImageController::class, 'storeMedia'])->name('image.storeMedia');
Route::post('image/deleteMedia', [ImageController::class, 'deleteMedia'])->name('image.deleteMedia');
