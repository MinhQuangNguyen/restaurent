/*
 Copyright (c) 2007-2017, CKSource - Frederico Knabben. All rights reserved.
 For licensing, see LICENSE.html or http://cksource.com/ckfinder/license
 */

var config = {};

// Set your configuration options below.

config.plugins = [
  'watermark'
];
config.Watermark = {

  // Optional, hide or show save button (default to true)
  overwriteButton: true,

  // Optional, hide or show save as new file button (default to true)
  newFileButton: false,

  // Optional, file suffix if save as new file (default to "-watermark")
  newFileSuffix: '-branded',

  // Required, watermark presets.
  watermarks: [
    {
      file: 'https://cehko.com.vn/themes/wp-content/uploads/2019/cehko-logo.png',
      label: 'CEHKO',
      size: '15',
      position: 'bottom right'
    },
    {
      file: 'http://cehko.com.vn/themes/cehko/images/logo_greenlife.png',
      label: 'Green Life',
      size: '15',
      position: 'bottom right'
    },

  ]
};
// Examples:
config.language = 'vi';
// config.skin = 'jquery-mobile';

CKFinder.define( config );
