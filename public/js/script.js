function checkStoryHeight() {
    var e = $(".art-story .story-description").innerHeight(),
        t = $(".art-story .img-content").innerHeight();
    if ($(window).resize(function () {
            if ($(window).width() > 1199) {
                var e = $(".art-story .story-description").innerHeight(),
                    t = $(".art-story .img-content").innerHeight();
                if (e <= t) {
                    var i = t - e;
                    $(".art-story").addClass("margin-bottom"), $(".art-story").css("margin-bottom", i)
                } else $(".art-story").css("margin-bottom", "0")
            } else $(".art-story").css("margin-bottom", "0")
        }), $(window).width() > 1199)
        if (e <= t) {
            var i = t - e;
            $(".art-story").addClass("margin-bottom"), $(".art-story").css("margin-bottom", i)
        } else $(".art-story").css("margin-bottom", "0");
    else $(".art-story").css("margin-bottom", "0")
}

function handleMenu() {
    console.log(1);
    $("body").toggleClass("menu-active");
    $("body").addClass("animating-menu");
    setTimeout(function () {
        $("body").removeClass("animating-menu")
    }, 850)
}

function handleMainMenu () {
    $("body").toggleClass("menu-active");
    $("body").addClass("animating-menu");
    setTimeout(function () {
        $("body").removeClass("animating-menu")
    }, 850)
}

function init_menu() {
    // $("#menu").click(function (e) {
    //         e.preventDefault();
    //         console.log(1);
    //         $("body").toggleClass("menu-active");
    //         $("body").addClass("animating-menu");
    //         setTimeout(function () {
    //             $("body").removeClass("animating-menu")
    //         }, 850)
    //     }),

    // $(".main-menu a").click(function (e) {
    //     e.preventDefault();
    //     $("body").toggleClass("menu-active");
    //     $("body").addClass("animating-menu");
    //     setTimeout(function () {
    //         $("body").removeClass("animating-menu")
    //     }, 850)
    // })
}

function load_reveal() {
    $(window).width() > 680 ? $(".reveal").waypoint({
        handler: function () {
            var e = $(this.element).data("delay") || 0,
                t = $(this.element);
            setTimeout(function () {
                t.addClass("animated")
            }, e)
        },
        offset: function () {
            var e = $(this.element).data("offset") || 0;
            return Waypoint.viewportWidth() <= 480 ? Waypoint.viewportHeight() - 180 : Waypoint.viewportHeight() + e
        }
    }) : $(".reveal").addClass("animated")
}

function fancybox() {
    $('[data-fancybox="gallery"]').fancybox({
        transitionEffect: "zoom-in-out"
    })
}

function ObjectFit() {
    if (!1 === function () {
            var e = document.createElement("div").style;
            return "object-fit" in e || "-o-object-fit" in e
        }()) {
        var e, t, i = document.getElementsByClassName("obj-fit-cover"),
            n = i.length,
            s = 0;
        for (s; s < n; s++) {
            "currentSrc" in i[s] ? "" === (e = i[s].currentSrc) && (e = i[s].src) : e = i[s].src, t = i[s].className;
            var o = document.createElement("div");
            o.className = t, o.style.background = "url(" + e + ") center center / cover no-repeat";
            var a = document.createElement("img");
            a.src = e, a.style.cssText = "display: block; width: 100%; height: 100%; visibility: hidden!important; opacity: 0!important;", o.appendChild(a), i[s].parentNode.replaceChild(o, i[s]), o = null, a = null
        }
        i = null
    }
}

function rellax() {
    $(".rellax").length && $(window).width() > 680 && new Rellax(".rellax", {
        speed: -1,
        center: !0,
        round: !0,
        vertical: !0,
        horizontal: !1
    })
}
window.log = function () {
        if (log.history = log.history || [], log.history.push(arguments), this.console) {
            arguments.callee = arguments.callee.caller;
            var e = [].slice.call(arguments);
            "object" == typeof console.log ? log.apply.call(console.log, console, e) : console.log.apply(console, e)
        }
    },
    function (e) {
        function t() {}
        for (var i, n = "assert,count,debug,dir,dirxml,error,exception,group,groupCollapsed,groupEnd,info,log,timeStamp,profile,profileEnd,time,timeEnd,trace,warn".split(","); i = n.pop();) e[i] = e[i] || t
    }(function () {
        try {
            return console.log(), window.console
        } catch (e) {
            return window.console = {}
        }
    }());
var DOM = window.DOM || {};
DOM.Methods = {}, DOM.LoadMethod = function (e) {
        void 0 === e && (e = $(document)), e.find("*[data-method]").each(function () {
            var e = $(this),
                t = e.attr("data-method");
            $.each(t.split(" "), function (t, i) {
                try {
                    var n = DOM.Methods[i];
                    new n(e)
                } catch (e) {}
            })
        })
    }, DOM.onReady = function () {        
        DOM.LoadMethod(), $("html").removeClass("no-js").addClass("js")
    }, $(document).ready(function () {
        DOM.onReady()
    }),
    function (e, t) {
        "object" == typeof exports && "object" == typeof module ? module.exports = t() : "function" == typeof define && define.amd ? define("Barba", [], t) : "object" == typeof exports ? exports.Barba = t() : e.Barba = t()
    }(this, function () {
        return function (e) {
            function t(n) {
                if (i[n]) return i[n].exports;
                var s = i[n] = {
                    exports: {},
                    id: n,
                    loaded: !1
                };
                return e[n].call(s.exports, s, s.exports, t), s.loaded = !0, s.exports
            }
            var i = {};
            return t.m = e, t.c = i, t.p = "http://localhost:8080/dist", t(0)
        }([function (e, t, i) {
            "function" != typeof Promise && (window.Promise = i(1));
            var n = {
                version: "1.0.0",
                BaseTransition: i(4),
                BaseView: i(6),
                BaseCache: i(8),
                Dispatcher: i(7),
                HistoryManager: i(9),
                Pjax: i(10),
                Prefetch: i(13),
                Utils: i(5)
            };
            e.exports = n
        }, function (e, t, i) {
            (function (t) {
                ! function (i) {
                    function n() {}

                    function s(e, t) {
                        return function () {
                            e.apply(t, arguments)
                        }
                    }

                    function o(e) {
                        if ("object" != typeof this) throw new TypeError("Promises must be constructed via new");
                        if ("function" != typeof e) throw new TypeError("not a function");
                        this._state = 0, this._handled = !1, this._value = void 0, this._deferreds = [], p(e, this)
                    }

                    function a(e, t) {
                        for (; 3 === e._state;) e = e._value;
                        return 0 === e._state ? void e._deferreds.push(t) : (e._handled = !0, void h(function () {
                            var i = 1 === e._state ? t.onFulfilled : t.onRejected;
                            if (null === i) return void(1 === e._state ? r : l)(t.promise, e._value);
                            var n;
                            try {
                                n = i(e._value)
                            } catch (e) {
                                return void l(t.promise, e)
                            }
                            r(t.promise, n)
                        }))
                    }

                    function r(e, t) {
                        try {
                            if (t === e) throw new TypeError("A promise cannot be resolved with itself.");
                            if (t && ("object" == typeof t || "function" == typeof t)) {
                                var i = t.then;
                                if (t instanceof o) return e._state = 3, e._value = t, void d(e);
                                if ("function" == typeof i) return void p(s(i, t), e)
                            }
                            e._state = 1, e._value = t, d(e)
                        } catch (t) {
                            l(e, t)
                        }
                    }

                    function l(e, t) {
                        e._state = 2, e._value = t, d(e)
                    }

                    function d(e) {
                        2 === e._state && 0 === e._deferreds.length && h(function () {
                            e._handled || f(e._value)
                        });
                        for (var t = 0, i = e._deferreds.length; t < i; t++) a(e, e._deferreds[t]);
                        e._deferreds = null
                    }

                    function c(e, t, i) {
                        this.onFulfilled = "function" == typeof e ? e : null, this.onRejected = "function" == typeof t ? t : null, this.promise = i
                    }

                    function p(e, t) {
                        var i = !1;
                        try {
                            e(function (e) {
                                i || (i = !0, r(t, e))
                            }, function (e) {
                                i || (i = !0, l(t, e))
                            })
                        } catch (e) {
                            if (i) return;
                            i = !0, l(t, e)
                        }
                    }
                    var u = setTimeout,
                        h = "function" == typeof t && t || function (e) {
                            u(e, 0)
                        },
                        f = function (e) {
                            "undefined" != typeof console && console && console.warn("Possible Unhandled Promise Rejection:", e)
                        };
                    o.prototype.catch = function (e) {
                        return this.then(null, e)
                    }, o.prototype.then = function (e, t) {
                        var i = new this.constructor(n);
                        return a(this, new c(e, t, i)), i
                    }, o.all = function (e) {
                        var t = Array.prototype.slice.call(e);
                        return new o(function (e, i) {
                            function n(o, a) {
                                try {
                                    if (a && ("object" == typeof a || "function" == typeof a)) {
                                        var r = a.then;
                                        if ("function" == typeof r) return void r.call(a, function (e) {
                                            n(o, e)
                                        }, i)
                                    }
                                    t[o] = a, 0 == --s && e(t)
                                } catch (e) {
                                    i(e)
                                }
                            }
                            if (0 === t.length) return e([]);
                            for (var s = t.length, o = 0; o < t.length; o++) n(o, t[o])
                        })
                    }, o.resolve = function (e) {
                        return e && "object" == typeof e && e.constructor === o ? e : new o(function (t) {
                            t(e)
                        })
                    }, o.reject = function (e) {
                        return new o(function (t, i) {
                            i(e)
                        })
                    }, o.race = function (e) {
                        return new o(function (t, i) {
                            for (var n = 0, s = e.length; n < s; n++) e[n].then(t, i)
                        })
                    }, o._setImmediateFn = function (e) {
                        h = e
                    }, o._setUnhandledRejectionFn = function (e) {
                        f = e
                    }, void 0 !== e && e.exports ? e.exports = o : i.Promise || (i.Promise = o)
                }(this)
            }).call(t, i(2).setImmediate)
        }, function (e, t, i) {
            (function (e, n) {
                function s(e, t) {
                    this._id = e, this._clearFn = t
                }
                var o = i(3).nextTick,
                    a = Function.prototype.apply,
                    r = Array.prototype.slice,
                    l = {},
                    d = 0;
                t.setTimeout = function () {
                    return new s(a.call(setTimeout, window, arguments), clearTimeout)
                }, t.setInterval = function () {
                    return new s(a.call(setInterval, window, arguments), clearInterval)
                }, t.clearTimeout = t.clearInterval = function (e) {
                    e.close()
                }, s.prototype.unref = s.prototype.ref = function () {}, s.prototype.close = function () {
                    this._clearFn.call(window, this._id)
                }, t.enroll = function (e, t) {
                    clearTimeout(e._idleTimeoutId), e._idleTimeout = t
                }, t.unenroll = function (e) {
                    clearTimeout(e._idleTimeoutId), e._idleTimeout = -1
                }, t._unrefActive = t.active = function (e) {
                    clearTimeout(e._idleTimeoutId);
                    var t = e._idleTimeout;
                    t >= 0 && (e._idleTimeoutId = setTimeout(function () {
                        e._onTimeout && e._onTimeout()
                    }, t))
                }, t.setImmediate = "function" == typeof e ? e : function (e) {
                    var i = d++,
                        n = !(arguments.length < 2) && r.call(arguments, 1);
                    return l[i] = !0, o(function () {
                        l[i] && (n ? e.apply(null, n) : e.call(null), t.clearImmediate(i))
                    }), i
                }, t.clearImmediate = "function" == typeof n ? n : function (e) {
                    delete l[e]
                }
            }).call(t, i(2).setImmediate, i(2).clearImmediate)
        }, function (e, t) {
            function i() {
                p && d && (p = !1, d.length ? c = d.concat(c) : u = -1, c.length && n())
            }

            function n() {
                if (!p) {
                    var e = a(i);
                    p = !0;
                    for (var t = c.length; t;) {
                        for (d = c, c = []; ++u < t;) d && d[u].run();
                        u = -1, t = c.length
                    }
                    d = null, p = !1, r(e)
                }
            }

            function s(e, t) {
                this.fun = e, this.array = t
            }

            function o() {}
            var a, r, l = e.exports = {};
            ! function () {
                try {
                    a = setTimeout
                } catch (e) {
                    a = function () {
                        throw new Error("setTimeout is not defined")
                    }
                }
                try {
                    r = clearTimeout
                } catch (e) {
                    r = function () {
                        throw new Error("clearTimeout is not defined")
                    }
                }
            }();
            var d, c = [],
                p = !1,
                u = -1;
            l.nextTick = function (e) {
                var t = new Array(arguments.length - 1);
                if (arguments.length > 1)
                    for (var i = 1; i < arguments.length; i++) t[i - 1] = arguments[i];
                c.push(new s(e, t)), 1 !== c.length || p || a(n, 0)
            }, s.prototype.run = function () {
                this.fun.apply(null, this.array)
            }, l.title = "browser", l.browser = !0, l.env = {}, l.argv = [], l.version = "", l.versions = {}, l.on = o, l.addListener = o, l.once = o, l.off = o, l.removeListener = o, l.removeAllListeners = o, l.emit = o, l.binding = function (e) {
                throw new Error("process.binding is not supported")
            }, l.cwd = function () {
                return "/"
            }, l.chdir = function (e) {
                throw new Error("process.chdir is not supported")
            }, l.umask = function () {
                return 0
            }
        }, function (e, t, i) {
            var n = i(5),
                s = {
                    oldContainer: void 0,
                    newContainer: void 0,
                    newContainerLoading: void 0,
                    extend: function (e) {
                        return n.extend(this, e)
                    },
                    init: function (e, t) {
                        var i = this;
                        return this.oldContainer = e, this._newContainerPromise = t, this.deferred = n.deferred(), this.newContainerReady = n.deferred(), this.newContainerLoading = this.newContainerReady.promise, this.start(), this._newContainerPromise.then(function (e) {
                            i.newContainer = e, i.newContainerReady.resolve()
                        }), this.deferred.promise
                    },
                    done: function () {
                        this.oldContainer.parentNode.removeChild(this.oldContainer), this.newContainer.style.visibility = "visible", this.deferred.resolve()
                    },
                    start: function () {}
                };
            e.exports = s
        }, function (e, t) {
            var i = {
                getCurrentUrl: function () {
                    return window.location.protocol + "//" + window.location.host + window.location.pathname + window.location.search
                },
                cleanLink: function (e) {
                    return e.replace(/#.*/, "")
                },
                xhrTimeout: 5e3,
                xhr: function (e) {
                    var t = this.deferred(),
                        i = new XMLHttpRequest;
                    return i.onreadystatechange = function () {
                        if (4 === i.readyState) return 200 === i.status ? t.resolve(i.responseText) : t.reject(new Error("xhr: HTTP code is not 200"))
                    }, i.ontimeout = function () {
                        return t.reject(new Error("xhr: Timeout exceeded"))
                    }, i.open("GET", e), i.timeout = this.xhrTimeout, i.setRequestHeader("x-barba", "yes"), i.send(), t.promise
                },
                extend: function (e, t) {
                    var i = Object.create(e);
                    for (var n in t) t.hasOwnProperty(n) && (i[n] = t[n]);
                    return i
                },
                deferred: function () {
                    return new function () {
                        this.resolve = null, this.reject = null, this.promise = new Promise(function (e, t) {
                            this.resolve = e, this.reject = t
                        }.bind(this))
                    }
                },
                getPort: function (e) {
                    var t = void 0 !== e ? e : window.location.port,
                        i = window.location.protocol;
                    return "" != t ? parseInt(t) : "http:" === i ? 80 : "https:" === i ? 443 : void 0
                }
            };
            e.exports = i
        }, function (e, t, i) {
            var n = i(7),
                s = i(5),
                o = {
                    namespace: null,
                    extend: function (e) {
                        return s.extend(this, e)
                    },
                    init: function () {
                        var e = this;
                        n.on("initStateChange", function (t, i) {
                            i && i.namespace === e.namespace && e.onLeave()
                        }), n.on("newPageReady", function (t, i, n) {
                            e.container = n, t.namespace === e.namespace && e.onEnter()
                        }), n.on("transitionCompleted", function (t, i) {
                            t.namespace === e.namespace && e.onEnterCompleted(), i && i.namespace === e.namespace && e.onLeaveCompleted()
                        })
                    },
                    onEnter: function () {},
                    onEnterCompleted: function () {},
                    onLeave: function () {},
                    onLeaveCompleted: function () {}
                };
            e.exports = o
        }, function (e, t) {
            var i = {
                events: {},
                on: function (e, t) {
                    this.events[e] = this.events[e] || [], this.events[e].push(t)
                },
                off: function (e, t) {
                    e in this.events != 0 && this.events[e].splice(this.events[e].indexOf(t), 1)
                },
                trigger: function (e) {
                    if (e in this.events != 0)
                        for (var t = 0; t < this.events[e].length; t++) this.events[e][t].apply(this, Array.prototype.slice.call(arguments, 1))
                }
            };
            e.exports = i
        }, function (e, t, i) {
            var n = i(5),
                s = {
                    data: {},
                    extend: function (e) {
                        return n.extend(this, e)
                    },
                    set: function (e, t) {
                        this.data[e] = t
                    },
                    get: function (e) {
                        return this.data[e]
                    },
                    reset: function () {
                        this.data = {}
                    }
                };
            e.exports = s
        }, function (e, t) {
            var i = {
                history: [],
                add: function (e, t) {
                    t || (t = void 0), this.history.push({
                        url: e,
                        namespace: t
                    })
                },
                currentStatus: function () {
                    return this.history[this.history.length - 1]
                },
                prevStatus: function () {
                    var e = this.history;
                    return e.length < 2 ? null : e[e.length - 2]
                }
            };
            e.exports = i
        }, function (e, t, i) {
            var n = i(5),
                s = i(7),
                o = i(11),
                a = i(8),
                r = i(9),
                l = i(12),
                d = {
                    Dom: l,
                    History: r,
                    Cache: a,
                    cacheEnabled: !0,
                    transitionProgress: !1,
                    ignoreClassLink: "no-barba",
                    start: function () {
                        this.init()
                    },
                    init: function () {
                        var e = this.Dom.getContainer();
                        this.Dom.getWrapper().setAttribute("aria-live", "polite"), this.History.add(this.getCurrentUrl(), this.Dom.getNamespace(e)), s.trigger("initStateChange", this.History.currentStatus()), s.trigger("newPageReady", this.History.currentStatus(), {}, e, this.Dom.currentHTML), s.trigger("transitionCompleted", this.History.currentStatus()), this.bindEvents()
                    },
                    bindEvents: function () {
                        document.addEventListener("click", this.onLinkClick.bind(this)), window.addEventListener("popstate", this.onStateChange.bind(this))
                    },
                    getCurrentUrl: function () {
                        return n.cleanLink(n.getCurrentUrl())
                    },
                    goTo: function (e) {
                        window.history.pushState(null, null, e), this.onStateChange()
                    },
                    forceGoTo: function (e) {
                        window.location = e
                    },
                    load: function (e) {
                        var t, i = n.deferred(),
                            s = this;
                        return t = this.Cache.get(e), t || (t = n.xhr(e), this.Cache.set(e, t)), t.then(function (e) {
                            var t = s.Dom.parseResponse(e);
                            s.Dom.putContainer(t), s.cacheEnabled || s.Cache.reset(), i.resolve(t)
                        }, function () {
                            s.forceGoTo(e), i.reject()
                        }), i.promise
                    },
                    getHref: function (e) {
                        if (e) return e.getAttribute && "string" == typeof e.getAttribute("xlink:href") ? e.getAttribute("xlink:href") : "string" == typeof e.href ? e.href : void 0
                    },
                    onLinkClick: function (e) {
                        for (var t = e.target; t && !this.getHref(t);) t = t.parentNode;
                        if (this.preventCheck(e, t)) {
                            e.stopPropagation(), e.preventDefault(), s.trigger("linkClicked", t, e);
                            var i = this.getHref(t);
                            this.goTo(i)
                        }
                    },
                    preventCheck: function (e, t) {
                        if (!window.history.pushState) return !1;
                        var i = this.getHref(t);
                        return !(!t || !i || e.which > 1 || e.metaKey || e.ctrlKey || e.shiftKey || e.altKey || t.target && "_blank" === t.target || window.location.protocol !== t.protocol || window.location.hostname !== t.hostname || n.getPort() !== n.getPort(t.port) || i.indexOf("#") > -1 || t.getAttribute && "string" == typeof t.getAttribute("download") || n.cleanLink(i) == n.cleanLink(location.href) || t.classList.contains(this.ignoreClassLink))
                    },
                    getTransition: function () {
                        return o
                    },
                    onStateChange: function () {
                        var e = this.getCurrentUrl();
                        if (this.transitionProgress && this.forceGoTo(e), this.History.currentStatus().url === e) return !1;
                        this.History.add(e);
                        var t = this.load(e),
                            i = Object.create(this.getTransition());
                        this.transitionProgress = !0, s.trigger("initStateChange", this.History.currentStatus(), this.History.prevStatus());
                        var n = i.init(this.Dom.getContainer(), t);
                        t.then(this.onNewContainerLoaded.bind(this)), n.then(this.onTransitionEnd.bind(this))
                    },
                    onNewContainerLoaded: function (e) {
                        this.History.currentStatus().namespace = this.Dom.getNamespace(e), s.trigger("newPageReady", this.History.currentStatus(), this.History.prevStatus(), e, this.Dom.currentHTML)
                    },
                    onTransitionEnd: function () {
                        this.transitionProgress = !1, s.trigger("transitionCompleted", this.History.currentStatus(), this.History.prevStatus())
                    }
                };
            e.exports = d
        }, function (e, t, i) {
            var n = i(4),
                s = n.extend({
                    start: function () {
                        this.newContainerLoading.then(this.finish.bind(this))
                    },
                    finish: function () {
                        document.body.scrollTop = 0, this.done()
                    }
                });
            e.exports = s
        }, function (e, t) {
            var i = {
                dataNamespace: "namespace",
                wrapperId: "barba-wrapper",
                containerClass: "barba-container",
                currentHTML: document.documentElement.innerHTML,
                parseResponse: function (e) {
                    this.currentHTML = e;
                    var t = document.createElement("div");
                    t.innerHTML = e;
                    var i = t.querySelector("title");
                    return i && (document.title = i.textContent), this.getContainer(t)
                },
                getWrapper: function () {
                    var e = document.getElementById(this.wrapperId);
                    if (!e) throw new Error("Barba.js: wrapper not found!");
                    return e
                },
                getContainer: function (e) {
                    if (e || (e = document.body), !e) throw new Error("Barba.js: DOM not ready!");
                    var t = this.parseContainer(e);
                    if (t && t.jquery && (t = t[0]), !t) throw new Error("Barba.js: no container found");
                    return t
                },
                getNamespace: function (e) {
                    return e && e.dataset ? e.dataset[this.dataNamespace] : e ? e.getAttribute("data-" + this.dataNamespace) : null
                },
                putContainer: function (e) {
                    e.style.visibility = "hidden", this.getWrapper().appendChild(e)
                },
                parseContainer: function (e) {
                    return e.querySelector("." + this.containerClass)
                }
            };
            e.exports = i
        }, function (e, t, i) {
            var n = i(5),
                s = i(10),
                o = {
                    ignoreClassLink: "no-barba-prefetch",
                    init: function () {
                        return !!window.history.pushState && (document.body.addEventListener("mouseover", this.onLinkEnter.bind(this)), void document.body.addEventListener("touchstart", this.onLinkEnter.bind(this)))
                    },
                    onLinkEnter: function (e) {
                        for (var t = e.target; t && !s.getHref(t);) t = t.parentNode;
                        if (t && !t.classList.contains(this.ignoreClassLink)) {
                            var i = s.getHref(t);
                            if (s.preventCheck(e, t) && !s.Cache.get(i)) {
                                var o = n.xhr(i);
                                s.Cache.set(i, o)
                            }
                        }
                    }
                };
            e.exports = o
        }])
    }),
    function () {
        "use strict";

        function e(t, n) {
            var s;
            if (n = n || {}, this.trackingClick = !1, this.trackingClickStart = 0, this.targetElement = null, this.touchStartX = 0, this.touchStartY = 0, this.lastTouchIdentifier = 0, this.touchBoundary = n.touchBoundary || 10, this.layer = t, this.tapDelay = n.tapDelay || 200, this.tapTimeout = n.tapTimeout || 700, !e.notNeeded(t)) {
                for (var o = ["onMouse", "onClick", "onTouchStart", "onTouchMove", "onTouchEnd", "onTouchCancel"], a = this, r = 0, l = o.length; r < l; r++) a[o[r]] = function (e, t) {
                    return function () {
                        return e.apply(t, arguments)
                    }
                }(a[o[r]], a);
                i && (t.addEventListener("mouseover", this.onMouse, !0), t.addEventListener("mousedown", this.onMouse, !0), t.addEventListener("mouseup", this.onMouse, !0)), t.addEventListener("click", this.onClick, !0), t.addEventListener("touchstart", this.onTouchStart, !1), t.addEventListener("touchmove", this.onTouchMove, !1), t.addEventListener("touchend", this.onTouchEnd, !1), t.addEventListener("touchcancel", this.onTouchCancel, !1), Event.prototype.stopImmediatePropagation || (t.removeEventListener = function (e, i, n) {
                    var s = Node.prototype.removeEventListener;
                    "click" === e ? s.call(t, e, i.hijacked || i, n) : s.call(t, e, i, n)
                }, t.addEventListener = function (e, i, n) {
                    var s = Node.prototype.addEventListener;
                    "click" === e ? s.call(t, e, i.hijacked || (i.hijacked = function (e) {
                        e.propagationStopped || i(e)
                    }), n) : s.call(t, e, i, n)
                }), "function" == typeof t.onclick && (s = t.onclick, t.addEventListener("click", function (e) {
                    s(e)
                }, !1), t.onclick = null)
            }
        }
        var t = navigator.userAgent.indexOf("Windows Phone") >= 0,
            i = navigator.userAgent.indexOf("Android") > 0 && !t,
            n = /iP(ad|hone|od)/.test(navigator.userAgent) && !t,
            s = n && /OS 4_\d(_\d)?/.test(navigator.userAgent),
            o = n && /OS [6-7]_\d/.test(navigator.userAgent),
            a = navigator.userAgent.indexOf("BB10") > 0;
        e.prototype.needsClick = function (e) {
            switch (e.nodeName.toLowerCase()) {
                case "button":
                case "select":
                case "textarea":
                    if (e.disabled) return !0;
                    break;
                case "input":
                    if (n && "file" === e.type || e.disabled) return !0;
                    break;
                case "label":
                case "iframe":
                case "video":
                    return !0
            }
            return /\bneedsclick\b/.test(e.className)
        }, e.prototype.needsFocus = function (e) {
            switch (e.nodeName.toLowerCase()) {
                case "textarea":
                    return !0;
                case "select":
                    return !i;
                case "input":
                    switch (e.type) {
                        case "button":
                        case "checkbox":
                        case "file":
                        case "image":
                        case "radio":
                        case "submit":
                            return !1
                    }
                    return !e.disabled && !e.readOnly;
                default:
                    return /\bneedsfocus\b/.test(e.className)
            }
        }, e.prototype.sendClick = function (e, t) {
            var i, n;
            document.activeElement && document.activeElement !== e && document.activeElement.blur(), n = t.changedTouches[0], i = document.createEvent("MouseEvents"), i.initMouseEvent(this.determineEventType(e), !0, !0, window, 1, n.screenX, n.screenY, n.clientX, n.clientY, !1, !1, !1, !1, 0, null), i.forwardedTouchEvent = !0, e.dispatchEvent(i)
        }, e.prototype.determineEventType = function (e) {
            return i && "select" === e.tagName.toLowerCase() ? "mousedown" : "click"
        }, e.prototype.focus = function (e) {
            var t;
            n && e.setSelectionRange && 0 !== e.type.indexOf("date") && "time" !== e.type && "month" !== e.type && "email" !== e.type ? (t = e.value.length, e.setSelectionRange(t, t)) : e.focus()
        }, e.prototype.updateScrollParent = function (e) {
            var t, i;
            if (!(t = e.fastClickScrollParent) || !t.contains(e)) {
                i = e;
                do {
                    if (i.scrollHeight > i.offsetHeight) {
                        t = i, e.fastClickScrollParent = i;
                        break
                    }
                    i = i.parentElement
                } while (i)
            }
            t && (t.fastClickLastScrollTop = t.scrollTop)
        }, e.prototype.getTargetElementFromEventTarget = function (e) {
            return e.nodeType === Node.TEXT_NODE ? e.parentNode : e
        }, e.prototype.onTouchStart = function (e) {
            var t, i, o;
            if (e.targetTouches.length > 1) return !0;
            if (t = this.getTargetElementFromEventTarget(e.target), i = e.targetTouches[0], n) {
                if (o = window.getSelection(), o.rangeCount && !o.isCollapsed) return !0;
                if (!s) {
                    if (i.identifier && i.identifier === this.lastTouchIdentifier) return e.preventDefault(), !1;
                    this.lastTouchIdentifier = i.identifier, this.updateScrollParent(t)
                }
            }
            return this.trackingClick = !0, this.trackingClickStart = e.timeStamp, this.targetElement = t, this.touchStartX = i.pageX, this.touchStartY = i.pageY, e.timeStamp - this.lastClickTime < this.tapDelay && e.preventDefault(), !0
        }, e.prototype.touchHasMoved = function (e) {
            var t = e.changedTouches[0],
                i = this.touchBoundary;
            return Math.abs(t.pageX - this.touchStartX) > i || Math.abs(t.pageY - this.touchStartY) > i
        }, e.prototype.onTouchMove = function (e) {
            return !this.trackingClick || ((this.targetElement !== this.getTargetElementFromEventTarget(e.target) || this.touchHasMoved(e)) && (this.trackingClick = !1, this.targetElement = null), !0)
        }, e.prototype.findControl = function (e) {
            return void 0 !== e.control ? e.control : e.htmlFor ? document.getElementById(e.htmlFor) : e.querySelector("button, input:not([type=hidden]), keygen, meter, output, progress, select, textarea")
        }, e.prototype.onTouchEnd = function (e) {
            var t, a, r, l, d, c = this.targetElement;
            if (!this.trackingClick) return !0;
            if (e.timeStamp - this.lastClickTime < this.tapDelay) return this.cancelNextClick = !0, !0;
            if (e.timeStamp - this.trackingClickStart > this.tapTimeout) return !0;
            if (this.cancelNextClick = !1, this.lastClickTime = e.timeStamp, a = this.trackingClickStart, this.trackingClick = !1, this.trackingClickStart = 0, o && (d = e.changedTouches[0], c = document.elementFromPoint(d.pageX - window.pageXOffset, d.pageY - window.pageYOffset) || c, c.fastClickScrollParent = this.targetElement.fastClickScrollParent), "label" === (r = c.tagName.toLowerCase())) {
                if (t = this.findControl(c)) {
                    if (this.focus(c), i) return !1;
                    c = t
                }
            } else if (this.needsFocus(c)) return e.timeStamp - a > 100 || n && window.top !== window && "input" === r ? (this.targetElement = null, !1) : (this.focus(c), this.sendClick(c, e), n && "select" === r || (this.targetElement = null, e.preventDefault()), !1);
            return !(!n || s || !(l = c.fastClickScrollParent) || l.fastClickLastScrollTop === l.scrollTop) || (this.needsClick(c) || (e.preventDefault(), this.sendClick(c, e)), !1)
        }, e.prototype.onTouchCancel = function () {
            this.trackingClick = !1, this.targetElement = null
        }, e.prototype.onMouse = function (e) {
            return !this.targetElement || (!!e.forwardedTouchEvent || (!e.cancelable || (!(!this.needsClick(this.targetElement) || this.cancelNextClick) || (e.stopImmediatePropagation ? e.stopImmediatePropagation() : e.propagationStopped = !0, e.stopPropagation(), e.preventDefault(), !1))))
        }, e.prototype.onClick = function (e) {
            var t;
            return this.trackingClick ? (this.targetElement = null, this.trackingClick = !1, !0) : "submit" === e.target.type && 0 === e.detail || (t = this.onMouse(e), t || (this.targetElement = null), t)
        }, e.prototype.destroy = function () {
            var e = this.layer;
            i && (e.removeEventListener("mouseover", this.onMouse, !0), e.removeEventListener("mousedown", this.onMouse, !0), e.removeEventListener("mouseup", this.onMouse, !0)), e.removeEventListener("click", this.onClick, !0), e.removeEventListener("touchstart", this.onTouchStart, !1), e.removeEventListener("touchmove", this.onTouchMove, !1), e.removeEventListener("touchend", this.onTouchEnd, !1), e.removeEventListener("touchcancel", this.onTouchCancel, !1)
        }, e.notNeeded = function (e) {
            var t, n, s;
            if (void 0 === window.ontouchstart) return !0;
            if (n = +(/Chrome\/([0-9]+)/.exec(navigator.userAgent) || [, 0])[1]) {
                if (!i) return !0;
                if (t = document.querySelector("meta[name=viewport]")) {
                    if (-1 !== t.content.indexOf("user-scalable=no")) return !0;
                    if (n > 31 && document.documentElement.scrollWidth <= window.outerWidth) return !0
                }
            }
            if (a && (s = navigator.userAgent.match(/Version\/([0-9]*)\.([0-9]*)/), s[1] >= 10 && s[2] >= 3 && (t = document.querySelector("meta[name=viewport]")))) {
                if (-1 !== t.content.indexOf("user-scalable=no")) return !0;
                if (document.documentElement.scrollWidth <= window.outerWidth) return !0
            }
            return "none" === e.style.msTouchAction || "manipulation" === e.style.touchAction || (!!(+(/Firefox\/([0-9]+)/.exec(navigator.userAgent) || [, 0])[1] >= 27 && (t = document.querySelector("meta[name=viewport]")) && (-1 !== t.content.indexOf("user-scalable=no") || document.documentElement.scrollWidth <= window.outerWidth)) || ("none" === e.style.touchAction || "manipulation" === e.style.touchAction))
        }, e.attach = function (t, i) {
            return new e(t, i)
        }, "function" == typeof define && "object" == typeof define.amd && define.amd ? define(function () {
            return e
        }) : "undefined" != typeof module && module.exports ? (module.exports = e.attach, module.exports.FastClick = e) : window.FastClick = e
    }(),
    function (e) {
        "object" == typeof exports && "undefined" != typeof module ? module.exports = e() : "function" == typeof define && define.amd ? define([], e) : ("undefined" != typeof window ? window : "undefined" != typeof global ? global : "undefined" != typeof self ? self : this).Parallax = e()
    }(function () {
        return function e(t, i, n) {
            function s(a, r) {
                if (!i[a]) {
                    if (!t[a]) {
                        var l = "function" == typeof require && require;
                        if (!r && l) return l(a, !0);
                        if (o) return o(a, !0);
                        var d = new Error("Cannot find module '" + a + "'");
                        throw d.code = "MODULE_NOT_FOUND", d
                    }
                    var c = i[a] = {
                        exports: {}
                    };
                    t[a][0].call(c.exports, function (e) {
                        return s(t[a][1][e] || e)
                    }, c, c.exports, e, t, i, n)
                }
                return i[a].exports
            }
            for (var o = "function" == typeof require && require, a = 0; a < n.length; a++) s(n[a]);
            return s
        }({
            1: [function (e, t, i) {
                "use strict";

                function n(e) {
                    if (null === e || void 0 === e) throw new TypeError("Object.assign cannot be called with null or undefined");
                    return Object(e)
                }
                var s = Object.getOwnPropertySymbols,
                    o = Object.prototype.hasOwnProperty,
                    a = Object.prototype.propertyIsEnumerable;
                t.exports = function () {
                    try {
                        if (!Object.assign) return !1;
                        var e = new String("abc");
                        if (e[5] = "de", "5" === Object.getOwnPropertyNames(e)[0]) return !1;
                        for (var t = {}, i = 0; i < 10; i++) t["_" + String.fromCharCode(i)] = i;
                        if ("0123456789" !== Object.getOwnPropertyNames(t).map(function (e) {
                                return t[e]
                            }).join("")) return !1;
                        var n = {};
                        return "abcdefghijklmnopqrst".split("").forEach(function (e) {
                            n[e] = e
                        }), "abcdefghijklmnopqrst" === Object.keys(Object.assign({}, n)).join("")
                    } catch (e) {
                        return !1
                    }
                }() ? Object.assign : function (e, t) {
                    for (var i, r, l = n(e), d = 1; d < arguments.length; d++) {
                        i = Object(arguments[d]);
                        for (var c in i) o.call(i, c) && (l[c] = i[c]);
                        if (s) {
                            r = s(i);
                            for (var p = 0; p < r.length; p++) a.call(i, r[p]) && (l[r[p]] = i[r[p]])
                        }
                    }
                    return l
                }
            }, {}],
            2: [function (e, t, i) {
                (function (e) {
                    (function () {
                        var i, n, s, o, a, r;
                        "undefined" != typeof performance && null !== performance && performance.now ? t.exports = function () {
                            return performance.now()
                        } : void 0 !== e && null !== e && e.hrtime ? (t.exports = function () {
                            return (i() - a) / 1e6
                        }, n = e.hrtime, o = (i = function () {
                            var e;
                            return 1e9 * (e = n())[0] + e[1]
                        })(), r = 1e9 * e.uptime(), a = o - r) : Date.now ? (t.exports = function () {
                            return Date.now() - s
                        }, s = Date.now()) : (t.exports = function () {
                            return (new Date).getTime() - s
                        }, s = (new Date).getTime())
                    }).call(this)
                }).call(this, e("_process"))
            }, {
                _process: 3
            }],
            3: [function (e, t, i) {
                function n() {
                    throw new Error("setTimeout has not been defined")
                }

                function s() {
                    throw new Error("clearTimeout has not been defined")
                }

                function o(e) {
                    if (p === setTimeout) return setTimeout(e, 0);
                    if ((p === n || !p) && setTimeout) return p = setTimeout, setTimeout(e, 0);
                    try {
                        return p(e, 0)
                    } catch (t) {
                        try {
                            return p.call(null, e, 0)
                        } catch (t) {
                            return p.call(this, e, 0)
                        }
                    }
                }

                function a(e) {
                    if (u === clearTimeout) return clearTimeout(e);
                    if ((u === s || !u) && clearTimeout) return u = clearTimeout, clearTimeout(e);
                    try {
                        return u(e)
                    } catch (t) {
                        try {
                            return u.call(null, e)
                        } catch (t) {
                            return u.call(this, e)
                        }
                    }
                }

                function r() {
                    m && f && (m = !1, f.length ? v = f.concat(v) : g = -1, v.length && l())
                }

                function l() {
                    if (!m) {
                        var e = o(r);
                        m = !0;
                        for (var t = v.length; t;) {
                            for (f = v, v = []; ++g < t;) f && f[g].run();
                            g = -1, t = v.length
                        }
                        f = null, m = !1, a(e)
                    }
                }

                function d(e, t) {
                    this.fun = e, this.array = t
                }

                function c() {}
                var p, u, h = t.exports = {};
                ! function () {
                    try {
                        p = "function" == typeof setTimeout ? setTimeout : n
                    } catch (e) {
                        p = n
                    }
                    try {
                        u = "function" == typeof clearTimeout ? clearTimeout : s
                    } catch (e) {
                        u = s
                    }
                }();
                var f, v = [],
                    m = !1,
                    g = -1;
                h.nextTick = function (e) {
                    var t = new Array(arguments.length - 1);
                    if (arguments.length > 1)
                        for (var i = 1; i < arguments.length; i++) t[i - 1] = arguments[i];
                    v.push(new d(e, t)), 1 !== v.length || m || o(l)
                }, d.prototype.run = function () {
                    this.fun.apply(null, this.array)
                }, h.title = "browser", h.browser = !0, h.env = {}, h.argv = [], h.version = "", h.versions = {}, h.on = c, h.addListener = c, h.once = c, h.off = c, h.removeListener = c, h.removeAllListeners = c, h.emit = c, h.prependListener = c, h.prependOnceListener = c, h.listeners = function (e) {
                    return []
                }, h.binding = function (e) {
                    throw new Error("process.binding is not supported")
                }, h.cwd = function () {
                    return "/"
                }, h.chdir = function (e) {
                    throw new Error("process.chdir is not supported")
                }, h.umask = function () {
                    return 0
                }
            }, {}],
            4: [function (e, t, i) {
                (function (i) {
                    for (var n = e("performance-now"), s = "undefined" == typeof window ? i : window, o = ["moz", "webkit"], a = "AnimationFrame", r = s["request" + a], l = s["cancel" + a] || s["cancelRequest" + a], d = 0; !r && d < o.length; d++) r = s[o[d] + "Request" + a], l = s[o[d] + "Cancel" + a] || s[o[d] + "CancelRequest" + a];
                    if (!r || !l) {
                        var c = 0,
                            p = 0,
                            u = [];
                        r = function (e) {
                            if (0 === u.length) {
                                var t = n(),
                                    i = Math.max(0, 1e3 / 60 - (t - c));
                                c = i + t, setTimeout(function () {
                                    var e = u.slice(0);
                                    u.length = 0;
                                    for (var t = 0; t < e.length; t++)
                                        if (!e[t].cancelled) try {
                                            e[t].callback(c)
                                        } catch (e) {
                                            setTimeout(function () {
                                                throw e
                                            }, 0)
                                        }
                                }, Math.round(i))
                            }
                            return u.push({
                                handle: ++p,
                                callback: e,
                                cancelled: !1
                            }), p
                        }, l = function (e) {
                            for (var t = 0; t < u.length; t++) u[t].handle === e && (u[t].cancelled = !0)
                        }
                    }
                    t.exports = function (e) {
                        return r.call(s, e)
                    }, t.exports.cancel = function () {
                        l.apply(s, arguments)
                    }, t.exports.polyfill = function () {
                        s.requestAnimationFrame = r, s.cancelAnimationFrame = l
                    }
                }).call(this, "undefined" != typeof global ? global : "undefined" != typeof self ? self : "undefined" != typeof window ? window : {})
            }, {
                "performance-now": 2
            }],
            5: [function (e, t, i) {
                "use strict";

                function n(e, t) {
                    if (!(e instanceof t)) throw new TypeError("Cannot call a class as a function")
                }
                var s = function () {
                        function e(e, t) {
                            for (var i = 0; i < t.length; i++) {
                                var n = t[i];
                                n.enumerable = n.enumerable || !1, n.configurable = !0, "value" in n && (n.writable = !0), Object.defineProperty(e, n.key, n)
                            }
                        }
                        return function (t, i, n) {
                            return i && e(t.prototype, i), n && e(t, n), t
                        }
                    }(),
                    o = e("raf"),
                    a = e("object-assign"),
                    r = {
                        propertyCache: {},
                        vendors: [null, ["-webkit-", "webkit"],
                            ["-moz-", "Moz"],
                            ["-o-", "O"],
                            ["-ms-", "ms"]
                        ],
                        clamp: function (e, t, i) {
                            return t < i ? e < t ? t : e > i ? i : e : e < i ? i : e > t ? t : e
                        },
                        data: function (e, t) {
                            return r.deserialize(e.getAttribute("data-" + t))
                        },
                        deserialize: function (e) {
                            return "true" === e || "false" !== e && ("null" === e ? null : !isNaN(parseFloat(e)) && isFinite(e) ? parseFloat(e) : e)
                        },
                        camelCase: function (e) {
                            return e.replace(/-+(.)?/g, function (e, t) {
                                return t ? t.toUpperCase() : ""
                            })
                        },
                        accelerate: function (e) {
                            r.css(e, "transform", "translate3d(0,0,0) rotate(0.0001deg)"), r.css(e, "transform-style", "preserve-3d"), r.css(e, "backface-visibility", "hidden")
                        },
                        transformSupport: function (e) {
                            for (var t = document.createElement("div"), i = !1, n = null, s = !1, o = null, a = null, l = 0, d = r.vendors.length; l < d; l++)
                                if (null !== r.vendors[l] ? (o = r.vendors[l][0] + "transform", a = r.vendors[l][1] + "Transform") : (o = "transform", a = "transform"), void 0 !== t.style[a]) {
                                    i = !0;
                                    break
                                } switch (e) {
                                case "2D":
                                    s = i;
                                    break;
                                case "3D":
                                    if (i) {
                                        var c = document.body || document.createElement("body"),
                                            p = document.documentElement,
                                            u = p.style.overflow,
                                            h = !1;
                                        document.body || (h = !0, p.style.overflow = "hidden", p.appendChild(c), c.style.overflow = "hidden", c.style.background = ""), c.appendChild(t), t.style[a] = "translate3d(1px,1px,1px)", s = void 0 !== (n = window.getComputedStyle(t).getPropertyValue(o)) && n.length > 0 && "none" !== n, p.style.overflow = u, c.removeChild(t), h && (c.removeAttribute("style"), c.parentNode.removeChild(c))
                                    }
                            }
                            return s
                        },
                        css: function (e, t, i) {
                            var n = r.propertyCache[t];
                            if (!n)
                                for (var s = 0, o = r.vendors.length; s < o; s++)
                                    if (n = null !== r.vendors[s] ? r.camelCase(r.vendors[s][1] + "-" + t) : t, void 0 !== e.style[n]) {
                                        r.propertyCache[t] = n;
                                        break
                                    } e.style[n] = i
                        }
                    },
                    l = {
                        relativeInput: !1,
                        clipRelativeInput: !1,
                        inputElement: null,
                        hoverOnly: !1,
                        calibrationThreshold: 100,
                        calibrationDelay: 500,
                        supportDelay: 500,
                        calibrateX: !1,
                        calibrateY: !0,
                        invertX: !0,
                        invertY: !0,
                        limitX: !1,
                        limitY: !1,
                        scalarX: 10,
                        scalarY: 10,
                        frictionX: .1,
                        frictionY: .1,
                        originX: .5,
                        originY: .5,
                        pointerEvents: !1,
                        precision: 1,
                        onReady: null,
                        selector: null
                    },
                    d = function () {
                        function e(t, i) {
                            n(this, e), this.element = t;
                            var s = {
                                calibrateX: r.data(this.element, "calibrate-x"),
                                calibrateY: r.data(this.element, "calibrate-y"),
                                invertX: r.data(this.element, "invert-x"),
                                invertY: r.data(this.element, "invert-y"),
                                limitX: r.data(this.element, "limit-x"),
                                limitY: r.data(this.element, "limit-y"),
                                scalarX: r.data(this.element, "scalar-x"),
                                scalarY: r.data(this.element, "scalar-y"),
                                frictionX: r.data(this.element, "friction-x"),
                                frictionY: r.data(this.element, "friction-y"),
                                originX: r.data(this.element, "origin-x"),
                                originY: r.data(this.element, "origin-y"),
                                pointerEvents: r.data(this.element, "pointer-events"),
                                precision: r.data(this.element, "precision"),
                                relativeInput: r.data(this.element, "relative-input"),
                                clipRelativeInput: r.data(this.element, "clip-relative-input"),
                                hoverOnly: r.data(this.element, "hover-only"),
                                inputElement: document.querySelector(r.data(this.element, "input-element")),
                                selector: r.data(this.element, "selector")
                            };
                            for (var o in s) null === s[o] && delete s[o];
                            a(this, l, s, i), this.inputElement || (this.inputElement = this.element), this.calibrationTimer = null, this.calibrationFlag = !0, this.enabled = !1, this.depthsX = [], this.depthsY = [], this.raf = null, this.bounds = null, this.elementPositionX = 0, this.elementPositionY = 0, this.elementWidth = 0, this.elementHeight = 0, this.elementCenterX = 0, this.elementCenterY = 0, this.elementRangeX = 0, this.elementRangeY = 0, this.calibrationX = 0, this.calibrationY = 0, this.inputX = 0, this.inputY = 0, this.motionX = 0, this.motionY = 0, this.velocityX = 0, this.velocityY = 0, this.onMouseMove = this.onMouseMove.bind(this), this.onDeviceOrientation = this.onDeviceOrientation.bind(this), this.onDeviceMotion = this.onDeviceMotion.bind(this), this.onOrientationTimer = this.onOrientationTimer.bind(this), this.onMotionTimer = this.onMotionTimer.bind(this), this.onCalibrationTimer = this.onCalibrationTimer.bind(this), this.onAnimationFrame = this.onAnimationFrame.bind(this), this.onWindowResize = this.onWindowResize.bind(this), this.windowWidth = null, this.windowHeight = null, this.windowCenterX = null, this.windowCenterY = null, this.windowRadiusX = null, this.windowRadiusY = null, this.portrait = !1, this.desktop = !navigator.userAgent.match(/(iPhone|iPod|iPad|Android|BlackBerry|BB10|mobi|tablet|opera mini|nexus 7)/i), this.motionSupport = !!window.DeviceMotionEvent && !this.desktop, this.orientationSupport = !!window.DeviceOrientationEvent && !this.desktop, this.orientationStatus = 0, this.motionStatus = 0, this.initialise()
                        }
                        return s(e, [{
                            key: "initialise",
                            value: function () {
                                void 0 === this.transform2DSupport && (this.transform2DSupport = r.transformSupport("2D"), this.transform3DSupport = r.transformSupport("3D")), this.transform3DSupport && r.accelerate(this.element), "static" === window.getComputedStyle(this.element).getPropertyValue("position") && (this.element.style.position = "relative"), this.pointerEvents || (this.element.style.pointerEvents = "none"), this.updateLayers(), this.updateDimensions(), this.enable(), this.queueCalibration(this.calibrationDelay)
                            }
                        }, {
                            key: "doReadyCallback",
                            value: function () {
                                this.onReady && this.onReady()
                            }
                        }, {
                            key: "updateLayers",
                            value: function () {
                                this.selector ? this.layers = this.element.querySelectorAll(this.selector) : this.layers = this.element.children, this.layers.length || console.warn("ParallaxJS: Your scene does not have any layers."), this.depthsX = [], this.depthsY = [];
                                for (var e = 0; e < this.layers.length; e++) {
                                    var t = this.layers[e];
                                    this.transform3DSupport && r.accelerate(t), t.style.position = e ? "absolute" : "relative", t.style.display = "block", t.style.left = 0, t.style.top = 0;
                                    var i = r.data(t, "depth") || 0;
                                    this.depthsX.push(r.data(t, "depth-x") || i), this.depthsY.push(r.data(t, "depth-y") || i)
                                }
                            }
                        }, {
                            key: "updateDimensions",
                            value: function () {
                                this.windowWidth = window.innerWidth, this.windowHeight = window.innerHeight, this.windowCenterX = this.windowWidth * this.originX, this.windowCenterY = this.windowHeight * this.originY, this.windowRadiusX = Math.max(this.windowCenterX, this.windowWidth - this.windowCenterX), this.windowRadiusY = Math.max(this.windowCenterY, this.windowHeight - this.windowCenterY)
                            }
                        }, {
                            key: "updateBounds",
                            value: function () {
                                this.bounds = this.inputElement.getBoundingClientRect(), this.elementPositionX = this.bounds.left, this.elementPositionY = this.bounds.top, this.elementWidth = this.bounds.width, this.elementHeight = this.bounds.height, this.elementCenterX = this.elementWidth * this.originX, this.elementCenterY = this.elementHeight * this.originY, this.elementRangeX = Math.max(this.elementCenterX, this.elementWidth - this.elementCenterX), this.elementRangeY = Math.max(this.elementCenterY, this.elementHeight - this.elementCenterY)
                            }
                        }, {
                            key: "queueCalibration",
                            value: function (e) {
                                clearTimeout(this.calibrationTimer), this.calibrationTimer = setTimeout(this.onCalibrationTimer, e)
                            }
                        }, {
                            key: "enable",
                            value: function () {
                                this.enabled || (this.enabled = !0, this.orientationSupport ? (this.portrait = !1, window.addEventListener("deviceorientation", this.onDeviceOrientation), this.detectionTimer = setTimeout(this.onOrientationTimer, this.supportDelay)) : this.motionSupport ? (this.portrait = !1, window.addEventListener("devicemotion", this.onDeviceMotion), this.detectionTimer = setTimeout(this.onMotionTimer, this.supportDelay)) : (this.calibrationX = 0, this.calibrationY = 0, this.portrait = !1, window.addEventListener("mousemove", this.onMouseMove), this.doReadyCallback()), window.addEventListener("resize", this.onWindowResize), this.raf = o(this.onAnimationFrame))
                            }
                        }, {
                            key: "disable",
                            value: function () {
                                this.enabled && (this.enabled = !1, this.orientationSupport ? window.removeEventListener("deviceorientation", this.onDeviceOrientation) : this.motionSupport ? window.removeEventListener("devicemotion", this.onDeviceMotion) : window.removeEventListener("mousemove", this.onMouseMove), window.removeEventListener("resize", this.onWindowResize), o.cancel(this.raf))
                            }
                        }, {
                            key: "calibrate",
                            value: function (e, t) {
                                this.calibrateX = void 0 === e ? this.calibrateX : e, this.calibrateY = void 0 === t ? this.calibrateY : t
                            }
                        }, {
                            key: "invert",
                            value: function (e, t) {
                                this.invertX = void 0 === e ? this.invertX : e, this.invertY = void 0 === t ? this.invertY : t
                            }
                        }, {
                            key: "friction",
                            value: function (e, t) {
                                this.frictionX = void 0 === e ? this.frictionX : e, this.frictionY = void 0 === t ? this.frictionY : t
                            }
                        }, {
                            key: "scalar",
                            value: function (e, t) {
                                this.scalarX = void 0 === e ? this.scalarX : e, this.scalarY = void 0 === t ? this.scalarY : t
                            }
                        }, {
                            key: "limit",
                            value: function (e, t) {
                                this.limitX = void 0 === e ? this.limitX : e, this.limitY = void 0 === t ? this.limitY : t
                            }
                        }, {
                            key: "origin",
                            value: function (e, t) {
                                this.originX = void 0 === e ? this.originX : e, this.originY = void 0 === t ? this.originY : t
                            }
                        }, {
                            key: "setInputElement",
                            value: function (e) {
                                this.inputElement = e, this.updateDimensions()
                            }
                        }, {
                            key: "setPosition",
                            value: function (e, t, i) {
                                t = t.toFixed(this.precision) + "px", i = i.toFixed(this.precision) + "px", this.transform3DSupport ? r.css(e, "transform", "translate3d(" + t + "," + i + ",0)") : this.transform2DSupport ? r.css(e, "transform", "translate(" + t + "," + i + ")") : (e.style.left = t, e.style.top = i)
                            }
                        }, {
                            key: "onOrientationTimer",
                            value: function () {
                                this.orientationSupport && 0 === this.orientationStatus ? (this.disable(), this.orientationSupport = !1, this.enable()) : this.doReadyCallback()
                            }
                        }, {
                            key: "onMotionTimer",
                            value: function () {
                                this.motionSupport && 0 === this.motionStatus ? (this.disable(), this.motionSupport = !1, this.enable()) : this.doReadyCallback()
                            }
                        }, {
                            key: "onCalibrationTimer",
                            value: function () {
                                this.calibrationFlag = !0
                            }
                        }, {
                            key: "onWindowResize",
                            value: function () {
                                this.updateDimensions()
                            }
                        }, {
                            key: "onAnimationFrame",
                            value: function () {
                                this.updateBounds();
                                var e = this.inputX - this.calibrationX,
                                    t = this.inputY - this.calibrationY;
                                (Math.abs(e) > this.calibrationThreshold || Math.abs(t) > this.calibrationThreshold) && this.queueCalibration(0), this.portrait ? (this.motionX = this.calibrateX ? t : this.inputY, this.motionY = this.calibrateY ? e : this.inputX) : (this.motionX = this.calibrateX ? e : this.inputX, this.motionY = this.calibrateY ? t : this.inputY), this.motionX *= this.elementWidth * (this.scalarX / 100), this.motionY *= this.elementHeight * (this.scalarY / 100), isNaN(parseFloat(this.limitX)) || (this.motionX = r.clamp(this.motionX, -this.limitX, this.limitX)), isNaN(parseFloat(this.limitY)) || (this.motionY = r.clamp(this.motionY, -this.limitY, this.limitY)), this.velocityX += (this.motionX - this.velocityX) * this.frictionX, this.velocityY += (this.motionY - this.velocityY) * this.frictionY;
                                for (var i = 0; i < this.layers.length; i++) {
                                    var n = this.layers[i],
                                        s = this.depthsX[i],
                                        a = this.depthsY[i],
                                        l = this.velocityX * (s * (this.invertX ? -1 : 1)),
                                        d = this.velocityY * (a * (this.invertY ? -1 : 1));
                                    this.setPosition(n, l, d)
                                }
                                this.raf = o(this.onAnimationFrame)
                            }
                        }, {
                            key: "rotate",
                            value: function (e, t) {
                                var i = (e || 0) / 30,
                                    n = (t || 0) / 30,
                                    s = this.windowHeight > this.windowWidth;
                                this.portrait !== s && (this.portrait = s, this.calibrationFlag = !0), this.calibrationFlag && (this.calibrationFlag = !1, this.calibrationX = i, this.calibrationY = n), this.inputX = i, this.inputY = n
                            }
                        }, {
                            key: "onDeviceOrientation",
                            value: function (e) {
                                var t = e.beta,
                                    i = e.gamma;
                                null !== t && null !== i && (this.orientationStatus = 1, this.rotate(t, i))
                            }
                        }, {
                            key: "onDeviceMotion",
                            value: function (e) {
                                var t = e.rotationRate.beta,
                                    i = e.rotationRate.gamma;
                                null !== t && null !== i && (this.motionStatus = 1, this.rotate(t, i))
                            }
                        }, {
                            key: "onMouseMove",
                            value: function (e) {
                                var t = e.clientX,
                                    i = e.clientY;
                                if (this.hoverOnly && (t < this.elementPositionX || t > this.elementPositionX + this.elementWidth || i < this.elementPositionY || i > this.elementPositionY + this.elementHeight)) return this.inputX = 0, void(this.inputY = 0);
                                this.relativeInput ? (this.clipRelativeInput && (t = Math.max(t, this.elementPositionX), t = Math.min(t, this.elementPositionX + this.elementWidth), i = Math.max(i, this.elementPositionY), i = Math.min(i, this.elementPositionY + this.elementHeight)), this.elementRangeX && this.elementRangeY && (this.inputX = (t - this.elementPositionX - this.elementCenterX) / this.elementRangeX, this.inputY = (i - this.elementPositionY - this.elementCenterY) / this.elementRangeY)) : this.windowRadiusX && this.windowRadiusY && (this.inputX = (t - this.windowCenterX) / this.windowRadiusX, this.inputY = (i - this.windowCenterY) / this.windowRadiusY)
                            }
                        }, {
                            key: "destroy",
                            value: function () {
                                this.disable(), clearTimeout(this.calibrationTimer), clearTimeout(this.detectionTimer), this.element.removeAttribute("style");
                                for (var e = 0; e < this.layers.length; e++) this.layers[e].removeAttribute("style");
                                delete this.element, delete this.layers
                            }
                        }, {
                            key: "version",
                            value: function () {
                                return "3.1.0"
                            }
                        }]), e
                    }();
                t.exports = d
            }, {
                "object-assign": 1,
                raf: 4
            }]
        }, {}, [5])(5)
    }),
    function (e, t) {
        "function" == typeof define && define.amd ? define([], t) : "object" == typeof module && module.exports ? module.exports = t() : e.Rellax = t()
    }(this, function () {
        var e = function (t, i) {
            "use strict";
            var n = Object.create(e.prototype),
                s = 0,
                o = 0,
                a = 0,
                r = [],
                l = !1,
                d = window.requestAnimationFrame || window.webkitRequestAnimationFrame || window.mozRequestAnimationFrame || window.msRequestAnimationFrame || window.oRequestAnimationFrame || function (e) {
                    setTimeout(e, 1e3 / 60)
                },
                c = window.transformProp || function () {
                    var e = document.createElement("div");
                    if (null === e.style.transform) {
                        var t = ["Webkit", "Moz", "ms"];
                        for (var i in t)
                            if (void 0 !== e.style[t[i] + "Transform"]) return t[i] + "Transform"
                    }
                    return "transform"
                }();
            n.options = {
                speed: -2,
                center: !1,
                wrapper: null,
                round: !0,
                vertical: !0,
                horizontal: !1,
                callback: function () {}
            }, i && Object.keys(i).forEach(function (e) {
                n.options[e] = i[e]
            }), t || (t = ".rellax");
            var p = "string" == typeof t ? document.querySelectorAll(t) : [t];
            if (!(p.length > 0)) throw new Error("The elements you're trying to select don't exist.");
            if (n.elems = p, n.options.wrapper && !n.options.wrapper.nodeType) {
                var u = document.querySelector(n.options.wrapper);
                if (!u) throw new Error("The wrapper you're trying to use don't exist.");
                n.options.wrapper = u
            }
            var h = function () {
                    for (var e = 0; e < n.elems.length; e++) {
                        var t = v(n.elems[e]);
                        r.push(t)
                    }
                },
                f = function () {
                    for (var e = 0; e < r.length; e++) n.elems[e].style.cssText = r[e].style;
                    r = [], o = window.innerHeight, a = window.innerWidth, m(), h(), y()
                },
                v = function (e) {
                    var t = e.getAttribute("data-rellax-percentage"),
                        i = e.getAttribute("data-rellax-speed"),
                        s = e.getAttribute("data-rellax-zindex") || 0,
                        r = n.options.wrapper ? n.options.wrapper.scrollTop : window.pageYOffset || document.documentElement.scrollTop || document.body.scrollTop,
                        l = n.options.vertical && (t || n.options.center) ? r : 0,
                        d = n.options.horizontal && (t || n.options.center) ? window.pageXOffset || document.documentElement.scrollLeft || document.body.scrollLeft : 0,
                        c = l + e.getBoundingClientRect().top,
                        p = e.clientHeight || e.offsetHeight || e.scrollHeight,
                        u = d + e.getBoundingClientRect().left,
                        h = e.clientWidth || e.offsetWidth || e.scrollWidth,
                        f = t || (l - c + o) / (p + o),
                        v = t || (d - u + a) / (h + a);
                    n.options.center && (v = .5, f = .5);
                    var m = i || n.options.speed,
                        w = g(v, f, m),
                        y = e.style.cssText,
                        b = "";
                    if (y.indexOf("transform") >= 0) {
                        var T = y.indexOf("transform"),
                            x = y.slice(T),
                            S = x.indexOf(";");
                        b = S ? " " + x.slice(11, S).replace(/\s/g, "") : " " + x.slice(11).replace(/\s/g, "")
                    }
                    return {
                        baseX: w.x,
                        baseY: w.y,
                        top: c,
                        left: u,
                        height: p,
                        width: h,
                        speed: m,
                        style: y,
                        transform: b,
                        zindex: s
                    }
                },
                m = function () {
                    var e = s;
                    return s = n.options.wrapper ? n.options.wrapper.scrollTop : (document.documentElement || document.body.parentNode || document.body).scrollTop || window.pageYOffset, !(e == s || !n.options.vertical)
                },
                g = function (e, t, i) {
                    var s = {},
                        o = i * (100 * (1 - e)),
                        a = i * (100 * (1 - t));
                    return s.x = n.options.round ? Math.round(o) : Math.round(100 * o) / 100, s.y = n.options.round ? Math.round(a) : Math.round(100 * a) / 100, s
                },
                w = function () {
                    m() && !1 === l && y(), d(w)
                },
                y = function () {
                    for (var e, t = 0; t < n.elems.length; t++) {
                        var i = (s - r[t].top + o) / (r[t].height + o),
                            l = (0 - r[t].left + a) / (r[t].width + a);
                        e = g(l, i, r[t].speed);
                        var d = e.y - r[t].baseY,
                            p = e.x - r[t].baseX,
                            u = r[t].zindex,
                            h = "translate3d(" + (n.options.horizontal ? p : "0") + "px," + (n.options.vertical ? d : "0") + "px," + u + "px) " + r[t].transform;
                        n.elems[t].style[c] = h
                    }
                    n.options.callback(e)
                };
            return n.destroy = function () {
                for (var e = 0; e < n.elems.length; e++) n.elems[e].style.cssText = r[e].style;
                l = !0
            }, f(), window.addEventListener("resize", function () {
                f()
            }), w(), n.refresh = f, n
        };
        return e
    }),
    function (e) {
        "use strict";
        "function" == typeof define && define.amd ? define(["jquery"], e) : "undefined" != typeof exports ? module.exports = e(require("jquery")) : e(jQuery)
    }(function (e) {
        "use strict";
        var t = window.Slick || {};
        t = function () {
            function t(t, n) {
                var s, o = this;
                o.defaults = {
                    accessibility: !0,
                    adaptiveHeight: !1,
                    appendArrows: e(t),
                    appendDots: e(t),
                    arrows: !0,
                    asNavFor: null,
                    prevArrow: '<button class="slick-prev" aria-label="Previous" type="button">Previous</button>',
                    nextArrow: '<button class="slick-next" aria-label="Next" type="button">Next</button>',
                    autoplay: !1,
                    autoplaySpeed: 3e3,
                    centerMode: !1,
                    centerPadding: "50px",
                    cssEase: "ease",
                    customPaging: function (t, i) {
                        return e('<button type="button" />').text(i + 1)
                    },
                    dots: !1,
                    dotsClass: "slick-dots",
                    draggable: !0,
                    easing: "linear",
                    edgeFriction: .35,
                    fade: !1,
                    focusOnSelect: !1,
                    focusOnChange: !1,
                    infinite: !0,
                    initialSlide: 0,
                    lazyLoad: "ondemand",
                    mobileFirst: !1,
                    pauseOnHover: !0,
                    pauseOnFocus: !0,
                    pauseOnDotsHover: !1,
                    respondTo: "window",
                    responsive: null,
                    rows: 1,
                    rtl: !1,
                    slide: "",
                    slidesPerRow: 1,
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    speed: 500,
                    swipe: !0,
                    swipeToSlide: !1,
                    touchMove: !0,
                    touchThreshold: 5,
                    useCSS: !0,
                    useTransform: !0,
                    variableWidth: !1,
                    vertical: !1,
                    verticalSwiping: !1,
                    waitForAnimate: !0,
                    zIndex: 1e3
                }, o.initials = {
                    animating: !1,
                    dragging: !1,
                    autoPlayTimer: null,
                    currentDirection: 0,
                    currentLeft: null,
                    currentSlide: 0,
                    direction: 1,
                    $dots: null,
                    listWidth: null,
                    listHeight: null,
                    loadIndex: 0,
                    $nextArrow: null,
                    $prevArrow: null,
                    scrolling: !1,
                    slideCount: null,
                    slideWidth: null,
                    $slideTrack: null,
                    $slides: null,
                    sliding: !1,
                    slideOffset: 0,
                    swipeLeft: null,
                    swiping: !1,
                    $list: null,
                    touchObject: {},
                    transformsEnabled: !1,
                    unslicked: !1
                }, e.extend(o, o.initials), o.activeBreakpoint = null, o.animType = null, o.animProp = null, o.breakpoints = [], o.breakpointSettings = [], o.cssTransitions = !1, o.focussed = !1, o.interrupted = !1, o.hidden = "hidden", o.paused = !0, o.positionProp = null, o.respondTo = null, o.rowCount = 1, o.shouldClick = !0, o.$slider = e(t), o.$slidesCache = null, o.transformType = null, o.transitionType = null, o.visibilityChange = "visibilitychange", o.windowWidth = 0, o.windowTimer = null, s = e(t).data("slick") || {}, o.options = e.extend({}, o.defaults, n, s), o.currentSlide = o.options.initialSlide, o.originalSettings = o.options, void 0 !== document.mozHidden ? (o.hidden = "mozHidden", o.visibilityChange = "mozvisibilitychange") : void 0 !== document.webkitHidden && (o.hidden = "webkitHidden", o.visibilityChange = "webkitvisibilitychange"), o.autoPlay = e.proxy(o.autoPlay, o), o.autoPlayClear = e.proxy(o.autoPlayClear, o), o.autoPlayIterator = e.proxy(o.autoPlayIterator, o), o.changeSlide = e.proxy(o.changeSlide, o), o.clickHandler = e.proxy(o.clickHandler, o), o.selectHandler = e.proxy(o.selectHandler, o), o.setPosition = e.proxy(o.setPosition, o), o.swipeHandler = e.proxy(o.swipeHandler, o), o.dragHandler = e.proxy(o.dragHandler, o), o.keyHandler = e.proxy(o.keyHandler, o), o.instanceUid = i++, o.htmlExpr = /^(?:\s*(<[\w\W]+>)[^>]*)$/, o.registerBreakpoints(), o.init(!0)
            }
            var i = 0;
            return t
        }(), t.prototype.activateADA = function () {
            this.$slideTrack.find(".slick-active").attr({
                "aria-hidden": "false"
            }).find("a, input, button, select").attr({
                tabindex: "0"
            })
        }, t.prototype.addSlide = t.prototype.slickAdd = function (t, i, n) {
            var s = this;
            if ("boolean" == typeof i) n = i, i = null;
            else if (i < 0 || i >= s.slideCount) return !1;
            s.unload(), "number" == typeof i ? 0 === i && 0 === s.$slides.length ? e(t).appendTo(s.$slideTrack) : n ? e(t).insertBefore(s.$slides.eq(i)) : e(t).insertAfter(s.$slides.eq(i)) : !0 === n ? e(t).prependTo(s.$slideTrack) : e(t).appendTo(s.$slideTrack), s.$slides = s.$slideTrack.children(this.options.slide), s.$slideTrack.children(this.options.slide).detach(), s.$slideTrack.append(s.$slides), s.$slides.each(function (t, i) {
                e(i).attr("data-slick-index", t)
            }), s.$slidesCache = s.$slides, s.reinit()
        }, t.prototype.animateHeight = function () {
            var e = this;
            if (1 === e.options.slidesToShow && !0 === e.options.adaptiveHeight && !1 === e.options.vertical) {
                var t = e.$slides.eq(e.currentSlide).outerHeight(!0);
                e.$list.animate({
                    height: t
                }, e.options.speed)
            }
        }, t.prototype.animateSlide = function (t, i) {
            var n = {},
                s = this;
            s.animateHeight(), !0 === s.options.rtl && !1 === s.options.vertical && (t = -t), !1 === s.transformsEnabled ? !1 === s.options.vertical ? s.$slideTrack.animate({
                left: t
            }, s.options.speed, s.options.easing, i) : s.$slideTrack.animate({
                top: t
            }, s.options.speed, s.options.easing, i) : !1 === s.cssTransitions ? (!0 === s.options.rtl && (s.currentLeft = -s.currentLeft), e({
                animStart: s.currentLeft
            }).animate({
                animStart: t
            }, {
                duration: s.options.speed,
                easing: s.options.easing,
                step: function (e) {
                    e = Math.ceil(e), !1 === s.options.vertical ? (n[s.animType] = "translate(" + e + "px, 0px)", s.$slideTrack.css(n)) : (n[s.animType] = "translate(0px," + e + "px)", s.$slideTrack.css(n))
                },
                complete: function () {
                    i && i.call()
                }
            })) : (s.applyTransition(), t = Math.ceil(t), !1 === s.options.vertical ? n[s.animType] = "translate3d(" + t + "px, 0px, 0px)" : n[s.animType] = "translate3d(0px," + t + "px, 0px)", s.$slideTrack.css(n), i && setTimeout(function () {
                s.disableTransition(), i.call()
            }, s.options.speed))
        }, t.prototype.getNavTarget = function () {
            var t = this,
                i = t.options.asNavFor;
            return i && null !== i && (i = e(i).not(t.$slider)), i
        }, t.prototype.asNavFor = function (t) {
            var i = this,
                n = i.getNavTarget();
            null !== n && "object" == typeof n && n.each(function () {
                var i = e(this).slick("getSlick");
                i.unslicked || i.slideHandler(t, !0)
            })
        }, t.prototype.applyTransition = function (e) {
            var t = this,
                i = {};
            !1 === t.options.fade ? i[t.transitionType] = t.transformType + " " + t.options.speed + "ms " + t.options.cssEase : i[t.transitionType] = "opacity " + t.options.speed + "ms " + t.options.cssEase, !1 === t.options.fade ? t.$slideTrack.css(i) : t.$slides.eq(e).css(i)
        }, t.prototype.autoPlay = function () {
            var e = this;
            e.autoPlayClear(), e.slideCount > e.options.slidesToShow && (e.autoPlayTimer = setInterval(e.autoPlayIterator, e.options.autoplaySpeed))
        }, t.prototype.autoPlayClear = function () {
            var e = this;
            e.autoPlayTimer && clearInterval(e.autoPlayTimer)
        }, t.prototype.autoPlayIterator = function () {
            var e = this,
                t = e.currentSlide + e.options.slidesToScroll;
            e.paused || e.interrupted || e.focussed || (!1 === e.options.infinite && (1 === e.direction && e.currentSlide + 1 === e.slideCount - 1 ? e.direction = 0 : 0 === e.direction && (t = e.currentSlide - e.options.slidesToScroll, e.currentSlide - 1 == 0 && (e.direction = 1))), e.slideHandler(t))
        }, t.prototype.buildArrows = function () {
            var t = this;
            !0 === t.options.arrows && (t.$prevArrow = e(t.options.prevArrow).addClass("slick-arrow"), t.$nextArrow = e(t.options.nextArrow).addClass("slick-arrow"), t.slideCount > t.options.slidesToShow ? (t.$prevArrow.removeClass("slick-hidden").removeAttr("aria-hidden tabindex"), t.$nextArrow.removeClass("slick-hidden").removeAttr("aria-hidden tabindex"), t.htmlExpr.test(t.options.prevArrow) && t.$prevArrow.prependTo(t.options.appendArrows), t.htmlExpr.test(t.options.nextArrow) && t.$nextArrow.appendTo(t.options.appendArrows), !0 !== t.options.infinite && t.$prevArrow.addClass("slick-disabled").attr("aria-disabled", "true")) : t.$prevArrow.add(t.$nextArrow).addClass("slick-hidden").attr({
                "aria-disabled": "true",
                tabindex: "-1"
            }))
        }, t.prototype.buildDots = function () {
            var t, i, n = this;
            if (!0 === n.options.dots && n.slideCount > n.options.slidesToShow) {
                for (n.$slider.addClass("slick-dotted"), i = e("<ul />").addClass(n.options.dotsClass), t = 0; t <= n.getDotCount(); t += 1) i.append(e("<li />").append(n.options.customPaging.call(this, n, t)));
                n.$dots = i.appendTo(n.options.appendDots), n.$dots.find("li").first().addClass("slick-active")
            }
        }, t.prototype.buildOut = function () {
            var t = this;
            t.$slides = t.$slider.children(t.options.slide + ":not(.slick-cloned)").addClass("slick-slide"), t.slideCount = t.$slides.length, t.$slides.each(function (t, i) {
                e(i).attr("data-slick-index", t).data("originalStyling", e(i).attr("style") || "")
            }), t.$slider.addClass("slick-slider"), t.$slideTrack = 0 === t.slideCount ? e('<div class="slick-track"/>').appendTo(t.$slider) : t.$slides.wrapAll('<div class="slick-track"/>').parent(), t.$list = t.$slideTrack.wrap('<div class="slick-list"/>').parent(), t.$slideTrack.css("opacity", 0), !0 !== t.options.centerMode && !0 !== t.options.swipeToSlide || (t.options.slidesToScroll = 1), e("img[data-lazy]", t.$slider).not("[src]").addClass("slick-loading"), t.setupInfinite(), t.buildArrows(), t.buildDots(), t.updateDots(), t.setSlideClasses("number" == typeof t.currentSlide ? t.currentSlide : 0), !0 === t.options.draggable && t.$list.addClass("draggable")
        }, t.prototype.buildRows = function () {
            var e, t, i, n, s, o, a, r = this;
            if (n = document.createDocumentFragment(), o = r.$slider.children(), r.options.rows > 0) {
                for (a = r.options.slidesPerRow * r.options.rows, s = Math.ceil(o.length / a), e = 0; e < s; e++) {
                    var l = document.createElement("div");
                    for (t = 0; t < r.options.rows; t++) {
                        var d = document.createElement("div");
                        for (i = 0; i < r.options.slidesPerRow; i++) {
                            var c = e * a + (t * r.options.slidesPerRow + i);
                            o.get(c) && d.appendChild(o.get(c))
                        }
                        l.appendChild(d)
                    }
                    n.appendChild(l)
                }
                r.$slider.empty().append(n), r.$slider.children().children().children().css({
                    width: 100 / r.options.slidesPerRow + "%",
                    display: "inline-block"
                })
            }
        }, t.prototype.checkResponsive = function (t, i) {
            var n, s, o, a = this,
                r = !1,
                l = a.$slider.width(),
                d = window.innerWidth || e(window).width();
            if ("window" === a.respondTo ? o = d : "slider" === a.respondTo ? o = l : "min" === a.respondTo && (o = Math.min(d, l)), a.options.responsive && a.options.responsive.length && null !== a.options.responsive) {
                s = null;
                for (n in a.breakpoints) a.breakpoints.hasOwnProperty(n) && (!1 === a.originalSettings.mobileFirst ? o < a.breakpoints[n] && (s = a.breakpoints[n]) : o > a.breakpoints[n] && (s = a.breakpoints[n]));
                null !== s ? null !== a.activeBreakpoint ? (s !== a.activeBreakpoint || i) && (a.activeBreakpoint = s, "unslick" === a.breakpointSettings[s] ? a.unslick(s) : (a.options = e.extend({}, a.originalSettings, a.breakpointSettings[s]), !0 === t && (a.currentSlide = a.options.initialSlide), a.refresh(t)), r = s) : (a.activeBreakpoint = s, "unslick" === a.breakpointSettings[s] ? a.unslick(s) : (a.options = e.extend({}, a.originalSettings, a.breakpointSettings[s]), !0 === t && (a.currentSlide = a.options.initialSlide), a.refresh(t)), r = s) : null !== a.activeBreakpoint && (a.activeBreakpoint = null, a.options = a.originalSettings, !0 === t && (a.currentSlide = a.options.initialSlide), a.refresh(t), r = s), t || !1 === r || a.$slider.trigger("breakpoint", [a, r])
            }
        }, t.prototype.changeSlide = function (t, i) {
            var n, s, o, a = this,
                r = e(t.currentTarget);
            switch (r.is("a") && t.preventDefault(), r.is("li") || (r = r.closest("li")), o = a.slideCount % a.options.slidesToScroll != 0, n = o ? 0 : (a.slideCount - a.currentSlide) % a.options.slidesToScroll, t.data.message) {
                case "previous":
                    s = 0 === n ? a.options.slidesToScroll : a.options.slidesToShow - n, a.slideCount > a.options.slidesToShow && a.slideHandler(a.currentSlide - s, !1, i);
                    break;
                case "next":
                    s = 0 === n ? a.options.slidesToScroll : n, a.slideCount > a.options.slidesToShow && a.slideHandler(a.currentSlide + s, !1, i);
                    break;
                case "index":
                    var l = 0 === t.data.index ? 0 : t.data.index || r.index() * a.options.slidesToScroll;
                    a.slideHandler(a.checkNavigable(l), !1, i), r.children().trigger("focus");
                    break;
                default:
                    return
            }
        }, t.prototype.checkNavigable = function (e) {
            var t, i, n = this;
            if (t = n.getNavigableIndexes(), i = 0, e > t[t.length - 1]) e = t[t.length - 1];
            else
                for (var s in t) {
                    if (e < t[s]) {
                        e = i;
                        break
                    }
                    i = t[s]
                }
            return e
        }, t.prototype.cleanUpEvents = function () {
            var t = this;
            t.options.dots && null !== t.$dots && (e("li", t.$dots).off("click.slick", t.changeSlide).off("mouseenter.slick", e.proxy(t.interrupt, t, !0)).off("mouseleave.slick", e.proxy(t.interrupt, t, !1)), !0 === t.options.accessibility && t.$dots.off("keydown.slick", t.keyHandler)), t.$slider.off("focus.slick blur.slick"), !0 === t.options.arrows && t.slideCount > t.options.slidesToShow && (t.$prevArrow && t.$prevArrow.off("click.slick", t.changeSlide), t.$nextArrow && t.$nextArrow.off("click.slick", t.changeSlide), !0 === t.options.accessibility && (t.$prevArrow && t.$prevArrow.off("keydown.slick", t.keyHandler), t.$nextArrow && t.$nextArrow.off("keydown.slick", t.keyHandler))), t.$list.off("touchstart.slick mousedown.slick", t.swipeHandler), t.$list.off("touchmove.slick mousemove.slick", t.swipeHandler), t.$list.off("touchend.slick mouseup.slick", t.swipeHandler), t.$list.off("touchcancel.slick mouseleave.slick", t.swipeHandler), t.$list.off("click.slick", t.clickHandler), e(document).off(t.visibilityChange, t.visibility), t.cleanUpSlideEvents(), !0 === t.options.accessibility && t.$list.off("keydown.slick", t.keyHandler), !0 === t.options.focusOnSelect && e(t.$slideTrack).children().off("click.slick", t.selectHandler), e(window).off("orientationchange.slick.slick-" + t.instanceUid, t.orientationChange), e(window).off("resize.slick.slick-" + t.instanceUid, t.resize), e("[draggable!=true]", t.$slideTrack).off("dragstart", t.preventDefault), e(window).off("load.slick.slick-" + t.instanceUid, t.setPosition)
        }, t.prototype.cleanUpSlideEvents = function () {
            var t = this;
            t.$list.off("mouseenter.slick", e.proxy(t.interrupt, t, !0)), t.$list.off("mouseleave.slick", e.proxy(t.interrupt, t, !1))
        }, t.prototype.cleanUpRows = function () {
            var e, t = this;
            t.options.rows > 0 && (e = t.$slides.children().children(), e.removeAttr("style"), t.$slider.empty().append(e))
        }, t.prototype.clickHandler = function (e) {
            !1 === this.shouldClick && (e.stopImmediatePropagation(), e.stopPropagation(), e.preventDefault())
        }, t.prototype.destroy = function (t) {
            var i = this;
            i.autoPlayClear(), i.touchObject = {}, i.cleanUpEvents(), e(".slick-cloned", i.$slider).detach(), i.$dots && i.$dots.remove(), i.$prevArrow && i.$prevArrow.length && (i.$prevArrow.removeClass("slick-disabled slick-arrow slick-hidden").removeAttr("aria-hidden aria-disabled tabindex").css("display", ""), i.htmlExpr.test(i.options.prevArrow) && i.$prevArrow.remove()), i.$nextArrow && i.$nextArrow.length && (i.$nextArrow.removeClass("slick-disabled slick-arrow slick-hidden").removeAttr("aria-hidden aria-disabled tabindex").css("display", ""), i.htmlExpr.test(i.options.nextArrow) && i.$nextArrow.remove()), i.$slides && (i.$slides.removeClass("slick-slide slick-active slick-center slick-visible slick-current").removeAttr("aria-hidden").removeAttr("data-slick-index").each(function () {
                e(this).attr("style", e(this).data("originalStyling"))
            }), i.$slideTrack.children(this.options.slide).detach(), i.$slideTrack.detach(), i.$list.detach(), i.$slider.append(i.$slides)), i.cleanUpRows(), i.$slider.removeClass("slick-slider"), i.$slider.removeClass("slick-initialized"), i.$slider.removeClass("slick-dotted"), i.unslicked = !0, t || i.$slider.trigger("destroy", [i])
        }, t.prototype.disableTransition = function (e) {
            var t = this,
                i = {};
            i[t.transitionType] = "", !1 === t.options.fade ? t.$slideTrack.css(i) : t.$slides.eq(e).css(i)
        }, t.prototype.fadeSlide = function (e, t) {
            var i = this;
            !1 === i.cssTransitions ? (i.$slides.eq(e).css({
                zIndex: i.options.zIndex
            }), i.$slides.eq(e).animate({
                opacity: 1
            }, i.options.speed, i.options.easing, t)) : (i.applyTransition(e), i.$slides.eq(e).css({
                opacity: 1,
                zIndex: i.options.zIndex
            }), t && setTimeout(function () {
                i.disableTransition(e), t.call()
            }, i.options.speed))
        }, t.prototype.fadeSlideOut = function (e) {
            var t = this;
            !1 === t.cssTransitions ? t.$slides.eq(e).animate({
                opacity: 0,
                zIndex: t.options.zIndex - 2
            }, t.options.speed, t.options.easing) : (t.applyTransition(e), t.$slides.eq(e).css({
                opacity: 0,
                zIndex: t.options.zIndex - 2
            }))
        }, t.prototype.filterSlides = t.prototype.slickFilter = function (e) {
            var t = this;
            null !== e && (t.$slidesCache = t.$slides, t.unload(), t.$slideTrack.children(this.options.slide).detach(), t.$slidesCache.filter(e).appendTo(t.$slideTrack), t.reinit())
        }, t.prototype.focusHandler = function () {
            var t = this;
            t.$slider.off("focus.slick blur.slick").on("focus.slick", "*", function (i) {
                var n = e(this);
                setTimeout(function () {
                    t.options.pauseOnFocus && n.is(":focus") && (t.focussed = !0, t.autoPlay())
                }, 0)
            }).on("blur.slick", "*", function (i) {
                e(this);
                t.options.pauseOnFocus && (t.focussed = !1, t.autoPlay())
            })
        }, t.prototype.getCurrent = t.prototype.slickCurrentSlide = function () {
            return this.currentSlide
        }, t.prototype.getDotCount = function () {
            var e = this,
                t = 0,
                i = 0,
                n = 0;
            if (!0 === e.options.infinite)
                if (e.slideCount <= e.options.slidesToShow) ++n;
                else
                    for (; t < e.slideCount;) ++n, t = i + e.options.slidesToScroll, i += e.options.slidesToScroll <= e.options.slidesToShow ? e.options.slidesToScroll : e.options.slidesToShow;
            else if (!0 === e.options.centerMode) n = e.slideCount;
            else if (e.options.asNavFor)
                for (; t < e.slideCount;) ++n, t = i + e.options.slidesToScroll, i += e.options.slidesToScroll <= e.options.slidesToShow ? e.options.slidesToScroll : e.options.slidesToShow;
            else n = 1 + Math.ceil((e.slideCount - e.options.slidesToShow) / e.options.slidesToScroll);
            return n - 1
        }, t.prototype.getLeft = function (e) {
            var t, i, n, s, o = this,
                a = 0;
            return o.slideOffset = 0, i = o.$slides.first().outerHeight(!0), !0 === o.options.infinite ? (o.slideCount > o.options.slidesToShow && (o.slideOffset = o.slideWidth * o.options.slidesToShow * -1, s = -1, !0 === o.options.vertical && !0 === o.options.centerMode && (2 === o.options.slidesToShow ? s = -1.5 : 1 === o.options.slidesToShow && (s = -2)), a = i * o.options.slidesToShow * s), o.slideCount % o.options.slidesToScroll != 0 && e + o.options.slidesToScroll > o.slideCount && o.slideCount > o.options.slidesToShow && (e > o.slideCount ? (o.slideOffset = (o.options.slidesToShow - (e - o.slideCount)) * o.slideWidth * -1, a = (o.options.slidesToShow - (e - o.slideCount)) * i * -1) : (o.slideOffset = o.slideCount % o.options.slidesToScroll * o.slideWidth * -1, a = o.slideCount % o.options.slidesToScroll * i * -1))) : e + o.options.slidesToShow > o.slideCount && (o.slideOffset = (e + o.options.slidesToShow - o.slideCount) * o.slideWidth, a = (e + o.options.slidesToShow - o.slideCount) * i), o.slideCount <= o.options.slidesToShow && (o.slideOffset = 0, a = 0), !0 === o.options.centerMode && o.slideCount <= o.options.slidesToShow ? o.slideOffset = o.slideWidth * Math.floor(o.options.slidesToShow) / 2 - o.slideWidth * o.slideCount / 2 : !0 === o.options.centerMode && !0 === o.options.infinite ? o.slideOffset += o.slideWidth * Math.floor(o.options.slidesToShow / 2) - o.slideWidth : !0 === o.options.centerMode && (o.slideOffset = 0, o.slideOffset += o.slideWidth * Math.floor(o.options.slidesToShow / 2)), t = !1 === o.options.vertical ? e * o.slideWidth * -1 + o.slideOffset : e * i * -1 + a, !0 === o.options.variableWidth && (n = o.slideCount <= o.options.slidesToShow || !1 === o.options.infinite ? o.$slideTrack.children(".slick-slide").eq(e) : o.$slideTrack.children(".slick-slide").eq(e + o.options.slidesToShow), t = !0 === o.options.rtl ? n[0] ? -1 * (o.$slideTrack.width() - n[0].offsetLeft - n.width()) : 0 : n[0] ? -1 * n[0].offsetLeft : 0, !0 === o.options.centerMode && (n = o.slideCount <= o.options.slidesToShow || !1 === o.options.infinite ? o.$slideTrack.children(".slick-slide").eq(e) : o.$slideTrack.children(".slick-slide").eq(e + o.options.slidesToShow + 1), t = !0 === o.options.rtl ? n[0] ? -1 * (o.$slideTrack.width() - n[0].offsetLeft - n.width()) : 0 : n[0] ? -1 * n[0].offsetLeft : 0, t += (o.$list.width() - n.outerWidth()) / 2)), t
        }, t.prototype.getOption = t.prototype.slickGetOption = function (e) {
            return this.options[e]
        }, t.prototype.getNavigableIndexes = function () {
            var e, t = this,
                i = 0,
                n = 0,
                s = [];
            for (!1 === t.options.infinite ? e = t.slideCount : (i = -1 * t.options.slidesToScroll, n = -1 * t.options.slidesToScroll, e = 2 * t.slideCount); i < e;) s.push(i), i = n + t.options.slidesToScroll, n += t.options.slidesToScroll <= t.options.slidesToShow ? t.options.slidesToScroll : t.options.slidesToShow;
            return s
        }, t.prototype.getSlick = function () {
            return this
        }, t.prototype.getSlideCount = function () {
            var t, i, n, s = this;
            return n = !0 === s.options.centerMode ? Math.floor(s.$list.width() / 2) : 0, i = -1 * s.swipeLeft + n, !0 === s.options.swipeToSlide ? (s.$slideTrack.find(".slick-slide").each(function (n, o) {
                var a, r, l;
                if (a = e(o).outerWidth(), r = o.offsetLeft, !0 !== s.options.centerMode && (r += a / 2), l = r + a, i < l) return t = o, !1
            }), Math.abs(e(t).attr("data-slick-index") - s.currentSlide) || 1) : s.options.slidesToScroll
        }, t.prototype.goTo = t.prototype.slickGoTo = function (e, t) {
            this.changeSlide({
                data: {
                    message: "index",
                    index: parseInt(e)
                }
            }, t)
        }, t.prototype.init = function (t) {
            var i = this;
            e(i.$slider).hasClass("slick-initialized") || (e(i.$slider).addClass("slick-initialized"), i.buildRows(), i.buildOut(), i.setProps(), i.startLoad(), i.loadSlider(), i.initializeEvents(), i.updateArrows(), i.updateDots(), i.checkResponsive(!0), i.focusHandler()),
                t && i.$slider.trigger("init", [i]), !0 === i.options.accessibility && i.initADA(), i.options.autoplay && (i.paused = !1, i.autoPlay())
        }, t.prototype.initADA = function () {
            var t = this,
                i = Math.ceil(t.slideCount / t.options.slidesToShow),
                n = t.getNavigableIndexes().filter(function (e) {
                    return e >= 0 && e < t.slideCount
                });
            t.$slides.add(t.$slideTrack.find(".slick-cloned")).attr({
                "aria-hidden": "true",
                tabindex: "-1"
            }).find("a, input, button, select").attr({
                tabindex: "-1"
            }), null !== t.$dots && (t.$slides.not(t.$slideTrack.find(".slick-cloned")).each(function (i) {
                var s = n.indexOf(i);
                if (e(this).attr({
                        role: "tabpanel",
                        id: "slick-slide" + t.instanceUid + i,
                        tabindex: -1
                    }), -1 !== s) {
                    var o = "slick-slide-control" + t.instanceUid + s;
                    e("#" + o).length && e(this).attr({
                        "aria-describedby": o
                    })
                }
            }), t.$dots.attr("role", "tablist").find("li").each(function (s) {
                var o = n[s];
                e(this).attr({
                    role: "presentation"
                }), e(this).find("button").first().attr({
                    role: "tab",
                    id: "slick-slide-control" + t.instanceUid + s,
                    "aria-controls": "slick-slide" + t.instanceUid + o,
                    "aria-label": s + 1 + " of " + i,
                    "aria-selected": null,
                    tabindex: "-1"
                })
            }).eq(t.currentSlide).find("button").attr({
                "aria-selected": "true",
                tabindex: "0"
            }).end());
            for (var s = t.currentSlide, o = s + t.options.slidesToShow; s < o; s++) t.options.focusOnChange ? t.$slides.eq(s).attr({
                tabindex: "0"
            }) : t.$slides.eq(s).removeAttr("tabindex");
            t.activateADA()
        }, t.prototype.initArrowEvents = function () {
            var e = this;
            !0 === e.options.arrows && e.slideCount > e.options.slidesToShow && (e.$prevArrow.off("click.slick").on("click.slick", {
                message: "previous"
            }, e.changeSlide), e.$nextArrow.off("click.slick").on("click.slick", {
                message: "next"
            }, e.changeSlide), !0 === e.options.accessibility && (e.$prevArrow.on("keydown.slick", e.keyHandler), e.$nextArrow.on("keydown.slick", e.keyHandler)))
        }, t.prototype.initDotEvents = function () {
            var t = this;
            !0 === t.options.dots && t.slideCount > t.options.slidesToShow && (e("li", t.$dots).on("click.slick", {
                message: "index"
            }, t.changeSlide), !0 === t.options.accessibility && t.$dots.on("keydown.slick", t.keyHandler)), !0 === t.options.dots && !0 === t.options.pauseOnDotsHover && t.slideCount > t.options.slidesToShow && e("li", t.$dots).on("mouseenter.slick", e.proxy(t.interrupt, t, !0)).on("mouseleave.slick", e.proxy(t.interrupt, t, !1))
        }, t.prototype.initSlideEvents = function () {
            var t = this;
            t.options.pauseOnHover && (t.$list.on("mouseenter.slick", e.proxy(t.interrupt, t, !0)), t.$list.on("mouseleave.slick", e.proxy(t.interrupt, t, !1)))
        }, t.prototype.initializeEvents = function () {
            var t = this;
            t.initArrowEvents(), t.initDotEvents(), t.initSlideEvents(), t.$list.on("touchstart.slick mousedown.slick", {
                action: "start"
            }, t.swipeHandler), t.$list.on("touchmove.slick mousemove.slick", {
                action: "move"
            }, t.swipeHandler), t.$list.on("touchend.slick mouseup.slick", {
                action: "end"
            }, t.swipeHandler), t.$list.on("touchcancel.slick mouseleave.slick", {
                action: "end"
            }, t.swipeHandler), t.$list.on("click.slick", t.clickHandler), e(document).on(t.visibilityChange, e.proxy(t.visibility, t)), !0 === t.options.accessibility && t.$list.on("keydown.slick", t.keyHandler), !0 === t.options.focusOnSelect && e(t.$slideTrack).children().on("click.slick", t.selectHandler), e(window).on("orientationchange.slick.slick-" + t.instanceUid, e.proxy(t.orientationChange, t)), e(window).on("resize.slick.slick-" + t.instanceUid, e.proxy(t.resize, t)), e("[draggable!=true]", t.$slideTrack).on("dragstart", t.preventDefault), e(window).on("load.slick.slick-" + t.instanceUid, t.setPosition), e(t.setPosition)
        }, t.prototype.initUI = function () {
            var e = this;
            !0 === e.options.arrows && e.slideCount > e.options.slidesToShow && (e.$prevArrow.show(), e.$nextArrow.show()), !0 === e.options.dots && e.slideCount > e.options.slidesToShow && e.$dots.show()
        }, t.prototype.keyHandler = function (e) {
            var t = this;
            e.target.tagName.match("TEXTAREA|INPUT|SELECT") || (37 === e.keyCode && !0 === t.options.accessibility ? t.changeSlide({
                data: {
                    message: !0 === t.options.rtl ? "next" : "previous"
                }
            }) : 39 === e.keyCode && !0 === t.options.accessibility && t.changeSlide({
                data: {
                    message: !0 === t.options.rtl ? "previous" : "next"
                }
            }))
        }, t.prototype.lazyLoad = function () {
            function t(t) {
                e("img[data-lazy]", t).each(function () {
                    var t = e(this),
                        i = e(this).attr("data-lazy"),
                        n = e(this).attr("data-srcset"),
                        s = e(this).attr("data-sizes") || a.$slider.attr("data-sizes"),
                        o = document.createElement("img");
                    o.onload = function () {
                        t.animate({
                            opacity: 0
                        }, 100, function () {
                            n && (t.attr("srcset", n), s && t.attr("sizes", s)), t.attr("src", i).animate({
                                opacity: 1
                            }, 200, function () {
                                t.removeAttr("data-lazy data-srcset data-sizes").removeClass("slick-loading")
                            }), a.$slider.trigger("lazyLoaded", [a, t, i])
                        })
                    }, o.onerror = function () {
                        t.removeAttr("data-lazy").removeClass("slick-loading").addClass("slick-lazyload-error"), a.$slider.trigger("lazyLoadError", [a, t, i])
                    }, o.src = i
                })
            }
            var i, n, s, o, a = this;
            if (!0 === a.options.centerMode ? !0 === a.options.infinite ? (s = a.currentSlide + (a.options.slidesToShow / 2 + 1), o = s + a.options.slidesToShow + 2) : (s = Math.max(0, a.currentSlide - (a.options.slidesToShow / 2 + 1)), o = a.options.slidesToShow / 2 + 1 + 2 + a.currentSlide) : (s = a.options.infinite ? a.options.slidesToShow + a.currentSlide : a.currentSlide, o = Math.ceil(s + a.options.slidesToShow), !0 === a.options.fade && (s > 0 && s--, o <= a.slideCount && o++)), i = a.$slider.find(".slick-slide").slice(s, o), "anticipated" === a.options.lazyLoad)
                for (var r = s - 1, l = o, d = a.$slider.find(".slick-slide"), c = 0; c < a.options.slidesToScroll; c++) r < 0 && (r = a.slideCount - 1), i = i.add(d.eq(r)), i = i.add(d.eq(l)), r--, l++;
            t(i), a.slideCount <= a.options.slidesToShow ? (n = a.$slider.find(".slick-slide"), t(n)) : a.currentSlide >= a.slideCount - a.options.slidesToShow ? (n = a.$slider.find(".slick-cloned").slice(0, a.options.slidesToShow), t(n)) : 0 === a.currentSlide && (n = a.$slider.find(".slick-cloned").slice(-1 * a.options.slidesToShow), t(n))
        }, t.prototype.loadSlider = function () {
            var e = this;
            e.setPosition(), e.$slideTrack.css({
                opacity: 1
            }), e.$slider.removeClass("slick-loading"), e.initUI(), "progressive" === e.options.lazyLoad && e.progressiveLazyLoad()
        }, t.prototype.next = t.prototype.slickNext = function () {
            this.changeSlide({
                data: {
                    message: "next"
                }
            })
        }, t.prototype.orientationChange = function () {
            var e = this;
            e.checkResponsive(), e.setPosition()
        }, t.prototype.pause = t.prototype.slickPause = function () {
            var e = this;
            e.autoPlayClear(), e.paused = !0
        }, t.prototype.play = t.prototype.slickPlay = function () {
            var e = this;
            e.autoPlay(), e.options.autoplay = !0, e.paused = !1, e.focussed = !1, e.interrupted = !1
        }, t.prototype.postSlide = function (t) {
            var i = this;
            if (!i.unslicked && (i.$slider.trigger("afterChange", [i, t]), i.animating = !1, i.slideCount > i.options.slidesToShow && i.setPosition(), i.swipeLeft = null, i.options.autoplay && i.autoPlay(), !0 === i.options.accessibility && (i.initADA(), i.options.focusOnChange))) {
                e(i.$slides.get(i.currentSlide)).attr("tabindex", 0).focus()
            }
        }, t.prototype.prev = t.prototype.slickPrev = function () {
            this.changeSlide({
                data: {
                    message: "previous"
                }
            })
        }, t.prototype.preventDefault = function (e) {
            e.preventDefault()
        }, t.prototype.progressiveLazyLoad = function (t) {
            t = t || 1;
            var i, n, s, o, a, r = this,
                l = e("img[data-lazy]", r.$slider);
            l.length ? (i = l.first(), n = i.attr("data-lazy"), s = i.attr("data-srcset"), o = i.attr("data-sizes") || r.$slider.attr("data-sizes"), a = document.createElement("img"), a.onload = function () {
                s && (i.attr("srcset", s), o && i.attr("sizes", o)), i.attr("src", n).removeAttr("data-lazy data-srcset data-sizes").removeClass("slick-loading"), !0 === r.options.adaptiveHeight && r.setPosition(), r.$slider.trigger("lazyLoaded", [r, i, n]), r.progressiveLazyLoad()
            }, a.onerror = function () {
                t < 3 ? setTimeout(function () {
                    r.progressiveLazyLoad(t + 1)
                }, 500) : (i.removeAttr("data-lazy").removeClass("slick-loading").addClass("slick-lazyload-error"), r.$slider.trigger("lazyLoadError", [r, i, n]), r.progressiveLazyLoad())
            }, a.src = n) : r.$slider.trigger("allImagesLoaded", [r])
        }, t.prototype.refresh = function (t) {
            var i, n, s = this;
            n = s.slideCount - s.options.slidesToShow, !s.options.infinite && s.currentSlide > n && (s.currentSlide = n), s.slideCount <= s.options.slidesToShow && (s.currentSlide = 0), i = s.currentSlide, s.destroy(!0), e.extend(s, s.initials, {
                currentSlide: i
            }), s.init(), t || s.changeSlide({
                data: {
                    message: "index",
                    index: i
                }
            }, !1)
        }, t.prototype.registerBreakpoints = function () {
            var t, i, n, s = this,
                o = s.options.responsive || null;
            if ("array" === e.type(o) && o.length) {
                s.respondTo = s.options.respondTo || "window";
                for (t in o)
                    if (n = s.breakpoints.length - 1, o.hasOwnProperty(t)) {
                        for (i = o[t].breakpoint; n >= 0;) s.breakpoints[n] && s.breakpoints[n] === i && s.breakpoints.splice(n, 1), n--;
                        s.breakpoints.push(i), s.breakpointSettings[i] = o[t].settings
                    } s.breakpoints.sort(function (e, t) {
                    return s.options.mobileFirst ? e - t : t - e
                })
            }
        }, t.prototype.reinit = function () {
            var t = this;
            t.$slides = t.$slideTrack.children(t.options.slide).addClass("slick-slide"), t.slideCount = t.$slides.length, t.currentSlide >= t.slideCount && 0 !== t.currentSlide && (t.currentSlide = t.currentSlide - t.options.slidesToScroll), t.slideCount <= t.options.slidesToShow && (t.currentSlide = 0), t.registerBreakpoints(), t.setProps(), t.setupInfinite(), t.buildArrows(), t.updateArrows(), t.initArrowEvents(), t.buildDots(), t.updateDots(), t.initDotEvents(), t.cleanUpSlideEvents(), t.initSlideEvents(), t.checkResponsive(!1, !0), !0 === t.options.focusOnSelect && e(t.$slideTrack).children().on("click.slick", t.selectHandler), t.setSlideClasses("number" == typeof t.currentSlide ? t.currentSlide : 0), t.setPosition(), t.focusHandler(), t.paused = !t.options.autoplay, t.autoPlay(), t.$slider.trigger("reInit", [t])
        }, t.prototype.resize = function () {
            var t = this;
            e(window).width() !== t.windowWidth && (clearTimeout(t.windowDelay), t.windowDelay = window.setTimeout(function () {
                t.windowWidth = e(window).width(), t.checkResponsive(), t.unslicked || t.setPosition()
            }, 50))
        }, t.prototype.removeSlide = t.prototype.slickRemove = function (e, t, i) {
            var n = this;
            if ("boolean" == typeof e ? (t = e, e = !0 === t ? 0 : n.slideCount - 1) : e = !0 === t ? --e : e, n.slideCount < 1 || e < 0 || e > n.slideCount - 1) return !1;
            n.unload(), !0 === i ? n.$slideTrack.children().remove() : n.$slideTrack.children(this.options.slide).eq(e).remove(), n.$slides = n.$slideTrack.children(this.options.slide), n.$slideTrack.children(this.options.slide).detach(), n.$slideTrack.append(n.$slides), n.$slidesCache = n.$slides, n.reinit()
        }, t.prototype.setCSS = function (e) {
            var t, i, n = this,
                s = {};
            !0 === n.options.rtl && (e = -e), t = "left" == n.positionProp ? Math.ceil(e) + "px" : "0px", i = "top" == n.positionProp ? Math.ceil(e) + "px" : "0px", s[n.positionProp] = e, !1 === n.transformsEnabled ? n.$slideTrack.css(s) : (s = {}, !1 === n.cssTransitions ? (s[n.animType] = "translate(" + t + ", " + i + ")", n.$slideTrack.css(s)) : (s[n.animType] = "translate3d(" + t + ", " + i + ", 0px)", n.$slideTrack.css(s)))
        }, t.prototype.setDimensions = function () {
            var e = this;
            !1 === e.options.vertical ? !0 === e.options.centerMode && e.$list.css({
                padding: "0px " + e.options.centerPadding
            }) : (e.$list.height(e.$slides.first().outerHeight(!0) * e.options.slidesToShow), !0 === e.options.centerMode && e.$list.css({
                padding: e.options.centerPadding + " 0px"
            })), e.listWidth = e.$list.width(), e.listHeight = e.$list.height(), !1 === e.options.vertical && !1 === e.options.variableWidth ? (e.slideWidth = Math.ceil(e.listWidth / e.options.slidesToShow), e.$slideTrack.width(Math.ceil(e.slideWidth * e.$slideTrack.children(".slick-slide").length))) : !0 === e.options.variableWidth ? e.$slideTrack.width(5e3 * e.slideCount) : (e.slideWidth = Math.ceil(e.listWidth), e.$slideTrack.height(Math.ceil(e.$slides.first().outerHeight(!0) * e.$slideTrack.children(".slick-slide").length)));
            var t = e.$slides.first().outerWidth(!0) - e.$slides.first().width();
            !1 === e.options.variableWidth && e.$slideTrack.children(".slick-slide").width(e.slideWidth - t)
        }, t.prototype.setFade = function () {
            var t, i = this;
            i.$slides.each(function (n, s) {
                t = i.slideWidth * n * -1, !0 === i.options.rtl ? e(s).css({
                    position: "relative",
                    right: t,
                    top: 0,
                    zIndex: i.options.zIndex - 2,
                    opacity: 0
                }) : e(s).css({
                    position: "relative",
                    left: t,
                    top: 0,
                    zIndex: i.options.zIndex - 2,
                    opacity: 0
                })
            }), i.$slides.eq(i.currentSlide).css({
                zIndex: i.options.zIndex - 1,
                opacity: 1
            })
        }, t.prototype.setHeight = function () {
            var e = this;
            if (1 === e.options.slidesToShow && !0 === e.options.adaptiveHeight && !1 === e.options.vertical) {
                var t = e.$slides.eq(e.currentSlide).outerHeight(!0);
                e.$list.css("height", t)
            }
        }, t.prototype.setOption = t.prototype.slickSetOption = function () {
            var t, i, n, s, o, a = this,
                r = !1;
            if ("object" === e.type(arguments[0]) ? (n = arguments[0], r = arguments[1], o = "multiple") : "string" === e.type(arguments[0]) && (n = arguments[0], s = arguments[1], r = arguments[2], "responsive" === arguments[0] && "array" === e.type(arguments[1]) ? o = "responsive" : void 0 !== arguments[1] && (o = "single")), "single" === o) a.options[n] = s;
            else if ("multiple" === o) e.each(n, function (e, t) {
                a.options[e] = t
            });
            else if ("responsive" === o)
                for (i in s)
                    if ("array" !== e.type(a.options.responsive)) a.options.responsive = [s[i]];
                    else {
                        for (t = a.options.responsive.length - 1; t >= 0;) a.options.responsive[t].breakpoint === s[i].breakpoint && a.options.responsive.splice(t, 1), t--;
                        a.options.responsive.push(s[i])
                    } r && (a.unload(), a.reinit())
        }, t.prototype.setPosition = function () {
            var e = this;
            e.setDimensions(), e.setHeight(), !1 === e.options.fade ? e.setCSS(e.getLeft(e.currentSlide)) : e.setFade(), e.$slider.trigger("setPosition", [e])
        }, t.prototype.setProps = function () {
            var e = this,
                t = document.body.style;
            e.positionProp = !0 === e.options.vertical ? "top" : "left", "top" === e.positionProp ? e.$slider.addClass("slick-vertical") : e.$slider.removeClass("slick-vertical"), void 0 === t.WebkitTransition && void 0 === t.MozTransition && void 0 === t.msTransition || !0 === e.options.useCSS && (e.cssTransitions = !0), e.options.fade && ("number" == typeof e.options.zIndex ? e.options.zIndex < 3 && (e.options.zIndex = 3) : e.options.zIndex = e.defaults.zIndex), void 0 !== t.OTransform && (e.animType = "OTransform", e.transformType = "-o-transform", e.transitionType = "OTransition", void 0 === t.perspectiveProperty && void 0 === t.webkitPerspective && (e.animType = !1)), void 0 !== t.MozTransform && (e.animType = "MozTransform", e.transformType = "-moz-transform", e.transitionType = "MozTransition", void 0 === t.perspectiveProperty && void 0 === t.MozPerspective && (e.animType = !1)), void 0 !== t.webkitTransform && (e.animType = "webkitTransform", e.transformType = "-webkit-transform", e.transitionType = "webkitTransition", void 0 === t.perspectiveProperty && void 0 === t.webkitPerspective && (e.animType = !1)), void 0 !== t.msTransform && (e.animType = "msTransform", e.transformType = "-ms-transform", e.transitionType = "msTransition", void 0 === t.msTransform && (e.animType = !1)), void 0 !== t.transform && !1 !== e.animType && (e.animType = "transform", e.transformType = "transform", e.transitionType = "transition"), e.transformsEnabled = e.options.useTransform && null !== e.animType && !1 !== e.animType
        }, t.prototype.setSlideClasses = function (e) {
            var t, i, n, s, o = this;
            if (i = o.$slider.find(".slick-slide").removeClass("slick-active slick-center slick-current").attr("aria-hidden", "true"), o.$slides.eq(e).addClass("slick-current"), !0 === o.options.centerMode) {
                var a = o.options.slidesToShow % 2 == 0 ? 1 : 0;
                t = Math.floor(o.options.slidesToShow / 2), !0 === o.options.infinite && (e >= t && e <= o.slideCount - 1 - t ? o.$slides.slice(e - t + a, e + t + 1).addClass("slick-active").attr("aria-hidden", "false") : (n = o.options.slidesToShow + e, i.slice(n - t + 1 + a, n + t + 2).addClass("slick-active").attr("aria-hidden", "false")), 0 === e ? i.eq(i.length - 1 - o.options.slidesToShow).addClass("slick-center") : e === o.slideCount - 1 && i.eq(o.options.slidesToShow).addClass("slick-center")), o.$slides.eq(e).addClass("slick-center")
            } else e >= 0 && e <= o.slideCount - o.options.slidesToShow ? o.$slides.slice(e, e + o.options.slidesToShow).addClass("slick-active").attr("aria-hidden", "false") : i.length <= o.options.slidesToShow ? i.addClass("slick-active").attr("aria-hidden", "false") : (s = o.slideCount % o.options.slidesToShow, n = !0 === o.options.infinite ? o.options.slidesToShow + e : e, o.options.slidesToShow == o.options.slidesToScroll && o.slideCount - e < o.options.slidesToShow ? i.slice(n - (o.options.slidesToShow - s), n + s).addClass("slick-active").attr("aria-hidden", "false") : i.slice(n, n + o.options.slidesToShow).addClass("slick-active").attr("aria-hidden", "false"));
            "ondemand" !== o.options.lazyLoad && "anticipated" !== o.options.lazyLoad || o.lazyLoad()
        }, t.prototype.setupInfinite = function () {
            var t, i, n, s = this;
            if (!0 === s.options.fade && (s.options.centerMode = !1), !0 === s.options.infinite && !1 === s.options.fade && (i = null, s.slideCount > s.options.slidesToShow)) {
                for (n = !0 === s.options.centerMode ? s.options.slidesToShow + 1 : s.options.slidesToShow, t = s.slideCount; t > s.slideCount - n; t -= 1) i = t - 1, e(s.$slides[i]).clone(!0).attr("id", "").attr("data-slick-index", i - s.slideCount).prependTo(s.$slideTrack).addClass("slick-cloned");
                for (t = 0; t < n + s.slideCount; t += 1) i = t, e(s.$slides[i]).clone(!0).attr("id", "").attr("data-slick-index", i + s.slideCount).appendTo(s.$slideTrack).addClass("slick-cloned");
                s.$slideTrack.find(".slick-cloned").find("[id]").each(function () {
                    e(this).attr("id", "")
                })
            }
        }, t.prototype.interrupt = function (e) {
            var t = this;
            e || t.autoPlay(), t.interrupted = e
        }, t.prototype.selectHandler = function (t) {
            var i = this,
                n = e(t.target).is(".slick-slide") ? e(t.target) : e(t.target).parents(".slick-slide"),
                s = parseInt(n.attr("data-slick-index"));
            if (s || (s = 0), i.slideCount <= i.options.slidesToShow) return void i.slideHandler(s, !1, !0);
            i.slideHandler(s)
        }, t.prototype.slideHandler = function (e, t, i) {
            var n, s, o, a, r, l = null,
                d = this;
            if (t = t || !1, !(!0 === d.animating && !0 === d.options.waitForAnimate || !0 === d.options.fade && d.currentSlide === e)) {
                if (!1 === t && d.asNavFor(e), n = e, l = d.getLeft(n), a = d.getLeft(d.currentSlide), d.currentLeft = null === d.swipeLeft ? a : d.swipeLeft, !1 === d.options.infinite && !1 === d.options.centerMode && (e < 0 || e > d.getDotCount() * d.options.slidesToScroll)) return void(!1 === d.options.fade && (n = d.currentSlide, !0 !== i && d.slideCount > d.options.slidesToShow ? d.animateSlide(a, function () {
                    d.postSlide(n)
                }) : d.postSlide(n)));
                if (!1 === d.options.infinite && !0 === d.options.centerMode && (e < 0 || e > d.slideCount - d.options.slidesToScroll)) return void(!1 === d.options.fade && (n = d.currentSlide, !0 !== i && d.slideCount > d.options.slidesToShow ? d.animateSlide(a, function () {
                    d.postSlide(n)
                }) : d.postSlide(n)));
                if (d.options.autoplay && clearInterval(d.autoPlayTimer), s = n < 0 ? d.slideCount % d.options.slidesToScroll != 0 ? d.slideCount - d.slideCount % d.options.slidesToScroll : d.slideCount + n : n >= d.slideCount ? d.slideCount % d.options.slidesToScroll != 0 ? 0 : n - d.slideCount : n, d.animating = !0, d.$slider.trigger("beforeChange", [d, d.currentSlide, s]), o = d.currentSlide, d.currentSlide = s, d.setSlideClasses(d.currentSlide), d.options.asNavFor && (r = d.getNavTarget(), r = r.slick("getSlick"), r.slideCount <= r.options.slidesToShow && r.setSlideClasses(d.currentSlide)), d.updateDots(), d.updateArrows(), !0 === d.options.fade) return !0 !== i ? (d.fadeSlideOut(o), d.fadeSlide(s, function () {
                    d.postSlide(s)
                })) : d.postSlide(s), void d.animateHeight();
                !0 !== i && d.slideCount > d.options.slidesToShow ? d.animateSlide(l, function () {
                    d.postSlide(s)
                }) : d.postSlide(s)
            }
        }, t.prototype.startLoad = function () {
            var e = this;
            !0 === e.options.arrows && e.slideCount > e.options.slidesToShow && (e.$prevArrow.hide(), e.$nextArrow.hide()), !0 === e.options.dots && e.slideCount > e.options.slidesToShow && e.$dots.hide(), e.$slider.addClass("slick-loading")
        }, t.prototype.swipeDirection = function () {
            var e, t, i, n, s = this;
            return e = s.touchObject.startX - s.touchObject.curX, t = s.touchObject.startY - s.touchObject.curY, i = Math.atan2(t, e), n = Math.round(180 * i / Math.PI), n < 0 && (n = 360 - Math.abs(n)), n <= 45 && n >= 0 ? !1 === s.options.rtl ? "left" : "right" : n <= 360 && n >= 315 ? !1 === s.options.rtl ? "left" : "right" : n >= 135 && n <= 225 ? !1 === s.options.rtl ? "right" : "left" : !0 === s.options.verticalSwiping ? n >= 35 && n <= 135 ? "down" : "up" : "vertical"
        }, t.prototype.swipeEnd = function (e) {
            var t, i, n = this;
            if (n.dragging = !1, n.swiping = !1, n.scrolling) return n.scrolling = !1, !1;
            if (n.interrupted = !1, n.shouldClick = !(n.touchObject.swipeLength > 10), void 0 === n.touchObject.curX) return !1;
            if (!0 === n.touchObject.edgeHit && n.$slider.trigger("edge", [n, n.swipeDirection()]), n.touchObject.swipeLength >= n.touchObject.minSwipe) {
                switch (i = n.swipeDirection()) {
                    case "left":
                    case "down":
                        t = n.options.swipeToSlide ? n.checkNavigable(n.currentSlide + n.getSlideCount()) : n.currentSlide + n.getSlideCount(), n.currentDirection = 0;
                        break;
                    case "right":
                    case "up":
                        t = n.options.swipeToSlide ? n.checkNavigable(n.currentSlide - n.getSlideCount()) : n.currentSlide - n.getSlideCount(), n.currentDirection = 1
                }
                "vertical" != i && (n.slideHandler(t), n.touchObject = {}, n.$slider.trigger("swipe", [n, i]))
            } else n.touchObject.startX !== n.touchObject.curX && (n.slideHandler(n.currentSlide), n.touchObject = {})
        }, t.prototype.swipeHandler = function (e) {
            var t = this;
            if (!(!1 === t.options.swipe || "ontouchend" in document && !1 === t.options.swipe || !1 === t.options.draggable && -1 !== e.type.indexOf("mouse"))) switch (t.touchObject.fingerCount = e.originalEvent && void 0 !== e.originalEvent.touches ? e.originalEvent.touches.length : 1, t.touchObject.minSwipe = t.listWidth / t.options.touchThreshold, !0 === t.options.verticalSwiping && (t.touchObject.minSwipe = t.listHeight / t.options.touchThreshold), e.data.action) {
                case "start":
                    t.swipeStart(e);
                    break;
                case "move":
                    t.swipeMove(e);
                    break;
                case "end":
                    t.swipeEnd(e)
            }
        }, t.prototype.swipeMove = function (e) {
            var t, i, n, s, o, a, r = this;
            return o = void 0 !== e.originalEvent ? e.originalEvent.touches : null, !(!r.dragging || r.scrolling || o && 1 !== o.length) && (t = r.getLeft(r.currentSlide), r.touchObject.curX = void 0 !== o ? o[0].pageX : e.clientX, r.touchObject.curY = void 0 !== o ? o[0].pageY : e.clientY, r.touchObject.swipeLength = Math.round(Math.sqrt(Math.pow(r.touchObject.curX - r.touchObject.startX, 2))), a = Math.round(Math.sqrt(Math.pow(r.touchObject.curY - r.touchObject.startY, 2))), !r.options.verticalSwiping && !r.swiping && a > 4 ? (r.scrolling = !0, !1) : (!0 === r.options.verticalSwiping && (r.touchObject.swipeLength = a), i = r.swipeDirection(), void 0 !== e.originalEvent && r.touchObject.swipeLength > 4 && (r.swiping = !0, e.preventDefault()), s = (!1 === r.options.rtl ? 1 : -1) * (r.touchObject.curX > r.touchObject.startX ? 1 : -1), !0 === r.options.verticalSwiping && (s = r.touchObject.curY > r.touchObject.startY ? 1 : -1), n = r.touchObject.swipeLength, r.touchObject.edgeHit = !1, !1 === r.options.infinite && (0 === r.currentSlide && "right" === i || r.currentSlide >= r.getDotCount() && "left" === i) && (n = r.touchObject.swipeLength * r.options.edgeFriction, r.touchObject.edgeHit = !0), !1 === r.options.vertical ? r.swipeLeft = t + n * s : r.swipeLeft = t + n * (r.$list.height() / r.listWidth) * s, !0 === r.options.verticalSwiping && (r.swipeLeft = t + n * s), !0 !== r.options.fade && !1 !== r.options.touchMove && (!0 === r.animating ? (r.swipeLeft = null, !1) : void r.setCSS(r.swipeLeft))))
        }, t.prototype.swipeStart = function (e) {
            var t, i = this;
            if (i.interrupted = !0, 1 !== i.touchObject.fingerCount || i.slideCount <= i.options.slidesToShow) return i.touchObject = {}, !1;
            void 0 !== e.originalEvent && void 0 !== e.originalEvent.touches && (t = e.originalEvent.touches[0]), i.touchObject.startX = i.touchObject.curX = void 0 !== t ? t.pageX : e.clientX, i.touchObject.startY = i.touchObject.curY = void 0 !== t ? t.pageY : e.clientY, i.dragging = !0
        }, t.prototype.unfilterSlides = t.prototype.slickUnfilter = function () {
            var e = this;
            null !== e.$slidesCache && (e.unload(), e.$slideTrack.children(this.options.slide).detach(), e.$slidesCache.appendTo(e.$slideTrack), e.reinit())
        }, t.prototype.unload = function () {
            var t = this;
            e(".slick-cloned", t.$slider).remove(), t.$dots && t.$dots.remove(), t.$prevArrow && t.htmlExpr.test(t.options.prevArrow) && t.$prevArrow.remove(), t.$nextArrow && t.htmlExpr.test(t.options.nextArrow) && t.$nextArrow.remove(), t.$slides.removeClass("slick-slide slick-active slick-visible slick-current").attr("aria-hidden", "true").css("width", "")
        }, t.prototype.unslick = function (e) {
            var t = this;
            t.$slider.trigger("unslick", [t, e]), t.destroy()
        }, t.prototype.updateArrows = function () {
            var e = this;
            Math.floor(e.options.slidesToShow / 2), !0 === e.options.arrows && e.slideCount > e.options.slidesToShow && !e.options.infinite && (e.$prevArrow.removeClass("slick-disabled").attr("aria-disabled", "false"), e.$nextArrow.removeClass("slick-disabled").attr("aria-disabled", "false"), 0 === e.currentSlide ? (e.$prevArrow.addClass("slick-disabled").attr("aria-disabled", "true"), e.$nextArrow.removeClass("slick-disabled").attr("aria-disabled", "false")) : e.currentSlide >= e.slideCount - e.options.slidesToShow && !1 === e.options.centerMode ? (e.$nextArrow.addClass("slick-disabled").attr("aria-disabled", "true"), e.$prevArrow.removeClass("slick-disabled").attr("aria-disabled", "false")) : e.currentSlide >= e.slideCount - 1 && !0 === e.options.centerMode && (e.$nextArrow.addClass("slick-disabled").attr("aria-disabled", "true"), e.$prevArrow.removeClass("slick-disabled").attr("aria-disabled", "false")))
        }, t.prototype.updateDots = function () {
            var e = this;
            null !== e.$dots && (e.$dots.find("li").removeClass("slick-active").end(), e.$dots.find("li").eq(Math.floor(e.currentSlide / e.options.slidesToScroll)).addClass("slick-active"))
        }, t.prototype.visibility = function () {
            var e = this;
            e.options.autoplay && (document[e.hidden] ? e.interrupted = !0 : e.interrupted = !1)
        }, e.fn.slick = function () {
            var e, i, n = this,
                s = arguments[0],
                o = Array.prototype.slice.call(arguments, 1),
                a = n.length;
            for (e = 0; e < a; e++)
                if ("object" == typeof s || void 0 === s ? n[e].slick = new t(n[e], s) : i = n[e].slick[s].apply(n[e].slick, o), void 0 !== i) return i;
            return n
        }
    }),
    function () {
        function e() {
            A.keyboardSupport && v("keydown", o)
        }

        function t() {
            if (!H && document.body) {
                H = !0;
                var t = document.body,
                    i = document.documentElement,
                    n = window.innerHeight,
                    s = t.scrollHeight;
                if (X = document.compatMode.indexOf("CSS") >= 0 ? i : t, E = t, e(), top != self) D = !0;
                else if (se && s > n && (t.offsetHeight <= n || i.offsetHeight <= n)) {
                    var o = document.createElement("div");
                    o.style.cssText = "position:absolute; z-index:-10000; top:0; left:0; right:0; height:" + X.scrollHeight + "px", document.body.appendChild(o);
                    var a;
                    M = function () {
                        a || (a = setTimeout(function () {
                            O || (o.style.height = "0", o.style.height = X.scrollHeight + "px", a = null)
                        }, 500))
                    }, setTimeout(M, 10), v("resize", M);
                    var r = {
                        attributes: !0,
                        childList: !0,
                        characterData: !1
                    };
                    if ($ = new K(M), $.observe(t, r), X.offsetHeight <= n) {
                        var l = document.createElement("div");
                        l.style.clear = "both", t.appendChild(l)
                    }
                }
                A.fixedBackground || O || (t.style.backgroundAttachment = "scroll", i.style.backgroundAttachment = "scroll")
            }
        }

        function i() {
            $ && $.disconnect(), m(le, s), m("mousedown", a), m("keydown", o), m("resize", M), m("load", t)
        }

        function n(e, t, i) {
            if (w(t, i), 1 != A.accelerationMax) {
                var n = Date.now(),
                    s = n - W;
                if (s < A.accelerationDelta) {
                    var o = (1 + 50 / s) / 2;
                    o > 1 && (o = Math.min(o, A.accelerationMax), t *= o, i *= o)
                }
                W = Date.now()
            }
            if (j.push({
                    x: t,
                    y: i,
                    lastX: t < 0 ? .99 : -.99,
                    lastY: i < 0 ? .99 : -.99,
                    start: Date.now()
                }), !F) {
                var a = Q(),
                    r = e === a || e === document.body;
                null == e.$scrollBehavior && f(e) && (e.$scrollBehavior = e.style.scrollBehavior, e.style.scrollBehavior = "auto");
                var l = function (n) {
                    for (var s = Date.now(), o = 0, a = 0, d = 0; d < j.length; d++) {
                        var c = j[d],
                            p = s - c.start,
                            u = p >= A.animationTime,
                            h = u ? 1 : p / A.animationTime;
                        A.pulseAlgorithm && (h = C(h));
                        var f = c.x * h - c.lastX >> 0,
                            v = c.y * h - c.lastY >> 0;
                        o += f, a += v, c.lastX += f, c.lastY += v, u && (j.splice(d, 1), d--)
                    }
                    r ? window.scrollBy(o, a) : (o && (e.scrollLeft += o), a && (e.scrollTop += a)), t || i || (j = []), j.length ? U(l, e, 1e3 / A.frameRate + 1) : (F = !1, null != e.$scrollBehavior && (e.style.scrollBehavior = e.$scrollBehavior, e.$scrollBehavior = null))
                };
                U(l, e, 0), F = !0
            }
        }

        function s(e) {
            H || t();
            var i = e.target;
            if (e.defaultPrevented || e.ctrlKey) return !0;
            if (g(E, "embed") || g(i, "embed") && /\.pdf/i.test(i.src) || g(E, "object") || i.shadowRoot) return !0;
            var s = -e.wheelDeltaX || e.deltaX || 0,
                o = -e.wheelDeltaY || e.deltaY || 0;
            N && (e.wheelDeltaX && b(e.wheelDeltaX, 120) && (s = e.wheelDeltaX / Math.abs(e.wheelDeltaX) * -120), e.wheelDeltaY && b(e.wheelDeltaY, 120) && (o = e.wheelDeltaY / Math.abs(e.wheelDeltaY) * -120)), s || o || (o = -e.wheelDelta || 0), 1 === e.deltaMode && (s *= 40, o *= 40);
            var a = c(i);
            return a ? !!y(o) || (Math.abs(s) > 1.2 && (s *= A.stepSize / 120), Math.abs(o) > 1.2 && (o *= A.stepSize / 120), n(a, s, o), e.preventDefault(), void r()) : !D || !ee || (Object.defineProperty(e, "target", {
                value: window.frameElement
            }), parent.wheel(e))
        }

        function o(e) {
            var t = e.target,
                i = e.ctrlKey || e.altKey || e.metaKey || e.shiftKey && e.keyCode !== B.spacebar;
            document.body.contains(E) || (E = document.activeElement);
            var s = /^(textarea|select|embed|object)$/i,
                o = /^(button|submit|radio|checkbox|file|color|image)$/i;
            if (e.defaultPrevented || s.test(t.nodeName) || g(t, "input") && !o.test(t.type) || g(E, "video") || x(e) || t.isContentEditable || i) return !0;
            if ((g(t, "button") || g(t, "input") && o.test(t.type)) && e.keyCode === B.spacebar) return !0;
            if (g(t, "input") && "radio" == t.type && R[e.keyCode]) return !0;
            var a, l = 0,
                d = 0,
                p = c(E);
            if (!p) return !D || !ee || parent.keydown(e);
            var u = p.clientHeight;
            switch (p == document.body && (u = window.innerHeight), e.keyCode) {
                case B.up:
                    d = -A.arrowScroll;
                    break;
                case B.down:
                    d = A.arrowScroll;
                    break;
                case B.spacebar:
                    a = e.shiftKey ? 1 : -1, d = -a * u * .9;
                    break;
                case B.pageup:
                    d = .9 * -u;
                    break;
                case B.pagedown:
                    d = .9 * u;
                    break;
                case B.home:
                    p == document.body && document.scrollingElement && (p = document.scrollingElement), d = -p.scrollTop;
                    break;
                case B.end:
                    var h = p.scrollHeight - p.scrollTop,
                        f = h - u;
                    d = f > 0 ? f + 10 : 0;
                    break;
                case B.left:
                    l = -A.arrowScroll;
                    break;
                case B.right:
                    l = A.arrowScroll;
                    break;
                default:
                    return !0
            }
            n(p, l, d), e.preventDefault(), r()
        }

        function a(e) {
            E = e.target
        }

        function r() {
            clearTimeout(z), z = setInterval(function () {
                G = V = _ = {}
            }, 1e3)
        }

        function l(e, t, i) {
            for (var n = i ? G : V, s = e.length; s--;) n[q(e[s])] = t;
            return t
        }

        function d(e, t) {
            return (t ? G : V)[q(e)]
        }

        function c(e) {
            var t = [],
                i = document.body,
                n = X.scrollHeight;
            do {
                var s = d(e, !1);
                if (s) return l(t, s);
                if (t.push(e), n === e.scrollHeight) {
                    var o = u(X) && u(i),
                        a = o || h(X);
                    if (D && p(X) || !D && a) return l(t, Q())
                } else if (p(e) && h(e)) return l(t, e)
            } while (e = e.parentElement)
        }

        function p(e) {
            return e.clientHeight + 10 < e.scrollHeight
        }

        function u(e) {
            return "hidden" !== getComputedStyle(e, "").getPropertyValue("overflow-y")
        }

        function h(e) {
            var t = getComputedStyle(e, "").getPropertyValue("overflow-y");
            return "scroll" === t || "auto" === t
        }

        function f(e) {
            var t = q(e);
            if (null == _[t]) {
                var i = getComputedStyle(e, "")["scroll-behavior"];
                _[t] = "smooth" == i
            }
            return _[t]
        }

        function v(e, t, i) {
            window.addEventListener(e, t, i || !1)
        }

        function m(e, t, i) {
            window.removeEventListener(e, t, i || !1)
        }

        function g(e, t) {
            return e && (e.nodeName || "").toLowerCase() === t.toLowerCase()
        }

        function w(e, t) {
            e = e > 0 ? 1 : -1, t = t > 0 ? 1 : -1, I.x === e && I.y === t || (I.x = e, I.y = t, j = [], W = 0)
        }

        function y(e) {
            if (e) {
                Y.length || (Y = [e, e, e]), e = Math.abs(e), Y.push(e), Y.shift(), clearTimeout(P), P = setTimeout(function () {
                    try {
                        localStorage.SS_deltaBuffer = Y.join(",")
                    } catch (e) {}
                }, 1e3);
                var t = e > 120 && T(e);
                return !T(120) && !T(100) && !t
            }
        }

        function b(e, t) {
            return Math.floor(e / t) == e / t
        }

        function T(e) {
            return b(Y[0], e) && b(Y[1], e) && b(Y[2], e)
        }

        function x(e) {
            var t = e.target,
                i = !1;
            if (-1 != document.URL.indexOf("www.youtube.com/watch"))
                do {
                    if (i = t.classList && t.classList.contains("html5-video-controls")) break
                } while (t = t.parentNode);
            return i
        }

        function S(e) {
            var t, i, n;
            return e *= A.pulseScale, e < 1 ? t = e - (1 - Math.exp(-e)) : (i = Math.exp(-1), e -= 1, n = 1 - Math.exp(-e), t = i + n * (1 - i)), t * A.pulseNormalize
        }

        function C(e) {
            return e >= 1 ? 1 : e <= 0 ? 0 : (1 == A.pulseNormalize && (A.pulseNormalize /= S(1)), S(e))
        }

        function k(e) {
            for (var t in e) L.hasOwnProperty(t) && (A[t] = e[t])
        }
        var E, $, M, P, z, L = {
                frameRate: 150,
                animationTime: 700,
                stepSize: 100,
                pulseAlgorithm: !0,
                pulseScale: 4,
                pulseNormalize: 1,
                accelerationDelta: 50,
                accelerationMax: 3,
                keyboardSupport: !0,
                arrowScroll: 50,
                fixedBackground: !0,
                excluded: ""
            },
            A = L,
            O = !1,
            D = !1,
            I = {
                x: 0,
                y: 0
            },
            H = !1,
            X = document.documentElement,
            Y = [],
            N = /^Mac/.test(navigator.platform),
            B = {
                left: 37,
                up: 38,
                right: 39,
                down: 40,
                spacebar: 32,
                pageup: 33,
                pagedown: 34,
                end: 35,
                home: 36
            },
            R = {
                37: 1,
                38: 1,
                39: 1,
                40: 1
            },
            j = [],
            F = !1,
            W = Date.now(),
            q = function () {
                var e = 0;
                return function (t) {
                    return t.uniqueID || (t.uniqueID = e++)
                }
            }(),
            G = {},
            V = {},
            _ = {};
        if (window.localStorage && localStorage.SS_deltaBuffer) try {
            Y = localStorage.SS_deltaBuffer.split(",")
        } catch (e) {}
        var U = function () {
                return window.requestAnimationFrame || window.webkitRequestAnimationFrame || window.mozRequestAnimationFrame || function (e, t, i) {
                    window.setTimeout(e, i || 1e3 / 60)
                }
            }(),
            K = window.MutationObserver || window.WebKitMutationObserver || window.MozMutationObserver,
            Q = function () {
                var e = document.scrollingElement;
                return function () {
                    if (!e) {
                        var t = document.createElement("div");
                        t.style.cssText = "height:10000px;width:1px;", document.body.appendChild(t);
                        var i = document.body.scrollTop;
                        document.documentElement.scrollTop;
                        window.scrollBy(0, 3), e = document.body.scrollTop != i ? document.body : document.documentElement, window.scrollBy(0, -3), document.body.removeChild(t)
                    }
                    return e
                }
            }(),
            Z = window.navigator.userAgent,
            J = /Edge/.test(Z),
            ee = /chrome/i.test(Z) && !J,
            te = /safari/i.test(Z) && !J,
            ie = /mobile/i.test(Z),
            ne = /Windows NT 6.1/i.test(Z) && /rv:11/i.test(Z),
            se = te && (/Version\/8/i.test(Z) || /Version\/9/i.test(Z)),
            oe = (ee || te || ne) && !ie,
            ae = !1;
        try {
            window.addEventListener("test", null, Object.defineProperty({}, "passive", {
                get: function () {
                    ae = !0
                }
            }))
        } catch (e) {}
        var re = !!ae && {
                passive: !1
            },
            le = "onwheel" in document.createElement("div") ? "wheel" : "mousewheel";
        le && oe && (v(le, s, re), v("mousedown", a), v("load", t)), k.destroy = i, window.SmoothScrollOptions && k(window.SmoothScrollOptions), "function" == typeof define && define.amd ? define(function () {
            return k
        }) : "object" == typeof exports ? module.exports = k : window.SmoothScroll = k
    }(),
    function (e, t) {
        "object" == typeof exports && "undefined" != typeof module ? module.exports = t() : "function" == typeof define && define.amd ? define(t) : (e = e || self).Swiper = t()
    }(this, function () {
        "use strict";

        function e(e, t) {
            var i = [],
                n = 0;
            if (e && !t && e instanceof a) return e;
            if (e)
                if ("string" == typeof e) {
                    var r, l, d = e.trim();
                    if (0 <= d.indexOf("<") && 0 <= d.indexOf(">")) {
                        var c = "div";
                        for (0 === d.indexOf("<li") && (c = "ul"), 0 === d.indexOf("<tr") && (c = "tbody"), 0 !== d.indexOf("<td") && 0 !== d.indexOf("<th") || (c = "tr"), 0 === d.indexOf("<tbody") && (c = "table"), 0 === d.indexOf("<option") && (c = "select"), (l = s.createElement(c)).innerHTML = d, n = 0; n < l.childNodes.length; n += 1) i.push(l.childNodes[n])
                    } else
                        for (r = t || "#" !== e[0] || e.match(/[ .<>:~]/) ? (t || s).querySelectorAll(e.trim()) : [s.getElementById(e.trim().split("#")[1])], n = 0; n < r.length; n += 1) r[n] && i.push(r[n])
                } else if (e.nodeType || e === o || e === s) i.push(e);
            else if (0 < e.length && e[0].nodeType)
                for (n = 0; n < e.length; n += 1) i.push(e[n]);
            return new a(i)
        }

        function t(e) {
            for (var t = [], i = 0; i < e.length; i += 1) - 1 === t.indexOf(e[i]) && t.push(e[i]);
            return t
        }

        function i(e) {
            void 0 === e && (e = {});
            var t = this;
            t.params = e, t.eventsListeners = {}, t.params && t.params.on && Object.keys(t.params.on).forEach(function (e) {
                t.on(e, t.params.on[e])
            })
        }

        function n() {
            var e = this,
                t = e.params,
                i = e.el;
            if (!i || 0 !== i.offsetWidth) {
                t.breakpoints && e.setBreakpoint();
                var n = e.allowSlideNext,
                    s = e.allowSlidePrev,
                    o = e.snapGrid;
                if (e.allowSlideNext = !0, e.allowSlidePrev = !0, e.updateSize(), e.updateSlides(), t.freeMode) {
                    var a = Math.min(Math.max(e.translate, e.maxTranslate()), e.minTranslate());
                    e.setTranslate(a), e.updateActiveIndex(), e.updateSlidesClasses(), t.autoHeight && e.updateAutoHeight()
                } else e.updateSlidesClasses(), ("auto" === t.slidesPerView || 1 < t.slidesPerView) && e.isEnd && !e.params.centeredSlides ? e.slideTo(e.slides.length - 1, 0, !1, !0) : e.slideTo(e.activeIndex, 0, !1, !0);
                e.autoplay && e.autoplay.running && e.autoplay.paused && e.autoplay.run(), e.allowSlidePrev = s, e.allowSlideNext = n, e.params.watchOverflow && o !== e.snapGrid && e.checkOverflow()
            }
        }
        var s = "undefined" == typeof document ? {
                body: {},
                addEventListener: function () {},
                removeEventListener: function () {},
                activeElement: {
                    blur: function () {},
                    nodeName: ""
                },
                querySelector: function () {
                    return null
                },
                querySelectorAll: function () {
                    return []
                },
                getElementById: function () {
                    return null
                },
                createEvent: function () {
                    return {
                        initEvent: function () {}
                    }
                },
                createElement: function () {
                    return {
                        children: [],
                        childNodes: [],
                        style: {},
                        setAttribute: function () {},
                        getElementsByTagName: function () {
                            return []
                        }
                    }
                },
                location: {
                    hash: ""
                }
            } : document,
            o = "undefined" == typeof window ? {
                document: s,
                navigator: {
                    userAgent: ""
                },
                location: {},
                history: {},
                CustomEvent: function () {
                    return this
                },
                addEventListener: function () {},
                removeEventListener: function () {},
                getComputedStyle: function () {
                    return {
                        getPropertyValue: function () {
                            return ""
                        }
                    }
                },
                Image: function () {},
                Date: function () {},
                screen: {},
                setTimeout: function () {},
                clearTimeout: function () {}
            } : window,
            a = function (e) {
                for (var t = 0; t < e.length; t += 1) this[t] = e[t];
                return this.length = e.length, this
            };
        e.fn = a.prototype, e.Class = a, e.Dom7 = a;
        var r = {
            addClass: function (e) {
                if (void 0 === e) return this;
                for (var t = e.split(" "), i = 0; i < t.length; i += 1)
                    for (var n = 0; n < this.length; n += 1) void 0 !== this[n] && void 0 !== this[n].classList && this[n].classList.add(t[i]);
                return this
            },
            removeClass: function (e) {
                for (var t = e.split(" "), i = 0; i < t.length; i += 1)
                    for (var n = 0; n < this.length; n += 1) void 0 !== this[n] && void 0 !== this[n].classList && this[n].classList.remove(t[i]);
                return this
            },
            hasClass: function (e) {
                return !!this[0] && this[0].classList.contains(e)
            },
            toggleClass: function (e) {
                for (var t = e.split(" "), i = 0; i < t.length; i += 1)
                    for (var n = 0; n < this.length; n += 1) void 0 !== this[n] && void 0 !== this[n].classList && this[n].classList.toggle(t[i]);
                return this
            },
            attr: function (e, t) {
                var i = arguments;
                if (1 === arguments.length && "string" == typeof e) return this[0] ? this[0].getAttribute(e) : void 0;
                for (var n = 0; n < this.length; n += 1)
                    if (2 === i.length) this[n].setAttribute(e, t);
                    else
                        for (var s in e) this[n][s] = e[s], this[n].setAttribute(s, e[s]);
                return this
            },
            removeAttr: function (e) {
                for (var t = 0; t < this.length; t += 1) this[t].removeAttribute(e);
                return this
            },
            data: function (e, t) {
                var i;
                if (void 0 !== t) {
                    for (var n = 0; n < this.length; n += 1)(i = this[n]).dom7ElementDataStorage || (i.dom7ElementDataStorage = {}), i.dom7ElementDataStorage[e] = t;
                    return this
                }
                if (i = this[0]) {
                    if (i.dom7ElementDataStorage && e in i.dom7ElementDataStorage) return i.dom7ElementDataStorage[e];
                    return i.getAttribute("data-" + e) || void 0
                }
            },
            transform: function (e) {
                for (var t = 0; t < this.length; t += 1) {
                    var i = this[t].style;
                    i.webkitTransform = e, i.transform = e
                }
                return this
            },
            transition: function (e) {
                "string" != typeof e && (e += "ms");
                for (var t = 0; t < this.length; t += 1) {
                    var i = this[t].style;
                    i.webkitTransitionDuration = e, i.transitionDuration = e
                }
                return this
            },
            on: function () {
                function t(t) {
                    var i = t.target;
                    if (i) {
                        var n = t.target.dom7EventData || [];
                        if (n.indexOf(t) < 0 && n.unshift(t), e(i).is(r)) l.apply(i, n);
                        else
                            for (var s = e(i).parents(), o = 0; o < s.length; o += 1) e(s[o]).is(r) && l.apply(s[o], n)
                    }
                }

                function i(e) {
                    var t = e && e.target && e.target.dom7EventData || [];
                    t.indexOf(e) < 0 && t.unshift(e), l.apply(this, t)
                }
                for (var n, s = [], o = arguments.length; o--;) s[o] = arguments[o];
                var a = s[0],
                    r = s[1],
                    l = s[2],
                    d = s[3];
                "function" == typeof s[1] && (a = (n = s)[0], l = n[1], d = n[2], r = void 0), d = d || !1;
                for (var c, p = a.split(" "), u = 0; u < this.length; u += 1) {
                    var h = this[u];
                    if (r)
                        for (c = 0; c < p.length; c += 1) {
                            var f = p[c];
                            h.dom7LiveListeners || (h.dom7LiveListeners = {}), h.dom7LiveListeners[f] || (h.dom7LiveListeners[f] = []), h.dom7LiveListeners[f].push({
                                listener: l,
                                proxyListener: t
                            }), h.addEventListener(f, t, d)
                        } else
                            for (c = 0; c < p.length; c += 1) {
                                var v = p[c];
                                h.dom7Listeners || (h.dom7Listeners = {}), h.dom7Listeners[v] || (h.dom7Listeners[v] = []), h.dom7Listeners[v].push({
                                    listener: l,
                                    proxyListener: i
                                }), h.addEventListener(v, i, d)
                            }
                }
                return this
            },
            off: function () {
                for (var e, t = [], i = arguments.length; i--;) t[i] = arguments[i];
                var n = t[0],
                    s = t[1],
                    o = t[2],
                    a = t[3];
                "function" == typeof t[1] && (n = (e = t)[0], o = e[1], a = e[2], s = void 0), a = a || !1;
                for (var r = n.split(" "), l = 0; l < r.length; l += 1)
                    for (var d = r[l], c = 0; c < this.length; c += 1) {
                        var p = this[c],
                            u = void 0;
                        if (!s && p.dom7Listeners ? u = p.dom7Listeners[d] : s && p.dom7LiveListeners && (u = p.dom7LiveListeners[d]), u && u.length)
                            for (var h = u.length - 1; 0 <= h; h -= 1) {
                                var f = u[h];
                                o && f.listener === o ? (p.removeEventListener(d, f.proxyListener, a), u.splice(h, 1)) : o && f.listener && f.listener.dom7proxy && f.listener.dom7proxy === o ? (p.removeEventListener(d, f.proxyListener, a), u.splice(h, 1)) : o || (p.removeEventListener(d, f.proxyListener, a), u.splice(h, 1))
                            }
                    }
                return this
            },
            trigger: function () {
                for (var e = [], t = arguments.length; t--;) e[t] = arguments[t];
                for (var i = e[0].split(" "), n = e[1], a = 0; a < i.length; a += 1)
                    for (var r = i[a], l = 0; l < this.length; l += 1) {
                        var d = this[l],
                            c = void 0;
                        try {
                            c = new o.CustomEvent(r, {
                                detail: n,
                                bubbles: !0,
                                cancelable: !0
                            })
                        } catch (e) {
                            (c = s.createEvent("Event")).initEvent(r, !0, !0), c.detail = n
                        }
                        d.dom7EventData = e.filter(function (e, t) {
                            return 0 < t
                        }), d.dispatchEvent(c), d.dom7EventData = [], delete d.dom7EventData
                    }
                return this
            },
            transitionEnd: function (e) {
                function t(o) {
                    if (o.target === this)
                        for (e.call(this, o), i = 0; i < n.length; i += 1) s.off(n[i], t)
                }
                var i, n = ["webkitTransitionEnd", "transitionend"],
                    s = this;
                if (e)
                    for (i = 0; i < n.length; i += 1) s.on(n[i], t);
                return this
            },
            outerWidth: function (e) {
                if (0 < this.length) {
                    if (e) {
                        var t = this.styles();
                        return this[0].offsetWidth + parseFloat(t.getPropertyValue("margin-right")) + parseFloat(t.getPropertyValue("margin-left"))
                    }
                    return this[0].offsetWidth
                }
                return null
            },
            outerHeight: function (e) {
                if (0 < this.length) {
                    if (e) {
                        var t = this.styles();
                        return this[0].offsetHeight + parseFloat(t.getPropertyValue("margin-top")) + parseFloat(t.getPropertyValue("margin-bottom"))
                    }
                    return this[0].offsetHeight
                }
                return null
            },
            offset: function () {
                if (0 < this.length) {
                    var e = this[0],
                        t = e.getBoundingClientRect(),
                        i = s.body,
                        n = e.clientTop || i.clientTop || 0,
                        a = e.clientLeft || i.clientLeft || 0,
                        r = e === o ? o.scrollY : e.scrollTop,
                        l = e === o ? o.scrollX : e.scrollLeft;
                    return {
                        top: t.top + r - n,
                        left: t.left + l - a
                    }
                }
                return null
            },
            css: function (e, t) {
                var i;
                if (1 === arguments.length) {
                    if ("string" != typeof e) {
                        for (i = 0; i < this.length; i += 1)
                            for (var n in e) this[i].style[n] = e[n];
                        return this
                    }
                    if (this[0]) return o.getComputedStyle(this[0], null).getPropertyValue(e)
                }
                if (2 !== arguments.length || "string" != typeof e) return this;
                for (i = 0; i < this.length; i += 1) this[i].style[e] = t;
                return this
            },
            each: function (e) {
                if (!e) return this;
                for (var t = 0; t < this.length; t += 1)
                    if (!1 === e.call(this[t], t, this[t])) return this;
                return this
            },
            html: function (e) {
                if (void 0 === e) return this[0] ? this[0].innerHTML : void 0;
                for (var t = 0; t < this.length; t += 1) this[t].innerHTML = e;
                return this
            },
            text: function (e) {
                if (void 0 === e) return this[0] ? this[0].textContent.trim() : null;
                for (var t = 0; t < this.length; t += 1) this[t].textContent = e;
                return this
            },
            is: function (t) {
                var i, n, r = this[0];
                if (!r || void 0 === t) return !1;
                if ("string" == typeof t) {
                    if (r.matches) return r.matches(t);
                    if (r.webkitMatchesSelector) return r.webkitMatchesSelector(t);
                    if (r.msMatchesSelector) return r.msMatchesSelector(t);
                    for (i = e(t), n = 0; n < i.length; n += 1)
                        if (i[n] === r) return !0;
                    return !1
                }
                if (t === s) return r === s;
                if (t === o) return r === o;
                if (t.nodeType || t instanceof a) {
                    for (i = t.nodeType ? [t] : t, n = 0; n < i.length; n += 1)
                        if (i[n] === r) return !0;
                    return !1
                }
                return !1
            },
            index: function () {
                var e, t = this[0];
                if (t) {
                    for (e = 0; null !== (t = t.previousSibling);) 1 === t.nodeType && (e += 1);
                    return e
                }
            },
            eq: function (e) {
                if (void 0 === e) return this;
                var t, i = this.length;
                return new a(i - 1 < e ? [] : e < 0 ? (t = i + e) < 0 ? [] : [this[t]] : [this[e]])
            },
            append: function () {
                for (var e, t = [], i = arguments.length; i--;) t[i] = arguments[i];
                for (var n = 0; n < t.length; n += 1) {
                    e = t[n];
                    for (var o = 0; o < this.length; o += 1)
                        if ("string" == typeof e) {
                            var r = s.createElement("div");
                            for (r.innerHTML = e; r.firstChild;) this[o].appendChild(r.firstChild)
                        } else if (e instanceof a)
                        for (var l = 0; l < e.length; l += 1) this[o].appendChild(e[l]);
                    else this[o].appendChild(e)
                }
                return this
            },
            prepend: function (e) {
                var t, i;
                for (t = 0; t < this.length; t += 1)
                    if ("string" == typeof e) {
                        var n = s.createElement("div");
                        for (n.innerHTML = e, i = n.childNodes.length - 1; 0 <= i; i -= 1) this[t].insertBefore(n.childNodes[i], this[t].childNodes[0])
                    } else if (e instanceof a)
                    for (i = 0; i < e.length; i += 1) this[t].insertBefore(e[i], this[t].childNodes[0]);
                else this[t].insertBefore(e, this[t].childNodes[0]);
                return this
            },
            next: function (t) {
                return new a(0 < this.length ? t ? this[0].nextElementSibling && e(this[0].nextElementSibling).is(t) ? [this[0].nextElementSibling] : [] : this[0].nextElementSibling ? [this[0].nextElementSibling] : [] : [])
            },
            nextAll: function (t) {
                var i = [],
                    n = this[0];
                if (!n) return new a([]);
                for (; n.nextElementSibling;) {
                    var s = n.nextElementSibling;
                    t ? e(s).is(t) && i.push(s) : i.push(s), n = s
                }
                return new a(i)
            },
            prev: function (t) {
                if (0 < this.length) {
                    var i = this[0];
                    return new a(t ? i.previousElementSibling && e(i.previousElementSibling).is(t) ? [i.previousElementSibling] : [] : i.previousElementSibling ? [i.previousElementSibling] : [])
                }
                return new a([])
            },
            prevAll: function (t) {
                var i = [],
                    n = this[0];
                if (!n) return new a([]);
                for (; n.previousElementSibling;) {
                    var s = n.previousElementSibling;
                    t ? e(s).is(t) && i.push(s) : i.push(s), n = s
                }
                return new a(i)
            },
            parent: function (i) {
                for (var n = [], s = 0; s < this.length; s += 1) null !== this[s].parentNode && (i ? e(this[s].parentNode).is(i) && n.push(this[s].parentNode) : n.push(this[s].parentNode));
                return e(t(n))
            },
            parents: function (i) {
                for (var n = [], s = 0; s < this.length; s += 1)
                    for (var o = this[s].parentNode; o;) i ? e(o).is(i) && n.push(o) : n.push(o), o = o.parentNode;
                return e(t(n))
            },
            closest: function (e) {
                var t = this;
                return void 0 === e ? new a([]) : (t.is(e) || (t = t.parents(e).eq(0)), t)
            },
            find: function (e) {
                for (var t = [], i = 0; i < this.length; i += 1)
                    for (var n = this[i].querySelectorAll(e), s = 0; s < n.length; s += 1) t.push(n[s]);
                return new a(t)
            },
            children: function (i) {
                for (var n = [], s = 0; s < this.length; s += 1)
                    for (var o = this[s].childNodes, r = 0; r < o.length; r += 1) i ? 1 === o[r].nodeType && e(o[r]).is(i) && n.push(o[r]) : 1 === o[r].nodeType && n.push(o[r]);
                return new a(t(n))
            },
            remove: function () {
                for (var e = 0; e < this.length; e += 1) this[e].parentNode && this[e].parentNode.removeChild(this[e]);
                return this
            },
            add: function () {
                for (var t = [], i = arguments.length; i--;) t[i] = arguments[i];
                var n, s;
                for (n = 0; n < t.length; n += 1) {
                    var o = e(t[n]);
                    for (s = 0; s < o.length; s += 1) this[this.length] = o[s], this.length += 1
                }
                return this
            },
            styles: function () {
                return this[0] ? o.getComputedStyle(this[0], null) : {}
            }
        };
        Object.keys(r).forEach(function (t) {
            e.fn[t] = e.fn[t] || r[t]
        });
        var l, d, c, p, u = {
                deleteProps: function (e) {
                    var t = e;
                    Object.keys(t).forEach(function (e) {
                        try {
                            t[e] = null
                        } catch (e) {}
                        try {
                            delete t[e]
                        } catch (e) {}
                    })
                },
                nextTick: function (e, t) {
                    return void 0 === t && (t = 0), setTimeout(e, t)
                },
                now: function () {
                    return Date.now()
                },
                getTranslate: function (e, t) {
                    var i, n, s;
                    void 0 === t && (t = "x");
                    var a = o.getComputedStyle(e, null);
                    return o.WebKitCSSMatrix ? (6 < (n = a.transform || a.webkitTransform).split(",").length && (n = n.split(", ").map(function (e) {
                        return e.replace(",", ".")
                    }).join(", ")), s = new o.WebKitCSSMatrix("none" === n ? "" : n)) : i = (s = a.MozTransform || a.OTransform || a.MsTransform || a.msTransform || a.transform || a.getPropertyValue("transform").replace("translate(", "matrix(1, 0, 0, 1,")).toString().split(","), "x" === t && (n = o.WebKitCSSMatrix ? s.m41 : 16 === i.length ? parseFloat(i[12]) : parseFloat(i[4])), "y" === t && (n = o.WebKitCSSMatrix ? s.m42 : 16 === i.length ? parseFloat(i[13]) : parseFloat(i[5])), n || 0
                },
                parseUrlQuery: function (e) {
                    var t, i, n, s, a = {},
                        r = e || o.location.href;
                    if ("string" == typeof r && r.length)
                        for (s = (i = (r = -1 < r.indexOf("?") ? r.replace(/\S*\?/, "") : "").split("&").filter(function (e) {
                                return "" !== e
                            })).length, t = 0; t < s; t += 1) n = i[t].replace(/#\S+/g, "").split("="), a[decodeURIComponent(n[0])] = void 0 === n[1] ? void 0 : decodeURIComponent(n[1]) || "";
                    return a
                },
                isObject: function (e) {
                    return "object" == typeof e && null !== e && e.constructor && e.constructor === Object
                },
                extend: function () {
                    for (var e = [], t = arguments.length; t--;) e[t] = arguments[t];
                    for (var i = Object(e[0]), n = 1; n < e.length; n += 1) {
                        var s = e[n];
                        if (null != s)
                            for (var o = Object.keys(Object(s)), a = 0, r = o.length; a < r; a += 1) {
                                var l = o[a],
                                    d = Object.getOwnPropertyDescriptor(s, l);
                                void 0 !== d && d.enumerable && (u.isObject(i[l]) && u.isObject(s[l]) ? u.extend(i[l], s[l]) : !u.isObject(i[l]) && u.isObject(s[l]) ? (i[l] = {}, u.extend(i[l], s[l])) : i[l] = s[l])
                            }
                    }
                    return i
                }
            },
            h = (c = s.createElement("div"), {
                touch: o.Modernizr && !0 === o.Modernizr.touch || !!(0 < o.navigator.maxTouchPoints || "ontouchstart" in o || o.DocumentTouch && s instanceof o.DocumentTouch),
                pointerEvents: !!(o.navigator.pointerEnabled || o.PointerEvent || "maxTouchPoints" in o.navigator && 0 < o.navigator.maxTouchPoints),
                prefixedPointerEvents: !!o.navigator.msPointerEnabled,
                transition: "transition" in (d = c.style) || "webkitTransition" in d || "MozTransition" in d,
                transforms3d: o.Modernizr && !0 === o.Modernizr.csstransforms3d || "webkitPerspective" in (l = c.style) || "MozPerspective" in l || "OPerspective" in l || "MsPerspective" in l || "perspective" in l,
                flexbox: function () {
                    for (var e = c.style, t = "alignItems webkitAlignItems webkitBoxAlign msFlexAlign mozBoxAlign webkitFlexDirection msFlexDirection mozBoxDirection mozBoxOrient webkitBoxDirection webkitBoxOrient".split(" "), i = 0; i < t.length; i += 1)
                        if (t[i] in e) return !0;
                    return !1
                }(),
                observer: "MutationObserver" in o || "WebkitMutationObserver" in o,
                passiveListener: function () {
                    var e = !1;
                    try {
                        var t = Object.defineProperty({}, "passive", {
                            get: function () {
                                e = !0
                            }
                        });
                        o.addEventListener("testPassiveListener", null, t)
                    } catch (e) {}
                    return e
                }(),
                gestures: "ongesturestart" in o
            }),
            f = {
                isIE: !!o.navigator.userAgent.match(/Trident/g) || !!o.navigator.userAgent.match(/MSIE/g),
                isEdge: !!o.navigator.userAgent.match(/Edge/g),
                isSafari: (p = o.navigator.userAgent.toLowerCase(), 0 <= p.indexOf("safari") && p.indexOf("chrome") < 0 && p.indexOf("android") < 0),
                isUiWebView: /(iPhone|iPod|iPad).*AppleWebKit(?!.*Safari)/i.test(o.navigator.userAgent)
            },
            v = {
                components: {
                    configurable: !0
                }
            };
        i.prototype.on = function (e, t, i) {
            var n = this;
            if ("function" != typeof t) return n;
            var s = i ? "unshift" : "push";
            return e.split(" ").forEach(function (e) {
                n.eventsListeners[e] || (n.eventsListeners[e] = []), n.eventsListeners[e][s](t)
            }), n
        }, i.prototype.once = function (e, t, i) {
            function n() {
                for (var i = [], o = arguments.length; o--;) i[o] = arguments[o];
                t.apply(s, i), s.off(e, n), n.f7proxy && delete n.f7proxy
            }
            var s = this;
            return "function" != typeof t ? s : (n.f7proxy = t, s.on(e, n, i))
        }, i.prototype.off = function (e, t) {
            var i = this;
            return i.eventsListeners && e.split(" ").forEach(function (e) {
                void 0 === t ? i.eventsListeners[e] = [] : i.eventsListeners[e] && i.eventsListeners[e].length && i.eventsListeners[e].forEach(function (n, s) {
                    (n === t || n.f7proxy && n.f7proxy === t) && i.eventsListeners[e].splice(s, 1)
                })
            }), i
        }, i.prototype.emit = function () {
            for (var e = [], t = arguments.length; t--;) e[t] = arguments[t];
            var i, n, s, o = this;
            return o.eventsListeners && (s = "string" == typeof e[0] || Array.isArray(e[0]) ? (i = e[0], n = e.slice(1, e.length), o) : (i = e[0].events, n = e[0].data, e[0].context || o), (Array.isArray(i) ? i : i.split(" ")).forEach(function (e) {
                if (o.eventsListeners && o.eventsListeners[e]) {
                    var t = [];
                    o.eventsListeners[e].forEach(function (e) {
                        t.push(e)
                    }), t.forEach(function (e) {
                        e.apply(s, n)
                    })
                }
            })), o
        }, i.prototype.useModulesParams = function (e) {
            var t = this;
            t.modules && Object.keys(t.modules).forEach(function (i) {
                var n = t.modules[i];
                n.params && u.extend(e, n.params)
            })
        }, i.prototype.useModules = function (e) {
            void 0 === e && (e = {});
            var t = this;
            t.modules && Object.keys(t.modules).forEach(function (i) {
                var n = t.modules[i],
                    s = e[i] || {};
                n.instance && Object.keys(n.instance).forEach(function (e) {
                    var i = n.instance[e];
                    t[e] = "function" == typeof i ? i.bind(t) : i
                }), n.on && t.on && Object.keys(n.on).forEach(function (e) {
                    t.on(e, n.on[e])
                }), n.create && n.create.bind(t)(s)
            })
        }, v.components.set = function (e) {
            this.use && this.use(e)
        }, i.installModule = function (e) {
            for (var t = [], i = arguments.length - 1; 0 < i--;) t[i] = arguments[i + 1];
            var n = this;
            n.prototype.modules || (n.prototype.modules = {});
            var s = e.name || Object.keys(n.prototype.modules).length + "_" + u.now();
            return (n.prototype.modules[s] = e).proto && Object.keys(e.proto).forEach(function (t) {
                n.prototype[t] = e.proto[t]
            }), e.static && Object.keys(e.static).forEach(function (t) {
                n[t] = e.static[t]
            }), e.install && e.install.apply(n, t), n
        }, i.use = function (e) {
            for (var t = [], i = arguments.length - 1; 0 < i--;) t[i] = arguments[i + 1];
            var n = this;
            return Array.isArray(e) ? (e.forEach(function (e) {
                return n.installModule(e)
            }), n) : n.installModule.apply(n, [e].concat(t))
        }, Object.defineProperties(i, v);
        var m = {
                updateSize: function () {
                    var e, t, i = this,
                        n = i.$el;
                    e = void 0 !== i.params.width ? i.params.width : n[0].clientWidth, t = void 0 !== i.params.height ? i.params.height : n[0].clientHeight, 0 === e && i.isHorizontal() || 0 === t && i.isVertical() || (e = e - parseInt(n.css("padding-left"), 10) - parseInt(n.css("padding-right"), 10), t = t - parseInt(n.css("padding-top"), 10) - parseInt(n.css("padding-bottom"), 10), u.extend(i, {
                        width: e,
                        height: t,
                        size: i.isHorizontal() ? e : t
                    }))
                },
                updateSlides: function () {
                    var e = this,
                        t = e.params,
                        i = e.$wrapperEl,
                        n = e.size,
                        s = e.rtlTranslate,
                        a = e.wrongRTL,
                        r = e.virtual && t.virtual.enabled,
                        l = r ? e.virtual.slides.length : e.slides.length,
                        d = i.children("." + e.params.slideClass),
                        c = r ? e.virtual.slides.length : d.length,
                        p = [],
                        v = [],
                        m = [],
                        g = t.slidesOffsetBefore;
                    "function" == typeof g && (g = t.slidesOffsetBefore.call(e));
                    var w = t.slidesOffsetAfter;
                    "function" == typeof w && (w = t.slidesOffsetAfter.call(e));
                    var y = e.snapGrid.length,
                        b = e.snapGrid.length,
                        T = t.spaceBetween,
                        x = -g,
                        S = 0,
                        C = 0;
                    if (void 0 !== n) {
                        var k, E;
                        "string" == typeof T && 0 <= T.indexOf("%") && (T = parseFloat(T.replace("%", "")) / 100 * n), e.virtualSize = -T, s ? d.css({
                            marginLeft: "",
                            marginTop: ""
                        }) : d.css({
                            marginRight: "",
                            marginBottom: ""
                        }), 1 < t.slidesPerColumn && (k = Math.floor(c / t.slidesPerColumn) === c / e.params.slidesPerColumn ? c : Math.ceil(c / t.slidesPerColumn) * t.slidesPerColumn, "auto" !== t.slidesPerView && "row" === t.slidesPerColumnFill && (k = Math.max(k, t.slidesPerView * t.slidesPerColumn)));
                        for (var $, M = t.slidesPerColumn, P = k / M, z = Math.floor(c / t.slidesPerColumn), L = 0; L < c; L += 1) {
                            E = 0;
                            var A = d.eq(L);
                            if (1 < t.slidesPerColumn) {
                                var O = void 0,
                                    D = void 0,
                                    I = void 0;
                                if ("column" === t.slidesPerColumnFill || "row" === t.slidesPerColumnFill && 1 < t.slidesPerGroup) {
                                    if ("column" === t.slidesPerColumnFill) I = L - (D = Math.floor(L / M)) * M, (z < D || D === z && I === M - 1) && M <= (I += 1) && (I = 0, D += 1);
                                    else {
                                        var H = Math.floor(L / t.slidesPerGroup);
                                        D = L - (I = Math.floor(L / t.slidesPerView) - H * t.slidesPerColumn) * t.slidesPerView - H * t.slidesPerView
                                    }
                                    O = D + I * k / M, A.css({
                                        "-webkit-box-ordinal-group": O,
                                        "-moz-box-ordinal-group": O,
                                        "-ms-flex-order": O,
                                        "-webkit-order": O,
                                        order: O
                                    })
                                } else D = L - (I = Math.floor(L / P)) * P;
                                A.css("margin-" + (e.isHorizontal() ? "top" : "left"), 0 !== I && t.spaceBetween && t.spaceBetween + "px").attr("data-swiper-column", D).attr("data-swiper-row", I)
                            }
                            if ("none" !== A.css("display")) {
                                if ("auto" === t.slidesPerView) {
                                    var X = o.getComputedStyle(A[0], null),
                                        Y = A[0].style.transform,
                                        N = A[0].style.webkitTransform;
                                    if (Y && (A[0].style.transform = "none"), N && (A[0].style.webkitTransform = "none"), t.roundLengths) E = e.isHorizontal() ? A.outerWidth(!0) : A.outerHeight(!0);
                                    else if (e.isHorizontal()) {
                                        var B = parseFloat(X.getPropertyValue("width")),
                                            R = parseFloat(X.getPropertyValue("padding-left")),
                                            j = parseFloat(X.getPropertyValue("padding-right")),
                                            F = parseFloat(X.getPropertyValue("margin-left")),
                                            W = parseFloat(X.getPropertyValue("margin-right")),
                                            q = X.getPropertyValue("box-sizing");
                                        E = q && "border-box" === q && !f.isIE ? B + F + W : B + R + j + F + W
                                    } else {
                                        var G = parseFloat(X.getPropertyValue("height")),
                                            V = parseFloat(X.getPropertyValue("padding-top")),
                                            _ = parseFloat(X.getPropertyValue("padding-bottom")),
                                            U = parseFloat(X.getPropertyValue("margin-top")),
                                            K = parseFloat(X.getPropertyValue("margin-bottom")),
                                            Q = X.getPropertyValue("box-sizing");
                                        E = Q && "border-box" === Q && !f.isIE ? G + U + K : G + V + _ + U + K
                                    }
                                    Y && (A[0].style.transform = Y), N && (A[0].style.webkitTransform = N), t.roundLengths && (E = Math.floor(E))
                                } else E = (n - (t.slidesPerView - 1) * T) / t.slidesPerView, t.roundLengths && (E = Math.floor(E)), d[L] && (e.isHorizontal() ? d[L].style.width = E + "px" : d[L].style.height = E + "px");
                                d[L] && (d[L].swiperSlideSize = E), m.push(E), t.centeredSlides ? (x = x + E / 2 + S / 2 + T, 0 === S && 0 !== L && (x = x - n / 2 - T), 0 === L && (x = x - n / 2 - T), Math.abs(x) < .001 && (x = 0), t.roundLengths && (x = Math.floor(x)), C % t.slidesPerGroup == 0 && p.push(x), v.push(x)) : (t.roundLengths && (x = Math.floor(x)), C % t.slidesPerGroup == 0 && p.push(x), v.push(x), x = x + E + T), e.virtualSize += E + T, S = E, C += 1
                            }
                        }
                        if (e.virtualSize = Math.max(e.virtualSize, n) + w, s && a && ("slide" === t.effect || "coverflow" === t.effect) && i.css({
                                width: e.virtualSize + t.spaceBetween + "px"
                            }), h.flexbox && !t.setWrapperSize || (e.isHorizontal() ? i.css({
                                width: e.virtualSize + t.spaceBetween + "px"
                            }) : i.css({
                                height: e.virtualSize + t.spaceBetween + "px"
                            })), 1 < t.slidesPerColumn && (e.virtualSize = (E + t.spaceBetween) * k, e.virtualSize = Math.ceil(e.virtualSize / t.slidesPerColumn) - t.spaceBetween, e.isHorizontal() ? i.css({
                                width: e.virtualSize + t.spaceBetween + "px"
                            }) : i.css({
                                height: e.virtualSize + t.spaceBetween + "px"
                            }), t.centeredSlides)) {
                            $ = [];
                            for (var Z = 0; Z < p.length; Z += 1) {
                                var J = p[Z];
                                t.roundLengths && (J = Math.floor(J)), p[Z] < e.virtualSize + p[0] && $.push(J)
                            }
                            p = $
                        }
                        if (!t.centeredSlides) {
                            $ = [];
                            for (var ee = 0; ee < p.length; ee += 1) {
                                var te = p[ee];
                                t.roundLengths && (te = Math.floor(te)), p[ee] <= e.virtualSize - n && $.push(te)
                            }
                            p = $, 1 < Math.floor(e.virtualSize - n) - Math.floor(p[p.length - 1]) && p.push(e.virtualSize - n)
                        }
                        if (0 === p.length && (p = [0]), 0 !== t.spaceBetween && (e.isHorizontal() ? s ? d.css({
                                marginLeft: T + "px"
                            }) : d.css({
                                marginRight: T + "px"
                            }) : d.css({
                                marginBottom: T + "px"
                            })), t.centerInsufficientSlides) {
                            var ie = 0;
                            if (m.forEach(function (e) {
                                    ie += e + (t.spaceBetween ? t.spaceBetween : 0)
                                }), (ie -= t.spaceBetween) < n) {
                                var ne = (n - ie) / 2;
                                p.forEach(function (e, t) {
                                    p[t] = e - ne
                                }), v.forEach(function (e, t) {
                                    v[t] = e + ne
                                })
                            }
                        }
                        u.extend(e, {
                            slides: d,
                            snapGrid: p,
                            slidesGrid: v,
                            slidesSizesGrid: m
                        }), c !== l && e.emit("slidesLengthChange"), p.length !== y && (e.params.watchOverflow && e.checkOverflow(), e.emit("snapGridLengthChange")), v.length !== b && e.emit("slidesGridLengthChange"), (t.watchSlidesProgress || t.watchSlidesVisibility) && e.updateSlidesOffset()
                    }
                },
                updateAutoHeight: function (e) {
                    var t, i = this,
                        n = [],
                        s = 0;
                    if ("number" == typeof e ? i.setTransition(e) : !0 === e && i.setTransition(i.params.speed), "auto" !== i.params.slidesPerView && 1 < i.params.slidesPerView)
                        for (t = 0; t < Math.ceil(i.params.slidesPerView); t += 1) {
                            var o = i.activeIndex + t;
                            if (o > i.slides.length) break;
                            n.push(i.slides.eq(o)[0])
                        } else n.push(i.slides.eq(i.activeIndex)[0]);
                    for (t = 0; t < n.length; t += 1)
                        if (void 0 !== n[t]) {
                            var a = n[t].offsetHeight;
                            s = s < a ? a : s
                        } s && i.$wrapperEl.css("height", s + "px")
                },
                updateSlidesOffset: function () {
                    for (var e = this.slides, t = 0; t < e.length; t += 1) e[t].swiperSlideOffset = this.isHorizontal() ? e[t].offsetLeft : e[t].offsetTop
                },
                updateSlidesProgress: function (t) {
                    void 0 === t && (t = this && this.translate || 0);
                    var i = this,
                        n = i.params,
                        s = i.slides,
                        o = i.rtlTranslate;
                    if (0 !== s.length) {
                        void 0 === s[0].swiperSlideOffset && i.updateSlidesOffset();
                        var a = -t;
                        o && (a = t), s.removeClass(n.slideVisibleClass), i.visibleSlidesIndexes = [], i.visibleSlides = [];
                        for (var r = 0; r < s.length; r += 1) {
                            var l = s[r],
                                d = (a + (n.centeredSlides ? i.minTranslate() : 0) - l.swiperSlideOffset) / (l.swiperSlideSize + n.spaceBetween);
                            if (n.watchSlidesVisibility) {
                                var c = -(a - l.swiperSlideOffset),
                                    p = c + i.slidesSizesGrid[r];
                                (0 <= c && c < i.size - 1 || 1 < p && p <= i.size || c <= 0 && p >= i.size) && (i.visibleSlides.push(l), i.visibleSlidesIndexes.push(r), s.eq(r).addClass(n.slideVisibleClass))
                            }
                            l.progress = o ? -d : d
                        }
                        i.visibleSlides = e(i.visibleSlides)
                    }
                },
                updateProgress: function (e) {
                    void 0 === e && (e = this && this.translate || 0);
                    var t = this,
                        i = t.params,
                        n = t.maxTranslate() - t.minTranslate(),
                        s = t.progress,
                        o = t.isBeginning,
                        a = t.isEnd,
                        r = o,
                        l = a;
                    a = 0 == n ? o = !(s = 0) : (o = (s = (e - t.minTranslate()) / n) <= 0, 1 <= s), u.extend(t, {
                        progress: s,
                        isBeginning: o,
                        isEnd: a
                    }), (i.watchSlidesProgress || i.watchSlidesVisibility) && t.updateSlidesProgress(e), o && !r && t.emit("reachBeginning toEdge"), a && !l && t.emit("reachEnd toEdge"), (r && !o || l && !a) && t.emit("fromEdge"), t.emit("progress", s)
                },
                updateSlidesClasses: function () {
                    var e, t = this,
                        i = t.slides,
                        n = t.params,
                        s = t.$wrapperEl,
                        o = t.activeIndex,
                        a = t.realIndex,
                        r = t.virtual && n.virtual.enabled;
                    i.removeClass(n.slideActiveClass + " " + n.slideNextClass + " " + n.slidePrevClass + " " + n.slideDuplicateActiveClass + " " + n.slideDuplicateNextClass + " " + n.slideDuplicatePrevClass), (e = r ? t.$wrapperEl.find("." + n.slideClass + '[data-swiper-slide-index="' + o + '"]') : i.eq(o)).addClass(n.slideActiveClass), n.loop && (e.hasClass(n.slideDuplicateClass) ? s.children("." + n.slideClass + ":not(." + n.slideDuplicateClass + ')[data-swiper-slide-index="' + a + '"]').addClass(n.slideDuplicateActiveClass) : s.children("." + n.slideClass + "." + n.slideDuplicateClass + '[data-swiper-slide-index="' + a + '"]').addClass(n.slideDuplicateActiveClass));
                    var l = e.nextAll("." + n.slideClass).eq(0).addClass(n.slideNextClass);
                    n.loop && 0 === l.length && (l = i.eq(0)).addClass(n.slideNextClass);
                    var d = e.prevAll("." + n.slideClass).eq(0).addClass(n.slidePrevClass);
                    n.loop && 0 === d.length && (d = i.eq(-1)).addClass(n.slidePrevClass), n.loop && (l.hasClass(n.slideDuplicateClass) ? s.children("." + n.slideClass + ":not(." + n.slideDuplicateClass + ')[data-swiper-slide-index="' + l.attr("data-swiper-slide-index") + '"]').addClass(n.slideDuplicateNextClass) : s.children("." + n.slideClass + "." + n.slideDuplicateClass + '[data-swiper-slide-index="' + l.attr("data-swiper-slide-index") + '"]').addClass(n.slideDuplicateNextClass), d.hasClass(n.slideDuplicateClass) ? s.children("." + n.slideClass + ":not(." + n.slideDuplicateClass + ')[data-swiper-slide-index="' + d.attr("data-swiper-slide-index") + '"]').addClass(n.slideDuplicatePrevClass) : s.children("." + n.slideClass + "." + n.slideDuplicateClass + '[data-swiper-slide-index="' + d.attr("data-swiper-slide-index") + '"]').addClass(n.slideDuplicatePrevClass))
                },
                updateActiveIndex: function (e) {
                    var t, i = this,
                        n = i.rtlTranslate ? i.translate : -i.translate,
                        s = i.slidesGrid,
                        o = i.snapGrid,
                        a = i.params,
                        r = i.activeIndex,
                        l = i.realIndex,
                        d = i.snapIndex,
                        c = e;
                    if (void 0 === c) {
                        for (var p = 0; p < s.length; p += 1) void 0 !== s[p + 1] ? n >= s[p] && n < s[p + 1] - (s[p + 1] - s[p]) / 2 ? c = p : n >= s[p] && n < s[p + 1] && (c = p + 1) : n >= s[p] && (c = p);
                        a.normalizeSlideIndex && (c < 0 || void 0 === c) && (c = 0)
                    }
                    if ((t = 0 <= o.indexOf(n) ? o.indexOf(n) : Math.floor(c / a.slidesPerGroup)) >= o.length && (t = o.length - 1), c !== r) {
                        var h = parseInt(i.slides.eq(c).attr("data-swiper-slide-index") || c, 10);
                        u.extend(i, {
                            snapIndex: t,
                            realIndex: h,
                            previousIndex: r,
                            activeIndex: c
                        }), i.emit("activeIndexChange"), i.emit("snapIndexChange"), l !== h && i.emit("realIndexChange"), (i.initialized || i.runCallbacksOnInit) && i.emit("slideChange")
                    } else t !== d && (i.snapIndex = t, i.emit("snapIndexChange"))
                },
                updateClickedSlide: function (t) {
                    var i = this,
                        n = i.params,
                        s = e(t.target).closest("." + n.slideClass)[0],
                        o = !1;
                    if (s)
                        for (var a = 0; a < i.slides.length; a += 1) i.slides[a] === s && (o = !0);
                    if (!s || !o) return i.clickedSlide = void 0, void(i.clickedIndex = void 0);
                    i.clickedSlide = s, i.virtual && i.params.virtual.enabled ? i.clickedIndex = parseInt(e(s).attr("data-swiper-slide-index"), 10) : i.clickedIndex = e(s).index(), n.slideToClickedSlide && void 0 !== i.clickedIndex && i.clickedIndex !== i.activeIndex && i.slideToClickedSlide()
                }
            },
            g = {
                getTranslate: function (e) {
                    void 0 === e && (e = this.isHorizontal() ? "x" : "y");
                    var t = this.params,
                        i = this.rtlTranslate,
                        n = this.translate,
                        s = this.$wrapperEl;
                    if (t.virtualTranslate) return i ? -n : n;
                    var o = u.getTranslate(s[0], e);
                    return i && (o = -o), o || 0
                },
                setTranslate: function (e, t) {
                    var i = this,
                        n = i.rtlTranslate,
                        s = i.params,
                        o = i.$wrapperEl,
                        a = i.progress,
                        r = 0,
                        l = 0;
                    i.isHorizontal() ? r = n ? -e : e : l = e, s.roundLengths && (r = Math.floor(r), l = Math.floor(l)), s.virtualTranslate || (h.transforms3d ? o.transform("translate3d(" + r + "px, " + l + "px, 0px)") : o.transform("translate(" + r + "px, " + l + "px)")), i.previousTranslate = i.translate, i.translate = i.isHorizontal() ? r : l;
                    var d = i.maxTranslate() - i.minTranslate();
                    (0 == d ? 0 : (e - i.minTranslate()) / d) !== a && i.updateProgress(e), i.emit("setTranslate", i.translate, t)
                },
                minTranslate: function () {
                    return -this.snapGrid[0]
                },
                maxTranslate: function () {
                    return -this.snapGrid[this.snapGrid.length - 1]
                }
            },
            w = {
                setTransition: function (e, t) {
                    this.$wrapperEl.transition(e), this.emit("setTransition", e, t)
                },
                transitionStart: function (e, t) {
                    void 0 === e && (e = !0);
                    var i = this,
                        n = i.activeIndex,
                        s = i.params,
                        o = i.previousIndex;
                    s.autoHeight && i.updateAutoHeight();
                    var a = t;
                    if (a = a || (o < n ? "next" : n < o ? "prev" : "reset"), i.emit("transitionStart"), e && n !== o) {
                        if ("reset" === a) return void i.emit("slideResetTransitionStart");
                        i.emit("slideChangeTransitionStart"), "next" === a ? i.emit("slideNextTransitionStart") : i.emit("slidePrevTransitionStart")
                    }
                },
                transitionEnd: function (e, t) {
                    void 0 === e && (e = !0);
                    var i = this,
                        n = i.activeIndex,
                        s = i.previousIndex;
                    i.animating = !1, i.setTransition(0);
                    var o = t;
                    if (o = o || (s < n ? "next" : n < s ? "prev" : "reset"), i.emit("transitionEnd"), e && n !== s) {
                        if ("reset" === o) return void i.emit("slideResetTransitionEnd");
                        i.emit("slideChangeTransitionEnd"), "next" === o ? i.emit("slideNextTransitionEnd") : i.emit("slidePrevTransitionEnd")
                    }
                }
            },
            y = {
                slideTo: function (e, t, i, n) {
                    void 0 === e && (e = 0), void 0 === t && (t = this.params.speed), void 0 === i && (i = !0);
                    var s = this,
                        o = e;
                    o < 0 && (o = 0);
                    var a = s.params,
                        r = s.snapGrid,
                        l = s.slidesGrid,
                        d = s.previousIndex,
                        c = s.activeIndex,
                        p = s.rtlTranslate;
                    if (s.animating && a.preventInteractionOnTransition) return !1;
                    var u = Math.floor(o / a.slidesPerGroup);
                    u >= r.length && (u = r.length - 1), (c || a.initialSlide || 0) === (d || 0) && i && s.emit("beforeSlideChangeStart");
                    var f, v = -r[u];
                    if (s.updateProgress(v), a.normalizeSlideIndex)
                        for (var m = 0; m < l.length; m += 1) - Math.floor(100 * v) >= Math.floor(100 * l[m]) && (o = m);
                    if (s.initialized && o !== c) {
                        if (!s.allowSlideNext && v < s.translate && v < s.minTranslate()) return !1;
                        if (!s.allowSlidePrev && v > s.translate && v > s.maxTranslate() && (c || 0) !== o) return !1
                    }
                    return f = c < o ? "next" : o < c ? "prev" : "reset", p && -v === s.translate || !p && v === s.translate ? (s.updateActiveIndex(o), a.autoHeight && s.updateAutoHeight(), s.updateSlidesClasses(), "slide" !== a.effect && s.setTranslate(v), "reset" !== f && (s.transitionStart(i, f), s.transitionEnd(i, f)), !1) : (0 !== t && h.transition ? (s.setTransition(t), s.setTranslate(v), s.updateActiveIndex(o), s.updateSlidesClasses(), s.emit("beforeTransitionStart", t, n), s.transitionStart(i, f), s.animating || (s.animating = !0, s.onSlideToWrapperTransitionEnd || (s.onSlideToWrapperTransitionEnd = function (e) {
                        s && !s.destroyed && e.target === this && (s.$wrapperEl[0].removeEventListener("transitionend", s.onSlideToWrapperTransitionEnd), s.$wrapperEl[0].removeEventListener("webkitTransitionEnd", s.onSlideToWrapperTransitionEnd), s.onSlideToWrapperTransitionEnd = null, delete s.onSlideToWrapperTransitionEnd, s.transitionEnd(i, f))
                    }), s.$wrapperEl[0].addEventListener("transitionend", s.onSlideToWrapperTransitionEnd), s.$wrapperEl[0].addEventListener("webkitTransitionEnd", s.onSlideToWrapperTransitionEnd))) : (s.setTransition(0), s.setTranslate(v), s.updateActiveIndex(o), s.updateSlidesClasses(), s.emit("beforeTransitionStart", t, n), s.transitionStart(i, f), s.transitionEnd(i, f)), !0)
                },
                slideToLoop: function (e, t, i, n) {
                    void 0 === e && (e = 0), void 0 === t && (t = this.params.speed), void 0 === i && (i = !0);
                    var s = e;
                    return this.params.loop && (s += this.loopedSlides), this.slideTo(s, t, i, n)
                },
                slideNext: function (e, t, i) {
                    void 0 === e && (e = this.params.speed), void 0 === t && (t = !0);
                    var n = this,
                        s = n.params,
                        o = n.animating;
                    return s.loop ? !o && (n.loopFix(), n._clientLeft = n.$wrapperEl[0].clientLeft, n.slideTo(n.activeIndex + s.slidesPerGroup, e, t, i)) : n.slideTo(n.activeIndex + s.slidesPerGroup, e, t, i)
                },
                slidePrev: function (e, t, i) {
                    function n(e) {
                        return e < 0 ? -Math.floor(Math.abs(e)) : Math.floor(e)
                    }
                    void 0 === e && (e = this.params.speed), void 0 === t && (t = !0);
                    var s = this,
                        o = s.params,
                        a = s.animating,
                        r = s.snapGrid,
                        l = s.slidesGrid,
                        d = s.rtlTranslate;
                    if (o.loop) {
                        if (a) return !1;
                        s.loopFix(), s._clientLeft = s.$wrapperEl[0].clientLeft
                    }
                    var c, p = n(d ? s.translate : -s.translate),
                        u = r.map(function (e) {
                            return n(e)
                        }),
                        h = (l.map(function (e) {
                            return n(e)
                        }), r[u.indexOf(p)], r[u.indexOf(p) - 1]);
                    return void 0 !== h && (c = l.indexOf(h)) < 0 && (c = s.activeIndex - 1), s.slideTo(c, e, t, i)
                },
                slideReset: function (e, t, i) {
                    return void 0 === e && (e = this.params.speed), void 0 === t && (t = !0), this.slideTo(this.activeIndex, e, t, i)
                },
                slideToClosest: function (e, t, i) {
                    void 0 === e && (e = this.params.speed), void 0 === t && (t = !0);
                    var n = this,
                        s = n.activeIndex,
                        o = Math.floor(s / n.params.slidesPerGroup);
                    if (o < n.snapGrid.length - 1) {
                        var a = n.rtlTranslate ? n.translate : -n.translate,
                            r = n.snapGrid[o];
                        (n.snapGrid[o + 1] - r) / 2 < a - r && (s = n.params.slidesPerGroup)
                    }
                    return n.slideTo(s, e, t, i)
                },
                slideToClickedSlide: function () {
                    var t, i = this,
                        n = i.params,
                        s = i.$wrapperEl,
                        o = "auto" === n.slidesPerView ? i.slidesPerViewDynamic() : n.slidesPerView,
                        a = i.clickedIndex;
                    if (n.loop) {
                        if (i.animating) return;
                        t = parseInt(e(i.clickedSlide).attr("data-swiper-slide-index"), 10), n.centeredSlides ? a < i.loopedSlides - o / 2 || a > i.slides.length - i.loopedSlides + o / 2 ? (i.loopFix(), a = s.children("." + n.slideClass + '[data-swiper-slide-index="' + t + '"]:not(.' + n.slideDuplicateClass + ")").eq(0).index(), u.nextTick(function () {
                            i.slideTo(a)
                        })) : i.slideTo(a) : a > i.slides.length - o ? (i.loopFix(), a = s.children("." + n.slideClass + '[data-swiper-slide-index="' + t + '"]:not(.' + n.slideDuplicateClass + ")").eq(0).index(), u.nextTick(function () {
                            i.slideTo(a)
                        })) : i.slideTo(a)
                    } else i.slideTo(a)
                }
            },
            b = {
                loopCreate: function () {
                    var t = this,
                        i = t.params,
                        n = t.$wrapperEl;
                    n.children("." + i.slideClass + "." + i.slideDuplicateClass).remove();
                    var o = n.children("." + i.slideClass);
                    if (i.loopFillGroupWithBlank) {
                        var a = i.slidesPerGroup - o.length % i.slidesPerGroup;
                        if (a !== i.slidesPerGroup) {
                            for (var r = 0; r < a; r += 1) {
                                var l = e(s.createElement("div")).addClass(i.slideClass + " " + i.slideBlankClass);
                                n.append(l)
                            }
                            o = n.children("." + i.slideClass)
                        }
                    }
                    "auto" !== i.slidesPerView || i.loopedSlides || (i.loopedSlides = o.length), t.loopedSlides = parseInt(i.loopedSlides || i.slidesPerView, 10), t.loopedSlides += i.loopAdditionalSlides, t.loopedSlides > o.length && (t.loopedSlides = o.length);
                    var d = [],
                        c = [];
                    o.each(function (i, n) {
                        var s = e(n);
                        i < t.loopedSlides && c.push(n), i < o.length && i >= o.length - t.loopedSlides && d.push(n), s.attr("data-swiper-slide-index", i)
                    });
                    for (var p = 0; p < c.length; p += 1) n.append(e(c[p].cloneNode(!0)).addClass(i.slideDuplicateClass));
                    for (var u = d.length - 1; 0 <= u; u -= 1) n.prepend(e(d[u].cloneNode(!0)).addClass(i.slideDuplicateClass))
                },
                loopFix: function () {
                    var e, t = this,
                        i = t.params,
                        n = t.activeIndex,
                        s = t.slides,
                        o = t.loopedSlides,
                        a = t.allowSlidePrev,
                        r = t.allowSlideNext,
                        l = t.snapGrid,
                        d = t.rtlTranslate;
                    t.allowSlidePrev = !0, t.allowSlideNext = !0;
                    var c = -l[n] - t.getTranslate();
                    n < o ? (e = s.length - 3 * o + n, e += o, t.slideTo(e, 0, !1, !0) && 0 != c && t.setTranslate((d ? -t.translate : t.translate) - c)) : ("auto" === i.slidesPerView && 2 * o <= n || n >= s.length - o) && (e = -s.length + n + o, e += o, t.slideTo(e, 0, !1, !0) && 0 != c && t.setTranslate((d ? -t.translate : t.translate) - c)), t.allowSlidePrev = a, t.allowSlideNext = r
                },
                loopDestroy: function () {
                    var e = this.$wrapperEl,
                        t = this.params,
                        i = this.slides;
                    e.children("." + t.slideClass + "." + t.slideDuplicateClass + ",." + t.slideClass + "." + t.slideBlankClass).remove(), i.removeAttr("data-swiper-slide-index")
                }
            },
            T = {
                setGrabCursor: function (e) {
                    if (!(h.touch || !this.params.simulateTouch || this.params.watchOverflow && this.isLocked)) {
                        var t = this.el;
                        t.style.cursor = "move", t.style.cursor = e ? "-webkit-grabbing" : "-webkit-grab", t.style.cursor = e ? "-moz-grabbin" : "-moz-grab", t.style.cursor = e ? "grabbing" : "grab"
                    }
                },
                unsetGrabCursor: function () {
                    h.touch || this.params.watchOverflow && this.isLocked || (this.el.style.cursor = "")
                }
            },
            x = {
                appendSlide: function (e) {
                    var t = this,
                        i = t.$wrapperEl,
                        n = t.params;
                    if (n.loop && t.loopDestroy(), "object" == typeof e && "length" in e)
                        for (var s = 0; s < e.length; s += 1) e[s] && i.append(e[s]);
                    else i.append(e);
                    n.loop && t.loopCreate(), n.observer && h.observer || t.update()
                },
                prependSlide: function (e) {
                    var t = this,
                        i = t.params,
                        n = t.$wrapperEl,
                        s = t.activeIndex;
                    i.loop && t.loopDestroy();
                    var o = s + 1;
                    if ("object" == typeof e && "length" in e) {
                        for (var a = 0; a < e.length; a += 1) e[a] && n.prepend(e[a]);
                        o = s + e.length
                    } else n.prepend(e);
                    i.loop && t.loopCreate(), i.observer && h.observer || t.update(), t.slideTo(o, 0, !1)
                },
                addSlide: function (e, t) {
                    var i = this,
                        n = i.$wrapperEl,
                        s = i.params,
                        o = i.activeIndex;
                    s.loop && (o -= i.loopedSlides, i.loopDestroy(), i.slides = n.children("." + s.slideClass));
                    var a = i.slides.length;
                    if (e <= 0) i.prependSlide(t);
                    else if (a <= e) i.appendSlide(t);
                    else {
                        for (var r = e < o ? o + 1 : o, l = [], d = a - 1; e <= d; d -= 1) {
                            var c = i.slides.eq(d);
                            c.remove(), l.unshift(c)
                        }
                        if ("object" == typeof t && "length" in t) {
                            for (var p = 0; p < t.length; p += 1) t[p] && n.append(t[p]);
                            r = e < o ? o + t.length : o
                        } else n.append(t);
                        for (var u = 0; u < l.length; u += 1) n.append(l[u]);
                        s.loop && i.loopCreate(), s.observer && h.observer || i.update(), s.loop ? i.slideTo(r + i.loopedSlides, 0, !1) : i.slideTo(r, 0, !1)
                    }
                },
                removeSlide: function (e) {
                    var t = this,
                        i = t.params,
                        n = t.$wrapperEl,
                        s = t.activeIndex;
                    i.loop && (s -= t.loopedSlides, t.loopDestroy(), t.slides = n.children("." + i.slideClass));
                    var o, a = s;
                    if ("object" == typeof e && "length" in e) {
                        for (var r = 0; r < e.length; r += 1) o = e[r], t.slides[o] && t.slides.eq(o).remove(), o < a && (a -= 1);
                        a = Math.max(a, 0)
                    } else o = e, t.slides[o] && t.slides.eq(o).remove(), o < a && (a -= 1), a = Math.max(a, 0);
                    i.loop && t.loopCreate(), i.observer && h.observer || t.update(), i.loop ? t.slideTo(a + t.loopedSlides, 0, !1) : t.slideTo(a, 0, !1)
                },
                removeAllSlides: function () {
                    for (var e = [], t = 0; t < this.slides.length; t += 1) e.push(t);
                    this.removeSlide(e)
                }
            },
            S = function () {
                var e = o.navigator.userAgent,
                    t = {
                        ios: !1,
                        android: !1,
                        androidChrome: !1,
                        desktop: !1,
                        windows: !1,
                        iphone: !1,
                        ipod: !1,
                        ipad: !1,
                        cordova: o.cordova || o.phonegap,
                        phonegap: o.cordova || o.phonegap
                    },
                    i = e.match(/(Windows Phone);?[\s\/]+([\d.]+)?/),
                    n = e.match(/(Android);?[\s\/]+([\d.]+)?/),
                    a = e.match(/(iPad).*OS\s([\d_]+)/),
                    r = e.match(/(iPod)(.*OS\s([\d_]+))?/),
                    l = !a && e.match(/(iPhone\sOS|iOS)\s([\d_]+)/);
                if (i && (t.os = "windows", t.osVersion = i[2], t.windows = !0), n && !i && (t.os = "android", t.osVersion = n[2], t.android = !0, t.androidChrome = 0 <= e.toLowerCase().indexOf("chrome")), (a || l || r) && (t.os = "ios", t.ios = !0), l && !r && (t.osVersion = l[2].replace(/_/g, "."), t.iphone = !0), a && (t.osVersion = a[2].replace(/_/g, "."), t.ipad = !0), r && (t.osVersion = r[3] ? r[3].replace(/_/g, ".") : null, t.iphone = !0), t.ios && t.osVersion && 0 <= e.indexOf("Version/") && "10" === t.osVersion.split(".")[0] && (t.osVersion = e.toLowerCase().split("version/")[1].split(" ")[0]), t.desktop = !(t.os || t.android || t.webView), t.webView = (l || a || r) && e.match(/.*AppleWebKit(?!.*Safari)/i), t.os && "ios" === t.os) {
                    var d = t.osVersion.split("."),
                        c = s.querySelector('meta[name="viewport"]');
                    t.minimalUi = !t.webView && (r || l) && (1 * d[0] == 7 ? 1 <= 1 * d[1] : 7 < 1 * d[0]) && c && 0 <= c.getAttribute("content").indexOf("minimal-ui")
                }
                return t.pixelRatio = o.devicePixelRatio || 1, t
            }(),
            C = {
                init: !0,
                direction: "horizontal",
                touchEventsTarget: "container",
                initialSlide: 0,
                speed: 300,
                preventInteractionOnTransition: !1,
                edgeSwipeDetection: !1,
                edgeSwipeThreshold: 20,
                freeMode: !1,
                freeModeMomentum: !0,
                freeModeMomentumRatio: 1,
                freeModeMomentumBounce: !0,
                freeModeMomentumBounceRatio: 1,
                freeModeMomentumVelocityRatio: 1,
                freeModeSticky: !1,
                freeModeMinimumVelocity: .02,
                autoHeight: !1,
                setWrapperSize: !1,
                virtualTranslate: !1,
                effect: "slide",
                breakpoints: void 0,
                breakpointsInverse: !1,
                spaceBetween: 0,
                slidesPerView: 1,
                slidesPerColumn: 1,
                slidesPerColumnFill: "column",
                slidesPerGroup: 1,
                centeredSlides: !1,
                slidesOffsetBefore: 0,
                slidesOffsetAfter: 0,
                normalizeSlideIndex: !0,
                centerInsufficientSlides: !1,
                watchOverflow: !1,
                roundLengths: !1,
                touchRatio: 1,
                touchAngle: 45,
                simulateTouch: !0,
                shortSwipes: !0,
                longSwipes: !0,
                longSwipesRatio: .5,
                longSwipesMs: 300,
                followFinger: !0,
                allowTouchMove: !0,
                threshold: 0,
                touchMoveStopPropagation: !0,
                touchStartPreventDefault: !0,
                touchStartForcePreventDefault: !1,
                touchReleaseOnEdges: !1,
                uniqueNavElements: !0,
                resistance: !0,
                resistanceRatio: .85,
                watchSlidesProgress: !1,
                watchSlidesVisibility: !1,
                grabCursor: !1,
                preventClicks: !0,
                preventClicksPropagation: !0,
                slideToClickedSlide: !1,
                preloadImages: !0,
                updateOnImagesReady: !0,
                loop: !1,
                loopAdditionalSlides: 0,
                loopedSlides: null,
                loopFillGroupWithBlank: !1,
                allowSlidePrev: !0,
                allowSlideNext: !0,
                swipeHandler: null,
                noSwiping: !0,
                noSwipingClass: "swiper-no-swiping",
                noSwipingSelector: null,
                passiveListeners: !0,
                containerModifierClass: "swiper-container-",
                slideClass: "swiper-slide",
                slideBlankClass: "swiper-slide-invisible-blank",
                slideActiveClass: "swiper-slide-active",
                slideDuplicateActiveClass: "swiper-slide-duplicate-active",
                slideVisibleClass: "swiper-slide-visible",
                slideDuplicateClass: "swiper-slide-duplicate",
                slideNextClass: "swiper-slide-next",
                slideDuplicateNextClass: "swiper-slide-duplicate-next",
                slidePrevClass: "swiper-slide-prev",
                slideDuplicatePrevClass: "swiper-slide-duplicate-prev",
                wrapperClass: "swiper-wrapper",
                runCallbacksOnInit: !0
            },
            k = {
                update: m,
                translate: g,
                transition: w,
                slide: y,
                loop: b,
                grabCursor: T,
                manipulation: x,
                events: {
                    attachEvents: function () {
                        var t = this,
                            i = t.params,
                            a = t.touchEvents,
                            r = t.el,
                            l = t.wrapperEl;
                        t.onTouchStart = function (t) {
                            var i = this,
                                n = i.touchEventsData,
                                a = i.params,
                                r = i.touches;
                            if (!i.animating || !a.preventInteractionOnTransition) {
                                var l = t;
                                if (l.originalEvent && (l = l.originalEvent), n.isTouchEvent = "touchstart" === l.type, (n.isTouchEvent || !("which" in l) || 3 !== l.which) && !(!n.isTouchEvent && "button" in l && 0 < l.button || n.isTouched && n.isMoved))
                                    if (a.noSwiping && e(l.target).closest(a.noSwipingSelector ? a.noSwipingSelector : "." + a.noSwipingClass)[0]) i.allowClick = !0;
                                    else if (!a.swipeHandler || e(l).closest(a.swipeHandler)[0]) {
                                    r.currentX = "touchstart" === l.type ? l.targetTouches[0].pageX : l.pageX, r.currentY = "touchstart" === l.type ? l.targetTouches[0].pageY : l.pageY;
                                    var d = r.currentX,
                                        c = r.currentY,
                                        p = a.edgeSwipeDetection || a.iOSEdgeSwipeDetection,
                                        h = a.edgeSwipeThreshold || a.iOSEdgeSwipeThreshold;
                                    if (!p || !(d <= h || d >= o.screen.width - h)) {
                                        if (u.extend(n, {
                                                isTouched: !0,
                                                isMoved: !1,
                                                allowTouchCallbacks: !0,
                                                isScrolling: void 0,
                                                startMoving: void 0
                                            }), r.startX = d, r.startY = c, n.touchStartTime = u.now(), i.allowClick = !0, i.updateSize(), i.swipeDirection = void 0, 0 < a.threshold && (n.allowThresholdMove = !1), "touchstart" !== l.type) {
                                            var f = !0;
                                            e(l.target).is(n.formElements) && (f = !1), s.activeElement && e(s.activeElement).is(n.formElements) && s.activeElement !== l.target && s.activeElement.blur();
                                            var v = f && i.allowTouchMove && a.touchStartPreventDefault;
                                            (a.touchStartForcePreventDefault || v) && l.preventDefault()
                                        }
                                        i.emit("touchStart", l)
                                    }
                                }
                            }
                        }.bind(t), t.onTouchMove = function (t) {
                            var i = this,
                                n = i.touchEventsData,
                                o = i.params,
                                a = i.touches,
                                r = i.rtlTranslate,
                                l = t;
                            if (l.originalEvent && (l = l.originalEvent), n.isTouched) {
                                if (!n.isTouchEvent || "mousemove" !== l.type) {
                                    var d = "touchmove" === l.type ? l.targetTouches[0].pageX : l.pageX,
                                        c = "touchmove" === l.type ? l.targetTouches[0].pageY : l.pageY;
                                    if (l.preventedByNestedSwiper) return a.startX = d, void(a.startY = c);
                                    if (!i.allowTouchMove) return i.allowClick = !1, void(n.isTouched && (u.extend(a, {
                                        startX: d,
                                        startY: c,
                                        currentX: d,
                                        currentY: c
                                    }), n.touchStartTime = u.now()));
                                    if (n.isTouchEvent && o.touchReleaseOnEdges && !o.loop)
                                        if (i.isVertical()) {
                                            if (c < a.startY && i.translate <= i.maxTranslate() || c > a.startY && i.translate >= i.minTranslate()) return n.isTouched = !1, void(n.isMoved = !1)
                                        } else if (d < a.startX && i.translate <= i.maxTranslate() || d > a.startX && i.translate >= i.minTranslate()) return;
                                    if (n.isTouchEvent && s.activeElement && l.target === s.activeElement && e(l.target).is(n.formElements)) return n.isMoved = !0, void(i.allowClick = !1);
                                    if (n.allowTouchCallbacks && i.emit("touchMove", l), !(l.targetTouches && 1 < l.targetTouches.length)) {
                                        a.currentX = d, a.currentY = c;
                                        var p = a.currentX - a.startX,
                                            h = a.currentY - a.startY;
                                        if (!(i.params.threshold && Math.sqrt(Math.pow(p, 2) + Math.pow(h, 2)) < i.params.threshold)) {
                                            var f;
                                            if (void 0 === n.isScrolling && (i.isHorizontal() && a.currentY === a.startY || i.isVertical() && a.currentX === a.startX ? n.isScrolling = !1 : 25 <= p * p + h * h && (f = 180 * Math.atan2(Math.abs(h), Math.abs(p)) / Math.PI, n.isScrolling = i.isHorizontal() ? f > o.touchAngle : 90 - f > o.touchAngle)), n.isScrolling && i.emit("touchMoveOpposite", l), void 0 === n.startMoving && (a.currentX === a.startX && a.currentY === a.startY || (n.startMoving = !0)), n.isScrolling) n.isTouched = !1;
                                            else if (n.startMoving) {
                                                i.allowClick = !1, l.preventDefault(), o.touchMoveStopPropagation && !o.nested && l.stopPropagation(), n.isMoved || (o.loop && i.loopFix(), n.startTranslate = i.getTranslate(), i.setTransition(0), i.animating && i.$wrapperEl.trigger("webkitTransitionEnd transitionend"), n.allowMomentumBounce = !1, !o.grabCursor || !0 !== i.allowSlideNext && !0 !== i.allowSlidePrev || i.setGrabCursor(!0), i.emit("sliderFirstMove", l)), i.emit("sliderMove", l), n.isMoved = !0;
                                                var v = i.isHorizontal() ? p : h;
                                                a.diff = v, v *= o.touchRatio, r && (v = -v), i.swipeDirection = 0 < v ? "prev" : "next", n.currentTranslate = v + n.startTranslate;
                                                var m = !0,
                                                    g = o.resistanceRatio;
                                                if (o.touchReleaseOnEdges && (g = 0), 0 < v && n.currentTranslate > i.minTranslate() ? (m = !1, o.resistance && (n.currentTranslate = i.minTranslate() - 1 + Math.pow(-i.minTranslate() + n.startTranslate + v, g))) : v < 0 && n.currentTranslate < i.maxTranslate() && (m = !1, o.resistance && (n.currentTranslate = i.maxTranslate() + 1 - Math.pow(i.maxTranslate() - n.startTranslate - v, g))), m && (l.preventedByNestedSwiper = !0), !i.allowSlideNext && "next" === i.swipeDirection && n.currentTranslate < n.startTranslate && (n.currentTranslate = n.startTranslate), !i.allowSlidePrev && "prev" === i.swipeDirection && n.currentTranslate > n.startTranslate && (n.currentTranslate = n.startTranslate), 0 < o.threshold) {
                                                    if (!(Math.abs(v) > o.threshold || n.allowThresholdMove)) return void(n.currentTranslate = n.startTranslate);
                                                    if (!n.allowThresholdMove) return n.allowThresholdMove = !0, a.startX = a.currentX, a.startY = a.currentY, n.currentTranslate = n.startTranslate, void(a.diff = i.isHorizontal() ? a.currentX - a.startX : a.currentY - a.startY)
                                                }
                                                o.followFinger && ((o.freeMode || o.watchSlidesProgress || o.watchSlidesVisibility) && (i.updateActiveIndex(), i.updateSlidesClasses()), o.freeMode && (0 === n.velocities.length && n.velocities.push({
                                                    position: a[i.isHorizontal() ? "startX" : "startY"],
                                                    time: n.touchStartTime
                                                }), n.velocities.push({
                                                    position: a[i.isHorizontal() ? "currentX" : "currentY"],
                                                    time: u.now()
                                                })), i.updateProgress(n.currentTranslate), i.setTranslate(n.currentTranslate))
                                            }
                                        }
                                    }
                                }
                            } else n.startMoving && n.isScrolling && i.emit("touchMoveOpposite", l)
                        }.bind(t), t.onTouchEnd = function (e) {
                            var t = this,
                                i = t.touchEventsData,
                                n = t.params,
                                s = t.touches,
                                o = t.rtlTranslate,
                                a = t.$wrapperEl,
                                r = t.slidesGrid,
                                l = t.snapGrid,
                                d = e;
                            if (d.originalEvent && (d = d.originalEvent), i.allowTouchCallbacks && t.emit("touchEnd", d), i.allowTouchCallbacks = !1, !i.isTouched) return i.isMoved && n.grabCursor && t.setGrabCursor(!1), i.isMoved = !1, void(i.startMoving = !1);
                            n.grabCursor && i.isMoved && i.isTouched && (!0 === t.allowSlideNext || !0 === t.allowSlidePrev) && t.setGrabCursor(!1);
                            var c, p = u.now(),
                                h = p - i.touchStartTime;
                            if (t.allowClick && (t.updateClickedSlide(d), t.emit("tap", d), h < 300 && 300 < p - i.lastClickTime && (i.clickTimeout && clearTimeout(i.clickTimeout), i.clickTimeout = u.nextTick(function () {
                                    t && !t.destroyed && t.emit("click", d)
                                }, 300)), h < 300 && p - i.lastClickTime < 300 && (i.clickTimeout && clearTimeout(i.clickTimeout), t.emit("doubleTap", d))), i.lastClickTime = u.now(), u.nextTick(function () {
                                    t.destroyed || (t.allowClick = !0)
                                }), !i.isTouched || !i.isMoved || !t.swipeDirection || 0 === s.diff || i.currentTranslate === i.startTranslate) return i.isTouched = !1, i.isMoved = !1, void(i.startMoving = !1);
                            if (i.isTouched = !1, i.isMoved = !1, i.startMoving = !1, c = n.followFinger ? o ? t.translate : -t.translate : -i.currentTranslate, n.freeMode) {
                                if (c < -t.minTranslate()) return void t.slideTo(t.activeIndex);
                                if (c > -t.maxTranslate()) return void(t.slides.length < l.length ? t.slideTo(l.length - 1) : t.slideTo(t.slides.length - 1));
                                if (n.freeModeMomentum) {
                                    if (1 < i.velocities.length) {
                                        var f = i.velocities.pop(),
                                            v = i.velocities.pop(),
                                            m = f.position - v.position,
                                            g = f.time - v.time;
                                        t.velocity = m / g, t.velocity /= 2, Math.abs(t.velocity) < n.freeModeMinimumVelocity && (t.velocity = 0), (150 < g || 300 < u.now() - f.time) && (t.velocity = 0)
                                    } else t.velocity = 0;
                                    t.velocity *= n.freeModeMomentumVelocityRatio, i.velocities.length = 0;
                                    var w = 1e3 * n.freeModeMomentumRatio,
                                        y = t.velocity * w,
                                        b = t.translate + y;
                                    o && (b = -b);
                                    var T, x, S = !1,
                                        C = 20 * Math.abs(t.velocity) * n.freeModeMomentumBounceRatio;
                                    if (b < t.maxTranslate()) n.freeModeMomentumBounce ? (b + t.maxTranslate() < -C && (b = t.maxTranslate() - C), T = t.maxTranslate(), S = !0, i.allowMomentumBounce = !0) : b = t.maxTranslate(), n.loop && n.centeredSlides && (x = !0);
                                    else if (b > t.minTranslate()) n.freeModeMomentumBounce ? (b - t.minTranslate() > C && (b = t.minTranslate() + C), T = t.minTranslate(), S = !0, i.allowMomentumBounce = !0) : b = t.minTranslate(), n.loop && n.centeredSlides && (x = !0);
                                    else if (n.freeModeSticky) {
                                        for (var k, E = 0; E < l.length; E += 1)
                                            if (l[E] > -b) {
                                                k = E;
                                                break
                                            } b = -(b = Math.abs(l[k] - b) < Math.abs(l[k - 1] - b) || "next" === t.swipeDirection ? l[k] : l[k - 1])
                                    }
                                    if (x && t.once("transitionEnd", function () {
                                            t.loopFix()
                                        }), 0 !== t.velocity) w = o ? Math.abs((-b - t.translate) / t.velocity) : Math.abs((b - t.translate) / t.velocity);
                                    else if (n.freeModeSticky) return void t.slideToClosest();
                                    n.freeModeMomentumBounce && S ? (t.updateProgress(T), t.setTransition(w), t.setTranslate(b), t.transitionStart(!0, t.swipeDirection), t.animating = !0, a.transitionEnd(function () {
                                        t && !t.destroyed && i.allowMomentumBounce && (t.emit("momentumBounce"), t.setTransition(n.speed), t.setTranslate(T), a.transitionEnd(function () {
                                            t && !t.destroyed && t.transitionEnd()
                                        }))
                                    })) : t.velocity ? (t.updateProgress(b), t.setTransition(w), t.setTranslate(b), t.transitionStart(!0, t.swipeDirection), t.animating || (t.animating = !0, a.transitionEnd(function () {
                                        t && !t.destroyed && t.transitionEnd()
                                    }))) : t.updateProgress(b), t.updateActiveIndex(), t.updateSlidesClasses()
                                } else if (n.freeModeSticky) return void t.slideToClosest();
                                (!n.freeModeMomentum || h >= n.longSwipesMs) && (t.updateProgress(), t.updateActiveIndex(), t.updateSlidesClasses())
                            } else {
                                for (var $ = 0, M = t.slidesSizesGrid[0], P = 0; P < r.length; P += n.slidesPerGroup) void 0 !== r[P + n.slidesPerGroup] ? c >= r[P] && c < r[P + n.slidesPerGroup] && (M = r[($ = P) + n.slidesPerGroup] - r[P]) : c >= r[P] && ($ = P, M = r[r.length - 1] - r[r.length - 2]);
                                var z = (c - r[$]) / M;
                                if (h > n.longSwipesMs) {
                                    if (!n.longSwipes) return void t.slideTo(t.activeIndex);
                                    "next" === t.swipeDirection && (z >= n.longSwipesRatio ? t.slideTo($ + n.slidesPerGroup) : t.slideTo($)), "prev" === t.swipeDirection && (z > 1 - n.longSwipesRatio ? t.slideTo($ + n.slidesPerGroup) : t.slideTo($))
                                } else {
                                    if (!n.shortSwipes) return void t.slideTo(t.activeIndex);
                                    "next" === t.swipeDirection && t.slideTo($ + n.slidesPerGroup), "prev" === t.swipeDirection && t.slideTo($)
                                }
                            }
                        }.bind(t), t.onClick = function (e) {
                            this.allowClick || (this.params.preventClicks && e.preventDefault(), this.params.preventClicksPropagation && this.animating && (e.stopPropagation(), e.stopImmediatePropagation()))
                        }.bind(t);
                        var d = "container" === i.touchEventsTarget ? r : l,
                            c = !!i.nested;
                        if (h.touch || !h.pointerEvents && !h.prefixedPointerEvents) {
                            if (h.touch) {
                                var p = !("touchstart" !== a.start || !h.passiveListener || !i.passiveListeners) && {
                                    passive: !0,
                                    capture: !1
                                };
                                d.addEventListener(a.start, t.onTouchStart, p), d.addEventListener(a.move, t.onTouchMove, h.passiveListener ? {
                                    passive: !1,
                                    capture: c
                                } : c), d.addEventListener(a.end, t.onTouchEnd, p)
                            }(i.simulateTouch && !S.ios && !S.android || i.simulateTouch && !h.touch && S.ios) && (d.addEventListener("mousedown", t.onTouchStart, !1), s.addEventListener("mousemove", t.onTouchMove, c), s.addEventListener("mouseup", t.onTouchEnd, !1))
                        } else d.addEventListener(a.start, t.onTouchStart, !1), s.addEventListener(a.move, t.onTouchMove, c), s.addEventListener(a.end, t.onTouchEnd, !1);
                        (i.preventClicks || i.preventClicksPropagation) && d.addEventListener("click", t.onClick, !0), t.on(S.ios || S.android ? "resize orientationchange observerUpdate" : "resize observerUpdate", n, !0)
                    },
                    detachEvents: function () {
                        var e = this,
                            t = e.params,
                            i = e.touchEvents,
                            o = e.el,
                            a = e.wrapperEl,
                            r = "container" === t.touchEventsTarget ? o : a,
                            l = !!t.nested;
                        if (h.touch || !h.pointerEvents && !h.prefixedPointerEvents) {
                            if (h.touch) {
                                var d = !("onTouchStart" !== i.start || !h.passiveListener || !t.passiveListeners) && {
                                    passive: !0,
                                    capture: !1
                                };
                                r.removeEventListener(i.start, e.onTouchStart, d), r.removeEventListener(i.move, e.onTouchMove, l), r.removeEventListener(i.end, e.onTouchEnd, d)
                            }(t.simulateTouch && !S.ios && !S.android || t.simulateTouch && !h.touch && S.ios) && (r.removeEventListener("mousedown", e.onTouchStart, !1), s.removeEventListener("mousemove", e.onTouchMove, l), s.removeEventListener("mouseup", e.onTouchEnd, !1))
                        } else r.removeEventListener(i.start, e.onTouchStart, !1), s.removeEventListener(i.move, e.onTouchMove, l), s.removeEventListener(i.end, e.onTouchEnd, !1);
                        (t.preventClicks || t.preventClicksPropagation) && r.removeEventListener("click", e.onClick, !0), e.off(S.ios || S.android ? "resize orientationchange observerUpdate" : "resize observerUpdate", n)
                    }
                },
                breakpoints: {
                    setBreakpoint: function () {
                        var e = this,
                            t = e.activeIndex,
                            i = e.initialized,
                            n = e.loopedSlides;
                        void 0 === n && (n = 0);
                        var s = e.params,
                            o = s.breakpoints;
                        if (o && (!o || 0 !== Object.keys(o).length)) {
                            var a = e.getBreakpoint(o);
                            if (a && e.currentBreakpoint !== a) {
                                var r = a in o ? o[a] : void 0;
                                r && ["slidesPerView", "spaceBetween", "slidesPerGroup"].forEach(function (e) {
                                    var t = r[e];
                                    void 0 !== t && (r[e] = "slidesPerView" !== e || "AUTO" !== t && "auto" !== t ? "slidesPerView" === e ? parseFloat(t) : parseInt(t, 10) : "auto")
                                });
                                var l = r || e.originalParams,
                                    d = l.direction && l.direction !== s.direction,
                                    c = s.loop && (l.slidesPerView !== s.slidesPerView || d);
                                d && i && e.changeDirection(), u.extend(e.params, l), u.extend(e, {
                                    allowTouchMove: e.params.allowTouchMove,
                                    allowSlideNext: e.params.allowSlideNext,
                                    allowSlidePrev: e.params.allowSlidePrev
                                }), e.currentBreakpoint = a, c && i && (e.loopDestroy(), e.loopCreate(), e.updateSlides(), e.slideTo(t - n + e.loopedSlides, 0, !1)), e.emit("breakpoint", l)
                            }
                        }
                    },
                    getBreakpoint: function (e) {
                        if (e) {
                            var t = !1,
                                i = [];
                            Object.keys(e).forEach(function (e) {
                                i.push(e)
                            }), i.sort(function (e, t) {
                                return parseInt(e, 10) - parseInt(t, 10)
                            });
                            for (var n = 0; n < i.length; n += 1) {
                                var s = i[n];
                                this.params.breakpointsInverse ? s <= o.innerWidth && (t = s) : s >= o.innerWidth && !t && (t = s)
                            }
                            return t || "max"
                        }
                    }
                },
                checkOverflow: {
                    checkOverflow: function () {
                        var e = this,
                            t = e.isLocked;
                        e.isLocked = 1 === e.snapGrid.length, e.allowSlideNext = !e.isLocked, e.allowSlidePrev = !e.isLocked, t !== e.isLocked && e.emit(e.isLocked ? "lock" : "unlock"), t && t !== e.isLocked && (e.isEnd = !1, e.navigation.update())
                    }
                },
                classes: {
                    addClasses: function () {
                        var e = this.classNames,
                            t = this.params,
                            i = this.rtl,
                            n = this.$el,
                            s = [];
                        s.push("initialized"), s.push(t.direction), t.freeMode && s.push("free-mode"), h.flexbox || s.push("no-flexbox"), t.autoHeight && s.push("autoheight"), i && s.push("rtl"), 1 < t.slidesPerColumn && s.push("multirow"), S.android && s.push("android"), S.ios && s.push("ios"), (f.isIE || f.isEdge) && (h.pointerEvents || h.prefixedPointerEvents) && s.push("wp8-" + t.direction), s.forEach(function (i) {
                            e.push(t.containerModifierClass + i)
                        }), n.addClass(e.join(" "))
                    },
                    removeClasses: function () {
                        var e = this.$el,
                            t = this.classNames;
                        e.removeClass(t.join(" "))
                    }
                },
                images: {
                    loadImage: function (e, t, i, n, s, a) {
                        function r() {
                            a && a()
                        }
                        var l;
                        e.complete && s ? r() : t ? ((l = new o.Image).onload = r, l.onerror = r, n && (l.sizes = n), i && (l.srcset = i), t && (l.src = t)) : r()
                    },
                    preloadImages: function () {
                        function e() {
                            null != t && t && !t.destroyed && (void 0 !== t.imagesLoaded && (t.imagesLoaded += 1), t.imagesLoaded === t.imagesToLoad.length && (t.params.updateOnImagesReady && t.update(), t.emit("imagesReady")))
                        }
                        var t = this;
                        t.imagesToLoad = t.$el.find("img");
                        for (var i = 0; i < t.imagesToLoad.length; i += 1) {
                            var n = t.imagesToLoad[i];
                            t.loadImage(n, n.currentSrc || n.getAttribute("src"), n.srcset || n.getAttribute("srcset"), n.sizes || n.getAttribute("sizes"), !0, e)
                        }
                    }
                }
            },
            E = {},
            $ = function (t) {
                function i() {
                    for (var n, s, o, a = [], r = arguments.length; r--;) a[r] = arguments[r];
                    o = (o = 1 === a.length && a[0].constructor && a[0].constructor === Object ? a[0] : (s = (n = a)[0], n[1])) || {}, o = u.extend({}, o), s && !o.el && (o.el = s), t.call(this, o), Object.keys(k).forEach(function (e) {
                        Object.keys(k[e]).forEach(function (t) {
                            i.prototype[t] || (i.prototype[t] = k[e][t])
                        })
                    });
                    var l = this;
                    void 0 === l.modules && (l.modules = {}), Object.keys(l.modules).forEach(function (e) {
                        var t = l.modules[e];
                        if (t.params) {
                            var i = Object.keys(t.params)[0],
                                n = t.params[i];
                            if ("object" != typeof n || null === n) return;
                            if (!(i in o && "enabled" in n)) return;
                            !0 === o[i] && (o[i] = {
                                enabled: !0
                            }), "object" != typeof o[i] || "enabled" in o[i] || (o[i].enabled = !0), o[i] || (o[i] = {
                                enabled: !1
                            })
                        }
                    });
                    var d = u.extend({}, C);
                    l.useModulesParams(d), l.params = u.extend({}, d, E, o), l.originalParams = u.extend({}, l.params), l.passedParams = u.extend({}, o);
                    var c = (l.$ = e)(l.params.el);
                    if (s = c[0]) {
                        if (1 < c.length) {
                            var p = [];
                            return c.each(function (e, t) {
                                var n = u.extend({}, o, {
                                    el: t
                                });
                                p.push(new i(n))
                            }), p
                        }
                        s.swiper = l, c.data("swiper", l);
                        var f, v, m = c.children("." + l.params.wrapperClass);
                        return u.extend(l, {
                            $el: c,
                            el: s,
                            $wrapperEl: m,
                            wrapperEl: m[0],
                            classNames: [],
                            slides: e(),
                            slidesGrid: [],
                            snapGrid: [],
                            slidesSizesGrid: [],
                            isHorizontal: function () {
                                return "horizontal" === l.params.direction
                            },
                            isVertical: function () {
                                return "vertical" === l.params.direction
                            },
                            rtl: "rtl" === s.dir.toLowerCase() || "rtl" === c.css("direction"),
                            rtlTranslate: "horizontal" === l.params.direction && ("rtl" === s.dir.toLowerCase() || "rtl" === c.css("direction")),
                            wrongRTL: "-webkit-box" === m.css("display"),
                            activeIndex: 0,
                            realIndex: 0,
                            isBeginning: !0,
                            isEnd: !1,
                            translate: 0,
                            previousTranslate: 0,
                            progress: 0,
                            velocity: 0,
                            animating: !1,
                            allowSlideNext: l.params.allowSlideNext,
                            allowSlidePrev: l.params.allowSlidePrev,
                            touchEvents: (f = ["touchstart", "touchmove", "touchend"], v = ["mousedown", "mousemove", "mouseup"], h.pointerEvents ? v = ["pointerdown", "pointermove", "pointerup"] : h.prefixedPointerEvents && (v = ["MSPointerDown", "MSPointerMove", "MSPointerUp"]), l.touchEventsTouch = {
                                start: f[0],
                                move: f[1],
                                end: f[2]
                            }, l.touchEventsDesktop = {
                                start: v[0],
                                move: v[1],
                                end: v[2]
                            }, h.touch || !l.params.simulateTouch ? l.touchEventsTouch : l.touchEventsDesktop),
                            touchEventsData: {
                                isTouched: void 0,
                                isMoved: void 0,
                                allowTouchCallbacks: void 0,
                                touchStartTime: void 0,
                                isScrolling: void 0,
                                currentTranslate: void 0,
                                startTranslate: void 0,
                                allowThresholdMove: void 0,
                                formElements: "input, select, option, textarea, button, video",
                                lastClickTime: u.now(),
                                clickTimeout: void 0,
                                velocities: [],
                                allowMomentumBounce: void 0,
                                isTouchEvent: void 0,
                                startMoving: void 0
                            },
                            allowClick: !0,
                            allowTouchMove: l.params.allowTouchMove,
                            touches: {
                                startX: 0,
                                startY: 0,
                                currentX: 0,
                                currentY: 0,
                                diff: 0
                            },
                            imagesToLoad: [],
                            imagesLoaded: 0
                        }), l.useModules(), l.params.init && l.init(), l
                    }
                }
                t && (i.__proto__ = t);
                var n = {
                    extendedDefaults: {
                        configurable: !0
                    },
                    defaults: {
                        configurable: !0
                    },
                    Class: {
                        configurable: !0
                    },
                    $: {
                        configurable: !0
                    }
                };
                return ((i.prototype = Object.create(t && t.prototype)).constructor = i).prototype.slidesPerViewDynamic = function () {
                    var e = this,
                        t = e.params,
                        i = e.slides,
                        n = e.slidesGrid,
                        s = e.size,
                        o = e.activeIndex,
                        a = 1;
                    if (t.centeredSlides) {
                        for (var r, l = i[o].swiperSlideSize, d = o + 1; d < i.length; d += 1) i[d] && !r && (a += 1, s < (l += i[d].swiperSlideSize) && (r = !0));
                        for (var c = o - 1; 0 <= c; c -= 1) i[c] && !r && (a += 1, s < (l += i[c].swiperSlideSize) && (r = !0))
                    } else
                        for (var p = o + 1; p < i.length; p += 1) n[p] - n[o] < s && (a += 1);
                    return a
                }, i.prototype.update = function () {
                    function e() {
                        var e = t.rtlTranslate ? -1 * t.translate : t.translate,
                            i = Math.min(Math.max(e, t.maxTranslate()), t.minTranslate());
                        t.setTranslate(i), t.updateActiveIndex(), t.updateSlidesClasses()
                    }
                    var t = this;
                    if (t && !t.destroyed) {
                        var i = t.snapGrid,
                            n = t.params;
                        n.breakpoints && t.setBreakpoint(), t.updateSize(), t.updateSlides(), t.updateProgress(), t.updateSlidesClasses(), t.params.freeMode ? (e(), t.params.autoHeight && t.updateAutoHeight()) : (("auto" === t.params.slidesPerView || 1 < t.params.slidesPerView) && t.isEnd && !t.params.centeredSlides ? t.slideTo(t.slides.length - 1, 0, !1, !0) : t.slideTo(t.activeIndex, 0, !1, !0)) || e(), n.watchOverflow && i !== t.snapGrid && t.checkOverflow(), t.emit("update")
                    }
                }, i.prototype.changeDirection = function (e, t) {
                    void 0 === t && (t = !0);
                    var i = this,
                        n = i.params.direction;
                    return (e = e || ("horizontal" === n ? "vertical" : "horizontal")) === n || "horizontal" !== e && "vertical" !== e || (i.$el.removeClass("" + i.params.containerModifierClass + n + " wp8-" + n).addClass("" + i.params.containerModifierClass + e), (f.isIE || f.isEdge) && (h.pointerEvents || h.prefixedPointerEvents) && i.$el.addClass(i.params.containerModifierClass + "wp8-" + e), i.params.direction = e, i.slides.each(function (t, i) {
                        "vertical" === e ? i.style.width = "" : i.style.height = ""
                    }), i.emit("changeDirection"), t && i.update()), i
                }, i.prototype.init = function () {
                    var e = this;
                    e.initialized || (e.emit("beforeInit"), e.params.breakpoints && e.setBreakpoint(), e.addClasses(), e.params.loop && e.loopCreate(), e.updateSize(), e.updateSlides(), e.params.watchOverflow && e.checkOverflow(), e.params.grabCursor && e.setGrabCursor(), e.params.preloadImages && e.preloadImages(), e.params.loop ? e.slideTo(e.params.initialSlide + e.loopedSlides, 0, e.params.runCallbacksOnInit) : e.slideTo(e.params.initialSlide, 0, e.params.runCallbacksOnInit), e.attachEvents(), e.initialized = !0, e.emit("init"))
                }, i.prototype.destroy = function (e, t) {
                    void 0 === e && (e = !0), void 0 === t && (t = !0);
                    var i = this,
                        n = i.params,
                        s = i.$el,
                        o = i.$wrapperEl,
                        a = i.slides;
                    return void 0 === i.params || i.destroyed || (i.emit("beforeDestroy"), i.initialized = !1, i.detachEvents(), n.loop && i.loopDestroy(), t && (i.removeClasses(), s.removeAttr("style"), o.removeAttr("style"), a && a.length && a.removeClass([n.slideVisibleClass, n.slideActiveClass, n.slideNextClass, n.slidePrevClass].join(" ")).removeAttr("style").removeAttr("data-swiper-slide-index").removeAttr("data-swiper-column").removeAttr("data-swiper-row")), i.emit("destroy"), Object.keys(i.eventsListeners).forEach(function (e) {
                        i.off(e)
                    }), !1 !== e && (i.$el[0].swiper = null, i.$el.data("swiper", null), u.deleteProps(i)), i.destroyed = !0), null
                }, i.extendDefaults = function (e) {
                    u.extend(E, e)
                }, n.extendedDefaults.get = function () {
                    return E
                }, n.defaults.get = function () {
                    return C
                }, n.Class.get = function () {
                    return t
                }, n.$.get = function () {
                    return e
                }, Object.defineProperties(i, n), i
            }(i),
            M = {
                name: "device",
                proto: {
                    device: S
                },
                static: {
                    device: S
                }
            },
            P = {
                name: "support",
                proto: {
                    support: h
                },
                static: {
                    support: h
                }
            },
            z = {
                name: "browser",
                proto: {
                    browser: f
                },
                static: {
                    browser: f
                }
            },
            L = {
                name: "resize",
                create: function () {
                    var e = this;
                    u.extend(e, {
                        resize: {
                            resizeHandler: function () {
                                e && !e.destroyed && e.initialized && (e.emit("beforeResize"), e.emit("resize"))
                            },
                            orientationChangeHandler: function () {
                                e && !e.destroyed && e.initialized && e.emit("orientationchange")
                            }
                        }
                    })
                },
                on: {
                    init: function () {
                        o.addEventListener("resize", this.resize.resizeHandler), o.addEventListener("orientationchange", this.resize.orientationChangeHandler)
                    },
                    destroy: function () {
                        o.removeEventListener("resize", this.resize.resizeHandler), o.removeEventListener("orientationchange", this.resize.orientationChangeHandler)
                    }
                }
            },
            A = {
                func: o.MutationObserver || o.WebkitMutationObserver,
                attach: function (e, t) {
                    void 0 === t && (t = {});
                    var i = this,
                        n = new A.func(function (e) {
                            if (1 !== e.length) {
                                var t = function () {
                                    i.emit("observerUpdate", e[0])
                                };
                                o.requestAnimationFrame ? o.requestAnimationFrame(t) : o.setTimeout(t, 0)
                            } else i.emit("observerUpdate", e[0])
                        });
                    n.observe(e, {
                        attributes: void 0 === t.attributes || t.attributes,
                        childList: void 0 === t.childList || t.childList,
                        characterData: void 0 === t.characterData || t.characterData
                    }), i.observer.observers.push(n)
                },
                init: function () {
                    var e = this;
                    if (h.observer && e.params.observer) {
                        if (e.params.observeParents)
                            for (var t = e.$el.parents(), i = 0; i < t.length; i += 1) e.observer.attach(t[i]);
                        e.observer.attach(e.$el[0], {
                            childList: e.params.observeSlideChildren
                        }), e.observer.attach(e.$wrapperEl[0], {
                            attributes: !1
                        })
                    }
                },
                destroy: function () {
                    this.observer.observers.forEach(function (e) {
                        e.disconnect()
                    }), this.observer.observers = []
                }
            },
            O = {
                name: "observer",
                params: {
                    observer: !1,
                    observeParents: !1,
                    observeSlideChildren: !1
                },
                create: function () {
                    u.extend(this, {
                        observer: {
                            init: A.init.bind(this),
                            attach: A.attach.bind(this),
                            destroy: A.destroy.bind(this),
                            observers: []
                        }
                    })
                },
                on: {
                    init: function () {
                        this.observer.init()
                    },
                    destroy: function () {
                        this.observer.destroy()
                    }
                }
            },
            D = {
                update: function (e) {
                    function t() {
                        i.updateSlides(), i.updateProgress(), i.updateSlidesClasses(), i.lazy && i.params.lazy.enabled && i.lazy.load()
                    }
                    var i = this,
                        n = i.params,
                        s = n.slidesPerView,
                        o = n.slidesPerGroup,
                        a = n.centeredSlides,
                        r = i.params.virtual,
                        l = r.addSlidesBefore,
                        d = r.addSlidesAfter,
                        c = i.virtual,
                        p = c.from,
                        h = c.to,
                        f = c.slides,
                        v = c.slidesGrid,
                        m = c.renderSlide,
                        g = c.offset;
                    i.updateActiveIndex();
                    var w, y, b, T = i.activeIndex || 0;
                    w = i.rtlTranslate ? "right" : i.isHorizontal() ? "left" : "top", b = a ? (y = Math.floor(s / 2) + o + l, Math.floor(s / 2) + o + d) : (y = s + (o - 1) + l, o + d);
                    var x = Math.max((T || 0) - b, 0),
                        S = Math.min((T || 0) + y, f.length - 1),
                        C = (i.slidesGrid[x] || 0) - (i.slidesGrid[0] || 0);
                    if (u.extend(i.virtual, {
                            from: x,
                            to: S,
                            offset: C,
                            slidesGrid: i.slidesGrid
                        }), p === x && h === S && !e) return i.slidesGrid !== v && C !== g && i.slides.css(w, C + "px"), void i.updateProgress();
                    if (i.params.virtual.renderExternal) return i.params.virtual.renderExternal.call(i, {
                        offset: C,
                        from: x,
                        to: S,
                        slides: function () {
                            for (var e = [], t = x; t <= S; t += 1) e.push(f[t]);
                            return e
                        }()
                    }), void t();
                    var k = [],
                        E = [];
                    if (e) i.$wrapperEl.find("." + i.params.slideClass).remove();
                    else
                        for (var $ = p; $ <= h; $ += 1)($ < x || S < $) && i.$wrapperEl.find("." + i.params.slideClass + '[data-swiper-slide-index="' + $ + '"]').remove();
                    for (var M = 0; M < f.length; M += 1) x <= M && M <= S && (void 0 === h || e ? E.push(M) : (h < M && E.push(M), M < p && k.push(M)));
                    E.forEach(function (e) {
                        i.$wrapperEl.append(m(f[e], e))
                    }), k.sort(function (e, t) {
                        return t - e
                    }).forEach(function (e) {
                        i.$wrapperEl.prepend(m(f[e], e))
                    }), i.$wrapperEl.children(".swiper-slide").css(w, C + "px"), t()
                },
                renderSlide: function (t, i) {
                    var n = this,
                        s = n.params.virtual;
                    if (s.cache && n.virtual.cache[i]) return n.virtual.cache[i];
                    var o = e(s.renderSlide ? s.renderSlide.call(n, t, i) : '<div class="' + n.params.slideClass + '" data-swiper-slide-index="' + i + '">' + t + "</div>");
                    return o.attr("data-swiper-slide-index") || o.attr("data-swiper-slide-index", i), s.cache && (n.virtual.cache[i] = o), o
                },
                appendSlide: function (e) {
                    if ("object" == typeof e && "length" in e)
                        for (var t = 0; t < e.length; t += 1) e[t] && this.virtual.slides.push(e[t]);
                    else this.virtual.slides.push(e);
                    this.virtual.update(!0)
                },
                prependSlide: function (e) {
                    var t = this,
                        i = t.activeIndex,
                        n = i + 1,
                        s = 1;
                    if (Array.isArray(e)) {
                        for (var o = 0; o < e.length; o += 1) e[o] && t.virtual.slides.unshift(e[o]);
                        n = i + e.length, s = e.length
                    } else t.virtual.slides.unshift(e);
                    if (t.params.virtual.cache) {
                        var a = t.virtual.cache,
                            r = {};
                        Object.keys(a).forEach(function (e) {
                            r[parseInt(e, 10) + s] = a[e]
                        }), t.virtual.cache = r
                    }
                    t.virtual.update(!0), t.slideTo(n, 0)
                },
                removeSlide: function (e) {
                    var t = this;
                    if (null != e) {
                        var i = t.activeIndex;
                        if (Array.isArray(e))
                            for (var n = e.length - 1; 0 <= n; n -= 1) t.virtual.slides.splice(e[n], 1), t.params.virtual.cache && delete t.virtual.cache[e[n]], e[n] < i && (i -= 1), i = Math.max(i, 0);
                        else t.virtual.slides.splice(e, 1), t.params.virtual.cache && delete t.virtual.cache[e], e < i && (i -= 1), i = Math.max(i, 0);
                        t.virtual.update(!0), t.slideTo(i, 0)
                    }
                },
                removeAllSlides: function () {
                    var e = this;
                    e.virtual.slides = [], e.params.virtual.cache && (e.virtual.cache = {}), e.virtual.update(!0), e.slideTo(0, 0)
                }
            },
            I = {
                name: "virtual",
                params: {
                    virtual: {
                        enabled: !1,
                        slides: [],
                        cache: !0,
                        renderSlide: null,
                        renderExternal: null,
                        addSlidesBefore: 0,
                        addSlidesAfter: 0
                    }
                },
                create: function () {
                    var e = this;
                    u.extend(e, {
                        virtual: {
                            update: D.update.bind(e),
                            appendSlide: D.appendSlide.bind(e),
                            prependSlide: D.prependSlide.bind(e),
                            removeSlide: D.removeSlide.bind(e),
                            removeAllSlides: D.removeAllSlides.bind(e),
                            renderSlide: D.renderSlide.bind(e),
                            slides: e.params.virtual.slides,
                            cache: {}
                        }
                    })
                },
                on: {
                    beforeInit: function () {
                        var e = this;
                        if (e.params.virtual.enabled) {
                            e.classNames.push(e.params.containerModifierClass + "virtual");
                            var t = {
                                watchSlidesProgress: !0
                            };
                            u.extend(e.params, t), u.extend(e.originalParams, t), e.params.initialSlide || e.virtual.update()
                        }
                    },
                    setTranslate: function () {
                        this.params.virtual.enabled && this.virtual.update()
                    }
                }
            },
            H = {
                handle: function (e) {
                    var t = this,
                        i = t.rtlTranslate,
                        n = e;
                    n.originalEvent && (n = n.originalEvent);
                    var a = n.keyCode || n.charCode;
                    if (!t.allowSlideNext && (t.isHorizontal() && 39 === a || t.isVertical() && 40 === a || 34 === a)) return !1;
                    if (!t.allowSlidePrev && (t.isHorizontal() && 37 === a || t.isVertical() && 38 === a || 33 === a)) return !1;
                    if (!(n.shiftKey || n.altKey || n.ctrlKey || n.metaKey || s.activeElement && s.activeElement.nodeName && ("input" === s.activeElement.nodeName.toLowerCase() || "textarea" === s.activeElement.nodeName.toLowerCase()))) {
                        if (t.params.keyboard.onlyInViewport && (33 === a || 34 === a || 37 === a || 39 === a || 38 === a || 40 === a)) {
                            var r = !1;
                            if (0 < t.$el.parents("." + t.params.slideClass).length && 0 === t.$el.parents("." + t.params.slideActiveClass).length) return;
                            var l = o.innerWidth,
                                d = o.innerHeight,
                                c = t.$el.offset();
                            i && (c.left -= t.$el[0].scrollLeft);
                            for (var p = [
                                    [c.left, c.top],
                                    [c.left + t.width, c.top],
                                    [c.left, c.top + t.height],
                                    [c.left + t.width, c.top + t.height]
                                ], u = 0; u < p.length; u += 1) {
                                var h = p[u];
                                0 <= h[0] && h[0] <= l && 0 <= h[1] && h[1] <= d && (r = !0)
                            }
                            if (!r) return
                        }
                        t.isHorizontal() ? (33 !== a && 34 !== a && 37 !== a && 39 !== a || (n.preventDefault ? n.preventDefault() : n.returnValue = !1), (34 !== a && 39 !== a || i) && (33 !== a && 37 !== a || !i) || t.slideNext(), (33 !== a && 37 !== a || i) && (34 !== a && 39 !== a || !i) || t.slidePrev()) : (33 !== a && 34 !== a && 38 !== a && 40 !== a || (n.preventDefault ? n.preventDefault() : n.returnValue = !1), 34 !== a && 40 !== a || t.slideNext(), 33 !== a && 38 !== a || t.slidePrev()), t.emit("keyPress", a)
                    }
                },
                enable: function () {
                    this.keyboard.enabled || (e(s).on("keydown", this.keyboard.handle), this.keyboard.enabled = !0)
                },
                disable: function () {
                    this.keyboard.enabled && (e(s).off("keydown", this.keyboard.handle), this.keyboard.enabled = !1)
                }
            },
            X = {
                name: "keyboard",
                params: {
                    keyboard: {
                        enabled: !1,
                        onlyInViewport: !0
                    }
                },
                create: function () {
                    u.extend(this, {
                        keyboard: {
                            enabled: !1,
                            enable: H.enable.bind(this),
                            disable: H.disable.bind(this),
                            handle: H.handle.bind(this)
                        }
                    })
                },
                on: {
                    init: function () {
                        this.params.keyboard.enabled && this.keyboard.enable()
                    },
                    destroy: function () {
                        this.keyboard.enabled && this.keyboard.disable()
                    }
                }
            },
            Y = {
                lastScrollTime: u.now(),
                event: -1 < o.navigator.userAgent.indexOf("firefox") ? "DOMMouseScroll" : function () {
                    var e = "onwheel",
                        t = e in s;
                    if (!t) {
                        var i = s.createElement("div");
                        i.setAttribute(e, "return;"), t = "function" == typeof i[e]
                    }
                    return !t && s.implementation && s.implementation.hasFeature && !0 !== s.implementation.hasFeature("", "") && (t = s.implementation.hasFeature("Events.wheel", "3.0")), t
                }() ? "wheel" : "mousewheel",
                normalize: function (e) {
                    var t = 0,
                        i = 0,
                        n = 0,
                        s = 0;
                    return "detail" in e && (i = e.detail), "wheelDelta" in e && (i = -e.wheelDelta / 120), "wheelDeltaY" in e && (i = -e.wheelDeltaY / 120), "wheelDeltaX" in e && (t = -e.wheelDeltaX / 120), "axis" in e && e.axis === e.HORIZONTAL_AXIS && (t = i, i = 0), n = 10 * t, s = 10 * i, "deltaY" in e && (s = e.deltaY), "deltaX" in e && (n = e.deltaX), (n || s) && e.deltaMode && (1 === e.deltaMode ? (n *= 40, s *= 40) : (n *= 800, s *= 800)), n && !t && (t = n < 1 ? -1 : 1), s && !i && (i = s < 1 ? -1 : 1), {
                        spinX: t,
                        spinY: i,
                        pixelX: n,
                        pixelY: s
                    }
                },
                handleMouseEnter: function () {
                    this.mouseEntered = !0
                },
                handleMouseLeave: function () {
                    this.mouseEntered = !1
                },
                handle: function (e) {
                    var t = e,
                        i = this,
                        n = i.params.mousewheel;
                    if (!i.mouseEntered && !n.releaseOnEdges) return !0;
                    t.originalEvent && (t = t.originalEvent);
                    var s = 0,
                        a = i.rtlTranslate ? -1 : 1,
                        r = Y.normalize(t);
                    if (n.forceToAxis)
                        if (i.isHorizontal()) {
                            if (!(Math.abs(r.pixelX) > Math.abs(r.pixelY))) return !0;
                            s = r.pixelX * a
                        } else {
                            if (!(Math.abs(r.pixelY) > Math.abs(r.pixelX))) return !0;
                            s = r.pixelY
                        }
                    else s = Math.abs(r.pixelX) > Math.abs(r.pixelY) ? -r.pixelX * a : -r.pixelY;
                    if (0 === s) return !0;
                    if (n.invert && (s = -s), i.params.freeMode) {
                        i.params.loop && i.loopFix();
                        var l = i.getTranslate() + s * n.sensitivity,
                            d = i.isBeginning,
                            c = i.isEnd;
                        if (l >= i.minTranslate() && (l = i.minTranslate()), l <= i.maxTranslate() && (l = i.maxTranslate()), i.setTransition(0), i.setTranslate(l), i.updateProgress(), i.updateActiveIndex(), i.updateSlidesClasses(), (!d && i.isBeginning || !c && i.isEnd) && i.updateSlidesClasses(), i.params.freeModeSticky && (clearTimeout(i.mousewheel.timeout), i.mousewheel.timeout = u.nextTick(function () {
                                i.slideToClosest()
                            }, 300)), i.emit("scroll", t), i.params.autoplay && i.params.autoplayDisableOnInteraction && i.autoplay.stop(), l === i.minTranslate() || l === i.maxTranslate()) return !0
                    } else {
                        if (60 < u.now() - i.mousewheel.lastScrollTime)
                            if (s < 0)
                                if (i.isEnd && !i.params.loop || i.animating) {
                                    if (n.releaseOnEdges) return !0
                                } else i.slideNext(), i.emit("scroll", t);
                        else if (i.isBeginning && !i.params.loop || i.animating) {
                            if (n.releaseOnEdges) return !0
                        } else i.slidePrev(), i.emit("scroll", t);
                        i.mousewheel.lastScrollTime = (new o.Date).getTime()
                    }
                    return t.preventDefault ? t.preventDefault() : t.returnValue = !1, !1
                },
                enable: function () {
                    var t = this;
                    if (!Y.event) return !1;
                    if (t.mousewheel.enabled) return !1;
                    var i = t.$el;
                    return "container" !== t.params.mousewheel.eventsTarged && (i = e(t.params.mousewheel.eventsTarged)), i.on("mouseenter", t.mousewheel.handleMouseEnter), i.on("mouseleave", t.mousewheel.handleMouseLeave), i.on(Y.event, t.mousewheel.handle), t.mousewheel.enabled = !0
                },
                disable: function () {
                    var t = this;
                    if (!Y.event) return !1;
                    if (!t.mousewheel.enabled) return !1;
                    var i = t.$el;
                    return "container" !== t.params.mousewheel.eventsTarged && (i = e(t.params.mousewheel.eventsTarged)), i.off(Y.event, t.mousewheel.handle), !(t.mousewheel.enabled = !1)
                }
            },
            N = {
                update: function () {
                    var e = this,
                        t = e.params.navigation;
                    if (!e.params.loop) {
                        var i = e.navigation,
                            n = i.$nextEl,
                            s = i.$prevEl;
                        s && 0 < s.length && (e.isBeginning ? s.addClass(t.disabledClass) : s.removeClass(t.disabledClass), s[e.params.watchOverflow && e.isLocked ? "addClass" : "removeClass"](t.lockClass)), n && 0 < n.length && (e.isEnd ? n.addClass(t.disabledClass) : n.removeClass(t.disabledClass), n[e.params.watchOverflow && e.isLocked ? "addClass" : "removeClass"](t.lockClass))
                    }
                },
                onPrevClick: function (e) {
                    e.preventDefault(), this.isBeginning && !this.params.loop || this.slidePrev()
                },
                onNextClick: function (e) {
                    e.preventDefault(), this.isEnd && !this.params.loop || this.slideNext()
                },
                init: function () {
                    var t, i, n = this,
                        s = n.params.navigation;
                    (s.nextEl || s.prevEl) && (s.nextEl && (t = e(s.nextEl), n.params.uniqueNavElements && "string" == typeof s.nextEl && 1 < t.length && 1 === n.$el.find(s.nextEl).length && (t = n.$el.find(s.nextEl))), s.prevEl && (i = e(s.prevEl), n.params.uniqueNavElements && "string" == typeof s.prevEl && 1 < i.length && 1 === n.$el.find(s.prevEl).length && (i = n.$el.find(s.prevEl))), t && 0 < t.length && t.on("click", n.navigation.onNextClick), i && 0 < i.length && i.on("click", n.navigation.onPrevClick), u.extend(n.navigation, {
                        $nextEl: t,
                        nextEl: t && t[0],
                        $prevEl: i,
                        prevEl: i && i[0]
                    }))
                },
                destroy: function () {
                    var e = this,
                        t = e.navigation,
                        i = t.$nextEl,
                        n = t.$prevEl;
                    i && i.length && (i.off("click", e.navigation.onNextClick), i.removeClass(e.params.navigation.disabledClass)), n && n.length && (n.off("click", e.navigation.onPrevClick), n.removeClass(e.params.navigation.disabledClass))
                }
            },
            B = {
                update: function () {
                    var t = this,
                        i = t.rtl,
                        n = t.params.pagination;
                    if (n.el && t.pagination.el && t.pagination.$el && 0 !== t.pagination.$el.length) {
                        var s, o = t.virtual && t.params.virtual.enabled ? t.virtual.slides.length : t.slides.length,
                            a = t.pagination.$el,
                            r = t.params.loop ? Math.ceil((o - 2 * t.loopedSlides) / t.params.slidesPerGroup) : t.snapGrid.length;
                        if (t.params.loop ? ((s = Math.ceil((t.activeIndex - t.loopedSlides) / t.params.slidesPerGroup)) > o - 1 - 2 * t.loopedSlides && (s -= o - 2 * t.loopedSlides), r - 1 < s && (s -= r), s < 0 && "bullets" !== t.params.paginationType && (s = r + s)) : s = void 0 !== t.snapIndex ? t.snapIndex : t.activeIndex || 0, "bullets" === n.type && t.pagination.bullets && 0 < t.pagination.bullets.length) {
                            var l, d, c, p = t.pagination.bullets;
                            if (n.dynamicBullets && (t.pagination.bulletSize = p.eq(0)[t.isHorizontal() ? "outerWidth" : "outerHeight"](!0), a.css(t.isHorizontal() ? "width" : "height", t.pagination.bulletSize * (n.dynamicMainBullets + 4) + "px"), 1 < n.dynamicMainBullets && void 0 !== t.previousIndex && (t.pagination.dynamicBulletIndex += s - t.previousIndex, t.pagination.dynamicBulletIndex > n.dynamicMainBullets - 1 ? t.pagination.dynamicBulletIndex = n.dynamicMainBullets - 1 : t.pagination.dynamicBulletIndex < 0 && (t.pagination.dynamicBulletIndex = 0)), l = s - t.pagination.dynamicBulletIndex, c = ((d = l + (Math.min(p.length, n.dynamicMainBullets) - 1)) + l) / 2), p.removeClass(n.bulletActiveClass + " " + n.bulletActiveClass + "-next " + n.bulletActiveClass + "-next-next " + n.bulletActiveClass + "-prev " + n.bulletActiveClass + "-prev-prev " + n.bulletActiveClass + "-main"), 1 < a.length) p.each(function (t, i) {
                                var o = e(i),
                                    a = o.index();
                                a === s && o.addClass(n.bulletActiveClass), n.dynamicBullets && (l <= a && a <= d && o.addClass(n.bulletActiveClass + "-main"), a === l && o.prev().addClass(n.bulletActiveClass + "-prev").prev().addClass(n.bulletActiveClass + "-prev-prev"), a === d && o.next().addClass(n.bulletActiveClass + "-next").next().addClass(n.bulletActiveClass + "-next-next"))
                            });
                            else if (p.eq(s).addClass(n.bulletActiveClass), n.dynamicBullets) {
                                for (var u = p.eq(l), h = p.eq(d), f = l; f <= d; f += 1) p.eq(f).addClass(n.bulletActiveClass + "-main");
                                u.prev().addClass(n.bulletActiveClass + "-prev").prev().addClass(n.bulletActiveClass + "-prev-prev"), h.next().addClass(n.bulletActiveClass + "-next").next().addClass(n.bulletActiveClass + "-next-next")
                            }
                            if (n.dynamicBullets) {
                                var v = Math.min(p.length, n.dynamicMainBullets + 4),
                                    m = (t.pagination.bulletSize * v - t.pagination.bulletSize) / 2 - c * t.pagination.bulletSize,
                                    g = i ? "right" : "left";
                                p.css(t.isHorizontal() ? g : "top", m + "px")
                            }
                        }
                        if ("fraction" === n.type && (a.find("." + n.currentClass).text(n.formatFractionCurrent(s + 1)), a.find("." + n.totalClass).text(n.formatFractionTotal(r))), "progressbar" === n.type) {
                            var w;
                            w = n.progressbarOpposite ? t.isHorizontal() ? "vertical" : "horizontal" : t.isHorizontal() ? "horizontal" : "vertical";
                            var y = (s + 1) / r,
                                b = 1,
                                T = 1;
                            "horizontal" === w ? b = y : T = y, a.find("." + n.progressbarFillClass).transform("translate3d(0,0,0) scaleX(" + b + ") scaleY(" + T + ")").transition(t.params.speed)
                        }
                        "custom" === n.type && n.renderCustom ? (a.html(n.renderCustom(t, s + 1, r)), t.emit("paginationRender", t, a[0])) : t.emit("paginationUpdate", t, a[0]), a[t.params.watchOverflow && t.isLocked ? "addClass" : "removeClass"](n.lockClass)
                    }
                },
                render: function () {
                    var e = this,
                        t = e.params.pagination;
                    if (t.el && e.pagination.el && e.pagination.$el && 0 !== e.pagination.$el.length) {
                        var i = e.virtual && e.params.virtual.enabled ? e.virtual.slides.length : e.slides.length,
                            n = e.pagination.$el,
                            s = "";
                        if ("bullets" === t.type) {
                            for (var o = e.params.loop ? Math.ceil((i - 2 * e.loopedSlides) / e.params.slidesPerGroup) : e.snapGrid.length, a = 0; a < o; a += 1) t.renderBullet ? s += t.renderBullet.call(e, a, t.bulletClass) : s += "<" + t.bulletElement + ' class="' + t.bulletClass + '"></' + t.bulletElement + ">";
                            n.html(s), e.pagination.bullets = n.find("." + t.bulletClass)
                        }
                        "fraction" === t.type && (s = t.renderFraction ? t.renderFraction.call(e, t.currentClass, t.totalClass) : '<span class="' + t.currentClass + '"></span> / <span class="' + t.totalClass + '"></span>', n.html(s)), "progressbar" === t.type && (s = t.renderProgressbar ? t.renderProgressbar.call(e, t.progressbarFillClass) : '<span class="' + t.progressbarFillClass + '"></span>', n.html(s)), "custom" !== t.type && e.emit("paginationRender", e.pagination.$el[0])
                    }
                },
                init: function () {
                    var t = this,
                        i = t.params.pagination;
                    if (i.el) {
                        var n = e(i.el);
                        0 !== n.length && (t.params.uniqueNavElements && "string" == typeof i.el && 1 < n.length && 1 === t.$el.find(i.el).length && (n = t.$el.find(i.el)), "bullets" === i.type && i.clickable && n.addClass(i.clickableClass), n.addClass(i.modifierClass + i.type), "bullets" === i.type && i.dynamicBullets && (n.addClass("" + i.modifierClass + i.type + "-dynamic"), t.pagination.dynamicBulletIndex = 0, i.dynamicMainBullets < 1 && (i.dynamicMainBullets = 1)), "progressbar" === i.type && i.progressbarOpposite && n.addClass(i.progressbarOppositeClass), i.clickable && n.on("click", "." + i.bulletClass, function (i) {
                            i.preventDefault();
                            var n = e(this).index() * t.params.slidesPerGroup;
                            t.params.loop && (n += t.loopedSlides), t.slideTo(n)
                        }), u.extend(t.pagination, {
                            $el: n,
                            el: n[0]
                        }))
                    }
                },
                destroy: function () {
                    var e = this,
                        t = e.params.pagination;
                    if (t.el && e.pagination.el && e.pagination.$el && 0 !== e.pagination.$el.length) {
                        var i = e.pagination.$el;
                        i.removeClass(t.hiddenClass), i.removeClass(t.modifierClass + t.type), e.pagination.bullets && e.pagination.bullets.removeClass(t.bulletActiveClass), t.clickable && i.off("click", "." + t.bulletClass)
                    }
                }
            },
            R = {
                setTranslate: function () {
                    var e = this;
                    if (e.params.scrollbar.el && e.scrollbar.el) {
                        var t = e.scrollbar,
                            i = e.rtlTranslate,
                            n = e.progress,
                            s = t.dragSize,
                            o = t.trackSize,
                            a = t.$dragEl,
                            r = t.$el,
                            l = e.params.scrollbar,
                            d = s,
                            c = (o - s) * n;
                        i ? 0 < (c = -c) ? (d = s - c, c = 0) : o < -c + s && (d = o + c) : c < 0 ? (d = s + c, c = 0) : o < c + s && (d = o - c), e.isHorizontal() ? (h.transforms3d ? a.transform("translate3d(" + c + "px, 0, 0)") : a.transform("translateX(" + c + "px)"), a[0].style.width = d + "px") : (h.transforms3d ? a.transform("translate3d(0px, " + c + "px, 0)") : a.transform("translateY(" + c + "px)"), a[0].style.height = d + "px"), l.hide && (clearTimeout(e.scrollbar.timeout), r[0].style.opacity = 1, e.scrollbar.timeout = setTimeout(function () {
                            r[0].style.opacity = 0, r.transition(400)
                        }, 1e3))
                    }
                },
                setTransition: function (e) {
                    this.params.scrollbar.el && this.scrollbar.el && this.scrollbar.$dragEl.transition(e)
                },
                updateSize: function () {
                    var e = this;
                    if (e.params.scrollbar.el && e.scrollbar.el) {
                        var t = e.scrollbar,
                            i = t.$dragEl,
                            n = t.$el;
                        i[0].style.width = "", i[0].style.height = "";
                        var s, o = e.isHorizontal() ? n[0].offsetWidth : n[0].offsetHeight,
                            a = e.size / e.virtualSize,
                            r = a * (o / e.size);
                        s = "auto" === e.params.scrollbar.dragSize ? o * a : parseInt(e.params.scrollbar.dragSize, 10), e.isHorizontal() ? i[0].style.width = s + "px" : i[0].style.height = s + "px", n[0].style.display = 1 <= a ? "none" : "", e.params.scrollbar.hide && (n[0].style.opacity = 0), u.extend(t, {
                            trackSize: o,
                            divider: a,
                            moveDivider: r,
                            dragSize: s
                        }), t.$el[e.params.watchOverflow && e.isLocked ? "addClass" : "removeClass"](e.params.scrollbar.lockClass)
                    }
                },
                getPointerPosition: function (e) {
                    return this.isHorizontal() ? "touchstart" === e.type || "touchmove" === e.type ? e.targetTouches[0].pageX : e.pageX || e.clientX : "touchstart" === e.type || "touchmove" === e.type ? e.targetTouches[0].pageY : e.pageY || e.clientY
                },
                setDragPosition: function (e) {
                    var t, i = this,
                        n = i.scrollbar,
                        s = i.rtlTranslate,
                        o = n.$el,
                        a = n.dragSize,
                        r = n.trackSize,
                        l = n.dragStartPos;
                    t = (n.getPointerPosition(e) - o.offset()[i.isHorizontal() ? "left" : "top"] - (null !== l ? l : a / 2)) / (r - a), t = Math.max(Math.min(t, 1), 0), s && (t = 1 - t);
                    var d = i.minTranslate() + (i.maxTranslate() - i.minTranslate()) * t;
                    i.updateProgress(d), i.setTranslate(d), i.updateActiveIndex(), i.updateSlidesClasses()
                },
                onDragStart: function (e) {
                    var t = this,
                        i = t.params.scrollbar,
                        n = t.scrollbar,
                        s = t.$wrapperEl,
                        o = n.$el,
                        a = n.$dragEl;
                    t.scrollbar.isTouched = !0, t.scrollbar.dragStartPos = e.target === a[0] || e.target === a ? n.getPointerPosition(e) - e.target.getBoundingClientRect()[t.isHorizontal() ? "left" : "top"] : null, e.preventDefault(), e.stopPropagation(), s.transition(100), a.transition(100), n.setDragPosition(e), clearTimeout(t.scrollbar.dragTimeout), o.transition(0), i.hide && o.css("opacity", 1), t.emit("scrollbarDragStart", e)
                },
                onDragMove: function (e) {
                    var t = this.scrollbar,
                        i = this.$wrapperEl,
                        n = t.$el,
                        s = t.$dragEl;
                    this.scrollbar.isTouched && (e.preventDefault ? e.preventDefault() : e.returnValue = !1, t.setDragPosition(e), i.transition(0), n.transition(0), s.transition(0), this.emit("scrollbarDragMove", e))
                },
                onDragEnd: function (e) {
                    var t = this,
                        i = t.params.scrollbar,
                        n = t.scrollbar.$el;
                    t.scrollbar.isTouched && (t.scrollbar.isTouched = !1, i.hide && (clearTimeout(t.scrollbar.dragTimeout), t.scrollbar.dragTimeout = u.nextTick(function () {
                        n.css("opacity", 0), n.transition(400)
                    }, 1e3)), t.emit("scrollbarDragEnd", e), i.snapOnRelease && t.slideToClosest())
                },
                enableDraggable: function () {
                    var e = this;
                    if (e.params.scrollbar.el) {
                        var t = e.scrollbar,
                            i = e.touchEventsTouch,
                            n = e.touchEventsDesktop,
                            o = e.params,
                            a = t.$el[0],
                            r = !(!h.passiveListener || !o.passiveListeners) && {
                                passive: !1,
                                capture: !1
                            },
                            l = !(!h.passiveListener || !o.passiveListeners) && {
                                passive: !0,
                                capture: !1
                            };
                        h.touch ? (a.addEventListener(i.start, e.scrollbar.onDragStart, r), a.addEventListener(i.move, e.scrollbar.onDragMove, r), a.addEventListener(i.end, e.scrollbar.onDragEnd, l)) : (a.addEventListener(n.start, e.scrollbar.onDragStart, r), s.addEventListener(n.move, e.scrollbar.onDragMove, r), s.addEventListener(n.end, e.scrollbar.onDragEnd, l))
                    }
                },
                disableDraggable: function () {
                    var e = this;
                    if (e.params.scrollbar.el) {
                        var t = e.scrollbar,
                            i = e.touchEventsTouch,
                            n = e.touchEventsDesktop,
                            o = e.params,
                            a = t.$el[0],
                            r = !(!h.passiveListener || !o.passiveListeners) && {
                                passive: !1,
                                capture: !1
                            },
                            l = !(!h.passiveListener || !o.passiveListeners) && {
                                passive: !0,
                                capture: !1
                            };
                        h.touch ? (a.removeEventListener(i.start, e.scrollbar.onDragStart, r), a.removeEventListener(i.move, e.scrollbar.onDragMove, r), a.removeEventListener(i.end, e.scrollbar.onDragEnd, l)) : (a.removeEventListener(n.start, e.scrollbar.onDragStart, r), s.removeEventListener(n.move, e.scrollbar.onDragMove, r), s.removeEventListener(n.end, e.scrollbar.onDragEnd, l))
                    }
                },
                init: function () {
                    var t = this;
                    if (t.params.scrollbar.el) {
                        var i = t.scrollbar,
                            n = t.$el,
                            s = t.params.scrollbar,
                            o = e(s.el);
                        t.params.uniqueNavElements && "string" == typeof s.el && 1 < o.length && 1 === n.find(s.el).length && (o = n.find(s.el));
                        var a = o.find("." + t.params.scrollbar.dragClass);
                        0 === a.length && (a = e('<div class="' + t.params.scrollbar.dragClass + '"></div>'), o.append(a)), u.extend(i, {
                            $el: o,
                            el: o[0],
                            $dragEl: a,
                            dragEl: a[0]
                        }), s.draggable && i.enableDraggable()
                    }
                },
                destroy: function () {
                    this.scrollbar.disableDraggable()
                }
            },
            j = {
                setTransform: function (t, i) {
                    var n = this.rtl,
                        s = e(t),
                        o = n ? -1 : 1,
                        a = s.attr("data-swiper-parallax") || "0",
                        r = s.attr("data-swiper-parallax-x"),
                        l = s.attr("data-swiper-parallax-y"),
                        d = s.attr("data-swiper-parallax-scale"),
                        c = s.attr("data-swiper-parallax-opacity");
                    if (r || l ? (r = r || "0", l = l || "0") : this.isHorizontal() ? (r = a, l = "0") : (l = a, r = "0"), r = 0 <= r.indexOf("%") ? parseInt(r, 10) * i * o + "%" : r * i * o + "px", l = 0 <= l.indexOf("%") ? parseInt(l, 10) * i + "%" : l * i + "px", null != c) {
                        var p = c - (c - 1) * (1 - Math.abs(i));
                        s[0].style.opacity = p
                    }
                    if (null == d) s.transform("translate3d(" + r + ", " + l + ", 0px)");
                    else {
                        var u = d - (d - 1) * (1 - Math.abs(i));
                        s.transform("translate3d(" + r + ", " + l + ", 0px) scale(" + u + ")")
                    }
                },
                setTranslate: function () {
                    var t = this,
                        i = t.$el,
                        n = t.slides,
                        s = t.progress,
                        o = t.snapGrid;
                    i.children("[data-swiper-parallax], [data-swiper-parallax-x], [data-swiper-parallax-y], [data-swiper-parallax-opacity], [data-swiper-parallax-scale]").each(function (e, i) {
                        t.parallax.setTransform(i, s)
                    }), n.each(function (i, n) {
                        var a = n.progress;
                        1 < t.params.slidesPerGroup && "auto" !== t.params.slidesPerView && (a += Math.ceil(i / 2) - s * (o.length - 1)), a = Math.min(Math.max(a, -1), 1), e(n).find("[data-swiper-parallax], [data-swiper-parallax-x], [data-swiper-parallax-y], [data-swiper-parallax-opacity], [data-swiper-parallax-scale]").each(function (e, i) {
                            t.parallax.setTransform(i, a)
                        })
                    })
                },
                setTransition: function (t) {
                    void 0 === t && (t = this.params.speed), this.$el.find("[data-swiper-parallax], [data-swiper-parallax-x], [data-swiper-parallax-y], [data-swiper-parallax-opacity], [data-swiper-parallax-scale]").each(function (i, n) {
                        var s = e(n),
                            o = parseInt(s.attr("data-swiper-parallax-duration"), 10) || t;
                        0 === t && (o = 0), s.transition(o)
                    })
                }
            },
            F = {
                getDistanceBetweenTouches: function (e) {
                    if (e.targetTouches.length < 2) return 1;
                    var t = e.targetTouches[0].pageX,
                        i = e.targetTouches[0].pageY,
                        n = e.targetTouches[1].pageX,
                        s = e.targetTouches[1].pageY;
                    return Math.sqrt(Math.pow(n - t, 2) + Math.pow(s - i, 2))
                },
                onGestureStart: function (t) {
                    var i = this,
                        n = i.params.zoom,
                        s = i.zoom,
                        o = s.gesture;
                    if (s.fakeGestureTouched = !1, s.fakeGestureMoved = !1, !h.gestures) {
                        if ("touchstart" !== t.type || "touchstart" === t.type && t.targetTouches.length < 2) return;
                        s.fakeGestureTouched = !0, o.scaleStart = F.getDistanceBetweenTouches(t)
                    }
                    o.$slideEl && o.$slideEl.length || (o.$slideEl = e(t.target).closest(".swiper-slide"), 0 === o.$slideEl.length && (o.$slideEl = i.slides.eq(i.activeIndex)), o.$imageEl = o.$slideEl.find("img, svg, canvas"), o.$imageWrapEl = o.$imageEl.parent("." + n.containerClass), o.maxRatio = o.$imageWrapEl.attr("data-swiper-zoom") || n.maxRatio, 0 !== o.$imageWrapEl.length) ? (o.$imageEl.transition(0), i.zoom.isScaling = !0) : o.$imageEl = void 0
                },
                onGestureChange: function (e) {
                    var t = this.params.zoom,
                        i = this.zoom,
                        n = i.gesture;
                    if (!h.gestures) {
                        if ("touchmove" !== e.type || "touchmove" === e.type && e.targetTouches.length < 2) return;
                        i.fakeGestureMoved = !0, n.scaleMove = F.getDistanceBetweenTouches(e)
                    }
                    n.$imageEl && 0 !== n.$imageEl.length && (i.scale = h.gestures ? e.scale * i.currentScale : n.scaleMove / n.scaleStart * i.currentScale, i.scale > n.maxRatio && (i.scale = n.maxRatio - 1 + Math.pow(i.scale - n.maxRatio + 1, .5)), i.scale < t.minRatio && (i.scale = t.minRatio + 1 - Math.pow(t.minRatio - i.scale + 1, .5)), n.$imageEl.transform("translate3d(0,0,0) scale(" + i.scale + ")"))
                },
                onGestureEnd: function (e) {
                    var t = this.params.zoom,
                        i = this.zoom,
                        n = i.gesture;
                    if (!h.gestures) {
                        if (!i.fakeGestureTouched || !i.fakeGestureMoved) return;
                        if ("touchend" !== e.type || "touchend" === e.type && e.changedTouches.length < 2 && !S.android) return;
                        i.fakeGestureTouched = !1, i.fakeGestureMoved = !1
                    }
                    n.$imageEl && 0 !== n.$imageEl.length && (i.scale = Math.max(Math.min(i.scale, n.maxRatio), t.minRatio), n.$imageEl.transition(this.params.speed).transform("translate3d(0,0,0) scale(" + i.scale + ")"), i.currentScale = i.scale, i.isScaling = !1, 1 === i.scale && (n.$slideEl = void 0))
                },
                onTouchStart: function (e) {
                    var t = this.zoom,
                        i = t.gesture,
                        n = t.image;
                    i.$imageEl && 0 !== i.$imageEl.length && (n.isTouched || (S.android && e.preventDefault(), n.isTouched = !0, n.touchesStart.x = "touchstart" === e.type ? e.targetTouches[0].pageX : e.pageX, n.touchesStart.y = "touchstart" === e.type ? e.targetTouches[0].pageY : e.pageY))
                },
                onTouchMove: function (e) {
                    var t = this,
                        i = t.zoom,
                        n = i.gesture,
                        s = i.image,
                        o = i.velocity;
                    if (n.$imageEl && 0 !== n.$imageEl.length && (t.allowClick = !1, s.isTouched && n.$slideEl)) {
                        s.isMoved || (s.width = n.$imageEl[0].offsetWidth, s.height = n.$imageEl[0].offsetHeight, s.startX = u.getTranslate(n.$imageWrapEl[0], "x") || 0, s.startY = u.getTranslate(n.$imageWrapEl[0], "y") || 0, n.slideWidth = n.$slideEl[0].offsetWidth, n.slideHeight = n.$slideEl[0].offsetHeight, n.$imageWrapEl.transition(0), t.rtl && (s.startX = -s.startX, s.startY = -s.startY));
                        var a = s.width * i.scale,
                            r = s.height * i.scale;
                        if (!(a < n.slideWidth && r < n.slideHeight)) {
                            if (s.minX = Math.min(n.slideWidth / 2 - a / 2, 0), s.maxX = -s.minX, s.minY = Math.min(n.slideHeight / 2 - r / 2, 0), s.maxY = -s.minY, s.touchesCurrent.x = "touchmove" === e.type ? e.targetTouches[0].pageX : e.pageX, s.touchesCurrent.y = "touchmove" === e.type ? e.targetTouches[0].pageY : e.pageY, !s.isMoved && !i.isScaling) {
                                if (t.isHorizontal() && (Math.floor(s.minX) === Math.floor(s.startX) && s.touchesCurrent.x < s.touchesStart.x || Math.floor(s.maxX) === Math.floor(s.startX) && s.touchesCurrent.x > s.touchesStart.x)) return void(s.isTouched = !1);
                                if (!t.isHorizontal() && (Math.floor(s.minY) === Math.floor(s.startY) && s.touchesCurrent.y < s.touchesStart.y || Math.floor(s.maxY) === Math.floor(s.startY) && s.touchesCurrent.y > s.touchesStart.y)) return void(s.isTouched = !1)
                            }
                            e.preventDefault(), e.stopPropagation(), s.isMoved = !0, s.currentX = s.touchesCurrent.x - s.touchesStart.x + s.startX, s.currentY = s.touchesCurrent.y - s.touchesStart.y + s.startY, s.currentX < s.minX && (s.currentX = s.minX + 1 - Math.pow(s.minX - s.currentX + 1, .8)), s.currentX > s.maxX && (s.currentX = s.maxX - 1 + Math.pow(s.currentX - s.maxX + 1, .8)), s.currentY < s.minY && (s.currentY = s.minY + 1 - Math.pow(s.minY - s.currentY + 1, .8)), s.currentY > s.maxY && (s.currentY = s.maxY - 1 + Math.pow(s.currentY - s.maxY + 1, .8)), o.prevPositionX || (o.prevPositionX = s.touchesCurrent.x), o.prevPositionY || (o.prevPositionY = s.touchesCurrent.y), o.prevTime || (o.prevTime = Date.now()), o.x = (s.touchesCurrent.x - o.prevPositionX) / (Date.now() - o.prevTime) / 2, o.y = (s.touchesCurrent.y - o.prevPositionY) / (Date.now() - o.prevTime) / 2, Math.abs(s.touchesCurrent.x - o.prevPositionX) < 2 && (o.x = 0), Math.abs(s.touchesCurrent.y - o.prevPositionY) < 2 && (o.y = 0), o.prevPositionX = s.touchesCurrent.x, o.prevPositionY = s.touchesCurrent.y, o.prevTime = Date.now(), n.$imageWrapEl.transform("translate3d(" + s.currentX + "px, " + s.currentY + "px,0)")
                        }
                    }
                },
                onTouchEnd: function () {
                    var e = this.zoom,
                        t = e.gesture,
                        i = e.image,
                        n = e.velocity;
                    if (t.$imageEl && 0 !== t.$imageEl.length) {
                        if (!i.isTouched || !i.isMoved) return i.isTouched = !1, void(i.isMoved = !1);
                        i.isTouched = !1, i.isMoved = !1;
                        var s = 300,
                            o = 300,
                            a = n.x * s,
                            r = i.currentX + a,
                            l = n.y * o,
                            d = i.currentY + l;
                        0 !== n.x && (s = Math.abs((r - i.currentX) / n.x)), 0 !== n.y && (o = Math.abs((d - i.currentY) / n.y));
                        var c = Math.max(s, o);
                        i.currentX = r, i.currentY = d;
                        var p = i.width * e.scale,
                            u = i.height * e.scale;
                        i.minX = Math.min(t.slideWidth / 2 - p / 2, 0), i.maxX = -i.minX, i.minY = Math.min(t.slideHeight / 2 - u / 2, 0), i.maxY = -i.minY, i.currentX = Math.max(Math.min(i.currentX, i.maxX), i.minX), i.currentY = Math.max(Math.min(i.currentY, i.maxY), i.minY), t.$imageWrapEl.transition(c).transform("translate3d(" + i.currentX + "px, " + i.currentY + "px,0)")
                    }
                },
                onTransitionEnd: function () {
                    var e = this.zoom,
                        t = e.gesture;
                    t.$slideEl && this.previousIndex !== this.activeIndex && (t.$imageEl.transform("translate3d(0,0,0) scale(1)"), t.$imageWrapEl.transform("translate3d(0,0,0)"), e.scale = 1, e.currentScale = 1, t.$slideEl = void 0, t.$imageEl = void 0, t.$imageWrapEl = void 0)
                },
                toggle: function (e) {
                    var t = this.zoom;
                    t.scale && 1 !== t.scale ? t.out() : t.in(e)
                },
                in: function (t) {
                    var i, n, s, o, a, r, l, d, c, p, u, h, f, v, m, g, w = this,
                        y = w.zoom,
                        b = w.params.zoom,
                        T = y.gesture,
                        x = y.image;
                    T.$slideEl || (T.$slideEl = w.clickedSlide ? e(w.clickedSlide) : w.slides.eq(w.activeIndex), T.$imageEl = T.$slideEl.find("img, svg, canvas"), T.$imageWrapEl = T.$imageEl.parent("." + b.containerClass)), T.$imageEl && 0 !== T.$imageEl.length && (T.$slideEl.addClass("" + b.zoomedSlideClass), n = void 0 === x.touchesStart.x && t ? (i = "touchend" === t.type ? t.changedTouches[0].pageX : t.pageX, "touchend" === t.type ? t.changedTouches[0].pageY : t.pageY) : (i = x.touchesStart.x, x.touchesStart.y), y.scale = T.$imageWrapEl.attr("data-swiper-zoom") || b.maxRatio, y.currentScale = T.$imageWrapEl.attr("data-swiper-zoom") || b.maxRatio, t ? (m = T.$slideEl[0].offsetWidth, g = T.$slideEl[0].offsetHeight, s = T.$slideEl.offset().left + m / 2 - i, o = T.$slideEl.offset().top + g / 2 - n, l = T.$imageEl[0].offsetWidth, d = T.$imageEl[0].offsetHeight, c = l * y.scale, p = d * y.scale, f = -(u = Math.min(m / 2 - c / 2, 0)), v = -(h = Math.min(g / 2 - p / 2, 0)), (a = s * y.scale) < u && (a = u), f < a && (a = f), (r = o * y.scale) < h && (r = h), v < r && (r = v)) : r = a = 0, T.$imageWrapEl.transition(300).transform("translate3d(" + a + "px, " + r + "px,0)"), T.$imageEl.transition(300).transform("translate3d(0,0,0) scale(" + y.scale + ")"))
                },
                out: function () {
                    var t = this,
                        i = t.zoom,
                        n = t.params.zoom,
                        s = i.gesture;
                    s.$slideEl || (s.$slideEl = t.clickedSlide ? e(t.clickedSlide) : t.slides.eq(t.activeIndex), s.$imageEl = s.$slideEl.find("img, svg, canvas"), s.$imageWrapEl = s.$imageEl.parent("." + n.containerClass)), s.$imageEl && 0 !== s.$imageEl.length && (i.scale = 1, i.currentScale = 1, s.$imageWrapEl.transition(300).transform("translate3d(0,0,0)"), s.$imageEl.transition(300).transform("translate3d(0,0,0) scale(1)"), s.$slideEl.removeClass("" + n.zoomedSlideClass), s.$slideEl = void 0)
                },
                enable: function () {
                    var e = this,
                        t = e.zoom;
                    if (!t.enabled) {
                        t.enabled = !0;
                        var i = !("touchstart" !== e.touchEvents.start || !h.passiveListener || !e.params.passiveListeners) && {
                            passive: !0,
                            capture: !1
                        };
                        h.gestures ? (e.$wrapperEl.on("gesturestart", ".swiper-slide", t.onGestureStart, i), e.$wrapperEl.on("gesturechange", ".swiper-slide", t.onGestureChange, i), e.$wrapperEl.on("gestureend", ".swiper-slide", t.onGestureEnd, i)) : "touchstart" === e.touchEvents.start && (e.$wrapperEl.on(e.touchEvents.start, ".swiper-slide", t.onGestureStart, i), e.$wrapperEl.on(e.touchEvents.move, ".swiper-slide", t.onGestureChange, i), e.$wrapperEl.on(e.touchEvents.end, ".swiper-slide", t.onGestureEnd, i)), e.$wrapperEl.on(e.touchEvents.move, "." + e.params.zoom.containerClass, t.onTouchMove)
                    }
                },
                disable: function () {
                    var e = this,
                        t = e.zoom;
                    if (t.enabled) {
                        e.zoom.enabled = !1;
                        var i = !("touchstart" !== e.touchEvents.start || !h.passiveListener || !e.params.passiveListeners) && {
                            passive: !0,
                            capture: !1
                        };
                        h.gestures ? (e.$wrapperEl.off("gesturestart", ".swiper-slide", t.onGestureStart, i), e.$wrapperEl.off("gesturechange", ".swiper-slide", t.onGestureChange, i), e.$wrapperEl.off("gestureend", ".swiper-slide", t.onGestureEnd, i)) : "touchstart" === e.touchEvents.start && (e.$wrapperEl.off(e.touchEvents.start, ".swiper-slide", t.onGestureStart, i), e.$wrapperEl.off(e.touchEvents.move, ".swiper-slide", t.onGestureChange, i), e.$wrapperEl.off(e.touchEvents.end, ".swiper-slide", t.onGestureEnd, i)), e.$wrapperEl.off(e.touchEvents.move, "." + e.params.zoom.containerClass, t.onTouchMove)
                    }
                }
            },
            W = {
                loadInSlide: function (t, i) {
                    void 0 === i && (i = !0);
                    var n = this,
                        s = n.params.lazy;
                    if (void 0 !== t && 0 !== n.slides.length) {
                        var o = n.virtual && n.params.virtual.enabled ? n.$wrapperEl.children("." + n.params.slideClass + '[data-swiper-slide-index="' + t + '"]') : n.slides.eq(t),
                            a = o.find("." + s.elementClass + ":not(." + s.loadedClass + "):not(." + s.loadingClass + ")");
                        !o.hasClass(s.elementClass) || o.hasClass(s.loadedClass) || o.hasClass(s.loadingClass) || (a = a.add(o[0])), 0 !== a.length && a.each(function (t, a) {
                            var r = e(a);
                            r.addClass(s.loadingClass);
                            var l = r.attr("data-background"),
                                d = r.attr("data-src"),
                                c = r.attr("data-srcset"),
                                p = r.attr("data-sizes");
                            n.loadImage(r[0], d || l, c, p, !1, function () {
                                if (null != n && n && (!n || n.params) && !n.destroyed) {
                                    if (l ? (r.css("background-image", 'url("' + l + '")'), r.removeAttr("data-background")) : (c && (r.attr("srcset", c), r.removeAttr("data-srcset")), p && (r.attr("sizes", p), r.removeAttr("data-sizes")), d && (r.attr("src", d), r.removeAttr("data-src"))), r.addClass(s.loadedClass).removeClass(s.loadingClass), o.find("." + s.preloaderClass).remove(), n.params.loop && i) {
                                        var e = o.attr("data-swiper-slide-index");
                                        if (o.hasClass(n.params.slideDuplicateClass)) {
                                            var t = n.$wrapperEl.children('[data-swiper-slide-index="' + e + '"]:not(.' + n.params.slideDuplicateClass + ")");
                                            n.lazy.loadInSlide(t.index(), !1)
                                        } else {
                                            var a = n.$wrapperEl.children("." + n.params.slideDuplicateClass + '[data-swiper-slide-index="' + e + '"]');
                                            n.lazy.loadInSlide(a.index(), !1)
                                        }
                                    }
                                    n.emit("lazyImageReady", o[0], r[0])
                                }
                            }), n.emit("lazyImageLoad", o[0], r[0])
                        })
                    }
                },
                load: function () {
                    function t(e) {
                        if (l) {
                            if (s.children("." + o.slideClass + '[data-swiper-slide-index="' + e + '"]').length) return !0
                        } else if (a[e]) return !0;
                        return !1
                    }

                    function i(t) {
                        return l ? e(t).attr("data-swiper-slide-index") : e(t).index()
                    }
                    var n = this,
                        s = n.$wrapperEl,
                        o = n.params,
                        a = n.slides,
                        r = n.activeIndex,
                        l = n.virtual && o.virtual.enabled,
                        d = o.lazy,
                        c = o.slidesPerView;
                    if ("auto" === c && (c = 0), n.lazy.initialImageLoaded || (n.lazy.initialImageLoaded = !0), n.params.watchSlidesVisibility) s.children("." + o.slideVisibleClass).each(function (t, i) {
                        var s = l ? e(i).attr("data-swiper-slide-index") : e(i).index();
                        n.lazy.loadInSlide(s)
                    });
                    else if (1 < c)
                        for (var p = r; p < r + c; p += 1) t(p) && n.lazy.loadInSlide(p);
                    else n.lazy.loadInSlide(r);
                    if (d.loadPrevNext)
                        if (1 < c || d.loadPrevNextAmount && 1 < d.loadPrevNextAmount) {
                            for (var u = d.loadPrevNextAmount, h = c, f = Math.min(r + h + Math.max(u, h), a.length), v = Math.max(r - Math.max(h, u), 0), m = r + c; m < f; m += 1) t(m) && n.lazy.loadInSlide(m);
                            for (var g = v; g < r; g += 1) t(g) && n.lazy.loadInSlide(g)
                        } else {
                            var w = s.children("." + o.slideNextClass);
                            0 < w.length && n.lazy.loadInSlide(i(w));
                            var y = s.children("." + o.slidePrevClass);
                            0 < y.length && n.lazy.loadInSlide(i(y))
                        }
                }
            },
            q = {
                LinearSpline: function (e, t) {
                    var i, n, s, o, a, r = function (e, t) {
                        for (n = -1, i = e.length; 1 < i - n;) e[s = i + n >> 1] <= t ? n = s : i = s;
                        return i
                    };
                    return this.x = e, this.y = t, this.lastIndex = e.length - 1, this.interpolate = function (e) {
                        return e ? (a = r(this.x, e), o = a - 1,
                            (e - this.x[o]) * (this.y[a] - this.y[o]) / (this.x[a] - this.x[o]) + this.y[o]) : 0
                    }, this
                },
                getInterpolateFunction: function (e) {
                    var t = this;
                    t.controller.spline || (t.controller.spline = t.params.loop ? new q.LinearSpline(t.slidesGrid, e.slidesGrid) : new q.LinearSpline(t.snapGrid, e.snapGrid))
                },
                setTranslate: function (e, t) {
                    function i(e) {
                        var t = o.rtlTranslate ? -o.translate : o.translate;
                        "slide" === o.params.controller.by && (o.controller.getInterpolateFunction(e), s = -o.controller.spline.interpolate(-t)), s && "container" !== o.params.controller.by || (n = (e.maxTranslate() - e.minTranslate()) / (o.maxTranslate() - o.minTranslate()), s = (t - o.minTranslate()) * n + e.minTranslate()), o.params.controller.inverse && (s = e.maxTranslate() - s), e.updateProgress(s), e.setTranslate(s, o), e.updateActiveIndex(), e.updateSlidesClasses()
                    }
                    var n, s, o = this,
                        a = o.controller.control;
                    if (Array.isArray(a))
                        for (var r = 0; r < a.length; r += 1) a[r] !== t && a[r] instanceof $ && i(a[r]);
                    else a instanceof $ && t !== a && i(a)
                },
                setTransition: function (e, t) {
                    function i(t) {
                        t.setTransition(e, s), 0 !== e && (t.transitionStart(), t.params.autoHeight && u.nextTick(function () {
                            t.updateAutoHeight()
                        }), t.$wrapperEl.transitionEnd(function () {
                            o && (t.params.loop && "slide" === s.params.controller.by && t.loopFix(), t.transitionEnd())
                        }))
                    }
                    var n, s = this,
                        o = s.controller.control;
                    if (Array.isArray(o))
                        for (n = 0; n < o.length; n += 1) o[n] !== t && o[n] instanceof $ && i(o[n]);
                    else o instanceof $ && t !== o && i(o)
                }
            },
            G = {
                makeElFocusable: function (e) {
                    return e.attr("tabIndex", "0"), e
                },
                addElRole: function (e, t) {
                    return e.attr("role", t), e
                },
                addElLabel: function (e, t) {
                    return e.attr("aria-label", t), e
                },
                disableEl: function (e) {
                    return e.attr("aria-disabled", !0), e
                },
                enableEl: function (e) {
                    return e.attr("aria-disabled", !1), e
                },
                onEnterKey: function (t) {
                    var i = this,
                        n = i.params.a11y;
                    if (13 === t.keyCode) {
                        var s = e(t.target);
                        i.navigation && i.navigation.$nextEl && s.is(i.navigation.$nextEl) && (i.isEnd && !i.params.loop || i.slideNext(), i.isEnd ? i.a11y.notify(n.lastSlideMessage) : i.a11y.notify(n.nextSlideMessage)), i.navigation && i.navigation.$prevEl && s.is(i.navigation.$prevEl) && (i.isBeginning && !i.params.loop || i.slidePrev(), i.isBeginning ? i.a11y.notify(n.firstSlideMessage) : i.a11y.notify(n.prevSlideMessage)), i.pagination && s.is("." + i.params.pagination.bulletClass) && s[0].click()
                    }
                },
                notify: function (e) {
                    var t = this.a11y.liveRegion;
                    0 !== t.length && (t.html(""), t.html(e))
                },
                updateNavigation: function () {
                    var e = this;
                    if (!e.params.loop) {
                        var t = e.navigation,
                            i = t.$nextEl,
                            n = t.$prevEl;
                        n && 0 < n.length && (e.isBeginning ? e.a11y.disableEl(n) : e.a11y.enableEl(n)), i && 0 < i.length && (e.isEnd ? e.a11y.disableEl(i) : e.a11y.enableEl(i))
                    }
                },
                updatePagination: function () {
                    var t = this,
                        i = t.params.a11y;
                    t.pagination && t.params.pagination.clickable && t.pagination.bullets && t.pagination.bullets.length && t.pagination.bullets.each(function (n, s) {
                        var o = e(s);
                        t.a11y.makeElFocusable(o), t.a11y.addElRole(o, "button"), t.a11y.addElLabel(o, i.paginationBulletMessage.replace(/{{index}}/, o.index() + 1))
                    })
                },
                init: function () {
                    var e = this;
                    e.$el.append(e.a11y.liveRegion);
                    var t, i, n = e.params.a11y;
                    e.navigation && e.navigation.$nextEl && (t = e.navigation.$nextEl), e.navigation && e.navigation.$prevEl && (i = e.navigation.$prevEl), t && (e.a11y.makeElFocusable(t), e.a11y.addElRole(t, "button"), e.a11y.addElLabel(t, n.nextSlideMessage), t.on("keydown", e.a11y.onEnterKey)), i && (e.a11y.makeElFocusable(i), e.a11y.addElRole(i, "button"), e.a11y.addElLabel(i, n.prevSlideMessage), i.on("keydown", e.a11y.onEnterKey)), e.pagination && e.params.pagination.clickable && e.pagination.bullets && e.pagination.bullets.length && e.pagination.$el.on("keydown", "." + e.params.pagination.bulletClass, e.a11y.onEnterKey)
                },
                destroy: function () {
                    var e, t, i = this;
                    i.a11y.liveRegion && 0 < i.a11y.liveRegion.length && i.a11y.liveRegion.remove(), i.navigation && i.navigation.$nextEl && (e = i.navigation.$nextEl), i.navigation && i.navigation.$prevEl && (t = i.navigation.$prevEl), e && e.off("keydown", i.a11y.onEnterKey), t && t.off("keydown", i.a11y.onEnterKey), i.pagination && i.params.pagination.clickable && i.pagination.bullets && i.pagination.bullets.length && i.pagination.$el.off("keydown", "." + i.params.pagination.bulletClass, i.a11y.onEnterKey)
                }
            },
            V = {
                init: function () {
                    var e = this;
                    if (e.params.history) {
                        if (!o.history || !o.history.pushState) return e.params.history.enabled = !1, void(e.params.hashNavigation.enabled = !0);
                        var t = e.history;
                        t.initialized = !0, t.paths = V.getPathValues(), (t.paths.key || t.paths.value) && (t.scrollToSlide(0, t.paths.value, e.params.runCallbacksOnInit), e.params.history.replaceState || o.addEventListener("popstate", e.history.setHistoryPopState))
                    }
                },
                destroy: function () {
                    this.params.history.replaceState || o.removeEventListener("popstate", this.history.setHistoryPopState)
                },
                setHistoryPopState: function () {
                    this.history.paths = V.getPathValues(), this.history.scrollToSlide(this.params.speed, this.history.paths.value, !1)
                },
                getPathValues: function () {
                    var e = o.location.pathname.slice(1).split("/").filter(function (e) {
                            return "" !== e
                        }),
                        t = e.length;
                    return {
                        key: e[t - 2],
                        value: e[t - 1]
                    }
                },
                setHistory: function (e, t) {
                    if (this.history.initialized && this.params.history.enabled) {
                        var i = this.slides.eq(t),
                            n = V.slugify(i.attr("data-history"));
                        o.location.pathname.includes(e) || (n = e + "/" + n);
                        var s = o.history.state;
                        s && s.value === n || (this.params.history.replaceState ? o.history.replaceState({
                            value: n
                        }, null, n) : o.history.pushState({
                            value: n
                        }, null, n))
                    }
                },
                slugify: function (e) {
                    return e.toString().replace(/\s+/g, "-").replace(/[^\w-]+/g, "").replace(/--+/g, "-").replace(/^-+/, "").replace(/-+$/, "")
                },
                scrollToSlide: function (e, t, i) {
                    var n = this;
                    if (t)
                        for (var s = 0, o = n.slides.length; s < o; s += 1) {
                            var a = n.slides.eq(s);
                            if (V.slugify(a.attr("data-history")) === t && !a.hasClass(n.params.slideDuplicateClass)) {
                                var r = a.index();
                                n.slideTo(r, e, i)
                            }
                        } else n.slideTo(0, e, i)
                }
            },
            _ = {
                onHashCange: function () {
                    var e = this,
                        t = s.location.hash.replace("#", "");
                    if (t !== e.slides.eq(e.activeIndex).attr("data-hash")) {
                        var i = e.$wrapperEl.children("." + e.params.slideClass + '[data-hash="' + t + '"]').index();
                        if (void 0 === i) return;
                        e.slideTo(i)
                    }
                },
                setHash: function () {
                    var e = this;
                    if (e.hashNavigation.initialized && e.params.hashNavigation.enabled)
                        if (e.params.hashNavigation.replaceState && o.history && o.history.replaceState) o.history.replaceState(null, null, "#" + e.slides.eq(e.activeIndex).attr("data-hash") || "");
                        else {
                            var t = e.slides.eq(e.activeIndex),
                                i = t.attr("data-hash") || t.attr("data-history");
                            s.location.hash = i || ""
                        }
                },
                init: function () {
                    var t = this;
                    if (!(!t.params.hashNavigation.enabled || t.params.history && t.params.history.enabled)) {
                        t.hashNavigation.initialized = !0;
                        var i = s.location.hash.replace("#", "");
                        if (i)
                            for (var n = 0, a = t.slides.length; n < a; n += 1) {
                                var r = t.slides.eq(n);
                                if ((r.attr("data-hash") || r.attr("data-history")) === i && !r.hasClass(t.params.slideDuplicateClass)) {
                                    var l = r.index();
                                    t.slideTo(l, 0, t.params.runCallbacksOnInit, !0)
                                }
                            }
                        t.params.hashNavigation.watchState && e(o).on("hashchange", t.hashNavigation.onHashCange)
                    }
                },
                destroy: function () {
                    this.params.hashNavigation.watchState && e(o).off("hashchange", this.hashNavigation.onHashCange)
                }
            },
            U = {
                run: function () {
                    var e = this,
                        t = e.slides.eq(e.activeIndex),
                        i = e.params.autoplay.delay;
                    t.attr("data-swiper-autoplay") && (i = t.attr("data-swiper-autoplay") || e.params.autoplay.delay), clearTimeout(e.autoplay.timeout), e.autoplay.timeout = u.nextTick(function () {
                        e.params.autoplay.reverseDirection ? e.params.loop ? (e.loopFix(), e.slidePrev(e.params.speed, !0, !0), e.emit("autoplay")) : e.isBeginning ? e.params.autoplay.stopOnLastSlide ? e.autoplay.stop() : (e.slideTo(e.slides.length - 1, e.params.speed, !0, !0), e.emit("autoplay")) : (e.slidePrev(e.params.speed, !0, !0), e.emit("autoplay")) : e.params.loop ? (e.loopFix(), e.slideNext(e.params.speed, !0, !0), e.emit("autoplay")) : e.isEnd ? e.params.autoplay.stopOnLastSlide ? e.autoplay.stop() : (e.slideTo(0, e.params.speed, !0, !0), e.emit("autoplay")) : (e.slideNext(e.params.speed, !0, !0), e.emit("autoplay"))
                    }, i)
                },
                start: function () {
                    var e = this;
                    return void 0 === e.autoplay.timeout && !e.autoplay.running && (e.autoplay.running = !0, e.emit("autoplayStart"), e.autoplay.run(), !0)
                },
                stop: function () {
                    var e = this;
                    return !!e.autoplay.running && void 0 !== e.autoplay.timeout && (e.autoplay.timeout && (clearTimeout(e.autoplay.timeout), e.autoplay.timeout = void 0), e.autoplay.running = !1, e.emit("autoplayStop"), !0)
                },
                pause: function (e) {
                    var t = this;
                    t.autoplay.running && (t.autoplay.paused || (t.autoplay.timeout && clearTimeout(t.autoplay.timeout), t.autoplay.paused = !0, 0 !== e && t.params.autoplay.waitForTransition ? (t.$wrapperEl[0].addEventListener("transitionend", t.autoplay.onTransitionEnd), t.$wrapperEl[0].addEventListener("webkitTransitionEnd", t.autoplay.onTransitionEnd)) : (t.autoplay.paused = !1, t.autoplay.run())))
                }
            },
            K = {
                setTranslate: function () {
                    for (var e = this, t = e.slides, i = 0; i < t.length; i += 1) {
                        var n = e.slides.eq(i),
                            s = -n[0].swiperSlideOffset;
                        e.params.virtualTranslate || (s -= e.translate);
                        var o = 0;
                        e.isHorizontal() || (o = s, s = 0);
                        var a = e.params.fadeEffect.crossFade ? Math.max(1 - Math.abs(n[0].progress), 0) : 1 + Math.min(Math.max(n[0].progress, -1), 0);
                        n.css({
                            opacity: a
                        }).transform("translate3d(" + s + "px, " + o + "px, 0px)")
                    }
                },
                setTransition: function (e) {
                    var t = this,
                        i = t.slides,
                        n = t.$wrapperEl;
                    if (i.transition(e), t.params.virtualTranslate && 0 !== e) {
                        var s = !1;
                        i.transitionEnd(function () {
                            if (!s && t && !t.destroyed) {
                                s = !0, t.animating = !1;
                                for (var e = ["webkitTransitionEnd", "transitionend"], i = 0; i < e.length; i += 1) n.trigger(e[i])
                            }
                        })
                    }
                }
            },
            Q = {
                setTranslate: function () {
                    var t, i = this,
                        n = i.$el,
                        s = i.$wrapperEl,
                        o = i.slides,
                        a = i.width,
                        r = i.height,
                        l = i.rtlTranslate,
                        d = i.size,
                        c = i.params.cubeEffect,
                        p = i.isHorizontal(),
                        u = i.virtual && i.params.virtual.enabled,
                        h = 0;
                    c.shadow && (p ? (0 === (t = s.find(".swiper-cube-shadow")).length && (t = e('<div class="swiper-cube-shadow"></div>'), s.append(t)), t.css({
                        height: a + "px"
                    })) : 0 === (t = n.find(".swiper-cube-shadow")).length && (t = e('<div class="swiper-cube-shadow"></div>'), n.append(t)));
                    for (var v = 0; v < o.length; v += 1) {
                        var m = o.eq(v),
                            g = v;
                        u && (g = parseInt(m.attr("data-swiper-slide-index"), 10));
                        var w = 90 * g,
                            y = Math.floor(w / 360);
                        l && (w = -w, y = Math.floor(-w / 360));
                        var b = Math.max(Math.min(m[0].progress, 1), -1),
                            T = 0,
                            x = 0,
                            S = 0;
                        g % 4 == 0 ? (T = 4 * -y * d, S = 0) : (g - 1) % 4 == 0 ? (T = 0, S = 4 * -y * d) : (g - 2) % 4 == 0 ? (T = d + 4 * y * d, S = d) : (g - 3) % 4 == 0 && (T = -d, S = 3 * d + 4 * d * y), l && (T = -T), p || (x = T, T = 0);
                        var C = "rotateX(" + (p ? 0 : -w) + "deg) rotateY(" + (p ? w : 0) + "deg) translate3d(" + T + "px, " + x + "px, " + S + "px)";
                        if (b <= 1 && -1 < b && (h = 90 * g + 90 * b, l && (h = 90 * -g - 90 * b)), m.transform(C), c.slideShadows) {
                            var k = p ? m.find(".swiper-slide-shadow-left") : m.find(".swiper-slide-shadow-top"),
                                E = p ? m.find(".swiper-slide-shadow-right") : m.find(".swiper-slide-shadow-bottom");
                            0 === k.length && (k = e('<div class="swiper-slide-shadow-' + (p ? "left" : "top") + '"></div>'), m.append(k)), 0 === E.length && (E = e('<div class="swiper-slide-shadow-' + (p ? "right" : "bottom") + '"></div>'), m.append(E)), k.length && (k[0].style.opacity = Math.max(-b, 0)), E.length && (E[0].style.opacity = Math.max(b, 0))
                        }
                    }
                    if (s.css({
                            "-webkit-transform-origin": "50% 50% -" + d / 2 + "px",
                            "-moz-transform-origin": "50% 50% -" + d / 2 + "px",
                            "-ms-transform-origin": "50% 50% -" + d / 2 + "px",
                            "transform-origin": "50% 50% -" + d / 2 + "px"
                        }), c.shadow)
                        if (p) t.transform("translate3d(0px, " + (a / 2 + c.shadowOffset) + "px, " + -a / 2 + "px) rotateX(90deg) rotateZ(0deg) scale(" + c.shadowScale + ")");
                        else {
                            var $ = Math.abs(h) - 90 * Math.floor(Math.abs(h) / 90),
                                M = 1.5 - (Math.sin(2 * $ * Math.PI / 360) / 2 + Math.cos(2 * $ * Math.PI / 360) / 2),
                                P = c.shadowScale,
                                z = c.shadowScale / M,
                                L = c.shadowOffset;
                            t.transform("scale3d(" + P + ", 1, " + z + ") translate3d(0px, " + (r / 2 + L) + "px, " + -r / 2 / z + "px) rotateX(-90deg)")
                        } var A = f.isSafari || f.isUiWebView ? -d / 2 : 0;
                    s.transform("translate3d(0px,0," + A + "px) rotateX(" + (i.isHorizontal() ? 0 : h) + "deg) rotateY(" + (i.isHorizontal() ? -h : 0) + "deg)")
                },
                setTransition: function (e) {
                    var t = this.$el;
                    this.slides.transition(e).find(".swiper-slide-shadow-top, .swiper-slide-shadow-right, .swiper-slide-shadow-bottom, .swiper-slide-shadow-left").transition(e), this.params.cubeEffect.shadow && !this.isHorizontal() && t.find(".swiper-cube-shadow").transition(e)
                }
            },
            Z = {
                setTranslate: function () {
                    for (var t = this, i = t.slides, n = t.rtlTranslate, s = 0; s < i.length; s += 1) {
                        var o = i.eq(s),
                            a = o[0].progress;
                        t.params.flipEffect.limitRotation && (a = Math.max(Math.min(o[0].progress, 1), -1));
                        var r = -180 * a,
                            l = 0,
                            d = -o[0].swiperSlideOffset,
                            c = 0;
                        if (t.isHorizontal() ? n && (r = -r) : (c = d, l = -r, r = d = 0), o[0].style.zIndex = -Math.abs(Math.round(a)) + i.length, t.params.flipEffect.slideShadows) {
                            var p = t.isHorizontal() ? o.find(".swiper-slide-shadow-left") : o.find(".swiper-slide-shadow-top"),
                                u = t.isHorizontal() ? o.find(".swiper-slide-shadow-right") : o.find(".swiper-slide-shadow-bottom");
                            0 === p.length && (p = e('<div class="swiper-slide-shadow-' + (t.isHorizontal() ? "left" : "top") + '"></div>'), o.append(p)), 0 === u.length && (u = e('<div class="swiper-slide-shadow-' + (t.isHorizontal() ? "right" : "bottom") + '"></div>'), o.append(u)), p.length && (p[0].style.opacity = Math.max(-a, 0)), u.length && (u[0].style.opacity = Math.max(a, 0))
                        }
                        o.transform("translate3d(" + d + "px, " + c + "px, 0px) rotateX(" + l + "deg) rotateY(" + r + "deg)")
                    }
                },
                setTransition: function (e) {
                    var t = this,
                        i = t.slides,
                        n = t.activeIndex,
                        s = t.$wrapperEl;
                    if (i.transition(e).find(".swiper-slide-shadow-top, .swiper-slide-shadow-right, .swiper-slide-shadow-bottom, .swiper-slide-shadow-left").transition(e), t.params.virtualTranslate && 0 !== e) {
                        var o = !1;
                        i.eq(n).transitionEnd(function () {
                            if (!o && t && !t.destroyed) {
                                o = !0, t.animating = !1;
                                for (var e = ["webkitTransitionEnd", "transitionend"], i = 0; i < e.length; i += 1) s.trigger(e[i])
                            }
                        })
                    }
                }
            },
            J = {
                setTranslate: function () {
                    for (var t = this, i = t.width, n = t.height, s = t.slides, o = t.$wrapperEl, a = t.slidesSizesGrid, r = t.params.coverflowEffect, l = t.isHorizontal(), d = t.translate, c = l ? i / 2 - d : n / 2 - d, p = l ? r.rotate : -r.rotate, u = r.depth, f = 0, v = s.length; f < v; f += 1) {
                        var m = s.eq(f),
                            g = a[f],
                            w = (c - m[0].swiperSlideOffset - g / 2) / g * r.modifier,
                            y = l ? p * w : 0,
                            b = l ? 0 : p * w,
                            T = -u * Math.abs(w),
                            x = l ? 0 : r.stretch * w,
                            S = l ? r.stretch * w : 0;
                        Math.abs(S) < .001 && (S = 0), Math.abs(x) < .001 && (x = 0), Math.abs(T) < .001 && (T = 0), Math.abs(y) < .001 && (y = 0), Math.abs(b) < .001 && (b = 0);
                        var C = "translate3d(" + S + "px," + x + "px," + T + "px)  rotateX(" + b + "deg) rotateY(" + y + "deg)";
                        if (m.transform(C), m[0].style.zIndex = 1 - Math.abs(Math.round(w)), r.slideShadows) {
                            var k = l ? m.find(".swiper-slide-shadow-left") : m.find(".swiper-slide-shadow-top"),
                                E = l ? m.find(".swiper-slide-shadow-right") : m.find(".swiper-slide-shadow-bottom");
                            0 === k.length && (k = e('<div class="swiper-slide-shadow-' + (l ? "left" : "top") + '"></div>'), m.append(k)), 0 === E.length && (E = e('<div class="swiper-slide-shadow-' + (l ? "right" : "bottom") + '"></div>'), m.append(E)), k.length && (k[0].style.opacity = 0 < w ? w : 0), E.length && (E[0].style.opacity = 0 < -w ? -w : 0)
                        }
                    }(h.pointerEvents || h.prefixedPointerEvents) && (o[0].style.perspectiveOrigin = c + "px 50%")
                },
                setTransition: function (e) {
                    this.slides.transition(e).find(".swiper-slide-shadow-top, .swiper-slide-shadow-right, .swiper-slide-shadow-bottom, .swiper-slide-shadow-left").transition(e)
                }
            },
            ee = {
                init: function () {
                    var e = this,
                        t = e.params.thumbs,
                        i = e.constructor;
                    t.swiper instanceof i ? (e.thumbs.swiper = t.swiper, u.extend(e.thumbs.swiper.originalParams, {
                        watchSlidesProgress: !0,
                        slideToClickedSlide: !1
                    }), u.extend(e.thumbs.swiper.params, {
                        watchSlidesProgress: !0,
                        slideToClickedSlide: !1
                    })) : u.isObject(t.swiper) && (e.thumbs.swiper = new i(u.extend({}, t.swiper, {
                        watchSlidesVisibility: !0,
                        watchSlidesProgress: !0,
                        slideToClickedSlide: !1
                    })), e.thumbs.swiperCreated = !0), e.thumbs.swiper.$el.addClass(e.params.thumbs.thumbsContainerClass), e.thumbs.swiper.on("tap", e.thumbs.onThumbClick)
                },
                onThumbClick: function () {
                    var t = this,
                        i = t.thumbs.swiper;
                    if (i) {
                        var n = i.clickedIndex,
                            s = i.clickedSlide;
                        if (!(s && e(s).hasClass(t.params.thumbs.slideThumbActiveClass) || null == n)) {
                            var o;
                            if (o = i.params.loop ? parseInt(e(i.clickedSlide).attr("data-swiper-slide-index"), 10) : n, t.params.loop) {
                                var a = t.activeIndex;
                                t.slides.eq(a).hasClass(t.params.slideDuplicateClass) && (t.loopFix(), t._clientLeft = t.$wrapperEl[0].clientLeft, a = t.activeIndex);
                                var r = t.slides.eq(a).prevAll('[data-swiper-slide-index="' + o + '"]').eq(0).index(),
                                    l = t.slides.eq(a).nextAll('[data-swiper-slide-index="' + o + '"]').eq(0).index();
                                o = void 0 === r ? l : void 0 === l ? r : l - a < a - r ? l : r
                            }
                            t.slideTo(o)
                        }
                    }
                },
                update: function (e) {
                    var t = this,
                        i = t.thumbs.swiper;
                    if (i) {
                        var n = "auto" === i.params.slidesPerView ? i.slidesPerViewDynamic() : i.params.slidesPerView;
                        if (t.realIndex !== i.realIndex) {
                            var s, o = i.activeIndex;
                            if (i.params.loop) {
                                i.slides.eq(o).hasClass(i.params.slideDuplicateClass) && (i.loopFix(), i._clientLeft = i.$wrapperEl[0].clientLeft, o = i.activeIndex);
                                var a = i.slides.eq(o).prevAll('[data-swiper-slide-index="' + t.realIndex + '"]').eq(0).index(),
                                    r = i.slides.eq(o).nextAll('[data-swiper-slide-index="' + t.realIndex + '"]').eq(0).index();
                                s = void 0 === a ? r : void 0 === r ? a : r - o == o - a ? o : r - o < o - a ? r : a
                            } else s = t.realIndex;
                            i.visibleSlidesIndexes && i.visibleSlidesIndexes.indexOf(s) < 0 && (i.params.centeredSlides ? s = o < s ? s - Math.floor(n / 2) + 1 : s + Math.floor(n / 2) - 1 : o < s && (s = s - n + 1), i.slideTo(s, e ? 0 : void 0))
                        }
                        var l = 1,
                            d = t.params.thumbs.slideThumbActiveClass;
                        if (1 < t.params.slidesPerView && !t.params.centeredSlides && (l = t.params.slidesPerView), i.slides.removeClass(d), i.params.loop || i.params.virtual)
                            for (var c = 0; c < l; c += 1) i.$wrapperEl.children('[data-swiper-slide-index="' + (t.realIndex + c) + '"]').addClass(d);
                        else
                            for (var p = 0; p < l; p += 1) i.slides.eq(t.realIndex + p).addClass(d)
                    }
                }
            },
            te = [M, P, z, L, O, I, X, {
                name: "mousewheel",
                params: {
                    mousewheel: {
                        enabled: !1,
                        releaseOnEdges: !1,
                        invert: !1,
                        forceToAxis: !1,
                        sensitivity: 1,
                        eventsTarged: "container"
                    }
                },
                create: function () {
                    var e = this;
                    u.extend(e, {
                        mousewheel: {
                            enabled: !1,
                            enable: Y.enable.bind(e),
                            disable: Y.disable.bind(e),
                            handle: Y.handle.bind(e),
                            handleMouseEnter: Y.handleMouseEnter.bind(e),
                            handleMouseLeave: Y.handleMouseLeave.bind(e),
                            lastScrollTime: u.now()
                        }
                    })
                },
                on: {
                    init: function () {
                        this.params.mousewheel.enabled && this.mousewheel.enable()
                    },
                    destroy: function () {
                        this.mousewheel.enabled && this.mousewheel.disable()
                    }
                }
            }, {
                name: "navigation",
                params: {
                    navigation: {
                        nextEl: null,
                        prevEl: null,
                        hideOnClick: !1,
                        disabledClass: "swiper-button-disabled",
                        hiddenClass: "swiper-button-hidden",
                        lockClass: "swiper-button-lock"
                    }
                },
                create: function () {
                    var e = this;
                    u.extend(e, {
                        navigation: {
                            init: N.init.bind(e),
                            update: N.update.bind(e),
                            destroy: N.destroy.bind(e),
                            onNextClick: N.onNextClick.bind(e),
                            onPrevClick: N.onPrevClick.bind(e)
                        }
                    })
                },
                on: {
                    init: function () {
                        this.navigation.init(), this.navigation.update()
                    },
                    toEdge: function () {
                        this.navigation.update()
                    },
                    fromEdge: function () {
                        this.navigation.update()
                    },
                    destroy: function () {
                        this.navigation.destroy()
                    },
                    click: function (t) {
                        var i, n = this,
                            s = n.navigation,
                            o = s.$nextEl,
                            a = s.$prevEl;
                        !n.params.navigation.hideOnClick || e(t.target).is(a) || e(t.target).is(o) || (o ? i = o.hasClass(n.params.navigation.hiddenClass) : a && (i = a.hasClass(n.params.navigation.hiddenClass)), !0 === i ? n.emit("navigationShow", n) : n.emit("navigationHide", n), o && o.toggleClass(n.params.navigation.hiddenClass), a && a.toggleClass(n.params.navigation.hiddenClass))
                    }
                }
            }, {
                name: "pagination",
                params: {
                    pagination: {
                        el: null,
                        bulletElement: "span",
                        clickable: !1,
                        hideOnClick: !1,
                        renderBullet: null,
                        renderProgressbar: null,
                        renderFraction: null,
                        renderCustom: null,
                        progressbarOpposite: !1,
                        type: "bullets",
                        dynamicBullets: !1,
                        dynamicMainBullets: 1,
                        formatFractionCurrent: function (e) {
                            return e
                        },
                        formatFractionTotal: function (e) {
                            return e
                        },
                        bulletClass: "swiper-pagination-bullet",
                        bulletActiveClass: "swiper-pagination-bullet-active",
                        modifierClass: "swiper-pagination-",
                        currentClass: "swiper-pagination-current",
                        totalClass: "swiper-pagination-total",
                        hiddenClass: "swiper-pagination-hidden",
                        progressbarFillClass: "swiper-pagination-progressbar-fill",
                        progressbarOppositeClass: "swiper-pagination-progressbar-opposite",
                        clickableClass: "swiper-pagination-clickable",
                        lockClass: "swiper-pagination-lock"
                    }
                },
                create: function () {
                    var e = this;
                    u.extend(e, {
                        pagination: {
                            init: B.init.bind(e),
                            render: B.render.bind(e),
                            update: B.update.bind(e),
                            destroy: B.destroy.bind(e),
                            dynamicBulletIndex: 0
                        }
                    })
                },
                on: {
                    init: function () {
                        this.pagination.init(), this.pagination.render(), this.pagination.update()
                    },
                    activeIndexChange: function () {
                        this.params.loop ? this.pagination.update() : void 0 === this.snapIndex && this.pagination.update()
                    },
                    snapIndexChange: function () {
                        this.params.loop || this.pagination.update()
                    },
                    slidesLengthChange: function () {
                        this.params.loop && (this.pagination.render(), this.pagination.update())
                    },
                    snapGridLengthChange: function () {
                        this.params.loop || (this.pagination.render(), this.pagination.update())
                    },
                    destroy: function () {
                        this.pagination.destroy()
                    },
                    click: function (t) {
                        var i = this;
                        i.params.pagination.el && i.params.pagination.hideOnClick && 0 < i.pagination.$el.length && !e(t.target).hasClass(i.params.pagination.bulletClass) && (!0 === i.pagination.$el.hasClass(i.params.pagination.hiddenClass) ? i.emit("paginationShow", i) : i.emit("paginationHide", i), i.pagination.$el.toggleClass(i.params.pagination.hiddenClass))
                    }
                }
            }, {
                name: "scrollbar",
                params: {
                    scrollbar: {
                        el: null,
                        dragSize: "auto",
                        hide: !1,
                        draggable: !1,
                        snapOnRelease: !0,
                        lockClass: "swiper-scrollbar-lock",
                        dragClass: "swiper-scrollbar-drag"
                    }
                },
                create: function () {
                    var e = this;
                    u.extend(e, {
                        scrollbar: {
                            init: R.init.bind(e),
                            destroy: R.destroy.bind(e),
                            updateSize: R.updateSize.bind(e),
                            setTranslate: R.setTranslate.bind(e),
                            setTransition: R.setTransition.bind(e),
                            enableDraggable: R.enableDraggable.bind(e),
                            disableDraggable: R.disableDraggable.bind(e),
                            setDragPosition: R.setDragPosition.bind(e),
                            getPointerPosition: R.getPointerPosition.bind(e),
                            onDragStart: R.onDragStart.bind(e),
                            onDragMove: R.onDragMove.bind(e),
                            onDragEnd: R.onDragEnd.bind(e),
                            isTouched: !1,
                            timeout: null,
                            dragTimeout: null
                        }
                    })
                },
                on: {
                    init: function () {
                        this.scrollbar.init(), this.scrollbar.updateSize(), this.scrollbar.setTranslate()
                    },
                    update: function () {
                        this.scrollbar.updateSize()
                    },
                    resize: function () {
                        this.scrollbar.updateSize()
                    },
                    observerUpdate: function () {
                        this.scrollbar.updateSize()
                    },
                    setTranslate: function () {
                        this.scrollbar.setTranslate()
                    },
                    setTransition: function (e) {
                        this.scrollbar.setTransition(e)
                    },
                    destroy: function () {
                        this.scrollbar.destroy()
                    }
                }
            }, {
                name: "parallax",
                params: {
                    parallax: {
                        enabled: !1
                    }
                },
                create: function () {
                    u.extend(this, {
                        parallax: {
                            setTransform: j.setTransform.bind(this),
                            setTranslate: j.setTranslate.bind(this),
                            setTransition: j.setTransition.bind(this)
                        }
                    })
                },
                on: {
                    beforeInit: function () {
                        this.params.parallax.enabled && (this.params.watchSlidesProgress = !0, this.originalParams.watchSlidesProgress = !0)
                    },
                    init: function () {
                        this.params.parallax.enabled && this.parallax.setTranslate()
                    },
                    setTranslate: function () {
                        this.params.parallax.enabled && this.parallax.setTranslate()
                    },
                    setTransition: function (e) {
                        this.params.parallax.enabled && this.parallax.setTransition(e)
                    }
                }
            }, {
                name: "zoom",
                params: {
                    zoom: {
                        enabled: !1,
                        maxRatio: 3,
                        minRatio: 1,
                        toggle: !0,
                        containerClass: "swiper-zoom-container",
                        zoomedSlideClass: "swiper-slide-zoomed"
                    }
                },
                create: function () {
                    var e = this,
                        t = {
                            enabled: !1,
                            scale: 1,
                            currentScale: 1,
                            isScaling: !1,
                            gesture: {
                                $slideEl: void 0,
                                slideWidth: void 0,
                                slideHeight: void 0,
                                $imageEl: void 0,
                                $imageWrapEl: void 0,
                                maxRatio: 3
                            },
                            image: {
                                isTouched: void 0,
                                isMoved: void 0,
                                currentX: void 0,
                                currentY: void 0,
                                minX: void 0,
                                minY: void 0,
                                maxX: void 0,
                                maxY: void 0,
                                width: void 0,
                                height: void 0,
                                startX: void 0,
                                startY: void 0,
                                touchesStart: {},
                                touchesCurrent: {}
                            },
                            velocity: {
                                x: void 0,
                                y: void 0,
                                prevPositionX: void 0,
                                prevPositionY: void 0,
                                prevTime: void 0
                            }
                        };
                    "onGestureStart onGestureChange onGestureEnd onTouchStart onTouchMove onTouchEnd onTransitionEnd toggle enable disable in out".split(" ").forEach(function (i) {
                        t[i] = F[i].bind(e)
                    }), u.extend(e, {
                        zoom: t
                    });
                    var i = 1;
                    Object.defineProperty(e.zoom, "scale", {
                        get: function () {
                            return i
                        },
                        set: function (t) {
                            if (i !== t) {
                                var n = e.zoom.gesture.$imageEl ? e.zoom.gesture.$imageEl[0] : void 0,
                                    s = e.zoom.gesture.$slideEl ? e.zoom.gesture.$slideEl[0] : void 0;
                                e.emit("zoomChange", t, n, s)
                            }
                            i = t
                        }
                    })
                },
                on: {
                    init: function () {
                        this.params.zoom.enabled && this.zoom.enable()
                    },
                    destroy: function () {
                        this.zoom.disable()
                    },
                    touchStart: function (e) {
                        this.zoom.enabled && this.zoom.onTouchStart(e)
                    },
                    touchEnd: function (e) {
                        this.zoom.enabled && this.zoom.onTouchEnd(e)
                    },
                    doubleTap: function (e) {
                        this.params.zoom.enabled && this.zoom.enabled && this.params.zoom.toggle && this.zoom.toggle(e)
                    },
                    transitionEnd: function () {
                        this.zoom.enabled && this.params.zoom.enabled && this.zoom.onTransitionEnd()
                    }
                }
            }, {
                name: "lazy",
                params: {
                    lazy: {
                        enabled: !1,
                        loadPrevNext: !1,
                        loadPrevNextAmount: 1,
                        loadOnTransitionStart: !1,
                        elementClass: "swiper-lazy",
                        loadingClass: "swiper-lazy-loading",
                        loadedClass: "swiper-lazy-loaded",
                        preloaderClass: "swiper-lazy-preloader"
                    }
                },
                create: function () {
                    u.extend(this, {
                        lazy: {
                            initialImageLoaded: !1,
                            load: W.load.bind(this),
                            loadInSlide: W.loadInSlide.bind(this)
                        }
                    })
                },
                on: {
                    beforeInit: function () {
                        this.params.lazy.enabled && this.params.preloadImages && (this.params.preloadImages = !1)
                    },
                    init: function () {
                        this.params.lazy.enabled && !this.params.loop && 0 === this.params.initialSlide && this.lazy.load()
                    },
                    scroll: function () {
                        this.params.freeMode && !this.params.freeModeSticky && this.lazy.load()
                    },
                    resize: function () {
                        this.params.lazy.enabled && this.lazy.load()
                    },
                    scrollbarDragMove: function () {
                        this.params.lazy.enabled && this.lazy.load()
                    },
                    transitionStart: function () {
                        var e = this;
                        e.params.lazy.enabled && (!e.params.lazy.loadOnTransitionStart && (e.params.lazy.loadOnTransitionStart || e.lazy.initialImageLoaded) || e.lazy.load())
                    },
                    transitionEnd: function () {
                        this.params.lazy.enabled && !this.params.lazy.loadOnTransitionStart && this.lazy.load()
                    }
                }
            }, {
                name: "controller",
                params: {
                    controller: {
                        control: void 0,
                        inverse: !1,
                        by: "slide"
                    }
                },
                create: function () {
                    var e = this;
                    u.extend(e, {
                        controller: {
                            control: e.params.controller.control,
                            getInterpolateFunction: q.getInterpolateFunction.bind(e),
                            setTranslate: q.setTranslate.bind(e),
                            setTransition: q.setTransition.bind(e)
                        }
                    })
                },
                on: {
                    update: function () {
                        this.controller.control && this.controller.spline && (this.controller.spline = void 0, delete this.controller.spline)
                    },
                    resize: function () {
                        this.controller.control && this.controller.spline && (this.controller.spline = void 0, delete this.controller.spline)
                    },
                    observerUpdate: function () {
                        this.controller.control && this.controller.spline && (this.controller.spline = void 0, delete this.controller.spline)
                    },
                    setTranslate: function (e, t) {
                        this.controller.control && this.controller.setTranslate(e, t)
                    },
                    setTransition: function (e, t) {
                        this.controller.control && this.controller.setTransition(e, t)
                    }
                }
            }, {
                name: "a11y",
                params: {
                    a11y: {
                        enabled: !0,
                        notificationClass: "swiper-notification",
                        prevSlideMessage: "Previous slide",
                        nextSlideMessage: "Next slide",
                        firstSlideMessage: "This is the first slide",
                        lastSlideMessage: "This is the last slide",
                        paginationBulletMessage: "Go to slide {{index}}"
                    }
                },
                create: function () {
                    var t = this;
                    u.extend(t, {
                        a11y: {
                            liveRegion: e('<span class="' + t.params.a11y.notificationClass + '" aria-live="assertive" aria-atomic="true"></span>')
                        }
                    }), Object.keys(G).forEach(function (e) {
                        t.a11y[e] = G[e].bind(t)
                    })
                },
                on: {
                    init: function () {
                        this.params.a11y.enabled && (this.a11y.init(), this.a11y.updateNavigation())
                    },
                    toEdge: function () {
                        this.params.a11y.enabled && this.a11y.updateNavigation()
                    },
                    fromEdge: function () {
                        this.params.a11y.enabled && this.a11y.updateNavigation()
                    },
                    paginationUpdate: function () {
                        this.params.a11y.enabled && this.a11y.updatePagination()
                    },
                    destroy: function () {
                        this.params.a11y.enabled && this.a11y.destroy()
                    }
                }
            }, {
                name: "history",
                params: {
                    history: {
                        enabled: !1,
                        replaceState: !1,
                        key: "slides"
                    }
                },
                create: function () {
                    var e = this;
                    u.extend(e, {
                        history: {
                            init: V.init.bind(e),
                            setHistory: V.setHistory.bind(e),
                            setHistoryPopState: V.setHistoryPopState.bind(e),
                            scrollToSlide: V.scrollToSlide.bind(e),
                            destroy: V.destroy.bind(e)
                        }
                    })
                },
                on: {
                    init: function () {
                        this.params.history.enabled && this.history.init()
                    },
                    destroy: function () {
                        this.params.history.enabled && this.history.destroy()
                    },
                    transitionEnd: function () {
                        this.history.initialized && this.history.setHistory(this.params.history.key, this.activeIndex)
                    }
                }
            }, {
                name: "hash-navigation",
                params: {
                    hashNavigation: {
                        enabled: !1,
                        replaceState: !1,
                        watchState: !1
                    }
                },
                create: function () {
                    var e = this;
                    u.extend(e, {
                        hashNavigation: {
                            initialized: !1,
                            init: _.init.bind(e),
                            destroy: _.destroy.bind(e),
                            setHash: _.setHash.bind(e),
                            onHashCange: _.onHashCange.bind(e)
                        }
                    })
                },
                on: {
                    init: function () {
                        this.params.hashNavigation.enabled && this.hashNavigation.init()
                    },
                    destroy: function () {
                        this.params.hashNavigation.enabled && this.hashNavigation.destroy()
                    },
                    transitionEnd: function () {
                        this.hashNavigation.initialized && this.hashNavigation.setHash()
                    }
                }
            }, {
                name: "autoplay",
                params: {
                    autoplay: {
                        enabled: !1,
                        delay: 3e3,
                        waitForTransition: !0,
                        disableOnInteraction: !0,
                        stopOnLastSlide: !1,
                        reverseDirection: !1
                    }
                },
                create: function () {
                    var e = this;
                    u.extend(e, {
                        autoplay: {
                            running: !1,
                            paused: !1,
                            run: U.run.bind(e),
                            start: U.start.bind(e),
                            stop: U.stop.bind(e),
                            pause: U.pause.bind(e),
                            onTransitionEnd: function (t) {
                                e && !e.destroyed && e.$wrapperEl && t.target === this && (e.$wrapperEl[0].removeEventListener("transitionend", e.autoplay.onTransitionEnd), e.$wrapperEl[0].removeEventListener("webkitTransitionEnd", e.autoplay.onTransitionEnd), e.autoplay.paused = !1, e.autoplay.running ? e.autoplay.run() : e.autoplay.stop())
                            }
                        }
                    })
                },
                on: {
                    init: function () {
                        this.params.autoplay.enabled && this.autoplay.start()
                    },
                    beforeTransitionStart: function (e, t) {
                        this.autoplay.running && (t || !this.params.autoplay.disableOnInteraction ? this.autoplay.pause(e) : this.autoplay.stop())
                    },
                    sliderFirstMove: function () {
                        this.autoplay.running && (this.params.autoplay.disableOnInteraction ? this.autoplay.stop() : this.autoplay.pause())
                    },
                    destroy: function () {
                        this.autoplay.running && this.autoplay.stop()
                    }
                }
            }, {
                name: "effect-fade",
                params: {
                    fadeEffect: {
                        crossFade: !1
                    }
                },
                create: function () {
                    u.extend(this, {
                        fadeEffect: {
                            setTranslate: K.setTranslate.bind(this),
                            setTransition: K.setTransition.bind(this)
                        }
                    })
                },
                on: {
                    beforeInit: function () {
                        var e = this;
                        if ("fade" === e.params.effect) {
                            e.classNames.push(e.params.containerModifierClass + "fade");
                            var t = {
                                slidesPerView: 1,
                                slidesPerColumn: 1,
                                slidesPerGroup: 1,
                                watchSlidesProgress: !0,
                                spaceBetween: 0,
                                virtualTranslate: !0
                            };
                            u.extend(e.params, t), u.extend(e.originalParams, t)
                        }
                    },
                    setTranslate: function () {
                        "fade" === this.params.effect && this.fadeEffect.setTranslate()
                    },
                    setTransition: function (e) {
                        "fade" === this.params.effect && this.fadeEffect.setTransition(e)
                    }
                }
            }, {
                name: "effect-cube",
                params: {
                    cubeEffect: {
                        slideShadows: !0,
                        shadow: !0,
                        shadowOffset: 20,
                        shadowScale: .94
                    }
                },
                create: function () {
                    u.extend(this, {
                        cubeEffect: {
                            setTranslate: Q.setTranslate.bind(this),
                            setTransition: Q.setTransition.bind(this)
                        }
                    })
                },
                on: {
                    beforeInit: function () {
                        var e = this;
                        if ("cube" === e.params.effect) {
                            e.classNames.push(e.params.containerModifierClass + "cube"), e.classNames.push(e.params.containerModifierClass + "3d");
                            var t = {
                                slidesPerView: 1,
                                slidesPerColumn: 1,
                                slidesPerGroup: 1,
                                watchSlidesProgress: !0,
                                resistanceRatio: 0,
                                spaceBetween: 0,
                                centeredSlides: !1,
                                virtualTranslate: !0
                            };
                            u.extend(e.params, t), u.extend(e.originalParams, t)
                        }
                    },
                    setTranslate: function () {
                        "cube" === this.params.effect && this.cubeEffect.setTranslate()
                    },
                    setTransition: function (e) {
                        "cube" === this.params.effect && this.cubeEffect.setTransition(e)
                    }
                }
            }, {
                name: "effect-flip",
                params: {
                    flipEffect: {
                        slideShadows: !0,
                        limitRotation: !0
                    }
                },
                create: function () {
                    u.extend(this, {
                        flipEffect: {
                            setTranslate: Z.setTranslate.bind(this),
                            setTransition: Z.setTransition.bind(this)
                        }
                    })
                },
                on: {
                    beforeInit: function () {
                        var e = this;
                        if ("flip" === e.params.effect) {
                            e.classNames.push(e.params.containerModifierClass + "flip"), e.classNames.push(e.params.containerModifierClass + "3d");
                            var t = {
                                slidesPerView: 1,
                                slidesPerColumn: 1,
                                slidesPerGroup: 1,
                                watchSlidesProgress: !0,
                                spaceBetween: 0,
                                virtualTranslate: !0
                            };
                            u.extend(e.params, t), u.extend(e.originalParams, t)
                        }
                    },
                    setTranslate: function () {
                        "flip" === this.params.effect && this.flipEffect.setTranslate()
                    },
                    setTransition: function (e) {
                        "flip" === this.params.effect && this.flipEffect.setTransition(e)
                    }
                }
            }, {
                name: "effect-coverflow",
                params: {
                    coverflowEffect: {
                        rotate: 50,
                        stretch: 0,
                        depth: 100,
                        modifier: 1,
                        slideShadows: !0
                    }
                },
                create: function () {
                    u.extend(this, {
                        coverflowEffect: {
                            setTranslate: J.setTranslate.bind(this),
                            setTransition: J.setTransition.bind(this)
                        }
                    })
                },
                on: {
                    beforeInit: function () {
                        var e = this;
                        "coverflow" === e.params.effect && (e.classNames.push(e.params.containerModifierClass + "coverflow"), e.classNames.push(e.params.containerModifierClass + "3d"), e.params.watchSlidesProgress = !0, e.originalParams.watchSlidesProgress = !0)
                    },
                    setTranslate: function () {
                        "coverflow" === this.params.effect && this.coverflowEffect.setTranslate()
                    },
                    setTransition: function (e) {
                        "coverflow" === this.params.effect && this.coverflowEffect.setTransition(e)
                    }
                }
            }, {
                name: "thumbs",
                params: {
                    thumbs: {
                        swiper: null,
                        slideThumbActiveClass: "swiper-slide-thumb-active",
                        thumbsContainerClass: "swiper-container-thumbs"
                    }
                },
                create: function () {
                    u.extend(this, {
                        thumbs: {
                            swiper: null,
                            init: ee.init.bind(this),
                            update: ee.update.bind(this),
                            onThumbClick: ee.onThumbClick.bind(this)
                        }
                    })
                },
                on: {
                    beforeInit: function () {
                        var e = this.params.thumbs;
                        e && e.swiper && (this.thumbs.init(), this.thumbs.update(!0))
                    },
                    slideChange: function () {
                        this.thumbs.swiper && this.thumbs.update()
                    },
                    update: function () {
                        this.thumbs.swiper && this.thumbs.update()
                    },
                    resize: function () {
                        this.thumbs.swiper && this.thumbs.update()
                    },
                    observerUpdate: function () {
                        this.thumbs.swiper && this.thumbs.update()
                    },
                    setTransition: function (e) {
                        var t = this.thumbs.swiper;
                        t && t.setTransition(e)
                    },
                    beforeDestroy: function () {
                        var e = this.thumbs.swiper;
                        e && this.thumbs.swiperCreated && e && e.destroy()
                    }
                }
            }];
        return void 0 === $.use && ($.use = $.Class.use,
            $.installModule = $.Class.installModule), $.use(te), $
    }),
    function (e, t) {
        "use strict";
        var i = "model",
            n = "name",
            s = "type",
            o = "vendor",
            a = "version",
            r = "mobile",
            l = "tablet",
            d = {
                extend: function (e, t) {
                    var i = {};
                    for (var n in e) t[n] && t[n].length % 2 == 0 ? i[n] = t[n].concat(e[n]) : i[n] = e[n];
                    return i
                },
                has: function (e, t) {
                    return "string" == typeof e && -1 !== t.toLowerCase().indexOf(e.toLowerCase())
                },
                lowerize: function (e) {
                    return e.toLowerCase()
                },
                major: function (e) {
                    return "string" == typeof e ? e.replace(/[^\d\.]/g, "").split(".")[0] : void 0
                },
                trim: function (e) {
                    return e.replace(/^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g, "")
                }
            },
            c = {
                rgx: function (e, t) {
                    for (var i, n, s, o, a, r, l = 0; l < t.length && !a;) {
                        var d = t[l],
                            c = t[l + 1];
                        for (i = n = 0; i < d.length && !a;)
                            if (a = d[i++].exec(e))
                                for (s = 0; s < c.length; s++) r = a[++n], o = c[s], "object" == typeof o && o.length > 0 ? 2 == o.length ? "function" == typeof o[1] ? this[o[0]] = o[1].call(this, r) : this[o[0]] = o[1] : 3 == o.length ? "function" != typeof o[1] || o[1].exec && o[1].test ? this[o[0]] = r ? r.replace(o[1], o[2]) : void 0 : this[o[0]] = r ? o[1].call(this, r, o[2]) : void 0 : 4 == o.length && (this[o[0]] = r ? o[3].call(this, r.replace(o[1], o[2])) : void 0) : this[o] = r || void 0;
                        l += 2
                    }
                },
                str: function (e, t) {
                    for (var i in t)
                        if ("object" == typeof t[i] && t[i].length > 0) {
                            for (var n = 0; n < t[i].length; n++)
                                if (d.has(t[i][n], e)) return "?" === i ? void 0 : i
                        } else if (d.has(t[i], e)) return "?" === i ? void 0 : i;
                    return e
                }
            },
            p = {
                browser: {
                    oldsafari: {
                        version: {
                            "1.0": "/8",
                            1.2: "/1",
                            1.3: "/3",
                            "2.0": "/412",
                            "2.0.2": "/416",
                            "2.0.3": "/417",
                            "2.0.4": "/419",
                            "?": "/"
                        }
                    }
                },
                device: {
                    amazon: {
                        model: {
                            "Fire Phone": ["SD", "KF"]
                        }
                    },
                    sprint: {
                        model: {
                            "Evo Shift 4G": "7373KT"
                        },
                        vendor: {
                            HTC: "APA",
                            Sprint: "Sprint"
                        }
                    }
                },
                os: {
                    windows: {
                        version: {
                            ME: "4.90",
                            "NT 3.11": "NT3.51",
                            "NT 4.0": "NT4.0",
                            2000: "NT 5.0",
                            XP: ["NT 5.1", "NT 5.2"],
                            Vista: "NT 6.0",
                            7: "NT 6.1",
                            8: "NT 6.2",
                            8.1: "NT 6.3",
                            10: ["NT 6.4", "NT 10.0"],
                            RT: "ARM"
                        }
                    }
                }
            },
            u = {
                browser: [
                    [/(opera\smini)\/([\w\.-]+)/i, /(opera\s[mobiletab]+).+version\/([\w\.-]+)/i, /(opera).+version\/([\w\.]+)/i, /(opera)[\/\s]+([\w\.]+)/i],
                    [n, a],
                    [/(opios)[\/\s]+([\w\.]+)/i],
                    [
                        [n, "Opera Mini"], a
                    ],
                    [/\s(opr)\/([\w\.]+)/i],
                    [
                        [n, "Opera"], a
                    ],
                    [/(kindle)\/([\w\.]+)/i, /(lunascape|maxthon|netfront|jasmine|blazer)[\/\s]?([\w\.]*)/i, /(avant\s|iemobile|slim|baidu)(?:browser)?[\/\s]?([\w\.]*)/i, /(?:ms|\()(ie)\s([\w\.]+)/i, /(rekonq)\/([\w\.]*)/i, /(chromium|flock|rockmelt|midori|epiphany|silk|skyfire|ovibrowser|bolt|iron|vivaldi|iridium|phantomjs|bowser|quark|qupzilla|falkon)\/([\w\.-]+)/i],
                    [n, a],
                    [/(konqueror)\/([\w\.]+)/i],
                    [
                        [n, "Konqueror"], a
                    ],
                    [/(trident).+rv[:\s]([\w\.]+).+like\sgecko/i],
                    [
                        [n, "IE"], a
                    ],
                    [/(edge|edgios|edga|edg)\/((\d+)?[\w\.]+)/i],
                    [
                        [n, "Edge"], a
                    ],
                    [/(yabrowser)\/([\w\.]+)/i],
                    [
                        [n, "Yandex"], a
                    ],
                    [/(puffin)\/([\w\.]+)/i],
                    [
                        [n, "Puffin"], a
                    ],
                    [/(focus)\/([\w\.]+)/i],
                    [
                        [n, "Firefox Focus"], a
                    ],
                    [/(opt)\/([\w\.]+)/i],
                    [
                        [n, "Opera Touch"], a
                    ],
                    [/((?:[\s\/])uc?\s?browser|(?:juc.+)ucweb)[\/\s]?([\w\.]+)/i],
                    [
                        [n, "UCBrowser"], a
                    ],
                    [/(comodo_dragon)\/([\w\.]+)/i],
                    [
                        [n, /_/g, " "], a
                    ],
                    [/(windowswechat qbcore)\/([\w\.]+)/i],
                    [
                        [n, "WeChat(Win) Desktop"], a
                    ],
                    [/(micromessenger)\/([\w\.]+)/i],
                    [
                        [n, "WeChat"], a
                    ],
                    [/(brave)\/([\w\.]+)/i],
                    [
                        [n, "Brave"], a
                    ],
                    [/(qqbrowserlite)\/([\w\.]+)/i],
                    [n, a],
                    [/(QQ)\/([\d\.]+)/i],
                    [n, a],
                    [/m?(qqbrowser)[\/\s]?([\w\.]+)/i],
                    [n, a],
                    [/(BIDUBrowser)[\/\s]?([\w\.]+)/i],
                    [n, a],
                    [/(2345Explorer)[\/\s]?([\w\.]+)/i],
                    [n, a],
                    [/(MetaSr)[\/\s]?([\w\.]+)/i],
                    [n],
                    [/(LBBROWSER)/i],
                    [n],
                    [/xiaomi\/miuibrowser\/([\w\.]+)/i],
                    [a, [n, "MIUI Browser"]],
                    [/;fbav\/([\w\.]+);/i],
                    [a, [n, "Facebook"]],
                    [/safari\s(line)\/([\w\.]+)/i, /android.+(line)\/([\w\.]+)\/iab/i],
                    [n, a],
                    [/headlesschrome(?:\/([\w\.]+)|\s)/i],
                    [a, [n, "Chrome Headless"]],
                    [/\swv\).+(chrome)\/([\w\.]+)/i],
                    [
                        [n, /(.+)/, "$1 WebView"], a
                    ],
                    [/((?:oculus|samsung)browser)\/([\w\.]+)/i],
                    [
                        [n, /(.+(?:g|us))(.+)/, "$1 $2"], a
                    ],
                    [/android.+version\/([\w\.]+)\s+(?:mobile\s?safari|safari)*/i],
                    [a, [n, "Android Browser"]],
                    [/(sailfishbrowser)\/([\w\.]+)/i],
                    [
                        [n, "Sailfish Browser"], a
                    ],
                    [/(chrome|omniweb|arora|[tizenoka]{5}\s?browser)\/v?([\w\.]+)/i],
                    [n, a],
                    [/(dolfin)\/([\w\.]+)/i],
                    [
                        [n, "Dolphin"], a
                    ],
                    [/((?:android.+)crmo|crios)\/([\w\.]+)/i],
                    [
                        [n, "Chrome"], a
                    ],
                    [/(coast)\/([\w\.]+)/i],
                    [
                        [n, "Opera Coast"], a
                    ],
                    [/fxios\/([\w\.-]+)/i],
                    [a, [n, "Firefox"]],
                    [/version\/([\w\.]+).+?mobile\/\w+\s(safari)/i],
                    [a, [n, "Mobile Safari"]],
                    [/version\/([\w\.]+).+?(mobile\s?safari|safari)/i],
                    [a, n],
                    [/webkit.+?(gsa)\/([\w\.]+).+?(mobile\s?safari|safari)(\/[\w\.]+)/i],
                    [
                        [n, "GSA"], a
                    ],
                    [/webkit.+?(mobile\s?safari|safari)(\/[\w\.]+)/i],
                    [n, [a, c.str, p.browser.oldsafari.version]],
                    [/(webkit|khtml)\/([\w\.]+)/i],
                    [n, a],
                    [/(navigator|netscape)\/([\w\.-]+)/i],
                    [
                        [n, "Netscape"], a
                    ],
                    [/(swiftfox)/i, /(icedragon|iceweasel|camino|chimera|fennec|maemo\sbrowser|minimo|conkeror)[\/\s]?([\w\.\+]+)/i, /(firefox|seamonkey|k-meleon|icecat|iceape|firebird|phoenix|palemoon|basilisk|waterfox)\/([\w\.-]+)$/i, /(mozilla)\/([\w\.]+).+rv\:.+gecko\/\d+/i, /(polaris|lynx|dillo|icab|doris|amaya|w3m|netsurf|sleipnir)[\/\s]?([\w\.]+)/i, /(links)\s\(([\w\.]+)/i, /(gobrowser)\/?([\w\.]*)/i, /(ice\s?browser)\/v?([\w\._]+)/i, /(mosaic)[\/\s]([\w\.]+)/i],
                    [n, a]
                ],
                cpu: [
                    [/(?:(amd|x(?:(?:86|64)[_-])?|wow|win)64)[;\)]/i],
                    [
                        ["architecture", "amd64"]
                    ],
                    [/(ia32(?=;))/i],
                    [
                        ["architecture", d.lowerize]
                    ],
                    [/((?:i[346]|x)86)[;\)]/i],
                    [
                        ["architecture", "ia32"]
                    ],
                    [/windows\s(ce|mobile);\sppc;/i],
                    [
                        ["architecture", "arm"]
                    ],
                    [/((?:ppc|powerpc)(?:64)?)(?:\smac|;|\))/i],
                    [
                        ["architecture", /ower/, "", d.lowerize]
                    ],
                    [/(sun4\w)[;\)]/i],
                    [
                        ["architecture", "sparc"]
                    ],
                    [/((?:avr32|ia64(?=;))|68k(?=\))|arm(?:64|(?=v\d+[;l]))|(?=atmel\s)avr|(?:irix|mips|sparc)(?:64)?(?=;)|pa-risc)/i],
                    [
                        ["architecture", d.lowerize]
                    ]
                ],
                device: [
                    [/\((ipad|playbook);[\w\s\),;-]+(rim|apple)/i],
                    [i, o, [s, l]],
                    [/applecoremedia\/[\w\.]+ \((ipad)/],
                    [i, [o, "Apple"],
                        [s, l]
                    ],
                    [/(apple\s{0,1}tv)/i],
                    [
                        [i, "Apple TV"],
                        [o, "Apple"]
                    ],
                    [/(archos)\s(gamepad2?)/i, /(hp).+(touchpad)/i, /(hp).+(tablet)/i, /(kindle)\/([\w\.]+)/i, /\s(nook)[\w\s]+build\/(\w+)/i, /(dell)\s(strea[kpr\s\d]*[\dko])/i],
                    [o, i, [s, l]],
                    [/(kf[A-z]+)\sbuild\/.+silk\//i],
                    [i, [o, "Amazon"],
                        [s, l]
                    ],
                    [/(sd|kf)[0349hijorstuw]+\sbuild\/.+silk\//i],
                    [
                        [i, c.str, p.device.amazon.model],
                        [o, "Amazon"],
                        [s, r]
                    ],
                    [/android.+aft([bms])\sbuild/i],
                    [i, [o, "Amazon"],
                        [s, "smarttv"]
                    ],
                    [/\((ip[honed|\s\w*]+);.+(apple)/i],
                    [i, o, [s, r]],
                    [/\((ip[honed|\s\w*]+);/i],
                    [i, [o, "Apple"],
                        [s, r]
                    ],
                    [/(blackberry)[\s-]?(\w+)/i, /(blackberry|benq|palm(?=\-)|sonyericsson|acer|asus|dell|meizu|motorola|polytron)[\s_-]?([\w-]*)/i, /(hp)\s([\w\s]+\w)/i, /(asus)-?(\w+)/i],
                    [o, i, [s, r]],
                    [/\(bb10;\s(\w+)/i],
                    [i, [o, "BlackBerry"],
                        [s, r]
                    ],
                    [/android.+(transfo[prime\s]{4,10}\s\w+|eeepc|slider\s\w+|nexus 7|padfone|p00c)/i],
                    [i, [o, "Asus"],
                        [s, l]
                    ],
                    [/(sony)\s(tablet\s[ps])\sbuild\//i, /(sony)?(?:sgp.+)\sbuild\//i],
                    [
                        [o, "Sony"],
                        [i, "Xperia Tablet"],
                        [s, l]
                    ],
                    [/android.+\s([c-g]\d{4}|so[-l]\w+)(?=\sbuild\/|\).+chrome\/(?![1-6]{0,1}\d\.))/i],
                    [i, [o, "Sony"],
                        [s, r]
                    ],
                    [/\s(ouya)\s/i, /(nintendo)\s([wids3u]+)/i],
                    [o, i, [s, "console"]],
                    [/android.+;\s(shield)\sbuild/i],
                    [i, [o, "Nvidia"],
                        [s, "console"]
                    ],
                    [/(playstation\s[34portablevi]+)/i],
                    [i, [o, "Sony"],
                        [s, "console"]
                    ],
                    [/(sprint\s(\w+))/i],
                    [
                        [o, c.str, p.device.sprint.vendor],
                        [i, c.str, p.device.sprint.model],
                        [s, r]
                    ],
                    [/(htc)[;_\s-]+([\w\s]+(?=\)|\sbuild)|\w+)/i, /(zte)-(\w*)/i, /(alcatel|geeksphone|nexian|panasonic|(?=;\s)sony)[_\s-]?([\w-]*)/i],
                    [o, [i, /_/g, " "],
                        [s, r]
                    ],
                    [/(nexus\s9)/i],
                    [i, [o, "HTC"],
                        [s, l]
                    ],
                    [/d\/huawei([\w\s-]+)[;\)]/i, /(nexus\s6p)/i],
                    [i, [o, "Huawei"],
                        [s, r]
                    ],
                    [/(microsoft);\s(lumia[\s\w]+)/i],
                    [o, i, [s, r]],
                    [/[\s\(;](xbox(?:\sone)?)[\s\);]/i],
                    [i, [o, "Microsoft"],
                        [s, "console"]
                    ],
                    [/(kin\.[onetw]{3})/i],
                    [
                        [i, /\./g, " "],
                        [o, "Microsoft"],
                        [s, r]
                    ],
                    [/\s(milestone|droid(?:[2-4x]|\s(?:bionic|x2|pro|razr))?:?(\s4g)?)[\w\s]+build\//i, /mot[\s-]?(\w*)/i, /(XT\d{3,4}) build\//i, /(nexus\s6)/i],
                    [i, [o, "Motorola"],
                        [s, r]
                    ],
                    [/android.+\s(mz60\d|xoom[\s2]{0,2})\sbuild\//i],
                    [i, [o, "Motorola"],
                        [s, l]
                    ],
                    [/hbbtv\/\d+\.\d+\.\d+\s+\([\w\s]*;\s*(\w[^;]*);([^;]*)/i],
                    [
                        [o, d.trim],
                        [i, d.trim],
                        [s, "smarttv"]
                    ],
                    [/hbbtv.+maple;(\d+)/i],
                    [
                        [i, /^/, "SmartTV"],
                        [o, "Samsung"],
                        [s, "smarttv"]
                    ],
                    [/\(dtv[\);].+(aquos)/i],
                    [i, [o, "Sharp"],
                        [s, "smarttv"]
                    ],
                    [/android.+((sch-i[89]0\d|shw-m380s|gt-p\d{4}|gt-n\d+|sgh-t8[56]9|nexus 10))/i, /((SM-T\w+))/i],
                    [
                        [o, "Samsung"], i, [s, l]
                    ],
                    [/smart-tv.+(samsung)/i],
                    [o, [s, "smarttv"], i],
                    [/((s[cgp]h-\w+|gt-\w+|galaxy\snexus|sm-\w[\w\d]+))/i, /(sam[sung]*)[\s-]*(\w+-?[\w-]*)/i, /sec-((sgh\w+))/i],
                    [
                        [o, "Samsung"], i, [s, r]
                    ],
                    [/sie-(\w*)/i],
                    [i, [o, "Siemens"],
                        [s, r]
                    ],
                    [/(maemo|nokia).*(n900|lumia\s\d+)/i, /(nokia)[\s_-]?([\w-]*)/i],
                    [
                        [o, "Nokia"], i, [s, r]
                    ],
                    [/android[x\d\.\s;]+\s([ab][1-7]\-?[0178a]\d\d?)/i],
                    [i, [o, "Acer"],
                        [s, l]
                    ],
                    [/android.+([vl]k\-?\d{3})\s+build/i],
                    [i, [o, "LG"],
                        [s, l]
                    ],
                    [/android\s3\.[\s\w;-]{10}(lg?)-([06cv9]{3,4})/i],
                    [
                        [o, "LG"], i, [s, l]
                    ],
                    [/(lg) netcast\.tv/i],
                    [o, i, [s, "smarttv"]],
                    [/(nexus\s[45])/i, /lg[e;\s\/-]+(\w*)/i, /android.+lg(\-?[\d\w]+)\s+build/i],
                    [i, [o, "LG"],
                        [s, r]
                    ],
                    [/(lenovo)\s?(s(?:5000|6000)(?:[\w-]+)|tab(?:[\s\w]+))/i],
                    [o, i, [s, l]],
                    [/android.+(ideatab[a-z0-9\-\s]+)/i],
                    [i, [o, "Lenovo"],
                        [s, l]
                    ],
                    [/(lenovo)[_\s-]?([\w-]+)/i],
                    [o, i, [s, r]],
                    [/linux;.+((jolla));/i],
                    [o, i, [s, r]],
                    [/((pebble))app\/[\d\.]+\s/i],
                    [o, i, [s, "wearable"]],
                    [/android.+;\s(oppo)\s?([\w\s]+)\sbuild/i],
                    [o, i, [s, r]],
                    [/crkey/i],
                    [
                        [i, "Chromecast"],
                        [o, "Google"]
                    ],
                    [/android.+;\s(glass)\s\d/i],
                    [i, [o, "Google"],
                        [s, "wearable"]
                    ],
                    [/android.+;\s(pixel c)[\s)]/i],
                    [i, [o, "Google"],
                        [s, l]
                    ],
                    [/android.+;\s(pixel( [23])?( xl)?)[\s)]/i],
                    [i, [o, "Google"],
                        [s, r]
                    ],
                    [/android.+;\s(\w+)\s+build\/hm\1/i, /android.+(hm[\s\-_]*note?[\s_]*(?:\d\w)?)\s+build/i, /android.+(mi[\s\-_]*(?:a\d|one|one[\s_]plus|note lte)?[\s_]*(?:\d?\w?)[\s_]*(?:plus)?)\s+build/i, /android.+(redmi[\s\-_]*(?:note)?(?:[\s_]*[\w\s]+))\s+build/i],
                    [
                        [i, /_/g, " "],
                        [o, "Xiaomi"],
                        [s, r]
                    ],
                    [/android.+(mi[\s\-_]*(?:pad)(?:[\s_]*[\w\s]+))\s+build/i],
                    [
                        [i, /_/g, " "],
                        [o, "Xiaomi"],
                        [s, l]
                    ],
                    [/android.+;\s(m[1-5]\snote)\sbuild/i],
                    [i, [o, "Meizu"],
                        [s, r]
                    ],
                    [/(mz)-([\w-]{2,})/i],
                    [
                        [o, "Meizu"], i, [s, r]
                    ],
                    [/android.+a000(1)\s+build/i, /android.+oneplus\s(a\d{4})\s+build/i],
                    [i, [o, "OnePlus"],
                        [s, r]
                    ],
                    [/android.+[;\/]\s*(RCT[\d\w]+)\s+build/i],
                    [i, [o, "RCA"],
                        [s, l]
                    ],
                    [/android.+[;\/\s]+(Venue[\d\s]{2,7})\s+build/i],
                    [i, [o, "Dell"],
                        [s, l]
                    ],
                    [/android.+[;\/]\s*(Q[T|M][\d\w]+)\s+build/i],
                    [i, [o, "Verizon"],
                        [s, l]
                    ],
                    [/android.+[;\/]\s+(Barnes[&\s]+Noble\s+|BN[RT])(V?.*)\s+build/i],
                    [
                        [o, "Barnes & Noble"], i, [s, l]
                    ],
                    [/android.+[;\/]\s+(TM\d{3}.*\b)\s+build/i],
                    [i, [o, "NuVision"],
                        [s, l]
                    ],
                    [/android.+;\s(k88)\sbuild/i],
                    [i, [o, "ZTE"],
                        [s, l]
                    ],
                    [/android.+[;\/]\s*(gen\d{3})\s+build.*49h/i],
                    [i, [o, "Swiss"],
                        [s, r]
                    ],
                    [/android.+[;\/]\s*(zur\d{3})\s+build/i],
                    [i, [o, "Swiss"],
                        [s, l]
                    ],
                    [/android.+[;\/]\s*((Zeki)?TB.*\b)\s+build/i],
                    [i, [o, "Zeki"],
                        [s, l]
                    ],
                    [/(android).+[;\/]\s+([YR]\d{2})\s+build/i, /android.+[;\/]\s+(Dragon[\-\s]+Touch\s+|DT)(\w{5})\sbuild/i],
                    [
                        [o, "Dragon Touch"], i, [s, l]
                    ],
                    [/android.+[;\/]\s*(NS-?\w{0,9})\sbuild/i],
                    [i, [o, "Insignia"],
                        [s, l]
                    ],
                    [/android.+[;\/]\s*((NX|Next)-?\w{0,9})\s+build/i],
                    [i, [o, "NextBook"],
                        [s, l]
                    ],
                    [/android.+[;\/]\s*(Xtreme\_)?(V(1[045]|2[015]|30|40|60|7[05]|90))\s+build/i],
                    [
                        [o, "Voice"], i, [s, r]
                    ],
                    [/android.+[;\/]\s*(LVTEL\-)?(V1[12])\s+build/i],
                    [
                        [o, "LvTel"], i, [s, r]
                    ],
                    [/android.+;\s(PH-1)\s/i],
                    [i, [o, "Essential"],
                        [s, r]
                    ],
                    [/android.+[;\/]\s*(V(100MD|700NA|7011|917G).*\b)\s+build/i],
                    [i, [o, "Envizen"],
                        [s, l]
                    ],
                    [/android.+[;\/]\s*(Le[\s\-]+Pan)[\s\-]+(\w{1,9})\s+build/i],
                    [o, i, [s, l]],
                    [/android.+[;\/]\s*(Trio[\s\-]*.*)\s+build/i],
                    [i, [o, "MachSpeed"],
                        [s, l]
                    ],
                    [/android.+[;\/]\s*(Trinity)[\-\s]*(T\d{3})\s+build/i],
                    [o, i, [s, l]],
                    [/android.+[;\/]\s*TU_(1491)\s+build/i],
                    [i, [o, "Rotor"],
                        [s, l]
                    ],
                    [/android.+(KS(.+))\s+build/i],
                    [i, [o, "Amazon"],
                        [s, l]
                    ],
                    [/android.+(Gigaset)[\s\-]+(Q\w{1,9})\s+build/i],
                    [o, i, [s, l]],
                    [/\s(tablet|tab)[;\/]/i, /\s(mobile)(?:[;\/]|\ssafari)/i],
                    [
                        [s, d.lowerize], o, i
                    ],
                    [/[\s\/\(](smart-?tv)[;\)]/i],
                    [
                        [s, "smarttv"]
                    ],
                    [/(android[\w\.\s\-]{0,9});.+build/i],
                    [i, [o, "Generic"]]
                ],
                engine: [
                    [/windows.+\sedge\/([\w\.]+)/i],
                    [a, [n, "EdgeHTML"]],
                    [/webkit\/537\.36.+chrome\/(?!27)/i],
                    [
                        [n, "Blink"]
                    ],
                    [/(presto)\/([\w\.]+)/i, /(webkit|trident|netfront|netsurf|amaya|lynx|w3m|goanna)\/([\w\.]+)/i, /(khtml|tasman|links)[\/\s]\(?([\w\.]+)/i, /(icab)[\/\s]([23]\.[\d\.]+)/i],
                    [n, a],
                    [/rv\:([\w\.]{1,9}).+(gecko)/i],
                    [a, n]
                ],
                os: [
                    [/microsoft\s(windows)\s(vista|xp)/i],
                    [n, a],
                    [/(windows)\snt\s6\.2;\s(arm)/i, /(windows\sphone(?:\sos)*)[\s\/]?([\d\.\s\w]*)/i, /(windows\smobile|windows)[\s\/]?([ntce\d\.\s]+\w)/i],
                    [n, [a, c.str, p.os.windows.version]],
                    [/(win(?=3|9|n)|win\s9x\s)([nt\d\.]+)/i],
                    [
                        [n, "Windows"],
                        [a, c.str, p.os.windows.version]
                    ],
                    [/\((bb)(10);/i],
                    [
                        [n, "BlackBerry"], a
                    ],
                    [/(blackberry)\w*\/?([\w\.]*)/i, /(tizen)[\/\s]([\w\.]+)/i, /(android|webos|palm\sos|qnx|bada|rim\stablet\sos|meego|sailfish|contiki)[\/\s-]?([\w\.]*)/i],
                    [n, a],
                    [/(symbian\s?os|symbos|s60(?=;))[\/\s-]?([\w\.]*)/i],
                    [
                        [n, "Symbian"], a
                    ],
                    [/\((series40);/i],
                    [n],
                    [/mozilla.+\(mobile;.+gecko.+firefox/i],
                    [
                        [n, "Firefox OS"], a
                    ],
                    [/(nintendo|playstation)\s([wids34portablevu]+)/i, /(mint)[\/\s\(]?(\w*)/i, /(mageia|vectorlinux)[;\s]/i, /(joli|[kxln]?ubuntu|debian|suse|opensuse|gentoo|(?=\s)arch|slackware|fedora|mandriva|centos|pclinuxos|redhat|zenwalk|linpus)[\/\s-]?(?!chrom)([\w\.-]*)/i, /(hurd|linux)\s?([\w\.]*)/i, /(gnu)\s?([\w\.]*)/i],
                    [n, a],
                    [/(cros)\s[\w]+\s([\w\.]+\w)/i],
                    [
                        [n, "Chromium OS"], a
                    ],
                    [/(sunos)\s?([\w\.\d]*)/i],
                    [
                        [n, "Solaris"], a
                    ],
                    [/\s([frentopc-]{0,4}bsd|dragonfly)\s?([\w\.]*)/i],
                    [n, a],
                    [/(haiku)\s(\w+)/i],
                    [n, a],
                    [/cfnetwork\/.+darwin/i, /ip[honead]{2,4}(?:.*os\s([\w]+)\slike\smac|;\sopera)/i],
                    [
                        [a, /_/g, "."],
                        [n, "iOS"]
                    ],
                    [/(mac\sos\sx)\s?([\w\s\.]*)/i, /(macintosh|mac(?=_powerpc)\s)/i],
                    [
                        [n, "Mac OS"],
                        [a, /_/g, "."]
                    ],
                    [/((?:open)?solaris)[\/\s-]?([\w\.]*)/i, /(aix)\s((\d)(?=\.|\)|\s)[\w\.])*/i, /(plan\s9|minix|beos|os\/2|amigaos|morphos|risc\sos|openvms|fuchsia)/i, /(unix)\s?([\w\.]*)/i],
                    [n, a]
                ]
            },
            h = function (t, i) {
                if ("object" == typeof t && (i = t, t = void 0), !(this instanceof h)) return new h(t, i).getResult();
                var n = t || (e && e.navigator && e.navigator.userAgent ? e.navigator.userAgent : ""),
                    s = i ? d.extend(u, i) : u;
                return this.getBrowser = function () {
                    var e = {
                        name: void 0,
                        version: void 0
                    };
                    return c.rgx.call(e, n, s.browser), e.major = d.major(e.version), e
                }, this.getCPU = function () {
                    var e = {
                        architecture: void 0
                    };
                    return c.rgx.call(e, n, s.cpu), e
                }, this.getDevice = function () {
                    var e = {
                        vendor: void 0,
                        model: void 0,
                        type: void 0
                    };
                    return c.rgx.call(e, n, s.device), e
                }, this.getEngine = function () {
                    var e = {
                        name: void 0,
                        version: void 0
                    };
                    return c.rgx.call(e, n, s.engine), e
                }, this.getOS = function () {
                    var e = {
                        name: void 0,
                        version: void 0
                    };
                    return c.rgx.call(e, n, s.os), e
                }, this.getResult = function () {
                    return {
                        ua: this.getUA(),
                        browser: this.getBrowser(),
                        engine: this.getEngine(),
                        os: this.getOS(),
                        device: this.getDevice(),
                        cpu: this.getCPU()
                    }
                }, this.getUA = function () {
                    return n
                }, this.setUA = function (e) {
                    return n = e, this
                }, this
            };
        h.VERSION = "0.7.20", h.BROWSER = {
            NAME: n,
            MAJOR: "major",
            VERSION: a
        }, h.CPU = {
            ARCHITECTURE: "architecture"
        }, h.DEVICE = {
            MODEL: i,
            VENDOR: o,
            TYPE: s,
            CONSOLE: "console",
            MOBILE: r,
            SMARTTV: "smarttv",
            TABLET: l,
            WEARABLE: "wearable",
            EMBEDDED: "embedded"
        }, h.ENGINE = {
            NAME: n,
            VERSION: a
        }, h.OS = {
            NAME: n,
            VERSION: a
        }, "undefined" != typeof exports ? ("undefined" != typeof module && module.exports && (exports = module.exports = h), exports.UAParser = h) : "function" == typeof define && define.amd ? define(function () {
            return h
        }) : e && (e.UAParser = h);
        var f = e && (e.jQuery || e.Zepto);
        if (void 0 !== f && !f.ua) {
            var v = new h;
            f.ua = v.getResult(), f.ua.get = function () {
                return v.getUA()
            }, f.ua.set = function (e) {
                v.setUA(e);
                var t = v.getResult();
                for (var i in t) f.ua[i] = t[i]
            }
        }
    }("object" == typeof window ? window : this),
    function () {
        "use strict";

        function e(n) {
            if (!n) throw new Error("No options passed to Waypoint constructor");
            if (!n.element) throw new Error("No element option passed to Waypoint constructor");
            if (!n.handler) throw new Error("No handler option passed to Waypoint constructor");
            this.key = "waypoint-" + t, this.options = e.Adapter.extend({}, e.defaults, n), this.element = this.options.element, this.adapter = new e.Adapter(this.element), this.callback = n.handler, this.axis = this.options.horizontal ? "horizontal" : "vertical", this.enabled = this.options.enabled, this.triggerPoint = null, this.group = e.Group.findOrCreate({
                name: this.options.group,
                axis: this.axis
            }), this.context = e.Context.findOrCreateByElement(this.options.context), e.offsetAliases[this.options.offset] && (this.options.offset = e.offsetAliases[this.options.offset]), this.group.add(this), this.context.add(this), i[this.key] = this, t += 1
        }
        var t = 0,
            i = {};
        e.prototype.queueTrigger = function (e) {
            this.group.queueTrigger(this, e)
        }, e.prototype.trigger = function (e) {
            this.enabled && this.callback && this.callback.apply(this, e)
        }, e.prototype.destroy = function () {
            this.context.remove(this), this.group.remove(this), delete i[this.key]
        }, e.prototype.disable = function () {
            return this.enabled = !1, this
        }, e.prototype.enable = function () {
            return this.context.refresh(), this.enabled = !0, this
        }, e.prototype.next = function () {
            return this.group.next(this)
        }, e.prototype.previous = function () {
            return this.group.previous(this)
        }, e.invokeAll = function (e) {
            var t = [];
            for (var n in i) t.push(i[n]);
            for (var s = 0, o = t.length; s < o; s++) t[s][e]()
        }, e.destroyAll = function () {
            e.invokeAll("destroy")
        }, e.disableAll = function () {
            e.invokeAll("disable")
        }, e.enableAll = function () {
            e.Context.refreshAll();
            for (var t in i) i[t].enabled = !0;
            return this
        }, e.refreshAll = function () {
            e.Context.refreshAll()
        }, e.viewportHeight = function () {
            return window.innerHeight || document.documentElement.clientHeight
        }, e.viewportWidth = function () {
            return document.documentElement.clientWidth
        }, e.adapters = [], e.defaults = {
            context: window,
            continuous: !0,
            enabled: !0,
            group: "default",
            horizontal: !1,
            offset: 0
        }, e.offsetAliases = {
            "bottom-in-view": function () {
                return this.context.innerHeight() - this.adapter.outerHeight()
            },
            "right-in-view": function () {
                return this.context.innerWidth() - this.adapter.outerWidth()
            }
        }, window.Waypoint = e
    }(),
    function () {
        "use strict";

        function e(e) {
            window.setTimeout(e, 1e3 / 60)
        }

        function t(e) {
            this.element = e, this.Adapter = s.Adapter, this.adapter = new this.Adapter(e), this.key = "waypoint-context-" + i, this.didScroll = !1, this.didResize = !1, this.oldScroll = {
                x: this.adapter.scrollLeft(),
                y: this.adapter.scrollTop()
            }, this.waypoints = {
                vertical: {},
                horizontal: {}
            }, e.waypointContextKey = this.key, n[e.waypointContextKey] = this, i += 1, s.windowContext || (s.windowContext = !0, s.windowContext = new t(window)), this.createThrottledScrollHandler(), this.createThrottledResizeHandler()
        }
        var i = 0,
            n = {},
            s = window.Waypoint,
            o = window.onload;
        t.prototype.add = function (e) {
            var t = e.options.horizontal ? "horizontal" : "vertical";
            this.waypoints[t][e.key] = e, this.refresh()
        }, t.prototype.checkEmpty = function () {
            var e = this.Adapter.isEmptyObject(this.waypoints.horizontal),
                t = this.Adapter.isEmptyObject(this.waypoints.vertical),
                i = this.element == this.element.window;
            e && t && !i && (this.adapter.off(".waypoints"), delete n[this.key])
        }, t.prototype.createThrottledResizeHandler = function () {
            function e() {
                t.handleResize(), t.didResize = !1
            }
            var t = this;
            this.adapter.on("resize.waypoints", function () {
                t.didResize || (t.didResize = !0, s.requestAnimationFrame(e))
            })
        }, t.prototype.createThrottledScrollHandler = function () {
            function e() {
                t.handleScroll(), t.didScroll = !1
            }
            var t = this;
            this.adapter.on("scroll.waypoints", function () {
                t.didScroll && !s.isTouch || (t.didScroll = !0, s.requestAnimationFrame(e))
            })
        }, t.prototype.handleResize = function () {
            s.Context.refreshAll()
        }, t.prototype.handleScroll = function () {
            var e = {},
                t = {
                    horizontal: {
                        newScroll: this.adapter.scrollLeft(),
                        oldScroll: this.oldScroll.x,
                        forward: "right",
                        backward: "left"
                    },
                    vertical: {
                        newScroll: this.adapter.scrollTop(),
                        oldScroll: this.oldScroll.y,
                        forward: "down",
                        backward: "up"
                    }
                };
            for (var i in t) {
                var n = t[i],
                    s = n.newScroll > n.oldScroll,
                    o = s ? n.forward : n.backward;
                for (var a in this.waypoints[i]) {
                    var r = this.waypoints[i][a];
                    if (null !== r.triggerPoint) {
                        var l = n.oldScroll < r.triggerPoint,
                            d = n.newScroll >= r.triggerPoint,
                            c = l && d,
                            p = !l && !d;
                        (c || p) && (r.queueTrigger(o), e[r.group.id] = r.group)
                    }
                }
            }
            for (var u in e) e[u].flushTriggers();
            this.oldScroll = {
                x: t.horizontal.newScroll,
                y: t.vertical.newScroll
            }
        }, t.prototype.innerHeight = function () {
            return this.element == this.element.window ? s.viewportHeight() : this.adapter.innerHeight()
        }, t.prototype.remove = function (e) {
            delete this.waypoints[e.axis][e.key], this.checkEmpty()
        }, t.prototype.innerWidth = function () {
            return this.element == this.element.window ? s.viewportWidth() : this.adapter.innerWidth()
        }, t.prototype.destroy = function () {
            var e = [];
            for (var t in this.waypoints)
                for (var i in this.waypoints[t]) e.push(this.waypoints[t][i]);
            for (var n = 0, s = e.length; n < s; n++) e[n].destroy()
        }, t.prototype.refresh = function () {
            var e, t = this.element == this.element.window,
                i = t ? void 0 : this.adapter.offset(),
                n = {};
            this.handleScroll(), e = {
                horizontal: {
                    contextOffset: t ? 0 : i.left,
                    contextScroll: t ? 0 : this.oldScroll.x,
                    contextDimension: this.innerWidth(),
                    oldScroll: this.oldScroll.x,
                    forward: "right",
                    backward: "left",
                    offsetProp: "left"
                },
                vertical: {
                    contextOffset: t ? 0 : i.top,
                    contextScroll: t ? 0 : this.oldScroll.y,
                    contextDimension: this.innerHeight(),
                    oldScroll: this.oldScroll.y,
                    forward: "down",
                    backward: "up",
                    offsetProp: "top"
                }
            };
            for (var o in e) {
                var a = e[o];
                for (var r in this.waypoints[o]) {
                    var l, d, c, p, u, h = this.waypoints[o][r],
                        f = h.options.offset,
                        v = h.triggerPoint,
                        m = 0,
                        g = null == v;
                    h.element !== h.element.window && (m = h.adapter.offset()[a.offsetProp]), "function" == typeof f ? f = f.apply(h) : "string" == typeof f && (f = parseFloat(f), h.options.offset.indexOf("%") > -1 && (f = Math.ceil(a.contextDimension * f / 100))), l = a.contextScroll - a.contextOffset, h.triggerPoint = Math.floor(m + l - f), d = v < a.oldScroll, c = h.triggerPoint >= a.oldScroll, p = d && c, u = !d && !c, !g && p ? (h.queueTrigger(a.backward), n[h.group.id] = h.group) : !g && u ? (h.queueTrigger(a.forward), n[h.group.id] = h.group) : g && a.oldScroll >= h.triggerPoint && (h.queueTrigger(a.forward), n[h.group.id] = h.group)
                }
            }
            return s.requestAnimationFrame(function () {
                for (var e in n) n[e].flushTriggers()
            }), this
        }, t.findOrCreateByElement = function (e) {
            return t.findByElement(e) || new t(e)
        }, t.refreshAll = function () {
            for (var e in n) n[e].refresh()
        }, t.findByElement = function (e) {
            return n[e.waypointContextKey]
        }, window.onload = function () {
            o && o(), t.refreshAll()
        }, s.requestAnimationFrame = function (t) {
            (window.requestAnimationFrame || window.mozRequestAnimationFrame || window.webkitRequestAnimationFrame || e).call(window, t)
        }, s.Context = t
    }(),
    function () {
        "use strict";

        function e(e, t) {
            return e.triggerPoint - t.triggerPoint
        }

        function t(e, t) {
            return t.triggerPoint - e.triggerPoint
        }

        function i(e) {
            this.name = e.name, this.axis = e.axis, this.id = this.name + "-" + this.axis, this.waypoints = [], this.clearTriggerQueues(), n[this.axis][this.name] = this
        }
        var n = {
                vertical: {},
                horizontal: {}
            },
            s = window.Waypoint;
        i.prototype.add = function (e) {
            this.waypoints.push(e)
        }, i.prototype.clearTriggerQueues = function () {
            this.triggerQueues = {
                up: [],
                down: [],
                left: [],
                right: []
            }
        }, i.prototype.flushTriggers = function () {
            for (var i in this.triggerQueues) {
                var n = this.triggerQueues[i],
                    s = "up" === i || "left" === i;
                n.sort(s ? t : e);
                for (var o = 0, a = n.length; o < a; o += 1) {
                    var r = n[o];
                    (r.options.continuous || o === n.length - 1) && r.trigger([i])
                }
            }
            this.clearTriggerQueues()
        }, i.prototype.next = function (t) {
            this.waypoints.sort(e);
            var i = s.Adapter.inArray(t, this.waypoints);
            return i === this.waypoints.length - 1 ? null : this.waypoints[i + 1]
        }, i.prototype.previous = function (t) {
            this.waypoints.sort(e);
            var i = s.Adapter.inArray(t, this.waypoints);
            return i ? this.waypoints[i - 1] : null
        }, i.prototype.queueTrigger = function (e, t) {
            this.triggerQueues[t].push(e)
        }, i.prototype.remove = function (e) {
            var t = s.Adapter.inArray(e, this.waypoints);
            t > -1 && this.waypoints.splice(t, 1)
        }, i.prototype.first = function () {
            return this.waypoints[0]
        }, i.prototype.last = function () {
            return this.waypoints[this.waypoints.length - 1]
        }, i.findOrCreate = function (e) {
            return n[e.axis][e.name] || new i(e)
        }, s.Group = i
    }(),
    function () {
        "use strict";

        function e(e) {
            this.$element = t(e)
        }
        var t = window.jQuery,
            i = window.Waypoint;
        t.each(["innerHeight", "innerWidth", "off", "offset", "on", "outerHeight", "outerWidth", "scrollLeft", "scrollTop"], function (t, i) {
            e.prototype[i] = function () {
                var e = Array.prototype.slice.call(arguments);
                return this.$element[i].apply(this.$element, e)
            }
        }), t.each(["extend", "inArray", "isEmptyObject"], function (i, n) {
            e[n] = t[n]
        }), i.adapters.push({
            name: "jquery",
            Adapter: e
        }), i.Adapter = e
    }(),
    function () {
        "use strict";

        function e(e) {
            return function () {
                var i = [],
                    n = arguments[0];
                return e.isFunction(arguments[0]) && (n = e.extend({}, arguments[1]), n.handler = arguments[0]), this.each(function () {
                    var s = e.extend({}, n, {
                        element: this
                    });
                    "string" == typeof s.context && (s.context = e(this).closest(s.context)[0]), i.push(new t(s))
                }), i
            }
        }
        var t = window.Waypoint;
        window.jQuery && (window.jQuery.fn.waypoint = e(window.jQuery)), window.Zepto && (window.Zepto.fn.waypoint = e(window.Zepto))
    }(), $(document).ready(function () {
        fancybox(), ObjectFit(), init_menu(), load_reveal(), checkStoryHeight()
    }), window.ADMIN_BAR_VISIBLE && (Barba.Pjax.preventCheck = function () {
        return !1
    });
var uaDetect = (new UAParser).getResult(),
    uaDetectDevice = (new UAParser).getDevice();
"IE" == uaDetect.browser.name && $("body").addClass("ie"), "Edge" == uaDetect.browser.name && $("body").addClass("edge"), "mobile" != uaDetectDevice.type && "tablet" != uaDetectDevice.type || $("body").addClass("touch-screen"), $(window).on("load", function () {
    $(".loader").fadeOut(400), setTimeout(function () {
        $("body").addClass("loaded")
    }, 400), setTimeout(function () {
        load_reveal()
    }, 600)
}), Barba.Pjax.start(), Barba.Prefetch.init();
var FadeTransition = Barba.BaseTransition.extend({
    start: function () {
        Promise.all([this.newContainerLoading, this.fadeOut()]).then(this.fadeIn.bind(this))
    },
    fadeOut: function () {
        return $("body").removeClass("menu-open"), $(".nav-icon").removeClass("open"), $("#barba-wrapper").addClass("page-unwrap-gradient"), setTimeout(function () {
            $("#barba-wrapper").addClass("active")
        }, 50), $(this.oldContainer).animate({
            opacity: 0
        }, 800).promise()
    },
    fadeIn: function () {
        $("html, body").animate({
            scrollTop: 0
        }, 0, "swing");
        var e = this,
            t = $(this.newContainer);
        $(this.oldContainer).hide(), t.css({
            visibility: "visible",
            opacity: 0
        }), setTimeout(function () {
            $("#barba-wrapper").addClass("animated"), $("#barba-wrapper").removeClass("active")
        }, 200), setTimeout(function () {
            $("#barba-wrapper").removeClass("animated"), $("#barba-wrapper").removeClass("page-unwrap-gradient")
        }, 700), t.addClass("fadeIn").animate({
            opacity: 1
        }, 600, function () {
            e.done()
        })
    }
});
Barba.Pjax.getTransition = function () {
    return FadeTransition
}, Barba.Dispatcher.on("transitionCompleted", function (e, t) {
    fancybox(), ObjectFit(), load_reveal(), checkStoryHeight(), DOM.LoadMethod(), $(window).trigger("resize")
}), Barba.Dispatcher.on("initStateChange", function () {
    window.ga && "localhost" != document.location.hostname && gtag("event", "page_view", {
        page_path: window.location.pathname
    })
}), FastClick.attach(document.body), DOM.Methods.exhibitionslider = function (e) {
    new Swiper(e, {
        slidesPerView: 4,
        slidesPerGroup: 4,
        spaceBetween: 15,
        watchSlidesVisibility: !0,
        speed: 1500,
        navigation: {
            nextEl: ".exhibition .right-arrow",
            prevEl: ".exhibition .left-arrow"
        },
        breakpoints: {
            1024: {
                slidesPerView: 3,
                slidesPerGroup: 3
            },
            768: {
                slidesPerView: 2,
                slidesPerGroup: 2
            },
            640: {
                slidesPerView: 1,
                slidesPerGroup: 1
            }
        }
    })
}, DOM.Methods.awardslider = function (e) {
    new Swiper(e, {
        slidesPerView: 4,
        slidesPerGroup: 4,
        spaceBetween: 15,
        watchSlidesVisibility: !0,
        speed: 1500,
        navigation: {
            nextEl: ".award .right-arrow",
            prevEl: ".award .left-arrow"
        },
        breakpoints: {
            1024: {
                slidesPerView: 3,
                slidesPerGroup: 3
            },
            768: {
                slidesPerView: 2,
                slidesPerGroup: 2
            },
            640: {
                slidesPerView: 1,
                slidesPerGroup: 1
            }
        }
    })
}, DOM.Methods.itemParallax1 = function (e) {
    var t = $(".layer-items1")[0];
    new Parallax(t)
}, DOM.Methods.itemParallax2 = function (e) {
    var t = $(".layer-items2")[0];
    new Parallax(t)
}, DOM.Methods.itemParallax3 = function (e) {
    var t = $(".layer-items3")[0];
    new Parallax(t)
}, DOM.Methods.itemParallax4 = function (e) {
    var t = $(".layer-items4")[0];
    new Parallax(t)
}, DOM.Methods.itemParallax5 = function (e) {
    var t = $(".layer-items5")[0];
    new Parallax(t)
}, DOM.Methods.itemParallax6 = function (e) {
    var t = $(".layer-items6")[0];
    new Parallax(t)
};
