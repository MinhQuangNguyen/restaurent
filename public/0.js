(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[0],{

/***/ "./resources/js/frontend/01f23.js?ver=5.3.6":
/*!**************************************************!*\
  !*** ./resources/js/frontend/01f23.js?ver=5.3.6 ***!
  \**************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function _createForOfIteratorHelper(o, allowArrayLike) { var it; if (typeof Symbol === "undefined" || o[Symbol.iterator] == null) { if (Array.isArray(o) || (it = _unsupportedIterableToArray(o)) || allowArrayLike && o && typeof o.length === "number") { if (it) o = it; var i = 0; var F = function F() {}; return { s: F, n: function n() { if (i >= o.length) return { done: true }; return { done: false, value: o[i++] }; }, e: function e(_e) { throw _e; }, f: F }; } throw new TypeError("Invalid attempt to iterate non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); } var normalCompletion = true, didErr = false, err; return { s: function s() { it = o[Symbol.iterator](); }, n: function n() { var step = it.next(); normalCompletion = step.done; return step; }, e: function e(_e2) { didErr = true; err = _e2; }, f: function f() { try { if (!normalCompletion && it["return"] != null) it["return"](); } finally { if (didErr) throw err; } } }; }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _classCallCheck2(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass2(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function isTouchDevice() {
  return 'ontouchstart' in window || 'onmsgesturechange' in window;
}

function isMobileDevice() {
  return typeof window.orientation !== "undefined" || navigator.userAgent.indexOf('IEMobile') !== -1;
}

;

jQuery.fn.isInViewport = function () {
  var elementTop = jQuery(this).offset().top;
  var elementBottom = elementTop + jQuery(this).outerHeight();
  var viewportTop = jQuery(window).scrollTop();
  var viewportBottom = viewportTop + jQuery(window).height();
  return elementBottom > viewportTop && elementTop < viewportBottom;
};

(function ($) {
  $(window).on('elementor/frontend/init', function () {
    var isTouch = ('ontouchstart' in document.documentElement);

    if (isTouch) {
      jQuery('#footer-wrapper').css('overflow-x', 'hidden');
    }

    jQuery("img.lazy").each(function () {
      var currentImg = jQuery(this);
      jQuery(this).Lazy({
        onFinishedAll: function onFinishedAll() {
          currentImg.parent("div.post-featured-image-hover").removeClass("lazy");
          currentImg.parent('.grandrestaurant_gallery_lightbox').parent("div.gallery-grid-item").removeClass("lazy");
        }
      });
    });
    jQuery('#page-content-wrapper .elementor-widget-image.animation').each(function () {
      jQuery(this).smoove({
        offset: '30%'
      });
    });
    var bodyBGColor = jQuery('body').css('background-color');

    if (bodyBGColor != '') {
      jQuery('#wrapper').css('background-color', bodyBGColor);
    }

    if (parseInt(jQuery(window).width()) < 501) {
      jQuery("section.elementor-section-height-min-height .elementor-container").each(function () {
        jQuery(this).height('auto');
      });
    }

    jQuery(window).resize(function () {
      jQuery("section.elementor-section-height-min-height .elementor-container").each(function () {
        var currentSection = jQuery(this);

        if (parseInt(jQuery(window).width()) < 501) {
          currentSection.height('auto');
        } else {
          currentSection.height('');
        }
      });
    });
    elementorFrontend.hooks.addAction('frontend/element_ready/global', function ($scope) {
      if (elementorFrontend.isEditMode()) {
        var elementSettings = {};
        var modelCID = $scope.data('model-cid');
        var settings = elementorFrontend.config.elements.data[modelCID];

        if (typeof settings != 'undefined') {
          var type = settings.attributes.widgetType || settings.attributes.elType,
              settingsKeys = elementorFrontend.config.elements.keys[type];

          if (!settingsKeys) {
            settingsKeys = elementorFrontend.config.elements.keys[type] = [];
            jQuery.each(settings.controls, function (name, control) {
              if (control.frontend_available) {
                settingsKeys.push(name);
              }
            });
          }

          jQuery.each(settings.getActiveControls(), function (controlKey) {
            if (-1 !== settingsKeys.indexOf(controlKey)) {
              elementSettings[controlKey] = settings.attributes[controlKey];
            }
          });
          var widgetExt = elementSettings;
        }
      } else {
        var widgetExtObj = $scope.attr('data-settings');

        if (typeof widgetExtObj != 'undefined') {
          var widgetExt = JSON.parse(widgetExtObj);
        }
      }

      if (typeof widgetExt != 'undefined') {
        if (typeof widgetExt.grandrestaurant_ext_is_background_parallax != 'undefined' && widgetExt.grandrestaurant_ext_is_background_parallax == 'true') {
          if (typeof widgetExt.background_background != 'undefined' && widgetExt.background_background == 'classic') {
            if (!elementorFrontend.isEditMode()) {
              var widgetBg = $scope.css('background-image');
              widgetBg = widgetBg.replace('url(', '').replace(')', '').replace(/\"/gi, "");

              if (widgetBg != '') {
                var jarallaxScrollSpeed = 0.5;

                if (typeof widgetExt.grandrestaurant_ext_is_background_parallax_speed.size != 'undefined') {
                  jarallaxScrollSpeed = parseFloat(widgetExt.grandrestaurant_ext_is_background_parallax_speed.size);
                }

                $scope.addClass('jarallax');
                $scope.append('<img class="jarallax-img" src="' + widgetBg + '"/>');
                $scope.jarallax({
                  speed: jarallaxScrollSpeed
                });

                if (!isMobileDevice()) {
                  $scope.css('background-image', 'none');
                }

                jQuery(window).resize(function () {
                  if (!isMobileDevice()) {
                    $scope.css('background-image', 'none');
                  } else {
                    $scope.css('background-image', 'url(' + widgetBg + ')');
                  }
                });
              }
            }
          }
        }

        if (typeof widgetExt.grandrestaurant_ext_is_fadeout_animation != 'undefined' && widgetExt.grandrestaurant_ext_is_fadeout_animation == 'true') {
          var scrollVelocity = parseFloat(widgetExt.grandrestaurant_ext_is_fadeout_animation_velocity.size);
          var scrollDirection = widgetExt.grandrestaurant_ext_is_fadeout_animation_direction;
          jQuery(window).scroll(function (i) {
            var scrollVar = jQuery(window).scrollTop();
            var scrollPx = -(scrollVelocity * scrollVar);

            if (scrollDirection == 'up') {
              scrollPx = -(scrollVelocity * scrollVar);
            } else if (scrollDirection == 'down') {
              scrollPx = scrollVelocity * scrollVar;
            } else {
              scrollPx = 0;
            }

            $scope.find('.elementor-widget-container').css({
              'transform': "translateY(" + scrollPx + "px)"
            });
            $scope.find('.elementor-widget-container').css({
              'opacity': (100 - scrollVar / 4) / 100
            });
          });
        }

        if (typeof widgetExt.grandrestaurant_ext_is_scrollme != 'undefined' && widgetExt.grandrestaurant_ext_is_scrollme == 'true') {
          var scrollArgs = {};

          if (typeof widgetExt.grandrestaurant_ext_scrollme_scalex.size != 'undefined' && widgetExt.grandrestaurant_ext_scrollme_scalex.size != 1) {
            scrollArgs['scaleX'] = widgetExt.grandrestaurant_ext_scrollme_scalex.size;
          }

          if (typeof widgetExt.grandrestaurant_ext_scrollme_scaley.size != 'undefined' && widgetExt.grandrestaurant_ext_scrollme_scaley.size != 1) {
            scrollArgs['scaleY'] = widgetExt.grandrestaurant_ext_scrollme_scaley.size;
          }

          if (typeof widgetExt.grandrestaurant_ext_scrollme_scalez.size != 'undefined' && widgetExt.grandrestaurant_ext_scrollme_scalez.size != 1) {
            scrollArgs['scaleZ'] = widgetExt.grandrestaurant_ext_scrollme_scalez.size;
          }

          if (typeof widgetExt.grandrestaurant_ext_scrollme_rotatex.size != 'undefined' && widgetExt.grandrestaurant_ext_scrollme_rotatex.size != 0) {
            scrollArgs['rotateX'] = widgetExt.grandrestaurant_ext_scrollme_rotatex.size;
          }

          if (typeof widgetExt.grandrestaurant_ext_scrollme_rotatey.size != 'undefined' && widgetExt.grandrestaurant_ext_scrollme_rotatey.size != 0) {
            scrollArgs['rotateY'] = widgetExt.grandrestaurant_ext_scrollme_rotatey.size;
          }

          if (typeof widgetExt.grandrestaurant_ext_scrollme_rotatez.size != 'undefined' && widgetExt.grandrestaurant_ext_scrollme_rotatez.size != 0) {
            scrollArgs['rotateY'] = widgetExt.grandrestaurant_ext_scrollme_rotatez.size;
          }

          if (typeof widgetExt.grandrestaurant_ext_scrollme_translatex.size != 'undefined' && widgetExt.grandrestaurant_ext_scrollme_translatex.size != 0) {
            scrollArgs['x'] = widgetExt.grandrestaurant_ext_scrollme_translatex.size;
          }

          if (typeof widgetExt.grandrestaurant_ext_scrollme_translatey.size != 'undefined' && widgetExt.grandrestaurant_ext_scrollme_translatey.size != 0) {
            scrollArgs['y'] = widgetExt.grandrestaurant_ext_scrollme_translatey.size;
          }

          if (typeof widgetExt.grandrestaurant_ext_scrollme_translatez.size != 'undefined' && widgetExt.grandrestaurant_ext_scrollme_translatez.size != 0) {
            scrollArgs['z'] = widgetExt.grandrestaurant_ext_scrollme_translatez.size;
          }

          if (typeof widgetExt.grandrestaurant_ext_scrollme_smoothness.size != 'undefined') {
            scrollArgs['smoothness'] = widgetExt.grandrestaurant_ext_scrollme_smoothness.size;
          }

          $scope.attr('data-parallax', JSON.stringify(scrollArgs));

          if (typeof widgetExt.grandrestaurant_ext_scrollme_disable != 'undefined') {
            if (widgetExt.grandrestaurant_ext_scrollme_disable == 'mobile') {
              if (parseInt(jQuery(window).width()) < 501) {
                $scope.addClass('noanimation');
              }
            }

            if (widgetExt.grandrestaurant_ext_scrollme_disable == 'tablet') {
              if (parseInt(jQuery(window).width()) < 769) {
                $scope.addClass('noanimation');
              }
            }

            jQuery(window).resize(function () {
              if (widgetExt.grandrestaurant_ext_scrollme_disable == 'mobile') {
                if (isMobileDevice() || parseInt(jQuery(window).width()) < 501) {
                  $scope.addClass('noanimation');
                } else {
                  $scope.removeClass('noanimation');
                }
              }

              if (widgetExt.grandrestaurant_ext_scrollme_disable == 'tablet') {
                if (parseInt(jQuery(window).width()) < 769) {
                  $scope.addClass('noanimation');
                } else {
                  $scope.removeClass('noanimation');
                }
              }
            });
          }
        }

        if (typeof widgetExt.grandrestaurant_ext_is_smoove != 'undefined' && widgetExt.grandrestaurant_ext_is_smoove == 'true') {
          $scope.addClass('init-smoove');
          $scope.smoove({
            min_width: parseInt(widgetExt.grandrestaurant_ext_smoove_disable),
            scaleX: widgetExt.grandrestaurant_ext_smoove_scalex.size,
            scaleY: widgetExt.grandrestaurant_ext_smoove_scaley.size,
            rotateX: parseInt(widgetExt.grandrestaurant_ext_smoove_rotatex.size) + 'deg',
            rotateY: parseInt(widgetExt.grandrestaurant_ext_smoove_rotatey.size) + 'deg',
            rotateZ: parseInt(widgetExt.grandrestaurant_ext_smoove_rotatez.size) + 'deg',
            moveX: parseInt(widgetExt.grandrestaurant_ext_smoove_translatex.size) + 'px',
            moveY: parseInt(widgetExt.grandrestaurant_ext_smoove_translatey.size) + 'px',
            moveZ: parseInt(widgetExt.grandrestaurant_ext_smoove_translatez.size) + 'px',
            skewX: parseInt(widgetExt.grandrestaurant_ext_smoove_skewx.size) + 'deg',
            skewY: parseInt(widgetExt.grandrestaurant_ext_smoove_skewy.size) + 'deg',
            perspective: parseInt(widgetExt.grandrestaurant_ext_smoove_perspective.size),
            offset: '-10%'
          });

          if (typeof widgetExt.grandrestaurant_ext_smoove_duration != 'undefined') {
            $scope.css('transition-duration', parseInt(widgetExt.grandrestaurant_ext_smoove_duration) + 'ms');
          }

          var width = jQuery(window).width();

          if (widgetExt.grandrestaurant_ext_smoove_disable >= width) {
            if (!$scope.hasClass('smooved')) {
              $scope.addClass('no-smooved');
            }

            return false;
          }
        }

        if (typeof widgetExt.grandrestaurant_ext_is_parallax_mouse != 'undefined' && widgetExt.grandrestaurant_ext_is_parallax_mouse == 'true') {
          var elementID = $scope.attr('data-id');
          $scope.find('.elementor-widget-container').attr('data-depth', parseFloat(widgetExt.grandrestaurant_ext_is_parallax_mouse_depth.size));
          $scope.attr('ID', 'parallax-' + elementID);
          var parentElement = document.getElementById('parallax-' + elementID);
          var parallax = new Parallax(parentElement, {
            relativeInput: true
          });

          if (elementorFrontend.isEditMode()) {
            if ($scope.width() == 0) {
              $scope.css('width', '100%');
            }

            if ($scope.height() == 0) {
              $scope.css('height', '100%');
            }
          }
        }

        if (typeof widgetExt.grandrestaurant_ext_is_infinite != 'undefined' && widgetExt.grandrestaurant_ext_is_infinite == 'true') {
          var animationClass = '';
          var keyframeName = '';
          var animationCSS = '';

          if (typeof widgetExt.grandrestaurant_ext_infinite_animation != 'undefined') {
            animationClass = widgetExt.grandrestaurant_ext_infinite_animation;

            switch (animationClass) {
              case 'if_swing1':
                keyframeName = 'swing';
                break;

              case 'if_swing2':
                keyframeName = 'swing2';
                break;

              case 'if_wave':
                keyframeName = 'wave';
                break;

              case 'if_tilt':
                keyframeName = 'tilt';
                break;

              case 'if_bounce':
                keyframeName = 'bounce';
                break;

              case 'if_scale':
                keyframeName = 'scale';
                break;

              case 'if_spin':
                keyframeName = 'spin';
                break;
            }

            animationCSS += keyframeName + ' ';
          }

          if (typeof widgetExt.grandrestaurant_ext_infinite_duration != 'undefined') {
            animationCSS += widgetExt.grandrestaurant_ext_infinite_duration + 's ';
          }

          animationCSS += 'infinite alternate ';

          if (typeof widgetExt.grandrestaurant_ext_infinite_easing != 'undefined') {
            animationCSS += 'cubic-bezier(' + widgetExt.grandrestaurant_ext_infinite_easing + ')';
          }

          $scope.css({
            'animation': animationCSS
          });
          $scope.addClass(animationClass);
        }

        if (typeof widgetExt.grandrestaurant_ext_is_background_on_scroll != 'undefined' && widgetExt.grandrestaurant_ext_is_background_on_scroll == 'true') {
          var bodyBackground = jQuery('body').css('background-color');
          var position = jQuery(window).scrollTop();
          jQuery(window).on("scroll touchmove", function () {
            clearTimeout($.data(this, 'scrollTimer'));
            $.data(this, 'scrollTimer', setTimeout(function () {
              jQuery('body').attr('data-scrollend', jQuery(window).scrollTop());
            }, 250));
            var scroll = jQuery(window).scrollTop();
            var position = jQuery('body').attr('data-scrollend');
            var windowHeight = jQuery(window).height();
            var windowHeightOffset = parseInt(windowHeight / 2);
            var elementTop = $scope.position().top - windowHeightOffset;
            var elementBottom = elementTop + $scope.outerHeight(true);

            if (scroll > position) {
              if (jQuery(document).scrollTop() >= elementTop && jQuery(document).scrollTop() <= elementBottom) {
                jQuery('#wrapper').css('background-color', widgetExt.grandrestaurant_ext_background_on_scroll_color);
                jQuery('.elementor-shape .elementor-shape-fill').css('fill', widgetExt.grandrestaurant_ext_background_on_scroll_color);
              }

              if (jQuery(document).scrollTop() > elementBottom) {
                jQuery('#wrapper').css('background-color', bodyBackground);
                jQuery('.elementor-shape .elementor-shape-fill').css('fill', bodyBackground);
              }
            } else {
              if (jQuery(document).scrollTop() <= elementBottom && jQuery(document).scrollTop() >= elementTop) {
                setTimeout(function () {
                  jQuery('#wrapper').css('background-color', widgetExt.grandrestaurant_ext_background_on_scroll_color).stop();
                  jQuery('.elementor-shape .elementor-shape-fill').css('fill', widgetExt.grandrestaurant_ext_background_on_scroll_color).stop();
                }, 100);
              }

              if (jQuery(document).scrollTop() < $scope.position().top) {
                jQuery('#wrapper').css('background-color', bodyBackground);
                jQuery('.elementor-shape .elementor-shape-fill').css('fill', bodyBackground);
              }
            }
          });
        }

        if (typeof widgetExt.grandrestaurant_ext_link_reservation != 'undefined' && widgetExt.grandrestaurant_ext_link_reservation == 'true') {
          $scope.on('click', function (e) {
            e.preventDefault();
            jQuery('#reservation_wrapper').fadeIn();
            jQuery('body').removeClass('js_nav');
            jQuery('body').addClass('overflow_hidden');
            jQuery('html').addClass('overflow_hidden');
          });
        }

        if (typeof widgetExt.grandrestaurant_ext_link_sidemenu != 'undefined' && widgetExt.grandrestaurant_ext_link_sidemenu == 'true') {
          $scope.on('click', function (e) {
            e.preventDefault();
            jQuery('body,html').animate({
              scrollTop: 0
            }, 100);
            jQuery('body').toggleClass('js_nav');
          });
        }
      }
    });
    elementorFrontend.hooks.addAction('frontend/element_ready/grandrestaurant-blog-posts.default', function ($scope) {
      jQuery(function ($) {
        jQuery("img.lazy").each(function () {
          var currentImg = jQuery(this);
          jQuery(this).Lazy({
            onFinishedAll: function onFinishedAll() {
              currentImg.parent("div.post-featured-image-hover").removeClass("lazy");
            }
          });
        });
        jQuery(".layout-masonry").each(function () {
          var grid = jQuery(this);
          grid.imagesLoaded().done(function () {
            grid.masonry({
              itemSelector: ".blog-posts-masonry",
              columnWidth: ".blog-posts-masonry",
              gutter: 45
            });
            jQuery(".layout-masonry .blog-posts-masonry").each(function (index) {
              setTimeout(function () {
                jQuery(".layout-masonry .blog-posts-masonry").eq(index).addClass("is-showing");
              }, 250 * index);
            });
          });
          jQuery(".layout-masonry img.lazy_masonry").each(function () {
            var currentImg = jQuery(this);
            currentImg.parent("div.post-featured-image-hover").removeClass("lazy");
            jQuery(this).Lazy({
              onFinishedAll: function onFinishedAll() {
                grid.masonry({
                  itemSelector: ".blog-posts-masonry",
                  columnWidth: ".blog-posts-masonry",
                  gutter: 45
                });
              }
            });
          });
        });
        jQuery(".layout-metro_masonry").each(function () {
          var grid = jQuery(this);
          grid.imagesLoaded().done(function () {
            grid.masonry({
              itemSelector: ".blog-posts-metro",
              columnWidth: ".blog-posts-metro",
              gutter: 40
            });
            jQuery(".layout-metro_masonry .blog-posts-metro").each(function (index) {
              setTimeout(function () {
                jQuery(".layout-metro_masonry .blog-posts-metro").eq(index).addClass("is-showing");
              }, 100 * index);
            });
          });
          jQuery(".post-metro-left-wrapper img.lazy_masonry, .layout-metro_masonry img.lazy_masonry").each(function () {
            var currentImg = jQuery(this);
            currentImg.parent("div.post-featured-image-hover").removeClass("lazy");
            jQuery(this).Lazy({
              onFinishedAll: function onFinishedAll() {
                grid.masonry({
                  itemSelector: ".blog-posts-metro",
                  columnWidth: ".blog-posts-metro",
                  gutter: 40
                });
              }
            });
          });
        });
        var menuLayout = jQuery('#pp_menu_layout').val();

        if (menuLayout != 'leftmenu') {
          jQuery(".post-metro-left-wrapper").stick_in_parent({
            offset_top: 120
          });
        } else {
          jQuery(".post-metro-left-wrapper").stick_in_parent({
            offset_top: 40
          });
        }

        if (jQuery(window).width() <= 768 || isTouchDevice()) {
          jQuery(".post-metro-left-wrapper").trigger("sticky_kit:detach");
        }
      });
    });
    elementorFrontend.hooks.addAction('frontend/element_ready/grandrestaurant-gallery-grid.default', function ($scope) {
      jQuery("img.lazy").each(function () {
        var currentImg = jQuery(this);
        jQuery(this).Lazy({
          onFinishedAll: function onFinishedAll() {
            currentImg.parent("div.post-featured-image-hover").removeClass("lazy");
            currentImg.parent('.grandrestaurant_gallery_lightbox').parent("div.gallery-grid-item").removeClass("lazy");
            currentImg.parent("div.gallery-grid-item").removeClass("lazy");
          }
        });
      });
    });
    elementorFrontend.hooks.addAction('frontend/element_ready/grandrestaurant-gallery-masonry.default', function ($scope) {
      jQuery(function ($) {
        jQuery(".grandrestaurant-gallery-grid-content-wrapper.do-masonry").each(function () {
          var grid = jQuery(this);
          var cols = grid.attr('data-cols');

          if (!grid.hasClass('has-no-space')) {
            var gutter = 30;

            if (cols > 4) {
              gutter = 20;
            }
          } else {
            gutter = 0;
          }

          grid.imagesLoaded().done(function () {
            grid.masonry({
              itemSelector: ".gallery-grid-item",
              columnWidth: ".gallery-grid-item",
              gutter: gutter
            });
            jQuery(".grandrestaurant-gallery-grid-content-wrapper.do-masonry .gallery-grid-item").each(function (index) {
              setTimeout(function () {
                jQuery(".do-masonry .gallery-grid-item").eq(index).addClass("is-showing");
              }, 100 * index);
            });
          });
          jQuery(".grandrestaurant-gallery-grid-content-wrapper.do-masonry img.lazy_masonry").each(function () {
            var currentImg = jQuery(this);
            currentImg.parent("div.post-featured-image-hover").removeClass("lazy");
            var cols = grid.attr('data-cols');

            if (!grid.hasClass('has-no-space')) {
              var gutter = 40;

              if (cols > 4) {
                gutter = 30;
              }
            } else {
              gutter = 0;
            }

            jQuery(this).Lazy({
              onFinishedAll: function onFinishedAll() {
                grid.masonry({
                  itemSelector: ".gallery-grid-item",
                  columnWidth: ".gallery-grid-item",
                  gutter: gutter
                });
              }
            });
          });
        });
      });
    });
    elementorFrontend.hooks.addAction('frontend/element_ready/grandrestaurant-gallery-justified.default', function ($scope) {
      jQuery(function ($) {
        jQuery("img.lazy").each(function () {
          var currentImg = jQuery(this);
          jQuery(this).Lazy({
            onFinishedAll: function onFinishedAll() {
              currentImg.parent("div.post-featured-image-hover").removeClass("lazy");
            }
          });
        });
        jQuery(".grandrestaurant-gallery-grid-content-wrapper.do-justified").each(function () {
          var grid = jQuery(this);
          var rowHeight = grid.attr('data-row_height');
          var margin = grid.attr('data-margin');
          var justifyLastRow = grid.attr('data-justify_last_row');
          var justifyLastRowStr = 'nojustify';

          if (justifyLastRow == 'yes') {
            justifyLastRowStr = 'justify';
          }

          grid.justifiedGallery({
            rowHeight: rowHeight,
            margins: margin,
            lastRow: justifyLastRowStr
          });
        });
      });
    });
    elementorFrontend.hooks.addAction('frontend/element_ready/grandrestaurant-gallery-fullscreen.default', function ($scope) {
      jQuery(function ($) {
        jQuery('body').addClass('elementor-fullscreen');
        var slideshow = jQuery('.fullscreen-gallery');
        var autoPlay = slideshow.attr('data-autoplay');
        var autoPlayArr = false;

        if (typeof autoPlay != "undefined") {
          autoPlayArr = {
            delay: autoPlay
          };
        }

        var effect = slideshow.attr('data-effect');

        if (typeof effect == "undefined") {
          effect = 'slide';
        }

        var speed = slideshow.attr('data-speed');

        if (typeof speed == "undefined") {
          speed = 400;
        }

        var galleryTop = new Swiper('.fullscreen-gallery', {
          navigation: {
            nextEl: '.swiper-button-next',
            prevEl: '.swiper-button-prev'
          },
          spaceBetween: 0,
          keyboardControl: true,
          speed: parseInt(speed),
          loop: true,
          effect: effect,
          grabCursor: true,
          preloadImages: false,
          lazy: {
            loadPrevNext: true
          },
          autoplay: autoPlayArr
        });
      });
    });
    elementorFrontend.hooks.addAction('frontend/element_ready/grandrestaurant-slider-vertical-parallax.default', function ($scope) {
      jQuery(function ($) {
        jQuery('body').addClass('elementor-fullscreen');
        var ticking = false;
        var isFirefox = /Firefox/i.test(navigator.userAgent);
        var isIe = /MSIE/i.test(navigator.userAgent) || /Trident.*rv\:11\./i.test(navigator.userAgent);
        var scrollSensitivitySetting = 30;
        var slideDurationSetting = 800;
        var currentSlideNumber = 0;
        var totalSlideNumber = jQuery('.parallax-slide-background').length;

        function parallaxScroll(evt) {
          if (isFirefox) {
            delta = evt.detail * -120;
          } else if (isIe) {
            delta = -evt.deltaY;
          } else {
            delta = evt.wheelDelta;
          }

          if (ticking != true) {
            if (delta <= -scrollSensitivitySetting) {
              ticking = true;

              if (currentSlideNumber !== totalSlideNumber - 1) {
                currentSlideNumber++;
                nextItem();
              }

              slideDurationTimeout(slideDurationSetting);
            }

            if (delta >= scrollSensitivitySetting) {
              ticking = true;

              if (currentSlideNumber !== 0) {
                currentSlideNumber--;
              }

              previousItem();
              slideDurationTimeout(slideDurationSetting);
            }
          }
        }

        function slideDurationTimeout(slideDuration) {
          setTimeout(function () {
            ticking = false;
          }, slideDuration);
        }

        var mousewheelEvent = isFirefox ? 'DOMMouseScroll' : 'wheel';
        window.addEventListener(mousewheelEvent, parallaxScroll, false);

        function nextItem() {
          var $previousSlide = jQuery('.parallax-slide-background').eq(currentSlideNumber - 1);
          $previousSlide.css('transform', 'translate3d(0,-130vh,0)').find('.parallax-slide-content-wrapper').css('transform', 'translateY(40vh)');
          currentSlideTransition();
        }

        function previousItem() {
          var $previousSlide = jQuery('.parallax-slide-background').eq(currentSlideNumber + 1);
          $previousSlide.css('transform', 'translate3d(0,30vh,0)').find('.parallax-slide-content-wrapper').css('transform', 'translateY(30vh)');
          currentSlideTransition();
        }

        function currentSlideTransition() {
          var $currentSlide = jQuery('.parallax-slide-background').eq(currentSlideNumber);
          $currentSlide.css('transform', 'translate3d(0,-15vh,0)').find('.parallax-slide-content-wrapper').css('transform', 'translateY(15vh)');
          ;
        }

        jQuery('body').on('touchmove', function (e) {
          e.preventDefault();
          e.stopPropagation();
          return false;
        });
        var ts;
        jQuery(document).bind('touchstart', function (e) {
          ts = e.originalEvent.touches[0].clientY;
        });
        jQuery(document).bind('touchend', function (e) {
          var te = e.originalEvent.changedTouches[0].clientY;

          if (ts > te + 5) {
            if (currentSlideNumber !== totalSlideNumber - 1) {
              currentSlideNumber++;
              nextItem();
            }

            slideDurationTimeout(slideDurationSetting);
          } else if (ts < te - 5) {
            if (currentSlideNumber !== 0) {
              currentSlideNumber--;
            }

            previousItem();
            slideDurationTimeout(slideDurationSetting);
          }
        });
      });
    });
    elementorFrontend.hooks.addAction('frontend/element_ready/grandrestaurant-gallery-horizontal.default', function ($scope) {
      jQuery('body').addClass('gallery-horizontal');
      jQuery(".horizontal-gallery-wrapper").each(function () {
        var $carousel = jQuery(this);
        var timer = $carousel.attr('data-autoplay');

        if (timer == 0) {
          timer = false;
        }

        var loop = $carousel.attr('data-loop');
        var navigation = $carousel.attr('data-navigation');

        if (navigation == 0) {
          navigation = false;
        }

        var pagination = $carousel.attr('data-pagination');

        if (pagination == 0) {
          pagination = false;
        }

        $carousel.flickity({
          percentPosition: false,
          imagesLoaded: false,
          selectedAttraction: 0.01,
          friction: 0.2,
          lazyLoad: 5,
          pauseAutoPlayOnHover: true,
          autoPlay: timer,
          contain: true,
          prevNextButtons: navigation,
          pageDots: pagination
        });
        var parallax = $carousel.attr('data-parallax');

        if (parallax == 1) {
          var $imgs = $carousel.find('.horizontal-gallery-cell img');
          var docStyle = document.documentElement.style;
          var transformProp = typeof docStyle.transform == 'string' ? 'transform' : 'WebkitTransform';
          var flkty = $carousel.data('flickity');
          $carousel.on('scroll.flickity', function () {
            flkty.slides.forEach(function (slide, i) {
              var img = $imgs[i];
              var x = (slide.target + flkty.x) * -1 / 3;
              img.style[transformProp] = 'translateX(' + x + 'px)';
            });
          });
        }

        var fullscreen = $carousel.attr('data-fullscreen');

        if (fullscreen != 0) {
          jQuery('body').addClass('elementor-fullscreen');
          var menuHeight = parseInt(jQuery('#wrapper').css('paddingTop'));
          var documentHeight = jQuery(window).innerHeight();
          var sliderHeight = parseInt(documentHeight - menuHeight);
          $carousel.find('.horizontal-gallery-cell').css('height', sliderHeight + 'px');
          $carousel.find('.horizontal-gallery-cell-img').css('height', sliderHeight + 'px');
          $carousel.flickity('resize');
          jQuery(window).resize(function () {
            var menuHeight = parseInt(jQuery('#wrapper').css('paddingTop'));
            var documentHeight = jQuery(window).innerHeight();
            var sliderHeight = parseInt(documentHeight - menuHeight);
            $carousel.find('.horizontal-gallery-cell').css('height', sliderHeight + 'px');
            $carousel.find('.horizontal-gallery-cell-img').css('height', sliderHeight + 'px');
            $carousel.flickity('resize');
          });
        }
      });
    });
    elementorFrontend.hooks.addAction('frontend/element_ready/grandrestaurant-slider-horizontal.default', function ($scope) {
      jQuery(".horizontal-slider-wrapper").each(function () {
        var $carousel = jQuery(this);
        var timer = $carousel.attr('data-autoplay');

        if (timer == 0) {
          timer = false;
        }

        var loop = $carousel.attr('data-loop');
        var navigation = $carousel.attr('data-navigation');

        if (navigation == 0) {
          navigation = false;
        }

        var pagination = $carousel.attr('data-pagination');

        if (pagination == 0) {
          pagination = false;
        }

        $carousel.flickity({
          percentPosition: false,
          imagesLoaded: true,
          pauseAutoPlayOnHover: true,
          autoPlay: timer,
          contain: true,
          prevNextButtons: navigation,
          pageDots: pagination
        });
        var fullscreen = $carousel.attr('data-fullscreen');

        if (fullscreen != 0) {
          jQuery('body').addClass('elementor-fullscreen');
          var menuHeight = parseInt(jQuery('#wrapper').css('paddingTop'));
          var documentHeight = jQuery(window).innerHeight();
          var sliderHeight = parseInt(documentHeight - menuHeight);
          $carousel.find('.horizontal-slider-cell').css('height', sliderHeight + 'px');
          $carousel.flickity('resize');
          jQuery(window).resize(function () {
            var menuHeight = parseInt(jQuery('#wrapper').css('paddingTop'));
            var documentHeight = jQuery(window).innerHeight();
            var sliderHeight = parseInt(documentHeight - menuHeight);
            $carousel.find('.horizontal-slider-cell').css('height', sliderHeight + 'px');
            $carousel.flickity('resize');
          });
        }
      });
    });
    elementorFrontend.hooks.addAction('frontend/element_ready/grandrestaurant-slider-animated-frame.default', function ($scope) {
      function debounce(func, wait, immediate) {
        var timeout;
        return function () {
          var context = this,
              args = arguments;

          var later = function later() {
            timeout = null;
            if (!immediate) func.apply(context, args);
          };

          var callNow = immediate && !timeout;
          clearTimeout(timeout);
          timeout = setTimeout(later, wait);
          if (callNow) func.apply(context, args);
        };
      }

      ;

      var Slideshow = /*#__PURE__*/function () {
        function Slideshow(el) {
          _classCallCheck2(this, Slideshow);

          this.DOM = {};
          this.DOM.el = el;
          this.settings = {
            animation: {
              slides: {
                duration: 600,
                easing: 'easeOutQuint'
              },
              shape: {
                duration: 300,
                easing: {
                  "in": 'easeOutQuint',
                  out: 'easeOutQuad'
                }
              }
            },
            frameFill: slideshowFrameColor
          };
          this.init();
        }

        _createClass2(Slideshow, [{
          key: "init",
          value: function init() {
            if (this.DOM.el) {
              this.DOM.slides = Array.from(this.DOM.el.querySelectorAll('.slides > .slide'));
              this.slidesTotal = this.DOM.slides.length;
              this.DOM.nav = this.DOM.el.querySelector('.slidenav');
              this.DOM.nextCtrl = this.DOM.nav.querySelector('.slidenav-item--next');
              this.DOM.prevCtrl = this.DOM.nav.querySelector('.slidenav-item--prev');
              this.current = 0;
              this.createFrame();
              this.initEvents();
            }
          }
        }, {
          key: "createFrame",
          value: function createFrame() {
            this.rect = this.DOM.el.getBoundingClientRect();
            this.frameSize = this.rect.width / 12;
            this.paths = {
              initial: this.calculatePath('initial'),
              "final": this.calculatePath('final')
            };
            this.DOM.svg = document.createElementNS('http://www.w3.org/2000/svg', 'svg');
            this.DOM.svg.setAttribute('class', 'shape');
            this.DOM.svg.setAttribute('width', '100%');
            this.DOM.svg.setAttribute('height', '100%');
            this.DOM.svg.setAttribute('viewbox', "0 0 ".concat(this.rect.width).concat(this.rect.height));
            this.DOM.svg.innerHTML = "<path fill=\"".concat(this.settings.frameFill, "\"d=\"").concat(this.paths.initial, "\"/>");
            this.DOM.el.insertBefore(this.DOM.svg, this.DOM.nav);
            this.DOM.shape = this.DOM.svg.querySelector('path');
          }
        }, {
          key: "updateFrame",
          value: function updateFrame() {
            this.paths.initial = this.calculatePath('initial');
            this.paths["final"] = this.calculatePath('final');
            this.DOM.svg.setAttribute('viewbox', "0 0 ".concat(this.rect.width).concat(this.rect.height));
            this.DOM.shape.setAttribute('d', this.isAnimating ? this.paths["final"] : this.paths.initial);
          }
        }, {
          key: "calculatePath",
          value: function calculatePath() {
            var path = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 'initial';
            return path === 'initial' ? "M 0,0 0,".concat(this.rect.height).concat(this.rect.width, ",").concat(this.rect.height).concat(this.rect.width, ",0 0,0 Z M 0,0 ").concat(this.rect.width, ",0 ").concat(this.rect.width, ",").concat(this.rect.height, "0,").concat(this.rect.height, "Z") : "M 0,0 0,".concat(this.rect.height).concat(this.rect.width, ",").concat(this.rect.height).concat(this.rect.width, ",0 0,0 Z M ").concat(this.frameSize, ",").concat(this.frameSize).concat(this.rect.width - this.frameSize, ",").concat(this.frameSize).concat(this.rect.width - this.frameSize, ",").concat(this.rect.height - this.frameSize).concat(this.frameSize, ",").concat(this.rect.height - this.frameSize, "Z");
          }
        }, {
          key: "initEvents",
          value: function initEvents() {
            var _this4 = this;

            this.DOM.nextCtrl.addEventListener('click', function () {
              return _this4.navigate('next');
            });
            this.DOM.prevCtrl.addEventListener('click', function () {
              return _this4.navigate('prev');
            });
            window.addEventListener('resize', debounce(function () {
              _this4.rect = _this4.DOM.el.getBoundingClientRect();

              _this4.updateFrame();
            }, 20));
            document.addEventListener('keydown', function (ev) {
              var keyCode = ev.keyCode || ev.which;

              if (keyCode === 37) {
                _this4.navigate('prev');
              } else if (keyCode === 39) {
                _this4.navigate('next');
              }
            });
          }
        }, {
          key: "navigate",
          value: function navigate() {
            var _this5 = this;

            var dir = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 'next';
            if (this.isAnimating) return false;
            this.isAnimating = true;
            var animateShapeIn = anime({
              targets: this.DOM.shape,
              duration: this.settings.animation.shape.duration,
              easing: this.settings.animation.shape.easing["in"],
              d: this.paths["final"]
            });

            var animateSlides = function animateSlides() {
              return new Promise(function (resolve, reject) {
                var currentSlide = _this5.DOM.slides[_this5.current];
                anime({
                  targets: currentSlide,
                  duration: _this5.settings.animation.slides.duration,
                  easing: _this5.settings.animation.slides.easing,
                  translateX: dir === 'next' ? -1 * _this5.rect.width : _this5.rect.width,
                  complete: function complete() {
                    currentSlide.classList.remove('slide-current');
                    resolve();
                  }
                });
                _this5.current = dir === 'next' ? _this5.current < _this5.slidesTotal - 1 ? _this5.current + 1 : 0 : _this5.current > 0 ? _this5.current - 1 : _this5.slidesTotal - 1;
                var newSlide = _this5.DOM.slides[_this5.current];
                newSlide.classList.add('slide-current');
                anime({
                  targets: newSlide,
                  duration: _this5.settings.animation.slides.duration,
                  easing: _this5.settings.animation.slides.easing,
                  translateX: [dir === 'next' ? _this5.rect.width : -1 * _this5.rect.width, 0]
                });
                var newSlideImg = newSlide.querySelector('.slide-img');
                anime.remove(newSlideImg);
                anime({
                  targets: newSlideImg,
                  duration: _this5.settings.animation.slides.duration * 4,
                  easing: _this5.settings.animation.slides.easing,
                  translateX: [dir === 'next' ? 200 : -200, 0]
                });
                anime({
                  targets: [newSlide.querySelector('.slide-title'), newSlide.querySelector('.slide-desc'), newSlide.querySelector('.slide-link')],
                  duration: _this5.settings.animation.slides.duration * 2,
                  easing: _this5.settings.animation.slides.easing,
                  delay: function delay(t, i) {
                    return i * 100 + 100;
                  },
                  translateX: [dir === 'next' ? 300 : -300, 0],
                  opacity: [0, 1]
                });
              });
            };

            var animateShapeOut = function animateShapeOut() {
              anime({
                targets: _this5.DOM.shape,
                duration: _this5.settings.animation.shape.duration,
                delay: 150,
                easing: _this5.settings.animation.shape.easing.out,
                d: _this5.paths.initial,
                complete: function complete() {
                  return _this5.isAnimating = false;
                }
              });
            };

            animateShapeIn.finished.then(animateSlides).then(animateShapeOut);
          }
        }]);

        return Slideshow;
      }();

      ;
      var slideshow = document.querySelector('.slideshow');

      if (slideshow) {
        var slideshowFrameColor = slideshow.getAttribute('data-background');
        new Slideshow(slideshow);
        imagesLoaded('.slide-img', {
          background: true
        });
      }
    });
    elementorFrontend.hooks.addAction('frontend/element_ready/grandrestaurant-slider-room.default', function ($scope) {
      jQuery('body').addClass('elementor-fullscreen');
      jQuery('body').addClass('room');

      function debounce(func, wait, immediate) {
        var timeout;
        return function () {
          var context = this,
              args = arguments;

          var later = function later() {
            timeout = null;
            if (!immediate) func.apply(context, args);
          };

          var callNow = immediate && !timeout;
          clearTimeout(timeout);
          timeout = setTimeout(later, wait);
          if (callNow) func.apply(context, args);
        };
      }

      ;

      function getMousePos(e) {
        var posx = 0;
        var posy = 0;
        if (!e) var e = window.event;

        if (e.pageX || e.pageY) {
          posx = e.pageX;
          posy = e.pageY;
        } else if (e.clientX || e.clientY) {
          posx = e.clientX + document.body.scrollLeft + document.documentElement.scrollLeft;
          posy = e.clientY + document.body.scrollTop + document.documentElement.scrollTop;
        }

        return {
          x: posx,
          y: posy
        };
      }

      var DOM = {};
      DOM.loader = document.querySelector('.room-slider-wrapper .overlay--loader');
      DOM.scroller = document.querySelector('.room-slider-wrapper .container > .scroller');

      if (DOM.scroller) {
        DOM.rooms = [].slice.call(DOM.scroller.querySelectorAll('.room'));
      } else {
        DOM.rooms = {};
      }

      DOM.content = document.querySelector('.room-slider-wrapper .content');

      if (DOM.content) {
        DOM.nav = {
          leftCtrl: DOM.content.querySelector('.room-slider-wrapper nav > .btn--nav-left'),
          rightCtrl: DOM.content.querySelector('.room-slider-wrapper nav > .btn--nav-right')
        };
        DOM.slides = [].slice.call(DOM.content.querySelectorAll('.slides > .slide'));
      }

      var currentRoom = 0,
          totalRooms = DOM.rooms.length,
          initTransform = {
        translateX: 0,
        translateY: 0,
        translateZ: '500px',
        rotateX: 0,
        rotateY: 0,
        rotateZ: 0
      },
          resetTransform = {
        translateX: 0,
        translateY: 0,
        translateZ: 0,
        rotateX: 0,
        rotateY: 0,
        rotateZ: 0
      },
          menuTransform = {
        translateX: 0,
        translateY: '150%',
        translateZ: 0,
        rotateX: '15deg',
        rotateY: 0,
        rotateZ: 0
      },
          menuTransform = {
        translateX: 0,
        translateY: '50%',
        translateZ: 0,
        rotateX: '-10deg',
        rotateY: 0,
        rotateZ: 0
      },
          infoTransform = {
        translateX: 0,
        translateY: 0,
        translateZ: '200px',
        rotateX: '2deg',
        rotateY: 0,
        rotateZ: '4deg'
      },
          initTransition = {
        speed: '0.9s',
        easing: 'ease'
      },
          roomTransition = {
        speed: '0.4s',
        easing: 'ease'
      },
          menuTransition = {
        speed: '1.5s',
        easing: 'cubic-bezier(0.2,1,0.3,1)'
      },
          infoTransition = {
        speed: '15s',
        easing: 'cubic-bezier(0.3,1,0.3,1)'
      },
          tiltTransition = {
        speed: '0.2s',
        easing: 'ease-out'
      },
          tilt = false,
          tiltRotation = {
        rotateX: 1,
        rotateY: -3
      },
          onEndTransition = function onEndTransition(el, callback) {
        var onEndCallbackFn = function onEndCallbackFn(ev) {
          this.removeEventListener('transitionend', onEndCallbackFn);

          if (callback && typeof callback === 'function') {
            callback.call();
          }
        };

        el.addEventListener('transitionend', onEndCallbackFn);
      },
          win = {
        width: window.innerWidth,
        height: window.innerHeight
      },
          isMoving,
          isNavigating;

      function init() {
        move({
          transition: initTransition,
          transform: initTransform
        }).then(function () {
          initTilt();
        });
        showSlide(100);
        initEvents();
      }

      function initTilt() {
        applyRoomTransition(tiltTransition);
        tilt = true;
      }

      function removeTilt() {
        tilt = false;
      }

      function move(opts) {
        return new Promise(function (resolve, reject) {
          if (isMoving && !opts.stopTransition) {
            return false;
          }

          isMoving = true;

          if (opts.transition) {
            applyRoomTransition(opts.transition);
          }

          if (opts.transform) {
            applyRoomTransform(opts.transform);

            var onEndFn = function onEndFn() {
              isMoving = false;
              resolve();
            };

            onEndTransition(DOM.scroller, onEndFn);
          } else {
            resolve();
          }
        });
      }

      function initEvents() {
        var onMouseMoveFn = function onMouseMoveFn(ev) {
          requestAnimationFrame(function () {
            if (!tilt) return false;
            var mousepos = getMousePos(ev),
                rotX = tiltRotation.rotateX ? initTransform.rotateX - (2 * tiltRotation.rotateX / win.height * mousepos.y - tiltRotation.rotateX) : 0,
                rotY = tiltRotation.rotateY ? initTransform.rotateY - (2 * tiltRotation.rotateY / win.width * mousepos.x - tiltRotation.rotateY) : 0;
            applyRoomTransform({
              'translateX': initTransform.translateX,
              'translateY': initTransform.translateY,
              'translateZ': initTransform.translateZ,
              'rotateX': rotX + 'deg',
              'rotateY': rotY + 'deg',
              'rotateZ': initTransform.rotateZ
            });
          });
        },
            debounceResizeFn = debounce(function () {
          win = {
            width: window.innerWidth,
            height: window.innerHeight
          };
        }, 10);

        document.addEventListener('mousemove', onMouseMoveFn);
        window.addEventListener('resize', debounceResizeFn);

        var onNavigatePrevFn = function onNavigatePrevFn() {
          navigate('prev');
        },
            onNavigateNextFn = function onNavigateNextFn() {
          navigate('next');
        };

        if (DOM.nav.leftCtrl && DOM.nav.rightCtrl) {
          DOM.nav.leftCtrl.addEventListener('click', onNavigatePrevFn);
          DOM.nav.rightCtrl.addEventListener('click', onNavigateNextFn);
        }
      }

      function applyRoomTransform(transform) {
        DOM.scroller.style.transform = 'translate3d(' + transform.translateX + ', ' + transform.translateY + ', ' + transform.translateZ + ') ' + 'rotate3d(1,0,0,' + transform.rotateX + ') rotate3d(0,1,0,' + transform.rotateY + ') rotate3d(0,0,1,' + transform.rotateZ + ')';
      }

      function applyRoomTransition(transition) {
        DOM.scroller.style.transition = transition === 'none' ? transition : 'transform ' + transition.speed + ' ' + transition.easing;
      }

      function toggleSlide(dir, _delay) {
        var slide = DOM.slides[currentRoom],
            name = slide.querySelector('.slide-name'),
            title = slide.querySelector('.slide-title'),
            date = slide.querySelector('.slide-date');
        _delay = _delay !== undefined ? _delay : 0;
        anime.remove([name, title, date]);
        var animeOpts = {
          targets: [name, title, date],
          duration: dir === 'in' ? 400 : 400,
          delay: function delay(t, i, c) {
            return _delay + 75 + i * 75;
          },
          easing: [0.25, 0.1, 0.25, 1],
          opacity: {
            value: dir === 'in' ? [0, 1] : [1, 0],
            duration: dir === 'in' ? 550 : 250
          },
          translateY: function translateY(t, i) {
            return dir === 'in' ? [150, 0] : [0, -150];
          }
        };

        if (dir === 'in') {
          animeOpts.begin = function () {
            slide.classList.add('slide-current');
          };
        } else {
          animeOpts.complete = function () {
            slide.classList.remove('slide-current');
          };
        }

        anime(animeOpts);
      }

      function showSlide(delay) {
        toggleSlide('in', delay);
      }

      function hideSlide(delay) {
        toggleSlide('out', delay);
      }

      function navigate(dir) {
        if (isMoving || isNavigating) {
          return false;
        }

        isNavigating = true;
        var room = DOM.rooms[currentRoom];
        removeTilt();
        hideSlide();

        if (dir === 'next') {
          currentRoom = currentRoom < totalRooms - 1 ? currentRoom + 1 : 0;
        } else {
          currentRoom = currentRoom > 0 ? currentRoom - 1 : totalRooms - 1;
        }

        var nextRoom = DOM.rooms[currentRoom];
        nextRoom.style.transform = 'translate3d(' + (dir === 'next' ? 100 : -100) + '%,0,0) translate3d(' + (dir === 'next' ? 1 : -1) + 'px,0,0)';
        nextRoom.style.opacity = 1;
        move({
          transition: roomTransition,
          transform: resetTransform
        }).then(function () {
          return move({
            transform: {
              translateX: (dir === 'next' ? -100 : 100) + '%',
              translateY: 0,
              translateZ: 0,
              rotateX: 0,
              rotateY: 0,
              rotateZ: 0
            }
          });
        }).then(function () {
          nextRoom.classList.add('room--current');
          room.classList.remove('room--current');
          room.style.opacity = 0;
          showSlide();
          return move({
            transform: {
              translateX: (dir === 'next' ? -100 : 100) + '%',
              translateY: 0,
              translateZ: '500px',
              rotateX: 0,
              rotateY: 0,
              rotateZ: 0
            }
          });
        }).then(function () {
          applyRoomTransition('none');
          nextRoom.style.transform = 'translate3d(0,0,0)';
          applyRoomTransform(initTransform);
          setTimeout(function () {
            initTilt();
          }, 60);
          isNavigating = false;
        });
      }

      function addAdjacentRooms() {
        var room = DOM.rooms[currentRoom],
            nextRoom = DOM.rooms[currentRoom < totalRooms - 1 ? currentRoom + 1 : 0],
            prevRoom = DOM.rooms[currentRoom > 0 ? currentRoom - 1 : totalRooms - 1];
        nextRoom.style.transform = 'translate3d(100%,0,0) translate3d(3px,0,0)';
        nextRoom.style.opacity = 1;
        prevRoom.style.transform = 'translate3d(-100%,0,0) translate3d(-3px,0,0)';
        prevRoom.style.opacity = 1;
      }

      function removeAdjacentRooms() {
        var room = DOM.rooms[currentRoom],
            nextRoom = DOM.rooms[currentRoom < totalRooms - 1 ? currentRoom + 1 : 0],
            prevRoom = DOM.rooms[currentRoom > 0 ? currentRoom - 1 : totalRooms - 1];
        nextRoom.style.transform = 'none';
        nextRoom.style.opacity = 0;
        prevRoom.style.transform = 'none';
        prevRoom.style.opacity = 0;
      }

      if (DOM.scroller) {
        imagesLoaded(DOM.scroller, function () {
          var extradelay = 1000;
          anime({
            targets: DOM.loader,
            duration: 600,
            easing: 'easeInOutCubic',
            delay: extradelay,
            translateY: '-100%',
            begin: function begin() {
              init();
            },
            complete: function complete() {
              DOM.loader.classList.remove('overlay--active');
            }
          });
        });
      }
    });
    elementorFrontend.hooks.addAction('frontend/element_ready/grandrestaurant-slider-multi-layouts.default', function ($scope) {
      document.documentElement.className = 'js';
      var slideshow = new MLSlideshow(document.querySelector('.slideshow'));

      if (document.querySelector('#next-slide')) {
        document.querySelector('#next-slide').addEventListener('click', function () {
          slideshow.next();
        });
      }

      if (document.querySelector('#prev-slide')) {
        document.querySelector('#prev-slide').addEventListener('click', function () {
          slideshow.prev();
        });
      }
    });
    elementorFrontend.hooks.addAction('frontend/element_ready/grandrestaurant-slider-velo.default', function ($scope) {
      jQuery('body').addClass('elementor-fullscreen');
      var scaleDownAmnt = 0.7;
      var boxShadowAmnt = '40px';
      $.Velocity.RegisterEffect("translateUp", {
        defaultDuration: 1,
        calls: [[{
          translateY: '-100%'
        }, 1]]
      });
      $.Velocity.RegisterEffect("translateDown", {
        defaultDuration: 1,
        calls: [[{
          translateY: '100%'
        }, 1]]
      });
      $.Velocity.RegisterEffect("translateNone", {
        defaultDuration: 1,
        calls: [[{
          translateY: '0',
          opacity: '1',
          scale: '1'
        }, 1]]
      });
      $.Velocity.RegisterEffect("scaleDown", {
        defaultDuration: 1,
        calls: [[{
          opacity: '0',
          scale: '0.7'
        }, 1]]
      });
      $.Velocity.RegisterEffect("scaleDown.moveUp", {
        defaultDuration: 1,
        calls: [[{
          translateY: '0%',
          scale: scaleDownAmnt
        }, 0.20], [{
          translateY: '-100%'
        }, 0.60], [{
          translateY: '-100%',
          scale: '1'
        }, 0.20]]
      });
      $.Velocity.RegisterEffect("scaleDown.moveUp.scroll", {
        defaultDuration: 1,
        calls: [[{
          translateY: '-100%',
          scale: scaleDownAmnt
        }, 0.60], [{
          translateY: '-100%',
          scale: '1'
        }, 0.40]]
      });
      $.Velocity.RegisterEffect("scaleUp.moveUp", {
        defaultDuration: 1,
        calls: [[{
          translateY: '90%',
          scale: scaleDownAmnt
        }, 0.20], [{
          translateY: '0%'
        }, 0.60], [{
          translateY: '0%',
          scale: '1'
        }, 0.20]]
      });
      $.Velocity.RegisterEffect("scaleUp.moveUp.scroll", {
        defaultDuration: 1,
        calls: [[{
          translateY: '0%',
          scale: scaleDownAmnt
        }, 0.60], [{
          translateY: '0%',
          scale: '1'
        }, 0.40]]
      });
      $.Velocity.RegisterEffect("scaleDown.moveDown", {
        defaultDuration: 1,
        calls: [[{
          translateY: '0%',
          scale: scaleDownAmnt
        }, 0.20], [{
          translateY: '100%'
        }, 0.60], [{
          translateY: '100%',
          scale: '1'
        }, 0.20]]
      });
      $.Velocity.RegisterEffect("scaleDown.moveDown.scroll", {
        defaultDuration: 1,
        calls: [[{}, 0.60], [{
          translateY: '100%',
          scale: '1'
        }, 0.40]]
      });
      $.Velocity.RegisterEffect("scaleUp.moveDown", {
        defaultDuration: 1,
        calls: [[{
          translateY: '-90%',
          scale: scaleDownAmnt
        }, 0.20], [{
          translateY: '0%'
        }, 0.60], [{
          translateY: '0%',
          scale: '1'
        }, 0.20]]
      });

      var VeloSlider = function () {
        var settings = {
          veloInit: $('.velo-slides').data('velo-slider'),
          $veloSlide: $('.velo-slide'),
          veloSlideBg: '.velo-slide-bg',
          navPrev: $('.velo-slides-nav').find('a.js-velo-slides-prev'),
          navNext: $('.velo-slides-nav').find('a.js-velo-slides-next'),
          veloBtn: $('.velo-slide-btn'),
          delta: 0,
          scrollThreshold: 7,
          currentSlide: 1,
          animating: false,
          animationDuration: 2000
        };
        var delta = 0,
            animating = false;
        return {
          init: function init() {
            this.bind();
          },
          bind: function bind() {
            settings.$veloSlide.first().addClass('is-active');

            if (settings.veloInit == 'on') {
              VeloSlider.initScrollJack();
              $(window).on('DOMMouseScroll mousewheel', VeloSlider.scrollJacking);
            }

            settings.navPrev.on('click', VeloSlider.prevSlide);
            settings.navNext.on('click', VeloSlider.nextSlide);
            $(document).on('keydown', function (e) {
              var keyNext = e.which == 39 || e.which == 40,
                  keyPrev = e.which == 37 || e.which == 38;

              if (keyNext && !settings.navNext.hasClass('inactive')) {
                e.preventDefault();
                VeloSlider.nextSlide();
              } else if (keyPrev && !settings.navPrev.hasClass('inactive')) {
                e.preventDefault();
                VeloSlider.prevSlide();
              }
            });
            jQuery('body').on('touchmove', function (e) {
              e.preventDefault();
              e.stopPropagation();
              return false;
            });
            var ts;
            jQuery(document).bind('touchstart', function (e) {
              ts = e.originalEvent.touches[0].clientY;
            });
            jQuery(document).bind('touchend', function (e) {
              var te = e.originalEvent.changedTouches[0].clientY;

              if (ts > te + 5) {
                VeloSlider.nextSlide();
              } else if (ts < te - 5) {
                VeloSlider.prevSlide();
              }
            });
            VeloSlider.checkNavigation();
            VeloSlider.hoverAnimation();
          },
          hoverAnimation: function hoverAnimation() {
            settings.veloBtn.hover(function () {
              $(this).closest(settings.$veloSlide).toggleClass('is-hovering');
            });
          },
          setAnimation: function setAnimation(midStep, direction) {
            var animationVisible = 'translateNone',
                animationTop = 'translateUp',
                animationBottom = 'translateDown',
                easing = 'ease',
                animDuration = settings.animationDuration;

            if (midStep) {
              animationVisible = 'scaleUp.moveUp.scroll';
              animationTop = 'scaleDown.moveUp.scroll';
              animationBottom = 'scaleDown.moveDown.scroll';
            } else {
              animationVisible = direction == 'next' ? 'scaleUp.moveUp' : 'scaleUp.moveDown';
              animationTop = 'scaleDown.moveUp';
              animationBottom = 'scaleDown.moveDown';
            }

            return [animationVisible, animationTop, animationBottom, animDuration, easing];
          },
          initScrollJack: function initScrollJack() {
            var visibleSlide = settings.$veloSlide.filter('.is-active'),
                topSection = visibleSlide.prevAll(settings.$veloSlide),
                bottomSection = visibleSlide.nextAll(settings.$veloSlide),
                animationParams = VeloSlider.setAnimation(false),
                animationVisible = animationParams[0],
                animationTop = animationParams[1],
                animationBottom = animationParams[2];
            visibleSlide.children('div').velocity(animationVisible, 1, function () {
              visibleSlide.css('opacity', 1);
              topSection.css('opacity', 1);
              bottomSection.css('opacity', 1);
            });
            topSection.children('div').velocity(animationTop, 0);
            bottomSection.children('div').velocity(animationBottom, 0);
          },
          scrollJacking: function scrollJacking(e) {
            if (!jQuery('body').hasClass('js_nav')) {
              if (e.originalEvent.detail < 0 || e.originalEvent.wheelDelta > 0) {
                delta--;
                Math.abs(delta) >= settings.scrollThreshold && VeloSlider.prevSlide();
              } else {
                delta++;
                delta >= settings.scrollThreshold && VeloSlider.nextSlide();
              }

              return false;
            }
          },
          prevSlide: function prevSlide(e) {
            typeof e !== 'undefined' && e.preventDefault();
            var visibleSlide = settings.$veloSlide.filter('.is-active'),
                animationParams = VeloSlider.setAnimation(midStep, 'prev'),
                midStep = false;
            visibleSlide = midStep ? visibleSlide.next(settings.$veloSlide) : visibleSlide;

            if (!animating && !visibleSlide.is(":first-child")) {
              animating = true;
              visibleSlide.removeClass('is-active').children(settings.veloSlideBg).velocity(animationParams[2], animationParams[3], animationParams[4]).end().prev(settings.$veloSlide).addClass('is-active').children(settings.veloSlideBg).velocity(animationParams[0], animationParams[3], animationParams[4], function () {
                animating = false;
              });
              currentSlide = settings.currentSlide - 1;
            }

            VeloSlider.resetScroll();
          },
          nextSlide: function nextSlide(e) {
            typeof e !== 'undefined' && e.preventDefault();
            var visibleSlide = settings.$veloSlide.filter('.is-active'),
                animationParams = VeloSlider.setAnimation(midStep, 'next'),
                midStep = false;

            if (!animating && !visibleSlide.is(":last-of-type")) {
              animating = true;
              visibleSlide.removeClass('is-active').children(settings.veloSlideBg).velocity(animationParams[1], animationParams[3]).end().next(settings.$veloSlide).addClass('is-active').children(settings.veloSlideBg).velocity(animationParams[0], animationParams[3], function () {
                animating = false;
              });
              currentSlide = settings.currentSlide + 1;
            }

            VeloSlider.resetScroll();
          },
          resetScroll: function resetScroll() {
            delta = 0;
            VeloSlider.checkNavigation();
          },
          checkNavigation: function checkNavigation() {
            settings.$veloSlide.filter('.is-active').is(':first-of-type') ? settings.navPrev.addClass('inactive') : settings.navPrev.removeClass('inactive');
            settings.$veloSlide.filter('.is-active').is(':last-of-type') ? settings.navNext.addClass('inactive') : settings.navNext.removeClass('inactive');
          }
        };
      }();

      VeloSlider.init();
    });
    elementorFrontend.hooks.addAction('frontend/element_ready/grandrestaurant-slider-popout.default', function ($scope) {
      jQuery('body').addClass('elementor-fullscreen');

      function goToSlide(number) {
        $('.slider-slide').removeClass('slider-slide--active');
        $('.slider-slide[data-slide=' + number + ']').addClass('slider-slide--active');
      }

      $('.slider__next, .go-to-next').on('click', function () {
        var currentSlide = Number($('.slider-slide--active').data('slide'));
        var totalSlides = $('.slider-slide').length;
        currentSlide++;

        if (currentSlide > totalSlides) {
          currentSlide = 1;
        }

        goToSlide(currentSlide);
      });

      for (var i = 1; i <= $('.slider-slide').length; i++) {
        $('.slider__indicators').append('<div class="slider__indicator" data-slide="' + i + '"></div>');
      }

      setTimeout(function () {
        $('.slider-wrap').addClass('slider-wrap--hacked');
      }, 1000);
    });
    elementorFrontend.hooks.addAction('frontend/element_ready/grandrestaurant-slider-clip-path.default', function ($scope) {
      jQuery('body').addClass('elementor-fullscreen');

      (function () {
        var $slides = document.querySelectorAll('.slide');
        var $controls = document.querySelectorAll('.slider-control');
        var numOfSlides = $slides.length;
        var slidingAT = 1300;
        var slidingBlocked = false;
        [].slice.call($slides).forEach(function ($el, index) {
          var i = index + 1;
          $el.classList.add('slide-' + i);
          $el.dataset.slide = i;
        });
        [].slice.call($controls).forEach(function ($el) {
          $el.addEventListener('click', controlClickHandler);
        });

        function controlClickHandler() {
          if (slidingBlocked) return;
          slidingBlocked = true;
          var $control = this;
          var isRight = $control.classList.contains('m--right');
          var $curActive = document.querySelector('.slide.s--active');
          var index = +$curActive.dataset.slide;
          isRight ? index++ : index--;
          if (index < 1) index = numOfSlides;
          if (index > numOfSlides) index = 1;
          var $newActive = document.querySelector('.slide-' + index);
          $control.classList.add('a--rotation');
          $curActive.classList.remove('s--active', 's--active-prev');
          document.querySelector('.slide.s--prev').classList.remove('s--prev');
          $newActive.classList.add('s--active');
          if (!isRight) $newActive.classList.add('s--active-prev');
          var prevIndex = index - 1;
          if (prevIndex < 1) prevIndex = numOfSlides;
          document.querySelector('.slide-' + prevIndex).classList.add('s--prev');
          setTimeout(function () {
            $control.classList.remove('a--rotation');
            slidingBlocked = false;
          }, slidingAT * 0.75);
        }

        ;
      })();
    });
    elementorFrontend.hooks.addAction('frontend/element_ready/grandrestaurant-gallery-preview.default', function ($scope) {
      jQuery('body').addClass('elementor-fullscreen');
      jQuery('body').addClass('grandrestaurant-gallery-preview');
      var $slider = $('.slider');
      var $slickTrack = $('.slick-track');
      var $slickCurrent = $('.slick-current');
      var slideDuration = 900;
      $slider.on('init', function (slick) {
        TweenMax.to($('.slick-track'), 0.9, {
          marginLeft: 0
        });
        TweenMax.to($('.slick-active'), 0.9, {
          x: 0,
          zIndex: 2
        });
      });
      $slider.on('beforeChange', function (event, slick, currentSlide, nextSlide) {
        TweenMax.to($('.slick-track'), 0.9, {
          marginLeft: 0
        });
        TweenMax.to($('.slick-active'), 0.9, {
          x: 0
        });
      });
      $slider.on('afterChange', function (event, slick, currentSlide) {
        TweenMax.to($('.slick-track'), 0.9, {
          marginLeft: 0
        });
        $('.slick-slide').css('z-index', '1');
        TweenMax.to($('.slick-active'), 0.9, {
          x: 0,
          zIndex: 2
        });
      });
      var $sliderPagination = $('.slider').attr('data-pagination');
      var dotsVar = false;

      if ($sliderPagination == 'yes') {
        dotsVar = true;
      }

      var $sliderNavigation = $('.slider').attr('data-navigation');
      var arrowsVar = false;

      if ($sliderNavigation == 'yes') {
        arrowsVar = true;
      }

      var $sliderAutoPlay = $('.slider').attr('data-autoplay');
      var autoPlayVar = false;
      var autoPlayTimeVar = 0;

      if (typeof $sliderAutoPlay != "undefined") {
        autoPlayVar = true;
        autoPlayTimeVar = $sliderAutoPlay;
      }

      $slider.slick({
        speed: slideDuration,
        touchMove: true,
        dots: dotsVar,
        arrows: arrowsVar,
        waitForAnimate: true,
        useTransform: true,
        autoplay: autoPlayVar,
        autoplaySpeed: autoPlayTimeVar,
        cssEase: 'cubic-bezier(0.455, 0.030, 0.130, 1.000)'
      });
      $('.slick-prev').on('mouseenter', function () {
        TweenMax.to($('.slick-track'), 0.6, {
          marginLeft: "180px",
          ease: Quad.easeOut
        });
        TweenMax.to($('.slick-current'), 0.6, {
          x: -100,
          ease: Quad.easeOut
        });
      });
      $('.slick-prev').on('mouseleave', function () {
        TweenMax.to($('.slick-track'), 0.4, {
          marginLeft: 0,
          ease: Sine.easeInOut
        });
        TweenMax.to($('.slick-current'), 0.4, {
          x: 0,
          ease: Sine.easeInOut
        });
      });
      $('.slick-next').on('mouseenter', function () {
        TweenMax.to($('.slick-track'), 0.6, {
          marginLeft: "-180px",
          ease: Quad.easeOut
        });
        TweenMax.to($('.slick-current'), 0.6, {
          x: 100,
          ease: Quad.easeOut
        });
      });
      $('.slick-next').on('mouseleave', function () {
        TweenMax.to($('.slick-track'), 0.4, {
          marginLeft: 0,
          ease: Quad.easeInOut
        });
        TweenMax.to($('.slick-current'), 0.4, {
          x: 0,
          ease: Quad.easeInOut
        });
      });
    });
    elementorFrontend.hooks.addAction('frontend/element_ready/grandrestaurant-slider-split-slick.default', function ($scope) {
      jQuery('body').addClass('elementor-fullscreen');
      var $slider = $('.slideshow .slider'),
          maxItems = $('.item', $slider).length,
          dragging = false,
          tracking,
          rightTracking;
      $sliderRight = $('.slideshow').clone().addClass('slideshow-right').appendTo($('.split-slideshow'));
      rightItems = $('.item', $sliderRight).toArray();
      reverseItems = rightItems.reverse();
      $('.slider', $sliderRight).html('');

      for (i = 0; i < maxItems; i++) {
        $(reverseItems[i]).appendTo($('.slider', $sliderRight));
      }

      $slider.addClass('slideshow-left');
      $('.slideshow-left').slick({
        vertical: true,
        verticalSwiping: true,
        arrows: false,
        infinite: true,
        dots: true,
        speed: 1000,
        cssEase: 'cubic-bezier(0.7, 0, 0.3, 1)'
      }).on('beforeChange', function (event, slick, currentSlide, nextSlide) {
        if (currentSlide > nextSlide && nextSlide == 0 && currentSlide == maxItems - 1) {
          $('.slideshow-right .slider').slick('slickGoTo', -1);
          $('.slideshow-text').slick('slickGoTo', maxItems);
        } else if (currentSlide < nextSlide && currentSlide == 0 && nextSlide == maxItems - 1) {
          $('.slideshow-right .slider').slick('slickGoTo', maxItems);
          $('.slideshow-text').slick('slickGoTo', -1);
        } else {
          $('.slideshow-right .slider').slick('slickGoTo', maxItems - 1 - nextSlide);
          $('.slideshow-text').slick('slickGoTo', nextSlide);
        }
      }).on("mousewheel", function (event) {
        event.preventDefault();

        if (event.deltaX > 0 || event.deltaY < 0) {
          $(this).slick('slickNext');
        } else if (event.deltaX < 0 || event.deltaY > 0) {
          $(this).slick('slickPrev');
        }

        ;
      }).on('mousedown touchstart', function () {
        dragging = true;
        tracking = $('.slick-track', $slider).css('transform');
        tracking = parseInt(tracking.split(',')[5]);
        rightTracking = $('.slideshow-right .slick-track').css('transform');
        rightTracking = parseInt(rightTracking.split(',')[5]);
      }).on('mousemove touchmove', function () {
        if (dragging) {
          newTracking = $('.slideshow-left .slick-track').css('transform');
          newTracking = parseInt(newTracking.split(',')[5]);
          diffTracking = newTracking - tracking;
          $('.slideshow-right .slick-track').css({
            'transform': 'matrix(1, 0, 0, 1, 0, ' + (rightTracking - diffTracking) + ')'
          });
        }
      }).on('mouseleave touchend mouseup', function () {
        dragging = false;
      });
      $('.slideshow-right .slider').slick({
        swipe: false,
        vertical: true,
        arrows: false,
        infinite: true,
        speed: 950,
        cssEase: 'cubic-bezier(0.7, 0, 0.3, 1)',
        initialSlide: maxItems - 1
      });
      $('.slideshow-text').slick({
        swipe: false,
        vertical: true,
        arrows: false,
        infinite: true,
        speed: 900,
        cssEase: 'cubic-bezier(0.7, 0, 0.3, 1)'
      });
    });
    elementorFrontend.hooks.addAction('frontend/element_ready/grandrestaurant-slider-transitions.default', function ($scope) {
      jQuery('body').addClass('elementor-fullscreen');
      var mySwiper = new Swiper(".swiper-container", {
        direction: "vertical",
        loop: true,
        pagination: {
          el: '.swiper-pagination',
          type: 'bullets',
          clickable: true
        },
        keyboard: {
          enabled: true,
          onlyInViewport: false
        },
        grabCursor: true,
        speed: 1000,
        paginationClickable: true,
        parallax: true,
        autoplay: false,
        effect: "slide",
        mousewheel: {
          invert: false
        }
      });
    });
    elementorFrontend.hooks.addAction('frontend/element_ready/grandrestaurant-slider-property-clip.default', function ($scope) {
      jQuery(".slider-property-clip-wrapper").each(function () {
        var slider = jQuery(this).find(".slider"),
            slides = slider.find('li'),
            nav = slider.find('nav');
        slides.eq(0).addClass('current');
        nav.children('a').eq(0).addClass('current-dot');
        nav.on('click', 'a', function (event) {
          event.preventDefault();
          $(this).addClass('current-dot').siblings().removeClass('current-dot');
          slides.eq($(this).index()).addClass('current').removeClass('prev').siblings().removeClass('current');
          slides.eq($(this).index()).prevAll().addClass('prev');
          slides.eq($(this).index()).nextAll().removeClass('prev');
        });
      });
    });
    elementorFrontend.hooks.addAction('frontend/element_ready/grandrestaurant-slider-slice.default', function ($scope) {
      jQuery(".slice-slide-container").each(function () {
        var slide = jQuery('.slide');
        var navPrev = jQuery('.js-prev');
        var navNext = jQuery('.js-next');
        var SliceSlider = {
          settings: {
            delta: 0,
            currentSlideIndex: 0,
            scrollThreshold: 40,
            slides: slide,
            numSlides: slide.length,
            navPrev: navPrev,
            navNext: navNext
          },
          init: function init() {
            s = this.settings;
            this.bindEvents();
          },
          bindEvents: function bindEvents() {
            s.navPrev.on({
              'click': SliceSlider.prevSlide
            });
            s.navNext.on({
              'click': SliceSlider.nextSlide
            });
            $(document).keyup(function (e) {
              if (e.which === 37 || e.which === 38) {
                SliceSlider.prevSlide();
              }

              if (e.which === 39 || e.which === 40) {
                SliceSlider.nextSlide();
              }
            });
          },
          handleScroll: function handleScroll(e) {
            if (e.originalEvent.detail < 0 || e.originalEvent.wheelDelta > 0) {
              s.delta--;

              if (Math.abs(s.delta) >= s.scrollThreshold) {
                SliceSlider.prevSlide();
              }
            } else {
              s.delta++;

              if (s.delta >= s.scrollThreshold) {
                SliceSlider.nextSlide();
              }
            }

            return false;
          },
          showSlide: function showSlide() {
            s.delta = 0;

            if ($('body').hasClass('is-sliding')) {
              return;
            }

            s.slides.each(function (i, slide) {
              $(slide).toggleClass('is-active', i === s.currentSlideIndex);
              $(slide).toggleClass('is-prev', i === s.currentSlideIndex - 1);
              $(slide).toggleClass('is-next', i === s.currentSlideIndex + 1);
              $('body').addClass('is-sliding');
              setTimeout(function () {
                $('body').removeClass('is-sliding');
              }, 1000);
            });
          },
          prevSlide: function prevSlide() {
            if (s.currentSlideIndex <= 0) {
              s.currentSlideIndex = s.numSlides;
            }

            s.currentSlideIndex--;
            SliceSlider.showSlide();
          },
          nextSlide: function nextSlide() {
            s.currentSlideIndex++;

            if (s.currentSlideIndex >= s.numSlides) {
              s.currentSlideIndex = 0;
            }

            SliceSlider.showSlide();
          }
        };
        SliceSlider.init();
      });
    });
    elementorFrontend.hooks.addAction('frontend/element_ready/grandrestaurant-slider-flip.default', function ($scope) {
      jQuery('body').addClass('elementor-fullscreen');
      jQuery('body').addClass('elementor-overflow');

      var Gallery = function () {
        var scrollTimeId;
        var posLeft = 0;

        function Gallery(config) {
          this.list = $(config.list);
          this.items = this.list.find('li');
          this.itemWidth = this.items.outerWidth();
        }

        ;
        Gallery.prototype = {
          constructor: Gallery,
          init: function init() {
            this.setGalleryWidth();
            this.eventManager();
            return this;
          },
          eventManager: function eventManager() {
            var _this = this;

            $("html, body").on('mousewheel', function (event) {
              clearTimeout(scrollTimeId);
              scrollTimeId = setTimeout(onScrollEventHandler.bind(this, event, _this.itemWidth), 0);
            });
          },
          setGalleryWidth: function setGalleryWidth() {
            this.list.css('width', this.getGalleryWidth());
            this.list.css('overflow', 'scroll');
          },
          getGalleryWidth: function getGalleryWidth() {
            var width = 0;
            this.items.each(function (index, item) {
              width += $(this).outerWidth();
            });
            return width;
          }
        };

        function onScrollEventHandler(event, width) {
          if (event.deltaY > 0) {
            this.scrollLeft -= width / 20;
          } else {
            this.scrollLeft += width / 20;
          }

          event.preventDefault();
        }

        ;
        return Gallery;
      }();

      $(document).ready(function () {
        var gallery = new Gallery({
          list: '.flip-slide-container .container .gallery'
        }).init();
        jQuery('.flip-slide-container').css('overflow', 'scroll');
        jQuery('body').css('overflow-x', 'scroll');
      });
    });
    elementorFrontend.hooks.addAction('frontend/element_ready/grandrestaurant-slider-split-carousel.default', function ($scope) {
      jQuery(".split-carousel-slider-wrapper").each(function () {
        var $carousel = jQuery(this);
        var fullscreen = $carousel.attr('data-fullscreen');

        if (fullscreen != 0) {
          jQuery('body').addClass('elementor-fullscreen');
          var menuHeight = parseInt(jQuery('#wrapper').css('paddingTop'));
          var documentHeight = jQuery(window).innerHeight();
          var sliderHeight = parseInt(documentHeight - menuHeight);
          $carousel.css('height', sliderHeight + 'px');
          jQuery(window).resize(function () {
            var menuHeight = parseInt(jQuery('#wrapper').css('paddingTop'));
            var documentHeight = jQuery(window).innerHeight();
            var sliderHeight = parseInt(documentHeight - menuHeight);
            $carousel.css('height', sliderHeight + 'px');
          });
        }

        var activeIndex = 0;
        var limit = 0;
        var disabled = false;
        var $stage = void 0;
        var $controls = void 0;
        var canvas = false;
        var SPIN_FORWARD_CLASS = 'js-spin-fwd';
        var SPIN_BACKWARD_CLASS = 'js-spin-bwd';
        var DISABLE_TRANSITIONS_CLASS = 'js-transitions-disabled';
        var SPIN_DUR = 1000;

        var appendControls = function appendControls() {
          for (var i = 0; i < limit; i++) {
            $('.carousel-control').append('<a href="#" data-index="' + i + '"></a>');
          }

          var height = $('.carousel-control').children().last().outerHeight();
          $('.carousel-control').css('height', 30 + limit * height);
          $controls = $('.carousel-control').children();
          $controls.eq(activeIndex).addClass('active');
        };

        var setIndexes = function setIndexes() {
          $('.spinner').children().each(function (i, el) {
            $(el).attr('data-index', i);
            limit++;
          });
        };

        var duplicateSpinner = function duplicateSpinner() {
          var $el = $('.spinner').parent();
          var html = $('.spinner').parent().html();
          $el.append(html);
          $('.spinner').last().addClass('spinner--right');
          $('.spinner--right').removeClass('spinner--left');
        };

        var paintFaces = function paintFaces() {
          $('.spinner-face').each(function (i, el) {
            var $el = $(el);
            var color = $(el).attr('data-bg');
            $el.children().css('backgroundImage', 'url(' + getBase64PixelByColor(color) + ')');
          });
        };

        var getBase64PixelByColor = function getBase64PixelByColor(hex) {
          if (!canvas) {
            canvas = document.createElement('canvas');
            canvas.height = 1;
            canvas.width = 1;
          }

          if (canvas.getContext) {
            var ctx = canvas.getContext('2d');
            ctx.fillStyle = hex;
            ctx.fillRect(0, 0, 1, 1);
            return canvas.toDataURL();
          }

          return false;
        };

        var prepareDom = function prepareDom() {
          setIndexes();
          paintFaces();
          duplicateSpinner();
          appendControls();
        };

        var spin = function spin() {
          var inc = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 1;
          if (disabled) return;
          if (!inc) return;
          activeIndex += inc;
          disabled = true;

          if (activeIndex >= limit) {
            activeIndex = 0;
          }

          if (activeIndex < 0) {
            activeIndex = limit - 1;
          }

          var $activeEls = $('.spinner-face.js-active');
          var $nextEls = $('.spinner-face[data-index=' + activeIndex + ']');
          $nextEls.addClass('js-next');

          if (inc > 0) {
            $stage.addClass(SPIN_FORWARD_CLASS);
          } else {
            $stage.addClass(SPIN_BACKWARD_CLASS);
          }

          $controls.removeClass('active');
          $controls.eq(activeIndex).addClass('active');
          setTimeout(function () {
            spinCallback(inc);
          }, SPIN_DUR, inc);
        };

        var spinCallback = function spinCallback(inc) {
          $('.js-active').removeClass('js-active');
          $('.js-next').removeClass('js-next').addClass('js-active');
          $stage.addClass(DISABLE_TRANSITIONS_CLASS).removeClass(SPIN_FORWARD_CLASS).removeClass(SPIN_BACKWARD_CLASS);
          $('.js-active').each(function (i, el) {
            var $el = $(el);
            $el.prependTo($el.parent());
          });
          setTimeout(function () {
            $stage.removeClass(DISABLE_TRANSITIONS_CLASS);
            disabled = false;
          }, 100);
        };

        var attachListeners = function attachListeners() {
          document.onkeyup = function (e) {
            switch (e.keyCode) {
              case 38:
                spin(-1);
                break;

              case 40:
                spin(1);
                break;
            }
          };

          $carousel.bind('DOMMouseScroll', function (e) {
            if (e.originalEvent.detail > 0) {
              spin(1);
            } else {
              spin(-1);
            }

            return false;
          });
          $carousel.bind('mousewheel', function (e) {
            if (e.originalEvent.wheelDelta < 0) {
              spin(1);
            } else {
              spin(-1);
            }

            return false;
          });
          jQuery('body').on('touchmove', function (e) {
            e.preventDefault();
            e.stopPropagation();
            return false;
          });
          var ts;
          $carousel.bind('touchstart', function (e) {
            ts = e.originalEvent.touches[0].clientY;
          });
          $carousel.bind('touchend', function (e) {
            var te = e.originalEvent.changedTouches[0].clientY;

            if (ts > te + 5) {
              spin(1);
            } else if (ts < te - 5) {
              spin(-1);
            }
          });
          $controls.on('click', function (e) {
            e.preventDefault();
            if (disabled) return;
            var $el = $(e.target);
            var toIndex = parseInt($el.attr('data-index'), 10);
            spin(toIndex - activeIndex);
          });
        };

        var assignEls = function assignEls() {
          $stage = $('.carousel-stage');
        };

        var init = function init() {
          assignEls();
          prepareDom();
          attachListeners();
        };

        $(function () {
          init();
        });
      });
    });
    elementorFrontend.hooks.addAction('frontend/element_ready/grandrestaurant-horizontal-timeline.default', function ($scope) {
      var timelines = jQuery('.cd-horizontal-timeline');
      var eventsMinDistance = timelines.attr('data-spacing');

      if (eventsMinDistance == '') {
        eventsMinDistance = 60;
      }

      timelines.length > 0 && initTimeline(timelines);

      function initTimeline(timelines) {
        timelines.each(function () {
          var timeline = jQuery(this),
              timelineComponents = {};
          timelineComponents['timelineWrapper'] = timeline.find('.events-wrapper');
          timelineComponents['eventsWrapper'] = timelineComponents['timelineWrapper'].children('.events');
          timelineComponents['fillingLine'] = timelineComponents['eventsWrapper'].children('.filling-line');
          timelineComponents['timelineEvents'] = timelineComponents['eventsWrapper'].find('a');
          timelineComponents['timelineDates'] = parseDate(timelineComponents['timelineEvents']);
          timelineComponents['eventsMinLapse'] = minLapse(timelineComponents['timelineDates']);
          timelineComponents['timelineNavigation'] = timeline.find('.cd-timeline-navigation');
          timelineComponents['eventsContent'] = timeline.children('.events-content');
          setDatePosition(timelineComponents, eventsMinDistance);
          var timelineTotWidth = setTimelineWidth(timelineComponents, eventsMinDistance);
          timeline.addClass('loaded');
          timelineComponents['timelineNavigation'].on('click', '.next', function (event) {
            event.preventDefault();
            updateSlide(timelineComponents, timelineTotWidth, 'next');
          });
          timelineComponents['timelineNavigation'].on('click', '.prev', function (event) {
            event.preventDefault();
            updateSlide(timelineComponents, timelineTotWidth, 'prev');
          });
          timelineComponents['eventsWrapper'].on('click', 'a', function (event) {
            event.preventDefault();
            timelineComponents['timelineEvents'].removeClass('selected');
            jQuery(this).addClass('selected');
            updateOlderEvents(jQuery(this));
            updateFilling(jQuery(this), timelineComponents['fillingLine'], timelineTotWidth);
            updateVisibleContent(jQuery(this), timelineComponents['eventsContent']);
          });
          timelineComponents['eventsContent'].on('swipeleft', function () {
            var mq = checkMQ();
            mq == 'mobile' && showNewContent(timelineComponents, timelineTotWidth, 'next');
          });
          timelineComponents['eventsContent'].on('swiperight', function () {
            var mq = checkMQ();
            mq == 'mobile' && showNewContent(timelineComponents, timelineTotWidth, 'prev');
          });
          jQuery(document).keyup(function (event) {
            if (event.which == '37' && elementInViewport(timeline.get(0))) {
              showNewContent(timelineComponents, timelineTotWidth, 'prev');
            } else if (event.which == '39' && elementInViewport(timeline.get(0))) {
              showNewContent(timelineComponents, timelineTotWidth, 'next');
            }
          });
        });
      }

      function updateSlide(timelineComponents, timelineTotWidth, string) {
        var translateValue = getTranslateValue(timelineComponents['eventsWrapper']),
            wrapperWidth = Number(timelineComponents['timelineWrapper'].css('width').replace('px', ''));
        string == 'next' ? translateTimeline(timelineComponents, translateValue - wrapperWidth + eventsMinDistance, wrapperWidth - timelineTotWidth) : translateTimeline(timelineComponents, translateValue + wrapperWidth - eventsMinDistance);
      }

      function showNewContent(timelineComponents, timelineTotWidth, string) {
        var visibleContent = timelineComponents['eventsContent'].find('.selected'),
            newContent = string == 'next' ? visibleContent.next() : visibleContent.prev();

        if (newContent.length > 0) {
          var selectedDate = timelineComponents['eventsWrapper'].find('.selected'),
              newEvent = string == 'next' ? selectedDate.parent('li').next('li').children('a') : selectedDate.parent('li').prev('li').children('a');
          updateFilling(newEvent, timelineComponents['fillingLine'], timelineTotWidth);
          updateVisibleContent(newEvent, timelineComponents['eventsContent']);
          newEvent.addClass('selected');
          selectedDate.removeClass('selected');
          updateOlderEvents(newEvent);
          updateTimelinePosition(string, newEvent, timelineComponents, timelineTotWidth);
        }
      }

      function updateTimelinePosition(string, event, timelineComponents, timelineTotWidth) {
        var eventStyle = window.getComputedStyle(event.get(0), null),
            eventLeft = Number(eventStyle.getPropertyValue("left").replace('px', '')),
            timelineWidth = Number(timelineComponents['timelineWrapper'].css('width').replace('px', '')),
            timelineTotWidth = Number(timelineComponents['eventsWrapper'].css('width').replace('px', ''));
        var timelineTranslate = getTranslateValue(timelineComponents['eventsWrapper']);

        if (string == 'next' && eventLeft > timelineWidth - timelineTranslate || string == 'prev' && eventLeft < -timelineTranslate) {
          translateTimeline(timelineComponents, -eventLeft + timelineWidth / 2, timelineWidth - timelineTotWidth);
        }
      }

      function translateTimeline(timelineComponents, value, totWidth) {
        var eventsWrapper = timelineComponents['eventsWrapper'].get(0);
        value = value > 0 ? 0 : value;
        value = !(typeof totWidth === 'undefined') && value < totWidth ? totWidth : value;
        setTransformValue(eventsWrapper, 'translateX', value + 'px');
        value == 0 ? timelineComponents['timelineNavigation'].find('.prev').addClass('inactive') : timelineComponents['timelineNavigation'].find('.prev').removeClass('inactive');
        value == totWidth ? timelineComponents['timelineNavigation'].find('.next').addClass('inactive') : timelineComponents['timelineNavigation'].find('.next').removeClass('inactive');
      }

      function updateFilling(selectedEvent, filling, totWidth) {
        var eventStyle = window.getComputedStyle(selectedEvent.get(0), null),
            eventLeft = eventStyle.getPropertyValue("left"),
            eventWidth = eventStyle.getPropertyValue("width");
        eventLeft = Number(eventLeft.replace('px', '')) + Number(eventWidth.replace('px', '')) / 2;
        var scaleValue = eventLeft / totWidth;
        setTransformValue(filling.get(0), 'scaleX', scaleValue);
      }

      function setDatePosition(timelineComponents, min) {
        for (i = 0; i < timelineComponents['timelineDates'].length; i++) {
          var distance = daydiff(timelineComponents['timelineDates'][0], timelineComponents['timelineDates'][i]),
              distanceNorm = Math.round(distance / timelineComponents['eventsMinLapse']) + 2;
          timelineComponents['timelineEvents'].eq(i).css('left', distanceNorm * min + 'px');
        }
      }

      function setTimelineWidth(timelineComponents, width) {
        var timeSpan = daydiff(timelineComponents['timelineDates'][0], timelineComponents['timelineDates'][timelineComponents['timelineDates'].length - 1]),
            timeSpanNorm = timeSpan / timelineComponents['eventsMinLapse'],
            timeSpanNorm = Math.round(timeSpanNorm) + 4,
            totalWidth = timeSpanNorm * width;
        timelineComponents['eventsWrapper'].css('width', totalWidth + 'px');
        updateFilling(timelineComponents['timelineEvents'].eq(0), timelineComponents['fillingLine'], totalWidth);
        return totalWidth;
      }

      function updateVisibleContent(event, eventsContent) {
        var eventDate = event.data('date'),
            visibleContent = eventsContent.find('.selected'),
            selectedContent = eventsContent.find('[data-date="' + eventDate + '"]'),
            selectedContentHeight = selectedContent.height();

        if (selectedContent.index() > visibleContent.index()) {
          var classEnetering = 'selected enter-right',
              classLeaving = 'leave-left';
        } else {
          var classEnetering = 'selected enter-left',
              classLeaving = 'leave-right';
        }

        selectedContent.attr('class', classEnetering);
        visibleContent.attr('class', classLeaving).one('webkitAnimationEnd oanimationend msAnimationEnd animationend', function () {
          visibleContent.removeClass('leave-right leave-left');
          selectedContent.removeClass('enter-left enter-right');
        });
        eventsContent.css('height', selectedContentHeight + 'px');
      }

      function updateOlderEvents(event) {
        event.parent('li').prevAll('li').children('a').addClass('older-event').end().end().nextAll('li').children('a').removeClass('older-event');
      }

      function getTranslateValue(timeline) {
        var timelineStyle = window.getComputedStyle(timeline.get(0), null),
            timelineTranslate = timelineStyle.getPropertyValue("-webkit-transform") || timelineStyle.getPropertyValue("-moz-transform") || timelineStyle.getPropertyValue("-ms-transform") || timelineStyle.getPropertyValue("-o-transform") || timelineStyle.getPropertyValue("transform");

        if (timelineTranslate.indexOf('(') >= 0) {
          var timelineTranslate = timelineTranslate.split('(')[1];
          timelineTranslate = timelineTranslate.split(')')[0];
          timelineTranslate = timelineTranslate.split(',');
          var translateValue = timelineTranslate[4];
        } else {
          var translateValue = 0;
        }

        return Number(translateValue);
      }

      function setTransformValue(element, property, value) {
        element.style["-webkit-transform"] = property + "(" + value + ")";
        element.style["-moz-transform"] = property + "(" + value + ")";
        element.style["-ms-transform"] = property + "(" + value + ")";
        element.style["-o-transform"] = property + "(" + value + ")";
        element.style["transform"] = property + "(" + value + ")";
      }

      function parseDate(events) {
        var dateArrays = [];
        events.each(function () {
          var dateComp = jQuery(this).data('date').split('/'),
              newDate = new Date(dateComp[2], dateComp[1] - 1, dateComp[0]);
          dateArrays.push(newDate);
        });
        return dateArrays;
      }

      function parseDate2(events) {
        var dateArrays = [];
        events.each(function () {
          var singleDate = jQuery(this),
              dateComp = singleDate.data('date').split('T');

          if (dateComp.length > 1) {
            var dayComp = dateComp[0].split('/'),
                timeComp = dateComp[1].split(':');
          } else if (dateComp[0].indexOf(':') >= 0) {
            var dayComp = ["2000", "0", "0"],
                timeComp = dateComp[0].split(':');
          } else {
            var dayComp = dateComp[0].split('/'),
                timeComp = ["0", "0"];
          }

          var newDate = new Date(dayComp[2], dayComp[1] - 1, dayComp[0], timeComp[0], timeComp[1]);
          dateArrays.push(newDate);
        });
        return dateArrays;
      }

      function daydiff(first, second) {
        return Math.round(second - first);
      }

      function minLapse(dates) {
        var dateDistances = [];

        for (i = 1; i < dates.length; i++) {
          var distance = daydiff(dates[i - 1], dates[i]);
          dateDistances.push(distance);
        }

        return Math.min.apply(null, dateDistances);
      }

      function elementInViewport(el) {
        var top = el.offsetTop;
        var left = el.offsetLeft;
        var width = el.offsetWidth;
        var height = el.offsetHeight;

        while (el.offsetParent) {
          el = el.offsetParent;
          top += el.offsetTop;
          left += el.offsetLeft;
        }

        return top < window.pageYOffset + window.innerHeight && left < window.pageXOffset + window.innerWidth && top + height > window.pageYOffset && left + width > window.pageXOffset;
      }

      function checkMQ() {
        return window.getComputedStyle(document.querySelector('.cd-horizontal-timeline'), '::before').getPropertyValue('content').replace(/'/g, "").replace(/"/g, "");
      }
    });
    elementorFrontend.hooks.addAction('frontend/element_ready/grandrestaurant-background-list.default', function ($scope) {
      jQuery(".background-list-wrapper").each(function () {
        var parentDiv = jQuery(this);
        parentDiv.children('.background-list-column').hover(function () {
          parentDiv.find('.background-list-img').removeClass('hover');
          jQuery(this).next('.background-list-img').addClass('hover');
        });
      });
    });
    elementorFrontend.hooks.addAction('frontend/element_ready/grandrestaurant-portfolio-masonry.default', function ($scope) {
      jQuery(function ($) {
        jQuery(".portfolio-masonry-content-wrapper.do-masonry").each(function () {
          var grid = jQuery(this);
          var cols = grid.attr('data-cols');
          cols = parseInt(cols);
          var gutter = 50;

          switch (cols) {
            case 2:
              gutter = 50;
              break;

            case 3:
              gutter = 40;
              break;

            case 4:
              gutter = 30;
              break;

            case 5:
              gutter = 20;
              break;
          }

          grid.imagesLoaded().done(function () {
            grid.masonry({
              itemSelector: ".gallery-grid-item",
              columnWidth: ".gallery-grid-item",
              gutter: gutter
            });
          });
          jQuery(".portfolio-masonry-content-wrapper.do-masonry img.lazy_masonry").each(function () {
            var currentImg = jQuery(this);
            currentImg.parent("div.post-featured-image-hover").removeClass("lazy");
            var cols = grid.attr('data-cols');
            var gutter = 50;

            switch (cols) {
              case 2:
                gutter = 50;
                break;

              case 3:
                gutter = 40;
                break;

              case 4:
                gutter = 30;
                break;

              case 5:
                gutter = 20;
                break;
            }

            jQuery(this).Lazy({
              onFinishedAll: function onFinishedAll() {
                grid.masonry({
                  itemSelector: ".portfolio-masonry-grid-wrapper.scale-anm",
                  columnWidth: ".portfolio-masonry-grid-wrapper.scale-anm",
                  gutter: gutter
                });
              }
            });
          });
          jQuery(".portfolio-masonry-container").each(function () {
            var containderDiv = jQuery(this);
            var selectedClass = "";
            containderDiv.find(".filter-tag-btn").on('click', function () {
              containderDiv.find(".filter-tag-btn").removeClass("active");
              jQuery(this).addClass("active");
              selectedClass = jQuery(this).attr("data-rel");
              var gridDiv = containderDiv.find(".portfolio-masonry-content-wrapper.do-masonry");
              gridDiv.fadeTo(100, 0);
              gridDiv.find(".portfolio-masonry-grid-wrapper").css({
                opacity: 0,
                display: 'none',
                transform: 'scale(0.0)'
              });
              gridDiv.find(".portfolio-masonry-grid-wrapper").not("." + selectedClass).fadeOut().removeClass('scale-anm');
              setTimeout(function () {
                jQuery("." + selectedClass).fadeIn().addClass('scale-anm');
                jQuery("." + selectedClass).css({
                  opacity: 1,
                  display: 'block',
                  transform: 'scale(1,1)'
                });
                gridDiv.masonry('destroy');
                var $grid = gridDiv.masonry({
                  itemSelector: ".portfolio-masonry-grid-wrapper.scale-anm",
                  columnWidth: ".portfolio-masonry-grid-wrapper.scale-anm",
                  gutter: gutter
                });
                $grid.masonry('reloadItems');
                setTimeout(function () {
                  gridDiv.fadeTo(300, 1);
                }, 300);
              }, 300);
            });
          });
        });
      });
    });
    elementorFrontend.hooks.addAction('frontend/element_ready/grandrestaurant-portfolio-masonry-grid.default', function ($scope) {
      jQuery(function ($) {
        jQuery(".portfolio-masonry-content-wrapper.do-masonry").each(function () {
          var grid = jQuery(this);
          var cols = grid.attr('data-cols');
          cols = parseInt(cols);
          var gutter = 50;

          switch (cols) {
            case 2:
              gutter = 50;
              break;

            case 3:
              gutter = 40;
              break;

            case 4:
              gutter = 30;
              break;

            case 5:
              gutter = 20;
              break;
          }

          grid.imagesLoaded().done(function () {
            grid.masonry({
              itemSelector: ".portfolio-masonry-grid-wrapper.scale-anm",
              columnWidth: ".portfolio-masonry-grid-wrapper.scale-anm",
              gutter: gutter
            });
          });
          jQuery(".portfolio-masonry-content-wrapper.do-masonry img.lazy_masonry").each(function () {
            var currentImg = jQuery(this);
            currentImg.parent("div.post-featured-image-hover").removeClass("lazy");
            var cols = grid.attr('data-cols');
            var gutter = 50;

            switch (cols) {
              case 2:
                gutter = 50;
                break;

              case 3:
                gutter = 40;
                break;

              case 4:
                gutter = 30;
                break;

              case 5:
                gutter = 20;
                break;
            }

            jQuery(this).Lazy({
              onFinishedAll: function onFinishedAll() {
                grid.masonry({
                  itemSelector: ".gallery-grid-item",
                  columnWidth: ".gallery-grid-item",
                  gutter: gutter
                });
              }
            });
          });
          jQuery(".portfolio-masonry-container").each(function () {
            var containderDiv = jQuery(this);
            var selectedClass = "";
            containderDiv.find(".filter-tag-btn").on('click', function () {
              containderDiv.find(".filter-tag-btn").removeClass("active");
              jQuery(this).addClass("active");
              selectedClass = jQuery(this).attr("data-rel");
              var gridDiv = containderDiv.find(".portfolio-masonry-content-wrapper.do-masonry");
              gridDiv.fadeTo(100, 0);
              gridDiv.find(".portfolio-masonry-grid-wrapper").css({
                opacity: 0,
                display: 'none',
                transform: 'scale(0.0)'
              });
              gridDiv.find(".portfolio-masonry-grid-wrapper").not("." + selectedClass).fadeOut().removeClass('scale-anm');
              setTimeout(function () {
                jQuery("." + selectedClass).fadeIn().addClass('scale-anm');
                jQuery("." + selectedClass).css({
                  opacity: 1,
                  display: 'block',
                  transform: 'scale(1,1)'
                });
                gridDiv.masonry('destroy');
                var $grid = gridDiv.masonry({
                  itemSelector: ".portfolio-masonry-grid-wrapper.scale-anm",
                  columnWidth: ".portfolio-masonry-grid-wrapper.scale-anm",
                  gutter: gutter
                });
                $grid.masonry('reloadItems');
                setTimeout(function () {
                  gridDiv.fadeTo(300, 1);
                }, 300);
              }, 300);
            });
          });
        });
      });
    });
    elementorFrontend.hooks.addAction('frontend/element_ready/grandrestaurant-portfolio-timeline.default', function ($scope) {
      var timelines = jQuery('.cd-horizontal-timeline');
      var eventsMinDistance = timelines.attr('data-spacing');

      if (eventsMinDistance == '') {
        eventsMinDistance = 60;
      }

      timelines.length > 0 && initTimeline(timelines);

      function initTimeline(timelines) {
        timelines.each(function () {
          var timeline = jQuery(this),
              timelineComponents = {};
          timelineComponents['timelineWrapper'] = timeline.find('.events-wrapper');
          timelineComponents['eventsWrapper'] = timelineComponents['timelineWrapper'].children('.events');
          timelineComponents['fillingLine'] = timelineComponents['eventsWrapper'].children('.filling-line');
          timelineComponents['timelineEvents'] = timelineComponents['eventsWrapper'].find('a');
          timelineComponents['timelineDates'] = parseDate(timelineComponents['timelineEvents']);
          timelineComponents['eventsMinLapse'] = minLapse(timelineComponents['timelineDates']);
          timelineComponents['timelineNavigation'] = timeline.find('.cd-timeline-navigation');
          timelineComponents['eventsContent'] = timeline.children('.events-content');
          setDatePosition(timelineComponents, eventsMinDistance);
          var timelineTotWidth = setTimelineWidth(timelineComponents, eventsMinDistance);
          timeline.addClass('loaded');
          timelineComponents['timelineNavigation'].on('click', '.next', function (event) {
            event.preventDefault();
            updateSlide(timelineComponents, timelineTotWidth, 'next');
          });
          timelineComponents['timelineNavigation'].on('click', '.prev', function (event) {
            event.preventDefault();
            updateSlide(timelineComponents, timelineTotWidth, 'prev');
          });
          timelineComponents['eventsWrapper'].on('click', 'a', function (event) {
            event.preventDefault();
            timelineComponents['timelineEvents'].removeClass('selected');
            jQuery(this).addClass('selected');
            updateOlderEvents(jQuery(this));
            updateFilling(jQuery(this), timelineComponents['fillingLine'], timelineTotWidth);
            updateVisibleContent(jQuery(this), timelineComponents['eventsContent']);
          });
          timelineComponents['eventsContent'].on('swipeleft', function () {
            var mq = checkMQ();
            mq == 'mobile' && showNewContent(timelineComponents, timelineTotWidth, 'next');
          });
          timelineComponents['eventsContent'].on('swiperight', function () {
            var mq = checkMQ();
            mq == 'mobile' && showNewContent(timelineComponents, timelineTotWidth, 'prev');
          });
          jQuery(document).keyup(function (event) {
            if (event.which == '37' && elementInViewport(timeline.get(0))) {
              showNewContent(timelineComponents, timelineTotWidth, 'prev');
            } else if (event.which == '39' && elementInViewport(timeline.get(0))) {
              showNewContent(timelineComponents, timelineTotWidth, 'next');
            }
          });
        });
      }

      function updateSlide(timelineComponents, timelineTotWidth, string) {
        var translateValue = getTranslateValue(timelineComponents['eventsWrapper']),
            wrapperWidth = Number(timelineComponents['timelineWrapper'].css('width').replace('px', ''));
        string == 'next' ? translateTimeline(timelineComponents, translateValue - wrapperWidth + eventsMinDistance, wrapperWidth - timelineTotWidth) : translateTimeline(timelineComponents, translateValue + wrapperWidth - eventsMinDistance);
      }

      function showNewContent(timelineComponents, timelineTotWidth, string) {
        var visibleContent = timelineComponents['eventsContent'].find('.selected'),
            newContent = string == 'next' ? visibleContent.next() : visibleContent.prev();

        if (newContent.length > 0) {
          var selectedDate = timelineComponents['eventsWrapper'].find('.selected'),
              newEvent = string == 'next' ? selectedDate.parent('li').next('li').children('a') : selectedDate.parent('li').prev('li').children('a');
          updateFilling(newEvent, timelineComponents['fillingLine'], timelineTotWidth);
          updateVisibleContent(newEvent, timelineComponents['eventsContent']);
          newEvent.addClass('selected');
          selectedDate.removeClass('selected');
          updateOlderEvents(newEvent);
          updateTimelinePosition(string, newEvent, timelineComponents, timelineTotWidth);
        }
      }

      function updateTimelinePosition(string, event, timelineComponents, timelineTotWidth) {
        var eventStyle = window.getComputedStyle(event.get(0), null),
            eventLeft = Number(eventStyle.getPropertyValue("left").replace('px', '')),
            timelineWidth = Number(timelineComponents['timelineWrapper'].css('width').replace('px', '')),
            timelineTotWidth = Number(timelineComponents['eventsWrapper'].css('width').replace('px', ''));
        var timelineTranslate = getTranslateValue(timelineComponents['eventsWrapper']);

        if (string == 'next' && eventLeft > timelineWidth - timelineTranslate || string == 'prev' && eventLeft < -timelineTranslate) {
          translateTimeline(timelineComponents, -eventLeft + timelineWidth / 2, timelineWidth - timelineTotWidth);
        }
      }

      function translateTimeline(timelineComponents, value, totWidth) {
        var eventsWrapper = timelineComponents['eventsWrapper'].get(0);
        value = value > 0 ? 0 : value;
        value = !(typeof totWidth === 'undefined') && value < totWidth ? totWidth : value;
        setTransformValue(eventsWrapper, 'translateX', value + 'px');
        value == 0 ? timelineComponents['timelineNavigation'].find('.prev').addClass('inactive') : timelineComponents['timelineNavigation'].find('.prev').removeClass('inactive');
        value == totWidth ? timelineComponents['timelineNavigation'].find('.next').addClass('inactive') : timelineComponents['timelineNavigation'].find('.next').removeClass('inactive');
      }

      function updateFilling(selectedEvent, filling, totWidth) {
        var eventStyle = window.getComputedStyle(selectedEvent.get(0), null),
            eventLeft = eventStyle.getPropertyValue("left"),
            eventWidth = eventStyle.getPropertyValue("width");
        eventLeft = Number(eventLeft.replace('px', '')) + Number(eventWidth.replace('px', '')) / 2;
        var scaleValue = eventLeft / totWidth;
        setTransformValue(filling.get(0), 'scaleX', scaleValue);
      }

      function setDatePosition(timelineComponents, min) {
        for (i = 0; i < timelineComponents['timelineDates'].length; i++) {
          var distance = daydiff(timelineComponents['timelineDates'][0], timelineComponents['timelineDates'][i]),
              distanceNorm = Math.round(distance / timelineComponents['eventsMinLapse']) + 2;
          timelineComponents['timelineEvents'].eq(i).css('left', distanceNorm * min + 'px');
        }
      }

      function setTimelineWidth(timelineComponents, width) {
        var timeSpan = daydiff(timelineComponents['timelineDates'][0], timelineComponents['timelineDates'][timelineComponents['timelineDates'].length - 1]),
            timeSpanNorm = timeSpan / timelineComponents['eventsMinLapse'],
            timeSpanNorm = Math.round(timeSpanNorm) + 4,
            totalWidth = timeSpanNorm * width;
        timelineComponents['eventsWrapper'].css('width', totalWidth + 'px');
        updateFilling(timelineComponents['timelineEvents'].eq(0), timelineComponents['fillingLine'], totalWidth);
        return totalWidth;
      }

      function updateVisibleContent(event, eventsContent) {
        var eventDate = event.data('date'),
            visibleContent = eventsContent.find('.selected'),
            selectedContent = eventsContent.find('[data-date="' + eventDate + '"]'),
            selectedContentHeight = selectedContent.height();

        if (selectedContent.index() > visibleContent.index()) {
          var classEnetering = 'selected enter-right',
              classLeaving = 'leave-left';
        } else {
          var classEnetering = 'selected enter-left',
              classLeaving = 'leave-right';
        }

        selectedContent.attr('class', classEnetering);
        visibleContent.attr('class', classLeaving).one('webkitAnimationEnd oanimationend msAnimationEnd animationend', function () {
          visibleContent.removeClass('leave-right leave-left');
          selectedContent.removeClass('enter-left enter-right');
        });
        eventsContent.css('height', selectedContentHeight + 'px');
      }

      function updateOlderEvents(event) {
        event.parent('li').prevAll('li').children('a').addClass('older-event').end().end().nextAll('li').children('a').removeClass('older-event');
      }

      function getTranslateValue(timeline) {
        var timelineStyle = window.getComputedStyle(timeline.get(0), null),
            timelineTranslate = timelineStyle.getPropertyValue("-webkit-transform") || timelineStyle.getPropertyValue("-moz-transform") || timelineStyle.getPropertyValue("-ms-transform") || timelineStyle.getPropertyValue("-o-transform") || timelineStyle.getPropertyValue("transform");

        if (timelineTranslate.indexOf('(') >= 0) {
          var timelineTranslate = timelineTranslate.split('(')[1];
          timelineTranslate = timelineTranslate.split(')')[0];
          timelineTranslate = timelineTranslate.split(',');
          var translateValue = timelineTranslate[4];
        } else {
          var translateValue = 0;
        }

        return Number(translateValue);
      }

      function setTransformValue(element, property, value) {
        element.style["-webkit-transform"] = property + "(" + value + ")";
        element.style["-moz-transform"] = property + "(" + value + ")";
        element.style["-ms-transform"] = property + "(" + value + ")";
        element.style["-o-transform"] = property + "(" + value + ")";
        element.style["transform"] = property + "(" + value + ")";
      }

      function parseDate(events) {
        var dateArrays = [];
        events.each(function () {
          var dateComp = jQuery(this).data('date').split('/'),
              newDate = new Date(dateComp[2], dateComp[1] - 1, dateComp[0]);
          dateArrays.push(newDate);
        });
        return dateArrays;
      }

      function parseDate2(events) {
        var dateArrays = [];
        events.each(function () {
          var singleDate = jQuery(this),
              dateComp = singleDate.data('date').split('T');

          if (dateComp.length > 1) {
            var dayComp = dateComp[0].split('/'),
                timeComp = dateComp[1].split(':');
          } else if (dateComp[0].indexOf(':') >= 0) {
            var dayComp = ["2000", "0", "0"],
                timeComp = dateComp[0].split(':');
          } else {
            var dayComp = dateComp[0].split('/'),
                timeComp = ["0", "0"];
          }

          var newDate = new Date(dayComp[2], dayComp[1] - 1, dayComp[0], timeComp[0], timeComp[1]);
          dateArrays.push(newDate);
        });
        return dateArrays;
      }

      function daydiff(first, second) {
        return Math.round(second - first);
      }

      function minLapse(dates) {
        var dateDistances = [];

        for (i = 1; i < dates.length; i++) {
          var distance = daydiff(dates[i - 1], dates[i]);
          dateDistances.push(distance);
        }

        return Math.min.apply(null, dateDistances);
      }

      function elementInViewport(el) {
        var top = el.offsetTop;
        var left = el.offsetLeft;
        var width = el.offsetWidth;
        var height = el.offsetHeight;

        while (el.offsetParent) {
          el = el.offsetParent;
          top += el.offsetTop;
          left += el.offsetLeft;
        }

        return top < window.pageYOffset + window.innerHeight && left < window.pageXOffset + window.innerWidth && top + height > window.pageYOffset && left + width > window.pageXOffset;
      }

      function checkMQ() {
        return window.getComputedStyle(document.querySelector('.cd-horizontal-timeline'), '::before').getPropertyValue('content').replace(/'/g, "").replace(/"/g, "");
      }
    });
    elementorFrontend.hooks.addAction('frontend/element_ready/grandrestaurant-portfolio-timeline-vertical.default', function ($scope) {
      jQuery(".portfolio-timeline-vertical-content-wrapper").each(function () {
        var slideshow = jQuery(this);
        var autoPlay = slideshow.attr('data-autoplay');
        var autoPlayArr = false;

        if (typeof autoPlay != "undefined") {
          autoPlayArr = {
            delay: autoPlay
          };
        }

        var speed = slideshow.attr('data-speed');

        if (typeof speed == "undefined") {
          speed = 1600;
        }

        var timelineSwiper = new Swiper('.portfolio-timeline-vertical-content-wrapper .timeline .swiper-container', {
          direction: 'vertical',
          loop: false,
          speed: parseInt(speed),
          autoplay: autoPlayArr,
          pagination: {
            el: '.swiper-pagination',
            type: 'bullets',
            renderBullet: function renderBullet(index, className) {
              var year = document.querySelectorAll('.swiper-slide')[index].getAttribute('data-year');
              return '<span class="' + className + '">' + year + '</span>';
              ;
            },
            clickable: true
          },
          navigation: {
            nextEl: '.swiper-button-next',
            prevEl: '.swiper-button-prev'
          },
          breakpoints: {
            768: {
              direction: 'horizontal'
            }
          },
          on: {
            init: function init() {
              slideshow.delay(100).queue(function (next) {
                jQuery(this).addClass('active');
              });
            }
          }
        });
      });
    });
    elementorFrontend.hooks.addAction('frontend/element_ready/grandrestaurant-slider-parallax.default', function ($scope) {
      var slideshow = jQuery('.slider-parallax-wrapper');
      var timer = slideshow.attr('data-autoplay');
      var autoplay = true;

      if (timer == 0) {
        timer = false;
        autoplay = false;
      }

      var pagination = slideshow.attr('data-pagination');

      if (pagination == 0) {
        var pagination = false;
      } else {
        var pagination = true;
      }

      var navigation = slideshow.attr('data-navigation');

      if (navigation == 0) {
        var navigation = false;
      } else {
        var navigation = true;
      }

      var slideshowDuration = timer;

      function slideshowSwitch(slideshow, index, auto) {
        if (slideshow.data('wait')) return;
        var slides = slideshow.find('.slide');
        var pages = slideshow.find('.pagination');
        var activeSlide = slides.filter('.is-active');
        var activeSlideImage = activeSlide.find('.image-container');
        var newSlide = slides.eq(index);
        var newSlideImage = newSlide.find('.image-container');
        var newSlideContent = newSlide.find('.slide-content');
        var newSlideElements = newSlide.find('.caption > *');
        if (newSlide.is(activeSlide)) return;
        newSlide.addClass('is-new');
        var timeout = slideshow.data('timeout');
        clearTimeout(timeout);
        slideshow.data('wait', true);
        var transition = slideshow.attr('data-transition');

        if (transition == 'fade') {
          newSlide.css({
            display: 'block',
            zIndex: 2
          });
          newSlideImage.css({
            opacity: 0
          });
          TweenMax.to(newSlideImage, 1, {
            alpha: 1,
            onComplete: function onComplete() {
              newSlide.addClass('is-active').removeClass('is-new');
              activeSlide.removeClass('is-active');
              newSlide.css({
                display: '',
                zIndex: ''
              });
              newSlideImage.css({
                opacity: ''
              });
              slideshow.find('.pagination').trigger('check');
              slideshow.data('wait', false);

              if (auto) {
                timeout = setTimeout(function () {
                  slideshowNext(slideshow, false, true);
                }, slideshowDuration);
                slideshow.data('timeout', timeout);
              }
            }
          });
        } else {
          if (newSlide.index() > activeSlide.index()) {
            var newSlideRight = 0;
            var newSlideLeft = 'auto';
            var newSlideImageRight = -slideshow.width() / 8;
            var newSlideImageLeft = 'auto';
            var newSlideImageToRight = 0;
            var newSlideImageToLeft = 'auto';
            var newSlideContentLeft = 'auto';
            var newSlideContentRight = 0;
            var activeSlideImageLeft = -slideshow.width() / 4;
          } else {
            var newSlideRight = '';
            var newSlideLeft = 0;
            var newSlideImageRight = 'auto';
            var newSlideImageLeft = -slideshow.width() / 8;
            var newSlideImageToRight = '';
            var newSlideImageToLeft = 0;
            var newSlideContentLeft = 0;
            var newSlideContentRight = 'auto';
            var activeSlideImageLeft = slideshow.width() / 4;
          }

          newSlide.css({
            display: 'block',
            width: 0,
            right: newSlideRight,
            left: newSlideLeft,
            zIndex: 2
          });
          newSlideImage.css({
            width: slideshow.width(),
            right: newSlideImageRight,
            left: newSlideImageLeft
          });
          newSlideContent.css({
            width: slideshow.width(),
            left: newSlideContentLeft,
            right: newSlideContentRight
          });
          activeSlideImage.css({
            left: 0
          });
          TweenMax.set(newSlideElements, {
            y: 20,
            force3D: true
          });
          TweenMax.to(activeSlideImage, 1, {
            left: activeSlideImageLeft,
            ease: Power3.easeInOut
          });
          TweenMax.to(newSlide, 1, {
            width: slideshow.width(),
            ease: Power3.easeInOut
          });
          TweenMax.to(newSlideImage, 1, {
            right: newSlideImageToRight,
            left: newSlideImageToLeft,
            ease: Power3.easeInOut
          });
          TweenMax.staggerFromTo(newSlideElements, 0.8, {
            alpha: 0,
            y: 60
          }, {
            alpha: 1,
            y: 0,
            ease: Power3.easeOut,
            force3D: true,
            delay: 0.6
          }, 0.1, function () {
            newSlide.addClass('is-active').removeClass('is-new');
            activeSlide.removeClass('is-active');
            newSlide.css({
              display: '',
              width: '',
              left: '',
              zIndex: ''
            });
            newSlideImage.css({
              width: '',
              right: '',
              left: ''
            });
            newSlideContent.css({
              width: '',
              left: ''
            });
            newSlideElements.css({
              opacity: '',
              transform: ''
            });
            activeSlideImage.css({
              left: ''
            });
            slideshow.find('.pagination').trigger('check');
            slideshow.data('wait', false);

            if (auto) {
              timeout = setTimeout(function () {
                slideshowNext(slideshow, false, true);
              }, slideshowDuration);
              slideshow.data('timeout', timeout);
            }
          });
        }
      }

      function slideshowNext(slideshow, previous, auto) {
        var slides = slideshow.find('.slide');
        var activeSlide = slides.filter('.is-active');
        var newSlide = null;

        if (previous) {
          newSlide = activeSlide.prev('.slide');

          if (newSlide.length === 0) {
            newSlide = slides.last();
          }
        } else {
          newSlide = activeSlide.next('.slide');
          if (newSlide.length == 0) newSlide = slides.filter('.slide').first();
        }

        slideshowSwitch(slideshow, newSlide.index(), auto);
      }

      function homeSlideshowparallax() {
        var scrollTop = jQuery(window).scrollTop();
        if (scrollTop > windowHeight) return;
        var inner = slideshow.find('.slideshow-inner');
        var newHeight = windowHeight - scrollTop / 2;
        var newTop = scrollTop * 0.8;
        inner.css({
          transform: 'translateY(' + newTop + 'px)',
          height: newHeight
        });
      }

      jQuery(document).ready(function () {
        jQuery('.slider-parallax-wrapper .slide').addClass('is-loaded');
        jQuery('.slider-parallax-wrapper .arrows .arrow').on('click', function () {
          slideshowNext(jQuery(this).closest('.slider-parallax-wrapper'), jQuery(this).hasClass('prev'));
        });
        jQuery('.slider-parallax-wrapper .pagination .item').on('click', function () {
          slideshowSwitch(jQuery(this).closest('.slider-parallax-wrapper'), jQuery(this).index());
        });
        jQuery('.slider-parallax-wrapper .pagination').on('check', function () {
          var slideshow = jQuery(this).closest('.slider-parallax-wrapper');
          var pages = jQuery(this).find('.item');
          var index = slideshow.find('.slider_parallax_slides .is-active').index();
          pages.removeClass('is-active');
          pages.eq(index).addClass('is-active');
        });

        if (autoplay) {
          var timeout = setTimeout(function () {
            slideshowNext(slideshow, false, true);
          }, slideshowDuration);
          slideshow.data('timeout', timeout);
        }
      });
    });
    elementorFrontend.hooks.addAction('frontend/element_ready/grandrestaurant-distortion-grid.default', function ($scope) {
      Array.from(document.querySelectorAll('.distortion-grid-item-img')).forEach(function (el) {
        var imgs = Array.from(el.querySelectorAll('img'));
        new hoverEffect({
          parent: el,
          intensity: el.dataset.intensity || undefined,
          speedIn: el.dataset.speedin || undefined,
          speedOut: el.dataset.speedout || undefined,
          easing: el.dataset.easing || undefined,
          hover: el.dataset.hover || undefined,
          image1: imgs[0].getAttribute('src'),
          image2: imgs[1].getAttribute('src'),
          displacementImage: el.dataset.displacement
        });
      });
    });
    elementorFrontend.hooks.addAction('frontend/element_ready/grandrestaurant-slider-animated.default', function ($scope) {
      var _createClass = function () {
        function defineProperties(target, props) {
          for (var i = 0; i < props.length; i++) {
            var descriptor = props[i];
            descriptor.enumerable = descriptor.enumerable || false;
            descriptor.configurable = true;
            if ("value" in descriptor) descriptor.writable = true;
            Object.defineProperty(target, descriptor.key, descriptor);
          }
        }

        return function (Constructor, protoProps, staticProps) {
          if (protoProps) defineProperties(Constructor.prototype, protoProps);
          if (staticProps) defineProperties(Constructor, staticProps);
          return Constructor;
        };
      }();

      function _classCallCheck(instance, Constructor) {
        if (!(instance instanceof Constructor)) {
          throw new TypeError("Cannot call a class as a function");
        }
      }

      var $window = jQuery(window);
      var $body = jQuery('body');

      var Slideshow = function () {
        function Slideshow() {
          var _this = this;

          var userOptions = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};

          _classCallCheck(this, Slideshow);

          var defaultOptions = {
            $el: jQuery('.animated-slider-wrapper'),
            showArrows: false,
            showpagination: true,
            duration: 10000,
            autoplay: true
          };
          var options = Object.assign({}, defaultOptions, userOptions);
          this.$el = options.$el;
          this.maxSlide = this.$el.find(jQuery('.js-slider-home-slide')).length;
          this.showArrows = this.maxSlide > 1 ? options.showArrows : false;
          this.showpagination = options.showpagination;
          this.currentSlide = 1;
          this.isAnimating = false;
          this.animationDuration = 1200;
          this.autoplaySpeed = options.duration;
          this.interval;
          this.$controls = this.$el.find('.js-slider-home-button');
          this.autoplay = this.maxSlide > 1 ? options.autoplay : false;
          this.autoplay = false;
          this.$el.on('click', '.js-slider-home-next', function (event) {
            return _this.nextSlide();
          });
          this.$el.on('click', '.js-slider-home-prev', function (event) {
            return _this.prevSlide();
          });
          this.$el.on('click', '.js-pagination-item', function (event) {
            if (!_this.isAnimating) {
              _this.preventClick();

              _this.goToSlide(event.target.dataset.slide);
            }
          });
          this.init();
        }

        _createClass(Slideshow, [{
          key: 'init',
          value: function init() {
            this.goToSlide(1);

            if (this.autoplay) {
              this.startAutoplay();
            }

            if (this.showpagination) {
              var paginationNumber = this.maxSlide;
              var pagination = '<div class="pagination"><div class="container">';

              for (var i = 0; i < this.maxSlide; i++) {
                var item = '<span class="pagination-item js-pagination-item ' + (i === 0 ? 'is-current' : '') + '" data-slide=' + (i + 1) + '>' + (i + 1) + '</span>';
                pagination = pagination + item;
              }

              pagination = pagination + '</div></div>';
              this.$el.append(pagination);
            }
          }
        }, {
          key: 'preventClick',
          value: function preventClick() {
            var _this2 = this;

            this.isAnimating = true;
            this.$controls.prop('disabled', true);
            clearInterval(this.interval);
            setTimeout(function () {
              _this2.isAnimating = false;

              _this2.$controls.prop('disabled', false);

              if (_this2.autoplay) {
                _this2.startAutoplay();
              }
            }, this.animationDuration);
          }
        }, {
          key: 'goToSlide',
          value: function goToSlide(index) {
            this.currentSlide = parseInt(index);

            if (this.currentSlide > this.maxSlide) {
              this.currentSlide = 1;
            }

            if (this.currentSlide === 0) {
              this.currentSlide = this.maxSlide;
            }

            var newCurrent = this.$el.find('.js-slider-home-slide[data-slide="' + this.currentSlide + '"]');
            var newprev = this.currentSlide === 1 ? this.$el.find('.js-slider-home-slide').last() : newCurrent.prev('.js-slider-home-slide');
            var newNext = this.currentSlide === this.maxSlide ? this.$el.find('.js-slider-home-slide').first() : newCurrent.next('.js-slider-home-slide');
            this.$el.find('.js-slider-home-slide').removeClass('is-prev is-next is-current');
            this.$el.find('.js-pagination-item').removeClass('is-current');

            if (this.maxSlide > 1) {
              newprev.addClass('is-prev');
              newNext.addClass('is-next');
            }

            newCurrent.addClass('is-current');
            this.$el.find('.js-pagination-item[data-slide="' + this.currentSlide + '"]').addClass('is-current');
          }
        }, {
          key: 'nextSlide',
          value: function nextSlide() {
            this.preventClick();
            this.goToSlide(this.currentSlide + 1);
          }
        }, {
          key: 'prevSlide',
          value: function prevSlide() {
            this.preventClick();
            this.goToSlide(this.currentSlide - 1);
          }
        }, {
          key: 'startAutoplay',
          value: function startAutoplay() {
            var _this3 = this;

            this.interval = setInterval(function () {
              if (!_this3.isAnimating) {
                _this3.nextSlide();
              }
            }, this.autoplaySpeed);
          }
        }, {
          key: 'destroy',
          value: function destroy() {
            this.$el.off();
          }
        }]);

        return Slideshow;
      }();

      (function () {
        var loaded = false;
        var maxLoad = 3000;

        function load() {
          var options = {
            showpagination: true
          };
          var slideShow = new Slideshow(options);
        }

        function addLoadClass() {
          $body.addClass('is-loaded');
          setTimeout(function () {
            $body.addClass('is-animated');
          }, 600);
        }

        $window.on('load', function () {
          if (!loaded) {
            loaded = true;
            load();
          }
        });
        setTimeout(function () {
          if (!loaded) {
            loaded = true;
            load();
          }
        }, maxLoad);
        addLoadClass();
      })();
    });
    elementorFrontend.hooks.addAction('frontend/element_ready/grandrestaurant-slider-fadeup.default', function ($scope) {
      function init(item) {
        var items = item.querySelectorAll('li'),
            current = 0,
            autoUpdate = true;
        var nav = document.createElement('nav');
        nav.className = 'nav_arrows';
        var prevbtn = document.createElement('button');
        prevbtn.className = 'prev';
        prevbtn.setAttribute('aria-label', 'Prev');
        var nextbtn = document.createElement('button');
        nextbtn.className = 'next';
        nextbtn.setAttribute('aria-label', 'Next');
        var counter = document.createElement('div');
        counter.className = 'counter';
        counter.innerHTML = "<span>1</span><span>" + items.length + "</span>";

        if (items.length > 1) {
          nav.appendChild(prevbtn);
          nav.appendChild(counter);
          nav.appendChild(nextbtn);
          item.appendChild(nav);
        }

        items[current].className = "current";
        if (items.length > 1) items[items.length - 1].className = "prev-slide";

        var navigate = function navigate(dir) {
          items[current].className = "";

          if (dir === 'right') {
            current = current < items.length - 1 ? current + 1 : 0;
          } else {
            current = current > 0 ? current - 1 : items.length - 1;
          }

          var nextCurrent = current < items.length - 1 ? current + 1 : 0,
              prevCurrent = current > 0 ? current - 1 : items.length - 1;
          items[current].className = "current";
          items[prevCurrent].className = "prev-slide";
          items[nextCurrent].className = "";
          counter.firstChild.textContent = current + 1;
        };

        item.addEventListener('mouseenter', function () {
          autoUpdate = false;
        });
        item.addEventListener('mouseleave', function () {
          autoUpdate = true;
        });
        prevbtn.addEventListener('click', function () {
          navigate('left');
        });
        nextbtn.addEventListener('click', function () {
          navigate('right');
        });
        document.addEventListener('keydown', function (ev) {
          var keyCode = ev.keyCode || ev.which;

          switch (keyCode) {
            case 37:
              navigate('left');
              break;

            case 39:
              navigate('right');
              break;
          }
        });
        item.addEventListener('touchstart', handleTouchStart, false);
        item.addEventListener('touchmove', handleTouchMove, false);
        var xDown = null;
        var yDown = null;

        function handleTouchStart(evt) {
          xDown = evt.touches[0].clientX;
          yDown = evt.touches[0].clientY;
        }

        ;

        function handleTouchMove(evt) {
          if (!xDown || !yDown) {
            return;
          }

          var xUp = evt.touches[0].clientX;
          var yUp = evt.touches[0].clientY;
          var xDiff = xDown - xUp;
          var yDiff = yDown - yUp;

          if (Math.abs(xDiff) > Math.abs(yDiff)) {
            if (xDiff > 0) {
              navigate('right');
            } else {
              navigate('left');
            }
          }

          xDown = null;
          yDown = null;
        }

        ;
      }

      [].slice.call(document.querySelectorAll('.fadeup-slider-wrapper')).forEach(function (item) {
        init(item);
      });
    });
    elementorFrontend.hooks.addAction('frontend/element_ready/grandrestaurant-slider-motion-reveal.default', function ($scope) {
      var Slide = /*#__PURE__*/function () {
        function Slide(el) {
          _classCallCheck2(this, Slide);

          this.DOM = {
            el: el
          };
          this.DOM.imgWrap = this.DOM.el.querySelector('.slide-img-wrap');
          this.DOM.revealer = this.DOM.imgWrap.querySelector('.slide-img-reveal');
          this.DOM.title = this.DOM.el.querySelector('.slide-title');
          this.DOM.number = this.DOM.el.querySelector('.slide-number');
          this.DOM.preview = {
            imgWrap: this.DOM.el.querySelector('.preview-img-wrap'),
            revealer: this.DOM.el.querySelector('.preview-img-wrap > .preview-img-reveal'),
            title: this.DOM.el.querySelector('.preview-title'),
            content: this.DOM.el.querySelector('.preview-content')
          };
          this.config = {
            animation: {
              duration: 0.6,
              ease: Expo.easeOut
            }
          };
        }

        _createClass2(Slide, [{
          key: "setCurrent",
          value: function setCurrent() {
            var isCurrent = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : true;
            this.DOM.el.classList[isCurrent ? 'add' : 'remove']('slide-current');
          }
        }, {
          key: "hide",
          value: function hide(direction) {
            return this.toggle('hide', direction);
          }
        }, {
          key: "show",
          value: function show(direction) {
            return this.toggle('show', direction);
          }
        }, {
          key: "toggle",
          value: function toggle(action, direction) {
            var _this6 = this;

            return new Promise(function (resolve, reject) {
              var revealerOpts = {
                delay: action === 'hide' ? 0 : _this6.config.animation.duration / 2,
                ease: _this6.config.animation.ease,
                onComplete: resolve
              };
              var commonOpts = {
                delay: action === 'hide' ? 0 : _this6.config.animation.duration / 2,
                ease: _this6.config.animation.ease,
                opacity: action === 'hide' ? 0 : 1
              };
              var imgOpts = Object.assign({}, commonOpts);
              var numberOpts = Object.assign({}, commonOpts);
              var titleOpts = Object.assign({}, commonOpts);

              if (direction === 'left' || direction === 'right') {
                revealerOpts.startAt = action === 'hide' ? {
                  x: direction === 'left' ? '-100%' : '100%',
                  y: '0%'
                } : {
                  x: '0%',
                  y: '0%'
                };
                revealerOpts.x = action === 'hide' ? '0%' : direction === 'left' ? '100%' : '-100%';
                imgOpts.startAt = action === 'show' ? {
                  opacity: 0,
                  x: direction === 'left' ? '-20%' : '20%'
                } : {};
                imgOpts.x = action === 'hide' ? direction === 'left' ? '20%' : '-20%' : '0%';
                titleOpts.startAt = action === 'show' ? {
                  opacity: 1,
                  scale: 0.2,
                  x: direction === 'left' ? '-200%' : '200%'
                } : {};
                titleOpts.x = action === 'hide' ? direction === 'left' ? '200%' : '-200%' : '0%';
                titleOpts.scale = action === 'hide' ? 0.2 : 1;
                numberOpts.startAt = action === 'show' ? {
                  opacity: 1,
                  x: direction === 'left' ? '-50%' : '50%'
                } : {};
                numberOpts.x = action === 'hide' ? direction === 'left' ? '50%' : '-50%' : '0%';
              } else {
                revealerOpts.startAt = action === 'hide' ? {
                  x: '0%',
                  y: direction === 'down' ? '-100%' : '100%'
                } : {
                  x: '0%',
                  y: '0%'
                };
                revealerOpts.y = action === 'hide' ? '0%' : direction === 'down' ? '100%' : '-100%';
                imgOpts.startAt = action === 'show' ? {
                  opacity: 1,
                  y: direction === 'down' ? '-10%' : '10%'
                } : {};
                imgOpts.y = action === 'hide' ? direction === 'down' ? '10%' : '-10%' : '0%';
                titleOpts.ease = _this6.config.animation.ease, titleOpts.startAt = action === 'show' ? {
                  opacity: 1,
                  y: direction === 'down' ? '-100%' : '100%'
                } : {};
                titleOpts.y = action === 'hide' ? direction === 'down' ? '100%' : '-100%' : '0%';
              }

              TweenMax.to(_this6.DOM.revealer, _this6.config.animation.duration, revealerOpts);
              TweenMax.to(_this6.DOM.imgWrap, _this6.config.animation.duration, imgOpts);
              TweenMax.to(_this6.DOM.title, _this6.config.animation.duration * 1.5, titleOpts);
              TweenMax.to(_this6.DOM.number, _this6.config.animation.duration, numberOpts);
            });
          }
        }, {
          key: "hidePreview",
          value: function hidePreview(delay) {
            return this.togglePreview('hide');
          }
        }, {
          key: "showpreview",
          value: function showpreview(delay) {
            return this.togglePreview('show');
          }
        }, {
          key: "togglePreview",
          value: function togglePreview(action) {
            var _this7 = this;

            return new Promise(function (resolve, reject) {
              TweenMax.to(_this7.DOM.preview.revealer, _this7.config.animation.duration, {
                delay: action === 'hide' ? 0 : _this7.config.animation.duration / 2,
                ease: _this7.config.animation.ease,
                startAt: action === 'hide' ? {
                  x: '0%',
                  y: '-100%'
                } : {
                  x: '0%',
                  y: '0%'
                },
                y: action === 'hide' ? '0%' : '-100%',
                onComplete: resolve
              });
              TweenMax.to(_this7.DOM.preview.imgWrap, _this7.config.animation.duration, {
                delay: action === 'hide' ? 0 : _this7.config.animation.duration / 2,
                ease: _this7.config.animation.ease,
                startAt: action === 'hide' ? {} : {
                  opacity: 0,
                  y: '20%'
                },
                y: action === 'hide' ? '20%' : '0%',
                opacity: action === 'hide' ? 0 : 1
              });
              TweenMax.to([_this7.DOM.preview.title, _this7.DOM.preview.content], _this7.config.animation.duration, {
                delay: action === 'hide' ? 0 : _this7.config.animation.duration / 2,
                ease: _this7.config.animation.ease,
                startAt: action === 'hide' ? {} : {
                  opacity: 0,
                  y: '200%'
                },
                y: action === 'hide' ? '200%' : '0%',
                opacity: action === 'hide' ? 0 : 1
              });
            });
          }
        }]);

        return Slide;
      }();

      var Slideshow = /*#__PURE__*/function () {
        function Slideshow(el) {
          var _this8 = this;

          _classCallCheck2(this, Slideshow);

          this.DOM = {
            el: el
          };
          this.DOM.prevCtrl = this.DOM.el.querySelector('.slidenav-item--prev');
          this.DOM.nextCtrl = this.DOM.el.querySelector('.slidenav-item--next');
          this.DOM.previewCtrl = this.DOM.el.querySelector('.slidenav-preview');
          this.slides = [];
          Array.from(this.DOM.el.querySelectorAll('.slide')).forEach(function (slideEl) {
            return _this8.slides.push(new Slide(slideEl));
          });
          this.slidesTotal = this.slides.length;
          this.current = 0;
          this.init();
        }

        _createClass2(Slideshow, [{
          key: "init",
          value: function init() {
            this.slides[this.current].setCurrent();
            this.initEvents();
          }
        }, {
          key: "initEvents",
          value: function initEvents() {
            var _this9 = this;

            this.DOM.prevCtrl.addEventListener('click', function () {
              return _this9.prev();
            });
            this.DOM.nextCtrl.addEventListener('click', function () {
              return _this9.next();
            });
            this.DOM.previewCtrl.addEventListener('click', function (ev) {
              if (_this9.isAnimating) return;

              if (ev.target.classList.contains('slidenav-preview--open')) {
                ev.target.classList.remove('slidenav-preview--open');

                _this9.exitPreview();
              } else {
                ev.target.classList.add('slidenav-preview--open');

                _this9.enterPreview();
              }
            });
          }
        }, {
          key: "prev",
          value: function prev() {
            this.navigate('left');
          }
        }, {
          key: "next",
          value: function next() {
            this.navigate('right');
          }
        }, {
          key: "enterPreview",
          value: function enterPreview() {
            this.togglePreview('enter');
          }
        }, {
          key: "exitPreview",
          value: function exitPreview() {
            this.togglePreview('exit');
          }
        }, {
          key: "togglePreview",
          value: function togglePreview(action) {
            var _this10 = this;

            if (this.isAnimating) return;
            this.isAnimating = true;
            var processing = action === 'enter' ? [this.slides[this.current].hide('up'), this.slides[this.current].showpreview()] : [this.slides[this.current].show('down'), this.slides[this.current].hidePreview()];
            this.toggleNavCtrls(action);
            Promise.all(processing).then(function () {
              return _this10.isAnimating = false;
            });
          }
        }, {
          key: "toggleNavCtrls",
          value: function toggleNavCtrls(action) {
            var _this11 = this;

            TweenMax.to([this.DOM.prevCtrl, this.DOM.nextCtrl], 0.5, {
              ease: 'Expo.easeOut',
              opacity: action === 'enter' ? 0 : 1,
              onStart: function onStart() {
                return _this11.DOM.prevCtrl.style.pointerEvents = _this11.DOM.nextCtrl.style.pointerEvents = action === 'enter' ? 'none' : 'auto';
              }
            });
          }
        }, {
          key: "navigate",
          value: function navigate(direction) {
            var _this12 = this;

            if (this.isAnimating) return;
            this.isAnimating = true;
            var nextSlidePos = direction === 'right' ? this.current < this.slidesTotal - 1 ? this.current + 1 : 0 : this.current > 0 ? this.current - 1 : this.slidesTotal - 1;
            Promise.all([this.slides[this.current].hide(direction), this.slides[nextSlidePos].show(direction)]).then(function () {
              _this12.slides[_this12.current].setCurrent(false);

              _this12.current = nextSlidePos;
              _this12.isAnimating = false;

              _this12.slides[_this12.current].setCurrent();
            });
          }
        }]);

        return Slideshow;
      }();

      var slideshow = new Slideshow(document.querySelector('.motion-reveal-slider-wrapper.slideshow'));
    });
    elementorFrontend.hooks.addAction('frontend/element_ready/grandrestaurant-testimonial-card.default', function ($scope) {
      jQuery(".testimonials-card-wrapper .owl-carousel").each(function () {
        var autoPlay = jQuery(this).attr('data-autoplay');

        if (typeof autoPlay == "undefined") {
          autoPlay = false;
        }

        if (autoPlay == 1) {
          autoPlay = true;
        } else {
          autoPlay = false;
        }

        var timer = jQuery(this).attr('data-timer');

        if (typeof timer == "undefined") {
          timer = 8000;
        }

        var pagination = jQuery(this).attr('data-pagination');

        if (typeof pagination == "undefined") {
          pagination = true;
        }

        if (pagination == 1) {
          pagination = true;
        } else {
          pagination = false;
        }

        jQuery(this).owlCarousel({
          loop: true,
          center: true,
          items: 3,
          margin: 0,
          autoplay: autoPlay,
          dots: pagination,
          autoplayTimeout: timer,
          smartSpeed: 450,
          responsive: {
            0: {
              items: 1
            },
            768: {
              items: 2
            },
            1170: {
              items: 3
            }
          }
        });
      });
    });
    elementorFrontend.hooks.addAction('frontend/element_ready/grandrestaurant-slider-image-carousel.default', function ($scope) {
      jQuery(".image-carousel-slider-wrapper").each(function () {
        jQuery(this).find('.carousel-item').eq(0).addClass('active');
        var total = jQuery(this).find('.carousel-item').length;
        var current = 0;
        var slideObj = jQuery(this);
        jQuery(this).find('#moveRight').on('click', function () {
          var next = current;
          current = current + 1;
          setSlide(next, current, slideObj);
        });
        jQuery(this).find('#moveLeft').on('click', function () {
          var prev = current;
          current = current - 1;
          setSlide(prev, current, slideObj);
        });

        function setSlide(prev, next, slideObj) {
          var slide = current;

          if (next > total - 1) {
            slide = 0;
            current = 0;
          }

          if (next < 0) {
            slide = total - 1;
            current = total - 1;
          }

          slideObj.find('.carousel-item').eq(prev).removeClass('active');
          slideObj.find('.carousel-item').eq(slide).addClass('active');
          setTimeout(function () {}, 800);
        }
      });
    });
    elementorFrontend.hooks.addAction('frontend/element_ready/grandrestaurant-portfolio-grid.default', function ($scope) {
      jQuery(".portfolio_grid_container").each(function () {
        var containderDiv = jQuery(this);
        var selectedClass = "";
        containderDiv.find(".filter-tag-btn").on('click', function () {
          containderDiv.find(".filter-tag-btn").removeClass("active");
          jQuery(this).addClass("active");
          selectedClass = jQuery(this).attr("data-rel");
          column = jQuery(this).attr("data-cols");
          var gridDiv = containderDiv.find(".portfolio-grid-content-wrapper");
          gridDiv.fadeTo(100, 0);
          gridDiv.find(".portfolio-grid-wrapper").css({
            opacity: 0,
            display: 'none',
            transform: 'scale(0.0)'
          });
          gridDiv.find(".portfolio-grid-wrapper").not("." + selectedClass).fadeOut().removeClass('scale-anm');
          setTimeout(function () {
            jQuery("." + selectedClass).fadeIn().addClass('scale-anm');
            jQuery("." + selectedClass).css({
              opacity: 1,
              display: 'block',
              transform: 'scale(1,1)'
            });
            gridDiv.find(".portfolio-grid-wrapper.last").removeClass('last');
            var count = gridDiv.find(".portfolio-grid-wrapper.scale-anm").length;
            gridDiv.find(".portfolio-grid-wrapper.scale-anm").each(function (index) {
              var lastIndex = parseInt(index + 1);

              if (lastIndex % column == 0) {
                jQuery(this).addClass('last');
              }

              if (lastIndex == count) {
                setTimeout(function () {
                  gridDiv.fadeTo(300, 1);
                }, 300);
              }
            });
          }, 300);
        });
      });
    });
    elementorFrontend.hooks.addAction('frontend/element_ready/grandrestaurant-portfolio-grid-overlay.default', function ($scope) {
      jQuery(".portfolio-grid-overlay-container").each(function () {
        var containderDiv = jQuery(this);
        var selectedClass = "";
        containderDiv.find(".portfolio-grid-wrapper-overlay").imagesLoaded().always(function () {
          var currentWrapper = containderDiv.find(".portfolio-grid-wrapper-overlay");
          var currentImg = containderDiv.find(".portfolio-grid-wrapper-overlay img");
          currentWrapper.css('height', currentImg.height() + 'px');
        });
        jQuery(window).resize(function () {
          containderDiv.find(".portfolio-grid-wrapper-overlay").each(function () {
            var currentImg = jQuery(this).find("img");
            jQuery(this).css('height', currentImg.height() + 'px');
          });
        });
        containderDiv.find(".filter-tag-btn").on('click', function () {
          containderDiv.find(".filter-tag-btn").removeClass("active");
          jQuery(this).addClass("active");
          selectedClass = jQuery(this).attr("data-rel");
          column = jQuery(this).attr("data-cols");
          var gridDiv = containderDiv.find(".portfolio-grid-content-wrapper");
          gridDiv.fadeTo(100, 0);
          gridDiv.find(".portfolio-grid-wrapper-overlay").css({
            opacity: 0,
            display: 'none',
            transform: 'scale(0.0)'
          });
          gridDiv.find(".portfolio-grid-wrapper-overlay").not("." + selectedClass).fadeOut().removeClass('scale-anm');
          setTimeout(function () {
            jQuery("." + selectedClass).fadeIn().addClass('scale-anm');
            jQuery("." + selectedClass).css({
              opacity: 1,
              display: 'block',
              transform: 'scale(1,1)'
            });
            gridDiv.find(".portfolio-grid-wrapper-overlay.last").removeClass('last');
            var count = gridDiv.find(".portfolio-grid-wrapper-overlay.scale-anm").length;
            gridDiv.find(".portfolio-grid-wrapper-overlay.scale-anm").each(function (index) {
              var lastIndex = parseInt(index + 1);

              if (lastIndex % column == 0) {
                jQuery(this).addClass('last');
              }

              if (lastIndex == count) {
                setTimeout(function () {
                  gridDiv.fadeTo(300, 1);
                }, 300);
              }
            });
          }, 300);
        });
      });
    });
    elementorFrontend.hooks.addAction('frontend/element_ready/grandrestaurant-portfolio-3d-overlay.default', function ($scope) {
      jQuery(".portfolio-grid-overlay-container").each(function () {
        var containderDiv = jQuery(this);
        var selectedClass = "";
        containderDiv.find(".portfolio-grid-wrapper-overlay").imagesLoaded().always(function () {
          var currentWrapper = containderDiv.find(".portfolio-grid-wrapper-overlay");
          var currentImg = containderDiv.find(".portfolio-grid-wrapper-overlay img");
          currentWrapper.css('height', currentImg.height() + 'px');
        });
        jQuery(window).resize(function () {
          containderDiv.find(".portfolio-grid-wrapper-overlay").each(function () {
            var currentImg = jQuery(this).find("img");
            jQuery(this).css('height', currentImg.height() + 'px');
          });
        });

        if (!is_touch_device()) {
          var scaleTilt = 1.15;

          if (navigator.userAgent.indexOf('Safari') != -1 && navigator.userAgent.indexOf('Chrome') == -1) {
            scaleTilt = 1;
          }

          containderDiv.find(".grid_tilt").tilt({
            scale: scaleTilt,
            perspective: 2000,
            glare: true,
            maxGlare: .5
          });
        }

        if (!elementorFrontend.isEditMode()) {
          containderDiv.find(".portfolio-grid-wrapper-overlay").on('click', function () {
            window.location = jQuery(this).find("a").first().attr("href");
          });
        }

        containderDiv.find(".filter-tag-btn").on('click', function () {
          containderDiv.find(".filter-tag-btn").removeClass("active");
          jQuery(this).addClass("active");
          selectedClass = jQuery(this).attr("data-rel");
          column = jQuery(this).attr("data-cols");
          var gridDiv = containderDiv.find(".portfolio-grid-content-wrapper");
          gridDiv.fadeTo(100, 0);
          gridDiv.find(".portfolio-grid-wrapper-overlay").css({
            opacity: 0,
            display: 'none',
            transform: 'scale(0.0)'
          });
          gridDiv.find(".portfolio-grid-wrapper-overlay").not("." + selectedClass).fadeOut().removeClass('scale-anm');
          setTimeout(function () {
            jQuery("." + selectedClass).fadeIn().addClass('scale-anm');
            jQuery("." + selectedClass).css({
              opacity: 1,
              display: 'block',
              transform: 'scale(1,1)'
            });
            gridDiv.find(".portfolio-grid-wrapper-overlay.last").removeClass('last');
            var count = gridDiv.find(".portfolio-grid-wrapper-overlay.scale-anm").length;
            gridDiv.find(".portfolio-grid-wrapper-overlay.scale-anm").each(function (index) {
              var lastIndex = parseInt(index + 1);

              if (lastIndex % column == 0) {
                jQuery(this).addClass('last');
              }

              if (lastIndex == count) {
                setTimeout(function () {
                  gridDiv.fadeTo(300, 1);
                }, 300);
              }
            });
          }, 300);
        });
      });
    });
    elementorFrontend.hooks.addAction('frontend/element_ready/grandrestaurant-portfolio-classic.default', function ($scope) {
      jQuery(".portfolio-classic-container").each(function () {
        var containderDiv = jQuery(this);
        var selectedClass = "";
        containderDiv.find(".filter-tag-btn").on('click', function () {
          containderDiv.find(".filter-tag-btn").removeClass("active");
          jQuery(this).addClass("active");
          selectedClass = jQuery(this).attr("data-rel");
          column = jQuery(this).attr("data-cols");
          var gridDiv = containderDiv.find(".portfolio-classic-content-wrapper");
          gridDiv.fadeTo(100, 0);
          gridDiv.find(".portfolio-classic-grid-wrapper").css({
            opacity: 0,
            display: 'none',
            transform: 'scale(0.0)'
          });
          gridDiv.find(".portfolio-classic-grid-wrapper").not("." + selectedClass).fadeOut().removeClass('scale-anm');
          setTimeout(function () {
            jQuery("." + selectedClass).addClass('scale-anm');
            jQuery("." + selectedClass).css({
              opacity: 1,
              display: 'block',
              transform: 'scale(1,1)'
            });
            gridDiv.find(".portfolio-classic-grid-wrapper.last").removeClass('last');
            var count = gridDiv.find(".portfolio-classic-grid-wrapper.scale-anm").length;
            gridDiv.find(".portfolio-classic-grid-wrapper.scale-anm").each(function (index) {
              var lastIndex = parseInt(index + 1);

              if (lastIndex % column == 0) {
                jQuery(this).addClass('last');
              }

              if (lastIndex == count) {
                setTimeout(function () {
                  gridDiv.fadeTo(300, 1);
                }, 300);
              }
            });
          }, 300);
        });
      });
    });
    elementorFrontend.hooks.addAction('frontend/element_ready/grandrestaurant-portfolio-contain.default', function ($scope) {
      jQuery(".portfolio-classic-container.contain").each(function () {
        var containderDiv = jQuery(this);
        var selectedClass = "";
        containderDiv.find(".filter-tag-btn").on('click', function () {
          containderDiv.find(".filter-tag-btn").removeClass("active");
          jQuery(this).addClass("active");
          selectedClass = jQuery(this).attr("data-rel");
          column = jQuery(this).attr("data-cols");
          var gridDiv = containderDiv.find(".portfolio-classic-content-wrapper");
          gridDiv.fadeTo(100, 0);
          gridDiv.find(".portfolio-classic-grid-wrapper").css({
            opacity: 0,
            display: 'none',
            transform: 'scale(0.0)'
          });
          gridDiv.find(".portfolio-classic-grid-wrapper").not("." + selectedClass).fadeOut().removeClass('scale-anm');
          setTimeout(function () {
            jQuery("." + selectedClass).addClass('scale-anm');
            jQuery("." + selectedClass).css({
              opacity: 1,
              display: 'block',
              transform: 'scale(1,1)'
            });
            gridDiv.find(".portfolio-classic-grid-wrapper.last").removeClass('last');
            var count = gridDiv.find(".portfolio-classic-grid-wrapper.scale-anm").length;
            gridDiv.find(".portfolio-classic-grid-wrapper.scale-anm").each(function (index) {
              var lastIndex = parseInt(index + 1);

              if (lastIndex % column == 0) {
                jQuery(this).addClass('last');
              }

              if (lastIndex == count) {
                setTimeout(function () {
                  gridDiv.fadeTo(300, 1);
                }, 300);
              }
            });
          }, 300);
        });
      });
    });
    elementorFrontend.hooks.addAction('frontend/element_ready/grandrestaurant-navigation-menu.default', function ($scope) {
      jQuery('.themegoods-navigation-wrapper .nav li.menu-item').hover(function () {
        jQuery(this).children('ul:first').addClass('visible');
        jQuery(this).children('ul:first').addClass('hover');
        jQuery(this).children('.elementor-megamenu-wrapper:first').addClass('visible');
        jQuery(this).children('.elementor-megamenu-wrapper:first').addClass('hover');
      }, function () {
        jQuery(this).children('ul:first').removeClass('visible');
        jQuery(this).children('ul:first').removeClass('hover');
        jQuery(this).children('.elementor-megamenu-wrapper:first').removeClass('visible');
        jQuery(this).children('.elementor-megamenu-wrapper:first').removeClass('hover');
      });
      jQuery('.themegoods-navigation-wrapper .nav li.menu-item').children('ul:first.hover').hover(function () {
        jQuery(this).stop().addClass('visible');
      }, function () {
        jQuery(this).stop().removeClass('visible');
      });
      jQuery('.themegoods-navigation-wrapper .nav li.menu-item').children('.elementor-megamenu-wrapper.hover').hover(function () {
        jQuery(this).stop().addClass('visible');
      }, function () {
        jQuery(this).stop().removeClass('visible');
      });
    });
    elementorFrontend.hooks.addAction('frontend/element_ready/grandrestaurant-video-grid.default', function ($scope) {
      jQuery(".portfolio-classic-container.video-grid").each(function () {
        var containerDiv = jQuery(this);
        var selectedClass = "";
        containerDiv.find(".filter-tag-btn").on('click', function () {
          containerDiv.find(".filter-tag-btn").removeClass("active");
          jQuery(this).addClass("active");
          selectedClass = jQuery(this).attr("data-rel");
          column = jQuery(this).attr("data-cols");
          var gridDiv = containerDiv.find(".portfolio-classic-content-wrapper");
          gridDiv.fadeTo(100, 0);
          gridDiv.find(".portfolio-classic-grid-wrapper").css({
            opacity: 0,
            display: 'none',
            transform: 'scale(0.0)'
          });
          gridDiv.find(".portfolio-classic-grid-wrapper").not("." + selectedClass).fadeOut().removeClass('scale-anm');
          setTimeout(function () {
            jQuery("." + selectedClass).addClass('scale-anm');
            jQuery("." + selectedClass).css({
              opacity: 1,
              display: 'block',
              transform: 'scale(1,1)'
            });
            gridDiv.find(".portfolio-classic-grid-wrapper.last").removeClass('last');
            var count = gridDiv.find(".portfolio-classic-grid-wrapper.scale-anm").length;
            gridDiv.find(".portfolio-classic-grid-wrapper.scale-anm").each(function (index) {
              var lastIndex = parseInt(index + 1);

              if (lastIndex % column == 0) {
                jQuery(this).addClass('last');
              }

              if (lastIndex == count) {
                setTimeout(function () {
                  gridDiv.fadeTo(300, 1);
                }, 300);
              }
            });
          }, 300);
        });
      });
      jQuery(".portfolio-classic-content-wrapper.video-grid .video-grid-wrapper").each(function () {
        var video = this.getElementsByClassName("video-card")[0];
        var id = jQuery(video).data('video-id');
        var play = document.createElement("div");
        var outline = document.createElement("div");
        var ring = document.createElement("div");
        var fill = document.createElement("div");
        var mask = document.createElement("div");
        var wipe = document.createElement("div");
        var wrap = document.createElement("div");
        var iframe = '<iframe width="100%" height="100%" src="https://www.youtube.com/embed/' + id + '" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>';
        TweenMax.set(play, {
          css: {
            width: 0,
            height: 0,
            "border-top": "10px solid transparent",
            "border-bottom": "10px solid transparent",
            "border-left": "14px solid white",
            "z-index": 50,
            position: 'absolute',
            top: "50%",
            left: "50%",
            marginLeft: "-4px",
            marginTop: "-9px"
          }
        });
        TweenMax.set(outline, {
          css: {
            width: 65,
            height: 65,
            border: "3px solid white",
            "border-radius": "999px",
            "z-index": 50,
            position: 'absolute',
            top: "50%",
            left: "50%",
            y: "-50%",
            x: "-50%"
          }
        });
        TweenMax.set(ring, {
          css: {
            width: 65,
            height: 65,
            border: "1px solid white",
            "border-radius": "999px",
            "z-index": 50,
            position: 'absolute',
            top: "50%",
            left: "50%",
            y: "-50%",
            x: "-50%"
          }
        });
        TweenMax.set(fill, {
          css: {
            width: 65,
            opacity: 0,
            height: 65,
            "background-color": "white",
            "border-radius": "999px",
            "z-index": 50,
            position: 'absolute',
            top: "50%",
            left: "50%",
            y: "-50%",
            x: "-50%"
          }
        });
        TweenMax.set(mask, {
          css: {
            width: 600,
            height: 600,
            scale: 1,
            opacity: 0,
            backgroundColor: "#fff",
            "border-radius": "300px",
            "z-index": 51,
            position: 'absolute',
            top: "50%",
            left: "50%",
            y: "-50%",
            x: "-50%"
          }
        });
        TweenMax.set(wipe, {
          css: {
            width: "100%",
            height: "100%",
            opacity: 0,
            backgroundColor: "#fff",
            "z-index": 53,
            position: 'absolute',
            top: "0",
            right: "0"
          }
        });
        TweenMax.set(wrap, {
          css: {
            width: "100%",
            height: "100%",
            autoAlpha: 0,
            "z-index": 52,
            position: 'absolute',
            top: "0",
            left: "0"
          }
        });
        video.appendChild(play);
        video.appendChild(outline);
        video.appendChild(ring);
        video.appendChild(fill);
        video.appendChild(mask);
        video.appendChild(wipe);
        jQuery(wrap).html(iframe);
        video.appendChild(wrap);
        jQuery(video).on("mouseenter", function (event) {
          TweenLite.killTweensOf([fill, outline, ring]);
          TweenLite.to(fill, 0.4, {
            opacity: 0.2,
            ease: "cubic-bezier(0.39, 0.575, 0.565, 1)"
          });
          TweenLite.to(outline, 0.4, {
            scale: 0.9,
            ease: "cubic-bezier(0.39, 0.575, 0.565, 1)"
          });
          TweenLite.fromTo(ring, 0.7, {
            scale: 1,
            opacity: 0.5
          }, {
            scale: 2,
            opacity: 0,
            ease: "cubic-bezier(0.165, 0.84, 0.44, 1)"
          });
          TweenLite.to(play, 0.2, {
            x: 8,
            opacity: 0,
            ease: "cubic-bezier(0.550, 0.055, 0.675, 0.190)",
            onComplete: function onComplete() {
              TweenLite.set(play, {
                x: -12,
                onComplete: function onComplete() {
                  TweenLite.to(play, 0.4, {
                    x: 0,
                    opacity: 1,
                    ease: "cubic-bezier(0.215, 0.610, 0.355, 1.000)"
                  });
                }
              });
            }
          });
        });
        jQuery(video).on("mouseleave", function (event) {
          TweenLite.killTweensOf([fill, outline]);
          TweenLite.to(fill, 0.4, {
            opacity: 0,
            ease: "cubic-bezier(0.39, 0.575, 0.565, 1)"
          });
          TweenLite.to(outline, 0.4, {
            scale: 1,
            ease: "cubic-bezier(0.39, 0.575, 0.565, 1)"
          });
        });
        jQuery(video).on("click", function (event) {
          event.preventDefault();
          TweenLite.fromTo(mask, 0.35, {
            opacity: 1,
            scale: 0
          }, {
            scale: 1,
            ease: "cubic-bezier(0.550, 0.055, 0.675, 0.190)",
            onComplete: function onComplete() {
              TweenLite.set(mask, {
                opacity: 0,
                scale: 0,
                delay: 0.5,
                onComplete: function onComplete() {
                  TweenLite.fromTo(wipe, 0.4, {
                    width: "100%"
                  }, {
                    width: 0,
                    ease: "cubic-bezier(.42,0,.58,1)"
                  });
                }
              });
              TweenLite.set(wipe, {
                opacity: 1
              });
              TweenLite.set(wrap, {
                autoAlpha: 1
              });
            }
          });
        });
      });
    });
    elementorFrontend.hooks.addAction('frontend/element_ready/grandrestaurant-portfolio-coverflow.default', function ($scope) {
      jQuery(".portfolio-coverflow").each(function () {
        var initialSlide = 0;
        initialSlide = parseInt(jQuery(this).attr('data-initial') - 1);
        var coverflowSwiper = new Swiper(jQuery(this), {
          grabCursor: true,
          centeredSlides: true,
          slidesPerView: 'auto',
          spaceBetween: 50,
          keyboard: {
            enabled: true,
            onlyInViewport: false
          },
          pagination: {
            el: '.swiper-pagination'
          },
          initialSlide: initialSlide
        });
      });
    });
    elementorFrontend.hooks.addAction('frontend/element_ready/grandrestaurant-glitch-slideshow.default', function ($scope) {
      var Slide = /*#__PURE__*/function () {
        function Slide(el) {
          _classCallCheck2(this, Slide);

          this.DOM = {
            el: el
          };
          this.DOM.slideImg = this.DOM.el.querySelector('.slide-img');
          this.bgImage = this.DOM.slideImg.style.backgroundImage;
          this.layout();
        }

        _createClass2(Slide, [{
          key: "layout",
          value: function layout() {
            this.DOM.slideImg.innerHTML = "<div class='glitch-img'style='background-image: ".concat(this.DOM.slideImg.style.backgroundImage, ";'></div>").repeat(5);
            this.DOM.glitchImgs = Array.from(this.DOM.slideImg.querySelectorAll('.glitch-img'));
          }
        }, {
          key: "changeBGImage",
          value: function changeBGImage(bgimage) {
            var _this13 = this;

            var pos = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 0;
            var delay = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : 0;
            setTimeout(function () {
              return _this13.DOM.glitchImgs[pos].style.backgroundImage = bgimage;
            }, delay);
          }
        }]);

        return Slide;
      }();

      var GlitchSlideshow = /*#__PURE__*/function () {
        function GlitchSlideshow(el) {
          var _this14 = this;

          _classCallCheck2(this, GlitchSlideshow);

          this.DOM = {
            el: el
          };
          this.DOM.slides = Array.from(this.DOM.el.querySelectorAll('.slide'));
          this.slidesTotal = this.DOM.slides.length;
          this.slides = [];
          this.DOM.slides.forEach(function (slide) {
            return _this14.slides.push(new Slide(slide));
          });
          this.current = 0;
          this.glitchTime = 1800;
          this.totalGlitchSlices = 5;
        }

        _createClass2(GlitchSlideshow, [{
          key: "glitch",
          value: function glitch(slideFrom, slideTo) {
            var _this15 = this;

            return new Promise(function (resolve, reject) {
              slideFrom.DOM.slideImg.classList.add('glitch--animate');
              var slideFromBGImage = slideFrom.bgImage;
              var slideToBGImage = slideTo.bgImage;

              for (var _i = _this15.totalGlitchSlices - 1; _i >= 0; --_i) {
                slideFrom.changeBGImage(slideToBGImage, _i, _this15.glitchTime / (_this15.totalGlitchSlices + 1) * (_this15.totalGlitchSlices - _i - 1) + _this15.glitchTime / (_this15.totalGlitchSlices + 1));
              }

              setTimeout(function () {
                slideFrom.DOM.slideImg.classList.remove('glitch--animate');

                for (var _i2 = _this15.totalGlitchSlices - 1; _i2 >= 0; --_i2) {
                  slideFrom.changeBGImage(slideFromBGImage, _i2, 0);
                }

                resolve();
              }, _this15.glitchTime);
            });
          }
        }, {
          key: "navigate",
          value: function navigate(dir) {
            var _this16 = this;

            if (this.isAnimating) return;
            this.isAnimating = true;
            var newCurrent = dir === 'next' ? this.current < this.slidesTotal - 1 ? this.current + 1 : 0 : this.current > 0 ? this.current - 1 : this.slidesTotal - 1;
            this.glitch(this.slides[this.current], this.slides[newCurrent]).then(function () {
              _this16.DOM.slides[_this16.current].classList.remove('slide-current');

              _this16.current = newCurrent;

              _this16.DOM.slides[_this16.current].classList.add('slide-current');

              _this16.isAnimating = false;
            });
          }
        }]);

        return GlitchSlideshow;
      }();

      imagesLoaded(document.querySelectorAll('.slide-img'), {
        background: true
      }, function () {
        document.body.classList.remove('loading');
        var slideshow = new GlitchSlideshow(document.querySelector('.slides'));
        var nav = Array.from(document.querySelectorAll('.slide-nav-button'));
        nav[0].addEventListener('click', function () {
          return slideshow.navigate('prev');
        });
        nav[1].addEventListener('click', function () {
          return slideshow.navigate('next');
        });
      });
    });
    elementorFrontend.hooks.addAction('frontend/element_ready/grandrestaurant-music-player.default', function ($scope) {
      jQuery(".grandrestaurant-music-player").each(function () {
        var containerDiv = jQuery(this);
        var songIndex = 0;
        var audio = new Audio();
        var $audio = jQuery(audio);
        var handleDrag = false;
        var timeIn = 0;
        var songListID = containerDiv.attr('data-songlist');
        var playlist = JSON.parse(jQuery('#' + songListID).val());
        var $player = containerDiv.find('.player'),
            $poster = containerDiv.find('.player-img'),
            $background = containerDiv.find('.player-background'),
            $title = containerDiv.find('.player-title'),
            $artist = containerDiv.find('.player-artist'),
            $prev = containerDiv.find('.player-controls__prev'),
            $next = containerDiv.find('.player-controls__next'),
            $play = containerDiv.find('.player-controls__play'),
            $scrubber = containerDiv.find('.player-scrubber'),
            $handle = containerDiv.find('.player-scrubber-handle'),
            $fill = containerDiv.find('.player-scrubber__fill'),
            $time = containerDiv.find('.player-time__played'),
            $dur = containerDiv.find('.player-time__duration');

        function time(seconds) {
          seconds = Math.floor(seconds);
          var mins = Math.floor(seconds / 60);
          var secs = seconds % 60;
          return mins + ":" + (secs < 10 ? "0" : "") + secs;
        }

        function changeSong(offset, play) {
          songIndex += offset;
          if (songIndex > playlist.length - 1) songIndex = 0;
          if (songIndex < 0) songIndex = playlist.length - 1;
          $audio.attr('src', playlist[songIndex].mp3);
          var src = playlist[songIndex].poster;
          $title.text(playlist[songIndex].title);
          $artist.text(playlist[songIndex].artist);
          $dur.text("-:--");
          $audio.on('loadedmetadata', function () {
            $dur.text(time(audio.duration));
            setHandle(0, audio.duration);
          });
          $audio.on('ended', function () {
            return changeSong(+1, true);
          });
          var img = new Image();
          jQuery(img).attr("src", src).on("load", function () {
            $poster.attr('src', src);
            $background.css('background-image', 'url("' + src + '")');
          }).on("error", function () {
            console.log("Cannot find image");
          });

          if (play) {
            audio.play();
            $play.find('i').removeClass('fa-play').addClass('fa-pause');
          } else {
            $play.find('i').removeClass('fa-pause').addClass('fa-play');
          }
        }

        function setHandle(seconds, duration) {
          var percent = seconds / duration * 100;
          $time.text(time(seconds));
          $handle.css('left', percent + "%");
          $fill.css('width', percent + "%");
        }

        $play.click(function () {
          if (audio.currentTime > 0 && !audio.paused) {
            audio.pause();
            $play.find('i').removeClass('fa-pause').addClass('fa-play');
          } else {
            audio.play();
            $play.find('i').removeClass('fa-play').addClass('fa-pause');
          }
        });
        $prev.click(function () {
          return changeSong(-1, true);
        });
        $next.click(function () {
          return changeSong(+1, true);
        });
        $audio.on("timeupdate", function () {
          setHandle(audio.currentTime, audio.duration);
        });
        $handle.on('mousedown', function () {
          handleDrag = true;
          audio.pause();
        });
        $player.on('mousemove', function (e) {
          if (handleDrag) {
            var width = $scrubber.width();
            var diff = $scrubber.offset().left - e.clientX;
            var percent = -diff / width * 100;
            percent = percent < 0 ? 0 : percent > 100 ? 100 : percent;
            var seconds = percent / 100 * audio.duration;
            setHandle(seconds, audio.duration);
            timeIn = seconds;
          }
        }).on('mouseup mouseleave', function () {
          if (handleDrag) {
            handleDrag = false;
            audio.currentTime = timeIn;
            $play.find('i').removeClass('fa-pause').addClass('fa-play');
          }
        });
        changeSong(0, false);
      });
    });
    elementorFrontend.hooks.addAction('frontend/element_ready/grandrestaurant-mouse-driven-vertical-carousel.default', function ($scope) {
      var VerticalMouseDrivenCarousel = /*#__PURE__*/function () {
        function VerticalMouseDrivenCarousel() {
          var options = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};

          _classCallCheck2(this, VerticalMouseDrivenCarousel);

          var _defaults = {
            carousel: ".mouse-driven-vertical-carousel-wrapper .js-carousel",
            bgImg: ".js-carousel-bg-img",
            list: ".js-carousel-list",
            listItem: ".js-carousel-list-item"
          };
          this.posY = 0;
          this.defaults = Object.assign({}, _defaults, options);
          this.initCursor();
          this.init();
          this.bgImgController();
        }

        _createClass2(VerticalMouseDrivenCarousel, [{
          key: "getBgImgs",
          value: function getBgImgs() {
            return document.querySelectorAll(this.defaults.bgImg);
          }
        }, {
          key: "getListItems",
          value: function getListItems() {
            return document.querySelectorAll(this.defaults.listItem);
          }
        }, {
          key: "getList",
          value: function getList() {
            return document.querySelector(this.defaults.list);
          }
        }, {
          key: "getCarousel",
          value: function getCarousel() {
            return document.querySelector(this.defaults.carousel);
          }
        }, {
          key: "init",
          value: function init() {
            TweenMax.set(this.getBgImgs(), {
              autoAlpha: 0,
              scale: 1.05
            });
            TweenMax.set(this.getBgImgs()[0], {
              autoAlpha: 1,
              scale: 1
            });
            this.listItems = this.getListItems().length - 1;
            this.listOpacityController(0);
          }
        }, {
          key: "initCursor",
          value: function initCursor() {
            var _this17 = this;

            if (jQuery(window).width() > 1024) {
              var listHeight = this.getList().clientHeight;
              var carouselHeight = this.getCarousel().clientHeight;
              var carouselPos = this.getCarousel().getBoundingClientRect();
              var carouselPosY = parseInt(carouselPos.top);
              this.getCarousel().addEventListener("mousemove", function (event) {
                _this17.posY = parseInt(event.pageY - carouselPosY) - _this17.getCarousel().offsetTop;
                var offset = -_this17.posY / carouselHeight * listHeight;
                TweenMax.to(_this17.getList(), 0.3, {
                  y: offset,
                  ease: Power4.easeOut
                });
              }, false);
            }
          }
        }, {
          key: "bgImgController",
          value: function bgImgController() {
            var _this18 = this;

            var _iterator = _createForOfIteratorHelper(this.getListItems()),
                _step;

            try {
              for (_iterator.s(); !(_step = _iterator.n()).done;) {
                var link = _step.value;
                link.addEventListener("mouseenter", function (ev) {
                  var currentId = ev.currentTarget.dataset.itemId;

                  _this18.listOpacityController(currentId);

                  TweenMax.to(ev.currentTarget, 0.3, {
                    autoAlpha: 1
                  });
                  TweenMax.to(".is-visible", 0.2, {
                    autoAlpha: 0,
                    scale: 1.05
                  });

                  if (!_this18.getBgImgs()[currentId].classList.contains("is-visible")) {
                    _this18.getBgImgs()[currentId].classList.add("is-visible");
                  }

                  TweenMax.to(_this18.getBgImgs()[currentId], 0.6, {
                    autoAlpha: 1,
                    scale: 1
                  });
                });
              }
            } catch (err) {
              _iterator.e(err);
            } finally {
              _iterator.f();
            }
          }
        }, {
          key: "listOpacityController",
          value: function listOpacityController(id) {
            id = parseInt(id);
            var aboveCurrent = this.listItems - id;
            var belowCurrent = parseInt(id);

            if (aboveCurrent > 0) {
              for (var _i3 = 1; _i3 <= aboveCurrent; _i3++) {
                var opacity = 0.5 / _i3;
                var offset = 5 * _i3;
                TweenMax.to(this.getListItems()[id + _i3], 0.5, {
                  autoAlpha: opacity,
                  x: offset,
                  ease: Power3.easeOut
                });
              }
            }

            if (belowCurrent > 0) {
              for (var _i4 = 0; _i4 <= belowCurrent; _i4++) {
                var _opacity = 0.5 / _i4;

                var _offset = 5 * _i4;

                TweenMax.to(this.getListItems()[id - _i4], 0.5, {
                  autoAlpha: _opacity,
                  x: _offset,
                  ease: Power3.easeOut
                });
              }
            }
          }
        }]);

        return VerticalMouseDrivenCarousel;
      }();

      new VerticalMouseDrivenCarousel();
    });
    elementorFrontend.hooks.addAction('frontend/element_ready/grandrestaurant-slider-synchronized-carousel.default', function ($scope) {
      jQuery(".synchronized-carousel-slider-wrapper").each(function () {
        var sliderID = jQuery(this).attr('id');
        var slidersContainer = document.querySelector("#" + sliderID);
        var countSlide = jQuery(this).attr('data-countslide');
        var msNumbers = new MomentumSlider({
          el: slidersContainer,
          cssClass: "ms--numbers",
          range: [1, countSlide],
          rangeContent: function rangeContent(i) {
            return "0" + i;
          },
          style: {
            transform: [{
              scale: [0.4, 1]
            }],
            opacity: [0, 1]
          },
          interactive: false
        });
        var titles = JSON.parse(jQuery(this).attr('data-slidetitles'));
        var msTitles = new MomentumSlider({
          el: slidersContainer,
          cssClass: "ms--titles",
          range: [0, parseInt(countSlide - 1)],
          rangeContent: function rangeContent(i) {
            return "<h3>" + titles[i] + "</h3>";
          },
          vertical: true,
          reverse: true,
          style: {
            opacity: [0, 1]
          },
          interactive: false
        });
        var buttonTitles = JSON.parse(jQuery(this).attr('data-slidebuttontitles'));
        var buttonUrls = JSON.parse(jQuery(this).attr('data-slidebuttonurls'));
        var msLinks = new MomentumSlider({
          el: slidersContainer,
          cssClass: "ms--links",
          range: [0, parseInt(countSlide - 1)],
          rangeContent: function rangeContent(i) {
            return "<a href=\"" + buttonUrls[i] + "\" class=\"ms-slide-link\">" + buttonTitles[i] + "</a>";
          },
          vertical: true,
          interactive: false
        });
        var paginationID = jQuery(this).attr('data-pagination');
        var pagination = document.querySelector("#" + paginationID);
        var paginationItems = [].slice.call(pagination.children);
        var images = JSON.parse(jQuery(this).attr('data-slideimages'));
        var msImages = new MomentumSlider({
          el: slidersContainer,
          cssClass: "ms--images",
          range: [0, parseInt(countSlide - 1)],
          rangeContent: function rangeContent(i) {
            return "<div class=\"ms-slide-image-container\"><div class=\"ms-slide-image\" style=\"background-image: url('" + images[i] + "')\"></div></div>";
          },
          sync: [msNumbers, msTitles, msLinks],
          style: {
            ".ms-slide-image": {
              transform: [{
                scale: [1.5, 1]
              }]
            }
          },
          change: function change(newIndex, oldIndex) {
            if (typeof oldIndex !== "undefined") {
              paginationItems[oldIndex].classList.remove("pagination-item--active");
            }

            paginationItems[newIndex].classList.add("pagination-item--active");
          }
        });
        pagination.addEventListener("click", function (e) {
          if (e.target.matches(".pagination-button")) {
            var index = paginationItems.indexOf(e.target.parentNode);
            msImages.select(index);
          }
        });
      });
    });
    elementorFrontend.hooks.addAction('frontend/element_ready/grandrestaurant-flip-box.default', function ($scope) {
      var countSquare = jQuery('.square').length;

      for (i = 0; i < countSquare; i++) {
        var firstImage = jQuery('.square').eq([i]);
        var secondImage = jQuery('.square2').eq([i]);
        var getImage = firstImage.attr('data-image');
        var getImage2 = secondImage.attr('data-image');
        firstImage.css('background-image', 'url(' + getImage + ')');
        secondImage.css('background-image', 'url(' + getImage2 + ')');
      }

      jQuery('.flip-box-wrapper').on('click', function () {
        jQuery(this).trigger("mouseover");
      });
    });
    elementorFrontend.hooks.addAction('frontend/element_ready/grandrestaurant-slider-zoom.default', function ($scope) {
      var _createClass = function () {
        function defineProperties(target, props) {
          for (var i = 0; i < props.length; i++) {
            var descriptor = props[i];
            descriptor.enumerable = descriptor.enumerable || false;
            descriptor.configurable = true;
            if ("value" in descriptor) descriptor.writable = true;
            Object.defineProperty(target, descriptor.key, descriptor);
          }
        }

        return function (Constructor, protoProps, staticProps) {
          if (protoProps) defineProperties(Constructor.prototype, protoProps);
          if (staticProps) defineProperties(Constructor, staticProps);
          return Constructor;
        };
      }();

      function _classCallCheck(instance, Constructor) {
        if (!(instance instanceof Constructor)) {
          throw new TypeError("Cannot call a class as a function");
        }
      }

      var $window = $(window);
      var $body = $('body');

      var Slideshow = function () {
        function Slideshow() {
          var _this = this;

          var userOptions = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};

          _classCallCheck(this, Slideshow);

          var timer = $('.slider-zoom-wrapper').attr('data-autoplay');
          var autoplay = true;

          if (timer == 0) {
            timer = false;
            autoplay = false;
          }

          var pagination = $('.slider-zoom-wrapper').attr('data-pagination');

          if (pagination == 0) {
            var pagination = false;
          } else {
            var pagination = true;
          }

          var defaultOptions = {
            $el: $('.slider-zoom-wrapper'),
            showArrows: false,
            showpagination: false,
            duration: timer,
            autoplay: autoplay
          };
          var options = Object.assign({}, defaultOptions, userOptions);
          this.$el = options.$el;
          this.maxSlide = this.$el.find($('.js-slider-home-slide')).length;
          this.showArrows = this.maxSlide > 1 ? options.showArrows : false;
          this.showpagination = pagination;
          this.currentSlide = 1;
          this.isAnimating = false;
          this.animationDuration = 1200;
          this.autoplaySpeed = options.duration;
          this.interval;
          this.$controls = this.$el.find('.js-slider-home-button');
          this.autoplay = this.maxSlide > 1 ? options.autoplay : false;
          this.$el.on('click', '.js-slider-home-next', function (event) {
            return _this.nextSlide();
          });
          this.$el.on('click', '.js-slider-home-prev', function (event) {
            return _this.prevSlide();
          });
          this.$el.on('click', '.js-pagination-item', function (event) {
            if (!_this.isAnimating) {
              _this.preventClick();

              _this.goToSlide(event.target.dataset.slide);
            }
          });
          this.init();
        }

        _createClass(Slideshow, [{
          key: 'init',
          value: function init() {
            this.goToSlide(1);

            if (this.autoplay) {
              this.startAutoplay();
            }

            if (this.showpagination) {
              var paginationNumber = this.maxSlide;
              var pagination = '<div class="pagination"><div class="container">';

              for (var i = 0; i < this.maxSlide; i++) {
                var item = '<span class="pagination-item js-pagination-item ' + (i === 0 ? 'is-current' : '') + '" data-slide=' + (i + 1) + '>' + (i + 1) + '</span>';
                pagination = pagination + item;
              }

              pagination = pagination + '</div></div>';
              this.$el.append(pagination);
            }
          }
        }, {
          key: 'preventClick',
          value: function preventClick() {
            var _this2 = this;

            this.isAnimating = true;
            this.$controls.prop('disabled', true);
            clearInterval(this.interval);
            setTimeout(function () {
              _this2.isAnimating = false;

              _this2.$controls.prop('disabled', false);

              if (_this2.autoplay) {
                _this2.startAutoplay();
              }
            }, this.animationDuration);
          }
        }, {
          key: 'goToSlide',
          value: function goToSlide(index) {
            this.currentSlide = parseInt(index);

            if (this.currentSlide > this.maxSlide) {
              this.currentSlide = 1;
            }

            if (this.currentSlide === 0) {
              this.currentSlide = this.maxSlide;
            }

            var newCurrent = this.$el.find('.js-slider-home-slide[data-slide="' + this.currentSlide + '"]');
            var newprev = this.currentSlide === 1 ? this.$el.find('.js-slider-home-slide').last() : newCurrent.prev('.js-slider-home-slide');
            var newNext = this.currentSlide === this.maxSlide ? this.$el.find('.js-slider-home-slide').first() : newCurrent.next('.js-slider-home-slide');
            this.$el.find('.js-slider-home-slide').removeClass('is-prev is-next is-current');
            this.$el.find('.js-pagination-item').removeClass('is-current');

            if (this.maxSlide > 1) {
              newprev.addClass('is-prev');
              newNext.addClass('is-next');
            }

            newCurrent.addClass('is-current');
            this.$el.find('.js-pagination-item[data-slide="' + this.currentSlide + '"]').addClass('is-current');
          }
        }, {
          key: 'nextSlide',
          value: function nextSlide() {
            this.preventClick();
            this.goToSlide(this.currentSlide + 1);
          }
        }, {
          key: 'prevSlide',
          value: function prevSlide() {
            this.preventClick();
            this.goToSlide(this.currentSlide - 1);
          }
        }, {
          key: 'startAutoplay',
          value: function startAutoplay() {
            var _this3 = this;

            this.interval = setInterval(function () {
              if (!_this3.isAnimating) {
                _this3.nextSlide();
              }
            }, this.autoplaySpeed);
          }
        }, {
          key: 'destroy',
          value: function destroy() {
            this.$el.off();
          }
        }]);

        return Slideshow;
      }();

      (function () {
        var loaded = false;
        var maxLoad = 3000;

        function load() {
          var options = {
            showpagination: true
          };
          var slideShow = new Slideshow(options);
        }

        function addLoadClass() {
          $body.addClass('is-loaded');
          setTimeout(function () {
            $body.addClass('is-animated');
          }, 600);
        }

        $window.on('load', function () {
          if (!loaded) {
            loaded = true;
            load();
          }
        });
        setTimeout(function () {
          if (!loaded) {
            loaded = true;
            load();
          }
        }, maxLoad);
        addLoadClass();
      })();
    });
    elementorFrontend.hooks.addAction('frontend/element_ready/grandrestaurant-course-grid.default', function ($scope) {
      jQuery('.course_tooltip').tooltipster({
        position: 'right',
        multiple: true,
        contentCloning: true,
        theme: 'tooltipster-shadow',
        minWidth: 300,
        maxWidth: 300,
        delay: 50,
        interactive: true
      });
    });
    elementorFrontend.hooks.addAction('frontend/element_ready/grandrestaurant-search.default', function ($scope) {
      jQuery(".grandrestaurant-search-icon").each(function () {
        var iconInput = jQuery(this).find('a');
        iconInput.bind('click', function () {
          var openDiv = jQuery(this).attr('data-open');
          jQuery('#' + openDiv).addClass('active');
          var isTouch = ('ontouchstart' in document.documentElement);

          if (isTouch) {
            jQuery('#' + openDiv).find('.grandrestaurant-search-inner').addClass('touch');
          }

          jQuery('body').addClass('elementor-no-overflow');
          jQuery('body').addClass('elementor-search-activate');
          setTimeout(function () {
            jQuery('#' + openDiv).find('.tg_search_form.autocomplete_form').find('input#s').trigger('focus');
          }, 300);
        });
      });
      jQuery(".tg_search_form.autocomplete_form").each(function () {
        var wrapper = jQuery(this).attr('data-open');
        var formInput = jQuery(this).find('input[name="s"]');
        var resultDiv = jQuery(this).attr('data-result');
        var isTouch = ('ontouchstart' in document.documentElement);

        if (!isTouch) {
          formInput.on('input', function () {
            if (jQuery(this).val().length > 1) {
              jQuery.ajax({
                url: tgAjax.ajaxurl,
                type: 'POST',
                data: 'action=grandrestaurant_ajax_search_result&' + jQuery(this).serialize(),
                success: function success(results) {
                  jQuery("#" + resultDiv).html(results);

                  if (results != '') {
                    jQuery("#" + resultDiv).addClass('visible');
                    jQuery("#" + resultDiv).show();
                    jQuery("#" + resultDiv).css('z-index', 99);
                    jQuery("#" + resultDiv + " ul li a").mousedown(function () {
                      jQuery("#" + resultDiv).addClass('visible');
                      jQuery("#" + resultDiv).attr('data-mousedown', 'true');
                      location.href = jQuery(this).attr('href');
                    });
                  } else {
                    jQuery("#" + resultDiv).hide();
                  }
                }
              });
            } else {
              jQuery("#" + resultDiv).html('');
            }
          });
        }

        formInput.bind('click', function () {
          jQuery("#" + resultDiv).addClass('visible');
          jQuery("#" + resultDiv).attr('data-mousedown', 'true');
        });
        formInput.bind('focus', function () {
          jQuery("#" + resultDiv).addClass('visible');
          formInput.addClass('visible');
        });
        formInput.bind('blur', function () {
          jQuery("#" + resultDiv).removeClass('visible');
          formInput.removeClass('visible');
        });
        jQuery("#" + wrapper).bind('click', function () {
          if (!formInput.hasClass('visible')) {
            if (jQuery("#" + resultDiv).attr('data-mousedown') != 'true') {
              jQuery("#" + resultDiv).removeClass('visible');
            }

            jQuery('#' + wrapper).removeClass('active');
            jQuery('body').removeClass('elementor-no-overflow');
            jQuery('body').removeClass('elementor-search-activate');
          }
        });
      });
    });
    elementorFrontend.hooks.addAction('frontend/element_ready/grandrestaurant-service-grid.default', function ($scope) {
      jQuery(".service-grid-wrapper").mouseover(function () {
        var dataHoverY = jQuery(this).attr('data-hoverY');
        jQuery(this).find('.header-wrap').css('transform', 'translateY(-' + dataHoverY + 'px)');
      }).mouseleave(function () {
        jQuery(this).find('.header-wrap').css('transform', 'translateY(0px)');
      });
      jQuery(".service-grid-wrapper").each(function () {
        var hoverContent = jQuery(this).find('.hover-content');
        var hoverMoveY = parseInt(hoverContent.height() + 10);
        jQuery(this).attr('data-hoverY', hoverMoveY);
      });
      jQuery(window).resize(function () {
        jQuery(".service-grid-wrapper").each(function () {
          var hoverContent = jQuery(this).find('.hover-content');
          var hoverMoveY = parseInt(hoverContent.height() + 10);
          jQuery(this).attr('data-hoverY', hoverMoveY);
        });
      });
    });
    elementorFrontend.hooks.addAction('frontend/element_ready/grandrestaurant-pricing-table.default', function ($scope) {
      var elems = Array.prototype.slice.call(document.querySelectorAll('.js-switch'));
      elems.forEach(function (elem) {
        var switchery = new Switchery(elem, {
          disabled: true,
          disabledOpacity: 1,
          color: elem.getAttribute('data-switch-bg'),
          secondaryColor: elem.getAttribute('data-switch-bg'),
          jackColor: elem.getAttribute('data-switch-button'),
          jackSecondaryColor: elem.getAttribute('data-switch-button')
        });
      });
      var changeCheckbox = document.querySelector('.js-switch');

      if (!!changeCheckbox) {
        changeCheckbox.onchange = function () {
          jQuery(".pricing-plan-price").each(function () {
            if (changeCheckbox.checked) {
              var pricingPlan = jQuery(this).attr('data-price-year');
              jQuery(this).html(pricingPlan);
              jQuery('.pricing-plan-unit-month').addClass('hide');
              jQuery('.pricing-plan-unit-year').removeClass('hide');
            } else {
              var pricingPlan = jQuery(this).attr('data-price-month');
              jQuery(this).html(pricingPlan);
              jQuery('.pricing-plan-unit-year').addClass('hide');
              jQuery('.pricing-plan-unit-month').removeClass('hide');
            }
          });
        };
      }
    });
    elementorFrontend.hooks.addAction('frontend/element_ready/grandrestaurant-food-menu.default', function ($scope) {
      jQuery('.food-tooltip').tooltipster({
        position: 'right',
        multiple: true,
        contentCloning: true,
        theme: 'tooltipster-shadow',
        minWidth: 300,
        maxWidth: 300,
        delay: 50,
        interactive: true
      });
    });
    elementorFrontend.hooks.addAction('frontend/element_ready/grandrestaurant-search-form.default', function ($scope) {
      jQuery(".tg-search-form.autocomplete-form").each(function () {
        var formInput = jQuery(this).find('input[name="s"]');
        var resultDiv = jQuery(this).attr('data-result');
        formInput.on('input', function () {
          if (jQuery(this).val().length > 1) {
            jQuery.ajax({
              url: tgAjax.ajaxurl,
              type: 'POST',
              data: 'action=grandrestaurant_ajax_search_result&' + jQuery(this).serialize(),
              success: function success(results) {
                jQuery("#" + resultDiv).html(results);

                if (results != '') {
                  jQuery("#" + resultDiv).addClass('visible');
                  jQuery("#" + resultDiv).show();
                  jQuery("#" + resultDiv).css('z-index', 99);
                  jQuery("#" + resultDiv + " ul li a").mousedown(function () {
                    jQuery("#" + resultDiv).attr('data-mousedown', true);
                  });
                } else {
                  jQuery("#" + resultDiv).hide();
                }
              }
            });
          }
        });
        formInput.bind('focus', function () {
          jQuery("#" + resultDiv).addClass('visible');
        });
        formInput.bind('blur', function () {
          if (jQuery("#" + resultDiv).attr('data-mousedown') != 'true') {
            jQuery("#" + resultDiv).removeClass('visible');
          }
        });
      });
    });
    elementorFrontend.hooks.addAction('frontend/element_ready/grandrestaurant-classic-food-menu-slider.default', function ($scope) {
      if (elementorFrontend.isEditMode()) {
        jQuery('.slider_wrapper').flexslider({
          animation: "fade",
          animationLoop: true,
          itemMargin: 0,
          minItems: 1,
          maxItems: 1,
          slideshow: 0,
          controlNav: false,
          smoothHeight: false,
          slideshowSpeed: 5000,
          move: 1
        });
        jQuery('.slider_wrapper.portfolio .slides li').each(function () {
          var height = jQuery(this).height();
          var imageHeight = jQuery(this).find('img').height();
          var offset = (height - imageHeight) / 2;
          jQuery(this).find('img').css('margin-top', offset + 'px');
        });
      }
    });
    elementorFrontend.hooks.addAction('frontend/element_ready/grandrestaurant-animated-text.default', function ($scope) {
      jQuery(".themegoods-animated-text").each(function () {
        var textContent = jQuery(this).first();
        var delimiterTypeOri = jQuery(this).attr('data-delimiter');
        var delimiterType = jQuery(this).attr('data-delimiter');
        var transitionSpeed = parseInt(jQuery(this).attr('data-transition'));
        var transitionDelay = parseInt(jQuery(this).attr('data-transition-delay'));
        var transitionDuration = parseInt(jQuery(this).attr('data-transition-duration'));

        if (delimiterType == 'sentence') {
          delimiterType = 'word';
        }

        var animatedText = textContent.blast({
          delimiter: delimiterType,
          aria: false
        });

        if (jQuery(this).hasClass('overflow-hidden')) {
          animatedText.each(function (i) {
            var textEachSpan = jQuery(this);
            var initialText = textEachSpan.text();
          });
        }

        if (textContent.isInViewport()) {
          animatedText.each(function (i) {
            var delaySpeed = parseInt(transitionDelay + i * transitionSpeed);

            if (delimiterTypeOri == 'sentence') {
              delaySpeed = parseInt(transitionDelay + transitionSpeed);
            }

            jQuery(this).queue(function (next) {
              jQuery(this).css({
                'transition-delay': delaySpeed + 'ms',
                'transition-duration': transitionDuration + 'ms',
                'transform': 'translateX(0px) translateY(0px) translateZ(0px)',
                'opacity': 1
              });
            });
          });
        }

        jQuery(window).on('resize scroll', function () {
          if (textContent.isInViewport()) {
            animatedText.each(function (i) {
              var delaySpeed = parseInt(transitionDelay + i * transitionSpeed);

              if (delimiterTypeOri == 'sentence') {
                delaySpeed = parseInt(transitionDelay + transitionSpeed);
              }

              jQuery(this).queue(function (next) {
                jQuery(this).css({
                  'transition-delay': delaySpeed + 'ms',
                  'transition-duration': transitionDuration + 'ms',
                  'transform': 'translateX(0px) translateY(0px) translateZ(0px)',
                  'opacity': 1
                });
              });
            });
          }
        });
      });
    });
    elementorFrontend.hooks.addAction('frontend/element_ready/grandrestaurant-animated-headline.default', function ($scope) {
      jQuery(".themegoods-animated-headline").each(function () {
        var animationType = jQuery(this).attr('data-animation');
        jQuery(this).animatedHeadline({
          animationType: animationType
        });
      });
    });
    elementorFrontend.hooks.addAction('frontend/element_ready/grandrestaurant-testimonial-carousel.default', function ($scope) {
      jQuery(".testimonials-carousel-wrapper .owl-carousel").each(function () {
        var autoPlay = jQuery(this).attr('data-autoplay');

        if (typeof autoPlay == "undefined") {
          autoPlay = false;
        }

        if (autoPlay == 1) {
          autoPlay = true;
        } else {
          autoPlay = false;
        }

        var timer = jQuery(this).attr('data-timer');

        if (typeof timer == "undefined") {
          timer = 8000;
        }

        var pagination = jQuery(this).attr('data-pagination');

        if (typeof pagination == "undefined") {
          pagination = true;
        }

        if (pagination == 1) {
          pagination = true;
        } else {
          pagination = false;
        }

        jQuery(this).owlCarousel({
          loop: true,
          center: true,
          items: 3,
          margin: 0,
          autoplay: autoPlay,
          dots: pagination,
          autoplayTimeout: timer,
          smartSpeed: 450,
          responsive: {
            0: {
              items: 1
            },
            768: {
              items: 2
            },
            1170: {
              items: 3
            }
          }
        });
      });
    });
  });
})(jQuery);

/***/ })

}]);
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vLi9yZXNvdXJjZXMvanMvZnJvbnRlbmQvMDFmMjMuanMiXSwibmFtZXMiOlsiaXNUb3VjaERldmljZSIsIndpbmRvdyIsImlzTW9iaWxlRGV2aWNlIiwib3JpZW50YXRpb24iLCJuYXZpZ2F0b3IiLCJ1c2VyQWdlbnQiLCJpbmRleE9mIiwialF1ZXJ5IiwiZm4iLCJpc0luVmlld3BvcnQiLCJlbGVtZW50VG9wIiwib2Zmc2V0IiwidG9wIiwiZWxlbWVudEJvdHRvbSIsIm91dGVySGVpZ2h0Iiwidmlld3BvcnRUb3AiLCJzY3JvbGxUb3AiLCJ2aWV3cG9ydEJvdHRvbSIsImhlaWdodCIsIiQiLCJvbiIsImlzVG91Y2giLCJkb2N1bWVudCIsImRvY3VtZW50RWxlbWVudCIsImNzcyIsImVhY2giLCJjdXJyZW50SW1nIiwiTGF6eSIsIm9uRmluaXNoZWRBbGwiLCJwYXJlbnQiLCJyZW1vdmVDbGFzcyIsInNtb292ZSIsImJvZHlCR0NvbG9yIiwicGFyc2VJbnQiLCJ3aWR0aCIsInJlc2l6ZSIsImN1cnJlbnRTZWN0aW9uIiwiZWxlbWVudG9yRnJvbnRlbmQiLCJob29rcyIsImFkZEFjdGlvbiIsIiRzY29wZSIsImlzRWRpdE1vZGUiLCJlbGVtZW50U2V0dGluZ3MiLCJtb2RlbENJRCIsImRhdGEiLCJzZXR0aW5ncyIsImNvbmZpZyIsImVsZW1lbnRzIiwidHlwZSIsImF0dHJpYnV0ZXMiLCJ3aWRnZXRUeXBlIiwiZWxUeXBlIiwic2V0dGluZ3NLZXlzIiwia2V5cyIsImNvbnRyb2xzIiwibmFtZSIsImNvbnRyb2wiLCJmcm9udGVuZF9hdmFpbGFibGUiLCJwdXNoIiwiZ2V0QWN0aXZlQ29udHJvbHMiLCJjb250cm9sS2V5Iiwid2lkZ2V0RXh0Iiwid2lkZ2V0RXh0T2JqIiwiYXR0ciIsIkpTT04iLCJwYXJzZSIsImdyYW5kcmVzdGF1cmFudF9leHRfaXNfYmFja2dyb3VuZF9wYXJhbGxheCIsImJhY2tncm91bmRfYmFja2dyb3VuZCIsIndpZGdldEJnIiwicmVwbGFjZSIsImphcmFsbGF4U2Nyb2xsU3BlZWQiLCJncmFuZHJlc3RhdXJhbnRfZXh0X2lzX2JhY2tncm91bmRfcGFyYWxsYXhfc3BlZWQiLCJzaXplIiwicGFyc2VGbG9hdCIsImFkZENsYXNzIiwiYXBwZW5kIiwiamFyYWxsYXgiLCJzcGVlZCIsImdyYW5kcmVzdGF1cmFudF9leHRfaXNfZmFkZW91dF9hbmltYXRpb24iLCJzY3JvbGxWZWxvY2l0eSIsImdyYW5kcmVzdGF1cmFudF9leHRfaXNfZmFkZW91dF9hbmltYXRpb25fdmVsb2NpdHkiLCJzY3JvbGxEaXJlY3Rpb24iLCJncmFuZHJlc3RhdXJhbnRfZXh0X2lzX2ZhZGVvdXRfYW5pbWF0aW9uX2RpcmVjdGlvbiIsInNjcm9sbCIsImkiLCJzY3JvbGxWYXIiLCJzY3JvbGxQeCIsImZpbmQiLCJncmFuZHJlc3RhdXJhbnRfZXh0X2lzX3Njcm9sbG1lIiwic2Nyb2xsQXJncyIsImdyYW5kcmVzdGF1cmFudF9leHRfc2Nyb2xsbWVfc2NhbGV4IiwiZ3JhbmRyZXN0YXVyYW50X2V4dF9zY3JvbGxtZV9zY2FsZXkiLCJncmFuZHJlc3RhdXJhbnRfZXh0X3Njcm9sbG1lX3NjYWxleiIsImdyYW5kcmVzdGF1cmFudF9leHRfc2Nyb2xsbWVfcm90YXRleCIsImdyYW5kcmVzdGF1cmFudF9leHRfc2Nyb2xsbWVfcm90YXRleSIsImdyYW5kcmVzdGF1cmFudF9leHRfc2Nyb2xsbWVfcm90YXRleiIsImdyYW5kcmVzdGF1cmFudF9leHRfc2Nyb2xsbWVfdHJhbnNsYXRleCIsImdyYW5kcmVzdGF1cmFudF9leHRfc2Nyb2xsbWVfdHJhbnNsYXRleSIsImdyYW5kcmVzdGF1cmFudF9leHRfc2Nyb2xsbWVfdHJhbnNsYXRleiIsImdyYW5kcmVzdGF1cmFudF9leHRfc2Nyb2xsbWVfc21vb3RobmVzcyIsInN0cmluZ2lmeSIsImdyYW5kcmVzdGF1cmFudF9leHRfc2Nyb2xsbWVfZGlzYWJsZSIsImdyYW5kcmVzdGF1cmFudF9leHRfaXNfc21vb3ZlIiwibWluX3dpZHRoIiwiZ3JhbmRyZXN0YXVyYW50X2V4dF9zbW9vdmVfZGlzYWJsZSIsInNjYWxlWCIsImdyYW5kcmVzdGF1cmFudF9leHRfc21vb3ZlX3NjYWxleCIsInNjYWxlWSIsImdyYW5kcmVzdGF1cmFudF9leHRfc21vb3ZlX3NjYWxleSIsInJvdGF0ZVgiLCJncmFuZHJlc3RhdXJhbnRfZXh0X3Ntb292ZV9yb3RhdGV4Iiwicm90YXRlWSIsImdyYW5kcmVzdGF1cmFudF9leHRfc21vb3ZlX3JvdGF0ZXkiLCJyb3RhdGVaIiwiZ3JhbmRyZXN0YXVyYW50X2V4dF9zbW9vdmVfcm90YXRleiIsIm1vdmVYIiwiZ3JhbmRyZXN0YXVyYW50X2V4dF9zbW9vdmVfdHJhbnNsYXRleCIsIm1vdmVZIiwiZ3JhbmRyZXN0YXVyYW50X2V4dF9zbW9vdmVfdHJhbnNsYXRleSIsIm1vdmVaIiwiZ3JhbmRyZXN0YXVyYW50X2V4dF9zbW9vdmVfdHJhbnNsYXRleiIsInNrZXdYIiwiZ3JhbmRyZXN0YXVyYW50X2V4dF9zbW9vdmVfc2tld3giLCJza2V3WSIsImdyYW5kcmVzdGF1cmFudF9leHRfc21vb3ZlX3NrZXd5IiwicGVyc3BlY3RpdmUiLCJncmFuZHJlc3RhdXJhbnRfZXh0X3Ntb292ZV9wZXJzcGVjdGl2ZSIsImdyYW5kcmVzdGF1cmFudF9leHRfc21vb3ZlX2R1cmF0aW9uIiwiaGFzQ2xhc3MiLCJncmFuZHJlc3RhdXJhbnRfZXh0X2lzX3BhcmFsbGF4X21vdXNlIiwiZWxlbWVudElEIiwiZ3JhbmRyZXN0YXVyYW50X2V4dF9pc19wYXJhbGxheF9tb3VzZV9kZXB0aCIsInBhcmVudEVsZW1lbnQiLCJnZXRFbGVtZW50QnlJZCIsInBhcmFsbGF4IiwiUGFyYWxsYXgiLCJyZWxhdGl2ZUlucHV0IiwiZ3JhbmRyZXN0YXVyYW50X2V4dF9pc19pbmZpbml0ZSIsImFuaW1hdGlvbkNsYXNzIiwia2V5ZnJhbWVOYW1lIiwiYW5pbWF0aW9uQ1NTIiwiZ3JhbmRyZXN0YXVyYW50X2V4dF9pbmZpbml0ZV9hbmltYXRpb24iLCJncmFuZHJlc3RhdXJhbnRfZXh0X2luZmluaXRlX2R1cmF0aW9uIiwiZ3JhbmRyZXN0YXVyYW50X2V4dF9pbmZpbml0ZV9lYXNpbmciLCJncmFuZHJlc3RhdXJhbnRfZXh0X2lzX2JhY2tncm91bmRfb25fc2Nyb2xsIiwiYm9keUJhY2tncm91bmQiLCJwb3NpdGlvbiIsImNsZWFyVGltZW91dCIsInNldFRpbWVvdXQiLCJ3aW5kb3dIZWlnaHQiLCJ3aW5kb3dIZWlnaHRPZmZzZXQiLCJncmFuZHJlc3RhdXJhbnRfZXh0X2JhY2tncm91bmRfb25fc2Nyb2xsX2NvbG9yIiwic3RvcCIsImdyYW5kcmVzdGF1cmFudF9leHRfbGlua19yZXNlcnZhdGlvbiIsImUiLCJwcmV2ZW50RGVmYXVsdCIsImZhZGVJbiIsImdyYW5kcmVzdGF1cmFudF9leHRfbGlua19zaWRlbWVudSIsImFuaW1hdGUiLCJ0b2dnbGVDbGFzcyIsImdyaWQiLCJpbWFnZXNMb2FkZWQiLCJkb25lIiwibWFzb25yeSIsIml0ZW1TZWxlY3RvciIsImNvbHVtbldpZHRoIiwiZ3V0dGVyIiwiaW5kZXgiLCJlcSIsIm1lbnVMYXlvdXQiLCJ2YWwiLCJzdGlja19pbl9wYXJlbnQiLCJvZmZzZXRfdG9wIiwidHJpZ2dlciIsImNvbHMiLCJyb3dIZWlnaHQiLCJtYXJnaW4iLCJqdXN0aWZ5TGFzdFJvdyIsImp1c3RpZnlMYXN0Um93U3RyIiwianVzdGlmaWVkR2FsbGVyeSIsIm1hcmdpbnMiLCJsYXN0Um93Iiwic2xpZGVzaG93IiwiYXV0b1BsYXkiLCJhdXRvUGxheUFyciIsImRlbGF5IiwiZWZmZWN0IiwiZ2FsbGVyeVRvcCIsIlN3aXBlciIsIm5hdmlnYXRpb24iLCJuZXh0RWwiLCJwcmV2RWwiLCJzcGFjZUJldHdlZW4iLCJrZXlib2FyZENvbnRyb2wiLCJsb29wIiwiZ3JhYkN1cnNvciIsInByZWxvYWRJbWFnZXMiLCJsYXp5IiwibG9hZFByZXZOZXh0IiwiYXV0b3BsYXkiLCJ0aWNraW5nIiwiaXNGaXJlZm94IiwidGVzdCIsImlzSWUiLCJzY3JvbGxTZW5zaXRpdml0eVNldHRpbmciLCJzbGlkZUR1cmF0aW9uU2V0dGluZyIsImN1cnJlbnRTbGlkZU51bWJlciIsInRvdGFsU2xpZGVOdW1iZXIiLCJsZW5ndGgiLCJwYXJhbGxheFNjcm9sbCIsImV2dCIsImRlbHRhIiwiZGV0YWlsIiwiZGVsdGFZIiwid2hlZWxEZWx0YSIsIm5leHRJdGVtIiwic2xpZGVEdXJhdGlvblRpbWVvdXQiLCJwcmV2aW91c0l0ZW0iLCJzbGlkZUR1cmF0aW9uIiwibW91c2V3aGVlbEV2ZW50IiwiYWRkRXZlbnRMaXN0ZW5lciIsIiRwcmV2aW91c1NsaWRlIiwiY3VycmVudFNsaWRlVHJhbnNpdGlvbiIsIiRjdXJyZW50U2xpZGUiLCJzdG9wUHJvcGFnYXRpb24iLCJ0cyIsImJpbmQiLCJvcmlnaW5hbEV2ZW50IiwidG91Y2hlcyIsImNsaWVudFkiLCJ0ZSIsImNoYW5nZWRUb3VjaGVzIiwiJGNhcm91c2VsIiwidGltZXIiLCJwYWdpbmF0aW9uIiwiZmxpY2tpdHkiLCJwZXJjZW50UG9zaXRpb24iLCJzZWxlY3RlZEF0dHJhY3Rpb24iLCJmcmljdGlvbiIsImxhenlMb2FkIiwicGF1c2VBdXRvUGxheU9uSG92ZXIiLCJjb250YWluIiwicHJldk5leHRCdXR0b25zIiwicGFnZURvdHMiLCIkaW1ncyIsImRvY1N0eWxlIiwic3R5bGUiLCJ0cmFuc2Zvcm1Qcm9wIiwidHJhbnNmb3JtIiwiZmxrdHkiLCJzbGlkZXMiLCJmb3JFYWNoIiwic2xpZGUiLCJpbWciLCJ4IiwidGFyZ2V0IiwiZnVsbHNjcmVlbiIsIm1lbnVIZWlnaHQiLCJkb2N1bWVudEhlaWdodCIsImlubmVySGVpZ2h0Iiwic2xpZGVySGVpZ2h0IiwiZGVib3VuY2UiLCJmdW5jIiwid2FpdCIsImltbWVkaWF0ZSIsInRpbWVvdXQiLCJjb250ZXh0IiwiYXJncyIsImFyZ3VtZW50cyIsImxhdGVyIiwiYXBwbHkiLCJjYWxsTm93IiwiU2xpZGVzaG93IiwiZWwiLCJET00iLCJhbmltYXRpb24iLCJkdXJhdGlvbiIsImVhc2luZyIsInNoYXBlIiwib3V0IiwiZnJhbWVGaWxsIiwic2xpZGVzaG93RnJhbWVDb2xvciIsImluaXQiLCJBcnJheSIsImZyb20iLCJxdWVyeVNlbGVjdG9yQWxsIiwic2xpZGVzVG90YWwiLCJuYXYiLCJxdWVyeVNlbGVjdG9yIiwibmV4dEN0cmwiLCJwcmV2Q3RybCIsImN1cnJlbnQiLCJjcmVhdGVGcmFtZSIsImluaXRFdmVudHMiLCJyZWN0IiwiZ2V0Qm91bmRpbmdDbGllbnRSZWN0IiwiZnJhbWVTaXplIiwicGF0aHMiLCJpbml0aWFsIiwiY2FsY3VsYXRlUGF0aCIsInN2ZyIsImNyZWF0ZUVsZW1lbnROUyIsInNldEF0dHJpYnV0ZSIsImlubmVySFRNTCIsImluc2VydEJlZm9yZSIsImlzQW5pbWF0aW5nIiwicGF0aCIsIm5hdmlnYXRlIiwidXBkYXRlRnJhbWUiLCJldiIsImtleUNvZGUiLCJ3aGljaCIsImRpciIsImFuaW1hdGVTaGFwZUluIiwiYW5pbWUiLCJ0YXJnZXRzIiwiZCIsImFuaW1hdGVTbGlkZXMiLCJQcm9taXNlIiwicmVzb2x2ZSIsInJlamVjdCIsImN1cnJlbnRTbGlkZSIsInRyYW5zbGF0ZVgiLCJjb21wbGV0ZSIsImNsYXNzTGlzdCIsInJlbW92ZSIsIm5ld1NsaWRlIiwiYWRkIiwibmV3U2xpZGVJbWciLCJ0Iiwib3BhY2l0eSIsImFuaW1hdGVTaGFwZU91dCIsImZpbmlzaGVkIiwidGhlbiIsImdldEF0dHJpYnV0ZSIsImJhY2tncm91bmQiLCJnZXRNb3VzZVBvcyIsInBvc3giLCJwb3N5IiwiZXZlbnQiLCJwYWdlWCIsInBhZ2VZIiwiY2xpZW50WCIsImJvZHkiLCJzY3JvbGxMZWZ0IiwieSIsImxvYWRlciIsInNjcm9sbGVyIiwicm9vbXMiLCJzbGljZSIsImNhbGwiLCJjb250ZW50IiwibGVmdEN0cmwiLCJyaWdodEN0cmwiLCJjdXJyZW50Um9vbSIsInRvdGFsUm9vbXMiLCJpbml0VHJhbnNmb3JtIiwidHJhbnNsYXRlWSIsInRyYW5zbGF0ZVoiLCJyZXNldFRyYW5zZm9ybSIsIm1lbnVUcmFuc2Zvcm0iLCJpbmZvVHJhbnNmb3JtIiwiaW5pdFRyYW5zaXRpb24iLCJyb29tVHJhbnNpdGlvbiIsIm1lbnVUcmFuc2l0aW9uIiwiaW5mb1RyYW5zaXRpb24iLCJ0aWx0VHJhbnNpdGlvbiIsInRpbHQiLCJ0aWx0Um90YXRpb24iLCJvbkVuZFRyYW5zaXRpb24iLCJjYWxsYmFjayIsIm9uRW5kQ2FsbGJhY2tGbiIsInJlbW92ZUV2ZW50TGlzdGVuZXIiLCJ3aW4iLCJpbm5lcldpZHRoIiwiaXNNb3ZpbmciLCJpc05hdmlnYXRpbmciLCJtb3ZlIiwidHJhbnNpdGlvbiIsImluaXRUaWx0Iiwic2hvd1NsaWRlIiwiYXBwbHlSb29tVHJhbnNpdGlvbiIsInJlbW92ZVRpbHQiLCJvcHRzIiwic3RvcFRyYW5zaXRpb24iLCJhcHBseVJvb21UcmFuc2Zvcm0iLCJvbkVuZEZuIiwib25Nb3VzZU1vdmVGbiIsInJlcXVlc3RBbmltYXRpb25GcmFtZSIsIm1vdXNlcG9zIiwicm90WCIsInJvdFkiLCJkZWJvdW5jZVJlc2l6ZUZuIiwib25OYXZpZ2F0ZVByZXZGbiIsIm9uTmF2aWdhdGVOZXh0Rm4iLCJ0b2dnbGVTbGlkZSIsInRpdGxlIiwiZGF0ZSIsInVuZGVmaW5lZCIsImFuaW1lT3B0cyIsImMiLCJ2YWx1ZSIsImJlZ2luIiwiaGlkZVNsaWRlIiwicm9vbSIsIm5leHRSb29tIiwiYWRkQWRqYWNlbnRSb29tcyIsInByZXZSb29tIiwicmVtb3ZlQWRqYWNlbnRSb29tcyIsImV4dHJhZGVsYXkiLCJjbGFzc05hbWUiLCJNTFNsaWRlc2hvdyIsIm5leHQiLCJwcmV2Iiwic2NhbGVEb3duQW1udCIsImJveFNoYWRvd0FtbnQiLCJWZWxvY2l0eSIsIlJlZ2lzdGVyRWZmZWN0IiwiZGVmYXVsdER1cmF0aW9uIiwiY2FsbHMiLCJzY2FsZSIsIlZlbG9TbGlkZXIiLCJ2ZWxvSW5pdCIsIiR2ZWxvU2xpZGUiLCJ2ZWxvU2xpZGVCZyIsIm5hdlByZXYiLCJuYXZOZXh0IiwidmVsb0J0biIsInNjcm9sbFRocmVzaG9sZCIsImFuaW1hdGluZyIsImFuaW1hdGlvbkR1cmF0aW9uIiwiZmlyc3QiLCJpbml0U2Nyb2xsSmFjayIsInNjcm9sbEphY2tpbmciLCJwcmV2U2xpZGUiLCJuZXh0U2xpZGUiLCJrZXlOZXh0Iiwia2V5UHJldiIsImNoZWNrTmF2aWdhdGlvbiIsImhvdmVyQW5pbWF0aW9uIiwiaG92ZXIiLCJjbG9zZXN0Iiwic2V0QW5pbWF0aW9uIiwibWlkU3RlcCIsImRpcmVjdGlvbiIsImFuaW1hdGlvblZpc2libGUiLCJhbmltYXRpb25Ub3AiLCJhbmltYXRpb25Cb3R0b20iLCJhbmltRHVyYXRpb24iLCJ2aXNpYmxlU2xpZGUiLCJmaWx0ZXIiLCJ0b3BTZWN0aW9uIiwicHJldkFsbCIsImJvdHRvbVNlY3Rpb24iLCJuZXh0QWxsIiwiYW5pbWF0aW9uUGFyYW1zIiwiY2hpbGRyZW4iLCJ2ZWxvY2l0eSIsIk1hdGgiLCJhYnMiLCJpcyIsImVuZCIsInJlc2V0U2Nyb2xsIiwiZ29Ub1NsaWRlIiwibnVtYmVyIiwiTnVtYmVyIiwidG90YWxTbGlkZXMiLCIkc2xpZGVzIiwiJGNvbnRyb2xzIiwibnVtT2ZTbGlkZXMiLCJzbGlkaW5nQVQiLCJzbGlkaW5nQmxvY2tlZCIsIiRlbCIsImRhdGFzZXQiLCJjb250cm9sQ2xpY2tIYW5kbGVyIiwiJGNvbnRyb2wiLCJpc1JpZ2h0IiwiY29udGFpbnMiLCIkY3VyQWN0aXZlIiwiJG5ld0FjdGl2ZSIsInByZXZJbmRleCIsIiRzbGlkZXIiLCIkc2xpY2tUcmFjayIsIiRzbGlja0N1cnJlbnQiLCJzbGljayIsIlR3ZWVuTWF4IiwidG8iLCJtYXJnaW5MZWZ0IiwiekluZGV4IiwiJHNsaWRlclBhZ2luYXRpb24iLCJkb3RzVmFyIiwiJHNsaWRlck5hdmlnYXRpb24iLCJhcnJvd3NWYXIiLCIkc2xpZGVyQXV0b1BsYXkiLCJhdXRvUGxheVZhciIsImF1dG9QbGF5VGltZVZhciIsInRvdWNoTW92ZSIsImRvdHMiLCJhcnJvd3MiLCJ3YWl0Rm9yQW5pbWF0ZSIsInVzZVRyYW5zZm9ybSIsImF1dG9wbGF5U3BlZWQiLCJjc3NFYXNlIiwiZWFzZSIsIlF1YWQiLCJlYXNlT3V0IiwiU2luZSIsImVhc2VJbk91dCIsIm1heEl0ZW1zIiwiZHJhZ2dpbmciLCJ0cmFja2luZyIsInJpZ2h0VHJhY2tpbmciLCIkc2xpZGVyUmlnaHQiLCJjbG9uZSIsImFwcGVuZFRvIiwicmlnaHRJdGVtcyIsInRvQXJyYXkiLCJyZXZlcnNlSXRlbXMiLCJyZXZlcnNlIiwiaHRtbCIsInZlcnRpY2FsIiwidmVydGljYWxTd2lwaW5nIiwiaW5maW5pdGUiLCJkZWx0YVgiLCJzcGxpdCIsIm5ld1RyYWNraW5nIiwiZGlmZlRyYWNraW5nIiwic3dpcGUiLCJpbml0aWFsU2xpZGUiLCJteVN3aXBlciIsImNsaWNrYWJsZSIsImtleWJvYXJkIiwiZW5hYmxlZCIsIm9ubHlJblZpZXdwb3J0IiwicGFnaW5hdGlvbkNsaWNrYWJsZSIsIm1vdXNld2hlZWwiLCJpbnZlcnQiLCJzbGlkZXIiLCJzaWJsaW5ncyIsIlNsaWNlU2xpZGVyIiwiY3VycmVudFNsaWRlSW5kZXgiLCJudW1TbGlkZXMiLCJzIiwiYmluZEV2ZW50cyIsImtleXVwIiwiaGFuZGxlU2Nyb2xsIiwiR2FsbGVyeSIsInNjcm9sbFRpbWVJZCIsInBvc0xlZnQiLCJsaXN0IiwiaXRlbXMiLCJpdGVtV2lkdGgiLCJvdXRlcldpZHRoIiwicHJvdG90eXBlIiwiY29uc3RydWN0b3IiLCJzZXRHYWxsZXJ5V2lkdGgiLCJldmVudE1hbmFnZXIiLCJfdGhpcyIsIm9uU2Nyb2xsRXZlbnRIYW5kbGVyIiwiZ2V0R2FsbGVyeVdpZHRoIiwiaXRlbSIsInJlYWR5IiwiZ2FsbGVyeSIsImFjdGl2ZUluZGV4IiwibGltaXQiLCJkaXNhYmxlZCIsIiRzdGFnZSIsImNhbnZhcyIsIlNQSU5fRk9SV0FSRF9DTEFTUyIsIlNQSU5fQkFDS1dBUkRfQ0xBU1MiLCJESVNBQkxFX1RSQU5TSVRJT05TX0NMQVNTIiwiU1BJTl9EVVIiLCJhcHBlbmRDb250cm9scyIsImxhc3QiLCJzZXRJbmRleGVzIiwiZHVwbGljYXRlU3Bpbm5lciIsInBhaW50RmFjZXMiLCJjb2xvciIsImdldEJhc2U2NFBpeGVsQnlDb2xvciIsImhleCIsImNyZWF0ZUVsZW1lbnQiLCJnZXRDb250ZXh0IiwiY3R4IiwiZmlsbFN0eWxlIiwiZmlsbFJlY3QiLCJ0b0RhdGFVUkwiLCJwcmVwYXJlRG9tIiwic3BpbiIsImluYyIsIiRhY3RpdmVFbHMiLCIkbmV4dEVscyIsInNwaW5DYWxsYmFjayIsInByZXBlbmRUbyIsImF0dGFjaExpc3RlbmVycyIsIm9ua2V5dXAiLCJ0b0luZGV4IiwiYXNzaWduRWxzIiwidGltZWxpbmVzIiwiZXZlbnRzTWluRGlzdGFuY2UiLCJpbml0VGltZWxpbmUiLCJ0aW1lbGluZSIsInRpbWVsaW5lQ29tcG9uZW50cyIsInBhcnNlRGF0ZSIsIm1pbkxhcHNlIiwic2V0RGF0ZVBvc2l0aW9uIiwidGltZWxpbmVUb3RXaWR0aCIsInNldFRpbWVsaW5lV2lkdGgiLCJ1cGRhdGVTbGlkZSIsInVwZGF0ZU9sZGVyRXZlbnRzIiwidXBkYXRlRmlsbGluZyIsInVwZGF0ZVZpc2libGVDb250ZW50IiwibXEiLCJjaGVja01RIiwic2hvd05ld0NvbnRlbnQiLCJlbGVtZW50SW5WaWV3cG9ydCIsImdldCIsInN0cmluZyIsInRyYW5zbGF0ZVZhbHVlIiwiZ2V0VHJhbnNsYXRlVmFsdWUiLCJ3cmFwcGVyV2lkdGgiLCJ0cmFuc2xhdGVUaW1lbGluZSIsInZpc2libGVDb250ZW50IiwibmV3Q29udGVudCIsInNlbGVjdGVkRGF0ZSIsIm5ld0V2ZW50IiwidXBkYXRlVGltZWxpbmVQb3NpdGlvbiIsImV2ZW50U3R5bGUiLCJnZXRDb21wdXRlZFN0eWxlIiwiZXZlbnRMZWZ0IiwiZ2V0UHJvcGVydHlWYWx1ZSIsInRpbWVsaW5lV2lkdGgiLCJ0aW1lbGluZVRyYW5zbGF0ZSIsInRvdFdpZHRoIiwiZXZlbnRzV3JhcHBlciIsInNldFRyYW5zZm9ybVZhbHVlIiwic2VsZWN0ZWRFdmVudCIsImZpbGxpbmciLCJldmVudFdpZHRoIiwic2NhbGVWYWx1ZSIsIm1pbiIsImRpc3RhbmNlIiwiZGF5ZGlmZiIsImRpc3RhbmNlTm9ybSIsInJvdW5kIiwidGltZVNwYW4iLCJ0aW1lU3Bhbk5vcm0iLCJ0b3RhbFdpZHRoIiwiZXZlbnRzQ29udGVudCIsImV2ZW50RGF0ZSIsInNlbGVjdGVkQ29udGVudCIsInNlbGVjdGVkQ29udGVudEhlaWdodCIsImNsYXNzRW5ldGVyaW5nIiwiY2xhc3NMZWF2aW5nIiwib25lIiwidGltZWxpbmVTdHlsZSIsImVsZW1lbnQiLCJwcm9wZXJ0eSIsImV2ZW50cyIsImRhdGVBcnJheXMiLCJkYXRlQ29tcCIsIm5ld0RhdGUiLCJEYXRlIiwicGFyc2VEYXRlMiIsInNpbmdsZURhdGUiLCJkYXlDb21wIiwidGltZUNvbXAiLCJzZWNvbmQiLCJkYXRlcyIsImRhdGVEaXN0YW5jZXMiLCJvZmZzZXRUb3AiLCJsZWZ0Iiwib2Zmc2V0TGVmdCIsIm9mZnNldFdpZHRoIiwib2Zmc2V0SGVpZ2h0Iiwib2Zmc2V0UGFyZW50IiwicGFnZVlPZmZzZXQiLCJwYWdlWE9mZnNldCIsInBhcmVudERpdiIsImNvbnRhaW5kZXJEaXYiLCJzZWxlY3RlZENsYXNzIiwiZ3JpZERpdiIsImZhZGVUbyIsImRpc3BsYXkiLCJub3QiLCJmYWRlT3V0IiwiJGdyaWQiLCJ0aW1lbGluZVN3aXBlciIsInJlbmRlckJ1bGxldCIsInllYXIiLCJicmVha3BvaW50cyIsInF1ZXVlIiwic2xpZGVzaG93RHVyYXRpb24iLCJzbGlkZXNob3dTd2l0Y2giLCJhdXRvIiwicGFnZXMiLCJhY3RpdmVTbGlkZSIsImFjdGl2ZVNsaWRlSW1hZ2UiLCJuZXdTbGlkZUltYWdlIiwibmV3U2xpZGVDb250ZW50IiwibmV3U2xpZGVFbGVtZW50cyIsImFscGhhIiwib25Db21wbGV0ZSIsInNsaWRlc2hvd05leHQiLCJuZXdTbGlkZVJpZ2h0IiwibmV3U2xpZGVMZWZ0IiwibmV3U2xpZGVJbWFnZVJpZ2h0IiwibmV3U2xpZGVJbWFnZUxlZnQiLCJuZXdTbGlkZUltYWdlVG9SaWdodCIsIm5ld1NsaWRlSW1hZ2VUb0xlZnQiLCJuZXdTbGlkZUNvbnRlbnRMZWZ0IiwibmV3U2xpZGVDb250ZW50UmlnaHQiLCJhY3RpdmVTbGlkZUltYWdlTGVmdCIsInJpZ2h0Iiwic2V0IiwiZm9yY2UzRCIsIlBvd2VyMyIsInN0YWdnZXJGcm9tVG8iLCJwcmV2aW91cyIsImhvbWVTbGlkZXNob3dwYXJhbGxheCIsImlubmVyIiwibmV3SGVpZ2h0IiwibmV3VG9wIiwiaW1ncyIsImhvdmVyRWZmZWN0IiwiaW50ZW5zaXR5Iiwic3BlZWRJbiIsInNwZWVkaW4iLCJzcGVlZE91dCIsInNwZWVkb3V0IiwiaW1hZ2UxIiwiaW1hZ2UyIiwiZGlzcGxhY2VtZW50SW1hZ2UiLCJkaXNwbGFjZW1lbnQiLCJfY3JlYXRlQ2xhc3MiLCJkZWZpbmVQcm9wZXJ0aWVzIiwicHJvcHMiLCJkZXNjcmlwdG9yIiwiZW51bWVyYWJsZSIsImNvbmZpZ3VyYWJsZSIsIndyaXRhYmxlIiwiT2JqZWN0IiwiZGVmaW5lUHJvcGVydHkiLCJrZXkiLCJDb25zdHJ1Y3RvciIsInByb3RvUHJvcHMiLCJzdGF0aWNQcm9wcyIsIl9jbGFzc0NhbGxDaGVjayIsImluc3RhbmNlIiwiVHlwZUVycm9yIiwiJHdpbmRvdyIsIiRib2R5IiwidXNlck9wdGlvbnMiLCJkZWZhdWx0T3B0aW9ucyIsInNob3dBcnJvd3MiLCJzaG93cGFnaW5hdGlvbiIsIm9wdGlvbnMiLCJhc3NpZ24iLCJtYXhTbGlkZSIsImludGVydmFsIiwicHJldmVudENsaWNrIiwic3RhcnRBdXRvcGxheSIsInBhZ2luYXRpb25OdW1iZXIiLCJfdGhpczIiLCJwcm9wIiwiY2xlYXJJbnRlcnZhbCIsIm5ld0N1cnJlbnQiLCJuZXdwcmV2IiwibmV3TmV4dCIsIl90aGlzMyIsInNldEludGVydmFsIiwiZGVzdHJveSIsIm9mZiIsImxvYWRlZCIsIm1heExvYWQiLCJsb2FkIiwic2xpZGVTaG93IiwiYWRkTG9hZENsYXNzIiwiYXV0b1VwZGF0ZSIsInByZXZidG4iLCJuZXh0YnRuIiwiY291bnRlciIsImFwcGVuZENoaWxkIiwibmV4dEN1cnJlbnQiLCJwcmV2Q3VycmVudCIsImZpcnN0Q2hpbGQiLCJ0ZXh0Q29udGVudCIsImhhbmRsZVRvdWNoU3RhcnQiLCJoYW5kbGVUb3VjaE1vdmUiLCJ4RG93biIsInlEb3duIiwieFVwIiwieVVwIiwieERpZmYiLCJ5RGlmZiIsIlNsaWRlIiwiaW1nV3JhcCIsInJldmVhbGVyIiwicHJldmlldyIsIkV4cG8iLCJpc0N1cnJlbnQiLCJ0b2dnbGUiLCJhY3Rpb24iLCJyZXZlYWxlck9wdHMiLCJjb21tb25PcHRzIiwiaW1nT3B0cyIsIm51bWJlck9wdHMiLCJ0aXRsZU9wdHMiLCJzdGFydEF0IiwidG9nZ2xlUHJldmlldyIsInByZXZpZXdDdHJsIiwic2xpZGVFbCIsInNldEN1cnJlbnQiLCJleGl0UHJldmlldyIsImVudGVyUHJldmlldyIsInByb2Nlc3NpbmciLCJoaWRlIiwic2hvd3ByZXZpZXciLCJzaG93IiwiaGlkZVByZXZpZXciLCJ0b2dnbGVOYXZDdHJscyIsImFsbCIsIm9uU3RhcnQiLCJwb2ludGVyRXZlbnRzIiwibmV4dFNsaWRlUG9zIiwib3dsQ2Fyb3VzZWwiLCJjZW50ZXIiLCJhdXRvcGxheVRpbWVvdXQiLCJzbWFydFNwZWVkIiwicmVzcG9uc2l2ZSIsInRvdGFsIiwic2xpZGVPYmoiLCJzZXRTbGlkZSIsImNvbHVtbiIsImNvdW50IiwibGFzdEluZGV4IiwiYWx3YXlzIiwiY3VycmVudFdyYXBwZXIiLCJpc190b3VjaF9kZXZpY2UiLCJzY2FsZVRpbHQiLCJnbGFyZSIsIm1heEdsYXJlIiwibG9jYXRpb24iLCJjb250YWluZXJEaXYiLCJ2aWRlbyIsImdldEVsZW1lbnRzQnlDbGFzc05hbWUiLCJpZCIsInBsYXkiLCJvdXRsaW5lIiwicmluZyIsImZpbGwiLCJtYXNrIiwid2lwZSIsIndyYXAiLCJpZnJhbWUiLCJtYXJnaW5Ub3AiLCJib3JkZXIiLCJiYWNrZ3JvdW5kQ29sb3IiLCJhdXRvQWxwaGEiLCJUd2VlbkxpdGUiLCJraWxsVHdlZW5zT2YiLCJmcm9tVG8iLCJjb3ZlcmZsb3dTd2lwZXIiLCJjZW50ZXJlZFNsaWRlcyIsInNsaWRlc1BlclZpZXciLCJzbGlkZUltZyIsImJnSW1hZ2UiLCJiYWNrZ3JvdW5kSW1hZ2UiLCJsYXlvdXQiLCJyZXBlYXQiLCJnbGl0Y2hJbWdzIiwiYmdpbWFnZSIsInBvcyIsIkdsaXRjaFNsaWRlc2hvdyIsImdsaXRjaFRpbWUiLCJ0b3RhbEdsaXRjaFNsaWNlcyIsInNsaWRlRnJvbSIsInNsaWRlVG8iLCJzbGlkZUZyb21CR0ltYWdlIiwic2xpZGVUb0JHSW1hZ2UiLCJjaGFuZ2VCR0ltYWdlIiwiZ2xpdGNoIiwic29uZ0luZGV4IiwiYXVkaW8iLCJBdWRpbyIsIiRhdWRpbyIsImhhbmRsZURyYWciLCJ0aW1lSW4iLCJzb25nTGlzdElEIiwicGxheWxpc3QiLCIkcGxheWVyIiwiJHBvc3RlciIsIiRiYWNrZ3JvdW5kIiwiJHRpdGxlIiwiJGFydGlzdCIsIiRwcmV2IiwiJG5leHQiLCIkcGxheSIsIiRzY3J1YmJlciIsIiRoYW5kbGUiLCIkZmlsbCIsIiR0aW1lIiwiJGR1ciIsInRpbWUiLCJzZWNvbmRzIiwiZmxvb3IiLCJtaW5zIiwic2VjcyIsImNoYW5nZVNvbmciLCJtcDMiLCJzcmMiLCJwb3N0ZXIiLCJ0ZXh0IiwiYXJ0aXN0Iiwic2V0SGFuZGxlIiwiSW1hZ2UiLCJjb25zb2xlIiwibG9nIiwicGVyY2VudCIsImNsaWNrIiwiY3VycmVudFRpbWUiLCJwYXVzZWQiLCJwYXVzZSIsImRpZmYiLCJWZXJ0aWNhbE1vdXNlRHJpdmVuQ2Fyb3VzZWwiLCJfZGVmYXVsdHMiLCJjYXJvdXNlbCIsImJnSW1nIiwibGlzdEl0ZW0iLCJwb3NZIiwiZGVmYXVsdHMiLCJpbml0Q3Vyc29yIiwiYmdJbWdDb250cm9sbGVyIiwiZ2V0QmdJbWdzIiwibGlzdEl0ZW1zIiwiZ2V0TGlzdEl0ZW1zIiwibGlzdE9wYWNpdHlDb250cm9sbGVyIiwibGlzdEhlaWdodCIsImdldExpc3QiLCJjbGllbnRIZWlnaHQiLCJjYXJvdXNlbEhlaWdodCIsImdldENhcm91c2VsIiwiY2Fyb3VzZWxQb3MiLCJjYXJvdXNlbFBvc1kiLCJQb3dlcjQiLCJsaW5rIiwiY3VycmVudElkIiwiY3VycmVudFRhcmdldCIsIml0ZW1JZCIsImFib3ZlQ3VycmVudCIsImJlbG93Q3VycmVudCIsInNsaWRlcklEIiwic2xpZGVyc0NvbnRhaW5lciIsImNvdW50U2xpZGUiLCJtc051bWJlcnMiLCJNb21lbnR1bVNsaWRlciIsImNzc0NsYXNzIiwicmFuZ2UiLCJyYW5nZUNvbnRlbnQiLCJpbnRlcmFjdGl2ZSIsInRpdGxlcyIsIm1zVGl0bGVzIiwiYnV0dG9uVGl0bGVzIiwiYnV0dG9uVXJscyIsIm1zTGlua3MiLCJwYWdpbmF0aW9uSUQiLCJwYWdpbmF0aW9uSXRlbXMiLCJpbWFnZXMiLCJtc0ltYWdlcyIsInN5bmMiLCJjaGFuZ2UiLCJuZXdJbmRleCIsIm9sZEluZGV4IiwibWF0Y2hlcyIsInBhcmVudE5vZGUiLCJzZWxlY3QiLCJjb3VudFNxdWFyZSIsImZpcnN0SW1hZ2UiLCJzZWNvbmRJbWFnZSIsImdldEltYWdlIiwiZ2V0SW1hZ2UyIiwidG9vbHRpcHN0ZXIiLCJtdWx0aXBsZSIsImNvbnRlbnRDbG9uaW5nIiwidGhlbWUiLCJtaW5XaWR0aCIsIm1heFdpZHRoIiwiaWNvbklucHV0Iiwib3BlbkRpdiIsIndyYXBwZXIiLCJmb3JtSW5wdXQiLCJyZXN1bHREaXYiLCJhamF4IiwidXJsIiwidGdBamF4IiwiYWpheHVybCIsInNlcmlhbGl6ZSIsInN1Y2Nlc3MiLCJyZXN1bHRzIiwibW91c2Vkb3duIiwiaHJlZiIsIm1vdXNlb3ZlciIsImRhdGFIb3ZlclkiLCJtb3VzZWxlYXZlIiwiaG92ZXJDb250ZW50IiwiaG92ZXJNb3ZlWSIsImVsZW1zIiwiZWxlbSIsInN3aXRjaGVyeSIsIlN3aXRjaGVyeSIsImRpc2FibGVkT3BhY2l0eSIsInNlY29uZGFyeUNvbG9yIiwiamFja0NvbG9yIiwiamFja1NlY29uZGFyeUNvbG9yIiwiY2hhbmdlQ2hlY2tib3giLCJvbmNoYW5nZSIsImNoZWNrZWQiLCJwcmljaW5nUGxhbiIsImZsZXhzbGlkZXIiLCJhbmltYXRpb25Mb29wIiwiaXRlbU1hcmdpbiIsIm1pbkl0ZW1zIiwiY29udHJvbE5hdiIsInNtb290aEhlaWdodCIsInNsaWRlc2hvd1NwZWVkIiwiaW1hZ2VIZWlnaHQiLCJkZWxpbWl0ZXJUeXBlT3JpIiwiZGVsaW1pdGVyVHlwZSIsInRyYW5zaXRpb25TcGVlZCIsInRyYW5zaXRpb25EZWxheSIsInRyYW5zaXRpb25EdXJhdGlvbiIsImFuaW1hdGVkVGV4dCIsImJsYXN0IiwiZGVsaW1pdGVyIiwiYXJpYSIsInRleHRFYWNoU3BhbiIsImluaXRpYWxUZXh0IiwiZGVsYXlTcGVlZCIsImFuaW1hdGlvblR5cGUiLCJhbmltYXRlZEhlYWRsaW5lIl0sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFBQSxTQUFTQSxhQUFULEdBQXdCO0FBQUMsU0FBTSxrQkFBaUJDLE1BQWpCLElBQXlCLHVCQUFzQkEsTUFBckQ7QUFBNkQ7O0FBQ3RGLFNBQVNDLGNBQVQsR0FBeUI7QUFBQyxTQUFPLE9BQU9ELE1BQU0sQ0FBQ0UsV0FBZCxLQUE0QixXQUE3QixJQUE0Q0MsU0FBUyxDQUFDQyxTQUFWLENBQW9CQyxPQUFwQixDQUE0QixVQUE1QixNQUEwQyxDQUFDLENBQTdGO0FBQWlHOztBQUFBOztBQUFDQyxNQUFNLENBQUNDLEVBQVAsQ0FBVUMsWUFBVixHQUF1QixZQUFVO0FBQUMsTUFBSUMsVUFBVSxHQUFDSCxNQUFNLENBQUMsSUFBRCxDQUFOLENBQWFJLE1BQWIsR0FBc0JDLEdBQXJDO0FBQXlDLE1BQUlDLGFBQWEsR0FBQ0gsVUFBVSxHQUFDSCxNQUFNLENBQUMsSUFBRCxDQUFOLENBQWFPLFdBQWIsRUFBN0I7QUFBd0QsTUFBSUMsV0FBVyxHQUFDUixNQUFNLENBQUNOLE1BQUQsQ0FBTixDQUFlZSxTQUFmLEVBQWhCO0FBQTJDLE1BQUlDLGNBQWMsR0FBQ0YsV0FBVyxHQUFDUixNQUFNLENBQUNOLE1BQUQsQ0FBTixDQUFlaUIsTUFBZixFQUEvQjtBQUF1RCxTQUFPTCxhQUFhLEdBQUNFLFdBQWQsSUFBMkJMLFVBQVUsR0FBQ08sY0FBN0M7QUFBNkQsQ0FBbFM7O0FBQW1TLENBQUMsVUFBU0UsQ0FBVCxFQUFXO0FBQUNBLEdBQUMsQ0FBQ2xCLE1BQUQsQ0FBRCxDQUFVbUIsRUFBVixDQUFhLHlCQUFiLEVBQXVDLFlBQVU7QUFBQyxRQUFJQyxPQUFPLElBQUUsa0JBQWlCQyxRQUFRLENBQUNDLGVBQTVCLENBQVg7O0FBQXdELFFBQUdGLE9BQUgsRUFBVztBQUFDZCxZQUFNLENBQUMsaUJBQUQsQ0FBTixDQUEwQmlCLEdBQTFCLENBQThCLFlBQTlCLEVBQTJDLFFBQTNDO0FBQXNEOztBQUN4bEJqQixVQUFNLENBQUMsVUFBRCxDQUFOLENBQW1Ca0IsSUFBbkIsQ0FBd0IsWUFBVTtBQUFDLFVBQUlDLFVBQVUsR0FBQ25CLE1BQU0sQ0FBQyxJQUFELENBQXJCO0FBQTRCQSxZQUFNLENBQUMsSUFBRCxDQUFOLENBQWFvQixJQUFiLENBQWtCO0FBQUNDLHFCQUFhLEVBQUMseUJBQVU7QUFBQ0Ysb0JBQVUsQ0FBQ0csTUFBWCxDQUFrQiwrQkFBbEIsRUFBbURDLFdBQW5ELENBQStELE1BQS9EO0FBQXVFSixvQkFBVSxDQUFDRyxNQUFYLENBQWtCLG1DQUFsQixFQUF1REEsTUFBdkQsQ0FBOEQsdUJBQTlELEVBQXVGQyxXQUF2RixDQUFtRyxNQUFuRztBQUE0RztBQUE3TSxPQUFsQjtBQUFtTyxLQUFsUztBQUFvU3ZCLFVBQU0sQ0FBQyx5REFBRCxDQUFOLENBQWtFa0IsSUFBbEUsQ0FBdUUsWUFBVTtBQUFDbEIsWUFBTSxDQUFDLElBQUQsQ0FBTixDQUFhd0IsTUFBYixDQUFvQjtBQUFDcEIsY0FBTSxFQUFDO0FBQVIsT0FBcEI7QUFBcUMsS0FBdkg7QUFBeUgsUUFBSXFCLFdBQVcsR0FBQ3pCLE1BQU0sQ0FBQyxNQUFELENBQU4sQ0FBZWlCLEdBQWYsQ0FBbUIsa0JBQW5CLENBQWhCOztBQUF1RCxRQUFHUSxXQUFXLElBQUUsRUFBaEIsRUFDcGQ7QUFBQ3pCLFlBQU0sQ0FBQyxVQUFELENBQU4sQ0FBbUJpQixHQUFuQixDQUF1QixrQkFBdkIsRUFBMENRLFdBQTFDO0FBQXdEOztBQUN6RCxRQUFHQyxRQUFRLENBQUMxQixNQUFNLENBQUNOLE1BQUQsQ0FBTixDQUFlaUMsS0FBZixFQUFELENBQVIsR0FBaUMsR0FBcEMsRUFDQTtBQUFDM0IsWUFBTSxDQUFDLGtFQUFELENBQU4sQ0FBMkVrQixJQUEzRSxDQUFnRixZQUFVO0FBQUNsQixjQUFNLENBQUMsSUFBRCxDQUFOLENBQWFXLE1BQWIsQ0FBb0IsTUFBcEI7QUFBNkIsT0FBeEg7QUFBMkg7O0FBQzVIWCxVQUFNLENBQUNOLE1BQUQsQ0FBTixDQUFla0MsTUFBZixDQUFzQixZQUFVO0FBQUM1QixZQUFNLENBQUMsa0VBQUQsQ0FBTixDQUEyRWtCLElBQTNFLENBQWdGLFlBQVU7QUFBQyxZQUFJVyxjQUFjLEdBQUM3QixNQUFNLENBQUMsSUFBRCxDQUF6Qjs7QUFBZ0MsWUFBRzBCLFFBQVEsQ0FBQzFCLE1BQU0sQ0FBQ04sTUFBRCxDQUFOLENBQWVpQyxLQUFmLEVBQUQsQ0FBUixHQUFpQyxHQUFwQyxFQUM1SjtBQUFDRSx3QkFBYyxDQUFDbEIsTUFBZixDQUFzQixNQUF0QjtBQUErQixTQUQ0SCxNQUc1SjtBQUFDa0Isd0JBQWMsQ0FBQ2xCLE1BQWYsQ0FBc0IsRUFBdEI7QUFBMkI7QUFBQyxPQUhJO0FBR0QsS0FIaEM7QUFHa0NtQixxQkFBaUIsQ0FBQ0MsS0FBbEIsQ0FBd0JDLFNBQXhCLENBQWtDLCtCQUFsQyxFQUFrRSxVQUFTQyxNQUFULEVBQWdCO0FBQUMsVUFBR0gsaUJBQWlCLENBQUNJLFVBQWxCLEVBQUgsRUFDckg7QUFBQyxZQUFJQyxlQUFlLEdBQUMsRUFBcEI7QUFBdUIsWUFBSUMsUUFBUSxHQUFDSCxNQUFNLENBQUNJLElBQVAsQ0FBWSxXQUFaLENBQWI7QUFBc0MsWUFBSUMsUUFBUSxHQUFDUixpQkFBaUIsQ0FBQ1MsTUFBbEIsQ0FBeUJDLFFBQXpCLENBQWtDSCxJQUFsQyxDQUF1Q0QsUUFBdkMsQ0FBYjs7QUFBOEQsWUFBRyxPQUFPRSxRQUFQLElBQWlCLFdBQXBCLEVBQzVIO0FBQUMsY0FBSUcsSUFBSSxHQUFDSCxRQUFRLENBQUNJLFVBQVQsQ0FBb0JDLFVBQXBCLElBQWdDTCxRQUFRLENBQUNJLFVBQVQsQ0FBb0JFLE1BQTdEO0FBQUEsY0FBb0VDLFlBQVksR0FBQ2YsaUJBQWlCLENBQUNTLE1BQWxCLENBQXlCQyxRQUF6QixDQUFrQ00sSUFBbEMsQ0FBdUNMLElBQXZDLENBQWpGOztBQUE4SCxjQUFHLENBQUNJLFlBQUosRUFBaUI7QUFBQ0Esd0JBQVksR0FBQ2YsaUJBQWlCLENBQUNTLE1BQWxCLENBQXlCQyxRQUF6QixDQUFrQ00sSUFBbEMsQ0FBdUNMLElBQXZDLElBQTZDLEVBQTFEO0FBQTZEekMsa0JBQU0sQ0FBQ2tCLElBQVAsQ0FBWW9CLFFBQVEsQ0FBQ1MsUUFBckIsRUFBOEIsVUFBU0MsSUFBVCxFQUFjQyxPQUFkLEVBQXNCO0FBQUMsa0JBQUdBLE9BQU8sQ0FBQ0Msa0JBQVgsRUFBOEI7QUFBQ0wsNEJBQVksQ0FBQ00sSUFBYixDQUFrQkgsSUFBbEI7QUFBeUI7QUFBQyxhQUE5RztBQUFpSDs7QUFDL1RoRCxnQkFBTSxDQUFDa0IsSUFBUCxDQUFZb0IsUUFBUSxDQUFDYyxpQkFBVCxFQUFaLEVBQXlDLFVBQVNDLFVBQVQsRUFBb0I7QUFBQyxnQkFBRyxDQUFDLENBQUQsS0FBS1IsWUFBWSxDQUFDOUMsT0FBYixDQUFxQnNELFVBQXJCLENBQVIsRUFBeUM7QUFBQ2xCLDZCQUFlLENBQUNrQixVQUFELENBQWYsR0FBNEJmLFFBQVEsQ0FBQ0ksVUFBVCxDQUFvQlcsVUFBcEIsQ0FBNUI7QUFBNkQ7QUFBQyxXQUF0SztBQUF3SyxjQUFJQyxTQUFTLEdBQUNuQixlQUFkO0FBQStCO0FBQUMsT0FIbkYsTUFLckg7QUFBQyxZQUFJb0IsWUFBWSxHQUFDdEIsTUFBTSxDQUFDdUIsSUFBUCxDQUFZLGVBQVosQ0FBakI7O0FBQThDLFlBQUcsT0FBT0QsWUFBUCxJQUFxQixXQUF4QixFQUMvQztBQUFDLGNBQUlELFNBQVMsR0FBQ0csSUFBSSxDQUFDQyxLQUFMLENBQVdILFlBQVgsQ0FBZDtBQUF3QztBQUFDOztBQUMxQyxVQUFHLE9BQU9ELFNBQVAsSUFBa0IsV0FBckIsRUFDQTtBQUFDLFlBQUcsT0FBT0EsU0FBUyxDQUFDSywwQ0FBakIsSUFBNkQsV0FBN0QsSUFBMEVMLFNBQVMsQ0FBQ0ssMENBQVYsSUFBc0QsTUFBbkksRUFDRDtBQUFDLGNBQUcsT0FBT0wsU0FBUyxDQUFDTSxxQkFBakIsSUFBd0MsV0FBeEMsSUFBcUROLFNBQVMsQ0FBQ00scUJBQVYsSUFBaUMsU0FBekYsRUFDRDtBQUFDLGdCQUFHLENBQUM5QixpQkFBaUIsQ0FBQ0ksVUFBbEIsRUFBSixFQUNEO0FBQUMsa0JBQUkyQixRQUFRLEdBQUM1QixNQUFNLENBQUNoQixHQUFQLENBQVcsa0JBQVgsQ0FBYjtBQUE0QzRDLHNCQUFRLEdBQUNBLFFBQVEsQ0FBQ0MsT0FBVCxDQUFpQixNQUFqQixFQUF3QixFQUF4QixFQUE0QkEsT0FBNUIsQ0FBb0MsR0FBcEMsRUFBd0MsRUFBeEMsRUFBNENBLE9BQTVDLENBQW9ELE1BQXBELEVBQTJELEVBQTNELENBQVQ7O0FBQXdFLGtCQUFHRCxRQUFRLElBQUUsRUFBYixFQUNySDtBQUFDLG9CQUFJRSxtQkFBbUIsR0FBQyxHQUF4Qjs7QUFBNEIsb0JBQUcsT0FBT1QsU0FBUyxDQUFDVSxnREFBVixDQUEyREMsSUFBbEUsSUFBd0UsV0FBM0UsRUFDN0I7QUFBQ0YscUNBQW1CLEdBQUNHLFVBQVUsQ0FBQ1osU0FBUyxDQUFDVSxnREFBVixDQUEyREMsSUFBNUQsQ0FBOUI7QUFBaUc7O0FBQ2xHaEMsc0JBQU0sQ0FBQ2tDLFFBQVAsQ0FBZ0IsVUFBaEI7QUFBNEJsQyxzQkFBTSxDQUFDbUMsTUFBUCxDQUFjLG9DQUFrQ1AsUUFBbEMsR0FBMkMsS0FBekQ7QUFBZ0U1QixzQkFBTSxDQUFDb0MsUUFBUCxDQUFnQjtBQUFDQyx1QkFBSyxFQUFDUDtBQUFQLGlCQUFoQjs7QUFBNkMsb0JBQUcsQ0FBQ3BFLGNBQWMsRUFBbEIsRUFDekk7QUFBQ3NDLHdCQUFNLENBQUNoQixHQUFQLENBQVcsa0JBQVgsRUFBOEIsTUFBOUI7QUFBdUM7O0FBQ3hDakIsc0JBQU0sQ0FBQ04sTUFBRCxDQUFOLENBQWVrQyxNQUFmLENBQXNCLFlBQVU7QUFBQyxzQkFBRyxDQUFDakMsY0FBYyxFQUFsQixFQUNqQztBQUFDc0MsMEJBQU0sQ0FBQ2hCLEdBQVAsQ0FBVyxrQkFBWCxFQUE4QixNQUE5QjtBQUF1QyxtQkFEUCxNQUdqQztBQUFDZ0IsMEJBQU0sQ0FBQ2hCLEdBQVAsQ0FBVyxrQkFBWCxFQUE4QixTQUFPNEMsUUFBUCxHQUFnQixHQUE5QztBQUFvRDtBQUFDLGlCQUh0RDtBQUd5RDtBQUFDO0FBQUM7QUFBQzs7QUFDNUQsWUFBRyxPQUFPUCxTQUFTLENBQUNpQix3Q0FBakIsSUFBMkQsV0FBM0QsSUFBd0VqQixTQUFTLENBQUNpQix3Q0FBVixJQUFvRCxNQUEvSCxFQUNBO0FBQUMsY0FBSUMsY0FBYyxHQUFDTixVQUFVLENBQUNaLFNBQVMsQ0FBQ21CLGlEQUFWLENBQTREUixJQUE3RCxDQUE3QjtBQUFnRyxjQUFJUyxlQUFlLEdBQUNwQixTQUFTLENBQUNxQixrREFBOUI7QUFBaUYzRSxnQkFBTSxDQUFDTixNQUFELENBQU4sQ0FBZWtGLE1BQWYsQ0FBc0IsVUFBU0MsQ0FBVCxFQUFXO0FBQUMsZ0JBQUlDLFNBQVMsR0FBQzlFLE1BQU0sQ0FBQ04sTUFBRCxDQUFOLENBQWVlLFNBQWYsRUFBZDtBQUF5QyxnQkFBSXNFLFFBQVEsR0FBQyxFQUFFUCxjQUFjLEdBQUNNLFNBQWpCLENBQWI7O0FBQXlDLGdCQUFHSixlQUFlLElBQUUsSUFBcEIsRUFDdFM7QUFBQ0ssc0JBQVEsR0FBQyxFQUFFUCxjQUFjLEdBQUNNLFNBQWpCLENBQVQ7QUFBc0MsYUFEK1AsTUFFalMsSUFBR0osZUFBZSxJQUFFLE1BQXBCLEVBQ0w7QUFBQ0ssc0JBQVEsR0FBQ1AsY0FBYyxHQUFDTSxTQUF4QjtBQUFtQyxhQUQvQixNQUdMO0FBQUNDLHNCQUFRLEdBQUMsQ0FBVDtBQUFZOztBQUNiOUMsa0JBQU0sQ0FBQytDLElBQVAsQ0FBWSw2QkFBWixFQUEyQy9ELEdBQTNDLENBQStDO0FBQUMsMkJBQVksZ0JBQWM4RCxRQUFkLEdBQXVCO0FBQXBDLGFBQS9DO0FBQTJGOUMsa0JBQU0sQ0FBQytDLElBQVAsQ0FBWSw2QkFBWixFQUEyQy9ELEdBQTNDLENBQStDO0FBQUMseUJBQVUsQ0FBQyxNQUFLNkQsU0FBUyxHQUFDLENBQWhCLElBQW9CO0FBQS9CLGFBQS9DO0FBQXFGLFdBTkU7QUFNQTs7QUFDbEwsWUFBRyxPQUFPeEIsU0FBUyxDQUFDMkIsK0JBQWpCLElBQWtELFdBQWxELElBQStEM0IsU0FBUyxDQUFDMkIsK0JBQVYsSUFBMkMsTUFBN0csRUFDQTtBQUFDLGNBQUlDLFVBQVUsR0FBQyxFQUFmOztBQUFrQixjQUFHLE9BQU81QixTQUFTLENBQUM2QixtQ0FBVixDQUE4Q2xCLElBQXJELElBQTJELFdBQTNELElBQXdFWCxTQUFTLENBQUM2QixtQ0FBVixDQUE4Q2xCLElBQTlDLElBQW9ELENBQS9ILEVBQ25CO0FBQUNpQixzQkFBVSxDQUFDLFFBQUQsQ0FBVixHQUFxQjVCLFNBQVMsQ0FBQzZCLG1DQUFWLENBQThDbEIsSUFBbkU7QUFBeUU7O0FBQzFFLGNBQUcsT0FBT1gsU0FBUyxDQUFDOEIsbUNBQVYsQ0FBOENuQixJQUFyRCxJQUEyRCxXQUEzRCxJQUF3RVgsU0FBUyxDQUFDOEIsbUNBQVYsQ0FBOENuQixJQUE5QyxJQUFvRCxDQUEvSCxFQUNBO0FBQUNpQixzQkFBVSxDQUFDLFFBQUQsQ0FBVixHQUFxQjVCLFNBQVMsQ0FBQzhCLG1DQUFWLENBQThDbkIsSUFBbkU7QUFBeUU7O0FBQzFFLGNBQUcsT0FBT1gsU0FBUyxDQUFDK0IsbUNBQVYsQ0FBOENwQixJQUFyRCxJQUEyRCxXQUEzRCxJQUF3RVgsU0FBUyxDQUFDK0IsbUNBQVYsQ0FBOENwQixJQUE5QyxJQUFvRCxDQUEvSCxFQUNBO0FBQUNpQixzQkFBVSxDQUFDLFFBQUQsQ0FBVixHQUFxQjVCLFNBQVMsQ0FBQytCLG1DQUFWLENBQThDcEIsSUFBbkU7QUFBeUU7O0FBQzFFLGNBQUcsT0FBT1gsU0FBUyxDQUFDZ0Msb0NBQVYsQ0FBK0NyQixJQUF0RCxJQUE0RCxXQUE1RCxJQUF5RVgsU0FBUyxDQUFDZ0Msb0NBQVYsQ0FBK0NyQixJQUEvQyxJQUFxRCxDQUFqSSxFQUNBO0FBQUNpQixzQkFBVSxDQUFDLFNBQUQsQ0FBVixHQUFzQjVCLFNBQVMsQ0FBQ2dDLG9DQUFWLENBQStDckIsSUFBckU7QUFBMkU7O0FBQzVFLGNBQUcsT0FBT1gsU0FBUyxDQUFDaUMsb0NBQVYsQ0FBK0N0QixJQUF0RCxJQUE0RCxXQUE1RCxJQUF5RVgsU0FBUyxDQUFDaUMsb0NBQVYsQ0FBK0N0QixJQUEvQyxJQUFxRCxDQUFqSSxFQUNBO0FBQUNpQixzQkFBVSxDQUFDLFNBQUQsQ0FBVixHQUFzQjVCLFNBQVMsQ0FBQ2lDLG9DQUFWLENBQStDdEIsSUFBckU7QUFBMkU7O0FBQzVFLGNBQUcsT0FBT1gsU0FBUyxDQUFDa0Msb0NBQVYsQ0FBK0N2QixJQUF0RCxJQUE0RCxXQUE1RCxJQUF5RVgsU0FBUyxDQUFDa0Msb0NBQVYsQ0FBK0N2QixJQUEvQyxJQUFxRCxDQUFqSSxFQUNBO0FBQUNpQixzQkFBVSxDQUFDLFNBQUQsQ0FBVixHQUFzQjVCLFNBQVMsQ0FBQ2tDLG9DQUFWLENBQStDdkIsSUFBckU7QUFBMkU7O0FBQzVFLGNBQUcsT0FBT1gsU0FBUyxDQUFDbUMsdUNBQVYsQ0FBa0R4QixJQUF6RCxJQUErRCxXQUEvRCxJQUE0RVgsU0FBUyxDQUFDbUMsdUNBQVYsQ0FBa0R4QixJQUFsRCxJQUF3RCxDQUF2SSxFQUNBO0FBQUNpQixzQkFBVSxDQUFDLEdBQUQsQ0FBVixHQUFnQjVCLFNBQVMsQ0FBQ21DLHVDQUFWLENBQWtEeEIsSUFBbEU7QUFBd0U7O0FBQ3pFLGNBQUcsT0FBT1gsU0FBUyxDQUFDb0MsdUNBQVYsQ0FBa0R6QixJQUF6RCxJQUErRCxXQUEvRCxJQUE0RVgsU0FBUyxDQUFDb0MsdUNBQVYsQ0FBa0R6QixJQUFsRCxJQUF3RCxDQUF2SSxFQUNBO0FBQUNpQixzQkFBVSxDQUFDLEdBQUQsQ0FBVixHQUFnQjVCLFNBQVMsQ0FBQ29DLHVDQUFWLENBQWtEekIsSUFBbEU7QUFBd0U7O0FBQ3pFLGNBQUcsT0FBT1gsU0FBUyxDQUFDcUMsdUNBQVYsQ0FBa0QxQixJQUF6RCxJQUErRCxXQUEvRCxJQUE0RVgsU0FBUyxDQUFDcUMsdUNBQVYsQ0FBa0QxQixJQUFsRCxJQUF3RCxDQUF2SSxFQUNBO0FBQUNpQixzQkFBVSxDQUFDLEdBQUQsQ0FBVixHQUFnQjVCLFNBQVMsQ0FBQ3FDLHVDQUFWLENBQWtEMUIsSUFBbEU7QUFBd0U7O0FBQ3pFLGNBQUcsT0FBT1gsU0FBUyxDQUFDc0MsdUNBQVYsQ0FBa0QzQixJQUF6RCxJQUErRCxXQUFsRSxFQUNBO0FBQUNpQixzQkFBVSxDQUFDLFlBQUQsQ0FBVixHQUF5QjVCLFNBQVMsQ0FBQ3NDLHVDQUFWLENBQWtEM0IsSUFBM0U7QUFBaUY7O0FBQ2xGaEMsZ0JBQU0sQ0FBQ3VCLElBQVAsQ0FBWSxlQUFaLEVBQTRCQyxJQUFJLENBQUNvQyxTQUFMLENBQWVYLFVBQWYsQ0FBNUI7O0FBQXdELGNBQUcsT0FBTzVCLFNBQVMsQ0FBQ3dDLG9DQUFqQixJQUF1RCxXQUExRCxFQUN4RDtBQUFDLGdCQUFHeEMsU0FBUyxDQUFDd0Msb0NBQVYsSUFBZ0QsUUFBbkQsRUFDRDtBQUFDLGtCQUFHcEUsUUFBUSxDQUFDMUIsTUFBTSxDQUFDTixNQUFELENBQU4sQ0FBZWlDLEtBQWYsRUFBRCxDQUFSLEdBQWlDLEdBQXBDLEVBQ0Q7QUFBQ00sc0JBQU0sQ0FBQ2tDLFFBQVAsQ0FBZ0IsYUFBaEI7QUFBZ0M7QUFBQzs7QUFDbEMsZ0JBQUdiLFNBQVMsQ0FBQ3dDLG9DQUFWLElBQWdELFFBQW5ELEVBQ0E7QUFBQyxrQkFBR3BFLFFBQVEsQ0FBQzFCLE1BQU0sQ0FBQ04sTUFBRCxDQUFOLENBQWVpQyxLQUFmLEVBQUQsQ0FBUixHQUFpQyxHQUFwQyxFQUNEO0FBQUNNLHNCQUFNLENBQUNrQyxRQUFQLENBQWdCLGFBQWhCO0FBQWdDO0FBQUM7O0FBQ2xDbkUsa0JBQU0sQ0FBQ04sTUFBRCxDQUFOLENBQWVrQyxNQUFmLENBQXNCLFlBQVU7QUFBQyxrQkFBRzBCLFNBQVMsQ0FBQ3dDLG9DQUFWLElBQWdELFFBQW5ELEVBQ2pDO0FBQUMsb0JBQUduRyxjQUFjLE1BQUkrQixRQUFRLENBQUMxQixNQUFNLENBQUNOLE1BQUQsQ0FBTixDQUFlaUMsS0FBZixFQUFELENBQVIsR0FBaUMsR0FBdEQsRUFDRDtBQUFDTSx3QkFBTSxDQUFDa0MsUUFBUCxDQUFnQixhQUFoQjtBQUFnQyxpQkFEaEMsTUFHRDtBQUFDbEMsd0JBQU0sQ0FBQ1YsV0FBUCxDQUFtQixhQUFuQjtBQUFtQztBQUFDOztBQUNyQyxrQkFBRytCLFNBQVMsQ0FBQ3dDLG9DQUFWLElBQWdELFFBQW5ELEVBQ0E7QUFBQyxvQkFBR3BFLFFBQVEsQ0FBQzFCLE1BQU0sQ0FBQ04sTUFBRCxDQUFOLENBQWVpQyxLQUFmLEVBQUQsQ0FBUixHQUFpQyxHQUFwQyxFQUNEO0FBQUNNLHdCQUFNLENBQUNrQyxRQUFQLENBQWdCLGFBQWhCO0FBQWdDLGlCQURoQyxNQUdEO0FBQUNsQyx3QkFBTSxDQUFDVixXQUFQLENBQW1CLGFBQW5CO0FBQW1DO0FBQUM7QUFBQyxhQVR0QztBQVN5QztBQUFDOztBQUMxQyxZQUFHLE9BQU8rQixTQUFTLENBQUN5Qyw2QkFBakIsSUFBZ0QsV0FBaEQsSUFBNkR6QyxTQUFTLENBQUN5Qyw2QkFBVixJQUF5QyxNQUF6RyxFQUNBO0FBQUM5RCxnQkFBTSxDQUFDa0MsUUFBUCxDQUFnQixhQUFoQjtBQUErQmxDLGdCQUFNLENBQUNULE1BQVAsQ0FBYztBQUFDd0UscUJBQVMsRUFBQ3RFLFFBQVEsQ0FBQzRCLFNBQVMsQ0FBQzJDLGtDQUFYLENBQW5CO0FBQWtFQyxrQkFBTSxFQUFDNUMsU0FBUyxDQUFDNkMsaUNBQVYsQ0FBNENsQyxJQUFySDtBQUEwSG1DLGtCQUFNLEVBQUM5QyxTQUFTLENBQUMrQyxpQ0FBVixDQUE0Q3BDLElBQTdLO0FBQWtMcUMsbUJBQU8sRUFBQzVFLFFBQVEsQ0FBQzRCLFNBQVMsQ0FBQ2lELGtDQUFWLENBQTZDdEMsSUFBOUMsQ0FBUixHQUE0RCxLQUF0UDtBQUE0UHVDLG1CQUFPLEVBQUM5RSxRQUFRLENBQUM0QixTQUFTLENBQUNtRCxrQ0FBVixDQUE2Q3hDLElBQTlDLENBQVIsR0FBNEQsS0FBaFU7QUFBc1V5QyxtQkFBTyxFQUFDaEYsUUFBUSxDQUFDNEIsU0FBUyxDQUFDcUQsa0NBQVYsQ0FBNkMxQyxJQUE5QyxDQUFSLEdBQTRELEtBQTFZO0FBQWdaMkMsaUJBQUssRUFBQ2xGLFFBQVEsQ0FBQzRCLFNBQVMsQ0FBQ3VELHFDQUFWLENBQWdENUMsSUFBakQsQ0FBUixHQUErRCxJQUFyZDtBQUEwZDZDLGlCQUFLLEVBQUNwRixRQUFRLENBQUM0QixTQUFTLENBQUN5RCxxQ0FBVixDQUFnRDlDLElBQWpELENBQVIsR0FBK0QsSUFBL2hCO0FBQW9pQitDLGlCQUFLLEVBQUN0RixRQUFRLENBQUM0QixTQUFTLENBQUMyRCxxQ0FBVixDQUFnRGhELElBQWpELENBQVIsR0FBK0QsSUFBem1CO0FBQThtQmlELGlCQUFLLEVBQUN4RixRQUFRLENBQUM0QixTQUFTLENBQUM2RCxnQ0FBVixDQUEyQ2xELElBQTVDLENBQVIsR0FBMEQsS0FBOXFCO0FBQW9yQm1ELGlCQUFLLEVBQUMxRixRQUFRLENBQUM0QixTQUFTLENBQUMrRCxnQ0FBVixDQUEyQ3BELElBQTVDLENBQVIsR0FBMEQsS0FBcHZCO0FBQTB2QnFELHVCQUFXLEVBQUM1RixRQUFRLENBQUM0QixTQUFTLENBQUNpRSxzQ0FBVixDQUFpRHRELElBQWxELENBQTl3QjtBQUFzMEI3RCxrQkFBTSxFQUFDO0FBQTcwQixXQUFkOztBQUFxMkIsY0FBRyxPQUFPa0QsU0FBUyxDQUFDa0UsbUNBQWpCLElBQXNELFdBQXpELEVBQ3I0QjtBQUFDdkYsa0JBQU0sQ0FBQ2hCLEdBQVAsQ0FBVyxxQkFBWCxFQUFpQ1MsUUFBUSxDQUFDNEIsU0FBUyxDQUFDa0UsbUNBQVgsQ0FBUixHQUF3RCxJQUF6RjtBQUFnRzs7QUFDakcsY0FBSTdGLEtBQUssR0FBQzNCLE1BQU0sQ0FBQ04sTUFBRCxDQUFOLENBQWVpQyxLQUFmLEVBQVY7O0FBQWlDLGNBQUcyQixTQUFTLENBQUMyQyxrQ0FBVixJQUE4Q3RFLEtBQWpELEVBQXVEO0FBQUMsZ0JBQUcsQ0FBQ00sTUFBTSxDQUFDd0YsUUFBUCxDQUFnQixTQUFoQixDQUFKLEVBQ3pGO0FBQUN4RixvQkFBTSxDQUFDa0MsUUFBUCxDQUFnQixZQUFoQjtBQUErQjs7QUFDaEMsbUJBQU8sS0FBUDtBQUFjO0FBQUM7O0FBQ2YsWUFBRyxPQUFPYixTQUFTLENBQUNvRSxxQ0FBakIsSUFBd0QsV0FBeEQsSUFBcUVwRSxTQUFTLENBQUNvRSxxQ0FBVixJQUFpRCxNQUF6SCxFQUNBO0FBQUMsY0FBSUMsU0FBUyxHQUFDMUYsTUFBTSxDQUFDdUIsSUFBUCxDQUFZLFNBQVosQ0FBZDtBQUFxQ3ZCLGdCQUFNLENBQUMrQyxJQUFQLENBQVksNkJBQVosRUFBMkN4QixJQUEzQyxDQUFnRCxZQUFoRCxFQUE2RFUsVUFBVSxDQUFDWixTQUFTLENBQUNzRSwyQ0FBVixDQUFzRDNELElBQXZELENBQXZFO0FBQXFJaEMsZ0JBQU0sQ0FBQ3VCLElBQVAsQ0FBWSxJQUFaLEVBQWlCLGNBQVltRSxTQUE3QjtBQUF3QyxjQUFJRSxhQUFhLEdBQUM5RyxRQUFRLENBQUMrRyxjQUFULENBQXdCLGNBQVlILFNBQXBDLENBQWxCO0FBQWlFLGNBQUlJLFFBQVEsR0FBQyxJQUFJQyxRQUFKLENBQWFILGFBQWIsRUFBMkI7QUFBQ0kseUJBQWEsRUFBQztBQUFmLFdBQTNCLENBQWI7O0FBQThELGNBQUduRyxpQkFBaUIsQ0FBQ0ksVUFBbEIsRUFBSCxFQUNsVjtBQUFDLGdCQUFHRCxNQUFNLENBQUNOLEtBQVAsTUFBZ0IsQ0FBbkIsRUFDRDtBQUFDTSxvQkFBTSxDQUFDaEIsR0FBUCxDQUFXLE9BQVgsRUFBbUIsTUFBbkI7QUFBNEI7O0FBQzdCLGdCQUFHZ0IsTUFBTSxDQUFDdEIsTUFBUCxNQUFpQixDQUFwQixFQUNBO0FBQUNzQixvQkFBTSxDQUFDaEIsR0FBUCxDQUFXLFFBQVgsRUFBb0IsTUFBcEI7QUFBNkI7QUFBQztBQUFDOztBQUNoQyxZQUFHLE9BQU9xQyxTQUFTLENBQUM0RSwrQkFBakIsSUFBa0QsV0FBbEQsSUFBK0Q1RSxTQUFTLENBQUM0RSwrQkFBVixJQUEyQyxNQUE3RyxFQUNBO0FBQUMsY0FBSUMsY0FBYyxHQUFDLEVBQW5CO0FBQXNCLGNBQUlDLFlBQVksR0FBQyxFQUFqQjtBQUFvQixjQUFJQyxZQUFZLEdBQUMsRUFBakI7O0FBQW9CLGNBQUcsT0FBTy9FLFNBQVMsQ0FBQ2dGLHNDQUFqQixJQUF5RCxXQUE1RCxFQUMvRDtBQUFDSCwwQkFBYyxHQUFDN0UsU0FBUyxDQUFDZ0Ysc0NBQXpCOztBQUFnRSxvQkFBT0gsY0FBUDtBQUF1QixtQkFBSSxXQUFKO0FBQWdCQyw0QkFBWSxHQUFDLE9BQWI7QUFBcUI7O0FBQU0sbUJBQUksV0FBSjtBQUFnQkEsNEJBQVksR0FBQyxRQUFiO0FBQXNCOztBQUFNLG1CQUFJLFNBQUo7QUFBY0EsNEJBQVksR0FBQyxNQUFiO0FBQW9COztBQUFNLG1CQUFJLFNBQUo7QUFBY0EsNEJBQVksR0FBQyxNQUFiO0FBQW9COztBQUFNLG1CQUFJLFdBQUo7QUFBZ0JBLDRCQUFZLEdBQUMsUUFBYjtBQUFzQjs7QUFBTSxtQkFBSSxVQUFKO0FBQWVBLDRCQUFZLEdBQUMsT0FBYjtBQUFxQjs7QUFBTSxtQkFBSSxTQUFKO0FBQWNBLDRCQUFZLEdBQUMsTUFBYjtBQUFvQjtBQUF0VDs7QUFDakVDLHdCQUFZLElBQUVELFlBQVksR0FBQyxHQUEzQjtBQUFnQzs7QUFDaEMsY0FBRyxPQUFPOUUsU0FBUyxDQUFDaUYscUNBQWpCLElBQXdELFdBQTNELEVBQ0E7QUFBQ0Ysd0JBQVksSUFBRS9FLFNBQVMsQ0FBQ2lGLHFDQUFWLEdBQWdELElBQTlEO0FBQW9FOztBQUNyRUYsc0JBQVksSUFBRSxxQkFBZDs7QUFBb0MsY0FBRyxPQUFPL0UsU0FBUyxDQUFDa0YsbUNBQWpCLElBQXNELFdBQXpELEVBQ3BDO0FBQUNILHdCQUFZLElBQUUsa0JBQWdCL0UsU0FBUyxDQUFDa0YsbUNBQTFCLEdBQThELEdBQTVFO0FBQWlGOztBQUNsRnZHLGdCQUFNLENBQUNoQixHQUFQLENBQVc7QUFBQyx5QkFBWW9IO0FBQWIsV0FBWDtBQUF3Q3BHLGdCQUFNLENBQUNrQyxRQUFQLENBQWdCZ0UsY0FBaEI7QUFBaUM7O0FBQ3pFLFlBQUcsT0FBTzdFLFNBQVMsQ0FBQ21GLDJDQUFqQixJQUE4RCxXQUE5RCxJQUEyRW5GLFNBQVMsQ0FBQ21GLDJDQUFWLElBQXVELE1BQXJJLEVBQ0E7QUFBQyxjQUFJQyxjQUFjLEdBQUMxSSxNQUFNLENBQUMsTUFBRCxDQUFOLENBQWVpQixHQUFmLENBQW1CLGtCQUFuQixDQUFuQjtBQUEwRCxjQUFJMEgsUUFBUSxHQUFDM0ksTUFBTSxDQUFDTixNQUFELENBQU4sQ0FBZWUsU0FBZixFQUFiO0FBQXdDVCxnQkFBTSxDQUFDTixNQUFELENBQU4sQ0FBZW1CLEVBQWYsQ0FBa0Isa0JBQWxCLEVBQXFDLFlBQVU7QUFBQytILHdCQUFZLENBQUNoSSxDQUFDLENBQUN5QixJQUFGLENBQU8sSUFBUCxFQUFZLGFBQVosQ0FBRCxDQUFaO0FBQXlDekIsYUFBQyxDQUFDeUIsSUFBRixDQUFPLElBQVAsRUFBWSxhQUFaLEVBQTBCd0csVUFBVSxDQUFDLFlBQVU7QUFBQzdJLG9CQUFNLENBQUMsTUFBRCxDQUFOLENBQWV3RCxJQUFmLENBQW9CLGdCQUFwQixFQUFxQ3hELE1BQU0sQ0FBQ04sTUFBRCxDQUFOLENBQWVlLFNBQWYsRUFBckM7QUFBa0UsYUFBOUUsRUFBK0UsR0FBL0UsQ0FBcEM7QUFBeUgsZ0JBQUltRSxNQUFNLEdBQUM1RSxNQUFNLENBQUNOLE1BQUQsQ0FBTixDQUFlZSxTQUFmLEVBQVg7QUFBc0MsZ0JBQUlrSSxRQUFRLEdBQUMzSSxNQUFNLENBQUMsTUFBRCxDQUFOLENBQWV3RCxJQUFmLENBQW9CLGdCQUFwQixDQUFiO0FBQW1ELGdCQUFJc0YsWUFBWSxHQUFDOUksTUFBTSxDQUFDTixNQUFELENBQU4sQ0FBZWlCLE1BQWYsRUFBakI7QUFBeUMsZ0JBQUlvSSxrQkFBa0IsR0FBQ3JILFFBQVEsQ0FBQ29ILFlBQVksR0FBQyxDQUFkLENBQS9CO0FBQWdELGdCQUFJM0ksVUFBVSxHQUFDOEIsTUFBTSxDQUFDMEcsUUFBUCxHQUFrQnRJLEdBQWxCLEdBQXNCMEksa0JBQXJDO0FBQXdELGdCQUFJekksYUFBYSxHQUFDSCxVQUFVLEdBQUM4QixNQUFNLENBQUMxQixXQUFQLENBQW1CLElBQW5CLENBQTdCOztBQUFzRCxnQkFBR3FFLE1BQU0sR0FBQytELFFBQVYsRUFBbUI7QUFBQyxrQkFBRzNJLE1BQU0sQ0FBQ2UsUUFBRCxDQUFOLENBQWlCTixTQUFqQixNQUE4Qk4sVUFBOUIsSUFBMENILE1BQU0sQ0FBQ2UsUUFBRCxDQUFOLENBQWlCTixTQUFqQixNQUE4QkgsYUFBM0UsRUFBeUY7QUFBQ04sc0JBQU0sQ0FBQyxVQUFELENBQU4sQ0FBbUJpQixHQUFuQixDQUF1QixrQkFBdkIsRUFBMENxQyxTQUFTLENBQUMwRiw4Q0FBcEQ7QUFBb0doSixzQkFBTSxDQUFDLHdDQUFELENBQU4sQ0FBaURpQixHQUFqRCxDQUFxRCxNQUFyRCxFQUE0RHFDLFNBQVMsQ0FBQzBGLDhDQUF0RTtBQUF1SDs7QUFDOTVCLGtCQUFHaEosTUFBTSxDQUFDZSxRQUFELENBQU4sQ0FBaUJOLFNBQWpCLEtBQTZCSCxhQUFoQyxFQUE4QztBQUFDTixzQkFBTSxDQUFDLFVBQUQsQ0FBTixDQUFtQmlCLEdBQW5CLENBQXVCLGtCQUF2QixFQUEwQ3lILGNBQTFDO0FBQTBEMUksc0JBQU0sQ0FBQyx3Q0FBRCxDQUFOLENBQWlEaUIsR0FBakQsQ0FBcUQsTUFBckQsRUFBNER5SCxjQUE1RDtBQUE2RTtBQUFDLGFBRDhaLE1BQzFaO0FBQUMsa0JBQUcxSSxNQUFNLENBQUNlLFFBQUQsQ0FBTixDQUFpQk4sU0FBakIsTUFBOEJILGFBQTlCLElBQTZDTixNQUFNLENBQUNlLFFBQUQsQ0FBTixDQUFpQk4sU0FBakIsTUFBOEJOLFVBQTlFLEVBQXlGO0FBQUMwSSwwQkFBVSxDQUFDLFlBQVU7QUFBQzdJLHdCQUFNLENBQUMsVUFBRCxDQUFOLENBQW1CaUIsR0FBbkIsQ0FBdUIsa0JBQXZCLEVBQTBDcUMsU0FBUyxDQUFDMEYsOENBQXBELEVBQW9HQyxJQUFwRztBQUEyR2pKLHdCQUFNLENBQUMsd0NBQUQsQ0FBTixDQUFpRGlCLEdBQWpELENBQXFELE1BQXJELEVBQTREcUMsU0FBUyxDQUFDMEYsOENBQXRFLEVBQXNIQyxJQUF0SDtBQUE4SCxpQkFBclAsRUFBc1AsR0FBdFAsQ0FBVjtBQUFzUTs7QUFDNWhCLGtCQUFHakosTUFBTSxDQUFDZSxRQUFELENBQU4sQ0FBaUJOLFNBQWpCLEtBQTZCd0IsTUFBTSxDQUFDMEcsUUFBUCxHQUFrQnRJLEdBQWxELEVBQXNEO0FBQUNMLHNCQUFNLENBQUMsVUFBRCxDQUFOLENBQW1CaUIsR0FBbkIsQ0FBdUIsa0JBQXZCLEVBQTBDeUgsY0FBMUM7QUFBMEQxSSxzQkFBTSxDQUFDLHdDQUFELENBQU4sQ0FBaURpQixHQUFqRCxDQUFxRCxNQUFyRCxFQUE0RHlILGNBQTVEO0FBQTZFO0FBQUM7QUFBQyxXQUY3RjtBQUVnRzs7QUFDbk0sWUFBRyxPQUFPcEYsU0FBUyxDQUFDNEYsb0NBQWpCLElBQXVELFdBQXZELElBQW9FNUYsU0FBUyxDQUFDNEYsb0NBQVYsSUFBZ0QsTUFBdkgsRUFDQTtBQUFDakgsZ0JBQU0sQ0FBQ3BCLEVBQVAsQ0FBVSxPQUFWLEVBQWtCLFVBQVNzSSxDQUFULEVBQVc7QUFBQ0EsYUFBQyxDQUFDQyxjQUFGO0FBQW1CcEosa0JBQU0sQ0FBQyxzQkFBRCxDQUFOLENBQStCcUosTUFBL0I7QUFBd0NySixrQkFBTSxDQUFDLE1BQUQsQ0FBTixDQUFldUIsV0FBZixDQUEyQixRQUEzQjtBQUFxQ3ZCLGtCQUFNLENBQUMsTUFBRCxDQUFOLENBQWVtRSxRQUFmLENBQXdCLGlCQUF4QjtBQUEyQ25FLGtCQUFNLENBQUMsTUFBRCxDQUFOLENBQWVtRSxRQUFmLENBQXdCLGlCQUF4QjtBQUE0QyxXQUFyTjtBQUF3Tjs7QUFDek4sWUFBRyxPQUFPYixTQUFTLENBQUNnRyxpQ0FBakIsSUFBb0QsV0FBcEQsSUFBaUVoRyxTQUFTLENBQUNnRyxpQ0FBVixJQUE2QyxNQUFqSCxFQUNBO0FBQUNySCxnQkFBTSxDQUFDcEIsRUFBUCxDQUFVLE9BQVYsRUFBa0IsVUFBU3NJLENBQVQsRUFBVztBQUFDQSxhQUFDLENBQUNDLGNBQUY7QUFBbUJwSixrQkFBTSxDQUFDLFdBQUQsQ0FBTixDQUFvQnVKLE9BQXBCLENBQTRCO0FBQUM5SSx1QkFBUyxFQUFDO0FBQVgsYUFBNUIsRUFBMEMsR0FBMUM7QUFBK0NULGtCQUFNLENBQUMsTUFBRCxDQUFOLENBQWV3SixXQUFmLENBQTJCLFFBQTNCO0FBQXNDLFdBQXRJO0FBQXlJO0FBQUM7QUFBQyxLQTlGMUc7QUE4RjRHMUgscUJBQWlCLENBQUNDLEtBQWxCLENBQXdCQyxTQUF4QixDQUFrQywyREFBbEMsRUFBOEYsVUFBU0MsTUFBVCxFQUFnQjtBQUFDakMsWUFBTSxDQUFDLFVBQVNZLENBQVQsRUFBVztBQUFDWixjQUFNLENBQUMsVUFBRCxDQUFOLENBQW1Ca0IsSUFBbkIsQ0FBd0IsWUFBVTtBQUFDLGNBQUlDLFVBQVUsR0FBQ25CLE1BQU0sQ0FBQyxJQUFELENBQXJCO0FBQTRCQSxnQkFBTSxDQUFDLElBQUQsQ0FBTixDQUFhb0IsSUFBYixDQUFrQjtBQUFDQyx5QkFBYSxFQUFDLHlCQUFVO0FBQUNGLHdCQUFVLENBQUNHLE1BQVgsQ0FBa0IsK0JBQWxCLEVBQW1EQyxXQUFuRCxDQUErRCxNQUEvRDtBQUF3RTtBQUFsRyxXQUFsQjtBQUF5SCxTQUF4TDtBQUEwTHZCLGNBQU0sQ0FBQyxpQkFBRCxDQUFOLENBQTBCa0IsSUFBMUIsQ0FBK0IsWUFBVTtBQUFDLGNBQUl1SSxJQUFJLEdBQUN6SixNQUFNLENBQUMsSUFBRCxDQUFmO0FBQXNCeUosY0FBSSxDQUFDQyxZQUFMLEdBQW9CQyxJQUFwQixDQUF5QixZQUFVO0FBQUNGLGdCQUFJLENBQUNHLE9BQUwsQ0FBYTtBQUFDQywwQkFBWSxFQUFDLHFCQUFkO0FBQW9DQyx5QkFBVyxFQUFDLHFCQUFoRDtBQUFzRUMsb0JBQU0sRUFBQztBQUE3RSxhQUFiO0FBQStGL0osa0JBQU0sQ0FBQyxxQ0FBRCxDQUFOLENBQThDa0IsSUFBOUMsQ0FBbUQsVUFBUzhJLEtBQVQsRUFBZTtBQUFDbkIsd0JBQVUsQ0FBQyxZQUFVO0FBQUM3SSxzQkFBTSxDQUFDLHFDQUFELENBQU4sQ0FBOENpSyxFQUE5QyxDQUFpREQsS0FBakQsRUFBd0Q3RixRQUF4RCxDQUFpRSxZQUFqRTtBQUFnRixlQUE1RixFQUE2RixNQUFJNkYsS0FBakcsQ0FBVjtBQUFtSCxhQUF0TDtBQUF5TCxXQUE1VDtBQUE4VGhLLGdCQUFNLENBQUMsa0NBQUQsQ0FBTixDQUEyQ2tCLElBQTNDLENBQWdELFlBQVU7QUFBQyxnQkFBSUMsVUFBVSxHQUFDbkIsTUFBTSxDQUFDLElBQUQsQ0FBckI7QUFBNEJtQixzQkFBVSxDQUFDRyxNQUFYLENBQWtCLCtCQUFsQixFQUFtREMsV0FBbkQsQ0FBK0QsTUFBL0Q7QUFBdUV2QixrQkFBTSxDQUFDLElBQUQsQ0FBTixDQUFhb0IsSUFBYixDQUFrQjtBQUFDQywyQkFBYSxFQUFDLHlCQUFVO0FBQUNvSSxvQkFBSSxDQUFDRyxPQUFMLENBQWE7QUFBQ0MsOEJBQVksRUFBQyxxQkFBZDtBQUFvQ0MsNkJBQVcsRUFBQyxxQkFBaEQ7QUFBc0VDLHdCQUFNLEVBQUM7QUFBN0UsaUJBQWI7QUFBZ0c7QUFBMUgsYUFBbEI7QUFBaUosV0FBL1M7QUFBa1QsU0FBaHJCO0FBQWtyQi9KLGNBQU0sQ0FBQyx1QkFBRCxDQUFOLENBQWdDa0IsSUFBaEMsQ0FBcUMsWUFBVTtBQUFDLGNBQUl1SSxJQUFJLEdBQUN6SixNQUFNLENBQUMsSUFBRCxDQUFmO0FBQXNCeUosY0FBSSxDQUFDQyxZQUFMLEdBQW9CQyxJQUFwQixDQUF5QixZQUFVO0FBQUNGLGdCQUFJLENBQUNHLE9BQUwsQ0FBYTtBQUFDQywwQkFBWSxFQUFDLG1CQUFkO0FBQWtDQyx5QkFBVyxFQUFDLG1CQUE5QztBQUFrRUMsb0JBQU0sRUFBQztBQUF6RSxhQUFiO0FBQTJGL0osa0JBQU0sQ0FBQyx5Q0FBRCxDQUFOLENBQWtEa0IsSUFBbEQsQ0FBdUQsVUFBUzhJLEtBQVQsRUFBZTtBQUFDbkIsd0JBQVUsQ0FBQyxZQUFVO0FBQUM3SSxzQkFBTSxDQUFDLHlDQUFELENBQU4sQ0FBa0RpSyxFQUFsRCxDQUFxREQsS0FBckQsRUFBNEQ3RixRQUE1RCxDQUFxRSxZQUFyRTtBQUFvRixlQUFoRyxFQUFpRyxNQUFJNkYsS0FBckcsQ0FBVjtBQUF1SCxhQUE5TDtBQUFpTSxXQUFoVTtBQUFrVWhLLGdCQUFNLENBQUMsbUZBQUQsQ0FBTixDQUE0RmtCLElBQTVGLENBQWlHLFlBQVU7QUFBQyxnQkFBSUMsVUFBVSxHQUFDbkIsTUFBTSxDQUFDLElBQUQsQ0FBckI7QUFBNEJtQixzQkFBVSxDQUFDRyxNQUFYLENBQWtCLCtCQUFsQixFQUFtREMsV0FBbkQsQ0FBK0QsTUFBL0Q7QUFBdUV2QixrQkFBTSxDQUFDLElBQUQsQ0FBTixDQUFhb0IsSUFBYixDQUFrQjtBQUFDQywyQkFBYSxFQUFDLHlCQUFVO0FBQUNvSSxvQkFBSSxDQUFDRyxPQUFMLENBQWE7QUFBQ0MsOEJBQVksRUFBQyxtQkFBZDtBQUFrQ0MsNkJBQVcsRUFBQyxtQkFBOUM7QUFBa0VDLHdCQUFNLEVBQUM7QUFBekUsaUJBQWI7QUFBNEY7QUFBdEgsYUFBbEI7QUFBNkksV0FBNVY7QUFBK1YsU0FBdnVCO0FBQXl1QixZQUFJRyxVQUFVLEdBQUNsSyxNQUFNLENBQUMsaUJBQUQsQ0FBTixDQUEwQm1LLEdBQTFCLEVBQWY7O0FBQStDLFlBQUdELFVBQVUsSUFBRSxVQUFmLEVBQ3A1RDtBQUFDbEssZ0JBQU0sQ0FBQywwQkFBRCxDQUFOLENBQW1Db0ssZUFBbkMsQ0FBbUQ7QUFBQ0Msc0JBQVUsRUFBQztBQUFaLFdBQW5EO0FBQXNFLFNBRDYwRCxNQUdwNUQ7QUFBQ3JLLGdCQUFNLENBQUMsMEJBQUQsQ0FBTixDQUFtQ29LLGVBQW5DLENBQW1EO0FBQUNDLHNCQUFVLEVBQUM7QUFBWixXQUFuRDtBQUFxRTs7QUFDdEUsWUFBR3JLLE1BQU0sQ0FBQ04sTUFBRCxDQUFOLENBQWVpQyxLQUFmLE1BQXdCLEdBQXhCLElBQTZCbEMsYUFBYSxFQUE3QyxFQUNBO0FBQUNPLGdCQUFNLENBQUMsMEJBQUQsQ0FBTixDQUFtQ3NLLE9BQW5DLENBQTJDLG1CQUEzQztBQUFpRTtBQUFDLE9BTGdNLENBQU47QUFLdkwsS0FMd0U7QUFLdEV4SSxxQkFBaUIsQ0FBQ0MsS0FBbEIsQ0FBd0JDLFNBQXhCLENBQWtDLDZEQUFsQyxFQUFnRyxVQUFTQyxNQUFULEVBQWdCO0FBQUNqQyxZQUFNLENBQUMsVUFBRCxDQUFOLENBQW1Ca0IsSUFBbkIsQ0FBd0IsWUFBVTtBQUFDLFlBQUlDLFVBQVUsR0FBQ25CLE1BQU0sQ0FBQyxJQUFELENBQXJCO0FBQTRCQSxjQUFNLENBQUMsSUFBRCxDQUFOLENBQWFvQixJQUFiLENBQWtCO0FBQUNDLHVCQUFhLEVBQUMseUJBQVU7QUFBQ0Ysc0JBQVUsQ0FBQ0csTUFBWCxDQUFrQiwrQkFBbEIsRUFBbURDLFdBQW5ELENBQStELE1BQS9EO0FBQXVFSixzQkFBVSxDQUFDRyxNQUFYLENBQWtCLG1DQUFsQixFQUF1REEsTUFBdkQsQ0FBOEQsdUJBQTlELEVBQXVGQyxXQUF2RixDQUFtRyxNQUFuRztBQUEyR0osc0JBQVUsQ0FBQ0csTUFBWCxDQUFrQix1QkFBbEIsRUFBMkNDLFdBQTNDLENBQXVELE1BQXZEO0FBQWdFO0FBQTVRLFNBQWxCO0FBQWtTLE9BQWpXO0FBQW9XLEtBQXJkO0FBQXVkTyxxQkFBaUIsQ0FBQ0MsS0FBbEIsQ0FBd0JDLFNBQXhCLENBQWtDLGdFQUFsQyxFQUFtRyxVQUFTQyxNQUFULEVBQWdCO0FBQUNqQyxZQUFNLENBQUMsVUFBU1ksQ0FBVCxFQUFXO0FBQUNaLGNBQU0sQ0FBQywwREFBRCxDQUFOLENBQW1Fa0IsSUFBbkUsQ0FBd0UsWUFBVTtBQUFDLGNBQUl1SSxJQUFJLEdBQUN6SixNQUFNLENBQUMsSUFBRCxDQUFmO0FBQXNCLGNBQUl1SyxJQUFJLEdBQUNkLElBQUksQ0FBQ2pHLElBQUwsQ0FBVSxXQUFWLENBQVQ7O0FBQWdDLGNBQUcsQ0FBQ2lHLElBQUksQ0FBQ2hDLFFBQUwsQ0FBYyxjQUFkLENBQUosRUFDL3lCO0FBQUMsZ0JBQUlzQyxNQUFNLEdBQUMsRUFBWDs7QUFBYyxnQkFBR1EsSUFBSSxHQUFDLENBQVIsRUFDZjtBQUFDUixvQkFBTSxHQUFDLEVBQVA7QUFBVztBQUFDLFdBRmt5QixNQUkveUI7QUFBQ0Esa0JBQU0sR0FBQyxDQUFQO0FBQVU7O0FBQ1hOLGNBQUksQ0FBQ0MsWUFBTCxHQUFvQkMsSUFBcEIsQ0FBeUIsWUFBVTtBQUFDRixnQkFBSSxDQUFDRyxPQUFMLENBQWE7QUFBQ0MsMEJBQVksRUFBQyxvQkFBZDtBQUFtQ0MseUJBQVcsRUFBQyxvQkFBL0M7QUFBb0VDLG9CQUFNLEVBQUNBO0FBQTNFLGFBQWI7QUFBaUcvSixrQkFBTSxDQUFDLDZFQUFELENBQU4sQ0FBc0ZrQixJQUF0RixDQUEyRixVQUFTOEksS0FBVCxFQUFlO0FBQUNuQix3QkFBVSxDQUFDLFlBQVU7QUFBQzdJLHNCQUFNLENBQUMsZ0NBQUQsQ0FBTixDQUF5Q2lLLEVBQXpDLENBQTRDRCxLQUE1QyxFQUFtRDdGLFFBQW5ELENBQTRELFlBQTVEO0FBQTJFLGVBQXZGLEVBQXdGLE1BQUk2RixLQUE1RixDQUFWO0FBQThHLGFBQXpOO0FBQTROLFdBQWpXO0FBQW1XaEssZ0JBQU0sQ0FBQywyRUFBRCxDQUFOLENBQW9Ga0IsSUFBcEYsQ0FBeUYsWUFBVTtBQUFDLGdCQUFJQyxVQUFVLEdBQUNuQixNQUFNLENBQUMsSUFBRCxDQUFyQjtBQUE0Qm1CLHNCQUFVLENBQUNHLE1BQVgsQ0FBa0IsK0JBQWxCLEVBQW1EQyxXQUFuRCxDQUErRCxNQUEvRDtBQUF1RSxnQkFBSWdKLElBQUksR0FBQ2QsSUFBSSxDQUFDakcsSUFBTCxDQUFVLFdBQVYsQ0FBVDs7QUFBZ0MsZ0JBQUcsQ0FBQ2lHLElBQUksQ0FBQ2hDLFFBQUwsQ0FBYyxjQUFkLENBQUosRUFDMWtCO0FBQUMsa0JBQUlzQyxNQUFNLEdBQUMsRUFBWDs7QUFBYyxrQkFBR1EsSUFBSSxHQUFDLENBQVIsRUFDZjtBQUFDUixzQkFBTSxHQUFDLEVBQVA7QUFBVztBQUFDLGFBRjZqQixNQUkxa0I7QUFBQ0Esb0JBQU0sR0FBQyxDQUFQO0FBQVU7O0FBQ1gvSixrQkFBTSxDQUFDLElBQUQsQ0FBTixDQUFhb0IsSUFBYixDQUFrQjtBQUFDQywyQkFBYSxFQUFDLHlCQUFVO0FBQUNvSSxvQkFBSSxDQUFDRyxPQUFMLENBQWE7QUFBQ0MsOEJBQVksRUFBQyxvQkFBZDtBQUFtQ0MsNkJBQVcsRUFBQyxvQkFBL0M7QUFBb0VDLHdCQUFNLEVBQUNBO0FBQTNFLGlCQUFiO0FBQWtHO0FBQTVILGFBQWxCO0FBQW1KLFdBTGdOO0FBSzdNLFNBVmdoQjtBQVU3Z0IsT0FWZ2dCLENBQU47QUFVdmYsS0FWbVk7QUFVallqSSxxQkFBaUIsQ0FBQ0MsS0FBbEIsQ0FBd0JDLFNBQXhCLENBQWtDLGtFQUFsQyxFQUFxRyxVQUFTQyxNQUFULEVBQWdCO0FBQUNqQyxZQUFNLENBQUMsVUFBU1ksQ0FBVCxFQUFXO0FBQUNaLGNBQU0sQ0FBQyxVQUFELENBQU4sQ0FBbUJrQixJQUFuQixDQUF3QixZQUFVO0FBQUMsY0FBSUMsVUFBVSxHQUFDbkIsTUFBTSxDQUFDLElBQUQsQ0FBckI7QUFBNEJBLGdCQUFNLENBQUMsSUFBRCxDQUFOLENBQWFvQixJQUFiLENBQWtCO0FBQUNDLHlCQUFhLEVBQUMseUJBQVU7QUFBQ0Ysd0JBQVUsQ0FBQ0csTUFBWCxDQUFrQiwrQkFBbEIsRUFBbURDLFdBQW5ELENBQStELE1BQS9EO0FBQXdFO0FBQWxHLFdBQWxCO0FBQXdILFNBQXZMO0FBQXlMdkIsY0FBTSxDQUFDLDREQUFELENBQU4sQ0FBcUVrQixJQUFyRSxDQUEwRSxZQUFVO0FBQUMsY0FBSXVJLElBQUksR0FBQ3pKLE1BQU0sQ0FBQyxJQUFELENBQWY7QUFBc0IsY0FBSXdLLFNBQVMsR0FBQ2YsSUFBSSxDQUFDakcsSUFBTCxDQUFVLGlCQUFWLENBQWQ7QUFBMkMsY0FBSWlILE1BQU0sR0FBQ2hCLElBQUksQ0FBQ2pHLElBQUwsQ0FBVSxhQUFWLENBQVg7QUFBb0MsY0FBSWtILGNBQWMsR0FBQ2pCLElBQUksQ0FBQ2pHLElBQUwsQ0FBVSx1QkFBVixDQUFuQjtBQUFzRCxjQUFJbUgsaUJBQWlCLEdBQUMsV0FBdEI7O0FBQWtDLGNBQUdELGNBQWMsSUFBRSxLQUFuQixFQUNsdkI7QUFBQ0MsNkJBQWlCLEdBQUMsU0FBbEI7QUFBNkI7O0FBQzlCbEIsY0FBSSxDQUFDbUIsZ0JBQUwsQ0FBc0I7QUFBQ0oscUJBQVMsRUFBQ0EsU0FBWDtBQUFxQkssbUJBQU8sRUFBQ0osTUFBN0I7QUFBb0NLLG1CQUFPLEVBQUNIO0FBQTVDLFdBQXRCO0FBQXVGLFNBRnlZO0FBRXRZLE9BRmdNLENBQU47QUFFdkwsS0FGaUU7QUFFL0Q3SSxxQkFBaUIsQ0FBQ0MsS0FBbEIsQ0FBd0JDLFNBQXhCLENBQWtDLG1FQUFsQyxFQUFzRyxVQUFTQyxNQUFULEVBQWdCO0FBQUNqQyxZQUFNLENBQUMsVUFBU1ksQ0FBVCxFQUFXO0FBQUNaLGNBQU0sQ0FBQyxNQUFELENBQU4sQ0FBZW1FLFFBQWYsQ0FBd0Isc0JBQXhCO0FBQWdELFlBQUk0RyxTQUFTLEdBQUMvSyxNQUFNLENBQUMscUJBQUQsQ0FBcEI7QUFBNEMsWUFBSWdMLFFBQVEsR0FBQ0QsU0FBUyxDQUFDdkgsSUFBVixDQUFlLGVBQWYsQ0FBYjtBQUE2QyxZQUFJeUgsV0FBVyxHQUFDLEtBQWhCOztBQUFzQixZQUFHLE9BQU9ELFFBQVAsSUFBaUIsV0FBcEIsRUFBZ0M7QUFBQ0MscUJBQVcsR0FBQztBQUFDQyxpQkFBSyxFQUFDRjtBQUFQLFdBQVo7QUFBOEI7O0FBQ3ZjLFlBQUlHLE1BQU0sR0FBQ0osU0FBUyxDQUFDdkgsSUFBVixDQUFlLGFBQWYsQ0FBWDs7QUFBeUMsWUFBRyxPQUFPMkgsTUFBUCxJQUFlLFdBQWxCLEVBQThCO0FBQUNBLGdCQUFNLEdBQUMsT0FBUDtBQUFnQjs7QUFDeEYsWUFBSTdHLEtBQUssR0FBQ3lHLFNBQVMsQ0FBQ3ZILElBQVYsQ0FBZSxZQUFmLENBQVY7O0FBQXVDLFlBQUcsT0FBT2MsS0FBUCxJQUFjLFdBQWpCLEVBQTZCO0FBQUNBLGVBQUssR0FBQyxHQUFOO0FBQVc7O0FBQ2hGLFlBQUk4RyxVQUFVLEdBQUMsSUFBSUMsTUFBSixDQUFXLHFCQUFYLEVBQWlDO0FBQUNDLG9CQUFVLEVBQUM7QUFBQ0Msa0JBQU0sRUFBQyxxQkFBUjtBQUE4QkMsa0JBQU0sRUFBQztBQUFyQyxXQUFaO0FBQXlFQyxzQkFBWSxFQUFDLENBQXRGO0FBQXdGQyx5QkFBZSxFQUFDLElBQXhHO0FBQTZHcEgsZUFBSyxFQUFDNUMsUUFBUSxDQUFDNEMsS0FBRCxDQUEzSDtBQUFtSXFILGNBQUksRUFBQyxJQUF4STtBQUE2SVIsZ0JBQU0sRUFBQ0EsTUFBcEo7QUFBMkpTLG9CQUFVLEVBQUMsSUFBdEs7QUFBMktDLHVCQUFhLEVBQUMsS0FBekw7QUFBK0xDLGNBQUksRUFBQztBQUFDQyx3QkFBWSxFQUFDO0FBQWQsV0FBcE07QUFBeU5DLGtCQUFRLEVBQUNmO0FBQWxPLFNBQWpDLENBQWY7QUFBaVMsT0FIckUsQ0FBTjtBQUc4RSxLQUhyTTtBQUd1TW5KLHFCQUFpQixDQUFDQyxLQUFsQixDQUF3QkMsU0FBeEIsQ0FBa0MseUVBQWxDLEVBQTRHLFVBQVNDLE1BQVQsRUFBZ0I7QUFBQ2pDLFlBQU0sQ0FBQyxVQUFTWSxDQUFULEVBQVc7QUFBQ1osY0FBTSxDQUFDLE1BQUQsQ0FBTixDQUFlbUUsUUFBZixDQUF3QixzQkFBeEI7QUFBZ0QsWUFBSThILE9BQU8sR0FBQyxLQUFaO0FBQWtCLFlBQUlDLFNBQVMsR0FBQyxXQUFXQyxJQUFYLENBQWdCdE0sU0FBUyxDQUFDQyxTQUExQixDQUFkO0FBQW1ELFlBQUlzTSxJQUFJLEdBQUMsUUFBUUQsSUFBUixDQUFhdE0sU0FBUyxDQUFDQyxTQUF2QixLQUFtQyxxQkFBcUJxTSxJQUFyQixDQUEwQnRNLFNBQVMsQ0FBQ0MsU0FBcEMsQ0FBNUM7QUFBMkYsWUFBSXVNLHdCQUF3QixHQUFDLEVBQTdCO0FBQWdDLFlBQUlDLG9CQUFvQixHQUFDLEdBQXpCO0FBQTZCLFlBQUlDLGtCQUFrQixHQUFDLENBQXZCO0FBQXlCLFlBQUlDLGdCQUFnQixHQUFDeE0sTUFBTSxDQUFDLDRCQUFELENBQU4sQ0FBcUN5TSxNQUExRDs7QUFBaUUsaUJBQVNDLGNBQVQsQ0FBd0JDLEdBQXhCLEVBQTRCO0FBQUMsY0FBR1QsU0FBSCxFQUFhO0FBQUNVLGlCQUFLLEdBQUNELEdBQUcsQ0FBQ0UsTUFBSixHQUFXLENBQUMsR0FBbEI7QUFBdUIsV0FBckMsTUFBMEMsSUFBR1QsSUFBSCxFQUFRO0FBQUNRLGlCQUFLLEdBQUMsQ0FBQ0QsR0FBRyxDQUFDRyxNQUFYO0FBQW1CLFdBQTVCLE1BQWdDO0FBQUNGLGlCQUFLLEdBQUNELEdBQUcsQ0FBQ0ksVUFBVjtBQUFzQjs7QUFDMzVCLGNBQUdkLE9BQU8sSUFBRSxJQUFaLEVBQWlCO0FBQUMsZ0JBQUdXLEtBQUssSUFBRSxDQUFDUCx3QkFBWCxFQUFvQztBQUFDSixxQkFBTyxHQUFDLElBQVI7O0FBQWEsa0JBQUdNLGtCQUFrQixLQUFHQyxnQkFBZ0IsR0FBQyxDQUF6QyxFQUEyQztBQUFDRCxrQ0FBa0I7QUFBR1Msd0JBQVE7QUFBSTs7QUFDakpDLGtDQUFvQixDQUFDWCxvQkFBRCxDQUFwQjtBQUE0Qzs7QUFDNUMsZ0JBQUdNLEtBQUssSUFBRVAsd0JBQVYsRUFBbUM7QUFBQ0oscUJBQU8sR0FBQyxJQUFSOztBQUFhLGtCQUFHTSxrQkFBa0IsS0FBRyxDQUF4QixFQUEwQjtBQUFDQSxrQ0FBa0I7QUFBSTs7QUFDbEdXLDBCQUFZO0FBQUdELGtDQUFvQixDQUFDWCxvQkFBRCxDQUFwQjtBQUE0QztBQUFDO0FBQUM7O0FBQzdELGlCQUFTVyxvQkFBVCxDQUE4QkUsYUFBOUIsRUFBNEM7QUFBQ3RFLG9CQUFVLENBQUMsWUFBVTtBQUFDb0QsbUJBQU8sR0FBQyxLQUFSO0FBQWUsV0FBM0IsRUFBNEJrQixhQUE1QixDQUFWO0FBQXNEOztBQUNuRyxZQUFJQyxlQUFlLEdBQUNsQixTQUFTLEdBQUMsZ0JBQUQsR0FBa0IsT0FBL0M7QUFBdUR4TSxjQUFNLENBQUMyTixnQkFBUCxDQUF3QkQsZUFBeEIsRUFBd0NWLGNBQXhDLEVBQXVELEtBQXZEOztBQUE4RCxpQkFBU00sUUFBVCxHQUFtQjtBQUFDLGNBQUlNLGNBQWMsR0FBQ3ROLE1BQU0sQ0FBQyw0QkFBRCxDQUFOLENBQXFDaUssRUFBckMsQ0FBd0NzQyxrQkFBa0IsR0FBQyxDQUEzRCxDQUFuQjtBQUFpRmUsd0JBQWMsQ0FBQ3JNLEdBQWYsQ0FBbUIsV0FBbkIsRUFBK0IseUJBQS9CLEVBQTBEK0QsSUFBMUQsQ0FBK0QsaUNBQS9ELEVBQWtHL0QsR0FBbEcsQ0FBc0csV0FBdEcsRUFBa0gsa0JBQWxIO0FBQXNJc00sZ0NBQXNCO0FBQUk7O0FBQzFYLGlCQUFTTCxZQUFULEdBQXVCO0FBQUMsY0FBSUksY0FBYyxHQUFDdE4sTUFBTSxDQUFDLDRCQUFELENBQU4sQ0FBcUNpSyxFQUFyQyxDQUF3Q3NDLGtCQUFrQixHQUFDLENBQTNELENBQW5CO0FBQWlGZSx3QkFBYyxDQUFDck0sR0FBZixDQUFtQixXQUFuQixFQUErQix1QkFBL0IsRUFBd0QrRCxJQUF4RCxDQUE2RCxpQ0FBN0QsRUFBZ0cvRCxHQUFoRyxDQUFvRyxXQUFwRyxFQUFnSCxrQkFBaEg7QUFBb0lzTSxnQ0FBc0I7QUFBSTs7QUFDdlEsaUJBQVNBLHNCQUFULEdBQWlDO0FBQUMsY0FBSUMsYUFBYSxHQUFDeE4sTUFBTSxDQUFDLDRCQUFELENBQU4sQ0FBcUNpSyxFQUFyQyxDQUF3Q3NDLGtCQUF4QyxDQUFsQjtBQUE4RWlCLHVCQUFhLENBQUN2TSxHQUFkLENBQWtCLFdBQWxCLEVBQThCLHdCQUE5QixFQUF3RCtELElBQXhELENBQTZELGlDQUE3RCxFQUFnRy9ELEdBQWhHLENBQW9HLFdBQXBHLEVBQWdILGtCQUFoSDtBQUFvSTtBQUFFOztBQUN0UGpCLGNBQU0sQ0FBQyxNQUFELENBQU4sQ0FBZWEsRUFBZixDQUFrQixXQUFsQixFQUE4QixVQUFTc0ksQ0FBVCxFQUFXO0FBQUNBLFdBQUMsQ0FBQ0MsY0FBRjtBQUFtQkQsV0FBQyxDQUFDc0UsZUFBRjtBQUFvQixpQkFBTyxLQUFQO0FBQWMsU0FBL0Y7QUFBaUcsWUFBSUMsRUFBSjtBQUFPMU4sY0FBTSxDQUFDZSxRQUFELENBQU4sQ0FBaUI0TSxJQUFqQixDQUFzQixZQUF0QixFQUFtQyxVQUFTeEUsQ0FBVCxFQUFXO0FBQUN1RSxZQUFFLEdBQUN2RSxDQUFDLENBQUN5RSxhQUFGLENBQWdCQyxPQUFoQixDQUF3QixDQUF4QixFQUEyQkMsT0FBOUI7QUFBdUMsU0FBdEY7QUFBd0Y5TixjQUFNLENBQUNlLFFBQUQsQ0FBTixDQUFpQjRNLElBQWpCLENBQXNCLFVBQXRCLEVBQWlDLFVBQVN4RSxDQUFULEVBQVc7QUFBQyxjQUFJNEUsRUFBRSxHQUFDNUUsQ0FBQyxDQUFDeUUsYUFBRixDQUFnQkksY0FBaEIsQ0FBK0IsQ0FBL0IsRUFBa0NGLE9BQXpDOztBQUFpRCxjQUFHSixFQUFFLEdBQUNLLEVBQUUsR0FBQyxDQUFULEVBQVc7QUFBQyxnQkFBR3hCLGtCQUFrQixLQUFHQyxnQkFBZ0IsR0FBQyxDQUF6QyxFQUEyQztBQUFDRCxnQ0FBa0I7QUFBR1Msc0JBQVE7QUFBSTs7QUFDdlhDLGdDQUFvQixDQUFDWCxvQkFBRCxDQUFwQjtBQUE0QyxXQURrUCxNQUM3TyxJQUFHb0IsRUFBRSxHQUFDSyxFQUFFLEdBQUMsQ0FBVCxFQUFXO0FBQUMsZ0JBQUd4QixrQkFBa0IsS0FBRyxDQUF4QixFQUEwQjtBQUFDQSxnQ0FBa0I7QUFBSTs7QUFDOUdXLHdCQUFZO0FBQUdELGdDQUFvQixDQUFDWCxvQkFBRCxDQUFwQjtBQUE0QztBQUFDLFNBRm9JO0FBRWpJLE9BWDBXLENBQU47QUFXalcsS0FYb087QUFXbE94SyxxQkFBaUIsQ0FBQ0MsS0FBbEIsQ0FBd0JDLFNBQXhCLENBQWtDLG1FQUFsQyxFQUFzRyxVQUFTQyxNQUFULEVBQWdCO0FBQUNqQyxZQUFNLENBQUMsTUFBRCxDQUFOLENBQWVtRSxRQUFmLENBQXdCLG9CQUF4QjtBQUE4Q25FLFlBQU0sQ0FBQyw2QkFBRCxDQUFOLENBQXNDa0IsSUFBdEMsQ0FBMkMsWUFBVTtBQUFDLFlBQUkrTSxTQUFTLEdBQUNqTyxNQUFNLENBQUMsSUFBRCxDQUFwQjtBQUEyQixZQUFJa08sS0FBSyxHQUFDRCxTQUFTLENBQUN6SyxJQUFWLENBQWUsZUFBZixDQUFWOztBQUEwQyxZQUFHMEssS0FBSyxJQUFFLENBQVYsRUFDcFc7QUFBQ0EsZUFBSyxHQUFDLEtBQU47QUFBYTs7QUFDZCxZQUFJdkMsSUFBSSxHQUFDc0MsU0FBUyxDQUFDekssSUFBVixDQUFlLFdBQWYsQ0FBVDtBQUFxQyxZQUFJOEgsVUFBVSxHQUFDMkMsU0FBUyxDQUFDekssSUFBVixDQUFlLGlCQUFmLENBQWY7O0FBQWlELFlBQUc4SCxVQUFVLElBQUUsQ0FBZixFQUN0RjtBQUFDQSxvQkFBVSxHQUFDLEtBQVg7QUFBa0I7O0FBQ25CLFlBQUk2QyxVQUFVLEdBQUNGLFNBQVMsQ0FBQ3pLLElBQVYsQ0FBZSxpQkFBZixDQUFmOztBQUFpRCxZQUFHMkssVUFBVSxJQUFFLENBQWYsRUFDakQ7QUFBQ0Esb0JBQVUsR0FBQyxLQUFYO0FBQWtCOztBQUNuQkYsaUJBQVMsQ0FBQ0csUUFBVixDQUFtQjtBQUFDQyx5QkFBZSxFQUFDLEtBQWpCO0FBQXVCM0Usc0JBQVksRUFBQyxLQUFwQztBQUEwQzRFLDRCQUFrQixFQUFDLElBQTdEO0FBQWtFQyxrQkFBUSxFQUFDLEdBQTNFO0FBQStFQyxrQkFBUSxFQUFDLENBQXhGO0FBQTBGQyw4QkFBb0IsRUFBQyxJQUEvRztBQUFvSHpELGtCQUFRLEVBQUNrRCxLQUE3SDtBQUFtSVEsaUJBQU8sRUFBQyxJQUEzSTtBQUFnSkMseUJBQWUsRUFBQ3JELFVBQWhLO0FBQTJLc0Qsa0JBQVEsRUFBQ1Q7QUFBcEwsU0FBbkI7QUFBb04sWUFBSXBHLFFBQVEsR0FBQ2tHLFNBQVMsQ0FBQ3pLLElBQVYsQ0FBZSxlQUFmLENBQWI7O0FBQTZDLFlBQUd1RSxRQUFRLElBQUUsQ0FBYixFQUNqUTtBQUFDLGNBQUk4RyxLQUFLLEdBQUNaLFNBQVMsQ0FBQ2pKLElBQVYsQ0FBZSw4QkFBZixDQUFWO0FBQXlELGNBQUk4SixRQUFRLEdBQUMvTixRQUFRLENBQUNDLGVBQVQsQ0FBeUIrTixLQUF0QztBQUE0QyxjQUFJQyxhQUFhLEdBQUMsT0FBT0YsUUFBUSxDQUFDRyxTQUFoQixJQUEyQixRQUEzQixHQUFvQyxXQUFwQyxHQUFnRCxpQkFBbEU7QUFBb0YsY0FBSUMsS0FBSyxHQUFDakIsU0FBUyxDQUFDNUwsSUFBVixDQUFlLFVBQWYsQ0FBVjtBQUFxQzRMLG1CQUFTLENBQUNwTixFQUFWLENBQWEsaUJBQWIsRUFBK0IsWUFBVTtBQUFDcU8saUJBQUssQ0FBQ0MsTUFBTixDQUFhQyxPQUFiLENBQXFCLFVBQVNDLEtBQVQsRUFBZXhLLENBQWYsRUFBaUI7QUFBQyxrQkFBSXlLLEdBQUcsR0FBQ1QsS0FBSyxDQUFDaEssQ0FBRCxDQUFiO0FBQWlCLGtCQUFJMEssQ0FBQyxHQUFDLENBQUNGLEtBQUssQ0FBQ0csTUFBTixHQUFhTixLQUFLLENBQUNLLENBQXBCLElBQXVCLENBQUMsQ0FBeEIsR0FBMEIsQ0FBaEM7QUFBa0NELGlCQUFHLENBQUNQLEtBQUosQ0FBVUMsYUFBVixJQUF5QixnQkFBY08sQ0FBZCxHQUFnQixLQUF6QztBQUFnRCxhQUExSTtBQUE2SSxXQUF2TDtBQUEwTDs7QUFDelosWUFBSUUsVUFBVSxHQUFDeEIsU0FBUyxDQUFDekssSUFBVixDQUFlLGlCQUFmLENBQWY7O0FBQWlELFlBQUdpTSxVQUFVLElBQUUsQ0FBZixFQUNqRDtBQUFDelAsZ0JBQU0sQ0FBQyxNQUFELENBQU4sQ0FBZW1FLFFBQWYsQ0FBd0Isc0JBQXhCO0FBQWdELGNBQUl1TCxVQUFVLEdBQUNoTyxRQUFRLENBQUMxQixNQUFNLENBQUMsVUFBRCxDQUFOLENBQW1CaUIsR0FBbkIsQ0FBdUIsWUFBdkIsQ0FBRCxDQUF2QjtBQUE4RCxjQUFJME8sY0FBYyxHQUFDM1AsTUFBTSxDQUFDTixNQUFELENBQU4sQ0FBZWtRLFdBQWYsRUFBbkI7QUFBZ0QsY0FBSUMsWUFBWSxHQUFDbk8sUUFBUSxDQUFDaU8sY0FBYyxHQUFDRCxVQUFoQixDQUF6QjtBQUFxRHpCLG1CQUFTLENBQUNqSixJQUFWLENBQWUsMEJBQWYsRUFBMkMvRCxHQUEzQyxDQUErQyxRQUEvQyxFQUF3RDRPLFlBQVksR0FBQyxJQUFyRTtBQUEyRTVCLG1CQUFTLENBQUNqSixJQUFWLENBQWUsOEJBQWYsRUFBK0MvRCxHQUEvQyxDQUFtRCxRQUFuRCxFQUE0RDRPLFlBQVksR0FBQyxJQUF6RTtBQUErRTVCLG1CQUFTLENBQUNHLFFBQVYsQ0FBbUIsUUFBbkI7QUFBNkJwTyxnQkFBTSxDQUFDTixNQUFELENBQU4sQ0FBZWtDLE1BQWYsQ0FBc0IsWUFBVTtBQUFDLGdCQUFJOE4sVUFBVSxHQUFDaE8sUUFBUSxDQUFDMUIsTUFBTSxDQUFDLFVBQUQsQ0FBTixDQUFtQmlCLEdBQW5CLENBQXVCLFlBQXZCLENBQUQsQ0FBdkI7QUFBOEQsZ0JBQUkwTyxjQUFjLEdBQUMzUCxNQUFNLENBQUNOLE1BQUQsQ0FBTixDQUFla1EsV0FBZixFQUFuQjtBQUFnRCxnQkFBSUMsWUFBWSxHQUFDbk8sUUFBUSxDQUFDaU8sY0FBYyxHQUFDRCxVQUFoQixDQUF6QjtBQUFxRHpCLHFCQUFTLENBQUNqSixJQUFWLENBQWUsMEJBQWYsRUFBMkMvRCxHQUEzQyxDQUErQyxRQUEvQyxFQUF3RDRPLFlBQVksR0FBQyxJQUFyRTtBQUEyRTVCLHFCQUFTLENBQUNqSixJQUFWLENBQWUsOEJBQWYsRUFBK0MvRCxHQUEvQyxDQUFtRCxRQUFuRCxFQUE0RDRPLFlBQVksR0FBQyxJQUF6RTtBQUErRTVCLHFCQUFTLENBQUNHLFFBQVYsQ0FBbUIsUUFBbkI7QUFBOEIsV0FBNVg7QUFBK1g7QUFBQyxPQVRsaUI7QUFTcWlCLEtBVDFzQjtBQVM0c0J0TSxxQkFBaUIsQ0FBQ0MsS0FBbEIsQ0FBd0JDLFNBQXhCLENBQWtDLGtFQUFsQyxFQUFxRyxVQUFTQyxNQUFULEVBQWdCO0FBQUNqQyxZQUFNLENBQUMsNEJBQUQsQ0FBTixDQUFxQ2tCLElBQXJDLENBQTBDLFlBQVU7QUFBQyxZQUFJK00sU0FBUyxHQUFDak8sTUFBTSxDQUFDLElBQUQsQ0FBcEI7QUFBMkIsWUFBSWtPLEtBQUssR0FBQ0QsU0FBUyxDQUFDekssSUFBVixDQUFlLGVBQWYsQ0FBVjs7QUFBMEMsWUFBRzBLLEtBQUssSUFBRSxDQUFWLEVBQ2hnQztBQUFDQSxlQUFLLEdBQUMsS0FBTjtBQUFhOztBQUNkLFlBQUl2QyxJQUFJLEdBQUNzQyxTQUFTLENBQUN6SyxJQUFWLENBQWUsV0FBZixDQUFUO0FBQXFDLFlBQUk4SCxVQUFVLEdBQUMyQyxTQUFTLENBQUN6SyxJQUFWLENBQWUsaUJBQWYsQ0FBZjs7QUFBaUQsWUFBRzhILFVBQVUsSUFBRSxDQUFmLEVBQ3RGO0FBQUNBLG9CQUFVLEdBQUMsS0FBWDtBQUFrQjs7QUFDbkIsWUFBSTZDLFVBQVUsR0FBQ0YsU0FBUyxDQUFDekssSUFBVixDQUFlLGlCQUFmLENBQWY7O0FBQWlELFlBQUcySyxVQUFVLElBQUUsQ0FBZixFQUNqRDtBQUFDQSxvQkFBVSxHQUFDLEtBQVg7QUFBa0I7O0FBQ25CRixpQkFBUyxDQUFDRyxRQUFWLENBQW1CO0FBQUNDLHlCQUFlLEVBQUMsS0FBakI7QUFBdUIzRSxzQkFBWSxFQUFDLElBQXBDO0FBQXlDK0UsOEJBQW9CLEVBQUMsSUFBOUQ7QUFBbUV6RCxrQkFBUSxFQUFDa0QsS0FBNUU7QUFBa0ZRLGlCQUFPLEVBQUMsSUFBMUY7QUFBK0ZDLHlCQUFlLEVBQUNyRCxVQUEvRztBQUEwSHNELGtCQUFRLEVBQUNUO0FBQW5JLFNBQW5CO0FBQW1LLFlBQUlzQixVQUFVLEdBQUN4QixTQUFTLENBQUN6SyxJQUFWLENBQWUsaUJBQWYsQ0FBZjs7QUFBaUQsWUFBR2lNLFVBQVUsSUFBRSxDQUFmLEVBQ3BOO0FBQUN6UCxnQkFBTSxDQUFDLE1BQUQsQ0FBTixDQUFlbUUsUUFBZixDQUF3QixzQkFBeEI7QUFBZ0QsY0FBSXVMLFVBQVUsR0FBQ2hPLFFBQVEsQ0FBQzFCLE1BQU0sQ0FBQyxVQUFELENBQU4sQ0FBbUJpQixHQUFuQixDQUF1QixZQUF2QixDQUFELENBQXZCO0FBQThELGNBQUkwTyxjQUFjLEdBQUMzUCxNQUFNLENBQUNOLE1BQUQsQ0FBTixDQUFla1EsV0FBZixFQUFuQjtBQUFnRCxjQUFJQyxZQUFZLEdBQUNuTyxRQUFRLENBQUNpTyxjQUFjLEdBQUNELFVBQWhCLENBQXpCO0FBQXFEekIsbUJBQVMsQ0FBQ2pKLElBQVYsQ0FBZSx5QkFBZixFQUEwQy9ELEdBQTFDLENBQThDLFFBQTlDLEVBQXVENE8sWUFBWSxHQUFDLElBQXBFO0FBQTBFNUIsbUJBQVMsQ0FBQ0csUUFBVixDQUFtQixRQUFuQjtBQUE2QnBPLGdCQUFNLENBQUNOLE1BQUQsQ0FBTixDQUFla0MsTUFBZixDQUFzQixZQUFVO0FBQUMsZ0JBQUk4TixVQUFVLEdBQUNoTyxRQUFRLENBQUMxQixNQUFNLENBQUMsVUFBRCxDQUFOLENBQW1CaUIsR0FBbkIsQ0FBdUIsWUFBdkIsQ0FBRCxDQUF2QjtBQUE4RCxnQkFBSTBPLGNBQWMsR0FBQzNQLE1BQU0sQ0FBQ04sTUFBRCxDQUFOLENBQWVrUSxXQUFmLEVBQW5CO0FBQWdELGdCQUFJQyxZQUFZLEdBQUNuTyxRQUFRLENBQUNpTyxjQUFjLEdBQUNELFVBQWhCLENBQXpCO0FBQXFEekIscUJBQVMsQ0FBQ2pKLElBQVYsQ0FBZSx5QkFBZixFQUEwQy9ELEdBQTFDLENBQThDLFFBQTlDLEVBQXVENE8sWUFBWSxHQUFDLElBQXBFO0FBQTBFNUIscUJBQVMsQ0FBQ0csUUFBVixDQUFtQixRQUFuQjtBQUE4QixXQUE1UztBQUErUztBQUFDLE9BUDJSO0FBT3hSLEtBUGtLO0FBT2hLdE0scUJBQWlCLENBQUNDLEtBQWxCLENBQXdCQyxTQUF4QixDQUFrQyxzRUFBbEMsRUFBeUcsVUFBU0MsTUFBVCxFQUFnQjtBQUFDLGVBQVM2TixRQUFULENBQWtCQyxJQUFsQixFQUF1QkMsSUFBdkIsRUFBNEJDLFNBQTVCLEVBQXNDO0FBQUMsWUFBSUMsT0FBSjtBQUFZLGVBQU8sWUFBVTtBQUFDLGNBQUlDLE9BQU8sR0FBQyxJQUFaO0FBQUEsY0FBaUJDLElBQUksR0FBQ0MsU0FBdEI7O0FBQWdDLGNBQUlDLEtBQUssR0FBQyxTQUFOQSxLQUFNLEdBQVU7QUFBQ0osbUJBQU8sR0FBQyxJQUFSO0FBQWEsZ0JBQUcsQ0FBQ0QsU0FBSixFQUFjRixJQUFJLENBQUNRLEtBQUwsQ0FBV0osT0FBWCxFQUFtQkMsSUFBbkI7QUFBMEIsV0FBMUU7O0FBQTJFLGNBQUlJLE9BQU8sR0FBQ1AsU0FBUyxJQUFFLENBQUNDLE9BQXhCO0FBQWdDdEgsc0JBQVksQ0FBQ3NILE9BQUQsQ0FBWjtBQUFzQkEsaUJBQU8sR0FBQ3JILFVBQVUsQ0FBQ3lILEtBQUQsRUFBT04sSUFBUCxDQUFsQjtBQUErQixjQUFHUSxPQUFILEVBQVdULElBQUksQ0FBQ1EsS0FBTCxDQUFXSixPQUFYLEVBQW1CQyxJQUFuQjtBQUEwQixTQUF2UDtBQUF5UDs7QUFBQTs7QUFBN1MsVUFBb1RLLFNBQXBUO0FBQThULDJCQUFZQyxFQUFaLEVBQWU7QUFBQTs7QUFBQyxlQUFLQyxHQUFMLEdBQVMsRUFBVDtBQUFZLGVBQUtBLEdBQUwsQ0FBU0QsRUFBVCxHQUFZQSxFQUFaO0FBQWUsZUFBS3BPLFFBQUwsR0FBYztBQUFDc08scUJBQVMsRUFBQztBQUFDekIsb0JBQU0sRUFBQztBQUFDMEIsd0JBQVEsRUFBQyxHQUFWO0FBQWNDLHNCQUFNLEVBQUM7QUFBckIsZUFBUjtBQUE2Q0MsbUJBQUssRUFBQztBQUFDRix3QkFBUSxFQUFDLEdBQVY7QUFBY0Msc0JBQU0sRUFBQztBQUFDLHdCQUFHLGNBQUo7QUFBbUJFLHFCQUFHLEVBQUM7QUFBdkI7QUFBckI7QUFBbkQsYUFBWDtBQUEySEMscUJBQVMsRUFBQ0M7QUFBckksV0FBZDtBQUNsbEMsZUFBS0MsSUFBTDtBQUFhOztBQUQ0dEI7QUFBQTtBQUFBLGlDQUVudUI7QUFBQyxnQkFBRyxLQUFLUixHQUFMLENBQVNELEVBQVosRUFDUDtBQUFDLG1CQUFLQyxHQUFMLENBQVN4QixNQUFULEdBQWdCaUMsS0FBSyxDQUFDQyxJQUFOLENBQVcsS0FBS1YsR0FBTCxDQUFTRCxFQUFULENBQVlZLGdCQUFaLENBQTZCLGtCQUE3QixDQUFYLENBQWhCO0FBQTZFLG1CQUFLQyxXQUFMLEdBQWlCLEtBQUtaLEdBQUwsQ0FBU3hCLE1BQVQsQ0FBZ0IxQyxNQUFqQztBQUF3QyxtQkFBS2tFLEdBQUwsQ0FBU2EsR0FBVCxHQUFhLEtBQUtiLEdBQUwsQ0FBU0QsRUFBVCxDQUFZZSxhQUFaLENBQTBCLFdBQTFCLENBQWI7QUFBb0QsbUJBQUtkLEdBQUwsQ0FBU2UsUUFBVCxHQUFrQixLQUFLZixHQUFMLENBQVNhLEdBQVQsQ0FBYUMsYUFBYixDQUEyQixzQkFBM0IsQ0FBbEI7QUFBcUUsbUJBQUtkLEdBQUwsQ0FBU2dCLFFBQVQsR0FBa0IsS0FBS2hCLEdBQUwsQ0FBU2EsR0FBVCxDQUFhQyxhQUFiLENBQTJCLHNCQUEzQixDQUFsQjtBQUFxRSxtQkFBS0csT0FBTCxHQUFhLENBQWI7QUFBZSxtQkFBS0MsV0FBTDtBQUFtQixtQkFBS0MsVUFBTDtBQUFtQjtBQUFDO0FBSCtYO0FBQUE7QUFBQSx3Q0FJNXRCO0FBQUMsaUJBQUtDLElBQUwsR0FBVSxLQUFLcEIsR0FBTCxDQUFTRCxFQUFULENBQVlzQixxQkFBWixFQUFWO0FBQThDLGlCQUFLQyxTQUFMLEdBQWUsS0FBS0YsSUFBTCxDQUFVcFEsS0FBVixHQUFnQixFQUEvQjtBQUFrQyxpQkFBS3VRLEtBQUwsR0FBVztBQUFDQyxxQkFBTyxFQUFDLEtBQUtDLGFBQUwsQ0FBbUIsU0FBbkIsQ0FBVDtBQUF1Qyx1QkFBTSxLQUFLQSxhQUFMLENBQW1CLE9BQW5CO0FBQTdDLGFBQVg7QUFBcUYsaUJBQUt6QixHQUFMLENBQVMwQixHQUFULEdBQWF0UixRQUFRLENBQUN1UixlQUFULENBQXlCLDRCQUF6QixFQUFzRCxLQUF0RCxDQUFiO0FBQTBFLGlCQUFLM0IsR0FBTCxDQUFTMEIsR0FBVCxDQUFhRSxZQUFiLENBQTBCLE9BQTFCLEVBQWtDLE9BQWxDO0FBQTJDLGlCQUFLNUIsR0FBTCxDQUFTMEIsR0FBVCxDQUFhRSxZQUFiLENBQTBCLE9BQTFCLEVBQWtDLE1BQWxDO0FBQTBDLGlCQUFLNUIsR0FBTCxDQUFTMEIsR0FBVCxDQUFhRSxZQUFiLENBQTBCLFFBQTFCLEVBQW1DLE1BQW5DO0FBQTJDLGlCQUFLNUIsR0FBTCxDQUFTMEIsR0FBVCxDQUFhRSxZQUFiLENBQTBCLFNBQTFCLGdCQUEyQyxLQUFLUixJQUFMLENBQVVwUSxLQUFyRCxTQUE2RCxLQUFLb1EsSUFBTCxDQUFVcFIsTUFBdkU7QUFBaUYsaUJBQUtnUSxHQUFMLENBQVMwQixHQUFULENBQWFHLFNBQWIsMEJBQXNDLEtBQUtsUSxRQUFMLENBQWMyTyxTQUFwRCxtQkFBb0UsS0FBS2lCLEtBQUwsQ0FBV0MsT0FBL0U7QUFBNEYsaUJBQUt4QixHQUFMLENBQVNELEVBQVQsQ0FBWStCLFlBQVosQ0FBeUIsS0FBSzlCLEdBQUwsQ0FBUzBCLEdBQWxDLEVBQXNDLEtBQUsxQixHQUFMLENBQVNhLEdBQS9DO0FBQW9ELGlCQUFLYixHQUFMLENBQVNJLEtBQVQsR0FBZSxLQUFLSixHQUFMLENBQVMwQixHQUFULENBQWFaLGFBQWIsQ0FBMkIsTUFBM0IsQ0FBZjtBQUFtRDtBQUp3RjtBQUFBO0FBQUEsd0NBSzV0QjtBQUFDLGlCQUFLUyxLQUFMLENBQVdDLE9BQVgsR0FBbUIsS0FBS0MsYUFBTCxDQUFtQixTQUFuQixDQUFuQjtBQUFpRCxpQkFBS0YsS0FBTCxZQUFpQixLQUFLRSxhQUFMLENBQW1CLE9BQW5CLENBQWpCO0FBQTZDLGlCQUFLekIsR0FBTCxDQUFTMEIsR0FBVCxDQUFhRSxZQUFiLENBQTBCLFNBQTFCLGdCQUEyQyxLQUFLUixJQUFMLENBQVVwUSxLQUFyRCxTQUE2RCxLQUFLb1EsSUFBTCxDQUFVcFIsTUFBdkU7QUFBaUYsaUJBQUtnUSxHQUFMLENBQVNJLEtBQVQsQ0FBZXdCLFlBQWYsQ0FBNEIsR0FBNUIsRUFBZ0MsS0FBS0csV0FBTCxHQUFpQixLQUFLUixLQUFMLFNBQWpCLEdBQWtDLEtBQUtBLEtBQUwsQ0FBV0MsT0FBN0U7QUFBdUY7QUFMcWQ7QUFBQTtBQUFBLDBDQU01c0I7QUFBQSxnQkFBZlEsSUFBZSx1RUFBVixTQUFVO0FBQUMsbUJBQU9BLElBQUksS0FBRyxTQUFQLHFCQUE0QixLQUFLWixJQUFMLENBQVVwUixNQUF0QyxTQUErQyxLQUFLb1IsSUFBTCxDQUFVcFEsS0FBekQsY0FBa0UsS0FBS29RLElBQUwsQ0FBVXBSLE1BQTVFLFNBQXFGLEtBQUtvUixJQUFMLENBQVVwUSxLQUEvRiw0QkFBc0gsS0FBS29RLElBQUwsQ0FBVXBRLEtBQWhJLGdCQUEySSxLQUFLb1EsSUFBTCxDQUFVcFEsS0FBckosY0FBOEosS0FBS29RLElBQUwsQ0FBVXBSLE1BQXhLLGVBQW1MLEtBQUtvUixJQUFMLENBQVVwUixNQUE3TCwyQkFBa04sS0FBS29SLElBQUwsQ0FBVXBSLE1BQTVOLFNBQXFPLEtBQUtvUixJQUFMLENBQVVwUSxLQUEvTyxjQUF3UCxLQUFLb1EsSUFBTCxDQUFVcFIsTUFBbFEsU0FBMlEsS0FBS29SLElBQUwsQ0FBVXBRLEtBQXJSLHdCQUF3UyxLQUFLc1EsU0FBN1MsY0FBMFQsS0FBS0EsU0FBL1QsU0FBMlUsS0FBS0YsSUFBTCxDQUFVcFEsS0FBVixHQUFnQixLQUFLc1EsU0FBaFcsY0FBNlcsS0FBS0EsU0FBbFgsU0FBOFgsS0FBS0YsSUFBTCxDQUFVcFEsS0FBVixHQUFnQixLQUFLc1EsU0FBblosY0FBZ2EsS0FBS0YsSUFBTCxDQUFVcFIsTUFBVixHQUFpQixLQUFLc1IsU0FBdGIsU0FBa2MsS0FBS0EsU0FBdmMsY0FBb2QsS0FBS0YsSUFBTCxDQUFVcFIsTUFBVixHQUFpQixLQUFLc1IsU0FBMWUsTUFBUDtBQUErZjtBQU40TTtBQUFBO0FBQUEsdUNBTzd0QjtBQUFBOztBQUFDLGlCQUFLdEIsR0FBTCxDQUFTZSxRQUFULENBQWtCckUsZ0JBQWxCLENBQW1DLE9BQW5DLEVBQTJDO0FBQUEscUJBQUksTUFBSSxDQUFDdUYsUUFBTCxDQUFjLE1BQWQsQ0FBSjtBQUFBLGFBQTNDO0FBQXNFLGlCQUFLakMsR0FBTCxDQUFTZ0IsUUFBVCxDQUFrQnRFLGdCQUFsQixDQUFtQyxPQUFuQyxFQUEyQztBQUFBLHFCQUFJLE1BQUksQ0FBQ3VGLFFBQUwsQ0FBYyxNQUFkLENBQUo7QUFBQSxhQUEzQztBQUFzRWxULGtCQUFNLENBQUMyTixnQkFBUCxDQUF3QixRQUF4QixFQUFpQ3lDLFFBQVEsQ0FBQyxZQUFJO0FBQUMsb0JBQUksQ0FBQ2lDLElBQUwsR0FBVSxNQUFJLENBQUNwQixHQUFMLENBQVNELEVBQVQsQ0FBWXNCLHFCQUFaLEVBQVY7O0FBQThDLG9CQUFJLENBQUNhLFdBQUw7QUFBb0IsYUFBeEUsRUFBeUUsRUFBekUsQ0FBekM7QUFBdUg5UixvQkFBUSxDQUFDc00sZ0JBQVQsQ0FBMEIsU0FBMUIsRUFBb0MsVUFBQ3lGLEVBQUQsRUFBTTtBQUFDLGtCQUFNQyxPQUFPLEdBQUNELEVBQUUsQ0FBQ0MsT0FBSCxJQUFZRCxFQUFFLENBQUNFLEtBQTdCOztBQUFtQyxrQkFBR0QsT0FBTyxLQUFHLEVBQWIsRUFBZ0I7QUFBQyxzQkFBSSxDQUFDSCxRQUFMLENBQWMsTUFBZDtBQUF1QixlQUF4QyxNQUN6VixJQUFHRyxPQUFPLEtBQUcsRUFBYixFQUFnQjtBQUFDLHNCQUFJLENBQUNILFFBQUwsQ0FBYyxNQUFkO0FBQXVCO0FBQUMsYUFEa087QUFDL047QUFSd3JCO0FBQUE7QUFBQSxxQ0FTcnRCO0FBQUE7O0FBQUEsZ0JBQVhLLEdBQVcsdUVBQVAsTUFBTztBQUFDLGdCQUFHLEtBQUtQLFdBQVIsRUFBb0IsT0FBTyxLQUFQO0FBQWEsaUJBQUtBLFdBQUwsR0FBaUIsSUFBakI7QUFBc0IsZ0JBQU1RLGNBQWMsR0FBQ0MsS0FBSyxDQUFDO0FBQUNDLHFCQUFPLEVBQUMsS0FBS3pDLEdBQUwsQ0FBU0ksS0FBbEI7QUFBd0JGLHNCQUFRLEVBQUMsS0FBS3ZPLFFBQUwsQ0FBY3NPLFNBQWQsQ0FBd0JHLEtBQXhCLENBQThCRixRQUEvRDtBQUF3RUMsb0JBQU0sRUFBQyxLQUFLeE8sUUFBTCxDQUFjc08sU0FBZCxDQUF3QkcsS0FBeEIsQ0FBOEJELE1BQTlCLE1BQS9FO0FBQXVIdUMsZUFBQyxFQUFDLEtBQUtuQixLQUFMO0FBQXpILGFBQUQsQ0FBMUI7O0FBQXVLLGdCQUFNb0IsYUFBYSxHQUFDLFNBQWRBLGFBQWMsR0FBSTtBQUFDLHFCQUFPLElBQUlDLE9BQUosQ0FBWSxVQUFDQyxPQUFELEVBQVNDLE1BQVQsRUFBa0I7QUFBQyxvQkFBTUMsWUFBWSxHQUFDLE1BQUksQ0FBQy9DLEdBQUwsQ0FBU3hCLE1BQVQsQ0FBZ0IsTUFBSSxDQUFDeUMsT0FBckIsQ0FBbkI7QUFBaUR1QixxQkFBSyxDQUFDO0FBQUNDLHlCQUFPLEVBQUNNLFlBQVQ7QUFBc0I3QywwQkFBUSxFQUFDLE1BQUksQ0FBQ3ZPLFFBQUwsQ0FBY3NPLFNBQWQsQ0FBd0J6QixNQUF4QixDQUErQjBCLFFBQTlEO0FBQXVFQyx3QkFBTSxFQUFDLE1BQUksQ0FBQ3hPLFFBQUwsQ0FBY3NPLFNBQWQsQ0FBd0J6QixNQUF4QixDQUErQjJCLE1BQTdHO0FBQW9INkMsNEJBQVUsRUFBQ1YsR0FBRyxLQUFHLE1BQU4sR0FBYSxDQUFDLENBQUQsR0FBRyxNQUFJLENBQUNsQixJQUFMLENBQVVwUSxLQUExQixHQUFnQyxNQUFJLENBQUNvUSxJQUFMLENBQVVwUSxLQUF6SztBQUErS2lTLDBCQUFRLEVBQUMsb0JBQUk7QUFBQ0YsZ0NBQVksQ0FBQ0csU0FBYixDQUF1QkMsTUFBdkIsQ0FBOEIsZUFBOUI7QUFBK0NOLDJCQUFPO0FBQUk7QUFBdlAsaUJBQUQsQ0FBTDtBQUFnUSxzQkFBSSxDQUFDNUIsT0FBTCxHQUFhcUIsR0FBRyxLQUFHLE1BQU4sR0FBYSxNQUFJLENBQUNyQixPQUFMLEdBQWEsTUFBSSxDQUFDTCxXQUFMLEdBQWlCLENBQTlCLEdBQWdDLE1BQUksQ0FBQ0ssT0FBTCxHQUFhLENBQTdDLEdBQStDLENBQTVELEdBQThELE1BQUksQ0FBQ0EsT0FBTCxHQUFhLENBQWIsR0FBZSxNQUFJLENBQUNBLE9BQUwsR0FBYSxDQUE1QixHQUE4QixNQUFJLENBQUNMLFdBQUwsR0FBaUIsQ0FBMUg7QUFBNEgsb0JBQU13QyxRQUFRLEdBQUMsTUFBSSxDQUFDcEQsR0FBTCxDQUFTeEIsTUFBVCxDQUFnQixNQUFJLENBQUN5QyxPQUFyQixDQUFmO0FBQTZDbUMsd0JBQVEsQ0FBQ0YsU0FBVCxDQUFtQkcsR0FBbkIsQ0FBdUIsZUFBdkI7QUFBd0NiLHFCQUFLLENBQUM7QUFBQ0MseUJBQU8sRUFBQ1csUUFBVDtBQUFrQmxELDBCQUFRLEVBQUMsTUFBSSxDQUFDdk8sUUFBTCxDQUFjc08sU0FBZCxDQUF3QnpCLE1BQXhCLENBQStCMEIsUUFBMUQ7QUFBbUVDLHdCQUFNLEVBQUMsTUFBSSxDQUFDeE8sUUFBTCxDQUFjc08sU0FBZCxDQUF3QnpCLE1BQXhCLENBQStCMkIsTUFBekc7QUFBZ0g2Qyw0QkFBVSxFQUFDLENBQUNWLEdBQUcsS0FBRyxNQUFOLEdBQWEsTUFBSSxDQUFDbEIsSUFBTCxDQUFVcFEsS0FBdkIsR0FBNkIsQ0FBQyxDQUFELEdBQUcsTUFBSSxDQUFDb1EsSUFBTCxDQUFVcFEsS0FBM0MsRUFBaUQsQ0FBakQ7QUFBM0gsaUJBQUQsQ0FBTDtBQUF1TCxvQkFBTXNTLFdBQVcsR0FBQ0YsUUFBUSxDQUFDdEMsYUFBVCxDQUF1QixZQUF2QixDQUFsQjtBQUF1RDBCLHFCQUFLLENBQUNXLE1BQU4sQ0FBYUcsV0FBYjtBQUEwQmQscUJBQUssQ0FBQztBQUFDQyx5QkFBTyxFQUFDYSxXQUFUO0FBQXFCcEQsMEJBQVEsRUFBQyxNQUFJLENBQUN2TyxRQUFMLENBQWNzTyxTQUFkLENBQXdCekIsTUFBeEIsQ0FBK0IwQixRQUEvQixHQUF3QyxDQUF0RTtBQUF3RUMsd0JBQU0sRUFBQyxNQUFJLENBQUN4TyxRQUFMLENBQWNzTyxTQUFkLENBQXdCekIsTUFBeEIsQ0FBK0IyQixNQUE5RztBQUFxSDZDLDRCQUFVLEVBQUMsQ0FBQ1YsR0FBRyxLQUFHLE1BQU4sR0FBYSxHQUFiLEdBQWlCLENBQUMsR0FBbkIsRUFBdUIsQ0FBdkI7QUFBaEksaUJBQUQsQ0FBTDtBQUFrS0UscUJBQUssQ0FBQztBQUFDQyx5QkFBTyxFQUFDLENBQUNXLFFBQVEsQ0FBQ3RDLGFBQVQsQ0FBdUIsY0FBdkIsQ0FBRCxFQUF3Q3NDLFFBQVEsQ0FBQ3RDLGFBQVQsQ0FBdUIsYUFBdkIsQ0FBeEMsRUFBOEVzQyxRQUFRLENBQUN0QyxhQUFULENBQXVCLGFBQXZCLENBQTlFLENBQVQ7QUFBOEhaLDBCQUFRLEVBQUMsTUFBSSxDQUFDdk8sUUFBTCxDQUFjc08sU0FBZCxDQUF3QnpCLE1BQXhCLENBQStCMEIsUUFBL0IsR0FBd0MsQ0FBL0s7QUFBaUxDLHdCQUFNLEVBQUMsTUFBSSxDQUFDeE8sUUFBTCxDQUFjc08sU0FBZCxDQUF3QnpCLE1BQXhCLENBQStCMkIsTUFBdk47QUFBOE41Rix1QkFBSyxFQUFDLGVBQUNnSixDQUFELEVBQUdyUCxDQUFIO0FBQUEsMkJBQU9BLENBQUMsR0FBQyxHQUFGLEdBQU0sR0FBYjtBQUFBLG1CQUFwTztBQUFxUDhPLDRCQUFVLEVBQUMsQ0FBQ1YsR0FBRyxLQUFHLE1BQU4sR0FBYSxHQUFiLEdBQWlCLENBQUMsR0FBbkIsRUFBdUIsQ0FBdkIsQ0FBaFE7QUFBMFJrQix5QkFBTyxFQUFDLENBQUMsQ0FBRCxFQUFHLENBQUg7QUFBbFMsaUJBQUQsQ0FBTDtBQUFpVCxlQUE1dkMsQ0FBUDtBQUFzd0MsYUFBL3hDOztBQUFneUMsZ0JBQU1DLGVBQWUsR0FBQyxTQUFoQkEsZUFBZ0IsR0FBSTtBQUFDakIsbUJBQUssQ0FBQztBQUFDQyx1QkFBTyxFQUFDLE1BQUksQ0FBQ3pDLEdBQUwsQ0FBU0ksS0FBbEI7QUFBd0JGLHdCQUFRLEVBQUMsTUFBSSxDQUFDdk8sUUFBTCxDQUFjc08sU0FBZCxDQUF3QkcsS0FBeEIsQ0FBOEJGLFFBQS9EO0FBQXdFM0YscUJBQUssRUFBQyxHQUE5RTtBQUFrRjRGLHNCQUFNLEVBQUMsTUFBSSxDQUFDeE8sUUFBTCxDQUFjc08sU0FBZCxDQUF3QkcsS0FBeEIsQ0FBOEJELE1BQTlCLENBQXFDRSxHQUE5SDtBQUFrSXFDLGlCQUFDLEVBQUMsTUFBSSxDQUFDbkIsS0FBTCxDQUFXQyxPQUEvSTtBQUF1SnlCLHdCQUFRLEVBQUM7QUFBQSx5QkFBSSxNQUFJLENBQUNsQixXQUFMLEdBQWlCLEtBQXJCO0FBQUE7QUFBaEssZUFBRCxDQUFMO0FBQW9NLGFBQS9OOztBQUNuaERRLDBCQUFjLENBQUNtQixRQUFmLENBQXdCQyxJQUF4QixDQUE2QmhCLGFBQTdCLEVBQTRDZ0IsSUFBNUMsQ0FBaURGLGVBQWpEO0FBQW1FO0FBVnNxQjs7QUFBQTtBQUFBOztBQVVycUI7QUFBQyxVQUFJckosU0FBUyxHQUFDaEssUUFBUSxDQUFDMFEsYUFBVCxDQUF1QixZQUF2QixDQUFkOztBQUFtRCxVQUFHMUcsU0FBSCxFQUN4SDtBQUFDLFlBQUltRyxtQkFBbUIsR0FBQ25HLFNBQVMsQ0FBQ3dKLFlBQVYsQ0FBdUIsaUJBQXZCLENBQXhCO0FBQWtFLFlBQUk5RCxTQUFKLENBQWMxRixTQUFkO0FBQXlCckIsb0JBQVksQ0FBQyxZQUFELEVBQWM7QUFBQzhLLG9CQUFVLEVBQUM7QUFBWixTQUFkLENBQVo7QUFBOEM7QUFBQyxLQVhxZTtBQVduZTFTLHFCQUFpQixDQUFDQyxLQUFsQixDQUF3QkMsU0FBeEIsQ0FBa0MsNERBQWxDLEVBQStGLFVBQVNDLE1BQVQsRUFBZ0I7QUFBQ2pDLFlBQU0sQ0FBQyxNQUFELENBQU4sQ0FBZW1FLFFBQWYsQ0FBd0Isc0JBQXhCO0FBQWdEbkUsWUFBTSxDQUFDLE1BQUQsQ0FBTixDQUFlbUUsUUFBZixDQUF3QixNQUF4Qjs7QUFBZ0MsZUFBUzJMLFFBQVQsQ0FBa0JDLElBQWxCLEVBQXVCQyxJQUF2QixFQUE0QkMsU0FBNUIsRUFBc0M7QUFBQyxZQUFJQyxPQUFKO0FBQVksZUFBTyxZQUFVO0FBQUMsY0FBSUMsT0FBTyxHQUFDLElBQVo7QUFBQSxjQUFpQkMsSUFBSSxHQUFDQyxTQUF0Qjs7QUFBZ0MsY0FBSUMsS0FBSyxHQUFDLFNBQU5BLEtBQU0sR0FBVTtBQUFDSixtQkFBTyxHQUFDLElBQVI7QUFBYSxnQkFBRyxDQUFDRCxTQUFKLEVBQWNGLElBQUksQ0FBQ1EsS0FBTCxDQUFXSixPQUFYLEVBQW1CQyxJQUFuQjtBQUEwQixXQUExRTs7QUFBMkUsY0FBSUksT0FBTyxHQUFDUCxTQUFTLElBQUUsQ0FBQ0MsT0FBeEI7QUFBZ0N0SCxzQkFBWSxDQUFDc0gsT0FBRCxDQUFaO0FBQXNCQSxpQkFBTyxHQUFDckgsVUFBVSxDQUFDeUgsS0FBRCxFQUFPTixJQUFQLENBQWxCO0FBQStCLGNBQUdRLE9BQUgsRUFBV1QsSUFBSSxDQUFDUSxLQUFMLENBQVdKLE9BQVgsRUFBbUJDLElBQW5CO0FBQTBCLFNBQXZQO0FBQXlQOztBQUFBOztBQUFDLGVBQVNxRSxXQUFULENBQXFCdEwsQ0FBckIsRUFBdUI7QUFBQyxZQUFJdUwsSUFBSSxHQUFDLENBQVQ7QUFBVyxZQUFJQyxJQUFJLEdBQUMsQ0FBVDtBQUFXLFlBQUcsQ0FBQ3hMLENBQUosRUFBTSxJQUFJQSxDQUFDLEdBQUN6SixNQUFNLENBQUNrVixLQUFiOztBQUFtQixZQUFHekwsQ0FBQyxDQUFDMEwsS0FBRixJQUFTMUwsQ0FBQyxDQUFDMkwsS0FBZCxFQUFvQjtBQUFDSixjQUFJLEdBQUN2TCxDQUFDLENBQUMwTCxLQUFQO0FBQWFGLGNBQUksR0FBQ3hMLENBQUMsQ0FBQzJMLEtBQVA7QUFBYyxTQUFoRCxNQUM1ckIsSUFBRzNMLENBQUMsQ0FBQzRMLE9BQUYsSUFBVzVMLENBQUMsQ0FBQzJFLE9BQWhCLEVBQXdCO0FBQUM0RyxjQUFJLEdBQUN2TCxDQUFDLENBQUM0TCxPQUFGLEdBQVVoVSxRQUFRLENBQUNpVSxJQUFULENBQWNDLFVBQXhCLEdBQ2xDbFUsUUFBUSxDQUFDQyxlQUFULENBQXlCaVUsVUFESTtBQUNPTixjQUFJLEdBQUN4TCxDQUFDLENBQUMyRSxPQUFGLEdBQVUvTSxRQUFRLENBQUNpVSxJQUFULENBQWN2VSxTQUF4QixHQUN6Q00sUUFBUSxDQUFDQyxlQUFULENBQXlCUCxTQURXO0FBQ0E7O0FBQ3JDLGVBQU07QUFBQzhPLFdBQUMsRUFBQ21GLElBQUg7QUFBUVEsV0FBQyxFQUFDUDtBQUFWLFNBQU47QUFBc0I7O0FBQ3RCLFVBQUloRSxHQUFHLEdBQUMsRUFBUjtBQUFXQSxTQUFHLENBQUN3RSxNQUFKLEdBQVdwVSxRQUFRLENBQUMwUSxhQUFULENBQXVCLHVDQUF2QixDQUFYO0FBQTJFZCxTQUFHLENBQUN5RSxRQUFKLEdBQWFyVSxRQUFRLENBQUMwUSxhQUFULENBQXVCLDZDQUF2QixDQUFiOztBQUFtRixVQUFHZCxHQUFHLENBQUN5RSxRQUFQLEVBQ3pLO0FBQUN6RSxXQUFHLENBQUMwRSxLQUFKLEdBQVUsR0FBR0MsS0FBSCxDQUFTQyxJQUFULENBQWM1RSxHQUFHLENBQUN5RSxRQUFKLENBQWE5RCxnQkFBYixDQUE4QixPQUE5QixDQUFkLENBQVY7QUFBaUUsT0FEdUcsTUFHeks7QUFBQ1gsV0FBRyxDQUFDMEUsS0FBSixHQUFVLEVBQVY7QUFBYzs7QUFDZjFFLFNBQUcsQ0FBQzZFLE9BQUosR0FBWXpVLFFBQVEsQ0FBQzBRLGFBQVQsQ0FBdUIsK0JBQXZCLENBQVo7O0FBQW9FLFVBQUdkLEdBQUcsQ0FBQzZFLE9BQVAsRUFDcEU7QUFBQzdFLFdBQUcsQ0FBQ2EsR0FBSixHQUFRO0FBQUNpRSxrQkFBUSxFQUFDOUUsR0FBRyxDQUFDNkUsT0FBSixDQUFZL0QsYUFBWixDQUEwQiwyQ0FBMUIsQ0FBVjtBQUFpRmlFLG1CQUFTLEVBQUMvRSxHQUFHLENBQUM2RSxPQUFKLENBQVkvRCxhQUFaLENBQTBCLDRDQUExQjtBQUEzRixTQUFSO0FBQTRLZCxXQUFHLENBQUN4QixNQUFKLEdBQVcsR0FBR21HLEtBQUgsQ0FBU0MsSUFBVCxDQUFjNUUsR0FBRyxDQUFDNkUsT0FBSixDQUFZbEUsZ0JBQVosQ0FBNkIsa0JBQTdCLENBQWQsQ0FBWDtBQUE0RTs7QUFDelAsVUFBSXFFLFdBQVcsR0FBQyxDQUFoQjtBQUFBLFVBQWtCQyxVQUFVLEdBQUNqRixHQUFHLENBQUMwRSxLQUFKLENBQVU1SSxNQUF2QztBQUFBLFVBQThDb0osYUFBYSxHQUFDO0FBQUNsQyxrQkFBVSxFQUFDLENBQVo7QUFBY21DLGtCQUFVLEVBQUMsQ0FBekI7QUFBMkJDLGtCQUFVLEVBQUMsT0FBdEM7QUFBOEN6UCxlQUFPLEVBQUMsQ0FBdEQ7QUFBd0RFLGVBQU8sRUFBQyxDQUFoRTtBQUFrRUUsZUFBTyxFQUFDO0FBQTFFLE9BQTVEO0FBQUEsVUFBeUlzUCxjQUFjLEdBQUM7QUFBQ3JDLGtCQUFVLEVBQUMsQ0FBWjtBQUFjbUMsa0JBQVUsRUFBQyxDQUF6QjtBQUEyQkMsa0JBQVUsRUFBQyxDQUF0QztBQUF3Q3pQLGVBQU8sRUFBQyxDQUFoRDtBQUFrREUsZUFBTyxFQUFDLENBQTFEO0FBQTRERSxlQUFPLEVBQUM7QUFBcEUsT0FBeEo7QUFBQSxVQUErTnVQLGFBQWEsR0FBQztBQUFDdEMsa0JBQVUsRUFBQyxDQUFaO0FBQWNtQyxrQkFBVSxFQUFDLE1BQXpCO0FBQWdDQyxrQkFBVSxFQUFDLENBQTNDO0FBQTZDelAsZUFBTyxFQUFDLE9BQXJEO0FBQTZERSxlQUFPLEVBQUMsQ0FBckU7QUFBdUVFLGVBQU8sRUFBQztBQUEvRSxPQUE3TztBQUFBLFVBQStUdVAsYUFBYSxHQUFDO0FBQUN0QyxrQkFBVSxFQUFDLENBQVo7QUFBY21DLGtCQUFVLEVBQUMsS0FBekI7QUFBK0JDLGtCQUFVLEVBQUMsQ0FBMUM7QUFBNEN6UCxlQUFPLEVBQUMsUUFBcEQ7QUFBNkRFLGVBQU8sRUFBQyxDQUFyRTtBQUF1RUUsZUFBTyxFQUFDO0FBQS9FLE9BQTdVO0FBQUEsVUFBK1p3UCxhQUFhLEdBQUM7QUFBQ3ZDLGtCQUFVLEVBQUMsQ0FBWjtBQUFjbUMsa0JBQVUsRUFBQyxDQUF6QjtBQUEyQkMsa0JBQVUsRUFBQyxPQUF0QztBQUE4Q3pQLGVBQU8sRUFBQyxNQUF0RDtBQUE2REUsZUFBTyxFQUFDLENBQXJFO0FBQXVFRSxlQUFPLEVBQUM7QUFBL0UsT0FBN2E7QUFBQSxVQUFvZ0J5UCxjQUFjLEdBQUM7QUFBQzdSLGFBQUssRUFBQyxNQUFQO0FBQWN3TSxjQUFNLEVBQUM7QUFBckIsT0FBbmhCO0FBQUEsVUFBZ2pCc0YsY0FBYyxHQUFDO0FBQUM5UixhQUFLLEVBQUMsTUFBUDtBQUFjd00sY0FBTSxFQUFDO0FBQXJCLE9BQS9qQjtBQUFBLFVBQTRsQnVGLGNBQWMsR0FBQztBQUFDL1IsYUFBSyxFQUFDLE1BQVA7QUFBY3dNLGNBQU0sRUFBQztBQUFyQixPQUEzbUI7QUFBQSxVQUE2cEJ3RixjQUFjLEdBQUM7QUFBQ2hTLGFBQUssRUFBQyxLQUFQO0FBQWF3TSxjQUFNLEVBQUM7QUFBcEIsT0FBNXFCO0FBQUEsVUFBNnRCeUYsY0FBYyxHQUFDO0FBQUNqUyxhQUFLLEVBQUMsTUFBUDtBQUFjd00sY0FBTSxFQUFDO0FBQXJCLE9BQTV1QjtBQUFBLFVBQTZ3QjBGLElBQUksR0FBQyxLQUFseEI7QUFBQSxVQUF3eEJDLFlBQVksR0FBQztBQUFDblEsZUFBTyxFQUFDLENBQVQ7QUFBV0UsZUFBTyxFQUFDLENBQUM7QUFBcEIsT0FBcnlCO0FBQUEsVUFBNHpCa1EsZUFBZSxHQUFDLFNBQWhCQSxlQUFnQixDQUFTaEcsRUFBVCxFQUFZaUcsUUFBWixFQUFxQjtBQUFDLFlBQUlDLGVBQWUsR0FBQyxTQUFoQkEsZUFBZ0IsQ0FBUzlELEVBQVQsRUFBWTtBQUFDLGVBQUsrRCxtQkFBTCxDQUF5QixlQUF6QixFQUF5Q0QsZUFBekM7O0FBQTBELGNBQUdELFFBQVEsSUFBRSxPQUFPQSxRQUFQLEtBQWtCLFVBQS9CLEVBQTBDO0FBQUNBLG9CQUFRLENBQUNwQixJQUFUO0FBQWlCO0FBQUMsU0FBeEo7O0FBQXlKN0UsVUFBRSxDQUFDckQsZ0JBQUgsQ0FBb0IsZUFBcEIsRUFBb0N1SixlQUFwQztBQUFzRCxPQUFqakM7QUFBQSxVQUFrakNFLEdBQUcsR0FBQztBQUFDblYsYUFBSyxFQUFDakMsTUFBTSxDQUFDcVgsVUFBZDtBQUF5QnBXLGNBQU0sRUFBQ2pCLE1BQU0sQ0FBQ2tRO0FBQXZDLE9BQXRqQztBQUFBLFVBQTBtQ29ILFFBQTFtQztBQUFBLFVBQW1uQ0MsWUFBbm5DOztBQUFnb0MsZUFBUzlGLElBQVQsR0FBZTtBQUFDK0YsWUFBSSxDQUFDO0FBQUNDLG9CQUFVLEVBQUNoQixjQUFaO0FBQTJCbEgsbUJBQVMsRUFBQzRHO0FBQXJDLFNBQUQsQ0FBSixDQUEwRHZCLElBQTFELENBQStELFlBQVU7QUFBQzhDLGtCQUFRO0FBQUksU0FBdEY7QUFBd0ZDLGlCQUFTLENBQUMsR0FBRCxDQUFUO0FBQWV2RixrQkFBVTtBQUFJOztBQUNyd0MsZUFBU3NGLFFBQVQsR0FBbUI7QUFBQ0UsMkJBQW1CLENBQUNmLGNBQUQsQ0FBbkI7QUFBb0NDLFlBQUksR0FBQyxJQUFMO0FBQVc7O0FBQ25FLGVBQVNlLFVBQVQsR0FBcUI7QUFBQ2YsWUFBSSxHQUFDLEtBQUw7QUFBWTs7QUFDbEMsZUFBU1UsSUFBVCxDQUFjTSxJQUFkLEVBQW1CO0FBQUMsZUFBTyxJQUFJakUsT0FBSixDQUFZLFVBQVNDLE9BQVQsRUFBaUJDLE1BQWpCLEVBQXdCO0FBQUMsY0FBR3VELFFBQVEsSUFBRSxDQUFDUSxJQUFJLENBQUNDLGNBQW5CLEVBQWtDO0FBQUMsbUJBQU8sS0FBUDtBQUFjOztBQUNqSFQsa0JBQVEsR0FBQyxJQUFUOztBQUFjLGNBQUdRLElBQUksQ0FBQ0wsVUFBUixFQUFtQjtBQUFDRywrQkFBbUIsQ0FBQ0UsSUFBSSxDQUFDTCxVQUFOLENBQW5CO0FBQXNDOztBQUN4RSxjQUFHSyxJQUFJLENBQUN2SSxTQUFSLEVBQWtCO0FBQUN5SSw4QkFBa0IsQ0FBQ0YsSUFBSSxDQUFDdkksU0FBTixDQUFsQjs7QUFBbUMsZ0JBQUkwSSxPQUFPLEdBQUMsU0FBUkEsT0FBUSxHQUFVO0FBQUNYLHNCQUFRLEdBQUMsS0FBVDtBQUFleEQscUJBQU87QUFBSSxhQUFqRDs7QUFBa0RrRCwyQkFBZSxDQUFDL0YsR0FBRyxDQUFDeUUsUUFBTCxFQUFjdUMsT0FBZCxDQUFmO0FBQXVDLFdBQS9JLE1BQ0k7QUFBQ25FLG1CQUFPO0FBQUk7QUFBQyxTQUhVLENBQVA7QUFHQTs7QUFDcEIsZUFBUzFCLFVBQVQsR0FBcUI7QUFBQyxZQUFJOEYsYUFBYSxHQUFDLFNBQWRBLGFBQWMsQ0FBUzlFLEVBQVQsRUFBWTtBQUFDK0UsK0JBQXFCLENBQUMsWUFBVTtBQUFDLGdCQUFHLENBQUNyQixJQUFKLEVBQVMsT0FBTyxLQUFQO0FBQWEsZ0JBQUlzQixRQUFRLEdBQUNyRCxXQUFXLENBQUMzQixFQUFELENBQXhCO0FBQUEsZ0JBQTZCaUYsSUFBSSxHQUFDdEIsWUFBWSxDQUFDblEsT0FBYixHQUFxQnVQLGFBQWEsQ0FBQ3ZQLE9BQWQsSUFBdUIsSUFBRW1RLFlBQVksQ0FBQ25RLE9BQWYsR0FBdUJ3USxHQUFHLENBQUNuVyxNQUEzQixHQUFrQ21YLFFBQVEsQ0FBQzVDLENBQTNDLEdBQTZDdUIsWUFBWSxDQUFDblEsT0FBakYsQ0FBckIsR0FBK0csQ0FBako7QUFBQSxnQkFBbUowUixJQUFJLEdBQUN2QixZQUFZLENBQUNqUSxPQUFiLEdBQXFCcVAsYUFBYSxDQUFDclAsT0FBZCxJQUF1QixJQUFFaVEsWUFBWSxDQUFDalEsT0FBZixHQUF1QnNRLEdBQUcsQ0FBQ25WLEtBQTNCLEdBQWlDbVcsUUFBUSxDQUFDdkksQ0FBMUMsR0FBNENrSCxZQUFZLENBQUNqUSxPQUFoRixDQUFyQixHQUE4RyxDQUF0UTtBQUF3UWtSLDhCQUFrQixDQUFDO0FBQUMsNEJBQWE3QixhQUFhLENBQUNsQyxVQUE1QjtBQUF1Qyw0QkFBYWtDLGFBQWEsQ0FBQ0MsVUFBbEU7QUFBNkUsNEJBQWFELGFBQWEsQ0FBQ0UsVUFBeEc7QUFBbUgseUJBQVVnQyxJQUFJLEdBQUMsS0FBbEk7QUFBd0kseUJBQVVDLElBQUksR0FBQyxLQUF2SjtBQUE2Six5QkFBVW5DLGFBQWEsQ0FBQ25QO0FBQXJMLGFBQUQsQ0FBbEI7QUFBbU4sV0FBN2YsQ0FBckI7QUFBcWhCLFNBQXBqQjtBQUFBLFlBQXFqQnVSLGdCQUFnQixHQUFDbkksUUFBUSxDQUFDLFlBQVU7QUFBQ2dILGFBQUcsR0FBQztBQUFDblYsaUJBQUssRUFBQ2pDLE1BQU0sQ0FBQ3FYLFVBQWQ7QUFBeUJwVyxrQkFBTSxFQUFDakIsTUFBTSxDQUFDa1E7QUFBdkMsV0FBSjtBQUF5RCxTQUFyRSxFQUFzRSxFQUF0RSxDQUE5a0I7O0FBQXdwQjdPLGdCQUFRLENBQUNzTSxnQkFBVCxDQUEwQixXQUExQixFQUFzQ3VLLGFBQXRDO0FBQXFEbFksY0FBTSxDQUFDMk4sZ0JBQVAsQ0FBd0IsUUFBeEIsRUFBaUM0SyxnQkFBakM7O0FBQW1ELFlBQUlDLGdCQUFnQixHQUFDLFNBQWpCQSxnQkFBaUIsR0FBVTtBQUFDdEYsa0JBQVEsQ0FBQyxNQUFELENBQVI7QUFBa0IsU0FBbEQ7QUFBQSxZQUFtRHVGLGdCQUFnQixHQUFDLFNBQWpCQSxnQkFBaUIsR0FBVTtBQUFDdkYsa0JBQVEsQ0FBQyxNQUFELENBQVI7QUFBa0IsU0FBakc7O0FBQWtHLFlBQUdqQyxHQUFHLENBQUNhLEdBQUosQ0FBUWlFLFFBQVIsSUFBa0I5RSxHQUFHLENBQUNhLEdBQUosQ0FBUWtFLFNBQTdCLEVBQ3gzQjtBQUFDL0UsYUFBRyxDQUFDYSxHQUFKLENBQVFpRSxRQUFSLENBQWlCcEksZ0JBQWpCLENBQWtDLE9BQWxDLEVBQTBDNkssZ0JBQTFDO0FBQTREdkgsYUFBRyxDQUFDYSxHQUFKLENBQVFrRSxTQUFSLENBQWtCckksZ0JBQWxCLENBQW1DLE9BQW5DLEVBQTJDOEssZ0JBQTNDO0FBQThEO0FBQUM7O0FBQzVILGVBQVNULGtCQUFULENBQTRCekksU0FBNUIsRUFBc0M7QUFBQzBCLFdBQUcsQ0FBQ3lFLFFBQUosQ0FBYXJHLEtBQWIsQ0FBbUJFLFNBQW5CLEdBQTZCLGlCQUFlQSxTQUFTLENBQUMwRSxVQUF6QixHQUFvQyxJQUFwQyxHQUF5QzFFLFNBQVMsQ0FBQzZHLFVBQW5ELEdBQThELElBQTlELEdBQW1FN0csU0FBUyxDQUFDOEcsVUFBN0UsR0FBd0YsSUFBeEYsR0FBNkYsaUJBQTdGLEdBQStHOUcsU0FBUyxDQUFDM0ksT0FBekgsR0FBaUksbUJBQWpJLEdBQXFKMkksU0FBUyxDQUFDekksT0FBL0osR0FBdUssbUJBQXZLLEdBQTJMeUksU0FBUyxDQUFDdkksT0FBck0sR0FBNk0sR0FBMU87QUFBK087O0FBQ3RSLGVBQVM0USxtQkFBVCxDQUE2QkgsVUFBN0IsRUFBd0M7QUFBQ3hHLFdBQUcsQ0FBQ3lFLFFBQUosQ0FBYXJHLEtBQWIsQ0FBbUJvSSxVQUFuQixHQUE4QkEsVUFBVSxLQUFHLE1BQWIsR0FBb0JBLFVBQXBCLEdBQStCLGVBQWFBLFVBQVUsQ0FBQzdTLEtBQXhCLEdBQThCLEdBQTlCLEdBQWtDNlMsVUFBVSxDQUFDckcsTUFBMUc7QUFBa0g7O0FBQzNKLGVBQVNzSCxXQUFULENBQXFCbkYsR0FBckIsRUFBeUIvSCxNQUF6QixFQUErQjtBQUFDLFlBQUltRSxLQUFLLEdBQUNzQixHQUFHLENBQUN4QixNQUFKLENBQVd3RyxXQUFYLENBQVY7QUFBQSxZQUFrQzNTLElBQUksR0FBQ3FNLEtBQUssQ0FBQ29DLGFBQU4sQ0FBb0IsYUFBcEIsQ0FBdkM7QUFBQSxZQUEwRTRHLEtBQUssR0FBQ2hKLEtBQUssQ0FBQ29DLGFBQU4sQ0FBb0IsY0FBcEIsQ0FBaEY7QUFBQSxZQUFvSDZHLElBQUksR0FBQ2pKLEtBQUssQ0FBQ29DLGFBQU4sQ0FBb0IsYUFBcEIsQ0FBekg7QUFBNEp2RyxjQUFLLEdBQUNBLE1BQUssS0FBR3FOLFNBQVIsR0FBa0JyTixNQUFsQixHQUF3QixDQUE5QjtBQUFnQ2lJLGFBQUssQ0FBQ1csTUFBTixDQUFhLENBQUM5USxJQUFELEVBQU1xVixLQUFOLEVBQVlDLElBQVosQ0FBYjtBQUFnQyxZQUFJRSxTQUFTLEdBQUM7QUFBQ3BGLGlCQUFPLEVBQUMsQ0FBQ3BRLElBQUQsRUFBTXFWLEtBQU4sRUFBWUMsSUFBWixDQUFUO0FBQTJCekgsa0JBQVEsRUFBQ29DLEdBQUcsS0FBRyxJQUFOLEdBQVcsR0FBWCxHQUFlLEdBQW5EO0FBQXVEL0gsZUFBSyxFQUFDLGVBQVNnSixDQUFULEVBQVdyUCxDQUFYLEVBQWE0VCxDQUFiLEVBQWU7QUFBQyxtQkFBT3ZOLE1BQUssR0FBQyxFQUFOLEdBQVNyRyxDQUFDLEdBQUMsRUFBbEI7QUFBc0IsV0FBbkc7QUFBb0dpTSxnQkFBTSxFQUFDLENBQUMsSUFBRCxFQUFNLEdBQU4sRUFBVSxJQUFWLEVBQWUsQ0FBZixDQUEzRztBQUE2SHFELGlCQUFPLEVBQUM7QUFBQ3VFLGlCQUFLLEVBQUN6RixHQUFHLEtBQUcsSUFBTixHQUFXLENBQUMsQ0FBRCxFQUFHLENBQUgsQ0FBWCxHQUFpQixDQUFDLENBQUQsRUFBRyxDQUFILENBQXhCO0FBQThCcEMsb0JBQVEsRUFBQ29DLEdBQUcsS0FBRyxJQUFOLEdBQVcsR0FBWCxHQUFlO0FBQXRELFdBQXJJO0FBQWdNNkMsb0JBQVUsRUFBQyxvQkFBUzVCLENBQVQsRUFBV3JQLENBQVgsRUFBYTtBQUFDLG1CQUFPb08sR0FBRyxLQUFHLElBQU4sR0FBVyxDQUFDLEdBQUQsRUFBSyxDQUFMLENBQVgsR0FBbUIsQ0FBQyxDQUFELEVBQUcsQ0FBQyxHQUFKLENBQTFCO0FBQW9DO0FBQTdQLFNBQWQ7O0FBQTZRLFlBQUdBLEdBQUcsS0FBRyxJQUFULEVBQWM7QUFBQ3VGLG1CQUFTLENBQUNHLEtBQVYsR0FBZ0IsWUFBVTtBQUFDdEosaUJBQUssQ0FBQ3dFLFNBQU4sQ0FBZ0JHLEdBQWhCLENBQW9CLGVBQXBCO0FBQXNDLFdBQWpFO0FBQW1FLFNBQWxGLE1BQ3JnQjtBQUFDd0UsbUJBQVMsQ0FBQzVFLFFBQVYsR0FBbUIsWUFBVTtBQUFDdkUsaUJBQUssQ0FBQ3dFLFNBQU4sQ0FBZ0JDLE1BQWhCLENBQXVCLGVBQXZCO0FBQXlDLFdBQXZFO0FBQXlFOztBQUM5RVgsYUFBSyxDQUFDcUYsU0FBRCxDQUFMO0FBQWtCOztBQUNsQixlQUFTbkIsU0FBVCxDQUFtQm5NLEtBQW5CLEVBQXlCO0FBQUNrTixtQkFBVyxDQUFDLElBQUQsRUFBTWxOLEtBQU4sQ0FBWDtBQUF5Qjs7QUFDbkQsZUFBUzBOLFNBQVQsQ0FBbUIxTixLQUFuQixFQUF5QjtBQUFDa04sbUJBQVcsQ0FBQyxLQUFELEVBQU9sTixLQUFQLENBQVg7QUFBMEI7O0FBQ3BELGVBQVMwSCxRQUFULENBQWtCSyxHQUFsQixFQUFzQjtBQUFDLFlBQUcrRCxRQUFRLElBQUVDLFlBQWIsRUFBMEI7QUFBQyxpQkFBTyxLQUFQO0FBQWM7O0FBQ2hFQSxvQkFBWSxHQUFDLElBQWI7QUFBa0IsWUFBSTRCLElBQUksR0FBQ2xJLEdBQUcsQ0FBQzBFLEtBQUosQ0FBVU0sV0FBVixDQUFUO0FBQWdDNEIsa0JBQVU7QUFBR3FCLGlCQUFTOztBQUFHLFlBQUczRixHQUFHLEtBQUcsTUFBVCxFQUFnQjtBQUFDMEMscUJBQVcsR0FBQ0EsV0FBVyxHQUFDQyxVQUFVLEdBQUMsQ0FBdkIsR0FBeUJELFdBQVcsR0FBQyxDQUFyQyxHQUF1QyxDQUFuRDtBQUFzRCxTQUF2RSxNQUN2RTtBQUFDQSxxQkFBVyxHQUFDQSxXQUFXLEdBQUMsQ0FBWixHQUFjQSxXQUFXLEdBQUMsQ0FBMUIsR0FBNEJDLFVBQVUsR0FBQyxDQUFuRDtBQUFzRDs7QUFDM0QsWUFBSWtELFFBQVEsR0FBQ25JLEdBQUcsQ0FBQzBFLEtBQUosQ0FBVU0sV0FBVixDQUFiO0FBQW9DbUQsZ0JBQVEsQ0FBQy9KLEtBQVQsQ0FBZUUsU0FBZixHQUF5QixrQkFBZ0JnRSxHQUFHLEtBQUcsTUFBTixHQUFhLEdBQWIsR0FBaUIsQ0FBQyxHQUFsQyxJQUF1QyxxQkFBdkMsSUFBOERBLEdBQUcsS0FBRyxNQUFOLEdBQWEsQ0FBYixHQUFlLENBQUMsQ0FBOUUsSUFBaUYsU0FBMUc7QUFBb0g2RixnQkFBUSxDQUFDL0osS0FBVCxDQUFlb0YsT0FBZixHQUF1QixDQUF2QjtBQUF5QitDLFlBQUksQ0FBQztBQUFDQyxvQkFBVSxFQUFDZixjQUFaO0FBQTJCbkgsbUJBQVMsRUFBQytHO0FBQXJDLFNBQUQsQ0FBSixDQUEyRDFCLElBQTNELENBQWdFLFlBQVU7QUFBQyxpQkFBTzRDLElBQUksQ0FBQztBQUFDakkscUJBQVMsRUFBQztBQUFDMEUsd0JBQVUsRUFBQyxDQUFDVixHQUFHLEtBQUcsTUFBTixHQUFhLENBQUMsR0FBZCxHQUFrQixHQUFuQixJQUF3QixHQUFwQztBQUF3QzZDLHdCQUFVLEVBQUMsQ0FBbkQ7QUFBcURDLHdCQUFVLEVBQUMsQ0FBaEU7QUFBa0V6UCxxQkFBTyxFQUFDLENBQTFFO0FBQTRFRSxxQkFBTyxFQUFDLENBQXBGO0FBQXNGRSxxQkFBTyxFQUFDO0FBQTlGO0FBQVgsV0FBRCxDQUFYO0FBQTJILFNBQXRNLEVBQXdNNE4sSUFBeE0sQ0FBNk0sWUFBVTtBQUFDd0Usa0JBQVEsQ0FBQ2pGLFNBQVQsQ0FBbUJHLEdBQW5CLENBQXVCLGVBQXZCO0FBQXdDNkUsY0FBSSxDQUFDaEYsU0FBTCxDQUFlQyxNQUFmLENBQXNCLGVBQXRCO0FBQXVDK0UsY0FBSSxDQUFDOUosS0FBTCxDQUFXb0YsT0FBWCxHQUFtQixDQUFuQjtBQUFxQmtELG1CQUFTO0FBQUcsaUJBQU9ILElBQUksQ0FBQztBQUFDakkscUJBQVMsRUFBQztBQUFDMEUsd0JBQVUsRUFBQyxDQUFDVixHQUFHLEtBQUcsTUFBTixHQUFhLENBQUMsR0FBZCxHQUFrQixHQUFuQixJQUF3QixHQUFwQztBQUF3QzZDLHdCQUFVLEVBQUMsQ0FBbkQ7QUFBcURDLHdCQUFVLEVBQUMsT0FBaEU7QUFBd0V6UCxxQkFBTyxFQUFDLENBQWhGO0FBQWtGRSxxQkFBTyxFQUFDLENBQTFGO0FBQTRGRSxxQkFBTyxFQUFDO0FBQXBHO0FBQVgsV0FBRCxDQUFYO0FBQWlJLFNBQXpjLEVBQTJjNE4sSUFBM2MsQ0FBZ2QsWUFBVTtBQUFDZ0QsNkJBQW1CLENBQUMsTUFBRCxDQUFuQjtBQUE0QndCLGtCQUFRLENBQUMvSixLQUFULENBQWVFLFNBQWYsR0FBeUIsb0JBQXpCO0FBQThDeUksNEJBQWtCLENBQUM3QixhQUFELENBQWxCO0FBQWtDaE4sb0JBQVUsQ0FBQyxZQUFVO0FBQUN1TyxvQkFBUTtBQUFJLFdBQXhCLEVBQXlCLEVBQXpCLENBQVY7QUFBdUNILHNCQUFZLEdBQUMsS0FBYjtBQUFvQixTQUFsb0I7QUFBcW9COztBQUN0ekIsZUFBUzhCLGdCQUFULEdBQTJCO0FBQUMsWUFBSUYsSUFBSSxHQUFDbEksR0FBRyxDQUFDMEUsS0FBSixDQUFVTSxXQUFWLENBQVQ7QUFBQSxZQUFnQ21ELFFBQVEsR0FBQ25JLEdBQUcsQ0FBQzBFLEtBQUosQ0FBVU0sV0FBVyxHQUFDQyxVQUFVLEdBQUMsQ0FBdkIsR0FBeUJELFdBQVcsR0FBQyxDQUFyQyxHQUF1QyxDQUFqRCxDQUF6QztBQUFBLFlBQTZGcUQsUUFBUSxHQUFDckksR0FBRyxDQUFDMEUsS0FBSixDQUFVTSxXQUFXLEdBQUMsQ0FBWixHQUFjQSxXQUFXLEdBQUMsQ0FBMUIsR0FBNEJDLFVBQVUsR0FBQyxDQUFqRCxDQUF0RztBQUEwSmtELGdCQUFRLENBQUMvSixLQUFULENBQWVFLFNBQWYsR0FBeUIsNENBQXpCO0FBQXNFNkosZ0JBQVEsQ0FBQy9KLEtBQVQsQ0FBZW9GLE9BQWYsR0FBdUIsQ0FBdkI7QUFBeUI2RSxnQkFBUSxDQUFDakssS0FBVCxDQUFlRSxTQUFmLEdBQXlCLDhDQUF6QjtBQUF3RStKLGdCQUFRLENBQUNqSyxLQUFULENBQWVvRixPQUFmLEdBQXVCLENBQXZCO0FBQTBCOztBQUN2WCxlQUFTOEUsbUJBQVQsR0FBOEI7QUFBQyxZQUFJSixJQUFJLEdBQUNsSSxHQUFHLENBQUMwRSxLQUFKLENBQVVNLFdBQVYsQ0FBVDtBQUFBLFlBQWdDbUQsUUFBUSxHQUFDbkksR0FBRyxDQUFDMEUsS0FBSixDQUFVTSxXQUFXLEdBQUNDLFVBQVUsR0FBQyxDQUF2QixHQUF5QkQsV0FBVyxHQUFDLENBQXJDLEdBQXVDLENBQWpELENBQXpDO0FBQUEsWUFBNkZxRCxRQUFRLEdBQUNySSxHQUFHLENBQUMwRSxLQUFKLENBQVVNLFdBQVcsR0FBQyxDQUFaLEdBQWNBLFdBQVcsR0FBQyxDQUExQixHQUE0QkMsVUFBVSxHQUFDLENBQWpELENBQXRHO0FBQTBKa0QsZ0JBQVEsQ0FBQy9KLEtBQVQsQ0FBZUUsU0FBZixHQUF5QixNQUF6QjtBQUFnQzZKLGdCQUFRLENBQUMvSixLQUFULENBQWVvRixPQUFmLEdBQXVCLENBQXZCO0FBQXlCNkUsZ0JBQVEsQ0FBQ2pLLEtBQVQsQ0FBZUUsU0FBZixHQUF5QixNQUF6QjtBQUFnQytKLGdCQUFRLENBQUNqSyxLQUFULENBQWVvRixPQUFmLEdBQXVCLENBQXZCO0FBQTBCOztBQUM1UyxVQUFHeEQsR0FBRyxDQUFDeUUsUUFBUCxFQUNBO0FBQUMxTCxvQkFBWSxDQUFDaUgsR0FBRyxDQUFDeUUsUUFBTCxFQUFjLFlBQVU7QUFBQyxjQUFJOEQsVUFBVSxHQUFDLElBQWY7QUFBb0IvRixlQUFLLENBQUM7QUFBQ0MsbUJBQU8sRUFBQ3pDLEdBQUcsQ0FBQ3dFLE1BQWI7QUFBb0J0RSxvQkFBUSxFQUFDLEdBQTdCO0FBQWlDQyxrQkFBTSxFQUFDLGdCQUF4QztBQUF5RDVGLGlCQUFLLEVBQUNnTyxVQUEvRDtBQUEwRXBELHNCQUFVLEVBQUMsT0FBckY7QUFBNkY2QyxpQkFBSyxFQUFDLGlCQUFVO0FBQUN4SCxrQkFBSTtBQUFJLGFBQXRIO0FBQXVIeUMsb0JBQVEsRUFBQyxvQkFBVTtBQUFDakQsaUJBQUcsQ0FBQ3dFLE1BQUosQ0FBV3RCLFNBQVgsQ0FBcUJDLE1BQXJCLENBQTRCLGlCQUE1QjtBQUFnRDtBQUEzTCxXQUFELENBQUw7QUFBcU0sU0FBbFAsQ0FBWjtBQUFpUTtBQUFDLEtBbEN0SDtBQWtDd0hoUyxxQkFBaUIsQ0FBQ0MsS0FBbEIsQ0FBd0JDLFNBQXhCLENBQWtDLHFFQUFsQyxFQUF3RyxVQUFTQyxNQUFULEVBQWdCO0FBQUNsQixjQUFRLENBQUNDLGVBQVQsQ0FBeUJtWSxTQUF6QixHQUFtQyxJQUFuQztBQUF3QyxVQUFJcE8sU0FBUyxHQUFDLElBQUlxTyxXQUFKLENBQWdCclksUUFBUSxDQUFDMFEsYUFBVCxDQUF1QixZQUF2QixDQUFoQixDQUFkOztBQUFvRSxVQUFHMVEsUUFBUSxDQUFDMFEsYUFBVCxDQUF1QixhQUF2QixDQUFILEVBQzFlO0FBQUMxUSxnQkFBUSxDQUFDMFEsYUFBVCxDQUF1QixhQUF2QixFQUFzQ3BFLGdCQUF0QyxDQUF1RCxPQUF2RCxFQUErRCxZQUFVO0FBQUN0QyxtQkFBUyxDQUFDc08sSUFBVjtBQUFrQixTQUE1RjtBQUErRjs7QUFDaEcsVUFBR3RZLFFBQVEsQ0FBQzBRLGFBQVQsQ0FBdUIsYUFBdkIsQ0FBSCxFQUNBO0FBQUMxUSxnQkFBUSxDQUFDMFEsYUFBVCxDQUF1QixhQUF2QixFQUFzQ3BFLGdCQUF0QyxDQUF1RCxPQUF2RCxFQUErRCxZQUFVO0FBQUN0QyxtQkFBUyxDQUFDdU8sSUFBVjtBQUFrQixTQUE1RjtBQUErRjtBQUFDLEtBSG9LO0FBR2xLeFgscUJBQWlCLENBQUNDLEtBQWxCLENBQXdCQyxTQUF4QixDQUFrQyw0REFBbEMsRUFBK0YsVUFBU0MsTUFBVCxFQUFnQjtBQUFDakMsWUFBTSxDQUFDLE1BQUQsQ0FBTixDQUFlbUUsUUFBZixDQUF3QixzQkFBeEI7QUFBZ0QsVUFBSW9WLGFBQWEsR0FBQyxHQUFsQjtBQUFzQixVQUFJQyxhQUFhLEdBQUMsTUFBbEI7QUFBeUI1WSxPQUFDLENBQUM2WSxRQUFGLENBQVdDLGNBQVgsQ0FBMEIsYUFBMUIsRUFBd0M7QUFBQ0MsdUJBQWUsRUFBQyxDQUFqQjtBQUFtQkMsYUFBSyxFQUFDLENBQUMsQ0FBQztBQUFDOUQsb0JBQVUsRUFBQztBQUFaLFNBQUQsRUFBc0IsQ0FBdEIsQ0FBRDtBQUF6QixPQUF4QztBQUE4RmxWLE9BQUMsQ0FBQzZZLFFBQUYsQ0FBV0MsY0FBWCxDQUEwQixlQUExQixFQUEwQztBQUFDQyx1QkFBZSxFQUFDLENBQWpCO0FBQW1CQyxhQUFLLEVBQUMsQ0FBQyxDQUFDO0FBQUM5RCxvQkFBVSxFQUFDO0FBQVosU0FBRCxFQUFxQixDQUFyQixDQUFEO0FBQXpCLE9BQTFDO0FBQStGbFYsT0FBQyxDQUFDNlksUUFBRixDQUFXQyxjQUFYLENBQTBCLGVBQTFCLEVBQTBDO0FBQUNDLHVCQUFlLEVBQUMsQ0FBakI7QUFBbUJDLGFBQUssRUFBQyxDQUFDLENBQUM7QUFBQzlELG9CQUFVLEVBQUMsR0FBWjtBQUFnQjNCLGlCQUFPLEVBQUMsR0FBeEI7QUFBNEIwRixlQUFLLEVBQUM7QUFBbEMsU0FBRCxFQUF5QyxDQUF6QyxDQUFEO0FBQXpCLE9BQTFDO0FBQW1IalosT0FBQyxDQUFDNlksUUFBRixDQUFXQyxjQUFYLENBQTBCLFdBQTFCLEVBQXNDO0FBQUNDLHVCQUFlLEVBQUMsQ0FBakI7QUFBbUJDLGFBQUssRUFBQyxDQUFDLENBQUM7QUFBQ3pGLGlCQUFPLEVBQUMsR0FBVDtBQUFhMEYsZUFBSyxFQUFDO0FBQW5CLFNBQUQsRUFBNEIsQ0FBNUIsQ0FBRDtBQUF6QixPQUF0QztBQUFrR2paLE9BQUMsQ0FBQzZZLFFBQUYsQ0FBV0MsY0FBWCxDQUEwQixrQkFBMUIsRUFBNkM7QUFBQ0MsdUJBQWUsRUFBQyxDQUFqQjtBQUFtQkMsYUFBSyxFQUFDLENBQUMsQ0FBQztBQUFDOUQsb0JBQVUsRUFBQyxJQUFaO0FBQWlCK0QsZUFBSyxFQUFDTjtBQUF2QixTQUFELEVBQXdDLElBQXhDLENBQUQsRUFBK0MsQ0FBQztBQUFDekQsb0JBQVUsRUFBQztBQUFaLFNBQUQsRUFBc0IsSUFBdEIsQ0FBL0MsRUFBMkUsQ0FBQztBQUFDQSxvQkFBVSxFQUFDLE9BQVo7QUFBb0IrRCxlQUFLLEVBQUM7QUFBMUIsU0FBRCxFQUFpQyxJQUFqQyxDQUEzRTtBQUF6QixPQUE3QztBQUEyTGpaLE9BQUMsQ0FBQzZZLFFBQUYsQ0FBV0MsY0FBWCxDQUEwQix5QkFBMUIsRUFBb0Q7QUFBQ0MsdUJBQWUsRUFBQyxDQUFqQjtBQUFtQkMsYUFBSyxFQUFDLENBQUMsQ0FBQztBQUFDOUQsb0JBQVUsRUFBQyxPQUFaO0FBQW9CK0QsZUFBSyxFQUFDTjtBQUExQixTQUFELEVBQTJDLElBQTNDLENBQUQsRUFBa0QsQ0FBQztBQUFDekQsb0JBQVUsRUFBQyxPQUFaO0FBQW9CK0QsZUFBSyxFQUFDO0FBQTFCLFNBQUQsRUFBaUMsSUFBakMsQ0FBbEQ7QUFBekIsT0FBcEQ7QUFBeUtqWixPQUFDLENBQUM2WSxRQUFGLENBQVdDLGNBQVgsQ0FBMEIsZ0JBQTFCLEVBQTJDO0FBQUNDLHVCQUFlLEVBQUMsQ0FBakI7QUFBbUJDLGFBQUssRUFBQyxDQUFDLENBQUM7QUFBQzlELG9CQUFVLEVBQUMsS0FBWjtBQUFrQitELGVBQUssRUFBQ047QUFBeEIsU0FBRCxFQUF5QyxJQUF6QyxDQUFELEVBQWdELENBQUM7QUFBQ3pELG9CQUFVLEVBQUM7QUFBWixTQUFELEVBQW1CLElBQW5CLENBQWhELEVBQXlFLENBQUM7QUFBQ0Esb0JBQVUsRUFBQyxJQUFaO0FBQWlCK0QsZUFBSyxFQUFDO0FBQXZCLFNBQUQsRUFBOEIsSUFBOUIsQ0FBekU7QUFBekIsT0FBM0M7QUFBb0xqWixPQUFDLENBQUM2WSxRQUFGLENBQVdDLGNBQVgsQ0FBMEIsdUJBQTFCLEVBQWtEO0FBQUNDLHVCQUFlLEVBQUMsQ0FBakI7QUFBbUJDLGFBQUssRUFBQyxDQUFDLENBQUM7QUFBQzlELG9CQUFVLEVBQUMsSUFBWjtBQUFpQitELGVBQUssRUFBQ047QUFBdkIsU0FBRCxFQUF3QyxJQUF4QyxDQUFELEVBQStDLENBQUM7QUFBQ3pELG9CQUFVLEVBQUMsSUFBWjtBQUFpQitELGVBQUssRUFBQztBQUF2QixTQUFELEVBQThCLElBQTlCLENBQS9DO0FBQXpCLE9BQWxEO0FBQWlLalosT0FBQyxDQUFDNlksUUFBRixDQUFXQyxjQUFYLENBQTBCLG9CQUExQixFQUErQztBQUFDQyx1QkFBZSxFQUFDLENBQWpCO0FBQW1CQyxhQUFLLEVBQUMsQ0FBQyxDQUFDO0FBQUM5RCxvQkFBVSxFQUFDLElBQVo7QUFBaUIrRCxlQUFLLEVBQUNOO0FBQXZCLFNBQUQsRUFBd0MsSUFBeEMsQ0FBRCxFQUErQyxDQUFDO0FBQUN6RCxvQkFBVSxFQUFDO0FBQVosU0FBRCxFQUFxQixJQUFyQixDQUEvQyxFQUEwRSxDQUFDO0FBQUNBLG9CQUFVLEVBQUMsTUFBWjtBQUFtQitELGVBQUssRUFBQztBQUF6QixTQUFELEVBQWdDLElBQWhDLENBQTFFO0FBQXpCLE9BQS9DO0FBQTJMalosT0FBQyxDQUFDNlksUUFBRixDQUFXQyxjQUFYLENBQTBCLDJCQUExQixFQUFzRDtBQUFDQyx1QkFBZSxFQUFDLENBQWpCO0FBQW1CQyxhQUFLLEVBQUMsQ0FBQyxDQUFDLEVBQUQsRUFBSSxJQUFKLENBQUQsRUFBVyxDQUFDO0FBQUM5RCxvQkFBVSxFQUFDLE1BQVo7QUFBbUIrRCxlQUFLLEVBQUM7QUFBekIsU0FBRCxFQUFnQyxJQUFoQyxDQUFYO0FBQXpCLE9BQXREO0FBQW1JalosT0FBQyxDQUFDNlksUUFBRixDQUFXQyxjQUFYLENBQTBCLGtCQUExQixFQUE2QztBQUFDQyx1QkFBZSxFQUFDLENBQWpCO0FBQW1CQyxhQUFLLEVBQUMsQ0FBQyxDQUFDO0FBQUM5RCxvQkFBVSxFQUFDLE1BQVo7QUFBbUIrRCxlQUFLLEVBQUNOO0FBQXpCLFNBQUQsRUFBMEMsSUFBMUMsQ0FBRCxFQUFpRCxDQUFDO0FBQUN6RCxvQkFBVSxFQUFDO0FBQVosU0FBRCxFQUFtQixJQUFuQixDQUFqRCxFQUEwRSxDQUFDO0FBQUNBLG9CQUFVLEVBQUMsSUFBWjtBQUFpQitELGVBQUssRUFBQztBQUF2QixTQUFELEVBQThCLElBQTlCLENBQTFFO0FBQXpCLE9BQTdDOztBQUF1TCxVQUFJQyxVQUFVLEdBQUUsWUFBVTtBQUFDLFlBQUl4WCxRQUFRLEdBQUM7QUFBQ3lYLGtCQUFRLEVBQUNuWixDQUFDLENBQUMsY0FBRCxDQUFELENBQWtCeUIsSUFBbEIsQ0FBdUIsYUFBdkIsQ0FBVjtBQUFnRDJYLG9CQUFVLEVBQUNwWixDQUFDLENBQUMsYUFBRCxDQUE1RDtBQUE0RXFaLHFCQUFXLEVBQUMsZ0JBQXhGO0FBQXlHQyxpQkFBTyxFQUFDdFosQ0FBQyxDQUFDLGtCQUFELENBQUQsQ0FBc0JvRSxJQUF0QixDQUEyQix1QkFBM0IsQ0FBakg7QUFBcUttVixpQkFBTyxFQUFDdlosQ0FBQyxDQUFDLGtCQUFELENBQUQsQ0FBc0JvRSxJQUF0QixDQUEyQix1QkFBM0IsQ0FBN0s7QUFBaU9vVixpQkFBTyxFQUFDeFosQ0FBQyxDQUFDLGlCQUFELENBQTFPO0FBQThQZ00sZUFBSyxFQUFDLENBQXBRO0FBQXNReU4seUJBQWUsRUFBQyxDQUF0UjtBQUF3UjNHLHNCQUFZLEVBQUMsQ0FBclM7QUFBdVM0RyxtQkFBUyxFQUFDLEtBQWpUO0FBQXVUQywyQkFBaUIsRUFBQztBQUF6VSxTQUFiO0FBQTRWLFlBQUkzTixLQUFLLEdBQUMsQ0FBVjtBQUFBLFlBQVkwTixTQUFTLEdBQUMsS0FBdEI7QUFBNEIsZUFBTTtBQUFDbkosY0FBSSxFQUFDLGdCQUFVO0FBQUMsaUJBQUt4RCxJQUFMO0FBQWEsV0FBOUI7QUFBK0JBLGNBQUksRUFBQyxnQkFBVTtBQUFDckwsb0JBQVEsQ0FBQzBYLFVBQVQsQ0FBb0JRLEtBQXBCLEdBQTRCclcsUUFBNUIsQ0FBcUMsV0FBckM7O0FBQWtELGdCQUFHN0IsUUFBUSxDQUFDeVgsUUFBVCxJQUFtQixJQUF0QixFQUEyQjtBQUFDRCx3QkFBVSxDQUFDVyxjQUFYO0FBQTRCN1osZUFBQyxDQUFDbEIsTUFBRCxDQUFELENBQVVtQixFQUFWLENBQWEsMkJBQWIsRUFBeUNpWixVQUFVLENBQUNZLGFBQXBEO0FBQW9FOztBQUN4K0VwWSxvQkFBUSxDQUFDNFgsT0FBVCxDQUFpQnJaLEVBQWpCLENBQW9CLE9BQXBCLEVBQTRCaVosVUFBVSxDQUFDYSxTQUF2QztBQUFrRHJZLG9CQUFRLENBQUM2WCxPQUFULENBQWlCdFosRUFBakIsQ0FBb0IsT0FBcEIsRUFBNEJpWixVQUFVLENBQUNjLFNBQXZDO0FBQWtEaGEsYUFBQyxDQUFDRyxRQUFELENBQUQsQ0FBWUYsRUFBWixDQUFlLFNBQWYsRUFBeUIsVUFBU3NJLENBQVQsRUFBVztBQUFDLGtCQUFJMFIsT0FBTyxHQUFFMVIsQ0FBQyxDQUFDNkosS0FBRixJQUFTLEVBQVQsSUFBYTdKLENBQUMsQ0FBQzZKLEtBQUYsSUFBUyxFQUFuQztBQUFBLGtCQUF1QzhILE9BQU8sR0FBRTNSLENBQUMsQ0FBQzZKLEtBQUYsSUFBUyxFQUFULElBQWE3SixDQUFDLENBQUM2SixLQUFGLElBQVMsRUFBdEU7O0FBQTBFLGtCQUFHNkgsT0FBTyxJQUFFLENBQUN2WSxRQUFRLENBQUM2WCxPQUFULENBQWlCMVMsUUFBakIsQ0FBMEIsVUFBMUIsQ0FBYixFQUFtRDtBQUFDMEIsaUJBQUMsQ0FBQ0MsY0FBRjtBQUFtQjBRLDBCQUFVLENBQUNjLFNBQVg7QUFBd0IsZUFBL0YsTUFBb0csSUFBR0UsT0FBTyxJQUFHLENBQUN4WSxRQUFRLENBQUM0WCxPQUFULENBQWlCelMsUUFBakIsQ0FBMEIsVUFBMUIsQ0FBZCxFQUFxRDtBQUFDMEIsaUJBQUMsQ0FBQ0MsY0FBRjtBQUFtQjBRLDBCQUFVLENBQUNhLFNBQVg7QUFBd0I7QUFBQyxhQUFyVDtBQUF1VDNhLGtCQUFNLENBQUMsTUFBRCxDQUFOLENBQWVhLEVBQWYsQ0FBa0IsV0FBbEIsRUFBOEIsVUFBU3NJLENBQVQsRUFBVztBQUFDQSxlQUFDLENBQUNDLGNBQUY7QUFBbUJELGVBQUMsQ0FBQ3NFLGVBQUY7QUFBb0IscUJBQU8sS0FBUDtBQUFjLGFBQS9GO0FBQWlHLGdCQUFJQyxFQUFKO0FBQU8xTixrQkFBTSxDQUFDZSxRQUFELENBQU4sQ0FBaUI0TSxJQUFqQixDQUFzQixZQUF0QixFQUFtQyxVQUFTeEUsQ0FBVCxFQUFXO0FBQUN1RSxnQkFBRSxHQUFDdkUsQ0FBQyxDQUFDeUUsYUFBRixDQUFnQkMsT0FBaEIsQ0FBd0IsQ0FBeEIsRUFBMkJDLE9BQTlCO0FBQXVDLGFBQXRGO0FBQXdGOU4sa0JBQU0sQ0FBQ2UsUUFBRCxDQUFOLENBQWlCNE0sSUFBakIsQ0FBc0IsVUFBdEIsRUFBaUMsVUFBU3hFLENBQVQsRUFBVztBQUFDLGtCQUFJNEUsRUFBRSxHQUFDNUUsQ0FBQyxDQUFDeUUsYUFBRixDQUFnQkksY0FBaEIsQ0FBK0IsQ0FBL0IsRUFBa0NGLE9BQXpDOztBQUFpRCxrQkFBR0osRUFBRSxHQUFDSyxFQUFFLEdBQUMsQ0FBVCxFQUFXO0FBQUMrTCwwQkFBVSxDQUFDYyxTQUFYO0FBQXdCLGVBQXBDLE1BQXlDLElBQUdsTixFQUFFLEdBQUNLLEVBQUUsR0FBQyxDQUFULEVBQVc7QUFBQytMLDBCQUFVLENBQUNhLFNBQVg7QUFBd0I7QUFBQyxhQUE1SztBQUE4S2Isc0JBQVUsQ0FBQ2lCLGVBQVg7QUFBNkJqQixzQkFBVSxDQUFDa0IsY0FBWDtBQUE2QixXQUR3OEM7QUFDdjhDQSx3QkFBYyxFQUFDLDBCQUFVO0FBQUMxWSxvQkFBUSxDQUFDOFgsT0FBVCxDQUFpQmEsS0FBakIsQ0FBdUIsWUFBVTtBQUFDcmEsZUFBQyxDQUFDLElBQUQsQ0FBRCxDQUFRc2EsT0FBUixDQUFnQjVZLFFBQVEsQ0FBQzBYLFVBQXpCLEVBQXFDeFEsV0FBckMsQ0FBaUQsYUFBakQ7QUFBaUUsYUFBbkc7QUFBc0csV0FEdTBDO0FBQ3QwQzJSLHNCQUFZLEVBQUMsc0JBQVNDLE9BQVQsRUFBaUJDLFNBQWpCLEVBQTJCO0FBQUMsZ0JBQUlDLGdCQUFnQixHQUFDLGVBQXJCO0FBQUEsZ0JBQXFDQyxZQUFZLEdBQUMsYUFBbEQ7QUFBQSxnQkFBZ0VDLGVBQWUsR0FBQyxlQUFoRjtBQUFBLGdCQUFnRzFLLE1BQU0sR0FBQyxNQUF2RztBQUFBLGdCQUE4RzJLLFlBQVksR0FBQ25aLFFBQVEsQ0FBQ2lZLGlCQUFwSTs7QUFBc0osZ0JBQUdhLE9BQUgsRUFBVztBQUFDRSw4QkFBZ0IsR0FBQyx1QkFBakI7QUFBeUNDLDBCQUFZLEdBQUMseUJBQWI7QUFBdUNDLDZCQUFlLEdBQUMsMkJBQWhCO0FBQTZDLGFBQXpJLE1BQTZJO0FBQUNGLDhCQUFnQixHQUFFRCxTQUFTLElBQUUsTUFBWixHQUFvQixnQkFBcEIsR0FBcUMsa0JBQXREO0FBQXlFRSwwQkFBWSxHQUFDLGtCQUFiO0FBQWdDQyw2QkFBZSxHQUFDLG9CQUFoQjtBQUFzQzs7QUFDajZDLG1CQUFNLENBQUNGLGdCQUFELEVBQWtCQyxZQUFsQixFQUErQkMsZUFBL0IsRUFBK0NDLFlBQS9DLEVBQTREM0ssTUFBNUQsQ0FBTjtBQUEyRSxXQUZnc0U7QUFFL3JFMkosd0JBQWMsRUFBQywwQkFBVTtBQUFDLGdCQUFJaUIsWUFBWSxHQUFDcFosUUFBUSxDQUFDMFgsVUFBVCxDQUFvQjJCLE1BQXBCLENBQTJCLFlBQTNCLENBQWpCO0FBQUEsZ0JBQTBEQyxVQUFVLEdBQUNGLFlBQVksQ0FBQ0csT0FBYixDQUFxQnZaLFFBQVEsQ0FBQzBYLFVBQTlCLENBQXJFO0FBQUEsZ0JBQStHOEIsYUFBYSxHQUFDSixZQUFZLENBQUNLLE9BQWIsQ0FBcUJ6WixRQUFRLENBQUMwWCxVQUE5QixDQUE3SDtBQUFBLGdCQUF1S2dDLGVBQWUsR0FBQ2xDLFVBQVUsQ0FBQ3FCLFlBQVgsQ0FBd0IsS0FBeEIsQ0FBdkw7QUFBQSxnQkFBc05HLGdCQUFnQixHQUFDVSxlQUFlLENBQUMsQ0FBRCxDQUF0UDtBQUFBLGdCQUEwUFQsWUFBWSxHQUFDUyxlQUFlLENBQUMsQ0FBRCxDQUF0UjtBQUFBLGdCQUEwUlIsZUFBZSxHQUFDUSxlQUFlLENBQUMsQ0FBRCxDQUF6VDtBQUE2VE4sd0JBQVksQ0FBQ08sUUFBYixDQUFzQixLQUF0QixFQUE2QkMsUUFBN0IsQ0FBc0NaLGdCQUF0QyxFQUF1RCxDQUF2RCxFQUF5RCxZQUFVO0FBQUNJLDBCQUFZLENBQUN6YSxHQUFiLENBQWlCLFNBQWpCLEVBQTJCLENBQTNCO0FBQThCMmEsd0JBQVUsQ0FBQzNhLEdBQVgsQ0FBZSxTQUFmLEVBQXlCLENBQXpCO0FBQTRCNmEsMkJBQWEsQ0FBQzdhLEdBQWQsQ0FBa0IsU0FBbEIsRUFBNEIsQ0FBNUI7QUFBZ0MsYUFBOUo7QUFBZ0syYSxzQkFBVSxDQUFDSyxRQUFYLENBQW9CLEtBQXBCLEVBQTJCQyxRQUEzQixDQUFvQ1gsWUFBcEMsRUFBaUQsQ0FBakQ7QUFBb0RPLHlCQUFhLENBQUNHLFFBQWQsQ0FBdUIsS0FBdkIsRUFBOEJDLFFBQTlCLENBQXVDVixlQUF2QyxFQUF1RCxDQUF2RDtBQUEyRCxXQUZ5bEQ7QUFFeGxEZCx1QkFBYSxFQUFDLHVCQUFTdlIsQ0FBVCxFQUFXO0FBQUMsZ0JBQUcsQ0FBQ25KLE1BQU0sQ0FBQyxNQUFELENBQU4sQ0FBZXlILFFBQWYsQ0FBd0IsUUFBeEIsQ0FBSixFQUM3c0I7QUFBQyxrQkFBRzBCLENBQUMsQ0FBQ3lFLGFBQUYsQ0FBZ0JmLE1BQWhCLEdBQXVCLENBQXZCLElBQTBCMUQsQ0FBQyxDQUFDeUUsYUFBRixDQUFnQmIsVUFBaEIsR0FBMkIsQ0FBeEQsRUFBMEQ7QUFBQ0gscUJBQUs7QUFBSXVQLG9CQUFJLENBQUNDLEdBQUwsQ0FBU3hQLEtBQVQsS0FBaUJ0SyxRQUFRLENBQUMrWCxlQUEzQixJQUE2Q1AsVUFBVSxDQUFDYSxTQUFYLEVBQTdDO0FBQXFFLGVBQXhJLE1BQTRJO0FBQUMvTixxQkFBSztBQUFJQSxxQkFBSyxJQUFFdEssUUFBUSxDQUFDK1gsZUFBakIsSUFBbUNQLFVBQVUsQ0FBQ2MsU0FBWCxFQUFuQztBQUEyRDs7QUFDak4scUJBQU8sS0FBUDtBQUFjO0FBQUMsV0FKNHZFO0FBSTN2RUQsbUJBQVMsRUFBQyxtQkFBU3hSLENBQVQsRUFBVztBQUFDLG1CQUFPQSxDQUFQLEtBQVcsV0FBWCxJQUF3QkEsQ0FBQyxDQUFDQyxjQUFGLEVBQXhCO0FBQTJDLGdCQUFJc1MsWUFBWSxHQUFDcFosUUFBUSxDQUFDMFgsVUFBVCxDQUFvQjJCLE1BQXBCLENBQTJCLFlBQTNCLENBQWpCO0FBQUEsZ0JBQTBESyxlQUFlLEdBQUNsQyxVQUFVLENBQUNxQixZQUFYLENBQXdCQyxPQUF4QixFQUFnQyxNQUFoQyxDQUExRTtBQUFBLGdCQUFrSEEsT0FBTyxHQUFDLEtBQTFIO0FBQWdJTSx3QkFBWSxHQUFDTixPQUFPLEdBQUNNLFlBQVksQ0FBQ3JDLElBQWIsQ0FBa0IvVyxRQUFRLENBQUMwWCxVQUEzQixDQUFELEdBQXdDMEIsWUFBNUQ7O0FBQXlFLGdCQUFHLENBQUNwQixTQUFELElBQVksQ0FBQ29CLFlBQVksQ0FBQ1csRUFBYixDQUFnQixjQUFoQixDQUFoQixFQUFnRDtBQUFDL0IsdUJBQVMsR0FBQyxJQUFWO0FBQWVvQiwwQkFBWSxDQUFDbmEsV0FBYixDQUF5QixXQUF6QixFQUFzQzBhLFFBQXRDLENBQStDM1osUUFBUSxDQUFDMlgsV0FBeEQsRUFBcUVpQyxRQUFyRSxDQUE4RUYsZUFBZSxDQUFDLENBQUQsQ0FBN0YsRUFBaUdBLGVBQWUsQ0FBQyxDQUFELENBQWhILEVBQW9IQSxlQUFlLENBQUMsQ0FBRCxDQUFuSSxFQUF3SU0sR0FBeEksR0FBOEloRCxJQUE5SSxDQUFtSmhYLFFBQVEsQ0FBQzBYLFVBQTVKLEVBQXdLN1YsUUFBeEssQ0FBaUwsV0FBakwsRUFBOEw4WCxRQUE5TCxDQUF1TTNaLFFBQVEsQ0FBQzJYLFdBQWhOLEVBQTZOaUMsUUFBN04sQ0FBc09GLGVBQWUsQ0FBQyxDQUFELENBQXJQLEVBQXlQQSxlQUFlLENBQUMsQ0FBRCxDQUF4USxFQUE0UUEsZUFBZSxDQUFDLENBQUQsQ0FBM1IsRUFBK1IsWUFBVTtBQUFDMUIseUJBQVMsR0FBQyxLQUFWO0FBQWlCLGVBQTNUO0FBQTZUNUcsMEJBQVksR0FBQ3BSLFFBQVEsQ0FBQ29SLFlBQVQsR0FBc0IsQ0FBbkM7QUFBc0M7O0FBQzdyQm9HLHNCQUFVLENBQUN5QyxXQUFYO0FBQTBCLFdBTGl2RTtBQUtodkUzQixtQkFBUyxFQUFDLG1CQUFTelIsQ0FBVCxFQUFXO0FBQUMsbUJBQU9BLENBQVAsS0FBVyxXQUFYLElBQXdCQSxDQUFDLENBQUNDLGNBQUYsRUFBeEI7QUFBMkMsZ0JBQUlzUyxZQUFZLEdBQUNwWixRQUFRLENBQUMwWCxVQUFULENBQW9CMkIsTUFBcEIsQ0FBMkIsWUFBM0IsQ0FBakI7QUFBQSxnQkFBMERLLGVBQWUsR0FBQ2xDLFVBQVUsQ0FBQ3FCLFlBQVgsQ0FBd0JDLE9BQXhCLEVBQWdDLE1BQWhDLENBQTFFO0FBQUEsZ0JBQWtIQSxPQUFPLEdBQUMsS0FBMUg7O0FBQWdJLGdCQUFHLENBQUNkLFNBQUQsSUFBWSxDQUFDb0IsWUFBWSxDQUFDVyxFQUFiLENBQWdCLGVBQWhCLENBQWhCLEVBQWlEO0FBQUMvQix1QkFBUyxHQUFDLElBQVY7QUFBZW9CLDBCQUFZLENBQUNuYSxXQUFiLENBQXlCLFdBQXpCLEVBQXNDMGEsUUFBdEMsQ0FBK0MzWixRQUFRLENBQUMyWCxXQUF4RCxFQUFxRWlDLFFBQXJFLENBQThFRixlQUFlLENBQUMsQ0FBRCxDQUE3RixFQUFpR0EsZUFBZSxDQUFDLENBQUQsQ0FBaEgsRUFBcUhNLEdBQXJILEdBQTJIakQsSUFBM0gsQ0FBZ0kvVyxRQUFRLENBQUMwWCxVQUF6SSxFQUFxSjdWLFFBQXJKLENBQThKLFdBQTlKLEVBQTJLOFgsUUFBM0ssQ0FBb0wzWixRQUFRLENBQUMyWCxXQUE3TCxFQUEwTWlDLFFBQTFNLENBQW1ORixlQUFlLENBQUMsQ0FBRCxDQUFsTyxFQUFzT0EsZUFBZSxDQUFDLENBQUQsQ0FBclAsRUFBeVAsWUFBVTtBQUFDMUIseUJBQVMsR0FBQyxLQUFWO0FBQWlCLGVBQXJSO0FBQXVSNUcsMEJBQVksR0FBQ3BSLFFBQVEsQ0FBQ29SLFlBQVQsR0FBc0IsQ0FBbkM7QUFBc0M7O0FBQzFsQm9HLHNCQUFVLENBQUN5QyxXQUFYO0FBQTBCLFdBTml2RTtBQU1odkVBLHFCQUFXLEVBQUMsdUJBQVU7QUFBQzNQLGlCQUFLLEdBQUMsQ0FBTjtBQUFRa04sc0JBQVUsQ0FBQ2lCLGVBQVg7QUFBOEIsV0FObXJFO0FBTWxyRUEseUJBQWUsRUFBQywyQkFBVTtBQUFFelksb0JBQVEsQ0FBQzBYLFVBQVQsQ0FBb0IyQixNQUFwQixDQUEyQixZQUEzQixFQUF5Q1UsRUFBekMsQ0FBNEMsZ0JBQTVDLENBQUQsR0FBZ0UvWixRQUFRLENBQUM0WCxPQUFULENBQWlCL1YsUUFBakIsQ0FBMEIsVUFBMUIsQ0FBaEUsR0FBc0c3QixRQUFRLENBQUM0WCxPQUFULENBQWlCM1ksV0FBakIsQ0FBNkIsVUFBN0IsQ0FBdEc7QUFBZ0plLG9CQUFRLENBQUMwWCxVQUFULENBQW9CMkIsTUFBcEIsQ0FBMkIsWUFBM0IsRUFBeUNVLEVBQXpDLENBQTRDLGVBQTVDLENBQUQsR0FBK0QvWixRQUFRLENBQUM2WCxPQUFULENBQWlCaFcsUUFBakIsQ0FBMEIsVUFBMUIsQ0FBL0QsR0FBcUc3QixRQUFRLENBQUM2WCxPQUFULENBQWlCNVksV0FBakIsQ0FBNkIsVUFBN0IsQ0FBckc7QUFBK0k7QUFOeTNELFNBQU47QUFNLzJELE9BTjIrQyxFQUFmOztBQU14OUN1WSxnQkFBVSxDQUFDM0ksSUFBWDtBQUFtQixLQU4xVTtBQU00VXJQLHFCQUFpQixDQUFDQyxLQUFsQixDQUF3QkMsU0FBeEIsQ0FBa0MsOERBQWxDLEVBQWlHLFVBQVNDLE1BQVQsRUFBZ0I7QUFBQ2pDLFlBQU0sQ0FBQyxNQUFELENBQU4sQ0FBZW1FLFFBQWYsQ0FBd0Isc0JBQXhCOztBQUFnRCxlQUFTcVksU0FBVCxDQUFtQkMsTUFBbkIsRUFBMEI7QUFBQzdiLFNBQUMsQ0FBQyxlQUFELENBQUQsQ0FBbUJXLFdBQW5CLENBQStCLHNCQUEvQjtBQUF1RFgsU0FBQyxDQUFDLDhCQUE0QjZiLE1BQTVCLEdBQW1DLEdBQXBDLENBQUQsQ0FBMEN0WSxRQUExQyxDQUFtRCxzQkFBbkQ7QUFBNEU7O0FBQy91QnZELE9BQUMsQ0FBQyw0QkFBRCxDQUFELENBQWdDQyxFQUFoQyxDQUFtQyxPQUFuQyxFQUEyQyxZQUFVO0FBQUMsWUFBSTZTLFlBQVksR0FBQ2dKLE1BQU0sQ0FBQzliLENBQUMsQ0FBQyx1QkFBRCxDQUFELENBQTJCeUIsSUFBM0IsQ0FBZ0MsT0FBaEMsQ0FBRCxDQUF2QjtBQUFrRSxZQUFJc2EsV0FBVyxHQUFDL2IsQ0FBQyxDQUFDLGVBQUQsQ0FBRCxDQUFtQjZMLE1BQW5DO0FBQTBDaUgsb0JBQVk7O0FBQzlLLFlBQUdBLFlBQVksR0FBQ2lKLFdBQWhCLEVBQTRCO0FBQUNqSixzQkFBWSxHQUFDLENBQWI7QUFBZ0I7O0FBQzdDOEksaUJBQVMsQ0FBQzlJLFlBQUQsQ0FBVDtBQUF5QixPQUZ6Qjs7QUFFMkIsV0FBSSxJQUFJN08sQ0FBQyxHQUFDLENBQVYsRUFBWUEsQ0FBQyxJQUFFakUsQ0FBQyxDQUFDLGVBQUQsQ0FBRCxDQUFtQjZMLE1BQWxDLEVBQXlDNUgsQ0FBQyxFQUExQyxFQUE2QztBQUFDakUsU0FBQyxDQUFDLHFCQUFELENBQUQsQ0FBeUJ3RCxNQUF6QixDQUFnQyxnREFBOENTLENBQTlDLEdBQWdELFVBQWhGO0FBQTRGOztBQUNyS2dFLGdCQUFVLENBQUMsWUFBVTtBQUFDakksU0FBQyxDQUFDLGNBQUQsQ0FBRCxDQUFrQnVELFFBQWxCLENBQTJCLHFCQUEzQjtBQUFtRCxPQUEvRCxFQUFnRSxJQUFoRSxDQUFWO0FBQWlGLEtBSjhWO0FBSTVWckMscUJBQWlCLENBQUNDLEtBQWxCLENBQXdCQyxTQUF4QixDQUFrQyxpRUFBbEMsRUFBb0csVUFBU0MsTUFBVCxFQUFnQjtBQUFDakMsWUFBTSxDQUFDLE1BQUQsQ0FBTixDQUFlbUUsUUFBZixDQUF3QixzQkFBeEI7O0FBQWlELG1CQUFVO0FBQUMsWUFBSXlZLE9BQU8sR0FBQzdiLFFBQVEsQ0FBQ3VRLGdCQUFULENBQTBCLFFBQTFCLENBQVo7QUFBZ0QsWUFBSXVMLFNBQVMsR0FBQzliLFFBQVEsQ0FBQ3VRLGdCQUFULENBQTBCLGlCQUExQixDQUFkO0FBQTJELFlBQUl3TCxXQUFXLEdBQUNGLE9BQU8sQ0FBQ25RLE1BQXhCO0FBQStCLFlBQUlzUSxTQUFTLEdBQUMsSUFBZDtBQUFtQixZQUFJQyxjQUFjLEdBQUMsS0FBbkI7QUFBeUIsV0FBRzFILEtBQUgsQ0FBU0MsSUFBVCxDQUFjcUgsT0FBZCxFQUF1QnhOLE9BQXZCLENBQStCLFVBQVM2TixHQUFULEVBQWFqVCxLQUFiLEVBQW1CO0FBQUMsY0FBSW5GLENBQUMsR0FBQ21GLEtBQUssR0FBQyxDQUFaO0FBQWNpVCxhQUFHLENBQUNwSixTQUFKLENBQWNHLEdBQWQsQ0FBa0IsV0FBU25QLENBQTNCO0FBQThCb1ksYUFBRyxDQUFDQyxPQUFKLENBQVk3TixLQUFaLEdBQWtCeEssQ0FBbEI7QUFBcUIsU0FBcEg7QUFBc0gsV0FBR3lRLEtBQUgsQ0FBU0MsSUFBVCxDQUFjc0gsU0FBZCxFQUF5QnpOLE9BQXpCLENBQWlDLFVBQVM2TixHQUFULEVBQWE7QUFBQ0EsYUFBRyxDQUFDNVAsZ0JBQUosQ0FBcUIsT0FBckIsRUFBNkI4UCxtQkFBN0I7QUFBbUQsU0FBbEc7O0FBQW9HLGlCQUFTQSxtQkFBVCxHQUE4QjtBQUFDLGNBQUdILGNBQUgsRUFBa0I7QUFBT0Esd0JBQWMsR0FBQyxJQUFmO0FBQW9CLGNBQUlJLFFBQVEsR0FBQyxJQUFiO0FBQWtCLGNBQUlDLE9BQU8sR0FBQ0QsUUFBUSxDQUFDdkosU0FBVCxDQUFtQnlKLFFBQW5CLENBQTRCLFVBQTVCLENBQVo7QUFBb0QsY0FBSUMsVUFBVSxHQUFDeGMsUUFBUSxDQUFDMFEsYUFBVCxDQUF1QixrQkFBdkIsQ0FBZjtBQUEwRCxjQUFJekgsS0FBSyxHQUFDLENBQUN1VCxVQUFVLENBQUNMLE9BQVgsQ0FBbUI3TixLQUE5QjtBQUFxQ2dPLGlCQUFELEdBQVVyVCxLQUFLLEVBQWYsR0FBa0JBLEtBQUssRUFBdkI7QUFBMEIsY0FBR0EsS0FBSyxHQUFDLENBQVQsRUFBV0EsS0FBSyxHQUFDOFMsV0FBTjtBQUFrQixjQUFHOVMsS0FBSyxHQUFDOFMsV0FBVCxFQUFxQjlTLEtBQUssR0FBQyxDQUFOO0FBQVEsY0FBSXdULFVBQVUsR0FBQ3pjLFFBQVEsQ0FBQzBRLGFBQVQsQ0FBdUIsWUFBVXpILEtBQWpDLENBQWY7QUFBdURvVCxrQkFBUSxDQUFDdkosU0FBVCxDQUFtQkcsR0FBbkIsQ0FBdUIsYUFBdkI7QUFBc0N1SixvQkFBVSxDQUFDMUosU0FBWCxDQUFxQkMsTUFBckIsQ0FBNEIsV0FBNUIsRUFBd0MsZ0JBQXhDO0FBQTBEL1Msa0JBQVEsQ0FBQzBRLGFBQVQsQ0FBdUIsZ0JBQXZCLEVBQXlDb0MsU0FBekMsQ0FBbURDLE1BQW5ELENBQTBELFNBQTFEO0FBQXFFMEosb0JBQVUsQ0FBQzNKLFNBQVgsQ0FBcUJHLEdBQXJCLENBQXlCLFdBQXpCO0FBQXNDLGNBQUcsQ0FBQ3FKLE9BQUosRUFBWUcsVUFBVSxDQUFDM0osU0FBWCxDQUFxQkcsR0FBckIsQ0FBeUIsZ0JBQXpCO0FBQTJDLGNBQUl5SixTQUFTLEdBQUN6VCxLQUFLLEdBQUMsQ0FBcEI7QUFBc0IsY0FBR3lULFNBQVMsR0FBQyxDQUFiLEVBQWVBLFNBQVMsR0FBQ1gsV0FBVjtBQUFzQi9iLGtCQUFRLENBQUMwUSxhQUFULENBQXVCLFlBQVVnTSxTQUFqQyxFQUE0QzVKLFNBQTVDLENBQXNERyxHQUF0RCxDQUEwRCxTQUExRDtBQUFxRW5MLG9CQUFVLENBQUMsWUFBVTtBQUFDdVUsb0JBQVEsQ0FBQ3ZKLFNBQVQsQ0FBbUJDLE1BQW5CLENBQTBCLGFBQTFCO0FBQXlDa0osMEJBQWMsR0FBQyxLQUFmO0FBQXNCLFdBQTNFLEVBQTRFRCxTQUFTLEdBQUMsSUFBdEYsQ0FBVjtBQUF1Rzs7QUFBQTtBQUFFLE9BQWp3QyxHQUFEO0FBQXV3QyxLQUE1NkM7QUFBODZDamIscUJBQWlCLENBQUNDLEtBQWxCLENBQXdCQyxTQUF4QixDQUFrQyxnRUFBbEMsRUFBbUcsVUFBU0MsTUFBVCxFQUFnQjtBQUFDakMsWUFBTSxDQUFDLE1BQUQsQ0FBTixDQUFlbUUsUUFBZixDQUF3QixzQkFBeEI7QUFBZ0RuRSxZQUFNLENBQUMsTUFBRCxDQUFOLENBQWVtRSxRQUFmLENBQXdCLGlDQUF4QjtBQUEyRCxVQUFJdVosT0FBTyxHQUFDOWMsQ0FBQyxDQUFDLFNBQUQsQ0FBYjtBQUF5QixVQUFJK2MsV0FBVyxHQUFDL2MsQ0FBQyxDQUFDLGNBQUQsQ0FBakI7QUFBa0MsVUFBSWdkLGFBQWEsR0FBQ2hkLENBQUMsQ0FBQyxnQkFBRCxDQUFuQjtBQUFzQyxVQUFJdU0sYUFBYSxHQUFDLEdBQWxCO0FBQXNCdVEsYUFBTyxDQUFDN2MsRUFBUixDQUFXLE1BQVgsRUFBa0IsVUFBU2dkLEtBQVQsRUFBZTtBQUFDQyxnQkFBUSxDQUFDQyxFQUFULENBQVluZCxDQUFDLENBQUMsY0FBRCxDQUFiLEVBQThCLEdBQTlCLEVBQWtDO0FBQUNvZCxvQkFBVSxFQUFDO0FBQVosU0FBbEM7QUFBa0RGLGdCQUFRLENBQUNDLEVBQVQsQ0FBWW5kLENBQUMsQ0FBQyxlQUFELENBQWIsRUFBK0IsR0FBL0IsRUFBbUM7QUFBQzJPLFdBQUMsRUFBQyxDQUFIO0FBQUswTyxnQkFBTSxFQUFDO0FBQVosU0FBbkM7QUFBb0QsT0FBeEk7QUFBMElQLGFBQU8sQ0FBQzdjLEVBQVIsQ0FBVyxjQUFYLEVBQTBCLFVBQVMrVCxLQUFULEVBQWVpSixLQUFmLEVBQXFCbkssWUFBckIsRUFBa0NrSCxTQUFsQyxFQUE0QztBQUFDa0QsZ0JBQVEsQ0FBQ0MsRUFBVCxDQUFZbmQsQ0FBQyxDQUFDLGNBQUQsQ0FBYixFQUE4QixHQUE5QixFQUFrQztBQUFDb2Qsb0JBQVUsRUFBQztBQUFaLFNBQWxDO0FBQWtERixnQkFBUSxDQUFDQyxFQUFULENBQVluZCxDQUFDLENBQUMsZUFBRCxDQUFiLEVBQStCLEdBQS9CLEVBQW1DO0FBQUMyTyxXQUFDLEVBQUM7QUFBSCxTQUFuQztBQUEyQyxPQUFwSztBQUFzS21PLGFBQU8sQ0FBQzdjLEVBQVIsQ0FBVyxhQUFYLEVBQXlCLFVBQVMrVCxLQUFULEVBQWVpSixLQUFmLEVBQXFCbkssWUFBckIsRUFBa0M7QUFBQ29LLGdCQUFRLENBQUNDLEVBQVQsQ0FBWW5kLENBQUMsQ0FBQyxjQUFELENBQWIsRUFBOEIsR0FBOUIsRUFBa0M7QUFBQ29kLG9CQUFVLEVBQUM7QUFBWixTQUFsQztBQUFrRHBkLFNBQUMsQ0FBQyxjQUFELENBQUQsQ0FBa0JLLEdBQWxCLENBQXNCLFNBQXRCLEVBQWdDLEdBQWhDO0FBQXFDNmMsZ0JBQVEsQ0FBQ0MsRUFBVCxDQUFZbmQsQ0FBQyxDQUFDLGVBQUQsQ0FBYixFQUErQixHQUEvQixFQUFtQztBQUFDMk8sV0FBQyxFQUFDLENBQUg7QUFBSzBPLGdCQUFNLEVBQUM7QUFBWixTQUFuQztBQUFvRCxPQUF2TTtBQUF5TSxVQUFJQyxpQkFBaUIsR0FBQ3RkLENBQUMsQ0FBQyxTQUFELENBQUQsQ0FBYTRDLElBQWIsQ0FBa0IsaUJBQWxCLENBQXRCO0FBQTJELFVBQUkyYSxPQUFPLEdBQUMsS0FBWjs7QUFBa0IsVUFBR0QsaUJBQWlCLElBQUUsS0FBdEIsRUFDNzVFO0FBQUNDLGVBQU8sR0FBQyxJQUFSO0FBQWM7O0FBQ2YsVUFBSUMsaUJBQWlCLEdBQUN4ZCxDQUFDLENBQUMsU0FBRCxDQUFELENBQWE0QyxJQUFiLENBQWtCLGlCQUFsQixDQUF0QjtBQUEyRCxVQUFJNmEsU0FBUyxHQUFDLEtBQWQ7O0FBQW9CLFVBQUdELGlCQUFpQixJQUFFLEtBQXRCLEVBQy9FO0FBQUNDLGlCQUFTLEdBQUMsSUFBVjtBQUFnQjs7QUFDakIsVUFBSUMsZUFBZSxHQUFDMWQsQ0FBQyxDQUFDLFNBQUQsQ0FBRCxDQUFhNEMsSUFBYixDQUFrQixlQUFsQixDQUFwQjtBQUF1RCxVQUFJK2EsV0FBVyxHQUFDLEtBQWhCO0FBQXNCLFVBQUlDLGVBQWUsR0FBQyxDQUFwQjs7QUFBc0IsVUFBRyxPQUFPRixlQUFQLElBQXdCLFdBQTNCLEVBQXVDO0FBQUNDLG1CQUFXLEdBQUMsSUFBWjtBQUFpQkMsdUJBQWUsR0FBQ0YsZUFBaEI7QUFBaUM7O0FBQzdMWixhQUFPLENBQUNHLEtBQVIsQ0FBYztBQUFDdlosYUFBSyxFQUFDNkksYUFBUDtBQUFxQnNSLGlCQUFTLEVBQUMsSUFBL0I7QUFBb0NDLFlBQUksRUFBQ1AsT0FBekM7QUFBaURRLGNBQU0sRUFBQ04sU0FBeEQ7QUFBa0VPLHNCQUFjLEVBQUMsSUFBakY7QUFBc0ZDLG9CQUFZLEVBQUMsSUFBbkc7QUFBd0c3UyxnQkFBUSxFQUFDdVMsV0FBakg7QUFBNkhPLHFCQUFhLEVBQUNOLGVBQTNJO0FBQTJKTyxlQUFPLEVBQUM7QUFBbkssT0FBZDtBQUNBbmUsT0FBQyxDQUFDLGFBQUQsQ0FBRCxDQUFpQkMsRUFBakIsQ0FBb0IsWUFBcEIsRUFBaUMsWUFBVTtBQUFDaWQsZ0JBQVEsQ0FBQ0MsRUFBVCxDQUFZbmQsQ0FBQyxDQUFDLGNBQUQsQ0FBYixFQUE4QixHQUE5QixFQUFrQztBQUFDb2Qsb0JBQVUsRUFBQyxPQUFaO0FBQW9CZ0IsY0FBSSxFQUFDQyxJQUFJLENBQUNDO0FBQTlCLFNBQWxDO0FBQTBFcEIsZ0JBQVEsQ0FBQ0MsRUFBVCxDQUFZbmQsQ0FBQyxDQUFDLGdCQUFELENBQWIsRUFBZ0MsR0FBaEMsRUFBb0M7QUFBQzJPLFdBQUMsRUFBQyxDQUFDLEdBQUo7QUFBUXlQLGNBQUksRUFBQ0MsSUFBSSxDQUFDQztBQUFsQixTQUFwQztBQUFpRSxPQUF2TDtBQUF5THRlLE9BQUMsQ0FBQyxhQUFELENBQUQsQ0FBaUJDLEVBQWpCLENBQW9CLFlBQXBCLEVBQWlDLFlBQVU7QUFBQ2lkLGdCQUFRLENBQUNDLEVBQVQsQ0FBWW5kLENBQUMsQ0FBQyxjQUFELENBQWIsRUFBOEIsR0FBOUIsRUFBa0M7QUFBQ29kLG9CQUFVLEVBQUMsQ0FBWjtBQUFjZ0IsY0FBSSxFQUFDRyxJQUFJLENBQUNDO0FBQXhCLFNBQWxDO0FBQXNFdEIsZ0JBQVEsQ0FBQ0MsRUFBVCxDQUFZbmQsQ0FBQyxDQUFDLGdCQUFELENBQWIsRUFBZ0MsR0FBaEMsRUFBb0M7QUFBQzJPLFdBQUMsRUFBQyxDQUFIO0FBQUt5UCxjQUFJLEVBQUNHLElBQUksQ0FBQ0M7QUFBZixTQUFwQztBQUFnRSxPQUFsTDtBQUFvTHhlLE9BQUMsQ0FBQyxhQUFELENBQUQsQ0FBaUJDLEVBQWpCLENBQW9CLFlBQXBCLEVBQWlDLFlBQVU7QUFBQ2lkLGdCQUFRLENBQUNDLEVBQVQsQ0FBWW5kLENBQUMsQ0FBQyxjQUFELENBQWIsRUFBOEIsR0FBOUIsRUFBa0M7QUFBQ29kLG9CQUFVLEVBQUMsUUFBWjtBQUFxQmdCLGNBQUksRUFBQ0MsSUFBSSxDQUFDQztBQUEvQixTQUFsQztBQUEyRXBCLGdCQUFRLENBQUNDLEVBQVQsQ0FBWW5kLENBQUMsQ0FBQyxnQkFBRCxDQUFiLEVBQWdDLEdBQWhDLEVBQW9DO0FBQUMyTyxXQUFDLEVBQUMsR0FBSDtBQUFPeVAsY0FBSSxFQUFDQyxJQUFJLENBQUNDO0FBQWpCLFNBQXBDO0FBQWdFLE9BQXZMO0FBQXlMdGUsT0FBQyxDQUFDLGFBQUQsQ0FBRCxDQUFpQkMsRUFBakIsQ0FBb0IsWUFBcEIsRUFBaUMsWUFBVTtBQUFDaWQsZ0JBQVEsQ0FBQ0MsRUFBVCxDQUFZbmQsQ0FBQyxDQUFDLGNBQUQsQ0FBYixFQUE4QixHQUE5QixFQUFrQztBQUFDb2Qsb0JBQVUsRUFBQyxDQUFaO0FBQWNnQixjQUFJLEVBQUNDLElBQUksQ0FBQ0c7QUFBeEIsU0FBbEM7QUFBc0V0QixnQkFBUSxDQUFDQyxFQUFULENBQVluZCxDQUFDLENBQUMsZ0JBQUQsQ0FBYixFQUFnQyxHQUFoQyxFQUFvQztBQUFDMk8sV0FBQyxFQUFDLENBQUg7QUFBS3lQLGNBQUksRUFBQ0MsSUFBSSxDQUFDRztBQUFmLFNBQXBDO0FBQWdFLE9BQWxMO0FBQXFMLEtBTnN5QjtBQU1weUJ0ZCxxQkFBaUIsQ0FBQ0MsS0FBbEIsQ0FBd0JDLFNBQXhCLENBQWtDLG1FQUFsQyxFQUFzRyxVQUFTQyxNQUFULEVBQWdCO0FBQUNqQyxZQUFNLENBQUMsTUFBRCxDQUFOLENBQWVtRSxRQUFmLENBQXdCLHNCQUF4QjtBQUFnRCxVQUFJdVosT0FBTyxHQUFDOWMsQ0FBQyxDQUFDLG9CQUFELENBQWI7QUFBQSxVQUFvQ3llLFFBQVEsR0FBQ3plLENBQUMsQ0FBQyxPQUFELEVBQVM4YyxPQUFULENBQUQsQ0FBbUJqUixNQUFoRTtBQUFBLFVBQXVFNlMsUUFBUSxHQUFDLEtBQWhGO0FBQUEsVUFBc0ZDLFFBQXRGO0FBQUEsVUFBK0ZDLGFBQS9GO0FBQTZHQyxrQkFBWSxHQUFDN2UsQ0FBQyxDQUFDLFlBQUQsQ0FBRCxDQUFnQjhlLEtBQWhCLEdBQXdCdmIsUUFBeEIsQ0FBaUMsaUJBQWpDLEVBQW9Ed2IsUUFBcEQsQ0FBNkQvZSxDQUFDLENBQUMsa0JBQUQsQ0FBOUQsQ0FBYjtBQUFpR2dmLGdCQUFVLEdBQUNoZixDQUFDLENBQUMsT0FBRCxFQUFTNmUsWUFBVCxDQUFELENBQXdCSSxPQUF4QixFQUFYO0FBQTZDQyxrQkFBWSxHQUFDRixVQUFVLENBQUNHLE9BQVgsRUFBYjtBQUFrQ25mLE9BQUMsQ0FBQyxTQUFELEVBQVc2ZSxZQUFYLENBQUQsQ0FBMEJPLElBQTFCLENBQStCLEVBQS9COztBQUFtQyxXQUFJbmIsQ0FBQyxHQUFDLENBQU4sRUFBUUEsQ0FBQyxHQUFDd2EsUUFBVixFQUFtQnhhLENBQUMsRUFBcEIsRUFBdUI7QUFBQ2pFLFNBQUMsQ0FBQ2tmLFlBQVksQ0FBQ2piLENBQUQsQ0FBYixDQUFELENBQW1COGEsUUFBbkIsQ0FBNEIvZSxDQUFDLENBQUMsU0FBRCxFQUFXNmUsWUFBWCxDQUE3QjtBQUF3RDs7QUFDcHhDL0IsYUFBTyxDQUFDdlosUUFBUixDQUFpQixnQkFBakI7QUFBbUN2RCxPQUFDLENBQUMsaUJBQUQsQ0FBRCxDQUFxQmlkLEtBQXJCLENBQTJCO0FBQUNvQyxnQkFBUSxFQUFDLElBQVY7QUFBZUMsdUJBQWUsRUFBQyxJQUEvQjtBQUFvQ3ZCLGNBQU0sRUFBQyxLQUEzQztBQUFpRHdCLGdCQUFRLEVBQUMsSUFBMUQ7QUFBK0R6QixZQUFJLEVBQUMsSUFBcEU7QUFBeUVwYSxhQUFLLEVBQUMsSUFBL0U7QUFBb0Z5YSxlQUFPLEVBQUM7QUFBNUYsT0FBM0IsRUFBd0psZSxFQUF4SixDQUEySixjQUEzSixFQUEwSyxVQUFTK1QsS0FBVCxFQUFlaUosS0FBZixFQUFxQm5LLFlBQXJCLEVBQWtDa0gsU0FBbEMsRUFBNEM7QUFBQyxZQUFHbEgsWUFBWSxHQUFDa0gsU0FBYixJQUF3QkEsU0FBUyxJQUFFLENBQW5DLElBQXNDbEgsWUFBWSxJQUFFMkwsUUFBUSxHQUFDLENBQWhFLEVBQWtFO0FBQUN6ZSxXQUFDLENBQUMsMEJBQUQsQ0FBRCxDQUE4QmlkLEtBQTlCLENBQW9DLFdBQXBDLEVBQWdELENBQUMsQ0FBakQ7QUFBb0RqZCxXQUFDLENBQUMsaUJBQUQsQ0FBRCxDQUFxQmlkLEtBQXJCLENBQTJCLFdBQTNCLEVBQXVDd0IsUUFBdkM7QUFBa0QsU0FBekssTUFBOEssSUFBRzNMLFlBQVksR0FBQ2tILFNBQWIsSUFBd0JsSCxZQUFZLElBQUUsQ0FBdEMsSUFBeUNrSCxTQUFTLElBQUV5RSxRQUFRLEdBQUMsQ0FBaEUsRUFBa0U7QUFBQ3plLFdBQUMsQ0FBQywwQkFBRCxDQUFELENBQThCaWQsS0FBOUIsQ0FBb0MsV0FBcEMsRUFBZ0R3QixRQUFoRDtBQUEwRHplLFdBQUMsQ0FBQyxpQkFBRCxDQUFELENBQXFCaWQsS0FBckIsQ0FBMkIsV0FBM0IsRUFBdUMsQ0FBQyxDQUF4QztBQUE0QyxTQUF6SyxNQUE2SztBQUFDamQsV0FBQyxDQUFDLDBCQUFELENBQUQsQ0FBOEJpZCxLQUE5QixDQUFvQyxXQUFwQyxFQUFnRHdCLFFBQVEsR0FBQyxDQUFULEdBQVd6RSxTQUEzRDtBQUFzRWhhLFdBQUMsQ0FBQyxpQkFBRCxDQUFELENBQXFCaWQsS0FBckIsQ0FBMkIsV0FBM0IsRUFBdUNqRCxTQUF2QztBQUFtRDtBQUFDLE9BQTdxQixFQUErcUIvWixFQUEvcUIsQ0FBa3JCLFlBQWxyQixFQUErckIsVUFBUytULEtBQVQsRUFBZTtBQUFDQSxhQUFLLENBQUN4TCxjQUFOOztBQUF1QixZQUFHd0wsS0FBSyxDQUFDd0wsTUFBTixHQUFhLENBQWIsSUFBZ0J4TCxLQUFLLENBQUM5SCxNQUFOLEdBQWEsQ0FBaEMsRUFBa0M7QUFBQ2xNLFdBQUMsQ0FBQyxJQUFELENBQUQsQ0FBUWlkLEtBQVIsQ0FBYyxXQUFkO0FBQTRCLFNBQS9ELE1BQW9FLElBQUdqSixLQUFLLENBQUN3TCxNQUFOLEdBQWEsQ0FBYixJQUFnQnhMLEtBQUssQ0FBQzlILE1BQU4sR0FBYSxDQUFoQyxFQUFrQztBQUFDbE0sV0FBQyxDQUFDLElBQUQsQ0FBRCxDQUFRaWQsS0FBUixDQUFjLFdBQWQ7QUFBNEI7O0FBQUE7QUFBRSxPQUEzMkIsRUFBNjJCaGQsRUFBNzJCLENBQWczQixzQkFBaDNCLEVBQXU0QixZQUFVO0FBQUN5ZSxnQkFBUSxHQUFDLElBQVQ7QUFBY0MsZ0JBQVEsR0FBQzNlLENBQUMsQ0FBQyxjQUFELEVBQWdCOGMsT0FBaEIsQ0FBRCxDQUEwQnpjLEdBQTFCLENBQThCLFdBQTlCLENBQVQ7QUFBb0RzZSxnQkFBUSxHQUFDN2QsUUFBUSxDQUFDNmQsUUFBUSxDQUFDYyxLQUFULENBQWUsR0FBZixFQUFvQixDQUFwQixDQUFELENBQWpCO0FBQTBDYixxQkFBYSxHQUFDNWUsQ0FBQyxDQUFDLCtCQUFELENBQUQsQ0FBbUNLLEdBQW5DLENBQXVDLFdBQXZDLENBQWQ7QUFBa0V1ZSxxQkFBYSxHQUFDOWQsUUFBUSxDQUFDOGQsYUFBYSxDQUFDYSxLQUFkLENBQW9CLEdBQXBCLEVBQXlCLENBQXpCLENBQUQsQ0FBdEI7QUFBcUQsT0FBcm5DLEVBQXVuQ3hmLEVBQXZuQyxDQUEwbkMscUJBQTFuQyxFQUFncEMsWUFBVTtBQUFDLFlBQUd5ZSxRQUFILEVBQVk7QUFBQ2dCLHFCQUFXLEdBQUMxZixDQUFDLENBQUMsOEJBQUQsQ0FBRCxDQUFrQ0ssR0FBbEMsQ0FBc0MsV0FBdEMsQ0FBWjtBQUErRHFmLHFCQUFXLEdBQUM1ZSxRQUFRLENBQUM0ZSxXQUFXLENBQUNELEtBQVosQ0FBa0IsR0FBbEIsRUFBdUIsQ0FBdkIsQ0FBRCxDQUFwQjtBQUFnREUsc0JBQVksR0FBQ0QsV0FBVyxHQUFDZixRQUF6QjtBQUFrQzNlLFdBQUMsQ0FBQywrQkFBRCxDQUFELENBQW1DSyxHQUFuQyxDQUF1QztBQUFDLHlCQUFZLDRCQUEwQnVlLGFBQWEsR0FBQ2UsWUFBeEMsSUFBc0Q7QUFBbkUsV0FBdkM7QUFBaUg7QUFBQyxPQUEzNkMsRUFBNjZDMWYsRUFBNzZDLENBQWc3Qyw2QkFBaDdDLEVBQTg4QyxZQUFVO0FBQUN5ZSxnQkFBUSxHQUFDLEtBQVQ7QUFBZ0IsT0FBeitDO0FBQTIrQzFlLE9BQUMsQ0FBQywwQkFBRCxDQUFELENBQThCaWQsS0FBOUIsQ0FBb0M7QUFBQzJDLGFBQUssRUFBQyxLQUFQO0FBQWFQLGdCQUFRLEVBQUMsSUFBdEI7QUFBMkJ0QixjQUFNLEVBQUMsS0FBbEM7QUFBd0N3QixnQkFBUSxFQUFDLElBQWpEO0FBQXNEN2IsYUFBSyxFQUFDLEdBQTVEO0FBQWdFeWEsZUFBTyxFQUFDLDhCQUF4RTtBQUF1RzBCLG9CQUFZLEVBQUNwQixRQUFRLEdBQUM7QUFBN0gsT0FBcEM7QUFBcUt6ZSxPQUFDLENBQUMsaUJBQUQsQ0FBRCxDQUFxQmlkLEtBQXJCLENBQTJCO0FBQUMyQyxhQUFLLEVBQUMsS0FBUDtBQUFhUCxnQkFBUSxFQUFDLElBQXRCO0FBQTJCdEIsY0FBTSxFQUFDLEtBQWxDO0FBQXdDd0IsZ0JBQVEsRUFBQyxJQUFqRDtBQUFzRDdiLGFBQUssRUFBQyxHQUE1RDtBQUFnRXlhLGVBQU8sRUFBQztBQUF4RSxPQUEzQjtBQUFxSSxLQUQzbEM7QUFDNmxDamQscUJBQWlCLENBQUNDLEtBQWxCLENBQXdCQyxTQUF4QixDQUFrQyxtRUFBbEMsRUFBc0csVUFBU0MsTUFBVCxFQUFnQjtBQUFDakMsWUFBTSxDQUFDLE1BQUQsQ0FBTixDQUFlbUUsUUFBZixDQUF3QixzQkFBeEI7QUFBZ0QsVUFBSXVjLFFBQVEsR0FBQyxJQUFJclYsTUFBSixDQUFXLG1CQUFYLEVBQStCO0FBQUNnUSxpQkFBUyxFQUFDLFVBQVg7QUFBc0IxUCxZQUFJLEVBQUMsSUFBM0I7QUFBZ0N3QyxrQkFBVSxFQUFDO0FBQUN1QyxZQUFFLEVBQUMsb0JBQUo7QUFBeUJqTyxjQUFJLEVBQUMsU0FBOUI7QUFBd0NrZSxtQkFBUyxFQUFDO0FBQWxELFNBQTNDO0FBQW1HQyxnQkFBUSxFQUFDO0FBQUNDLGlCQUFPLEVBQUMsSUFBVDtBQUFjQyx3QkFBYyxFQUFDO0FBQTdCLFNBQTVHO0FBQWlKbFYsa0JBQVUsRUFBQyxJQUE1SjtBQUFpS3RILGFBQUssRUFBQyxJQUF2SztBQUE0S3ljLDJCQUFtQixFQUFDLElBQWhNO0FBQXFNaFosZ0JBQVEsRUFBQyxJQUE5TTtBQUFtTmlFLGdCQUFRLEVBQUMsS0FBNU47QUFBa09iLGNBQU0sRUFBQyxPQUF6TztBQUFpUDZWLGtCQUFVLEVBQUM7QUFBQ0MsZ0JBQU0sRUFBQztBQUFSO0FBQTVQLE9BQS9CLENBQWI7QUFBNFQsS0FBbmU7QUFBcWVuZixxQkFBaUIsQ0FBQ0MsS0FBbEIsQ0FBd0JDLFNBQXhCLENBQWtDLHFFQUFsQyxFQUF3RyxVQUFTQyxNQUFULEVBQWdCO0FBQUNqQyxZQUFNLENBQUMsK0JBQUQsQ0FBTixDQUF3Q2tCLElBQXhDLENBQTZDLFlBQVU7QUFBQyxZQUFJZ2dCLE1BQU0sR0FBQ2xoQixNQUFNLENBQUMsSUFBRCxDQUFOLENBQWFnRixJQUFiLENBQWtCLFNBQWxCLENBQVg7QUFBQSxZQUF3Q21LLE1BQU0sR0FBQytSLE1BQU0sQ0FBQ2xjLElBQVAsQ0FBWSxJQUFaLENBQS9DO0FBQUEsWUFBaUV3TSxHQUFHLEdBQUMwUCxNQUFNLENBQUNsYyxJQUFQLENBQVksS0FBWixDQUFyRTtBQUF3Rm1LLGNBQU0sQ0FBQ2xGLEVBQVAsQ0FBVSxDQUFWLEVBQWE5RixRQUFiLENBQXNCLFNBQXRCO0FBQWlDcU4sV0FBRyxDQUFDeUssUUFBSixDQUFhLEdBQWIsRUFBa0JoUyxFQUFsQixDQUFxQixDQUFyQixFQUF3QjlGLFFBQXhCLENBQWlDLGFBQWpDO0FBQWdEcU4sV0FBRyxDQUFDM1EsRUFBSixDQUFPLE9BQVAsRUFBZSxHQUFmLEVBQW1CLFVBQVMrVCxLQUFULEVBQWU7QUFBQ0EsZUFBSyxDQUFDeEwsY0FBTjtBQUF1QnhJLFdBQUMsQ0FBQyxJQUFELENBQUQsQ0FBUXVELFFBQVIsQ0FBaUIsYUFBakIsRUFBZ0NnZCxRQUFoQyxHQUEyQzVmLFdBQTNDLENBQXVELGFBQXZEO0FBQXNFNE4sZ0JBQU0sQ0FBQ2xGLEVBQVAsQ0FBVXJKLENBQUMsQ0FBQyxJQUFELENBQUQsQ0FBUW9KLEtBQVIsRUFBVixFQUEyQjdGLFFBQTNCLENBQW9DLFNBQXBDLEVBQStDNUMsV0FBL0MsQ0FBMkQsTUFBM0QsRUFBbUU0ZixRQUFuRSxHQUE4RTVmLFdBQTlFLENBQTBGLFNBQTFGO0FBQXFHNE4sZ0JBQU0sQ0FBQ2xGLEVBQVAsQ0FBVXJKLENBQUMsQ0FBQyxJQUFELENBQUQsQ0FBUW9KLEtBQVIsRUFBVixFQUEyQjZSLE9BQTNCLEdBQXFDMVgsUUFBckMsQ0FBOEMsTUFBOUM7QUFBc0RnTCxnQkFBTSxDQUFDbEYsRUFBUCxDQUFVckosQ0FBQyxDQUFDLElBQUQsQ0FBRCxDQUFRb0osS0FBUixFQUFWLEVBQTJCK1IsT0FBM0IsR0FBcUN4YSxXQUFyQyxDQUFpRCxNQUFqRDtBQUEwRCxTQUFyVjtBQUF3VixPQUF6akI7QUFBNGpCLEtBQXJyQjtBQUF1ckJPLHFCQUFpQixDQUFDQyxLQUFsQixDQUF3QkMsU0FBeEIsQ0FBa0MsNkRBQWxDLEVBQWdHLFVBQVNDLE1BQVQsRUFBZ0I7QUFBQ2pDLFlBQU0sQ0FBQyx3QkFBRCxDQUFOLENBQWlDa0IsSUFBakMsQ0FBc0MsWUFBVTtBQUFDLFlBQUltTyxLQUFLLEdBQUNyUCxNQUFNLENBQUMsUUFBRCxDQUFoQjtBQUEyQixZQUFJa2EsT0FBTyxHQUFDbGEsTUFBTSxDQUFDLFVBQUQsQ0FBbEI7QUFBK0IsWUFBSW1hLE9BQU8sR0FBQ25hLE1BQU0sQ0FBQyxVQUFELENBQWxCO0FBQStCLFlBQUlvaEIsV0FBVyxHQUFDO0FBQUM5ZSxrQkFBUSxFQUFDO0FBQUNzSyxpQkFBSyxFQUFDLENBQVA7QUFBU3lVLDZCQUFpQixFQUFDLENBQTNCO0FBQTZCaEgsMkJBQWUsRUFBQyxFQUE3QztBQUFnRGxMLGtCQUFNLEVBQUNFLEtBQXZEO0FBQTZEaVMscUJBQVMsRUFBQ2pTLEtBQUssQ0FBQzVDLE1BQTdFO0FBQW9GeU4sbUJBQU8sRUFBQ0EsT0FBNUY7QUFBb0dDLG1CQUFPLEVBQUNBO0FBQTVHLFdBQVY7QUFBZ0loSixjQUFJLEVBQUMsZ0JBQVU7QUFBQ29RLGFBQUMsR0FBQyxLQUFLamYsUUFBUDtBQUFnQixpQkFBS2tmLFVBQUw7QUFBbUIsV0FBbkw7QUFBb0xBLG9CQUFVLEVBQUMsc0JBQVU7QUFBQ0QsYUFBQyxDQUFDckgsT0FBRixDQUFVclosRUFBVixDQUFhO0FBQUMsdUJBQVF1Z0IsV0FBVyxDQUFDekc7QUFBckIsYUFBYjtBQUE4QzRHLGFBQUMsQ0FBQ3BILE9BQUYsQ0FBVXRaLEVBQVYsQ0FBYTtBQUFDLHVCQUFRdWdCLFdBQVcsQ0FBQ3hHO0FBQXJCLGFBQWI7QUFBOENoYSxhQUFDLENBQUNHLFFBQUQsQ0FBRCxDQUFZMGdCLEtBQVosQ0FBa0IsVUFBU3RZLENBQVQsRUFBVztBQUFDLGtCQUFJQSxDQUFDLENBQUM2SixLQUFGLEtBQVUsRUFBWCxJQUFpQjdKLENBQUMsQ0FBQzZKLEtBQUYsS0FBVSxFQUE5QixFQUFrQztBQUFDb08sMkJBQVcsQ0FBQ3pHLFNBQVo7QUFBeUI7O0FBQ2ptSCxrQkFBSXhSLENBQUMsQ0FBQzZKLEtBQUYsS0FBVSxFQUFYLElBQWlCN0osQ0FBQyxDQUFDNkosS0FBRixLQUFVLEVBQTlCLEVBQWtDO0FBQUNvTywyQkFBVyxDQUFDeEcsU0FBWjtBQUF5QjtBQUFDLGFBRDA4RztBQUN2OEcsV0FEaXFHO0FBQ2hxRzhHLHNCQUFZLEVBQUMsc0JBQVN2WSxDQUFULEVBQVc7QUFBQyxnQkFBR0EsQ0FBQyxDQUFDeUUsYUFBRixDQUFnQmYsTUFBaEIsR0FBdUIsQ0FBdkIsSUFBMEIxRCxDQUFDLENBQUN5RSxhQUFGLENBQWdCYixVQUFoQixHQUEyQixDQUF4RCxFQUEwRDtBQUFDd1UsZUFBQyxDQUFDM1UsS0FBRjs7QUFBVSxrQkFBR3VQLElBQUksQ0FBQ0MsR0FBTCxDQUFTbUYsQ0FBQyxDQUFDM1UsS0FBWCxLQUFtQjJVLENBQUMsQ0FBQ2xILGVBQXhCLEVBQXdDO0FBQUMrRywyQkFBVyxDQUFDekcsU0FBWjtBQUF5QjtBQUFDLGFBQXhJLE1BQ3RGO0FBQUM0RyxlQUFDLENBQUMzVSxLQUFGOztBQUFVLGtCQUFHMlUsQ0FBQyxDQUFDM1UsS0FBRixJQUFTMlUsQ0FBQyxDQUFDbEgsZUFBZCxFQUE4QjtBQUFDK0csMkJBQVcsQ0FBQ3hHLFNBQVo7QUFBeUI7QUFBQzs7QUFDeEUsbUJBQU8sS0FBUDtBQUFjLFdBSG10RztBQUdsdEd2RCxtQkFBUyxFQUFDLHFCQUFVO0FBQUNrSyxhQUFDLENBQUMzVSxLQUFGLEdBQVEsQ0FBUjs7QUFBVSxnQkFBR2hNLENBQUMsQ0FBQyxNQUFELENBQUQsQ0FBVTZHLFFBQVYsQ0FBbUIsWUFBbkIsQ0FBSCxFQUFvQztBQUFDO0FBQVE7O0FBQzNGOFosYUFBQyxDQUFDcFMsTUFBRixDQUFTak8sSUFBVCxDQUFjLFVBQVMyRCxDQUFULEVBQVd3SyxLQUFYLEVBQWlCO0FBQUN6TyxlQUFDLENBQUN5TyxLQUFELENBQUQsQ0FBUzdGLFdBQVQsQ0FBcUIsV0FBckIsRUFBa0MzRSxDQUFDLEtBQUcwYyxDQUFDLENBQUNGLGlCQUF4QztBQUE0RHpnQixlQUFDLENBQUN5TyxLQUFELENBQUQsQ0FBUzdGLFdBQVQsQ0FBcUIsU0FBckIsRUFBZ0MzRSxDQUFDLEtBQUcwYyxDQUFDLENBQUNGLGlCQUFGLEdBQW9CLENBQXhEO0FBQTREemdCLGVBQUMsQ0FBQ3lPLEtBQUQsQ0FBRCxDQUFTN0YsV0FBVCxDQUFxQixTQUFyQixFQUFnQzNFLENBQUMsS0FBRzBjLENBQUMsQ0FBQ0YsaUJBQUYsR0FBb0IsQ0FBeEQ7QUFBNER6Z0IsZUFBQyxDQUFDLE1BQUQsQ0FBRCxDQUFVdUQsUUFBVixDQUFtQixZQUFuQjtBQUFpQzBFLHdCQUFVLENBQUMsWUFBVTtBQUFDakksaUJBQUMsQ0FBQyxNQUFELENBQUQsQ0FBVVcsV0FBVixDQUFzQixZQUF0QjtBQUFxQyxlQUFqRCxFQUFrRCxJQUFsRCxDQUFWO0FBQW1FLGFBQXhUO0FBQTJULFdBSnM2RjtBQUlyNkZvWixtQkFBUyxFQUFDLHFCQUFVO0FBQUMsZ0JBQUc0RyxDQUFDLENBQUNGLGlCQUFGLElBQXFCLENBQXhCLEVBQTBCO0FBQUNFLGVBQUMsQ0FBQ0YsaUJBQUYsR0FBb0JFLENBQUMsQ0FBQ0QsU0FBdEI7QUFBaUM7O0FBQzdZQyxhQUFDLENBQUNGLGlCQUFGO0FBQXNCRCx1QkFBVyxDQUFDL0osU0FBWjtBQUF5QixXQUxrckc7QUFLanJHdUQsbUJBQVMsRUFBQyxxQkFBVTtBQUFDMkcsYUFBQyxDQUFDRixpQkFBRjs7QUFBc0IsZ0JBQUdFLENBQUMsQ0FBQ0YsaUJBQUYsSUFBcUJFLENBQUMsQ0FBQ0QsU0FBMUIsRUFBb0M7QUFBQ0MsZUFBQyxDQUFDRixpQkFBRixHQUFvQixDQUFwQjtBQUF1Qjs7QUFDdkpELHVCQUFXLENBQUMvSixTQUFaO0FBQXlCO0FBTndzRyxTQUFoQjtBQU1yckcrSixtQkFBVyxDQUFDalEsSUFBWjtBQUFvQixPQU51aEc7QUFNcGhHLEtBTm02RjtBQU1qNkZyUCxxQkFBaUIsQ0FBQ0MsS0FBbEIsQ0FBd0JDLFNBQXhCLENBQWtDLDREQUFsQyxFQUErRixVQUFTQyxNQUFULEVBQWdCO0FBQUNqQyxZQUFNLENBQUMsTUFBRCxDQUFOLENBQWVtRSxRQUFmLENBQXdCLHNCQUF4QjtBQUFnRG5FLFlBQU0sQ0FBQyxNQUFELENBQU4sQ0FBZW1FLFFBQWYsQ0FBd0Isb0JBQXhCOztBQUE4QyxVQUFJd2QsT0FBTyxHQUFFLFlBQVU7QUFBQyxZQUFJQyxZQUFKO0FBQWlCLFlBQUlDLE9BQU8sR0FBQyxDQUFaOztBQUFjLGlCQUFTRixPQUFULENBQWlCcGYsTUFBakIsRUFBd0I7QUFBQyxlQUFLdWYsSUFBTCxHQUFVbGhCLENBQUMsQ0FBQzJCLE1BQU0sQ0FBQ3VmLElBQVIsQ0FBWDtBQUF5QixlQUFLQyxLQUFMLEdBQVcsS0FBS0QsSUFBTCxDQUFVOWMsSUFBVixDQUFlLElBQWYsQ0FBWDtBQUFnQyxlQUFLZ2QsU0FBTCxHQUFlLEtBQUtELEtBQUwsQ0FBV0UsVUFBWCxFQUFmO0FBQXdDOztBQUFBO0FBQUNOLGVBQU8sQ0FBQ08sU0FBUixHQUFrQjtBQUFDQyxxQkFBVyxFQUFDUixPQUFiO0FBQXFCeFEsY0FBSSxFQUFDLGdCQUFVO0FBQUMsaUJBQUtpUixlQUFMO0FBQXVCLGlCQUFLQyxZQUFMO0FBQW9CLG1CQUFPLElBQVA7QUFBYSxXQUE3RjtBQUE4RkEsc0JBQVksRUFBQyx3QkFBVTtBQUFDLGdCQUFJQyxLQUFLLEdBQUMsSUFBVjs7QUFBZTFoQixhQUFDLENBQUMsWUFBRCxDQUFELENBQWdCQyxFQUFoQixDQUFtQixZQUFuQixFQUFnQyxVQUFTK1QsS0FBVCxFQUFlO0FBQUNoTSwwQkFBWSxDQUFDZ1osWUFBRCxDQUFaO0FBQTJCQSwwQkFBWSxHQUFDL1ksVUFBVSxDQUFDMFosb0JBQW9CLENBQUM1VSxJQUFyQixDQUEwQixJQUExQixFQUErQmlILEtBQS9CLEVBQXFDME4sS0FBSyxDQUFDTixTQUEzQyxDQUFELEVBQXVELENBQXZELENBQXZCO0FBQWtGLGFBQTdKO0FBQWdLLFdBQXJTO0FBQXNTSSx5QkFBZSxFQUFDLDJCQUFVO0FBQUMsaUJBQUtOLElBQUwsQ0FBVTdnQixHQUFWLENBQWMsT0FBZCxFQUFzQixLQUFLdWhCLGVBQUwsRUFBdEI7QUFBOEMsaUJBQUtWLElBQUwsQ0FBVTdnQixHQUFWLENBQWMsVUFBZCxFQUF5QixRQUF6QjtBQUFvQyxXQUFuWjtBQUFvWnVoQix5QkFBZSxFQUFDLDJCQUFVO0FBQUMsZ0JBQUk3Z0IsS0FBSyxHQUFDLENBQVY7QUFBWSxpQkFBS29nQixLQUFMLENBQVc3Z0IsSUFBWCxDQUFnQixVQUFTOEksS0FBVCxFQUFleVksSUFBZixFQUFvQjtBQUFDOWdCLG1CQUFLLElBQUVmLENBQUMsQ0FBQyxJQUFELENBQUQsQ0FBUXFoQixVQUFSLEVBQVA7QUFBNkIsYUFBbEU7QUFBb0UsbUJBQU90Z0IsS0FBUDtBQUFjO0FBQTdnQixTQUFsQjs7QUFBaWlCLGlCQUFTNGdCLG9CQUFULENBQThCM04sS0FBOUIsRUFBb0NqVCxLQUFwQyxFQUEwQztBQUFDLGNBQUdpVCxLQUFLLENBQUM5SCxNQUFOLEdBQWEsQ0FBaEIsRUFBa0I7QUFBQyxpQkFBS21JLFVBQUwsSUFBaUJ0VCxLQUFLLEdBQUMsRUFBdkI7QUFBMkIsV0FBOUMsTUFBa0Q7QUFBQyxpQkFBS3NULFVBQUwsSUFBaUJ0VCxLQUFLLEdBQUMsRUFBdkI7QUFBMkI7O0FBQy9rQ2lULGVBQUssQ0FBQ3hMLGNBQU47QUFBd0I7O0FBQUE7QUFBQyxlQUFPdVksT0FBUDtBQUFnQixPQURzTyxFQUFaOztBQUN0Ti9nQixPQUFDLENBQUNHLFFBQUQsQ0FBRCxDQUFZMmhCLEtBQVosQ0FBa0IsWUFBVTtBQUFDLFlBQUlDLE9BQU8sR0FBQyxJQUFJaEIsT0FBSixDQUFZO0FBQUNHLGNBQUksRUFBQztBQUFOLFNBQVosRUFBZ0UzUSxJQUFoRSxFQUFaO0FBQW1GblIsY0FBTSxDQUFDLHVCQUFELENBQU4sQ0FBZ0NpQixHQUFoQyxDQUFvQyxVQUFwQyxFQUErQyxRQUEvQztBQUF5RGpCLGNBQU0sQ0FBQyxNQUFELENBQU4sQ0FBZWlCLEdBQWYsQ0FBbUIsWUFBbkIsRUFBZ0MsUUFBaEM7QUFBMkMsT0FBcE47QUFBdU4sS0FEL007QUFDaU5hLHFCQUFpQixDQUFDQyxLQUFsQixDQUF3QkMsU0FBeEIsQ0FBa0Msc0VBQWxDLEVBQXlHLFVBQVNDLE1BQVQsRUFBZ0I7QUFBQ2pDLFlBQU0sQ0FBQyxnQ0FBRCxDQUFOLENBQXlDa0IsSUFBekMsQ0FBOEMsWUFBVTtBQUFDLFlBQUkrTSxTQUFTLEdBQUNqTyxNQUFNLENBQUMsSUFBRCxDQUFwQjtBQUEyQixZQUFJeVAsVUFBVSxHQUFDeEIsU0FBUyxDQUFDekssSUFBVixDQUFlLGlCQUFmLENBQWY7O0FBQWlELFlBQUdpTSxVQUFVLElBQUUsQ0FBZixFQUNyZ0I7QUFBQ3pQLGdCQUFNLENBQUMsTUFBRCxDQUFOLENBQWVtRSxRQUFmLENBQXdCLHNCQUF4QjtBQUFnRCxjQUFJdUwsVUFBVSxHQUFDaE8sUUFBUSxDQUFDMUIsTUFBTSxDQUFDLFVBQUQsQ0FBTixDQUFtQmlCLEdBQW5CLENBQXVCLFlBQXZCLENBQUQsQ0FBdkI7QUFBOEQsY0FBSTBPLGNBQWMsR0FBQzNQLE1BQU0sQ0FBQ04sTUFBRCxDQUFOLENBQWVrUSxXQUFmLEVBQW5CO0FBQWdELGNBQUlDLFlBQVksR0FBQ25PLFFBQVEsQ0FBQ2lPLGNBQWMsR0FBQ0QsVUFBaEIsQ0FBekI7QUFBcUR6QixtQkFBUyxDQUFDaE4sR0FBVixDQUFjLFFBQWQsRUFBdUI0TyxZQUFZLEdBQUMsSUFBcEM7QUFBMEM3UCxnQkFBTSxDQUFDTixNQUFELENBQU4sQ0FBZWtDLE1BQWYsQ0FBc0IsWUFBVTtBQUFDLGdCQUFJOE4sVUFBVSxHQUFDaE8sUUFBUSxDQUFDMUIsTUFBTSxDQUFDLFVBQUQsQ0FBTixDQUFtQmlCLEdBQW5CLENBQXVCLFlBQXZCLENBQUQsQ0FBdkI7QUFBOEQsZ0JBQUkwTyxjQUFjLEdBQUMzUCxNQUFNLENBQUNOLE1BQUQsQ0FBTixDQUFla1EsV0FBZixFQUFuQjtBQUFnRCxnQkFBSUMsWUFBWSxHQUFDbk8sUUFBUSxDQUFDaU8sY0FBYyxHQUFDRCxVQUFoQixDQUF6QjtBQUFxRHpCLHFCQUFTLENBQUNoTixHQUFWLENBQWMsUUFBZCxFQUF1QjRPLFlBQVksR0FBQyxJQUFwQztBQUEyQyxXQUEvTztBQUFrUDs7QUFDaGYsWUFBSStTLFdBQVcsR0FBQyxDQUFoQjtBQUFrQixZQUFJQyxLQUFLLEdBQUMsQ0FBVjtBQUFZLFlBQUlDLFFBQVEsR0FBQyxLQUFiO0FBQW1CLFlBQUlDLE1BQU0sR0FBQyxLQUFLLENBQWhCO0FBQWtCLFlBQUlsRyxTQUFTLEdBQUMsS0FBSyxDQUFuQjtBQUFxQixZQUFJbUcsTUFBTSxHQUFDLEtBQVg7QUFBaUIsWUFBSUMsa0JBQWtCLEdBQUMsYUFBdkI7QUFBcUMsWUFBSUMsbUJBQW1CLEdBQUMsYUFBeEI7QUFBc0MsWUFBSUMseUJBQXlCLEdBQUMseUJBQTlCO0FBQXdELFlBQUlDLFFBQVEsR0FBQyxJQUFiOztBQUFrQixZQUFJQyxjQUFjLEdBQUMsU0FBU0EsY0FBVCxHQUF5QjtBQUFDLGVBQUksSUFBSXhlLENBQUMsR0FBQyxDQUFWLEVBQVlBLENBQUMsR0FBQ2dlLEtBQWQsRUFBb0JoZSxDQUFDLEVBQXJCLEVBQXdCO0FBQUNqRSxhQUFDLENBQUMsbUJBQUQsQ0FBRCxDQUF1QndELE1BQXZCLENBQThCLDZCQUEyQlMsQ0FBM0IsR0FBNkIsUUFBM0Q7QUFBc0U7O0FBQzFZLGNBQUlsRSxNQUFNLEdBQUNDLENBQUMsQ0FBQyxtQkFBRCxDQUFELENBQXVCcWIsUUFBdkIsR0FBa0NxSCxJQUFsQyxHQUF5Qy9pQixXQUF6QyxFQUFYO0FBQWtFSyxXQUFDLENBQUMsbUJBQUQsQ0FBRCxDQUF1QkssR0FBdkIsQ0FBMkIsUUFBM0IsRUFBb0MsS0FBRzRoQixLQUFLLEdBQUNsaUIsTUFBN0M7QUFBcURrYyxtQkFBUyxHQUFDamMsQ0FBQyxDQUFDLG1CQUFELENBQUQsQ0FBdUJxYixRQUF2QixFQUFWO0FBQTRDWSxtQkFBUyxDQUFDNVMsRUFBVixDQUFhMlksV0FBYixFQUEwQnplLFFBQTFCLENBQW1DLFFBQW5DO0FBQThDLFNBRDZDOztBQUM1QyxZQUFJb2YsVUFBVSxHQUFDLFNBQVNBLFVBQVQsR0FBcUI7QUFBQzNpQixXQUFDLENBQUMsVUFBRCxDQUFELENBQWNxYixRQUFkLEdBQXlCL2EsSUFBekIsQ0FBOEIsVUFBUzJELENBQVQsRUFBVzZMLEVBQVgsRUFBYztBQUFDOVAsYUFBQyxDQUFDOFAsRUFBRCxDQUFELENBQU1sTixJQUFOLENBQVcsWUFBWCxFQUF3QnFCLENBQXhCO0FBQTJCZ2UsaUJBQUs7QUFBSSxXQUFqRjtBQUFvRixTQUF6SDs7QUFBMEgsWUFBSVcsZ0JBQWdCLEdBQUMsU0FBU0EsZ0JBQVQsR0FBMkI7QUFBQyxjQUFJdkcsR0FBRyxHQUFDcmMsQ0FBQyxDQUFDLFVBQUQsQ0FBRCxDQUFjVSxNQUFkLEVBQVI7QUFBK0IsY0FBSTBlLElBQUksR0FBQ3BmLENBQUMsQ0FBQyxVQUFELENBQUQsQ0FBY1UsTUFBZCxHQUF1QjBlLElBQXZCLEVBQVQ7QUFBdUMvQyxhQUFHLENBQUM3WSxNQUFKLENBQVc0YixJQUFYO0FBQWlCcGYsV0FBQyxDQUFDLFVBQUQsQ0FBRCxDQUFjMGlCLElBQWQsR0FBcUJuZixRQUFyQixDQUE4QixnQkFBOUI7QUFBZ0R2RCxXQUFDLENBQUMsaUJBQUQsQ0FBRCxDQUFxQlcsV0FBckIsQ0FBaUMsZUFBakM7QUFBbUQsU0FBM087O0FBQTRPLFlBQUlraUIsVUFBVSxHQUFDLFNBQVNBLFVBQVQsR0FBcUI7QUFBQzdpQixXQUFDLENBQUMsZUFBRCxDQUFELENBQW1CTSxJQUFuQixDQUF3QixVQUFTMkQsQ0FBVCxFQUFXNkwsRUFBWCxFQUFjO0FBQUMsZ0JBQUl1TSxHQUFHLEdBQUNyYyxDQUFDLENBQUM4UCxFQUFELENBQVQ7QUFBYyxnQkFBSWdULEtBQUssR0FBQzlpQixDQUFDLENBQUM4UCxFQUFELENBQUQsQ0FBTWxOLElBQU4sQ0FBVyxTQUFYLENBQVY7QUFBZ0N5WixlQUFHLENBQUNoQixRQUFKLEdBQWVoYixHQUFmLENBQW1CLGlCQUFuQixFQUFxQyxTQUFPMGlCLHFCQUFxQixDQUFDRCxLQUFELENBQTVCLEdBQW9DLEdBQXpFO0FBQStFLFdBQXBLO0FBQXVLLFNBQTVNOztBQUE2TSxZQUFJQyxxQkFBcUIsR0FBQyxTQUFTQSxxQkFBVCxDQUErQkMsR0FBL0IsRUFBbUM7QUFBQyxjQUFHLENBQUNaLE1BQUosRUFBVztBQUFDQSxrQkFBTSxHQUFDamlCLFFBQVEsQ0FBQzhpQixhQUFULENBQXVCLFFBQXZCLENBQVA7QUFBd0NiLGtCQUFNLENBQUNyaUIsTUFBUCxHQUFjLENBQWQ7QUFBZ0JxaUIsa0JBQU0sQ0FBQ3JoQixLQUFQLEdBQWEsQ0FBYjtBQUFnQjs7QUFDdjVCLGNBQUdxaEIsTUFBTSxDQUFDYyxVQUFWLEVBQXFCO0FBQUMsZ0JBQUlDLEdBQUcsR0FBQ2YsTUFBTSxDQUFDYyxVQUFQLENBQWtCLElBQWxCLENBQVI7QUFBZ0NDLGVBQUcsQ0FBQ0MsU0FBSixHQUFjSixHQUFkO0FBQWtCRyxlQUFHLENBQUNFLFFBQUosQ0FBYSxDQUFiLEVBQWUsQ0FBZixFQUFpQixDQUFqQixFQUFtQixDQUFuQjtBQUFzQixtQkFBT2pCLE1BQU0sQ0FBQ2tCLFNBQVAsRUFBUDtBQUEyQjs7QUFDekgsaUJBQU8sS0FBUDtBQUFjLFNBRnV2Qjs7QUFFdHZCLFlBQUlDLFVBQVUsR0FBQyxTQUFTQSxVQUFULEdBQXFCO0FBQUNaLG9CQUFVO0FBQUdFLG9CQUFVO0FBQUdELDBCQUFnQjtBQUFHSCx3QkFBYztBQUFJLFNBQXBHOztBQUFxRyxZQUFJZSxJQUFJLEdBQUMsU0FBU0EsSUFBVCxHQUFlO0FBQUMsY0FBSUMsR0FBRyxHQUFDaFUsU0FBUyxDQUFDNUQsTUFBVixHQUFpQixDQUFqQixJQUFvQjRELFNBQVMsQ0FBQyxDQUFELENBQVQsS0FBZWtJLFNBQW5DLEdBQTZDbEksU0FBUyxDQUFDLENBQUQsQ0FBdEQsR0FBMEQsQ0FBbEU7QUFBb0UsY0FBR3lTLFFBQUgsRUFBWTtBQUFPLGNBQUcsQ0FBQ3VCLEdBQUosRUFBUTtBQUFPekIscUJBQVcsSUFBRXlCLEdBQWI7QUFBaUJ2QixrQkFBUSxHQUFDLElBQVQ7O0FBQWMsY0FBR0YsV0FBVyxJQUFFQyxLQUFoQixFQUFzQjtBQUFDRCx1QkFBVyxHQUFDLENBQVo7QUFBZTs7QUFDeFQsY0FBR0EsV0FBVyxHQUFDLENBQWYsRUFBaUI7QUFBQ0EsdUJBQVcsR0FBQ0MsS0FBSyxHQUFDLENBQWxCO0FBQXFCOztBQUN2QyxjQUFJeUIsVUFBVSxHQUFDMWpCLENBQUMsQ0FBQyx5QkFBRCxDQUFoQjtBQUE0QyxjQUFJMmpCLFFBQVEsR0FBQzNqQixDQUFDLENBQUMsOEJBQTRCZ2lCLFdBQTVCLEdBQXdDLEdBQXpDLENBQWQ7QUFBNEQyQixrQkFBUSxDQUFDcGdCLFFBQVQsQ0FBa0IsU0FBbEI7O0FBQTZCLGNBQUdrZ0IsR0FBRyxHQUFDLENBQVAsRUFBUztBQUFDdEIsa0JBQU0sQ0FBQzVlLFFBQVAsQ0FBZ0I4ZSxrQkFBaEI7QUFBcUMsV0FBL0MsTUFBbUQ7QUFBQ0Ysa0JBQU0sQ0FBQzVlLFFBQVAsQ0FBZ0IrZSxtQkFBaEI7QUFBc0M7O0FBQy9OckcsbUJBQVMsQ0FBQ3RiLFdBQVYsQ0FBc0IsUUFBdEI7QUFBZ0NzYixtQkFBUyxDQUFDNVMsRUFBVixDQUFhMlksV0FBYixFQUEwQnplLFFBQTFCLENBQW1DLFFBQW5DO0FBQTZDMEUsb0JBQVUsQ0FBQyxZQUFVO0FBQUMyYix3QkFBWSxDQUFDSCxHQUFELENBQVo7QUFBbUIsV0FBL0IsRUFBZ0NqQixRQUFoQyxFQUF5Q2lCLEdBQXpDLENBQVY7QUFBeUQsU0FIbEI7O0FBR21CLFlBQUlHLFlBQVksR0FBQyxTQUFTQSxZQUFULENBQXNCSCxHQUF0QixFQUEwQjtBQUFDempCLFdBQUMsQ0FBQyxZQUFELENBQUQsQ0FBZ0JXLFdBQWhCLENBQTRCLFdBQTVCO0FBQXlDWCxXQUFDLENBQUMsVUFBRCxDQUFELENBQWNXLFdBQWQsQ0FBMEIsU0FBMUIsRUFBcUM0QyxRQUFyQyxDQUE4QyxXQUE5QztBQUEyRDRlLGdCQUFNLENBQUM1ZSxRQUFQLENBQWdCZ2YseUJBQWhCLEVBQTJDNWhCLFdBQTNDLENBQXVEMGhCLGtCQUF2RCxFQUEyRTFoQixXQUEzRSxDQUF1RjJoQixtQkFBdkY7QUFBNEd0aUIsV0FBQyxDQUFDLFlBQUQsQ0FBRCxDQUFnQk0sSUFBaEIsQ0FBcUIsVUFBUzJELENBQVQsRUFBVzZMLEVBQVgsRUFBYztBQUFDLGdCQUFJdU0sR0FBRyxHQUFDcmMsQ0FBQyxDQUFDOFAsRUFBRCxDQUFUO0FBQWN1TSxlQUFHLENBQUN3SCxTQUFKLENBQWN4SCxHQUFHLENBQUMzYixNQUFKLEVBQWQ7QUFBNkIsV0FBL0U7QUFBaUZ1SCxvQkFBVSxDQUFDLFlBQVU7QUFBQ2thLGtCQUFNLENBQUN4aEIsV0FBUCxDQUFtQjRoQix5QkFBbkI7QUFBOENMLG9CQUFRLEdBQUMsS0FBVDtBQUFnQixXQUExRSxFQUEyRSxHQUEzRSxDQUFWO0FBQTJGLFNBQXhhOztBQUF5YSxZQUFJNEIsZUFBZSxHQUFDLFNBQVNBLGVBQVQsR0FBMEI7QUFBQzNqQixrQkFBUSxDQUFDNGpCLE9BQVQsR0FBaUIsVUFBU3hiLENBQVQsRUFBVztBQUFDLG9CQUFPQSxDQUFDLENBQUM0SixPQUFUO0FBQWtCLG1CQUFLLEVBQUw7QUFBUXFSLG9CQUFJLENBQUMsQ0FBQyxDQUFGLENBQUo7QUFBUzs7QUFBTSxtQkFBSyxFQUFMO0FBQVFBLG9CQUFJLENBQUMsQ0FBRCxDQUFKO0FBQVE7QUFBekQ7QUFBaUUsV0FBOUY7O0FBQStGblcsbUJBQVMsQ0FBQ04sSUFBVixDQUFlLGdCQUFmLEVBQWdDLFVBQVN4RSxDQUFULEVBQVc7QUFBQyxnQkFBR0EsQ0FBQyxDQUFDeUUsYUFBRixDQUFnQmYsTUFBaEIsR0FBdUIsQ0FBMUIsRUFBNEI7QUFBQ3VYLGtCQUFJLENBQUMsQ0FBRCxDQUFKO0FBQVMsYUFBdEMsTUFBMEM7QUFBQ0Esa0JBQUksQ0FBQyxDQUFDLENBQUYsQ0FBSjtBQUFVOztBQUMveEIsbUJBQU8sS0FBUDtBQUFjLFdBRGdyQjtBQUM5cUJuVyxtQkFBUyxDQUFDTixJQUFWLENBQWUsWUFBZixFQUE0QixVQUFTeEUsQ0FBVCxFQUFXO0FBQUMsZ0JBQUdBLENBQUMsQ0FBQ3lFLGFBQUYsQ0FBZ0JiLFVBQWhCLEdBQTJCLENBQTlCLEVBQWdDO0FBQUNxWCxrQkFBSSxDQUFDLENBQUQsQ0FBSjtBQUFTLGFBQTFDLE1BQThDO0FBQUNBLGtCQUFJLENBQUMsQ0FBQyxDQUFGLENBQUo7QUFBVTs7QUFDakgsbUJBQU8sS0FBUDtBQUFjLFdBREU7QUFDQXBrQixnQkFBTSxDQUFDLE1BQUQsQ0FBTixDQUFlYSxFQUFmLENBQWtCLFdBQWxCLEVBQThCLFVBQVNzSSxDQUFULEVBQVc7QUFBQ0EsYUFBQyxDQUFDQyxjQUFGO0FBQW1CRCxhQUFDLENBQUNzRSxlQUFGO0FBQW9CLG1CQUFPLEtBQVA7QUFBYyxXQUEvRjtBQUFpRyxjQUFJQyxFQUFKO0FBQU9PLG1CQUFTLENBQUNOLElBQVYsQ0FBZSxZQUFmLEVBQTRCLFVBQVN4RSxDQUFULEVBQVc7QUFBQ3VFLGNBQUUsR0FBQ3ZFLENBQUMsQ0FBQ3lFLGFBQUYsQ0FBZ0JDLE9BQWhCLENBQXdCLENBQXhCLEVBQTJCQyxPQUE5QjtBQUF1QyxXQUEvRTtBQUFpRkcsbUJBQVMsQ0FBQ04sSUFBVixDQUFlLFVBQWYsRUFBMEIsVUFBU3hFLENBQVQsRUFBVztBQUFDLGdCQUFJNEUsRUFBRSxHQUFDNUUsQ0FBQyxDQUFDeUUsYUFBRixDQUFnQkksY0FBaEIsQ0FBK0IsQ0FBL0IsRUFBa0NGLE9BQXpDOztBQUFpRCxnQkFBR0osRUFBRSxHQUFDSyxFQUFFLEdBQUMsQ0FBVCxFQUFXO0FBQUNxVyxrQkFBSSxDQUFDLENBQUQsQ0FBSjtBQUFTLGFBQXJCLE1BQTBCLElBQUcxVyxFQUFFLEdBQUNLLEVBQUUsR0FBQyxDQUFULEVBQVc7QUFBQ3FXLGtCQUFJLENBQUMsQ0FBQyxDQUFGLENBQUo7QUFBVTtBQUFDLFdBQXhJO0FBQTBJdkgsbUJBQVMsQ0FBQ2hjLEVBQVYsQ0FBYSxPQUFiLEVBQXFCLFVBQVNzSSxDQUFULEVBQVc7QUFBQ0EsYUFBQyxDQUFDQyxjQUFGO0FBQW1CLGdCQUFHMFosUUFBSCxFQUFZO0FBQU8sZ0JBQUk3RixHQUFHLEdBQUNyYyxDQUFDLENBQUN1SSxDQUFDLENBQUNxRyxNQUFILENBQVQ7QUFBb0IsZ0JBQUlvVixPQUFPLEdBQUNsakIsUUFBUSxDQUFDdWIsR0FBRyxDQUFDelosSUFBSixDQUFTLFlBQVQsQ0FBRCxFQUF3QixFQUF4QixDQUFwQjtBQUFnRDRnQixnQkFBSSxDQUFDUSxPQUFPLEdBQUNoQyxXQUFULENBQUo7QUFBMkIsV0FBdEs7QUFBeUssU0FGb0Q7O0FBRW5ELFlBQUlpQyxTQUFTLEdBQUMsU0FBU0EsU0FBVCxHQUFvQjtBQUFDOUIsZ0JBQU0sR0FBQ25pQixDQUFDLENBQUMsaUJBQUQsQ0FBUjtBQUE2QixTQUFoRTs7QUFBaUUsWUFBSXVRLElBQUksR0FBQyxTQUFTQSxJQUFULEdBQWU7QUFBQzBULG1CQUFTO0FBQUdWLG9CQUFVO0FBQUdPLHlCQUFlO0FBQUksU0FBckU7O0FBQXNFOWpCLFNBQUMsQ0FBQyxZQUFVO0FBQUN1USxjQUFJO0FBQUksU0FBcEIsQ0FBRDtBQUF3QixPQVY1UjtBQVUrUixLQVZ6WjtBQVUyWnJQLHFCQUFpQixDQUFDQyxLQUFsQixDQUF3QkMsU0FBeEIsQ0FBa0Msb0VBQWxDLEVBQXVHLFVBQVNDLE1BQVQsRUFBZ0I7QUFBQyxVQUFJNmlCLFNBQVMsR0FBQzlrQixNQUFNLENBQUMseUJBQUQsQ0FBcEI7QUFBZ0QsVUFBSStrQixpQkFBaUIsR0FBQ0QsU0FBUyxDQUFDdGhCLElBQVYsQ0FBZSxjQUFmLENBQXRCOztBQUFxRCxVQUFHdWhCLGlCQUFpQixJQUFFLEVBQXRCLEVBQzkzQjtBQUFDQSx5QkFBaUIsR0FBQyxFQUFsQjtBQUFzQjs7QUFDdEJELGVBQVMsQ0FBQ3JZLE1BQVYsR0FBaUIsQ0FBbEIsSUFBc0J1WSxZQUFZLENBQUNGLFNBQUQsQ0FBbEM7O0FBQThDLGVBQVNFLFlBQVQsQ0FBc0JGLFNBQXRCLEVBQWdDO0FBQUNBLGlCQUFTLENBQUM1akIsSUFBVixDQUFlLFlBQVU7QUFBQyxjQUFJK2pCLFFBQVEsR0FBQ2psQixNQUFNLENBQUMsSUFBRCxDQUFuQjtBQUFBLGNBQTBCa2xCLGtCQUFrQixHQUFDLEVBQTdDO0FBQWdEQSw0QkFBa0IsQ0FBQyxpQkFBRCxDQUFsQixHQUFzQ0QsUUFBUSxDQUFDamdCLElBQVQsQ0FBYyxpQkFBZCxDQUF0QztBQUF1RWtnQiw0QkFBa0IsQ0FBQyxlQUFELENBQWxCLEdBQW9DQSxrQkFBa0IsQ0FBQyxpQkFBRCxDQUFsQixDQUFzQ2pKLFFBQXRDLENBQStDLFNBQS9DLENBQXBDO0FBQThGaUosNEJBQWtCLENBQUMsYUFBRCxDQUFsQixHQUFrQ0Esa0JBQWtCLENBQUMsZUFBRCxDQUFsQixDQUFvQ2pKLFFBQXBDLENBQTZDLGVBQTdDLENBQWxDO0FBQWdHaUosNEJBQWtCLENBQUMsZ0JBQUQsQ0FBbEIsR0FBcUNBLGtCQUFrQixDQUFDLGVBQUQsQ0FBbEIsQ0FBb0NsZ0IsSUFBcEMsQ0FBeUMsR0FBekMsQ0FBckM7QUFBbUZrZ0IsNEJBQWtCLENBQUMsZUFBRCxDQUFsQixHQUFvQ0MsU0FBUyxDQUFDRCxrQkFBa0IsQ0FBQyxnQkFBRCxDQUFuQixDQUE3QztBQUFvRkEsNEJBQWtCLENBQUMsZ0JBQUQsQ0FBbEIsR0FBcUNFLFFBQVEsQ0FBQ0Ysa0JBQWtCLENBQUMsZUFBRCxDQUFuQixDQUE3QztBQUFtRkEsNEJBQWtCLENBQUMsb0JBQUQsQ0FBbEIsR0FBeUNELFFBQVEsQ0FBQ2pnQixJQUFULENBQWMseUJBQWQsQ0FBekM7QUFBa0ZrZ0IsNEJBQWtCLENBQUMsZUFBRCxDQUFsQixHQUFvQ0QsUUFBUSxDQUFDaEosUUFBVCxDQUFrQixpQkFBbEIsQ0FBcEM7QUFBeUVvSix5QkFBZSxDQUFDSCxrQkFBRCxFQUFvQkgsaUJBQXBCLENBQWY7QUFBc0QsY0FBSU8sZ0JBQWdCLEdBQUNDLGdCQUFnQixDQUFDTCxrQkFBRCxFQUFvQkgsaUJBQXBCLENBQXJDO0FBQTRFRSxrQkFBUSxDQUFDOWdCLFFBQVQsQ0FBa0IsUUFBbEI7QUFBNEIrZ0IsNEJBQWtCLENBQUMsb0JBQUQsQ0FBbEIsQ0FBeUNya0IsRUFBekMsQ0FBNEMsT0FBNUMsRUFBb0QsT0FBcEQsRUFBNEQsVUFBUytULEtBQVQsRUFBZTtBQUFDQSxpQkFBSyxDQUFDeEwsY0FBTjtBQUF1Qm9jLHVCQUFXLENBQUNOLGtCQUFELEVBQW9CSSxnQkFBcEIsRUFBcUMsTUFBckMsQ0FBWDtBQUF5RCxXQUE1SjtBQUE4SkosNEJBQWtCLENBQUMsb0JBQUQsQ0FBbEIsQ0FBeUNya0IsRUFBekMsQ0FBNEMsT0FBNUMsRUFBb0QsT0FBcEQsRUFBNEQsVUFBUytULEtBQVQsRUFBZTtBQUFDQSxpQkFBSyxDQUFDeEwsY0FBTjtBQUF1Qm9jLHVCQUFXLENBQUNOLGtCQUFELEVBQW9CSSxnQkFBcEIsRUFBcUMsTUFBckMsQ0FBWDtBQUF5RCxXQUE1SjtBQUE4SkosNEJBQWtCLENBQUMsZUFBRCxDQUFsQixDQUFvQ3JrQixFQUFwQyxDQUF1QyxPQUF2QyxFQUErQyxHQUEvQyxFQUFtRCxVQUFTK1QsS0FBVCxFQUFlO0FBQUNBLGlCQUFLLENBQUN4TCxjQUFOO0FBQXVCOGIsOEJBQWtCLENBQUMsZ0JBQUQsQ0FBbEIsQ0FBcUMzakIsV0FBckMsQ0FBaUQsVUFBakQ7QUFBNkR2QixrQkFBTSxDQUFDLElBQUQsQ0FBTixDQUFhbUUsUUFBYixDQUFzQixVQUF0QjtBQUFrQ3NoQiw2QkFBaUIsQ0FBQ3psQixNQUFNLENBQUMsSUFBRCxDQUFQLENBQWpCO0FBQWdDMGxCLHlCQUFhLENBQUMxbEIsTUFBTSxDQUFDLElBQUQsQ0FBUCxFQUFja2xCLGtCQUFrQixDQUFDLGFBQUQsQ0FBaEMsRUFBZ0RJLGdCQUFoRCxDQUFiO0FBQStFSyxnQ0FBb0IsQ0FBQzNsQixNQUFNLENBQUMsSUFBRCxDQUFQLEVBQWNrbEIsa0JBQWtCLENBQUMsZUFBRCxDQUFoQyxDQUFwQjtBQUF3RSxXQUFoWDtBQUFrWEEsNEJBQWtCLENBQUMsZUFBRCxDQUFsQixDQUFvQ3JrQixFQUFwQyxDQUF1QyxXQUF2QyxFQUFtRCxZQUFVO0FBQUMsZ0JBQUkra0IsRUFBRSxHQUFDQyxPQUFPLEVBQWQ7QUFBa0JELGNBQUUsSUFBRSxRQUFMLElBQWdCRSxjQUFjLENBQUNaLGtCQUFELEVBQW9CSSxnQkFBcEIsRUFBcUMsTUFBckMsQ0FBOUI7QUFBNEUsV0FBM0o7QUFBNkpKLDRCQUFrQixDQUFDLGVBQUQsQ0FBbEIsQ0FBb0Nya0IsRUFBcEMsQ0FBdUMsWUFBdkMsRUFBb0QsWUFBVTtBQUFDLGdCQUFJK2tCLEVBQUUsR0FBQ0MsT0FBTyxFQUFkO0FBQWtCRCxjQUFFLElBQUUsUUFBTCxJQUFnQkUsY0FBYyxDQUFDWixrQkFBRCxFQUFvQkksZ0JBQXBCLEVBQXFDLE1BQXJDLENBQTlCO0FBQTRFLFdBQTVKO0FBQThKdGxCLGdCQUFNLENBQUNlLFFBQUQsQ0FBTixDQUFpQjBnQixLQUFqQixDQUF1QixVQUFTN00sS0FBVCxFQUFlO0FBQUMsZ0JBQUdBLEtBQUssQ0FBQzVCLEtBQU4sSUFBYSxJQUFiLElBQW1CK1MsaUJBQWlCLENBQUNkLFFBQVEsQ0FBQ2UsR0FBVCxDQUFhLENBQWIsQ0FBRCxDQUF2QyxFQUF5RDtBQUFDRiw0QkFBYyxDQUFDWixrQkFBRCxFQUFvQkksZ0JBQXBCLEVBQXFDLE1BQXJDLENBQWQ7QUFBNEQsYUFBdEgsTUFBMkgsSUFBRzFRLEtBQUssQ0FBQzVCLEtBQU4sSUFBYSxJQUFiLElBQW1CK1MsaUJBQWlCLENBQUNkLFFBQVEsQ0FBQ2UsR0FBVCxDQUFhLENBQWIsQ0FBRCxDQUF2QyxFQUF5RDtBQUFDRiw0QkFBYyxDQUFDWixrQkFBRCxFQUFvQkksZ0JBQXBCLEVBQXFDLE1BQXJDLENBQWQ7QUFBNEQ7QUFBQyxXQUF6UjtBQUE0UixTQUF2b0U7QUFBMG9FOztBQUN6dEUsZUFBU0UsV0FBVCxDQUFxQk4sa0JBQXJCLEVBQXdDSSxnQkFBeEMsRUFBeURXLE1BQXpELEVBQWdFO0FBQUMsWUFBSUMsY0FBYyxHQUFDQyxpQkFBaUIsQ0FBQ2pCLGtCQUFrQixDQUFDLGVBQUQsQ0FBbkIsQ0FBcEM7QUFBQSxZQUEwRWtCLFlBQVksR0FBQzFKLE1BQU0sQ0FBQ3dJLGtCQUFrQixDQUFDLGlCQUFELENBQWxCLENBQXNDamtCLEdBQXRDLENBQTBDLE9BQTFDLEVBQW1ENkMsT0FBbkQsQ0FBMkQsSUFBM0QsRUFBZ0UsRUFBaEUsQ0FBRCxDQUE3RjtBQUFvS21pQixjQUFNLElBQUUsTUFBVCxHQUFpQkksaUJBQWlCLENBQUNuQixrQkFBRCxFQUFvQmdCLGNBQWMsR0FBQ0UsWUFBZixHQUE0QnJCLGlCQUFoRCxFQUFrRXFCLFlBQVksR0FBQ2QsZ0JBQS9FLENBQWxDLEdBQW1JZSxpQkFBaUIsQ0FBQ25CLGtCQUFELEVBQW9CZ0IsY0FBYyxHQUFDRSxZQUFmLEdBQTRCckIsaUJBQWhELENBQXBKO0FBQXdOOztBQUM1YixlQUFTZSxjQUFULENBQXdCWixrQkFBeEIsRUFBMkNJLGdCQUEzQyxFQUE0RFcsTUFBNUQsRUFBbUU7QUFBQyxZQUFJSyxjQUFjLEdBQUNwQixrQkFBa0IsQ0FBQyxlQUFELENBQWxCLENBQW9DbGdCLElBQXBDLENBQXlDLFdBQXpDLENBQW5CO0FBQUEsWUFBeUV1aEIsVUFBVSxHQUFFTixNQUFNLElBQUUsTUFBVCxHQUFpQkssY0FBYyxDQUFDak4sSUFBZixFQUFqQixHQUF1Q2lOLGNBQWMsQ0FBQ2hOLElBQWYsRUFBM0g7O0FBQWlKLFlBQUdpTixVQUFVLENBQUM5WixNQUFYLEdBQWtCLENBQXJCLEVBQXVCO0FBQUMsY0FBSStaLFlBQVksR0FBQ3RCLGtCQUFrQixDQUFDLGVBQUQsQ0FBbEIsQ0FBb0NsZ0IsSUFBcEMsQ0FBeUMsV0FBekMsQ0FBakI7QUFBQSxjQUF1RXloQixRQUFRLEdBQUVSLE1BQU0sSUFBRSxNQUFULEdBQWlCTyxZQUFZLENBQUNsbEIsTUFBYixDQUFvQixJQUFwQixFQUEwQitYLElBQTFCLENBQStCLElBQS9CLEVBQXFDNEMsUUFBckMsQ0FBOEMsR0FBOUMsQ0FBakIsR0FBb0V1SyxZQUFZLENBQUNsbEIsTUFBYixDQUFvQixJQUFwQixFQUEwQmdZLElBQTFCLENBQStCLElBQS9CLEVBQXFDMkMsUUFBckMsQ0FBOEMsR0FBOUMsQ0FBcEo7QUFBdU15Six1QkFBYSxDQUFDZSxRQUFELEVBQVV2QixrQkFBa0IsQ0FBQyxhQUFELENBQTVCLEVBQTRDSSxnQkFBNUMsQ0FBYjtBQUEyRUssOEJBQW9CLENBQUNjLFFBQUQsRUFBVXZCLGtCQUFrQixDQUFDLGVBQUQsQ0FBNUIsQ0FBcEI7QUFBbUV1QixrQkFBUSxDQUFDdGlCLFFBQVQsQ0FBa0IsVUFBbEI7QUFBOEJxaUIsc0JBQVksQ0FBQ2psQixXQUFiLENBQXlCLFVBQXpCO0FBQXFDa2tCLDJCQUFpQixDQUFDZ0IsUUFBRCxDQUFqQjtBQUE0QkMsZ0NBQXNCLENBQUNULE1BQUQsRUFBUVEsUUFBUixFQUFpQnZCLGtCQUFqQixFQUFvQ0ksZ0JBQXBDLENBQXRCO0FBQTZFO0FBQUM7O0FBQy91QixlQUFTb0Isc0JBQVQsQ0FBZ0NULE1BQWhDLEVBQXVDclIsS0FBdkMsRUFBNkNzUSxrQkFBN0MsRUFBZ0VJLGdCQUFoRSxFQUFpRjtBQUFDLFlBQUlxQixVQUFVLEdBQUNqbkIsTUFBTSxDQUFDa25CLGdCQUFQLENBQXdCaFMsS0FBSyxDQUFDb1IsR0FBTixDQUFVLENBQVYsQ0FBeEIsRUFBcUMsSUFBckMsQ0FBZjtBQUFBLFlBQTBEYSxTQUFTLEdBQUNuSyxNQUFNLENBQUNpSyxVQUFVLENBQUNHLGdCQUFYLENBQTRCLE1BQTVCLEVBQW9DaGpCLE9BQXBDLENBQTRDLElBQTVDLEVBQWlELEVBQWpELENBQUQsQ0FBMUU7QUFBQSxZQUFpSWlqQixhQUFhLEdBQUNySyxNQUFNLENBQUN3SSxrQkFBa0IsQ0FBQyxpQkFBRCxDQUFsQixDQUFzQ2prQixHQUF0QyxDQUEwQyxPQUExQyxFQUFtRDZDLE9BQW5ELENBQTJELElBQTNELEVBQWdFLEVBQWhFLENBQUQsQ0FBcko7QUFBQSxZQUEyTndoQixnQkFBZ0IsR0FBQzVJLE1BQU0sQ0FBQ3dJLGtCQUFrQixDQUFDLGVBQUQsQ0FBbEIsQ0FBb0Nqa0IsR0FBcEMsQ0FBd0MsT0FBeEMsRUFBaUQ2QyxPQUFqRCxDQUF5RCxJQUF6RCxFQUE4RCxFQUE5RCxDQUFELENBQWxQO0FBQXNULFlBQUlrakIsaUJBQWlCLEdBQUNiLGlCQUFpQixDQUFDakIsa0JBQWtCLENBQUMsZUFBRCxDQUFuQixDQUF2Qzs7QUFBNkUsWUFBSWUsTUFBTSxJQUFFLE1BQVIsSUFBZ0JZLFNBQVMsR0FBQ0UsYUFBYSxHQUFDQyxpQkFBekMsSUFBOERmLE1BQU0sSUFBRSxNQUFSLElBQWdCWSxTQUFTLEdBQUMsQ0FBQ0csaUJBQTVGLEVBQStHO0FBQUNYLDJCQUFpQixDQUFDbkIsa0JBQUQsRUFBb0IsQ0FBQzJCLFNBQUQsR0FBV0UsYUFBYSxHQUFDLENBQTdDLEVBQStDQSxhQUFhLEdBQUN6QixnQkFBN0QsQ0FBakI7QUFBaUc7QUFBQzs7QUFDdnFCLGVBQVNlLGlCQUFULENBQTJCbkIsa0JBQTNCLEVBQThDeE0sS0FBOUMsRUFBb0R1TyxRQUFwRCxFQUE2RDtBQUFDLFlBQUlDLGFBQWEsR0FBQ2hDLGtCQUFrQixDQUFDLGVBQUQsQ0FBbEIsQ0FBb0NjLEdBQXBDLENBQXdDLENBQXhDLENBQWxCO0FBQTZEdE4sYUFBSyxHQUFFQSxLQUFLLEdBQUMsQ0FBUCxHQUFVLENBQVYsR0FBWUEsS0FBbEI7QUFBd0JBLGFBQUssR0FBRSxFQUFFLE9BQU91TyxRQUFQLEtBQWtCLFdBQXBCLEtBQWtDdk8sS0FBSyxHQUFDdU8sUUFBekMsR0FBbURBLFFBQW5ELEdBQTREdk8sS0FBbEU7QUFBd0V5Tyx5QkFBaUIsQ0FBQ0QsYUFBRCxFQUFlLFlBQWYsRUFBNEJ4TyxLQUFLLEdBQUMsSUFBbEMsQ0FBakI7QUFBMERBLGFBQUssSUFBRSxDQUFSLEdBQVd3TSxrQkFBa0IsQ0FBQyxvQkFBRCxDQUFsQixDQUF5Q2xnQixJQUF6QyxDQUE4QyxPQUE5QyxFQUF1RGIsUUFBdkQsQ0FBZ0UsVUFBaEUsQ0FBWCxHQUF1RitnQixrQkFBa0IsQ0FBQyxvQkFBRCxDQUFsQixDQUF5Q2xnQixJQUF6QyxDQUE4QyxPQUE5QyxFQUF1RHpELFdBQXZELENBQW1FLFVBQW5FLENBQXZGO0FBQXVLbVgsYUFBSyxJQUFFdU8sUUFBUixHQUFrQi9CLGtCQUFrQixDQUFDLG9CQUFELENBQWxCLENBQXlDbGdCLElBQXpDLENBQThDLE9BQTlDLEVBQXVEYixRQUF2RCxDQUFnRSxVQUFoRSxDQUFsQixHQUE4RitnQixrQkFBa0IsQ0FBQyxvQkFBRCxDQUFsQixDQUF5Q2xnQixJQUF6QyxDQUE4QyxPQUE5QyxFQUF1RHpELFdBQXZELENBQW1FLFVBQW5FLENBQTlGO0FBQThLOztBQUN4bUIsZUFBU21rQixhQUFULENBQXVCMEIsYUFBdkIsRUFBcUNDLE9BQXJDLEVBQTZDSixRQUE3QyxFQUFzRDtBQUFDLFlBQUlOLFVBQVUsR0FBQ2puQixNQUFNLENBQUNrbkIsZ0JBQVAsQ0FBd0JRLGFBQWEsQ0FBQ3BCLEdBQWQsQ0FBa0IsQ0FBbEIsQ0FBeEIsRUFBNkMsSUFBN0MsQ0FBZjtBQUFBLFlBQWtFYSxTQUFTLEdBQUNGLFVBQVUsQ0FBQ0csZ0JBQVgsQ0FBNEIsTUFBNUIsQ0FBNUU7QUFBQSxZQUFnSFEsVUFBVSxHQUFDWCxVQUFVLENBQUNHLGdCQUFYLENBQTRCLE9BQTVCLENBQTNIO0FBQWdLRCxpQkFBUyxHQUFDbkssTUFBTSxDQUFDbUssU0FBUyxDQUFDL2lCLE9BQVYsQ0FBa0IsSUFBbEIsRUFBdUIsRUFBdkIsQ0FBRCxDQUFOLEdBQW1DNFksTUFBTSxDQUFDNEssVUFBVSxDQUFDeGpCLE9BQVgsQ0FBbUIsSUFBbkIsRUFBd0IsRUFBeEIsQ0FBRCxDQUFOLEdBQW9DLENBQWpGO0FBQW1GLFlBQUl5akIsVUFBVSxHQUFDVixTQUFTLEdBQUNJLFFBQXpCO0FBQWtDRSx5QkFBaUIsQ0FBQ0UsT0FBTyxDQUFDckIsR0FBUixDQUFZLENBQVosQ0FBRCxFQUFnQixRQUFoQixFQUF5QnVCLFVBQXpCLENBQWpCO0FBQXVEOztBQUNuWSxlQUFTbEMsZUFBVCxDQUF5Qkgsa0JBQXpCLEVBQTRDc0MsR0FBNUMsRUFBZ0Q7QUFBQyxhQUFJM2lCLENBQUMsR0FBQyxDQUFOLEVBQVFBLENBQUMsR0FBQ3FnQixrQkFBa0IsQ0FBQyxlQUFELENBQWxCLENBQW9DelksTUFBOUMsRUFBcUQ1SCxDQUFDLEVBQXRELEVBQXlEO0FBQUMsY0FBSTRpQixRQUFRLEdBQUNDLE9BQU8sQ0FBQ3hDLGtCQUFrQixDQUFDLGVBQUQsQ0FBbEIsQ0FBb0MsQ0FBcEMsQ0FBRCxFQUF3Q0Esa0JBQWtCLENBQUMsZUFBRCxDQUFsQixDQUFvQ3JnQixDQUFwQyxDQUF4QyxDQUFwQjtBQUFBLGNBQW9HOGlCLFlBQVksR0FBQ3hMLElBQUksQ0FBQ3lMLEtBQUwsQ0FBV0gsUUFBUSxHQUFDdkMsa0JBQWtCLENBQUMsZ0JBQUQsQ0FBdEMsSUFBMEQsQ0FBM0s7QUFBNktBLDRCQUFrQixDQUFDLGdCQUFELENBQWxCLENBQXFDamIsRUFBckMsQ0FBd0NwRixDQUF4QyxFQUEyQzVELEdBQTNDLENBQStDLE1BQS9DLEVBQXNEMG1CLFlBQVksR0FBQ0gsR0FBYixHQUFpQixJQUF2RTtBQUE4RTtBQUFDOztBQUN2VyxlQUFTakMsZ0JBQVQsQ0FBMEJMLGtCQUExQixFQUE2Q3ZqQixLQUE3QyxFQUFtRDtBQUFDLFlBQUlrbUIsUUFBUSxHQUFDSCxPQUFPLENBQUN4QyxrQkFBa0IsQ0FBQyxlQUFELENBQWxCLENBQW9DLENBQXBDLENBQUQsRUFBd0NBLGtCQUFrQixDQUFDLGVBQUQsQ0FBbEIsQ0FBb0NBLGtCQUFrQixDQUFDLGVBQUQsQ0FBbEIsQ0FBb0N6WSxNQUFwQyxHQUEyQyxDQUEvRSxDQUF4QyxDQUFwQjtBQUFBLFlBQStJcWIsWUFBWSxHQUFDRCxRQUFRLEdBQUMzQyxrQkFBa0IsQ0FBQyxnQkFBRCxDQUF2TDtBQUFBLFlBQTBNNEMsWUFBWSxHQUFDM0wsSUFBSSxDQUFDeUwsS0FBTCxDQUFXRSxZQUFYLElBQXlCLENBQWhQO0FBQUEsWUFBa1BDLFVBQVUsR0FBQ0QsWUFBWSxHQUFDbm1CLEtBQTFRO0FBQWdSdWpCLDBCQUFrQixDQUFDLGVBQUQsQ0FBbEIsQ0FBb0Nqa0IsR0FBcEMsQ0FBd0MsT0FBeEMsRUFBZ0Q4bUIsVUFBVSxHQUFDLElBQTNEO0FBQWlFckMscUJBQWEsQ0FBQ1Isa0JBQWtCLENBQUMsZ0JBQUQsQ0FBbEIsQ0FBcUNqYixFQUFyQyxDQUF3QyxDQUF4QyxDQUFELEVBQTRDaWIsa0JBQWtCLENBQUMsYUFBRCxDQUE5RCxFQUE4RTZDLFVBQTlFLENBQWI7QUFBdUcsZUFBT0EsVUFBUDtBQUFtQjs7QUFDL2YsZUFBU3BDLG9CQUFULENBQThCL1EsS0FBOUIsRUFBb0NvVCxhQUFwQyxFQUFrRDtBQUFDLFlBQUlDLFNBQVMsR0FBQ3JULEtBQUssQ0FBQ3ZTLElBQU4sQ0FBVyxNQUFYLENBQWQ7QUFBQSxZQUFpQ2lrQixjQUFjLEdBQUMwQixhQUFhLENBQUNoakIsSUFBZCxDQUFtQixXQUFuQixDQUFoRDtBQUFBLFlBQWdGa2pCLGVBQWUsR0FBQ0YsYUFBYSxDQUFDaGpCLElBQWQsQ0FBbUIsaUJBQWVpakIsU0FBZixHQUF5QixJQUE1QyxDQUFoRztBQUFBLFlBQWtKRSxxQkFBcUIsR0FBQ0QsZUFBZSxDQUFDdm5CLE1BQWhCLEVBQXhLOztBQUFpTSxZQUFHdW5CLGVBQWUsQ0FBQ2xlLEtBQWhCLEtBQXdCc2MsY0FBYyxDQUFDdGMsS0FBZixFQUEzQixFQUFrRDtBQUFDLGNBQUlvZSxjQUFjLEdBQUMsc0JBQW5CO0FBQUEsY0FBMENDLFlBQVksR0FBQyxZQUF2RDtBQUFxRSxTQUF4SCxNQUE0SDtBQUFDLGNBQUlELGNBQWMsR0FBQyxxQkFBbkI7QUFBQSxjQUF5Q0MsWUFBWSxHQUFDLGFBQXREO0FBQXFFOztBQUN0YkgsdUJBQWUsQ0FBQzFrQixJQUFoQixDQUFxQixPQUFyQixFQUE2QjRrQixjQUE3QjtBQUE2QzlCLHNCQUFjLENBQUM5aUIsSUFBZixDQUFvQixPQUFwQixFQUE0QjZrQixZQUE1QixFQUEwQ0MsR0FBMUMsQ0FBOEMsOERBQTlDLEVBQTZHLFlBQVU7QUFBQ2hDLHdCQUFjLENBQUMva0IsV0FBZixDQUEyQix3QkFBM0I7QUFBcUQybUIseUJBQWUsQ0FBQzNtQixXQUFoQixDQUE0Qix3QkFBNUI7QUFBdUQsU0FBcE87QUFBc095bUIscUJBQWEsQ0FBQy9tQixHQUFkLENBQWtCLFFBQWxCLEVBQTJCa25CLHFCQUFxQixHQUFDLElBQWpEO0FBQXdEOztBQUMzVSxlQUFTMUMsaUJBQVQsQ0FBMkI3USxLQUEzQixFQUFpQztBQUFDQSxhQUFLLENBQUN0VCxNQUFOLENBQWEsSUFBYixFQUFtQnVhLE9BQW5CLENBQTJCLElBQTNCLEVBQWlDSSxRQUFqQyxDQUEwQyxHQUExQyxFQUErQzlYLFFBQS9DLENBQXdELGFBQXhELEVBQXVFbVksR0FBdkUsR0FBNkVBLEdBQTdFLEdBQW1GUCxPQUFuRixDQUEyRixJQUEzRixFQUFpR0UsUUFBakcsQ0FBMEcsR0FBMUcsRUFBK0cxYSxXQUEvRyxDQUEySCxhQUEzSDtBQUEySTs7QUFDN0ssZUFBUzRrQixpQkFBVCxDQUEyQmxCLFFBQTNCLEVBQW9DO0FBQUMsWUFBSXNELGFBQWEsR0FBQzdvQixNQUFNLENBQUNrbkIsZ0JBQVAsQ0FBd0IzQixRQUFRLENBQUNlLEdBQVQsQ0FBYSxDQUFiLENBQXhCLEVBQXdDLElBQXhDLENBQWxCO0FBQUEsWUFBZ0VnQixpQkFBaUIsR0FBQ3VCLGFBQWEsQ0FBQ3pCLGdCQUFkLENBQStCLG1CQUEvQixLQUFxRHlCLGFBQWEsQ0FBQ3pCLGdCQUFkLENBQStCLGdCQUEvQixDQUFyRCxJQUF1R3lCLGFBQWEsQ0FBQ3pCLGdCQUFkLENBQStCLGVBQS9CLENBQXZHLElBQXdKeUIsYUFBYSxDQUFDekIsZ0JBQWQsQ0FBK0IsY0FBL0IsQ0FBeEosSUFBd015QixhQUFhLENBQUN6QixnQkFBZCxDQUErQixXQUEvQixDQUExUjs7QUFBc1UsWUFBR0UsaUJBQWlCLENBQUNqbkIsT0FBbEIsQ0FBMEIsR0FBMUIsS0FBZ0MsQ0FBbkMsRUFBcUM7QUFBQyxjQUFJaW5CLGlCQUFpQixHQUFDQSxpQkFBaUIsQ0FBQzNHLEtBQWxCLENBQXdCLEdBQXhCLEVBQTZCLENBQTdCLENBQXRCO0FBQXNEMkcsMkJBQWlCLEdBQUNBLGlCQUFpQixDQUFDM0csS0FBbEIsQ0FBd0IsR0FBeEIsRUFBNkIsQ0FBN0IsQ0FBbEI7QUFBa0QyRywyQkFBaUIsR0FBQ0EsaUJBQWlCLENBQUMzRyxLQUFsQixDQUF3QixHQUF4QixDQUFsQjtBQUErQyxjQUFJNkYsY0FBYyxHQUFDYyxpQkFBaUIsQ0FBQyxDQUFELENBQXBDO0FBQXlDLFNBQXRPLE1BQTBPO0FBQUMsY0FBSWQsY0FBYyxHQUFDLENBQW5CO0FBQXNCOztBQUM1bUIsZUFBT3hKLE1BQU0sQ0FBQ3dKLGNBQUQsQ0FBYjtBQUErQjs7QUFDL0IsZUFBU2lCLGlCQUFULENBQTJCcUIsT0FBM0IsRUFBbUNDLFFBQW5DLEVBQTRDL1AsS0FBNUMsRUFBa0Q7QUFBQzhQLGVBQU8sQ0FBQ3paLEtBQVIsQ0FBYyxtQkFBZCxJQUFtQzBaLFFBQVEsR0FBQyxHQUFULEdBQWEvUCxLQUFiLEdBQW1CLEdBQXREO0FBQTBEOFAsZUFBTyxDQUFDelosS0FBUixDQUFjLGdCQUFkLElBQWdDMFosUUFBUSxHQUFDLEdBQVQsR0FBYS9QLEtBQWIsR0FBbUIsR0FBbkQ7QUFBdUQ4UCxlQUFPLENBQUN6WixLQUFSLENBQWMsZUFBZCxJQUErQjBaLFFBQVEsR0FBQyxHQUFULEdBQWEvUCxLQUFiLEdBQW1CLEdBQWxEO0FBQXNEOFAsZUFBTyxDQUFDelosS0FBUixDQUFjLGNBQWQsSUFBOEIwWixRQUFRLEdBQUMsR0FBVCxHQUFhL1AsS0FBYixHQUFtQixHQUFqRDtBQUFxRDhQLGVBQU8sQ0FBQ3paLEtBQVIsQ0FBYyxXQUFkLElBQTJCMFosUUFBUSxHQUFDLEdBQVQsR0FBYS9QLEtBQWIsR0FBbUIsR0FBOUM7QUFBbUQ7O0FBQ2xVLGVBQVN5TSxTQUFULENBQW1CdUQsTUFBbkIsRUFBMEI7QUFBQyxZQUFJQyxVQUFVLEdBQUMsRUFBZjtBQUFrQkQsY0FBTSxDQUFDeG5CLElBQVAsQ0FBWSxZQUFVO0FBQUMsY0FBSTBuQixRQUFRLEdBQUM1b0IsTUFBTSxDQUFDLElBQUQsQ0FBTixDQUFhcUMsSUFBYixDQUFrQixNQUFsQixFQUEwQmdlLEtBQTFCLENBQWdDLEdBQWhDLENBQWI7QUFBQSxjQUFrRHdJLE9BQU8sR0FBQyxJQUFJQyxJQUFKLENBQVNGLFFBQVEsQ0FBQyxDQUFELENBQWpCLEVBQXFCQSxRQUFRLENBQUMsQ0FBRCxDQUFSLEdBQVksQ0FBakMsRUFBbUNBLFFBQVEsQ0FBQyxDQUFELENBQTNDLENBQTFEO0FBQTBHRCxvQkFBVSxDQUFDeGxCLElBQVgsQ0FBZ0IwbEIsT0FBaEI7QUFBMEIsU0FBM0o7QUFBNkosZUFBT0YsVUFBUDtBQUFtQjs7QUFDN04sZUFBU0ksVUFBVCxDQUFvQkwsTUFBcEIsRUFBMkI7QUFBQyxZQUFJQyxVQUFVLEdBQUMsRUFBZjtBQUFrQkQsY0FBTSxDQUFDeG5CLElBQVAsQ0FBWSxZQUFVO0FBQUMsY0FBSThuQixVQUFVLEdBQUNocEIsTUFBTSxDQUFDLElBQUQsQ0FBckI7QUFBQSxjQUE0QjRvQixRQUFRLEdBQUNJLFVBQVUsQ0FBQzNtQixJQUFYLENBQWdCLE1BQWhCLEVBQXdCZ2UsS0FBeEIsQ0FBOEIsR0FBOUIsQ0FBckM7O0FBQXdFLGNBQUd1SSxRQUFRLENBQUNuYyxNQUFULEdBQWdCLENBQW5CLEVBQXFCO0FBQUMsZ0JBQUl3YyxPQUFPLEdBQUNMLFFBQVEsQ0FBQyxDQUFELENBQVIsQ0FBWXZJLEtBQVosQ0FBa0IsR0FBbEIsQ0FBWjtBQUFBLGdCQUFtQzZJLFFBQVEsR0FBQ04sUUFBUSxDQUFDLENBQUQsQ0FBUixDQUFZdkksS0FBWixDQUFrQixHQUFsQixDQUE1QztBQUFvRSxXQUExRixNQUErRixJQUFHdUksUUFBUSxDQUFDLENBQUQsQ0FBUixDQUFZN29CLE9BQVosQ0FBb0IsR0FBcEIsS0FBMEIsQ0FBN0IsRUFBK0I7QUFBQyxnQkFBSWtwQixPQUFPLEdBQUMsQ0FBQyxNQUFELEVBQVEsR0FBUixFQUFZLEdBQVosQ0FBWjtBQUFBLGdCQUE2QkMsUUFBUSxHQUFDTixRQUFRLENBQUMsQ0FBRCxDQUFSLENBQVl2SSxLQUFaLENBQWtCLEdBQWxCLENBQXRDO0FBQThELFdBQTlGLE1BQWtHO0FBQUMsZ0JBQUk0SSxPQUFPLEdBQUNMLFFBQVEsQ0FBQyxDQUFELENBQVIsQ0FBWXZJLEtBQVosQ0FBa0IsR0FBbEIsQ0FBWjtBQUFBLGdCQUFtQzZJLFFBQVEsR0FBQyxDQUFDLEdBQUQsRUFBSyxHQUFMLENBQTVDO0FBQXVEOztBQUN0WSxjQUFJTCxPQUFPLEdBQUMsSUFBSUMsSUFBSixDQUFTRyxPQUFPLENBQUMsQ0FBRCxDQUFoQixFQUFvQkEsT0FBTyxDQUFDLENBQUQsQ0FBUCxHQUFXLENBQS9CLEVBQWlDQSxPQUFPLENBQUMsQ0FBRCxDQUF4QyxFQUE0Q0MsUUFBUSxDQUFDLENBQUQsQ0FBcEQsRUFBd0RBLFFBQVEsQ0FBQyxDQUFELENBQWhFLENBQVo7QUFBaUZQLG9CQUFVLENBQUN4bEIsSUFBWCxDQUFnQjBsQixPQUFoQjtBQUEwQixTQUQ3RDtBQUMrRCxlQUFPRixVQUFQO0FBQW1COztBQUNoSSxlQUFTakIsT0FBVCxDQUFpQmxOLEtBQWpCLEVBQXVCMk8sTUFBdkIsRUFBOEI7QUFBQyxlQUFPaE4sSUFBSSxDQUFDeUwsS0FBTCxDQUFZdUIsTUFBTSxHQUFDM08sS0FBbkIsQ0FBUDtBQUFtQzs7QUFDbEUsZUFBUzRLLFFBQVQsQ0FBa0JnRSxLQUFsQixFQUF3QjtBQUFDLFlBQUlDLGFBQWEsR0FBQyxFQUFsQjs7QUFBcUIsYUFBSXhrQixDQUFDLEdBQUMsQ0FBTixFQUFRQSxDQUFDLEdBQUN1a0IsS0FBSyxDQUFDM2MsTUFBaEIsRUFBdUI1SCxDQUFDLEVBQXhCLEVBQTJCO0FBQUMsY0FBSTRpQixRQUFRLEdBQUNDLE9BQU8sQ0FBQzBCLEtBQUssQ0FBQ3ZrQixDQUFDLEdBQUMsQ0FBSCxDQUFOLEVBQVl1a0IsS0FBSyxDQUFDdmtCLENBQUQsQ0FBakIsQ0FBcEI7QUFBMEN3a0IsdUJBQWEsQ0FBQ2xtQixJQUFkLENBQW1Cc2tCLFFBQW5CO0FBQThCOztBQUNsSixlQUFPdEwsSUFBSSxDQUFDcUwsR0FBTCxDQUFTalgsS0FBVCxDQUFlLElBQWYsRUFBb0I4WSxhQUFwQixDQUFQO0FBQTJDOztBQUMzQyxlQUFTdEQsaUJBQVQsQ0FBMkJyVixFQUEzQixFQUE4QjtBQUFDLFlBQUlyUSxHQUFHLEdBQUNxUSxFQUFFLENBQUM0WSxTQUFYO0FBQXFCLFlBQUlDLElBQUksR0FBQzdZLEVBQUUsQ0FBQzhZLFVBQVo7QUFBdUIsWUFBSTduQixLQUFLLEdBQUMrTyxFQUFFLENBQUMrWSxXQUFiO0FBQXlCLFlBQUk5b0IsTUFBTSxHQUFDK1AsRUFBRSxDQUFDZ1osWUFBZDs7QUFBMkIsZUFBTWhaLEVBQUUsQ0FBQ2laLFlBQVQsRUFBc0I7QUFBQ2paLFlBQUUsR0FBQ0EsRUFBRSxDQUFDaVosWUFBTjtBQUFtQnRwQixhQUFHLElBQUVxUSxFQUFFLENBQUM0WSxTQUFSO0FBQWtCQyxjQUFJLElBQUU3WSxFQUFFLENBQUM4WSxVQUFUO0FBQXFCOztBQUNoTixlQUFPbnBCLEdBQUcsR0FBRVgsTUFBTSxDQUFDa3FCLFdBQVAsR0FBbUJscUIsTUFBTSxDQUFDa1EsV0FBL0IsSUFBNkMyWixJQUFJLEdBQUU3cEIsTUFBTSxDQUFDbXFCLFdBQVAsR0FBbUJucUIsTUFBTSxDQUFDcVgsVUFBN0UsSUFBMkYxVyxHQUFHLEdBQUNNLE1BQUwsR0FBYWpCLE1BQU0sQ0FBQ2txQixXQUE5RyxJQUE0SEwsSUFBSSxHQUFDNW5CLEtBQU4sR0FBYWpDLE1BQU0sQ0FBQ21xQixXQUF0SjtBQUFvSzs7QUFDcEssZUFBU2hFLE9BQVQsR0FBa0I7QUFBQyxlQUFPbm1CLE1BQU0sQ0FBQ2tuQixnQkFBUCxDQUF3QjdsQixRQUFRLENBQUMwUSxhQUFULENBQXVCLHlCQUF2QixDQUF4QixFQUEwRSxVQUExRSxFQUFzRnFWLGdCQUF0RixDQUF1RyxTQUF2RyxFQUFrSGhqQixPQUFsSCxDQUEwSCxJQUExSCxFQUErSCxFQUEvSCxFQUFtSUEsT0FBbkksQ0FBMkksSUFBM0ksRUFBZ0osRUFBaEosQ0FBUDtBQUE0SjtBQUFDLEtBeEJpZjtBQXdCL2VoQyxxQkFBaUIsQ0FBQ0MsS0FBbEIsQ0FBd0JDLFNBQXhCLENBQWtDLGdFQUFsQyxFQUFtRyxVQUFTQyxNQUFULEVBQWdCO0FBQUNqQyxZQUFNLENBQUMsMEJBQUQsQ0FBTixDQUFtQ2tCLElBQW5DLENBQXdDLFlBQVU7QUFBQyxZQUFJNG9CLFNBQVMsR0FBQzlwQixNQUFNLENBQUMsSUFBRCxDQUFwQjtBQUEyQjhwQixpQkFBUyxDQUFDN04sUUFBVixDQUFtQix5QkFBbkIsRUFBOENoQixLQUE5QyxDQUFvRCxZQUFVO0FBQUM2TyxtQkFBUyxDQUFDOWtCLElBQVYsQ0FBZSxzQkFBZixFQUF1Q3pELFdBQXZDLENBQW1ELE9BQW5EO0FBQTREdkIsZ0JBQU0sQ0FBQyxJQUFELENBQU4sQ0FBYXFaLElBQWIsQ0FBa0Isc0JBQWxCLEVBQTBDbFYsUUFBMUMsQ0FBbUQsT0FBbkQ7QUFBNkQsU0FBeEw7QUFBMkwsT0FBelE7QUFBNFEsS0FBaFk7QUFBa1lyQyxxQkFBaUIsQ0FBQ0MsS0FBbEIsQ0FBd0JDLFNBQXhCLENBQWtDLGtFQUFsQyxFQUFxRyxVQUFTQyxNQUFULEVBQWdCO0FBQUNqQyxZQUFNLENBQUMsVUFBU1ksQ0FBVCxFQUFXO0FBQUNaLGNBQU0sQ0FBQywrQ0FBRCxDQUFOLENBQXdEa0IsSUFBeEQsQ0FBNkQsWUFBVTtBQUFDLGNBQUl1SSxJQUFJLEdBQUN6SixNQUFNLENBQUMsSUFBRCxDQUFmO0FBQXNCLGNBQUl1SyxJQUFJLEdBQUNkLElBQUksQ0FBQ2pHLElBQUwsQ0FBVSxXQUFWLENBQVQ7QUFBZ0MrRyxjQUFJLEdBQUM3SSxRQUFRLENBQUM2SSxJQUFELENBQWI7QUFBb0IsY0FBSVIsTUFBTSxHQUFDLEVBQVg7O0FBQWMsa0JBQU9RLElBQVA7QUFDNTFCLGlCQUFLLENBQUw7QUFBT1Isb0JBQU0sR0FBQyxFQUFQO0FBQVU7O0FBQU0saUJBQUssQ0FBTDtBQUFPQSxvQkFBTSxHQUFDLEVBQVA7QUFBVTs7QUFBTSxpQkFBSyxDQUFMO0FBQU9BLG9CQUFNLEdBQUMsRUFBUDtBQUFVOztBQUFNLGlCQUFLLENBQUw7QUFBT0Esb0JBQU0sR0FBQyxFQUFQO0FBQVU7QUFEc3dCOztBQUU3MUJOLGNBQUksQ0FBQ0MsWUFBTCxHQUFvQkMsSUFBcEIsQ0FBeUIsWUFBVTtBQUFDRixnQkFBSSxDQUFDRyxPQUFMLENBQWE7QUFBQ0MsMEJBQVksRUFBQyxvQkFBZDtBQUFtQ0MseUJBQVcsRUFBQyxvQkFBL0M7QUFBb0VDLG9CQUFNLEVBQUNBO0FBQTNFLGFBQWI7QUFBa0csV0FBdEk7QUFBd0kvSixnQkFBTSxDQUFDLGdFQUFELENBQU4sQ0FBeUVrQixJQUF6RSxDQUE4RSxZQUFVO0FBQUMsZ0JBQUlDLFVBQVUsR0FBQ25CLE1BQU0sQ0FBQyxJQUFELENBQXJCO0FBQTRCbUIsc0JBQVUsQ0FBQ0csTUFBWCxDQUFrQiwrQkFBbEIsRUFBbURDLFdBQW5ELENBQStELE1BQS9EO0FBQXVFLGdCQUFJZ0osSUFBSSxHQUFDZCxJQUFJLENBQUNqRyxJQUFMLENBQVUsV0FBVixDQUFUO0FBQWdDLGdCQUFJdUcsTUFBTSxHQUFDLEVBQVg7O0FBQWMsb0JBQU9RLElBQVA7QUFDalgsbUJBQUssQ0FBTDtBQUFPUixzQkFBTSxHQUFDLEVBQVA7QUFBVTs7QUFBTSxtQkFBSyxDQUFMO0FBQU9BLHNCQUFNLEdBQUMsRUFBUDtBQUFVOztBQUFNLG1CQUFLLENBQUw7QUFBT0Esc0JBQU0sR0FBQyxFQUFQO0FBQVU7O0FBQU0sbUJBQUssQ0FBTDtBQUFPQSxzQkFBTSxHQUFDLEVBQVA7QUFBVTtBQUQyUjs7QUFFbFgvSixrQkFBTSxDQUFDLElBQUQsQ0FBTixDQUFhb0IsSUFBYixDQUFrQjtBQUFDQywyQkFBYSxFQUFDLHlCQUFVO0FBQUNvSSxvQkFBSSxDQUFDRyxPQUFMLENBQWE7QUFBQ0MsOEJBQVksRUFBQywyQ0FBZDtBQUEwREMsNkJBQVcsRUFBQywyQ0FBdEU7QUFBa0hDLHdCQUFNLEVBQUNBO0FBQXpILGlCQUFiO0FBQWdKO0FBQTFLLGFBQWxCO0FBQWlNLFdBRnpEO0FBRTJEL0osZ0JBQU0sQ0FBQyw4QkFBRCxDQUFOLENBQXVDa0IsSUFBdkMsQ0FBNEMsWUFBVTtBQUFDLGdCQUFJNm9CLGFBQWEsR0FBQy9wQixNQUFNLENBQUMsSUFBRCxDQUF4QjtBQUErQixnQkFBSWdxQixhQUFhLEdBQUMsRUFBbEI7QUFBcUJELHlCQUFhLENBQUMva0IsSUFBZCxDQUFtQixpQkFBbkIsRUFBc0NuRSxFQUF0QyxDQUF5QyxPQUF6QyxFQUFpRCxZQUFVO0FBQUNrcEIsMkJBQWEsQ0FBQy9rQixJQUFkLENBQW1CLGlCQUFuQixFQUFzQ3pELFdBQXRDLENBQWtELFFBQWxEO0FBQTREdkIsb0JBQU0sQ0FBQyxJQUFELENBQU4sQ0FBYW1FLFFBQWIsQ0FBc0IsUUFBdEI7QUFBZ0M2bEIsMkJBQWEsR0FBQ2hxQixNQUFNLENBQUMsSUFBRCxDQUFOLENBQWF3RCxJQUFiLENBQWtCLFVBQWxCLENBQWQ7QUFBNEMsa0JBQUl5bUIsT0FBTyxHQUFDRixhQUFhLENBQUMva0IsSUFBZCxDQUFtQiwrQ0FBbkIsQ0FBWjtBQUFnRmlsQixxQkFBTyxDQUFDQyxNQUFSLENBQWUsR0FBZixFQUFtQixDQUFuQjtBQUFzQkQscUJBQU8sQ0FBQ2psQixJQUFSLENBQWEsaUNBQWIsRUFBZ0QvRCxHQUFoRCxDQUFvRDtBQUFDa1QsdUJBQU8sRUFBQyxDQUFUO0FBQVdnVyx1QkFBTyxFQUFDLE1BQW5CO0FBQTBCbGIseUJBQVMsRUFBQztBQUFwQyxlQUFwRDtBQUF1R2diLHFCQUFPLENBQUNqbEIsSUFBUixDQUFhLGlDQUFiLEVBQWdEb2xCLEdBQWhELENBQW9ELE1BQUlKLGFBQXhELEVBQXVFSyxPQUF2RSxHQUFpRjlvQixXQUFqRixDQUE2RixXQUE3RjtBQUEwR3NILHdCQUFVLENBQUMsWUFBVTtBQUFDN0ksc0JBQU0sQ0FBQyxNQUFJZ3FCLGFBQUwsQ0FBTixDQUEwQjNnQixNQUExQixHQUFtQ2xGLFFBQW5DLENBQTRDLFdBQTVDO0FBQXlEbkUsc0JBQU0sQ0FBQyxNQUFJZ3FCLGFBQUwsQ0FBTixDQUEwQi9vQixHQUExQixDQUE4QjtBQUFDa1QseUJBQU8sRUFBQyxDQUFUO0FBQVdnVyx5QkFBTyxFQUFDLE9BQW5CO0FBQTJCbGIsMkJBQVMsRUFBQztBQUFyQyxpQkFBOUI7QUFBa0ZnYix1QkFBTyxDQUFDcmdCLE9BQVIsQ0FBZ0IsU0FBaEI7QUFBMkIsb0JBQUkwZ0IsS0FBSyxHQUFDTCxPQUFPLENBQUNyZ0IsT0FBUixDQUFnQjtBQUFDQyw4QkFBWSxFQUFDLDJDQUFkO0FBQTBEQyw2QkFBVyxFQUFDLDJDQUF0RTtBQUFrSEMsd0JBQU0sRUFBQ0E7QUFBekgsaUJBQWhCLENBQVY7QUFBNEp1Z0IscUJBQUssQ0FBQzFnQixPQUFOLENBQWMsYUFBZDtBQUE2QmYsMEJBQVUsQ0FBQyxZQUFVO0FBQUNvaEIseUJBQU8sQ0FBQ0MsTUFBUixDQUFlLEdBQWYsRUFBbUIsQ0FBbkI7QUFBdUIsaUJBQW5DLEVBQW9DLEdBQXBDLENBQVY7QUFBb0QsZUFBL1osRUFBZ2EsR0FBaGEsQ0FBVjtBQUFnYixhQUEzNkI7QUFBODZCLFdBQXpoQztBQUE0aEMsU0FKbGlCO0FBSXFpQixPQUpsakIsQ0FBTjtBQUkyakIsS0FKanJCO0FBSW1yQnBvQixxQkFBaUIsQ0FBQ0MsS0FBbEIsQ0FBd0JDLFNBQXhCLENBQWtDLHVFQUFsQyxFQUEwRyxVQUFTQyxNQUFULEVBQWdCO0FBQUNqQyxZQUFNLENBQUMsVUFBU1ksQ0FBVCxFQUFXO0FBQUNaLGNBQU0sQ0FBQywrQ0FBRCxDQUFOLENBQXdEa0IsSUFBeEQsQ0FBNkQsWUFBVTtBQUFDLGNBQUl1SSxJQUFJLEdBQUN6SixNQUFNLENBQUMsSUFBRCxDQUFmO0FBQXNCLGNBQUl1SyxJQUFJLEdBQUNkLElBQUksQ0FBQ2pHLElBQUwsQ0FBVSxXQUFWLENBQVQ7QUFBZ0MrRyxjQUFJLEdBQUM3SSxRQUFRLENBQUM2SSxJQUFELENBQWI7QUFBb0IsY0FBSVIsTUFBTSxHQUFDLEVBQVg7O0FBQWMsa0JBQU9RLElBQVA7QUFDcGhELGlCQUFLLENBQUw7QUFBT1Isb0JBQU0sR0FBQyxFQUFQO0FBQVU7O0FBQU0saUJBQUssQ0FBTDtBQUFPQSxvQkFBTSxHQUFDLEVBQVA7QUFBVTs7QUFBTSxpQkFBSyxDQUFMO0FBQU9BLG9CQUFNLEdBQUMsRUFBUDtBQUFVOztBQUFNLGlCQUFLLENBQUw7QUFBT0Esb0JBQU0sR0FBQyxFQUFQO0FBQVU7QUFEODdDOztBQUVyaEROLGNBQUksQ0FBQ0MsWUFBTCxHQUFvQkMsSUFBcEIsQ0FBeUIsWUFBVTtBQUFDRixnQkFBSSxDQUFDRyxPQUFMLENBQWE7QUFBQ0MsMEJBQVksRUFBQywyQ0FBZDtBQUEwREMseUJBQVcsRUFBQywyQ0FBdEU7QUFBa0hDLG9CQUFNLEVBQUNBO0FBQXpILGFBQWI7QUFBZ0osV0FBcEw7QUFBc0wvSixnQkFBTSxDQUFDLGdFQUFELENBQU4sQ0FBeUVrQixJQUF6RSxDQUE4RSxZQUFVO0FBQUMsZ0JBQUlDLFVBQVUsR0FBQ25CLE1BQU0sQ0FBQyxJQUFELENBQXJCO0FBQTRCbUIsc0JBQVUsQ0FBQ0csTUFBWCxDQUFrQiwrQkFBbEIsRUFBbURDLFdBQW5ELENBQStELE1BQS9EO0FBQXVFLGdCQUFJZ0osSUFBSSxHQUFDZCxJQUFJLENBQUNqRyxJQUFMLENBQVUsV0FBVixDQUFUO0FBQWdDLGdCQUFJdUcsTUFBTSxHQUFDLEVBQVg7O0FBQWMsb0JBQU9RLElBQVA7QUFDL1osbUJBQUssQ0FBTDtBQUFPUixzQkFBTSxHQUFDLEVBQVA7QUFBVTs7QUFBTSxtQkFBSyxDQUFMO0FBQU9BLHNCQUFNLEdBQUMsRUFBUDtBQUFVOztBQUFNLG1CQUFLLENBQUw7QUFBT0Esc0JBQU0sR0FBQyxFQUFQO0FBQVU7O0FBQU0sbUJBQUssQ0FBTDtBQUFPQSxzQkFBTSxHQUFDLEVBQVA7QUFBVTtBQUR5VTs7QUFFaGEvSixrQkFBTSxDQUFDLElBQUQsQ0FBTixDQUFhb0IsSUFBYixDQUFrQjtBQUFDQywyQkFBYSxFQUFDLHlCQUFVO0FBQUNvSSxvQkFBSSxDQUFDRyxPQUFMLENBQWE7QUFBQ0MsOEJBQVksRUFBQyxvQkFBZDtBQUFtQ0MsNkJBQVcsRUFBQyxvQkFBL0M7QUFBb0VDLHdCQUFNLEVBQUNBO0FBQTNFLGlCQUFiO0FBQWtHO0FBQTVILGFBQWxCO0FBQW1KLFdBRm1DO0FBRWpDL0osZ0JBQU0sQ0FBQyw4QkFBRCxDQUFOLENBQXVDa0IsSUFBdkMsQ0FBNEMsWUFBVTtBQUFDLGdCQUFJNm9CLGFBQWEsR0FBQy9wQixNQUFNLENBQUMsSUFBRCxDQUF4QjtBQUErQixnQkFBSWdxQixhQUFhLEdBQUMsRUFBbEI7QUFBcUJELHlCQUFhLENBQUMva0IsSUFBZCxDQUFtQixpQkFBbkIsRUFBc0NuRSxFQUF0QyxDQUF5QyxPQUF6QyxFQUFpRCxZQUFVO0FBQUNrcEIsMkJBQWEsQ0FBQy9rQixJQUFkLENBQW1CLGlCQUFuQixFQUFzQ3pELFdBQXRDLENBQWtELFFBQWxEO0FBQTREdkIsb0JBQU0sQ0FBQyxJQUFELENBQU4sQ0FBYW1FLFFBQWIsQ0FBc0IsUUFBdEI7QUFBZ0M2bEIsMkJBQWEsR0FBQ2hxQixNQUFNLENBQUMsSUFBRCxDQUFOLENBQWF3RCxJQUFiLENBQWtCLFVBQWxCLENBQWQ7QUFBNEMsa0JBQUl5bUIsT0FBTyxHQUFDRixhQUFhLENBQUMva0IsSUFBZCxDQUFtQiwrQ0FBbkIsQ0FBWjtBQUFnRmlsQixxQkFBTyxDQUFDQyxNQUFSLENBQWUsR0FBZixFQUFtQixDQUFuQjtBQUFzQkQscUJBQU8sQ0FBQ2psQixJQUFSLENBQWEsaUNBQWIsRUFBZ0QvRCxHQUFoRCxDQUFvRDtBQUFDa1QsdUJBQU8sRUFBQyxDQUFUO0FBQVdnVyx1QkFBTyxFQUFDLE1BQW5CO0FBQTBCbGIseUJBQVMsRUFBQztBQUFwQyxlQUFwRDtBQUF1R2diLHFCQUFPLENBQUNqbEIsSUFBUixDQUFhLGlDQUFiLEVBQWdEb2xCLEdBQWhELENBQW9ELE1BQUlKLGFBQXhELEVBQXVFSyxPQUF2RSxHQUFpRjlvQixXQUFqRixDQUE2RixXQUE3RjtBQUEwR3NILHdCQUFVLENBQUMsWUFBVTtBQUFDN0ksc0JBQU0sQ0FBQyxNQUFJZ3FCLGFBQUwsQ0FBTixDQUEwQjNnQixNQUExQixHQUFtQ2xGLFFBQW5DLENBQTRDLFdBQTVDO0FBQXlEbkUsc0JBQU0sQ0FBQyxNQUFJZ3FCLGFBQUwsQ0FBTixDQUEwQi9vQixHQUExQixDQUE4QjtBQUFDa1QseUJBQU8sRUFBQyxDQUFUO0FBQVdnVyx5QkFBTyxFQUFDLE9BQW5CO0FBQTJCbGIsMkJBQVMsRUFBQztBQUFyQyxpQkFBOUI7QUFBa0ZnYix1QkFBTyxDQUFDcmdCLE9BQVIsQ0FBZ0IsU0FBaEI7QUFBMkIsb0JBQUkwZ0IsS0FBSyxHQUFDTCxPQUFPLENBQUNyZ0IsT0FBUixDQUFnQjtBQUFDQyw4QkFBWSxFQUFDLDJDQUFkO0FBQTBEQyw2QkFBVyxFQUFDLDJDQUF0RTtBQUFrSEMsd0JBQU0sRUFBQ0E7QUFBekgsaUJBQWhCLENBQVY7QUFBNEp1Z0IscUJBQUssQ0FBQzFnQixPQUFOLENBQWMsYUFBZDtBQUE2QmYsMEJBQVUsQ0FBQyxZQUFVO0FBQUNvaEIseUJBQU8sQ0FBQ0MsTUFBUixDQUFlLEdBQWYsRUFBbUIsQ0FBbkI7QUFBdUIsaUJBQW5DLEVBQW9DLEdBQXBDLENBQVY7QUFBb0QsZUFBL1osRUFBZ2EsR0FBaGEsQ0FBVjtBQUFnYixhQUEzNkI7QUFBODZCLFdBQXpoQztBQUE0aEMsU0FKb007QUFJak0sT0FKb0wsQ0FBTjtBQUkzSyxLQUpnRDtBQUk5Q3BvQixxQkFBaUIsQ0FBQ0MsS0FBbEIsQ0FBd0JDLFNBQXhCLENBQWtDLG1FQUFsQyxFQUFzRyxVQUFTQyxNQUFULEVBQWdCO0FBQUMsVUFBSTZpQixTQUFTLEdBQUM5a0IsTUFBTSxDQUFDLHlCQUFELENBQXBCO0FBQWdELFVBQUkra0IsaUJBQWlCLEdBQUNELFNBQVMsQ0FBQ3RoQixJQUFWLENBQWUsY0FBZixDQUF0Qjs7QUFBcUQsVUFBR3VoQixpQkFBaUIsSUFBRSxFQUF0QixFQUNyNUM7QUFBQ0EseUJBQWlCLEdBQUMsRUFBbEI7QUFBc0I7O0FBQ3RCRCxlQUFTLENBQUNyWSxNQUFWLEdBQWlCLENBQWxCLElBQXNCdVksWUFBWSxDQUFDRixTQUFELENBQWxDOztBQUE4QyxlQUFTRSxZQUFULENBQXNCRixTQUF0QixFQUFnQztBQUFDQSxpQkFBUyxDQUFDNWpCLElBQVYsQ0FBZSxZQUFVO0FBQUMsY0FBSStqQixRQUFRLEdBQUNqbEIsTUFBTSxDQUFDLElBQUQsQ0FBbkI7QUFBQSxjQUEwQmtsQixrQkFBa0IsR0FBQyxFQUE3QztBQUFnREEsNEJBQWtCLENBQUMsaUJBQUQsQ0FBbEIsR0FBc0NELFFBQVEsQ0FBQ2pnQixJQUFULENBQWMsaUJBQWQsQ0FBdEM7QUFBdUVrZ0IsNEJBQWtCLENBQUMsZUFBRCxDQUFsQixHQUFvQ0Esa0JBQWtCLENBQUMsaUJBQUQsQ0FBbEIsQ0FBc0NqSixRQUF0QyxDQUErQyxTQUEvQyxDQUFwQztBQUE4RmlKLDRCQUFrQixDQUFDLGFBQUQsQ0FBbEIsR0FBa0NBLGtCQUFrQixDQUFDLGVBQUQsQ0FBbEIsQ0FBb0NqSixRQUFwQyxDQUE2QyxlQUE3QyxDQUFsQztBQUFnR2lKLDRCQUFrQixDQUFDLGdCQUFELENBQWxCLEdBQXFDQSxrQkFBa0IsQ0FBQyxlQUFELENBQWxCLENBQW9DbGdCLElBQXBDLENBQXlDLEdBQXpDLENBQXJDO0FBQW1Ga2dCLDRCQUFrQixDQUFDLGVBQUQsQ0FBbEIsR0FBb0NDLFNBQVMsQ0FBQ0Qsa0JBQWtCLENBQUMsZ0JBQUQsQ0FBbkIsQ0FBN0M7QUFBb0ZBLDRCQUFrQixDQUFDLGdCQUFELENBQWxCLEdBQXFDRSxRQUFRLENBQUNGLGtCQUFrQixDQUFDLGVBQUQsQ0FBbkIsQ0FBN0M7QUFBbUZBLDRCQUFrQixDQUFDLG9CQUFELENBQWxCLEdBQXlDRCxRQUFRLENBQUNqZ0IsSUFBVCxDQUFjLHlCQUFkLENBQXpDO0FBQWtGa2dCLDRCQUFrQixDQUFDLGVBQUQsQ0FBbEIsR0FBb0NELFFBQVEsQ0FBQ2hKLFFBQVQsQ0FBa0IsaUJBQWxCLENBQXBDO0FBQXlFb0oseUJBQWUsQ0FBQ0gsa0JBQUQsRUFBb0JILGlCQUFwQixDQUFmO0FBQXNELGNBQUlPLGdCQUFnQixHQUFDQyxnQkFBZ0IsQ0FBQ0wsa0JBQUQsRUFBb0JILGlCQUFwQixDQUFyQztBQUE0RUUsa0JBQVEsQ0FBQzlnQixRQUFULENBQWtCLFFBQWxCO0FBQTRCK2dCLDRCQUFrQixDQUFDLG9CQUFELENBQWxCLENBQXlDcmtCLEVBQXpDLENBQTRDLE9BQTVDLEVBQW9ELE9BQXBELEVBQTRELFVBQVMrVCxLQUFULEVBQWU7QUFBQ0EsaUJBQUssQ0FBQ3hMLGNBQU47QUFBdUJvYyx1QkFBVyxDQUFDTixrQkFBRCxFQUFvQkksZ0JBQXBCLEVBQXFDLE1BQXJDLENBQVg7QUFBeUQsV0FBNUo7QUFBOEpKLDRCQUFrQixDQUFDLG9CQUFELENBQWxCLENBQXlDcmtCLEVBQXpDLENBQTRDLE9BQTVDLEVBQW9ELE9BQXBELEVBQTRELFVBQVMrVCxLQUFULEVBQWU7QUFBQ0EsaUJBQUssQ0FBQ3hMLGNBQU47QUFBdUJvYyx1QkFBVyxDQUFDTixrQkFBRCxFQUFvQkksZ0JBQXBCLEVBQXFDLE1BQXJDLENBQVg7QUFBeUQsV0FBNUo7QUFBOEpKLDRCQUFrQixDQUFDLGVBQUQsQ0FBbEIsQ0FBb0Nya0IsRUFBcEMsQ0FBdUMsT0FBdkMsRUFBK0MsR0FBL0MsRUFBbUQsVUFBUytULEtBQVQsRUFBZTtBQUFDQSxpQkFBSyxDQUFDeEwsY0FBTjtBQUF1QjhiLDhCQUFrQixDQUFDLGdCQUFELENBQWxCLENBQXFDM2pCLFdBQXJDLENBQWlELFVBQWpEO0FBQTZEdkIsa0JBQU0sQ0FBQyxJQUFELENBQU4sQ0FBYW1FLFFBQWIsQ0FBc0IsVUFBdEI7QUFBa0NzaEIsNkJBQWlCLENBQUN6bEIsTUFBTSxDQUFDLElBQUQsQ0FBUCxDQUFqQjtBQUFnQzBsQix5QkFBYSxDQUFDMWxCLE1BQU0sQ0FBQyxJQUFELENBQVAsRUFBY2tsQixrQkFBa0IsQ0FBQyxhQUFELENBQWhDLEVBQWdESSxnQkFBaEQsQ0FBYjtBQUErRUssZ0NBQW9CLENBQUMzbEIsTUFBTSxDQUFDLElBQUQsQ0FBUCxFQUFja2xCLGtCQUFrQixDQUFDLGVBQUQsQ0FBaEMsQ0FBcEI7QUFBd0UsV0FBaFg7QUFBa1hBLDRCQUFrQixDQUFDLGVBQUQsQ0FBbEIsQ0FBb0Nya0IsRUFBcEMsQ0FBdUMsV0FBdkMsRUFBbUQsWUFBVTtBQUFDLGdCQUFJK2tCLEVBQUUsR0FBQ0MsT0FBTyxFQUFkO0FBQWtCRCxjQUFFLElBQUUsUUFBTCxJQUFnQkUsY0FBYyxDQUFDWixrQkFBRCxFQUFvQkksZ0JBQXBCLEVBQXFDLE1BQXJDLENBQTlCO0FBQTRFLFdBQTNKO0FBQTZKSiw0QkFBa0IsQ0FBQyxlQUFELENBQWxCLENBQW9DcmtCLEVBQXBDLENBQXVDLFlBQXZDLEVBQW9ELFlBQVU7QUFBQyxnQkFBSStrQixFQUFFLEdBQUNDLE9BQU8sRUFBZDtBQUFrQkQsY0FBRSxJQUFFLFFBQUwsSUFBZ0JFLGNBQWMsQ0FBQ1osa0JBQUQsRUFBb0JJLGdCQUFwQixFQUFxQyxNQUFyQyxDQUE5QjtBQUE0RSxXQUE1SjtBQUE4SnRsQixnQkFBTSxDQUFDZSxRQUFELENBQU4sQ0FBaUIwZ0IsS0FBakIsQ0FBdUIsVUFBUzdNLEtBQVQsRUFBZTtBQUFDLGdCQUFHQSxLQUFLLENBQUM1QixLQUFOLElBQWEsSUFBYixJQUFtQitTLGlCQUFpQixDQUFDZCxRQUFRLENBQUNlLEdBQVQsQ0FBYSxDQUFiLENBQUQsQ0FBdkMsRUFBeUQ7QUFBQ0YsNEJBQWMsQ0FBQ1osa0JBQUQsRUFBb0JJLGdCQUFwQixFQUFxQyxNQUFyQyxDQUFkO0FBQTRELGFBQXRILE1BQTJILElBQUcxUSxLQUFLLENBQUM1QixLQUFOLElBQWEsSUFBYixJQUFtQitTLGlCQUFpQixDQUFDZCxRQUFRLENBQUNlLEdBQVQsQ0FBYSxDQUFiLENBQUQsQ0FBdkMsRUFBeUQ7QUFBQ0YsNEJBQWMsQ0FBQ1osa0JBQUQsRUFBb0JJLGdCQUFwQixFQUFxQyxNQUFyQyxDQUFkO0FBQTREO0FBQUMsV0FBelI7QUFBNFIsU0FBdm9FO0FBQTBvRTs7QUFDenRFLGVBQVNFLFdBQVQsQ0FBcUJOLGtCQUFyQixFQUF3Q0ksZ0JBQXhDLEVBQXlEVyxNQUF6RCxFQUFnRTtBQUFDLFlBQUlDLGNBQWMsR0FBQ0MsaUJBQWlCLENBQUNqQixrQkFBa0IsQ0FBQyxlQUFELENBQW5CLENBQXBDO0FBQUEsWUFBMEVrQixZQUFZLEdBQUMxSixNQUFNLENBQUN3SSxrQkFBa0IsQ0FBQyxpQkFBRCxDQUFsQixDQUFzQ2prQixHQUF0QyxDQUEwQyxPQUExQyxFQUFtRDZDLE9BQW5ELENBQTJELElBQTNELEVBQWdFLEVBQWhFLENBQUQsQ0FBN0Y7QUFBb0ttaUIsY0FBTSxJQUFFLE1BQVQsR0FBaUJJLGlCQUFpQixDQUFDbkIsa0JBQUQsRUFBb0JnQixjQUFjLEdBQUNFLFlBQWYsR0FBNEJyQixpQkFBaEQsRUFBa0VxQixZQUFZLEdBQUNkLGdCQUEvRSxDQUFsQyxHQUFtSWUsaUJBQWlCLENBQUNuQixrQkFBRCxFQUFvQmdCLGNBQWMsR0FBQ0UsWUFBZixHQUE0QnJCLGlCQUFoRCxDQUFwSjtBQUF3Tjs7QUFDNWIsZUFBU2UsY0FBVCxDQUF3Qlosa0JBQXhCLEVBQTJDSSxnQkFBM0MsRUFBNERXLE1BQTVELEVBQW1FO0FBQUMsWUFBSUssY0FBYyxHQUFDcEIsa0JBQWtCLENBQUMsZUFBRCxDQUFsQixDQUFvQ2xnQixJQUFwQyxDQUF5QyxXQUF6QyxDQUFuQjtBQUFBLFlBQXlFdWhCLFVBQVUsR0FBRU4sTUFBTSxJQUFFLE1BQVQsR0FBaUJLLGNBQWMsQ0FBQ2pOLElBQWYsRUFBakIsR0FBdUNpTixjQUFjLENBQUNoTixJQUFmLEVBQTNIOztBQUFpSixZQUFHaU4sVUFBVSxDQUFDOVosTUFBWCxHQUFrQixDQUFyQixFQUF1QjtBQUFDLGNBQUkrWixZQUFZLEdBQUN0QixrQkFBa0IsQ0FBQyxlQUFELENBQWxCLENBQW9DbGdCLElBQXBDLENBQXlDLFdBQXpDLENBQWpCO0FBQUEsY0FBdUV5aEIsUUFBUSxHQUFFUixNQUFNLElBQUUsTUFBVCxHQUFpQk8sWUFBWSxDQUFDbGxCLE1BQWIsQ0FBb0IsSUFBcEIsRUFBMEIrWCxJQUExQixDQUErQixJQUEvQixFQUFxQzRDLFFBQXJDLENBQThDLEdBQTlDLENBQWpCLEdBQW9FdUssWUFBWSxDQUFDbGxCLE1BQWIsQ0FBb0IsSUFBcEIsRUFBMEJnWSxJQUExQixDQUErQixJQUEvQixFQUFxQzJDLFFBQXJDLENBQThDLEdBQTlDLENBQXBKO0FBQXVNeUosdUJBQWEsQ0FBQ2UsUUFBRCxFQUFVdkIsa0JBQWtCLENBQUMsYUFBRCxDQUE1QixFQUE0Q0ksZ0JBQTVDLENBQWI7QUFBMkVLLDhCQUFvQixDQUFDYyxRQUFELEVBQVV2QixrQkFBa0IsQ0FBQyxlQUFELENBQTVCLENBQXBCO0FBQW1FdUIsa0JBQVEsQ0FBQ3RpQixRQUFULENBQWtCLFVBQWxCO0FBQThCcWlCLHNCQUFZLENBQUNqbEIsV0FBYixDQUF5QixVQUF6QjtBQUFxQ2trQiwyQkFBaUIsQ0FBQ2dCLFFBQUQsQ0FBakI7QUFBNEJDLGdDQUFzQixDQUFDVCxNQUFELEVBQVFRLFFBQVIsRUFBaUJ2QixrQkFBakIsRUFBb0NJLGdCQUFwQyxDQUF0QjtBQUE2RTtBQUFDOztBQUMvdUIsZUFBU29CLHNCQUFULENBQWdDVCxNQUFoQyxFQUF1Q3JSLEtBQXZDLEVBQTZDc1Esa0JBQTdDLEVBQWdFSSxnQkFBaEUsRUFBaUY7QUFBQyxZQUFJcUIsVUFBVSxHQUFDam5CLE1BQU0sQ0FBQ2tuQixnQkFBUCxDQUF3QmhTLEtBQUssQ0FBQ29SLEdBQU4sQ0FBVSxDQUFWLENBQXhCLEVBQXFDLElBQXJDLENBQWY7QUFBQSxZQUEwRGEsU0FBUyxHQUFDbkssTUFBTSxDQUFDaUssVUFBVSxDQUFDRyxnQkFBWCxDQUE0QixNQUE1QixFQUFvQ2hqQixPQUFwQyxDQUE0QyxJQUE1QyxFQUFpRCxFQUFqRCxDQUFELENBQTFFO0FBQUEsWUFBaUlpakIsYUFBYSxHQUFDckssTUFBTSxDQUFDd0ksa0JBQWtCLENBQUMsaUJBQUQsQ0FBbEIsQ0FBc0Nqa0IsR0FBdEMsQ0FBMEMsT0FBMUMsRUFBbUQ2QyxPQUFuRCxDQUEyRCxJQUEzRCxFQUFnRSxFQUFoRSxDQUFELENBQXJKO0FBQUEsWUFBMk53aEIsZ0JBQWdCLEdBQUM1SSxNQUFNLENBQUN3SSxrQkFBa0IsQ0FBQyxlQUFELENBQWxCLENBQW9DamtCLEdBQXBDLENBQXdDLE9BQXhDLEVBQWlENkMsT0FBakQsQ0FBeUQsSUFBekQsRUFBOEQsRUFBOUQsQ0FBRCxDQUFsUDtBQUFzVCxZQUFJa2pCLGlCQUFpQixHQUFDYixpQkFBaUIsQ0FBQ2pCLGtCQUFrQixDQUFDLGVBQUQsQ0FBbkIsQ0FBdkM7O0FBQTZFLFlBQUllLE1BQU0sSUFBRSxNQUFSLElBQWdCWSxTQUFTLEdBQUNFLGFBQWEsR0FBQ0MsaUJBQXpDLElBQThEZixNQUFNLElBQUUsTUFBUixJQUFnQlksU0FBUyxHQUFDLENBQUNHLGlCQUE1RixFQUErRztBQUFDWCwyQkFBaUIsQ0FBQ25CLGtCQUFELEVBQW9CLENBQUMyQixTQUFELEdBQVdFLGFBQWEsR0FBQyxDQUE3QyxFQUErQ0EsYUFBYSxHQUFDekIsZ0JBQTdELENBQWpCO0FBQWlHO0FBQUM7O0FBQ3ZxQixlQUFTZSxpQkFBVCxDQUEyQm5CLGtCQUEzQixFQUE4Q3hNLEtBQTlDLEVBQW9EdU8sUUFBcEQsRUFBNkQ7QUFBQyxZQUFJQyxhQUFhLEdBQUNoQyxrQkFBa0IsQ0FBQyxlQUFELENBQWxCLENBQW9DYyxHQUFwQyxDQUF3QyxDQUF4QyxDQUFsQjtBQUE2RHROLGFBQUssR0FBRUEsS0FBSyxHQUFDLENBQVAsR0FBVSxDQUFWLEdBQVlBLEtBQWxCO0FBQXdCQSxhQUFLLEdBQUUsRUFBRSxPQUFPdU8sUUFBUCxLQUFrQixXQUFwQixLQUFrQ3ZPLEtBQUssR0FBQ3VPLFFBQXpDLEdBQW1EQSxRQUFuRCxHQUE0RHZPLEtBQWxFO0FBQXdFeU8seUJBQWlCLENBQUNELGFBQUQsRUFBZSxZQUFmLEVBQTRCeE8sS0FBSyxHQUFDLElBQWxDLENBQWpCO0FBQTBEQSxhQUFLLElBQUUsQ0FBUixHQUFXd00sa0JBQWtCLENBQUMsb0JBQUQsQ0FBbEIsQ0FBeUNsZ0IsSUFBekMsQ0FBOEMsT0FBOUMsRUFBdURiLFFBQXZELENBQWdFLFVBQWhFLENBQVgsR0FBdUYrZ0Isa0JBQWtCLENBQUMsb0JBQUQsQ0FBbEIsQ0FBeUNsZ0IsSUFBekMsQ0FBOEMsT0FBOUMsRUFBdUR6RCxXQUF2RCxDQUFtRSxVQUFuRSxDQUF2RjtBQUF1S21YLGFBQUssSUFBRXVPLFFBQVIsR0FBa0IvQixrQkFBa0IsQ0FBQyxvQkFBRCxDQUFsQixDQUF5Q2xnQixJQUF6QyxDQUE4QyxPQUE5QyxFQUF1RGIsUUFBdkQsQ0FBZ0UsVUFBaEUsQ0FBbEIsR0FBOEYrZ0Isa0JBQWtCLENBQUMsb0JBQUQsQ0FBbEIsQ0FBeUNsZ0IsSUFBekMsQ0FBOEMsT0FBOUMsRUFBdUR6RCxXQUF2RCxDQUFtRSxVQUFuRSxDQUE5RjtBQUE4Szs7QUFDeG1CLGVBQVNta0IsYUFBVCxDQUF1QjBCLGFBQXZCLEVBQXFDQyxPQUFyQyxFQUE2Q0osUUFBN0MsRUFBc0Q7QUFBQyxZQUFJTixVQUFVLEdBQUNqbkIsTUFBTSxDQUFDa25CLGdCQUFQLENBQXdCUSxhQUFhLENBQUNwQixHQUFkLENBQWtCLENBQWxCLENBQXhCLEVBQTZDLElBQTdDLENBQWY7QUFBQSxZQUFrRWEsU0FBUyxHQUFDRixVQUFVLENBQUNHLGdCQUFYLENBQTRCLE1BQTVCLENBQTVFO0FBQUEsWUFBZ0hRLFVBQVUsR0FBQ1gsVUFBVSxDQUFDRyxnQkFBWCxDQUE0QixPQUE1QixDQUEzSDtBQUFnS0QsaUJBQVMsR0FBQ25LLE1BQU0sQ0FBQ21LLFNBQVMsQ0FBQy9pQixPQUFWLENBQWtCLElBQWxCLEVBQXVCLEVBQXZCLENBQUQsQ0FBTixHQUFtQzRZLE1BQU0sQ0FBQzRLLFVBQVUsQ0FBQ3hqQixPQUFYLENBQW1CLElBQW5CLEVBQXdCLEVBQXhCLENBQUQsQ0FBTixHQUFvQyxDQUFqRjtBQUFtRixZQUFJeWpCLFVBQVUsR0FBQ1YsU0FBUyxHQUFDSSxRQUF6QjtBQUFrQ0UseUJBQWlCLENBQUNFLE9BQU8sQ0FBQ3JCLEdBQVIsQ0FBWSxDQUFaLENBQUQsRUFBZ0IsUUFBaEIsRUFBeUJ1QixVQUF6QixDQUFqQjtBQUF1RDs7QUFDblksZUFBU2xDLGVBQVQsQ0FBeUJILGtCQUF6QixFQUE0Q3NDLEdBQTVDLEVBQWdEO0FBQUMsYUFBSTNpQixDQUFDLEdBQUMsQ0FBTixFQUFRQSxDQUFDLEdBQUNxZ0Isa0JBQWtCLENBQUMsZUFBRCxDQUFsQixDQUFvQ3pZLE1BQTlDLEVBQXFENUgsQ0FBQyxFQUF0RCxFQUF5RDtBQUFDLGNBQUk0aUIsUUFBUSxHQUFDQyxPQUFPLENBQUN4QyxrQkFBa0IsQ0FBQyxlQUFELENBQWxCLENBQW9DLENBQXBDLENBQUQsRUFBd0NBLGtCQUFrQixDQUFDLGVBQUQsQ0FBbEIsQ0FBb0NyZ0IsQ0FBcEMsQ0FBeEMsQ0FBcEI7QUFBQSxjQUFvRzhpQixZQUFZLEdBQUN4TCxJQUFJLENBQUN5TCxLQUFMLENBQVdILFFBQVEsR0FBQ3ZDLGtCQUFrQixDQUFDLGdCQUFELENBQXRDLElBQTBELENBQTNLO0FBQTZLQSw0QkFBa0IsQ0FBQyxnQkFBRCxDQUFsQixDQUFxQ2piLEVBQXJDLENBQXdDcEYsQ0FBeEMsRUFBMkM1RCxHQUEzQyxDQUErQyxNQUEvQyxFQUFzRDBtQixZQUFZLEdBQUNILEdBQWIsR0FBaUIsSUFBdkU7QUFBOEU7QUFBQzs7QUFDdlcsZUFBU2pDLGdCQUFULENBQTBCTCxrQkFBMUIsRUFBNkN2akIsS0FBN0MsRUFBbUQ7QUFBQyxZQUFJa21CLFFBQVEsR0FBQ0gsT0FBTyxDQUFDeEMsa0JBQWtCLENBQUMsZUFBRCxDQUFsQixDQUFvQyxDQUFwQyxDQUFELEVBQXdDQSxrQkFBa0IsQ0FBQyxlQUFELENBQWxCLENBQW9DQSxrQkFBa0IsQ0FBQyxlQUFELENBQWxCLENBQW9DelksTUFBcEMsR0FBMkMsQ0FBL0UsQ0FBeEMsQ0FBcEI7QUFBQSxZQUErSXFiLFlBQVksR0FBQ0QsUUFBUSxHQUFDM0Msa0JBQWtCLENBQUMsZ0JBQUQsQ0FBdkw7QUFBQSxZQUEwTTRDLFlBQVksR0FBQzNMLElBQUksQ0FBQ3lMLEtBQUwsQ0FBV0UsWUFBWCxJQUF5QixDQUFoUDtBQUFBLFlBQWtQQyxVQUFVLEdBQUNELFlBQVksR0FBQ25tQixLQUExUTtBQUFnUnVqQiwwQkFBa0IsQ0FBQyxlQUFELENBQWxCLENBQW9DamtCLEdBQXBDLENBQXdDLE9BQXhDLEVBQWdEOG1CLFVBQVUsR0FBQyxJQUEzRDtBQUFpRXJDLHFCQUFhLENBQUNSLGtCQUFrQixDQUFDLGdCQUFELENBQWxCLENBQXFDamIsRUFBckMsQ0FBd0MsQ0FBeEMsQ0FBRCxFQUE0Q2liLGtCQUFrQixDQUFDLGFBQUQsQ0FBOUQsRUFBOEU2QyxVQUE5RSxDQUFiO0FBQXVHLGVBQU9BLFVBQVA7QUFBbUI7O0FBQy9mLGVBQVNwQyxvQkFBVCxDQUE4Qi9RLEtBQTlCLEVBQW9Db1QsYUFBcEMsRUFBa0Q7QUFBQyxZQUFJQyxTQUFTLEdBQUNyVCxLQUFLLENBQUN2UyxJQUFOLENBQVcsTUFBWCxDQUFkO0FBQUEsWUFBaUNpa0IsY0FBYyxHQUFDMEIsYUFBYSxDQUFDaGpCLElBQWQsQ0FBbUIsV0FBbkIsQ0FBaEQ7QUFBQSxZQUFnRmtqQixlQUFlLEdBQUNGLGFBQWEsQ0FBQ2hqQixJQUFkLENBQW1CLGlCQUFlaWpCLFNBQWYsR0FBeUIsSUFBNUMsQ0FBaEc7QUFBQSxZQUFrSkUscUJBQXFCLEdBQUNELGVBQWUsQ0FBQ3ZuQixNQUFoQixFQUF4Szs7QUFBaU0sWUFBR3VuQixlQUFlLENBQUNsZSxLQUFoQixLQUF3QnNjLGNBQWMsQ0FBQ3RjLEtBQWYsRUFBM0IsRUFBa0Q7QUFBQyxjQUFJb2UsY0FBYyxHQUFDLHNCQUFuQjtBQUFBLGNBQTBDQyxZQUFZLEdBQUMsWUFBdkQ7QUFBcUUsU0FBeEgsTUFBNEg7QUFBQyxjQUFJRCxjQUFjLEdBQUMscUJBQW5CO0FBQUEsY0FBeUNDLFlBQVksR0FBQyxhQUF0RDtBQUFxRTs7QUFDdGJILHVCQUFlLENBQUMxa0IsSUFBaEIsQ0FBcUIsT0FBckIsRUFBNkI0a0IsY0FBN0I7QUFBNkM5QixzQkFBYyxDQUFDOWlCLElBQWYsQ0FBb0IsT0FBcEIsRUFBNEI2a0IsWUFBNUIsRUFBMENDLEdBQTFDLENBQThDLDhEQUE5QyxFQUE2RyxZQUFVO0FBQUNoQyx3QkFBYyxDQUFDL2tCLFdBQWYsQ0FBMkIsd0JBQTNCO0FBQXFEMm1CLHlCQUFlLENBQUMzbUIsV0FBaEIsQ0FBNEIsd0JBQTVCO0FBQXVELFNBQXBPO0FBQXNPeW1CLHFCQUFhLENBQUMvbUIsR0FBZCxDQUFrQixRQUFsQixFQUEyQmtuQixxQkFBcUIsR0FBQyxJQUFqRDtBQUF3RDs7QUFDM1UsZUFBUzFDLGlCQUFULENBQTJCN1EsS0FBM0IsRUFBaUM7QUFBQ0EsYUFBSyxDQUFDdFQsTUFBTixDQUFhLElBQWIsRUFBbUJ1YSxPQUFuQixDQUEyQixJQUEzQixFQUFpQ0ksUUFBakMsQ0FBMEMsR0FBMUMsRUFBK0M5WCxRQUEvQyxDQUF3RCxhQUF4RCxFQUF1RW1ZLEdBQXZFLEdBQTZFQSxHQUE3RSxHQUFtRlAsT0FBbkYsQ0FBMkYsSUFBM0YsRUFBaUdFLFFBQWpHLENBQTBHLEdBQTFHLEVBQStHMWEsV0FBL0csQ0FBMkgsYUFBM0g7QUFBMkk7O0FBQzdLLGVBQVM0a0IsaUJBQVQsQ0FBMkJsQixRQUEzQixFQUFvQztBQUFDLFlBQUlzRCxhQUFhLEdBQUM3b0IsTUFBTSxDQUFDa25CLGdCQUFQLENBQXdCM0IsUUFBUSxDQUFDZSxHQUFULENBQWEsQ0FBYixDQUF4QixFQUF3QyxJQUF4QyxDQUFsQjtBQUFBLFlBQWdFZ0IsaUJBQWlCLEdBQUN1QixhQUFhLENBQUN6QixnQkFBZCxDQUErQixtQkFBL0IsS0FBcUR5QixhQUFhLENBQUN6QixnQkFBZCxDQUErQixnQkFBL0IsQ0FBckQsSUFBdUd5QixhQUFhLENBQUN6QixnQkFBZCxDQUErQixlQUEvQixDQUF2RyxJQUF3SnlCLGFBQWEsQ0FBQ3pCLGdCQUFkLENBQStCLGNBQS9CLENBQXhKLElBQXdNeUIsYUFBYSxDQUFDekIsZ0JBQWQsQ0FBK0IsV0FBL0IsQ0FBMVI7O0FBQXNVLFlBQUdFLGlCQUFpQixDQUFDam5CLE9BQWxCLENBQTBCLEdBQTFCLEtBQWdDLENBQW5DLEVBQXFDO0FBQUMsY0FBSWluQixpQkFBaUIsR0FBQ0EsaUJBQWlCLENBQUMzRyxLQUFsQixDQUF3QixHQUF4QixFQUE2QixDQUE3QixDQUF0QjtBQUFzRDJHLDJCQUFpQixHQUFDQSxpQkFBaUIsQ0FBQzNHLEtBQWxCLENBQXdCLEdBQXhCLEVBQTZCLENBQTdCLENBQWxCO0FBQWtEMkcsMkJBQWlCLEdBQUNBLGlCQUFpQixDQUFDM0csS0FBbEIsQ0FBd0IsR0FBeEIsQ0FBbEI7QUFBK0MsY0FBSTZGLGNBQWMsR0FBQ2MsaUJBQWlCLENBQUMsQ0FBRCxDQUFwQztBQUF5QyxTQUF0TyxNQUEwTztBQUFDLGNBQUlkLGNBQWMsR0FBQyxDQUFuQjtBQUFzQjs7QUFDNW1CLGVBQU94SixNQUFNLENBQUN3SixjQUFELENBQWI7QUFBK0I7O0FBQy9CLGVBQVNpQixpQkFBVCxDQUEyQnFCLE9BQTNCLEVBQW1DQyxRQUFuQyxFQUE0Qy9QLEtBQTVDLEVBQWtEO0FBQUM4UCxlQUFPLENBQUN6WixLQUFSLENBQWMsbUJBQWQsSUFBbUMwWixRQUFRLEdBQUMsR0FBVCxHQUFhL1AsS0FBYixHQUFtQixHQUF0RDtBQUEwRDhQLGVBQU8sQ0FBQ3paLEtBQVIsQ0FBYyxnQkFBZCxJQUFnQzBaLFFBQVEsR0FBQyxHQUFULEdBQWEvUCxLQUFiLEdBQW1CLEdBQW5EO0FBQXVEOFAsZUFBTyxDQUFDelosS0FBUixDQUFjLGVBQWQsSUFBK0IwWixRQUFRLEdBQUMsR0FBVCxHQUFhL1AsS0FBYixHQUFtQixHQUFsRDtBQUFzRDhQLGVBQU8sQ0FBQ3paLEtBQVIsQ0FBYyxjQUFkLElBQThCMFosUUFBUSxHQUFDLEdBQVQsR0FBYS9QLEtBQWIsR0FBbUIsR0FBakQ7QUFBcUQ4UCxlQUFPLENBQUN6WixLQUFSLENBQWMsV0FBZCxJQUEyQjBaLFFBQVEsR0FBQyxHQUFULEdBQWEvUCxLQUFiLEdBQW1CLEdBQTlDO0FBQW1EOztBQUNsVSxlQUFTeU0sU0FBVCxDQUFtQnVELE1BQW5CLEVBQTBCO0FBQUMsWUFBSUMsVUFBVSxHQUFDLEVBQWY7QUFBa0JELGNBQU0sQ0FBQ3huQixJQUFQLENBQVksWUFBVTtBQUFDLGNBQUkwbkIsUUFBUSxHQUFDNW9CLE1BQU0sQ0FBQyxJQUFELENBQU4sQ0FBYXFDLElBQWIsQ0FBa0IsTUFBbEIsRUFBMEJnZSxLQUExQixDQUFnQyxHQUFoQyxDQUFiO0FBQUEsY0FBa0R3SSxPQUFPLEdBQUMsSUFBSUMsSUFBSixDQUFTRixRQUFRLENBQUMsQ0FBRCxDQUFqQixFQUFxQkEsUUFBUSxDQUFDLENBQUQsQ0FBUixHQUFZLENBQWpDLEVBQW1DQSxRQUFRLENBQUMsQ0FBRCxDQUEzQyxDQUExRDtBQUEwR0Qsb0JBQVUsQ0FBQ3hsQixJQUFYLENBQWdCMGxCLE9BQWhCO0FBQTBCLFNBQTNKO0FBQTZKLGVBQU9GLFVBQVA7QUFBbUI7O0FBQzdOLGVBQVNJLFVBQVQsQ0FBb0JMLE1BQXBCLEVBQTJCO0FBQUMsWUFBSUMsVUFBVSxHQUFDLEVBQWY7QUFBa0JELGNBQU0sQ0FBQ3huQixJQUFQLENBQVksWUFBVTtBQUFDLGNBQUk4bkIsVUFBVSxHQUFDaHBCLE1BQU0sQ0FBQyxJQUFELENBQXJCO0FBQUEsY0FBNEI0b0IsUUFBUSxHQUFDSSxVQUFVLENBQUMzbUIsSUFBWCxDQUFnQixNQUFoQixFQUF3QmdlLEtBQXhCLENBQThCLEdBQTlCLENBQXJDOztBQUF3RSxjQUFHdUksUUFBUSxDQUFDbmMsTUFBVCxHQUFnQixDQUFuQixFQUFxQjtBQUFDLGdCQUFJd2MsT0FBTyxHQUFDTCxRQUFRLENBQUMsQ0FBRCxDQUFSLENBQVl2SSxLQUFaLENBQWtCLEdBQWxCLENBQVo7QUFBQSxnQkFBbUM2SSxRQUFRLEdBQUNOLFFBQVEsQ0FBQyxDQUFELENBQVIsQ0FBWXZJLEtBQVosQ0FBa0IsR0FBbEIsQ0FBNUM7QUFBb0UsV0FBMUYsTUFBK0YsSUFBR3VJLFFBQVEsQ0FBQyxDQUFELENBQVIsQ0FBWTdvQixPQUFaLENBQW9CLEdBQXBCLEtBQTBCLENBQTdCLEVBQStCO0FBQUMsZ0JBQUlrcEIsT0FBTyxHQUFDLENBQUMsTUFBRCxFQUFRLEdBQVIsRUFBWSxHQUFaLENBQVo7QUFBQSxnQkFBNkJDLFFBQVEsR0FBQ04sUUFBUSxDQUFDLENBQUQsQ0FBUixDQUFZdkksS0FBWixDQUFrQixHQUFsQixDQUF0QztBQUE4RCxXQUE5RixNQUFrRztBQUFDLGdCQUFJNEksT0FBTyxHQUFDTCxRQUFRLENBQUMsQ0FBRCxDQUFSLENBQVl2SSxLQUFaLENBQWtCLEdBQWxCLENBQVo7QUFBQSxnQkFBbUM2SSxRQUFRLEdBQUMsQ0FBQyxHQUFELEVBQUssR0FBTCxDQUE1QztBQUF1RDs7QUFDdFksY0FBSUwsT0FBTyxHQUFDLElBQUlDLElBQUosQ0FBU0csT0FBTyxDQUFDLENBQUQsQ0FBaEIsRUFBb0JBLE9BQU8sQ0FBQyxDQUFELENBQVAsR0FBVyxDQUEvQixFQUFpQ0EsT0FBTyxDQUFDLENBQUQsQ0FBeEMsRUFBNENDLFFBQVEsQ0FBQyxDQUFELENBQXBELEVBQXdEQSxRQUFRLENBQUMsQ0FBRCxDQUFoRSxDQUFaO0FBQWlGUCxvQkFBVSxDQUFDeGxCLElBQVgsQ0FBZ0IwbEIsT0FBaEI7QUFBMEIsU0FEN0Q7QUFDK0QsZUFBT0YsVUFBUDtBQUFtQjs7QUFDaEksZUFBU2pCLE9BQVQsQ0FBaUJsTixLQUFqQixFQUF1QjJPLE1BQXZCLEVBQThCO0FBQUMsZUFBT2hOLElBQUksQ0FBQ3lMLEtBQUwsQ0FBWXVCLE1BQU0sR0FBQzNPLEtBQW5CLENBQVA7QUFBbUM7O0FBQ2xFLGVBQVM0SyxRQUFULENBQWtCZ0UsS0FBbEIsRUFBd0I7QUFBQyxZQUFJQyxhQUFhLEdBQUMsRUFBbEI7O0FBQXFCLGFBQUl4a0IsQ0FBQyxHQUFDLENBQU4sRUFBUUEsQ0FBQyxHQUFDdWtCLEtBQUssQ0FBQzNjLE1BQWhCLEVBQXVCNUgsQ0FBQyxFQUF4QixFQUEyQjtBQUFDLGNBQUk0aUIsUUFBUSxHQUFDQyxPQUFPLENBQUMwQixLQUFLLENBQUN2a0IsQ0FBQyxHQUFDLENBQUgsQ0FBTixFQUFZdWtCLEtBQUssQ0FBQ3ZrQixDQUFELENBQWpCLENBQXBCO0FBQTBDd2tCLHVCQUFhLENBQUNsbUIsSUFBZCxDQUFtQnNrQixRQUFuQjtBQUE4Qjs7QUFDbEosZUFBT3RMLElBQUksQ0FBQ3FMLEdBQUwsQ0FBU2pYLEtBQVQsQ0FBZSxJQUFmLEVBQW9COFksYUFBcEIsQ0FBUDtBQUEyQzs7QUFDM0MsZUFBU3RELGlCQUFULENBQTJCclYsRUFBM0IsRUFBOEI7QUFBQyxZQUFJclEsR0FBRyxHQUFDcVEsRUFBRSxDQUFDNFksU0FBWDtBQUFxQixZQUFJQyxJQUFJLEdBQUM3WSxFQUFFLENBQUM4WSxVQUFaO0FBQXVCLFlBQUk3bkIsS0FBSyxHQUFDK08sRUFBRSxDQUFDK1ksV0FBYjtBQUF5QixZQUFJOW9CLE1BQU0sR0FBQytQLEVBQUUsQ0FBQ2daLFlBQWQ7O0FBQTJCLGVBQU1oWixFQUFFLENBQUNpWixZQUFULEVBQXNCO0FBQUNqWixZQUFFLEdBQUNBLEVBQUUsQ0FBQ2laLFlBQU47QUFBbUJ0cEIsYUFBRyxJQUFFcVEsRUFBRSxDQUFDNFksU0FBUjtBQUFrQkMsY0FBSSxJQUFFN1ksRUFBRSxDQUFDOFksVUFBVDtBQUFxQjs7QUFDaE4sZUFBT25wQixHQUFHLEdBQUVYLE1BQU0sQ0FBQ2txQixXQUFQLEdBQW1CbHFCLE1BQU0sQ0FBQ2tRLFdBQS9CLElBQTZDMlosSUFBSSxHQUFFN3BCLE1BQU0sQ0FBQ21xQixXQUFQLEdBQW1CbnFCLE1BQU0sQ0FBQ3FYLFVBQTdFLElBQTJGMVcsR0FBRyxHQUFDTSxNQUFMLEdBQWFqQixNQUFNLENBQUNrcUIsV0FBOUcsSUFBNEhMLElBQUksR0FBQzVuQixLQUFOLEdBQWFqQyxNQUFNLENBQUNtcUIsV0FBdEo7QUFBb0s7O0FBQ3BLLGVBQVNoRSxPQUFULEdBQWtCO0FBQUMsZUFBT25tQixNQUFNLENBQUNrbkIsZ0JBQVAsQ0FBd0I3bEIsUUFBUSxDQUFDMFEsYUFBVCxDQUF1Qix5QkFBdkIsQ0FBeEIsRUFBMEUsVUFBMUUsRUFBc0ZxVixnQkFBdEYsQ0FBdUcsU0FBdkcsRUFBa0hoakIsT0FBbEgsQ0FBMEgsSUFBMUgsRUFBK0gsRUFBL0gsRUFBbUlBLE9BQW5JLENBQTJJLElBQTNJLEVBQWdKLEVBQWhKLENBQVA7QUFBNEo7QUFBQyxLQXhCeWdDO0FBd0J2Z0NoQyxxQkFBaUIsQ0FBQ0MsS0FBbEIsQ0FBd0JDLFNBQXhCLENBQWtDLDRFQUFsQyxFQUErRyxVQUFTQyxNQUFULEVBQWdCO0FBQUNqQyxZQUFNLENBQUMsOENBQUQsQ0FBTixDQUF1RGtCLElBQXZELENBQTRELFlBQVU7QUFBQyxZQUFJNkosU0FBUyxHQUFDL0ssTUFBTSxDQUFDLElBQUQsQ0FBcEI7QUFBMkIsWUFBSWdMLFFBQVEsR0FBQ0QsU0FBUyxDQUFDdkgsSUFBVixDQUFlLGVBQWYsQ0FBYjtBQUE2QyxZQUFJeUgsV0FBVyxHQUFDLEtBQWhCOztBQUFzQixZQUFHLE9BQU9ELFFBQVAsSUFBaUIsV0FBcEIsRUFBZ0M7QUFBQ0MscUJBQVcsR0FBQztBQUFDQyxpQkFBSyxFQUFDRjtBQUFQLFdBQVo7QUFBOEI7O0FBQ3RoQixZQUFJMUcsS0FBSyxHQUFDeUcsU0FBUyxDQUFDdkgsSUFBVixDQUFlLFlBQWYsQ0FBVjs7QUFBdUMsWUFBRyxPQUFPYyxLQUFQLElBQWMsV0FBakIsRUFBNkI7QUFBQ0EsZUFBSyxHQUFDLElBQU47QUFBWTs7QUFDakYsWUFBSWltQixjQUFjLEdBQUMsSUFBSWxmLE1BQUosQ0FBVywwRUFBWCxFQUFzRjtBQUFDZ1EsbUJBQVMsRUFBQyxVQUFYO0FBQXNCMVAsY0FBSSxFQUFDLEtBQTNCO0FBQWlDckgsZUFBSyxFQUFDNUMsUUFBUSxDQUFDNEMsS0FBRCxDQUEvQztBQUF1RDBILGtCQUFRLEVBQUNmLFdBQWhFO0FBQTRFa0Qsb0JBQVUsRUFBQztBQUFDdUMsY0FBRSxFQUFDLG9CQUFKO0FBQXlCak8sZ0JBQUksRUFBQyxTQUE5QjtBQUF3QytuQix3QkFBWSxFQUFDLHNCQUFTeGdCLEtBQVQsRUFBZW1QLFNBQWYsRUFBeUI7QUFBQyxrQkFBSXNSLElBQUksR0FBQzFwQixRQUFRLENBQUN1USxnQkFBVCxDQUEwQixlQUExQixFQUEyQ3RILEtBQTNDLEVBQWtEdUssWUFBbEQsQ0FBK0QsV0FBL0QsQ0FBVDtBQUFxRixxQkFBTSxrQkFBZ0I0RSxTQUFoQixHQUEwQixJQUExQixHQUErQnNSLElBQS9CLEdBQW9DLFNBQTFDO0FBQW9EO0FBQUUsYUFBMU47QUFBMk45SixxQkFBUyxFQUFDO0FBQXJPLFdBQXZGO0FBQWtVclYsb0JBQVUsRUFBQztBQUFDQyxrQkFBTSxFQUFDLHFCQUFSO0FBQThCQyxrQkFBTSxFQUFDO0FBQXJDLFdBQTdVO0FBQTBZa2YscUJBQVcsRUFBQztBQUFDLGlCQUFJO0FBQUNyUCx1QkFBUyxFQUFDO0FBQVg7QUFBTCxXQUF0WjtBQUFzYnhhLFlBQUUsRUFBQztBQUFDc1EsZ0JBQUksRUFBQyxnQkFBVTtBQUFDcEcsdUJBQVMsQ0FBQ0csS0FBVixDQUFnQixHQUFoQixFQUFxQnlmLEtBQXJCLENBQTJCLFVBQVN0UixJQUFULEVBQWM7QUFBQ3JaLHNCQUFNLENBQUMsSUFBRCxDQUFOLENBQWFtRSxRQUFiLENBQXNCLFFBQXRCO0FBQWlDLGVBQTNFO0FBQThFO0FBQS9GO0FBQXpiLFNBQXRGLENBQW5CO0FBQXVvQixPQUZyVjtBQUV3VixLQUZ4ZDtBQUUwZHJDLHFCQUFpQixDQUFDQyxLQUFsQixDQUF3QkMsU0FBeEIsQ0FBa0MsZ0VBQWxDLEVBQW1HLFVBQVNDLE1BQVQsRUFBZ0I7QUFBQyxVQUFJOEksU0FBUyxHQUFDL0ssTUFBTSxDQUFDLDBCQUFELENBQXBCO0FBQWlELFVBQUlrTyxLQUFLLEdBQUNuRCxTQUFTLENBQUN2SCxJQUFWLENBQWUsZUFBZixDQUFWO0FBQTBDLFVBQUl3SSxRQUFRLEdBQUMsSUFBYjs7QUFBa0IsVUFBR2tDLEtBQUssSUFBRSxDQUFWLEVBQzcyQjtBQUFDQSxhQUFLLEdBQUMsS0FBTjtBQUFZbEMsZ0JBQVEsR0FBQyxLQUFUO0FBQWdCOztBQUM3QixVQUFJbUMsVUFBVSxHQUFDcEQsU0FBUyxDQUFDdkgsSUFBVixDQUFlLGlCQUFmLENBQWY7O0FBQWlELFVBQUcySyxVQUFVLElBQUUsQ0FBZixFQUNqRDtBQUFDLFlBQUlBLFVBQVUsR0FBQyxLQUFmO0FBQXNCLE9BRDBCLE1BR2pEO0FBQUMsWUFBSUEsVUFBVSxHQUFDLElBQWY7QUFBcUI7O0FBQ3RCLFVBQUk3QyxVQUFVLEdBQUNQLFNBQVMsQ0FBQ3ZILElBQVYsQ0FBZSxpQkFBZixDQUFmOztBQUFpRCxVQUFHOEgsVUFBVSxJQUFFLENBQWYsRUFDakQ7QUFBQyxZQUFJQSxVQUFVLEdBQUMsS0FBZjtBQUFzQixPQUQwQixNQUdqRDtBQUFDLFlBQUlBLFVBQVUsR0FBQyxJQUFmO0FBQXFCOztBQUN0QixVQUFJc2YsaUJBQWlCLEdBQUMxYyxLQUF0Qjs7QUFBNEIsZUFBUzJjLGVBQVQsQ0FBeUI5ZixTQUF6QixFQUFtQ2YsS0FBbkMsRUFBeUM4Z0IsSUFBekMsRUFBOEM7QUFBQyxZQUFHL2YsU0FBUyxDQUFDMUksSUFBVixDQUFlLE1BQWYsQ0FBSCxFQUEwQjtBQUFPLFlBQUk4TSxNQUFNLEdBQUNwRSxTQUFTLENBQUMvRixJQUFWLENBQWUsUUFBZixDQUFYO0FBQW9DLFlBQUkrbEIsS0FBSyxHQUFDaGdCLFNBQVMsQ0FBQy9GLElBQVYsQ0FBZSxhQUFmLENBQVY7QUFBd0MsWUFBSWdtQixXQUFXLEdBQUM3YixNQUFNLENBQUN3TSxNQUFQLENBQWMsWUFBZCxDQUFoQjtBQUE0QyxZQUFJc1AsZ0JBQWdCLEdBQUNELFdBQVcsQ0FBQ2htQixJQUFaLENBQWlCLGtCQUFqQixDQUFyQjtBQUEwRCxZQUFJK08sUUFBUSxHQUFDNUUsTUFBTSxDQUFDbEYsRUFBUCxDQUFVRCxLQUFWLENBQWI7QUFBOEIsWUFBSWtoQixhQUFhLEdBQUNuWCxRQUFRLENBQUMvTyxJQUFULENBQWMsa0JBQWQsQ0FBbEI7QUFBb0QsWUFBSW1tQixlQUFlLEdBQUNwWCxRQUFRLENBQUMvTyxJQUFULENBQWMsZ0JBQWQsQ0FBcEI7QUFBb0QsWUFBSW9tQixnQkFBZ0IsR0FBQ3JYLFFBQVEsQ0FBQy9PLElBQVQsQ0FBYyxjQUFkLENBQXJCO0FBQW1ELFlBQUcrTyxRQUFRLENBQUNzSSxFQUFULENBQVkyTyxXQUFaLENBQUgsRUFBNEI7QUFBT2pYLGdCQUFRLENBQUM1UCxRQUFULENBQWtCLFFBQWxCO0FBQTRCLFlBQUkrTCxPQUFPLEdBQUNuRixTQUFTLENBQUMxSSxJQUFWLENBQWUsU0FBZixDQUFaO0FBQXNDdUcsb0JBQVksQ0FBQ3NILE9BQUQsQ0FBWjtBQUFzQm5GLGlCQUFTLENBQUMxSSxJQUFWLENBQWUsTUFBZixFQUFzQixJQUF0QjtBQUE0QixZQUFJOFUsVUFBVSxHQUFDcE0sU0FBUyxDQUFDdkgsSUFBVixDQUFlLGlCQUFmLENBQWY7O0FBQWlELFlBQUcyVCxVQUFVLElBQUUsTUFBZixFQUFzQjtBQUFDcEQsa0JBQVEsQ0FBQzlTLEdBQVQsQ0FBYTtBQUFDa3BCLG1CQUFPLEVBQUMsT0FBVDtBQUFpQmxNLGtCQUFNLEVBQUM7QUFBeEIsV0FBYjtBQUF5Q2lOLHVCQUFhLENBQUNqcUIsR0FBZCxDQUFrQjtBQUFDa1QsbUJBQU8sRUFBQztBQUFULFdBQWxCO0FBQStCMkosa0JBQVEsQ0FBQ0MsRUFBVCxDQUFZbU4sYUFBWixFQUEwQixDQUExQixFQUE0QjtBQUFDRyxpQkFBSyxFQUFDLENBQVA7QUFBU0Msc0JBQVUsRUFBQyxzQkFBVTtBQUFDdlgsc0JBQVEsQ0FBQzVQLFFBQVQsQ0FBa0IsV0FBbEIsRUFBK0I1QyxXQUEvQixDQUEyQyxRQUEzQztBQUFxRHlwQix5QkFBVyxDQUFDenBCLFdBQVosQ0FBd0IsV0FBeEI7QUFBcUN3UyxzQkFBUSxDQUFDOVMsR0FBVCxDQUFhO0FBQUNrcEIsdUJBQU8sRUFBQyxFQUFUO0FBQVlsTSxzQkFBTSxFQUFDO0FBQW5CLGVBQWI7QUFBcUNpTiwyQkFBYSxDQUFDanFCLEdBQWQsQ0FBa0I7QUFBQ2tULHVCQUFPLEVBQUM7QUFBVCxlQUFsQjtBQUFnQ3BKLHVCQUFTLENBQUMvRixJQUFWLENBQWUsYUFBZixFQUE4QnNGLE9BQTlCLENBQXNDLE9BQXRDO0FBQStDUyx1QkFBUyxDQUFDMUksSUFBVixDQUFlLE1BQWYsRUFBc0IsS0FBdEI7O0FBQTZCLGtCQUFHeW9CLElBQUgsRUFBUTtBQUFDNWEsdUJBQU8sR0FBQ3JILFVBQVUsQ0FBQyxZQUFVO0FBQUMwaUIsK0JBQWEsQ0FBQ3hnQixTQUFELEVBQVcsS0FBWCxFQUFpQixJQUFqQixDQUFiO0FBQXFDLGlCQUFqRCxFQUFrRDZmLGlCQUFsRCxDQUFsQjtBQUF1RjdmLHlCQUFTLENBQUMxSSxJQUFWLENBQWUsU0FBZixFQUF5QjZOLE9BQXpCO0FBQW1DO0FBQUM7QUFBOVksV0FBNUI7QUFBOGEsU0FBN2dCLE1BQWloQjtBQUFDLGNBQUc2RCxRQUFRLENBQUMvSixLQUFULEtBQWlCZ2hCLFdBQVcsQ0FBQ2hoQixLQUFaLEVBQXBCLEVBQXdDO0FBQUMsZ0JBQUl3aEIsYUFBYSxHQUFDLENBQWxCO0FBQW9CLGdCQUFJQyxZQUFZLEdBQUMsTUFBakI7QUFBd0IsZ0JBQUlDLGtCQUFrQixHQUFDLENBQUMzZ0IsU0FBUyxDQUFDcEosS0FBVixFQUFELEdBQW1CLENBQTFDO0FBQTRDLGdCQUFJZ3FCLGlCQUFpQixHQUFDLE1BQXRCO0FBQTZCLGdCQUFJQyxvQkFBb0IsR0FBQyxDQUF6QjtBQUEyQixnQkFBSUMsbUJBQW1CLEdBQUMsTUFBeEI7QUFBK0IsZ0JBQUlDLG1CQUFtQixHQUFDLE1BQXhCO0FBQStCLGdCQUFJQyxvQkFBb0IsR0FBQyxDQUF6QjtBQUEyQixnQkFBSUMsb0JBQW9CLEdBQUMsQ0FBQ2poQixTQUFTLENBQUNwSixLQUFWLEVBQUQsR0FBbUIsQ0FBNUM7QUFBK0MsV0FBalUsTUFBcVU7QUFBQyxnQkFBSTZwQixhQUFhLEdBQUMsRUFBbEI7QUFBcUIsZ0JBQUlDLFlBQVksR0FBQyxDQUFqQjtBQUFtQixnQkFBSUMsa0JBQWtCLEdBQUMsTUFBdkI7QUFBOEIsZ0JBQUlDLGlCQUFpQixHQUFDLENBQUM1Z0IsU0FBUyxDQUFDcEosS0FBVixFQUFELEdBQW1CLENBQXpDO0FBQTJDLGdCQUFJaXFCLG9CQUFvQixHQUFDLEVBQXpCO0FBQTRCLGdCQUFJQyxtQkFBbUIsR0FBQyxDQUF4QjtBQUEwQixnQkFBSUMsbUJBQW1CLEdBQUMsQ0FBeEI7QUFBMEIsZ0JBQUlDLG9CQUFvQixHQUFDLE1BQXpCO0FBQWdDLGdCQUFJQyxvQkFBb0IsR0FBQ2poQixTQUFTLENBQUNwSixLQUFWLEtBQWtCLENBQTNDO0FBQThDOztBQUN0d0RvUyxrQkFBUSxDQUFDOVMsR0FBVCxDQUFhO0FBQUNrcEIsbUJBQU8sRUFBQyxPQUFUO0FBQWlCeG9CLGlCQUFLLEVBQUMsQ0FBdkI7QUFBeUJzcUIsaUJBQUssRUFBQ1QsYUFBL0I7QUFBNkNqQyxnQkFBSSxFQUFDa0MsWUFBbEQ7QUFBK0R4TixrQkFBTSxFQUFDO0FBQXRFLFdBQWI7QUFBdUZpTix1QkFBYSxDQUFDanFCLEdBQWQsQ0FBa0I7QUFBQ1UsaUJBQUssRUFBQ29KLFNBQVMsQ0FBQ3BKLEtBQVYsRUFBUDtBQUF5QnNxQixpQkFBSyxFQUFDUCxrQkFBL0I7QUFBa0RuQyxnQkFBSSxFQUFDb0M7QUFBdkQsV0FBbEI7QUFBNkZSLHlCQUFlLENBQUNscUIsR0FBaEIsQ0FBb0I7QUFBQ1UsaUJBQUssRUFBQ29KLFNBQVMsQ0FBQ3BKLEtBQVYsRUFBUDtBQUF5QjRuQixnQkFBSSxFQUFDdUMsbUJBQTlCO0FBQWtERyxpQkFBSyxFQUFDRjtBQUF4RCxXQUFwQjtBQUFtR2QsMEJBQWdCLENBQUNocUIsR0FBakIsQ0FBcUI7QUFBQ3NvQixnQkFBSSxFQUFDO0FBQU4sV0FBckI7QUFBK0J6TCxrQkFBUSxDQUFDb08sR0FBVCxDQUFhZCxnQkFBYixFQUE4QjtBQUFDbFcsYUFBQyxFQUFDLEVBQUg7QUFBTWlYLG1CQUFPLEVBQUM7QUFBZCxXQUE5QjtBQUFtRHJPLGtCQUFRLENBQUNDLEVBQVQsQ0FBWWtOLGdCQUFaLEVBQTZCLENBQTdCLEVBQStCO0FBQUMxQixnQkFBSSxFQUFDeUMsb0JBQU47QUFBMkJoTixnQkFBSSxFQUFDb04sTUFBTSxDQUFDaE47QUFBdkMsV0FBL0I7QUFBa0Z0QixrQkFBUSxDQUFDQyxFQUFULENBQVloSyxRQUFaLEVBQXFCLENBQXJCLEVBQXVCO0FBQUNwUyxpQkFBSyxFQUFDb0osU0FBUyxDQUFDcEosS0FBVixFQUFQO0FBQXlCcWQsZ0JBQUksRUFBQ29OLE1BQU0sQ0FBQ2hOO0FBQXJDLFdBQXZCO0FBQXdFdEIsa0JBQVEsQ0FBQ0MsRUFBVCxDQUFZbU4sYUFBWixFQUEwQixDQUExQixFQUE0QjtBQUFDZSxpQkFBSyxFQUFDTCxvQkFBUDtBQUE0QnJDLGdCQUFJLEVBQUNzQyxtQkFBakM7QUFBcUQ3TSxnQkFBSSxFQUFDb04sTUFBTSxDQUFDaE47QUFBakUsV0FBNUI7QUFBeUd0QixrQkFBUSxDQUFDdU8sYUFBVCxDQUF1QmpCLGdCQUF2QixFQUF3QyxHQUF4QyxFQUE0QztBQUFDQyxpQkFBSyxFQUFDLENBQVA7QUFBU25XLGFBQUMsRUFBQztBQUFYLFdBQTVDLEVBQTJEO0FBQUNtVyxpQkFBSyxFQUFDLENBQVA7QUFBU25XLGFBQUMsRUFBQyxDQUFYO0FBQWE4SixnQkFBSSxFQUFDb04sTUFBTSxDQUFDbE4sT0FBekI7QUFBaUNpTixtQkFBTyxFQUFDLElBQXpDO0FBQThDamhCLGlCQUFLLEVBQUM7QUFBcEQsV0FBM0QsRUFBb0gsR0FBcEgsRUFBd0gsWUFBVTtBQUFDNkksb0JBQVEsQ0FBQzVQLFFBQVQsQ0FBa0IsV0FBbEIsRUFBK0I1QyxXQUEvQixDQUEyQyxRQUEzQztBQUFxRHlwQix1QkFBVyxDQUFDenBCLFdBQVosQ0FBd0IsV0FBeEI7QUFBcUN3UyxvQkFBUSxDQUFDOVMsR0FBVCxDQUFhO0FBQUNrcEIscUJBQU8sRUFBQyxFQUFUO0FBQVl4b0IsbUJBQUssRUFBQyxFQUFsQjtBQUFxQjRuQixrQkFBSSxFQUFDLEVBQTFCO0FBQTZCdEwsb0JBQU0sRUFBQztBQUFwQyxhQUFiO0FBQXNEaU4seUJBQWEsQ0FBQ2pxQixHQUFkLENBQWtCO0FBQUNVLG1CQUFLLEVBQUMsRUFBUDtBQUFVc3FCLG1CQUFLLEVBQUMsRUFBaEI7QUFBbUIxQyxrQkFBSSxFQUFDO0FBQXhCLGFBQWxCO0FBQStDNEIsMkJBQWUsQ0FBQ2xxQixHQUFoQixDQUFvQjtBQUFDVSxtQkFBSyxFQUFDLEVBQVA7QUFBVTRuQixrQkFBSSxFQUFDO0FBQWYsYUFBcEI7QUFBd0M2Qiw0QkFBZ0IsQ0FBQ25xQixHQUFqQixDQUFxQjtBQUFDa1QscUJBQU8sRUFBQyxFQUFUO0FBQVlsRix1QkFBUyxFQUFDO0FBQXRCLGFBQXJCO0FBQWdEZ2MsNEJBQWdCLENBQUNocUIsR0FBakIsQ0FBcUI7QUFBQ3NvQixrQkFBSSxFQUFDO0FBQU4sYUFBckI7QUFBZ0N4ZSxxQkFBUyxDQUFDL0YsSUFBVixDQUFlLGFBQWYsRUFBOEJzRixPQUE5QixDQUFzQyxPQUF0QztBQUErQ1MscUJBQVMsQ0FBQzFJLElBQVYsQ0FBZSxNQUFmLEVBQXNCLEtBQXRCOztBQUE2QixnQkFBR3lvQixJQUFILEVBQVE7QUFBQzVhLHFCQUFPLEdBQUNySCxVQUFVLENBQUMsWUFBVTtBQUFDMGlCLDZCQUFhLENBQUN4Z0IsU0FBRCxFQUFXLEtBQVgsRUFBaUIsSUFBakIsQ0FBYjtBQUFxQyxlQUFqRCxFQUFrRDZmLGlCQUFsRCxDQUFsQjtBQUF1RjdmLHVCQUFTLENBQUMxSSxJQUFWLENBQWUsU0FBZixFQUF5QjZOLE9BQXpCO0FBQW1DO0FBQUMsV0FBMW9CO0FBQTZvQjtBQUFDOztBQUMxdkMsZUFBU3FiLGFBQVQsQ0FBdUJ4Z0IsU0FBdkIsRUFBaUN1aEIsUUFBakMsRUFBMEN4QixJQUExQyxFQUErQztBQUFDLFlBQUkzYixNQUFNLEdBQUNwRSxTQUFTLENBQUMvRixJQUFWLENBQWUsUUFBZixDQUFYO0FBQW9DLFlBQUlnbUIsV0FBVyxHQUFDN2IsTUFBTSxDQUFDd00sTUFBUCxDQUFjLFlBQWQsQ0FBaEI7QUFBNEMsWUFBSTVILFFBQVEsR0FBQyxJQUFiOztBQUFrQixZQUFHdVksUUFBSCxFQUFZO0FBQUN2WSxrQkFBUSxHQUFDaVgsV0FBVyxDQUFDMVIsSUFBWixDQUFpQixRQUFqQixDQUFUOztBQUFvQyxjQUFHdkYsUUFBUSxDQUFDdEgsTUFBVCxLQUFrQixDQUFyQixFQUF1QjtBQUFDc0gsb0JBQVEsR0FBQzVFLE1BQU0sQ0FBQ21VLElBQVAsRUFBVDtBQUF3QjtBQUFDLFNBQWxHLE1BQXNHO0FBQUN2UCxrQkFBUSxHQUFDaVgsV0FBVyxDQUFDM1IsSUFBWixDQUFpQixRQUFqQixDQUFUO0FBQW9DLGNBQUd0RixRQUFRLENBQUN0SCxNQUFULElBQWlCLENBQXBCLEVBQzdSc0gsUUFBUSxHQUFDNUUsTUFBTSxDQUFDd00sTUFBUCxDQUFjLFFBQWQsRUFBd0JuQixLQUF4QixFQUFUO0FBQTBDOztBQUMxQ3FRLHVCQUFlLENBQUM5ZixTQUFELEVBQVdnSixRQUFRLENBQUMvSixLQUFULEVBQVgsRUFBNEI4Z0IsSUFBNUIsQ0FBZjtBQUFrRDs7QUFDbEQsZUFBU3lCLHFCQUFULEdBQWdDO0FBQUMsWUFBSTlyQixTQUFTLEdBQUNULE1BQU0sQ0FBQ04sTUFBRCxDQUFOLENBQWVlLFNBQWYsRUFBZDtBQUF5QyxZQUFHQSxTQUFTLEdBQUNxSSxZQUFiLEVBQTBCO0FBQU8sWUFBSTBqQixLQUFLLEdBQUN6aEIsU0FBUyxDQUFDL0YsSUFBVixDQUFlLGtCQUFmLENBQVY7QUFBNkMsWUFBSXluQixTQUFTLEdBQUMzakIsWUFBWSxHQUFFckksU0FBUyxHQUFDLENBQXRDO0FBQXlDLFlBQUlpc0IsTUFBTSxHQUFDanNCLFNBQVMsR0FBQyxHQUFyQjtBQUF5QityQixhQUFLLENBQUN2ckIsR0FBTixDQUFVO0FBQUNnTyxtQkFBUyxFQUFDLGdCQUFjeWQsTUFBZCxHQUFxQixLQUFoQztBQUFzQy9yQixnQkFBTSxFQUFDOHJCO0FBQTdDLFNBQVY7QUFBb0U7O0FBQzlSenNCLFlBQU0sQ0FBQ2UsUUFBRCxDQUFOLENBQWlCMmhCLEtBQWpCLENBQXVCLFlBQVU7QUFBQzFpQixjQUFNLENBQUMsaUNBQUQsQ0FBTixDQUEwQ21FLFFBQTFDLENBQW1ELFdBQW5EO0FBQWdFbkUsY0FBTSxDQUFDLHlDQUFELENBQU4sQ0FBa0RhLEVBQWxELENBQXFELE9BQXJELEVBQTZELFlBQVU7QUFBQzBxQix1QkFBYSxDQUFDdnJCLE1BQU0sQ0FBQyxJQUFELENBQU4sQ0FBYWtiLE9BQWIsQ0FBcUIsMEJBQXJCLENBQUQsRUFBa0RsYixNQUFNLENBQUMsSUFBRCxDQUFOLENBQWF5SCxRQUFiLENBQXNCLE1BQXRCLENBQWxELENBQWI7QUFBK0YsU0FBdks7QUFBeUt6SCxjQUFNLENBQUMsNENBQUQsQ0FBTixDQUFxRGEsRUFBckQsQ0FBd0QsT0FBeEQsRUFBZ0UsWUFBVTtBQUFDZ3FCLHlCQUFlLENBQUM3cUIsTUFBTSxDQUFDLElBQUQsQ0FBTixDQUFha2IsT0FBYixDQUFxQiwwQkFBckIsQ0FBRCxFQUFrRGxiLE1BQU0sQ0FBQyxJQUFELENBQU4sQ0FBYWdLLEtBQWIsRUFBbEQsQ0FBZjtBQUF3RixTQUFuSztBQUFxS2hLLGNBQU0sQ0FBQyxzQ0FBRCxDQUFOLENBQStDYSxFQUEvQyxDQUFrRCxPQUFsRCxFQUEwRCxZQUFVO0FBQUMsY0FBSWtLLFNBQVMsR0FBQy9LLE1BQU0sQ0FBQyxJQUFELENBQU4sQ0FBYWtiLE9BQWIsQ0FBcUIsMEJBQXJCLENBQWQ7QUFBK0QsY0FBSTZQLEtBQUssR0FBQy9xQixNQUFNLENBQUMsSUFBRCxDQUFOLENBQWFnRixJQUFiLENBQWtCLE9BQWxCLENBQVY7QUFBcUMsY0FBSWdGLEtBQUssR0FBQ2UsU0FBUyxDQUFDL0YsSUFBVixDQUFlLG9DQUFmLEVBQXFEZ0YsS0FBckQsRUFBVjtBQUF1RStnQixlQUFLLENBQUN4cEIsV0FBTixDQUFrQixXQUFsQjtBQUErQndwQixlQUFLLENBQUM5Z0IsRUFBTixDQUFTRCxLQUFULEVBQWdCN0YsUUFBaEIsQ0FBeUIsV0FBekI7QUFBdUMsU0FBdFQ7O0FBQXdULFlBQUc2SCxRQUFILEVBQ3h1QjtBQUFDLGNBQUlrRSxPQUFPLEdBQUNySCxVQUFVLENBQUMsWUFBVTtBQUFDMGlCLHlCQUFhLENBQUN4Z0IsU0FBRCxFQUFXLEtBQVgsRUFBaUIsSUFBakIsQ0FBYjtBQUFxQyxXQUFqRCxFQUFrRDZmLGlCQUFsRCxDQUF0QjtBQUEyRjdmLG1CQUFTLENBQUMxSSxJQUFWLENBQWUsU0FBZixFQUF5QjZOLE9BQXpCO0FBQW1DO0FBQUMsT0FEaEk7QUFDbUksS0FqQnlnQjtBQWlCdmdCcE8scUJBQWlCLENBQUNDLEtBQWxCLENBQXdCQyxTQUF4QixDQUFrQyxnRUFBbEMsRUFBbUcsVUFBU0MsTUFBVCxFQUFnQjtBQUFDbVAsV0FBSyxDQUFDQyxJQUFOLENBQVd0USxRQUFRLENBQUN1USxnQkFBVCxDQUEwQiwyQkFBMUIsQ0FBWCxFQUFtRWxDLE9BQW5FLENBQTJFLFVBQUNzQixFQUFELEVBQU07QUFBQyxZQUFNaWMsSUFBSSxHQUFDdmIsS0FBSyxDQUFDQyxJQUFOLENBQVdYLEVBQUUsQ0FBQ1ksZ0JBQUgsQ0FBb0IsS0FBcEIsQ0FBWCxDQUFYO0FBQWtELFlBQUlzYixXQUFKLENBQWdCO0FBQUN0ckIsZ0JBQU0sRUFBQ29QLEVBQVI7QUFBV21jLG1CQUFTLEVBQUNuYyxFQUFFLENBQUN3TSxPQUFILENBQVcyUCxTQUFYLElBQXNCdFUsU0FBM0M7QUFBcUR1VSxpQkFBTyxFQUFDcGMsRUFBRSxDQUFDd00sT0FBSCxDQUFXNlAsT0FBWCxJQUFvQnhVLFNBQWpGO0FBQTJGeVUsa0JBQVEsRUFBQ3RjLEVBQUUsQ0FBQ3dNLE9BQUgsQ0FBVytQLFFBQVgsSUFBcUIxVSxTQUF6SDtBQUFtSXpILGdCQUFNLEVBQUNKLEVBQUUsQ0FBQ3dNLE9BQUgsQ0FBV3BNLE1BQVgsSUFBbUJ5SCxTQUE3SjtBQUF1SzBDLGVBQUssRUFBQ3ZLLEVBQUUsQ0FBQ3dNLE9BQUgsQ0FBV2pDLEtBQVgsSUFBa0IxQyxTQUEvTDtBQUF5TTJVLGdCQUFNLEVBQUNQLElBQUksQ0FBQyxDQUFELENBQUosQ0FBUXBZLFlBQVIsQ0FBcUIsS0FBckIsQ0FBaE47QUFBNE80WSxnQkFBTSxFQUFDUixJQUFJLENBQUMsQ0FBRCxDQUFKLENBQVFwWSxZQUFSLENBQXFCLEtBQXJCLENBQW5QO0FBQStRNlksMkJBQWlCLEVBQUMxYyxFQUFFLENBQUN3TSxPQUFILENBQVdtUTtBQUE1UyxTQUFoQjtBQUE0VSxPQUFoZDtBQUFtZCxLQUF2a0I7QUFBeWtCdnJCLHFCQUFpQixDQUFDQyxLQUFsQixDQUF3QkMsU0FBeEIsQ0FBa0MsZ0VBQWxDLEVBQW1HLFVBQVNDLE1BQVQsRUFBZ0I7QUFBQyxVQUFJcXJCLFlBQVksR0FBQyxZQUFVO0FBQUMsaUJBQVNDLGdCQUFULENBQTBCL2QsTUFBMUIsRUFBaUNnZSxLQUFqQyxFQUF1QztBQUFDLGVBQUksSUFBSTNvQixDQUFDLEdBQUMsQ0FBVixFQUFZQSxDQUFDLEdBQUMyb0IsS0FBSyxDQUFDL2dCLE1BQXBCLEVBQTJCNUgsQ0FBQyxFQUE1QixFQUErQjtBQUFDLGdCQUFJNG9CLFVBQVUsR0FBQ0QsS0FBSyxDQUFDM29CLENBQUQsQ0FBcEI7QUFBd0I0b0Isc0JBQVUsQ0FBQ0MsVUFBWCxHQUFzQkQsVUFBVSxDQUFDQyxVQUFYLElBQXVCLEtBQTdDO0FBQW1ERCxzQkFBVSxDQUFDRSxZQUFYLEdBQXdCLElBQXhCO0FBQTZCLGdCQUFHLFdBQVVGLFVBQWIsRUFBd0JBLFVBQVUsQ0FBQ0csUUFBWCxHQUFvQixJQUFwQjtBQUF5QkMsa0JBQU0sQ0FBQ0MsY0FBUCxDQUFzQnRlLE1BQXRCLEVBQTZCaWUsVUFBVSxDQUFDTSxHQUF4QyxFQUE0Q04sVUFBNUM7QUFBeUQ7QUFBQzs7QUFBQSxlQUFPLFVBQVNPLFdBQVQsRUFBcUJDLFVBQXJCLEVBQWdDQyxXQUFoQyxFQUE0QztBQUFDLGNBQUdELFVBQUgsRUFBY1YsZ0JBQWdCLENBQUNTLFdBQVcsQ0FBQzlMLFNBQWIsRUFBdUIrTCxVQUF2QixDQUFoQjtBQUFtRCxjQUFHQyxXQUFILEVBQWVYLGdCQUFnQixDQUFDUyxXQUFELEVBQWFFLFdBQWIsQ0FBaEI7QUFBMEMsaUJBQU9GLFdBQVA7QUFBb0IsU0FBbE07QUFBb00sT0FBMWUsRUFBakI7O0FBQThmLGVBQVNHLGVBQVQsQ0FBeUJDLFFBQXpCLEVBQWtDSixXQUFsQyxFQUE4QztBQUFDLFlBQUcsRUFBRUksUUFBUSxZQUFZSixXQUF0QixDQUFILEVBQXNDO0FBQUMsZ0JBQU0sSUFBSUssU0FBSixDQUFjLG1DQUFkLENBQU47QUFBMEQ7QUFBQzs7QUFDajlDLFVBQUlDLE9BQU8sR0FBQ3R1QixNQUFNLENBQUNOLE1BQUQsQ0FBbEI7QUFBMkIsVUFBSTZ1QixLQUFLLEdBQUN2dUIsTUFBTSxDQUFDLE1BQUQsQ0FBaEI7O0FBQXlCLFVBQUl5USxTQUFTLEdBQUMsWUFBVTtBQUFDLGlCQUFTQSxTQUFULEdBQW9CO0FBQUMsY0FBSTZSLEtBQUssR0FBQyxJQUFWOztBQUFlLGNBQUlrTSxXQUFXLEdBQUNuZSxTQUFTLENBQUM1RCxNQUFWLEdBQWlCLENBQWpCLElBQW9CNEQsU0FBUyxDQUFDLENBQUQsQ0FBVCxLQUFla0ksU0FBbkMsR0FBNkNsSSxTQUFTLENBQUMsQ0FBRCxDQUF0RCxHQUEwRCxFQUExRTs7QUFBNkU4ZCx5QkFBZSxDQUFDLElBQUQsRUFBTTFkLFNBQU4sQ0FBZjs7QUFBZ0MsY0FBSWdlLGNBQWMsR0FBQztBQUFDeFIsZUFBRyxFQUFDamQsTUFBTSxDQUFDLDBCQUFELENBQVg7QUFBd0MwdUIsc0JBQVUsRUFBQyxLQUFuRDtBQUF5REMsMEJBQWMsRUFBQyxJQUF4RTtBQUE2RTlkLG9CQUFRLEVBQUMsS0FBdEY7QUFBNEY3RSxvQkFBUSxFQUFDO0FBQXJHLFdBQW5CO0FBQThILGNBQUk0aUIsT0FBTyxHQUFDZixNQUFNLENBQUNnQixNQUFQLENBQWMsRUFBZCxFQUFpQkosY0FBakIsRUFBZ0NELFdBQWhDLENBQVo7QUFBeUQsZUFBS3ZSLEdBQUwsR0FBUzJSLE9BQU8sQ0FBQzNSLEdBQWpCO0FBQXFCLGVBQUs2UixRQUFMLEdBQWMsS0FBSzdSLEdBQUwsQ0FBU2pZLElBQVQsQ0FBY2hGLE1BQU0sQ0FBQyx1QkFBRCxDQUFwQixFQUErQ3lNLE1BQTdEO0FBQW9FLGVBQUtpaUIsVUFBTCxHQUFnQixLQUFLSSxRQUFMLEdBQWMsQ0FBZCxHQUFnQkYsT0FBTyxDQUFDRixVQUF4QixHQUFtQyxLQUFuRDtBQUF5RCxlQUFLQyxjQUFMLEdBQW9CQyxPQUFPLENBQUNELGNBQTVCO0FBQTJDLGVBQUtqYixZQUFMLEdBQWtCLENBQWxCO0FBQW9CLGVBQUtoQixXQUFMLEdBQWlCLEtBQWpCO0FBQXVCLGVBQUs2SCxpQkFBTCxHQUF1QixJQUF2QjtBQUE0QixlQUFLdUUsYUFBTCxHQUFtQjhQLE9BQU8sQ0FBQy9kLFFBQTNCO0FBQW9DLGVBQUtrZSxRQUFMO0FBQWMsZUFBS2xTLFNBQUwsR0FBZSxLQUFLSSxHQUFMLENBQVNqWSxJQUFULENBQWMsd0JBQWQsQ0FBZjtBQUF1RCxlQUFLZ0gsUUFBTCxHQUFjLEtBQUs4aUIsUUFBTCxHQUFjLENBQWQsR0FBZ0JGLE9BQU8sQ0FBQzVpQixRQUF4QixHQUFpQyxLQUEvQztBQUFxRCxlQUFLQSxRQUFMLEdBQWMsS0FBZDtBQUFvQixlQUFLaVIsR0FBTCxDQUFTcGMsRUFBVCxDQUFZLE9BQVosRUFBb0Isc0JBQXBCLEVBQTJDLFVBQVMrVCxLQUFULEVBQWU7QUFBQyxtQkFBTzBOLEtBQUssQ0FBQzFILFNBQU4sRUFBUDtBQUEwQixXQUFyRjtBQUF1RixlQUFLcUMsR0FBTCxDQUFTcGMsRUFBVCxDQUFZLE9BQVosRUFBb0Isc0JBQXBCLEVBQTJDLFVBQVMrVCxLQUFULEVBQWU7QUFBQyxtQkFBTzBOLEtBQUssQ0FBQzNILFNBQU4sRUFBUDtBQUEwQixXQUFyRjtBQUF1RixlQUFLc0MsR0FBTCxDQUFTcGMsRUFBVCxDQUFZLE9BQVosRUFBb0IscUJBQXBCLEVBQTBDLFVBQVMrVCxLQUFULEVBQWU7QUFBQyxnQkFBRyxDQUFDME4sS0FBSyxDQUFDNVAsV0FBVixFQUFzQjtBQUFDNFAsbUJBQUssQ0FBQzBNLFlBQU47O0FBQXFCMU0sbUJBQUssQ0FBQzlGLFNBQU4sQ0FBZ0I1SCxLQUFLLENBQUNwRixNQUFOLENBQWEwTixPQUFiLENBQXFCN04sS0FBckM7QUFBNkM7QUFBQyxXQUFwSjtBQUFzSixlQUFLOEIsSUFBTDtBQUFhOztBQUM1cENtYyxvQkFBWSxDQUFDN2MsU0FBRCxFQUFXLENBQUM7QUFBQ3NkLGFBQUcsRUFBQyxNQUFMO0FBQVlyVixlQUFLLEVBQUMsU0FBU3ZILElBQVQsR0FBZTtBQUFDLGlCQUFLcUwsU0FBTCxDQUFlLENBQWY7O0FBQWtCLGdCQUFHLEtBQUt4USxRQUFSLEVBQWlCO0FBQUMsbUJBQUtpakIsYUFBTDtBQUFzQjs7QUFDcEgsZ0JBQUcsS0FBS04sY0FBUixFQUF1QjtBQUFDLGtCQUFJTyxnQkFBZ0IsR0FBQyxLQUFLSixRQUExQjtBQUFtQyxrQkFBSTNnQixVQUFVLEdBQUMsaURBQWY7O0FBQWlFLG1CQUFJLElBQUl0SixDQUFDLEdBQUMsQ0FBVixFQUFZQSxDQUFDLEdBQUMsS0FBS2lxQixRQUFuQixFQUE0QmpxQixDQUFDLEVBQTdCLEVBQWdDO0FBQUMsb0JBQUk0ZCxJQUFJLEdBQUMsc0RBQW9ENWQsQ0FBQyxLQUFHLENBQUosR0FBTSxZQUFOLEdBQW1CLEVBQXZFLElBQTJFLGVBQTNFLElBQTRGQSxDQUFDLEdBQUMsQ0FBOUYsSUFBaUcsR0FBakcsSUFBc0dBLENBQUMsR0FBQyxDQUF4RyxJQUEyRyxTQUFwSDtBQUE4SHNKLDBCQUFVLEdBQUNBLFVBQVUsR0FBQ3NVLElBQXRCO0FBQTRCOztBQUN2VHRVLHdCQUFVLEdBQUNBLFVBQVUsR0FBQyxjQUF0QjtBQUFxQyxtQkFBSzhPLEdBQUwsQ0FBUzdZLE1BQVQsQ0FBZ0IrSixVQUFoQjtBQUE2QjtBQUFDO0FBRjNDLFNBQUQsRUFFOEM7QUFBQzRmLGFBQUcsRUFBQyxjQUFMO0FBQW9CclYsZUFBSyxFQUFDLFNBQVNzVyxZQUFULEdBQXVCO0FBQUMsZ0JBQUlHLE1BQU0sR0FBQyxJQUFYOztBQUFnQixpQkFBS3pjLFdBQUwsR0FBaUIsSUFBakI7QUFBc0IsaUJBQUttSyxTQUFMLENBQWV1UyxJQUFmLENBQW9CLFVBQXBCLEVBQStCLElBQS9CO0FBQXFDQyx5QkFBYSxDQUFDLEtBQUtOLFFBQU4sQ0FBYjtBQUE2QmxtQixzQkFBVSxDQUFDLFlBQVU7QUFBQ3NtQixvQkFBTSxDQUFDemMsV0FBUCxHQUFtQixLQUFuQjs7QUFBeUJ5YyxvQkFBTSxDQUFDdFMsU0FBUCxDQUFpQnVTLElBQWpCLENBQXNCLFVBQXRCLEVBQWlDLEtBQWpDOztBQUF3QyxrQkFBR0QsTUFBTSxDQUFDbmpCLFFBQVYsRUFBbUI7QUFBQ21qQixzQkFBTSxDQUFDRixhQUFQO0FBQXdCO0FBQUMsYUFBMUgsRUFBMkgsS0FBSzFVLGlCQUFoSSxDQUFWO0FBQThKO0FBQXhULFNBRjlDLEVBRXdXO0FBQUN3VCxhQUFHLEVBQUMsV0FBTDtBQUFpQnJWLGVBQUssRUFBQyxTQUFTOEQsU0FBVCxDQUFtQnhTLEtBQW5CLEVBQXlCO0FBQUMsaUJBQUswSixZQUFMLEdBQWtCaFMsUUFBUSxDQUFDc0ksS0FBRCxDQUExQjs7QUFBa0MsZ0JBQUcsS0FBSzBKLFlBQUwsR0FBa0IsS0FBS29iLFFBQTFCLEVBQW1DO0FBQUMsbUJBQUtwYixZQUFMLEdBQWtCLENBQWxCO0FBQXFCOztBQUMzZ0IsZ0JBQUcsS0FBS0EsWUFBTCxLQUFvQixDQUF2QixFQUF5QjtBQUFDLG1CQUFLQSxZQUFMLEdBQWtCLEtBQUtvYixRQUF2QjtBQUFpQzs7QUFDM0QsZ0JBQUlRLFVBQVUsR0FBQyxLQUFLclMsR0FBTCxDQUFTalksSUFBVCxDQUFjLHVDQUFxQyxLQUFLME8sWUFBMUMsR0FBdUQsSUFBckUsQ0FBZjtBQUEwRixnQkFBSTZiLE9BQU8sR0FBQyxLQUFLN2IsWUFBTCxLQUFvQixDQUFwQixHQUFzQixLQUFLdUosR0FBTCxDQUFTalksSUFBVCxDQUFjLHVCQUFkLEVBQXVDc2UsSUFBdkMsRUFBdEIsR0FBb0VnTSxVQUFVLENBQUNoVyxJQUFYLENBQWdCLHVCQUFoQixDQUFoRjtBQUF5SCxnQkFBSWtXLE9BQU8sR0FBQyxLQUFLOWIsWUFBTCxLQUFvQixLQUFLb2IsUUFBekIsR0FBa0MsS0FBSzdSLEdBQUwsQ0FBU2pZLElBQVQsQ0FBYyx1QkFBZCxFQUF1Q3dWLEtBQXZDLEVBQWxDLEdBQWlGOFUsVUFBVSxDQUFDalcsSUFBWCxDQUFnQix1QkFBaEIsQ0FBN0Y7QUFBc0ksaUJBQUs0RCxHQUFMLENBQVNqWSxJQUFULENBQWMsdUJBQWQsRUFBdUN6RCxXQUF2QyxDQUFtRCw0QkFBbkQ7QUFBaUYsaUJBQUswYixHQUFMLENBQVNqWSxJQUFULENBQWMscUJBQWQsRUFBcUN6RCxXQUFyQyxDQUFpRCxZQUFqRDs7QUFBK0QsZ0JBQUcsS0FBS3V0QixRQUFMLEdBQWMsQ0FBakIsRUFBbUI7QUFBQ1MscUJBQU8sQ0FBQ3ByQixRQUFSLENBQWlCLFNBQWpCO0FBQTRCcXJCLHFCQUFPLENBQUNyckIsUUFBUixDQUFpQixTQUFqQjtBQUE2Qjs7QUFDdGpCbXJCLHNCQUFVLENBQUNuckIsUUFBWCxDQUFvQixZQUFwQjtBQUFrQyxpQkFBSzhZLEdBQUwsQ0FBU2pZLElBQVQsQ0FBYyxxQ0FBbUMsS0FBSzBPLFlBQXhDLEdBQXFELElBQW5FLEVBQXlFdlAsUUFBekUsQ0FBa0YsWUFBbEY7QUFBaUc7QUFINFAsU0FGeFcsRUFLOEc7QUFBQzRwQixhQUFHLEVBQUMsV0FBTDtBQUFpQnJWLGVBQUssRUFBQyxTQUFTa0MsU0FBVCxHQUFvQjtBQUFDLGlCQUFLb1UsWUFBTDtBQUFvQixpQkFBS3hTLFNBQUwsQ0FBZSxLQUFLOUksWUFBTCxHQUFrQixDQUFqQztBQUFxQztBQUFyRyxTQUw5RyxFQUtxTjtBQUFDcWEsYUFBRyxFQUFDLFdBQUw7QUFBaUJyVixlQUFLLEVBQUMsU0FBU2lDLFNBQVQsR0FBb0I7QUFBQyxpQkFBS3FVLFlBQUw7QUFBb0IsaUJBQUt4UyxTQUFMLENBQWUsS0FBSzlJLFlBQUwsR0FBa0IsQ0FBakM7QUFBcUM7QUFBckcsU0FMck4sRUFLNFQ7QUFBQ3FhLGFBQUcsRUFBQyxlQUFMO0FBQXFCclYsZUFBSyxFQUFDLFNBQVN1VyxhQUFULEdBQXdCO0FBQUMsZ0JBQUlRLE1BQU0sR0FBQyxJQUFYOztBQUFnQixpQkFBS1YsUUFBTCxHQUFjVyxXQUFXLENBQUMsWUFBVTtBQUFDLGtCQUFHLENBQUNELE1BQU0sQ0FBQy9jLFdBQVgsRUFBdUI7QUFBQytjLHNCQUFNLENBQUM3VSxTQUFQO0FBQW9CO0FBQUMsYUFBekQsRUFBMEQsS0FBS2tFLGFBQS9ELENBQXpCO0FBQXdHO0FBQTVLLFNBTDVULEVBSzBlO0FBQUNpUCxhQUFHLEVBQUMsU0FBTDtBQUFlclYsZUFBSyxFQUFDLFNBQVNpWCxPQUFULEdBQWtCO0FBQUMsaUJBQUsxUyxHQUFMLENBQVMyUyxHQUFUO0FBQWdCO0FBQXhELFNBTDFlLENBQVgsQ0FBWjs7QUFLNmpCLGVBQU9uZixTQUFQO0FBQWtCLE9BTjdnQixFQUFkOztBQU04aEIsT0FBQyxZQUFVO0FBQUMsWUFBSW9mLE1BQU0sR0FBQyxLQUFYO0FBQWlCLFlBQUlDLE9BQU8sR0FBQyxJQUFaOztBQUFpQixpQkFBU0MsSUFBVCxHQUFlO0FBQUMsY0FBSW5CLE9BQU8sR0FBQztBQUFDRCwwQkFBYyxFQUFDO0FBQWhCLFdBQVo7QUFBa0MsY0FBSXFCLFNBQVMsR0FBQyxJQUFJdmYsU0FBSixDQUFjbWUsT0FBZCxDQUFkO0FBQXNDOztBQUN4dEIsaUJBQVNxQixZQUFULEdBQXVCO0FBQUMxQixlQUFLLENBQUNwcUIsUUFBTixDQUFlLFdBQWY7QUFBNEIwRSxvQkFBVSxDQUFDLFlBQVU7QUFBQzBsQixpQkFBSyxDQUFDcHFCLFFBQU4sQ0FBZSxhQUFmO0FBQStCLFdBQTNDLEVBQTRDLEdBQTVDLENBQVY7QUFBNEQ7O0FBQ2hIbXFCLGVBQU8sQ0FBQ3p0QixFQUFSLENBQVcsTUFBWCxFQUFrQixZQUFVO0FBQUMsY0FBRyxDQUFDZ3ZCLE1BQUosRUFBVztBQUFDQSxrQkFBTSxHQUFDLElBQVA7QUFBWUUsZ0JBQUk7QUFBSTtBQUFDLFNBQTlEO0FBQWdFbG5CLGtCQUFVLENBQUMsWUFBVTtBQUFDLGNBQUcsQ0FBQ2duQixNQUFKLEVBQVc7QUFBQ0Esa0JBQU0sR0FBQyxJQUFQO0FBQVlFLGdCQUFJO0FBQUk7QUFBQyxTQUE3QyxFQUE4Q0QsT0FBOUMsQ0FBVjtBQUFpRUcsb0JBQVk7QUFBSSxPQUZpYztBQUU1YixLQVR3akI7QUFTdGpCbnVCLHFCQUFpQixDQUFDQyxLQUFsQixDQUF3QkMsU0FBeEIsQ0FBa0MsOERBQWxDLEVBQWlHLFVBQVNDLE1BQVQsRUFBZ0I7QUFBQyxlQUFTa1AsSUFBVCxDQUFjc1IsSUFBZCxFQUFtQjtBQUFDLFlBQUlWLEtBQUssR0FBQ1UsSUFBSSxDQUFDblIsZ0JBQUwsQ0FBc0IsSUFBdEIsQ0FBVjtBQUFBLFlBQXNDTSxPQUFPLEdBQUMsQ0FBOUM7QUFBQSxZQUFnRHNlLFVBQVUsR0FBQyxJQUEzRDtBQUFnRSxZQUFJMWUsR0FBRyxHQUFDelEsUUFBUSxDQUFDOGlCLGFBQVQsQ0FBdUIsS0FBdkIsQ0FBUjtBQUFzQ3JTLFdBQUcsQ0FBQzJILFNBQUosR0FBYyxZQUFkO0FBQTJCLFlBQUlnWCxPQUFPLEdBQUNwdkIsUUFBUSxDQUFDOGlCLGFBQVQsQ0FBdUIsUUFBdkIsQ0FBWjtBQUE2Q3NNLGVBQU8sQ0FBQ2hYLFNBQVIsR0FBa0IsTUFBbEI7QUFBeUJnWCxlQUFPLENBQUM1ZCxZQUFSLENBQXFCLFlBQXJCLEVBQWtDLE1BQWxDO0FBQTBDLFlBQUk2ZCxPQUFPLEdBQUNydkIsUUFBUSxDQUFDOGlCLGFBQVQsQ0FBdUIsUUFBdkIsQ0FBWjtBQUE2Q3VNLGVBQU8sQ0FBQ2pYLFNBQVIsR0FBa0IsTUFBbEI7QUFBeUJpWCxlQUFPLENBQUM3ZCxZQUFSLENBQXFCLFlBQXJCLEVBQWtDLE1BQWxDO0FBQTBDLFlBQUk4ZCxPQUFPLEdBQUN0dkIsUUFBUSxDQUFDOGlCLGFBQVQsQ0FBdUIsS0FBdkIsQ0FBWjtBQUEwQ3dNLGVBQU8sQ0FBQ2xYLFNBQVIsR0FBa0IsU0FBbEI7QUFBNEJrWCxlQUFPLENBQUM3ZCxTQUFSLEdBQWtCLHlCQUF1QnVQLEtBQUssQ0FBQ3RWLE1BQTdCLEdBQW9DLFNBQXREOztBQUFnRSxZQUFHc1YsS0FBSyxDQUFDdFYsTUFBTixHQUFhLENBQWhCLEVBQWtCO0FBQUMrRSxhQUFHLENBQUM4ZSxXQUFKLENBQWdCSCxPQUFoQjtBQUF5QjNlLGFBQUcsQ0FBQzhlLFdBQUosQ0FBZ0JELE9BQWhCO0FBQXlCN2UsYUFBRyxDQUFDOGUsV0FBSixDQUFnQkYsT0FBaEI7QUFBeUIzTixjQUFJLENBQUM2TixXQUFMLENBQWlCOWUsR0FBakI7QUFBdUI7O0FBQzEzQnVRLGFBQUssQ0FBQ25RLE9BQUQsQ0FBTCxDQUFldUgsU0FBZixHQUF5QixTQUF6QjtBQUFtQyxZQUFHNEksS0FBSyxDQUFDdFYsTUFBTixHQUFhLENBQWhCLEVBQWtCc1YsS0FBSyxDQUFDQSxLQUFLLENBQUN0VixNQUFOLEdBQWEsQ0FBZCxDQUFMLENBQXNCME0sU0FBdEIsR0FBZ0MsWUFBaEM7O0FBQTZDLFlBQUl2RyxRQUFRLEdBQUMsU0FBVEEsUUFBUyxDQUFTSyxHQUFULEVBQWE7QUFBQzhPLGVBQUssQ0FBQ25RLE9BQUQsQ0FBTCxDQUFldUgsU0FBZixHQUF5QixFQUF6Qjs7QUFBNEIsY0FBR2xHLEdBQUcsS0FBRyxPQUFULEVBQWlCO0FBQUNyQixtQkFBTyxHQUFDQSxPQUFPLEdBQUNtUSxLQUFLLENBQUN0VixNQUFOLEdBQWEsQ0FBckIsR0FBdUJtRixPQUFPLEdBQUMsQ0FBL0IsR0FBaUMsQ0FBekM7QUFBNEMsV0FBOUQsTUFBa0U7QUFBQ0EsbUJBQU8sR0FBQ0EsT0FBTyxHQUFDLENBQVIsR0FBVUEsT0FBTyxHQUFDLENBQWxCLEdBQW9CbVEsS0FBSyxDQUFDdFYsTUFBTixHQUFhLENBQXpDO0FBQTRDOztBQUN4USxjQUFJOGpCLFdBQVcsR0FBQzNlLE9BQU8sR0FBQ21RLEtBQUssQ0FBQ3RWLE1BQU4sR0FBYSxDQUFyQixHQUF1Qm1GLE9BQU8sR0FBQyxDQUEvQixHQUFpQyxDQUFqRDtBQUFBLGNBQW1ENGUsV0FBVyxHQUFDNWUsT0FBTyxHQUFDLENBQVIsR0FBVUEsT0FBTyxHQUFDLENBQWxCLEdBQW9CbVEsS0FBSyxDQUFDdFYsTUFBTixHQUFhLENBQWhHO0FBQWtHc1YsZUFBSyxDQUFDblEsT0FBRCxDQUFMLENBQWV1SCxTQUFmLEdBQXlCLFNBQXpCO0FBQW1DNEksZUFBSyxDQUFDeU8sV0FBRCxDQUFMLENBQW1CclgsU0FBbkIsR0FBNkIsWUFBN0I7QUFBMEM0SSxlQUFLLENBQUN3TyxXQUFELENBQUwsQ0FBbUJwWCxTQUFuQixHQUE2QixFQUE3QjtBQUFnQ2tYLGlCQUFPLENBQUNJLFVBQVIsQ0FBbUJDLFdBQW5CLEdBQStCOWUsT0FBTyxHQUFDLENBQXZDO0FBQTBDLFNBRHZKOztBQUVsRzZRLFlBQUksQ0FBQ3BWLGdCQUFMLENBQXNCLFlBQXRCLEVBQW1DLFlBQVU7QUFBQzZpQixvQkFBVSxHQUFDLEtBQVg7QUFBa0IsU0FBaEU7QUFBa0V6TixZQUFJLENBQUNwVixnQkFBTCxDQUFzQixZQUF0QixFQUFtQyxZQUFVO0FBQUM2aUIsb0JBQVUsR0FBQyxJQUFYO0FBQWlCLFNBQS9EO0FBQWlFQyxlQUFPLENBQUM5aUIsZ0JBQVIsQ0FBeUIsT0FBekIsRUFBaUMsWUFBVTtBQUFDdUYsa0JBQVEsQ0FBQyxNQUFELENBQVI7QUFBa0IsU0FBOUQ7QUFBZ0V3ZCxlQUFPLENBQUMvaUIsZ0JBQVIsQ0FBeUIsT0FBekIsRUFBaUMsWUFBVTtBQUFDdUYsa0JBQVEsQ0FBQyxPQUFELENBQVI7QUFBbUIsU0FBL0Q7QUFBaUU3UixnQkFBUSxDQUFDc00sZ0JBQVQsQ0FBMEIsU0FBMUIsRUFBb0MsVUFBU3lGLEVBQVQsRUFBWTtBQUFDLGNBQUlDLE9BQU8sR0FBQ0QsRUFBRSxDQUFDQyxPQUFILElBQVlELEVBQUUsQ0FBQ0UsS0FBM0I7O0FBQWlDLGtCQUFPRCxPQUFQO0FBQWdCLGlCQUFLLEVBQUw7QUFBUUgsc0JBQVEsQ0FBQyxNQUFELENBQVI7QUFBaUI7O0FBQU0saUJBQUssRUFBTDtBQUFRQSxzQkFBUSxDQUFDLE9BQUQsQ0FBUjtBQUFrQjtBQUF6RTtBQUFpRixTQUFuSztBQUFxSzZQLFlBQUksQ0FBQ3BWLGdCQUFMLENBQXNCLFlBQXRCLEVBQW1Dc2pCLGdCQUFuQyxFQUFvRCxLQUFwRDtBQUEyRGxPLFlBQUksQ0FBQ3BWLGdCQUFMLENBQXNCLFdBQXRCLEVBQWtDdWpCLGVBQWxDLEVBQWtELEtBQWxEO0FBQXlELFlBQUlDLEtBQUssR0FBQyxJQUFWO0FBQWUsWUFBSUMsS0FBSyxHQUFDLElBQVY7O0FBQWUsaUJBQVNILGdCQUFULENBQTBCaGtCLEdBQTFCLEVBQThCO0FBQUNra0IsZUFBSyxHQUFDbGtCLEdBQUcsQ0FBQ2tCLE9BQUosQ0FBWSxDQUFaLEVBQWVrSCxPQUFyQjtBQUE2QitiLGVBQUssR0FBQ25rQixHQUFHLENBQUNrQixPQUFKLENBQVksQ0FBWixFQUFlQyxPQUFyQjtBQUE4Qjs7QUFBQTs7QUFBQyxpQkFBUzhpQixlQUFULENBQXlCamtCLEdBQXpCLEVBQTZCO0FBQUMsY0FBRyxDQUFDa2tCLEtBQUQsSUFBUSxDQUFDQyxLQUFaLEVBQWtCO0FBQUM7QUFBUTs7QUFDL3NCLGNBQUlDLEdBQUcsR0FBQ3BrQixHQUFHLENBQUNrQixPQUFKLENBQVksQ0FBWixFQUFla0gsT0FBdkI7QUFBK0IsY0FBSWljLEdBQUcsR0FBQ3JrQixHQUFHLENBQUNrQixPQUFKLENBQVksQ0FBWixFQUFlQyxPQUF2QjtBQUErQixjQUFJbWpCLEtBQUssR0FBQ0osS0FBSyxHQUFDRSxHQUFoQjtBQUFvQixjQUFJRyxLQUFLLEdBQUNKLEtBQUssR0FBQ0UsR0FBaEI7O0FBQW9CLGNBQUc3VSxJQUFJLENBQUNDLEdBQUwsQ0FBUzZVLEtBQVQsSUFBZ0I5VSxJQUFJLENBQUNDLEdBQUwsQ0FBUzhVLEtBQVQsQ0FBbkIsRUFBbUM7QUFBQyxnQkFBR0QsS0FBSyxHQUFDLENBQVQsRUFBVztBQUFDcmUsc0JBQVEsQ0FBQyxPQUFELENBQVI7QUFBbUIsYUFBL0IsTUFBbUM7QUFBQ0Esc0JBQVEsQ0FBQyxNQUFELENBQVI7QUFBa0I7QUFBQzs7QUFDak1pZSxlQUFLLEdBQUMsSUFBTjtBQUFXQyxlQUFLLEdBQUMsSUFBTjtBQUFZOztBQUFBO0FBQUU7O0FBQ3pCLFNBQUd4YixLQUFILENBQVNDLElBQVQsQ0FBY3hVLFFBQVEsQ0FBQ3VRLGdCQUFULENBQTBCLHdCQUExQixDQUFkLEVBQW1FbEMsT0FBbkUsQ0FBMkUsVUFBU3FULElBQVQsRUFBYztBQUFDdFIsWUFBSSxDQUFDc1IsSUFBRCxDQUFKO0FBQVksT0FBdEc7QUFBeUcsS0FOK0M7QUFNN0MzZ0IscUJBQWlCLENBQUNDLEtBQWxCLENBQXdCQyxTQUF4QixDQUFrQyxxRUFBbEMsRUFBd0csVUFBU0MsTUFBVCxFQUFnQjtBQUFBLFVBQU9rdkIsS0FBUDtBQUFhLHVCQUFZemdCLEVBQVosRUFBZTtBQUFBOztBQUFDLGVBQUtDLEdBQUwsR0FBUztBQUFDRCxjQUFFLEVBQUNBO0FBQUosV0FBVDtBQUFpQixlQUFLQyxHQUFMLENBQVN5Z0IsT0FBVCxHQUFpQixLQUFLemdCLEdBQUwsQ0FBU0QsRUFBVCxDQUFZZSxhQUFaLENBQTBCLGlCQUExQixDQUFqQjtBQUE4RCxlQUFLZCxHQUFMLENBQVMwZ0IsUUFBVCxHQUFrQixLQUFLMWdCLEdBQUwsQ0FBU3lnQixPQUFULENBQWlCM2YsYUFBakIsQ0FBK0IsbUJBQS9CLENBQWxCO0FBQXNFLGVBQUtkLEdBQUwsQ0FBUzBILEtBQVQsR0FBZSxLQUFLMUgsR0FBTCxDQUFTRCxFQUFULENBQVllLGFBQVosQ0FBMEIsY0FBMUIsQ0FBZjtBQUF5RCxlQUFLZCxHQUFMLENBQVM4TCxNQUFULEdBQWdCLEtBQUs5TCxHQUFMLENBQVNELEVBQVQsQ0FBWWUsYUFBWixDQUEwQixlQUExQixDQUFoQjtBQUEyRCxlQUFLZCxHQUFMLENBQVMyZ0IsT0FBVCxHQUFpQjtBQUFDRixtQkFBTyxFQUFDLEtBQUt6Z0IsR0FBTCxDQUFTRCxFQUFULENBQVllLGFBQVosQ0FBMEIsbUJBQTFCLENBQVQ7QUFBd0Q0ZixvQkFBUSxFQUFDLEtBQUsxZ0IsR0FBTCxDQUFTRCxFQUFULENBQVllLGFBQVosQ0FBMEIseUNBQTFCLENBQWpFO0FBQXNJNEcsaUJBQUssRUFBQyxLQUFLMUgsR0FBTCxDQUFTRCxFQUFULENBQVllLGFBQVosQ0FBMEIsZ0JBQTFCLENBQTVJO0FBQXdMK0QsbUJBQU8sRUFBQyxLQUFLN0UsR0FBTCxDQUFTRCxFQUFULENBQVllLGFBQVosQ0FBMEIsa0JBQTFCO0FBQWhNLFdBQWpCO0FBQWdRLGVBQUtsUCxNQUFMLEdBQVk7QUFBQ3FPLHFCQUFTLEVBQUM7QUFBQ0Msc0JBQVEsRUFBQyxHQUFWO0FBQWNtTyxrQkFBSSxFQUFDdVMsSUFBSSxDQUFDclM7QUFBeEI7QUFBWCxXQUFaO0FBQTBEOztBQUFobUI7QUFBQTtBQUFBLHVDQUN6TTtBQUFBLGdCQUFmc1MsU0FBZSx1RUFBTCxJQUFLO0FBQUMsaUJBQUs3Z0IsR0FBTCxDQUFTRCxFQUFULENBQVltRCxTQUFaLENBQXNCMmQsU0FBUyxHQUFDLEtBQUQsR0FBTyxRQUF0QyxFQUFnRCxlQUFoRDtBQUFrRTtBQURzSTtBQUFBO0FBQUEsK0JBRTlOblcsU0FGOE4sRUFFcE47QUFBQyxtQkFBTyxLQUFLb1csTUFBTCxDQUFZLE1BQVosRUFBbUJwVyxTQUFuQixDQUFQO0FBQXNDO0FBRjZLO0FBQUE7QUFBQSwrQkFHOU5BLFNBSDhOLEVBR3BOO0FBQUMsbUJBQU8sS0FBS29XLE1BQUwsQ0FBWSxNQUFaLEVBQW1CcFcsU0FBbkIsQ0FBUDtBQUFzQztBQUg2SztBQUFBO0FBQUEsaUNBSTVOcVcsTUFKNE4sRUFJck5yVyxTQUpxTixFQUkzTTtBQUFBOztBQUFDLG1CQUFPLElBQUk5SCxPQUFKLENBQVksVUFBQ0MsT0FBRCxFQUFTQyxNQUFULEVBQWtCO0FBQUMsa0JBQUlrZSxZQUFZLEdBQUM7QUFBQ3ptQixxQkFBSyxFQUFDd21CLE1BQU0sS0FBRyxNQUFULEdBQWdCLENBQWhCLEdBQWtCLE1BQUksQ0FBQ252QixNQUFMLENBQVlxTyxTQUFaLENBQXNCQyxRQUF0QixHQUErQixDQUF4RDtBQUEwRG1PLG9CQUFJLEVBQUMsTUFBSSxDQUFDemMsTUFBTCxDQUFZcU8sU0FBWixDQUFzQm9PLElBQXJGO0FBQTBGc00sMEJBQVUsRUFBQzlYO0FBQXJHLGVBQWpCO0FBQStILGtCQUFNb2UsVUFBVSxHQUFDO0FBQUMxbUIscUJBQUssRUFBQ3dtQixNQUFNLEtBQUcsTUFBVCxHQUFnQixDQUFoQixHQUFrQixNQUFJLENBQUNudkIsTUFBTCxDQUFZcU8sU0FBWixDQUFzQkMsUUFBdEIsR0FBK0IsQ0FBeEQ7QUFBMERtTyxvQkFBSSxFQUFDLE1BQUksQ0FBQ3pjLE1BQUwsQ0FBWXFPLFNBQVosQ0FBc0JvTyxJQUFyRjtBQUEwRjdLLHVCQUFPLEVBQUN1ZCxNQUFNLEtBQUcsTUFBVCxHQUFnQixDQUFoQixHQUFrQjtBQUFwSCxlQUFqQjtBQUF3SSxrQkFBSUcsT0FBTyxHQUFDaEUsTUFBTSxDQUFDZ0IsTUFBUCxDQUFjLEVBQWQsRUFBaUIrQyxVQUFqQixDQUFaO0FBQXlDLGtCQUFJRSxVQUFVLEdBQUNqRSxNQUFNLENBQUNnQixNQUFQLENBQWMsRUFBZCxFQUFpQitDLFVBQWpCLENBQWY7QUFBNEMsa0JBQUlHLFNBQVMsR0FBQ2xFLE1BQU0sQ0FBQ2dCLE1BQVAsQ0FBYyxFQUFkLEVBQWlCK0MsVUFBakIsQ0FBZDs7QUFBMkMsa0JBQUd2VyxTQUFTLEtBQUcsTUFBWixJQUFvQkEsU0FBUyxLQUFHLE9BQW5DLEVBQTJDO0FBQUNzVyw0QkFBWSxDQUFDSyxPQUFiLEdBQXFCTixNQUFNLEtBQUcsTUFBVCxHQUFnQjtBQUFDbmlCLG1CQUFDLEVBQUM4TCxTQUFTLEtBQUcsTUFBWixHQUFtQixPQUFuQixHQUEyQixNQUE5QjtBQUFxQ25HLG1CQUFDLEVBQUM7QUFBdkMsaUJBQWhCLEdBQTZEO0FBQUMzRixtQkFBQyxFQUFDLElBQUg7QUFBUTJGLG1CQUFDLEVBQUM7QUFBVixpQkFBbEY7QUFBa0d5Yyw0QkFBWSxDQUFDcGlCLENBQWIsR0FBZW1pQixNQUFNLEtBQUcsTUFBVCxHQUFnQixJQUFoQixHQUFxQnJXLFNBQVMsS0FBRyxNQUFaLEdBQW1CLE1BQW5CLEdBQTBCLE9BQTlEO0FBQXNFd1csdUJBQU8sQ0FBQ0csT0FBUixHQUFnQk4sTUFBTSxLQUFHLE1BQVQsR0FBZ0I7QUFBQ3ZkLHlCQUFPLEVBQUMsQ0FBVDtBQUFXNUUsbUJBQUMsRUFBQzhMLFNBQVMsS0FBRyxNQUFaLEdBQW1CLE1BQW5CLEdBQTBCO0FBQXZDLGlCQUFoQixHQUE4RCxFQUE5RTtBQUFpRndXLHVCQUFPLENBQUN0aUIsQ0FBUixHQUFVbWlCLE1BQU0sS0FBRyxNQUFULEdBQWdCclcsU0FBUyxLQUFHLE1BQVosR0FBbUIsS0FBbkIsR0FBeUIsTUFBekMsR0FBZ0QsSUFBMUQ7QUFBK0QwVyx5QkFBUyxDQUFDQyxPQUFWLEdBQWtCTixNQUFNLEtBQUcsTUFBVCxHQUFnQjtBQUFDdmQseUJBQU8sRUFBQyxDQUFUO0FBQVcwRix1QkFBSyxFQUFDLEdBQWpCO0FBQXFCdEssbUJBQUMsRUFBQzhMLFNBQVMsS0FBRyxNQUFaLEdBQW1CLE9BQW5CLEdBQTJCO0FBQWxELGlCQUFoQixHQUEwRSxFQUE1RjtBQUErRjBXLHlCQUFTLENBQUN4aUIsQ0FBVixHQUFZbWlCLE1BQU0sS0FBRyxNQUFULEdBQWdCclcsU0FBUyxLQUFHLE1BQVosR0FBbUIsTUFBbkIsR0FBMEIsT0FBMUMsR0FBa0QsSUFBOUQ7QUFBbUUwVyx5QkFBUyxDQUFDbFksS0FBVixHQUFnQjZYLE1BQU0sS0FBRyxNQUFULEdBQWdCLEdBQWhCLEdBQW9CLENBQXBDO0FBQXNDSSwwQkFBVSxDQUFDRSxPQUFYLEdBQW1CTixNQUFNLEtBQUcsTUFBVCxHQUFnQjtBQUFDdmQseUJBQU8sRUFBQyxDQUFUO0FBQVc1RSxtQkFBQyxFQUFDOEwsU0FBUyxLQUFHLE1BQVosR0FBbUIsTUFBbkIsR0FBMEI7QUFBdkMsaUJBQWhCLEdBQThELEVBQWpGO0FBQW9GeVcsMEJBQVUsQ0FBQ3ZpQixDQUFYLEdBQWFtaUIsTUFBTSxLQUFHLE1BQVQsR0FBZ0JyVyxTQUFTLEtBQUcsTUFBWixHQUFtQixLQUFuQixHQUF5QixNQUF6QyxHQUFnRCxJQUE3RDtBQUFtRSxlQUFuc0IsTUFDbGM7QUFBQ3NXLDRCQUFZLENBQUNLLE9BQWIsR0FBcUJOLE1BQU0sS0FBRyxNQUFULEdBQWdCO0FBQUNuaUIsbUJBQUMsRUFBQyxJQUFIO0FBQVEyRixtQkFBQyxFQUFDbUcsU0FBUyxLQUFHLE1BQVosR0FBbUIsT0FBbkIsR0FBMkI7QUFBckMsaUJBQWhCLEdBQTZEO0FBQUM5TCxtQkFBQyxFQUFDLElBQUg7QUFBUTJGLG1CQUFDLEVBQUM7QUFBVixpQkFBbEY7QUFBa0d5Yyw0QkFBWSxDQUFDemMsQ0FBYixHQUFld2MsTUFBTSxLQUFHLE1BQVQsR0FBZ0IsSUFBaEIsR0FBcUJyVyxTQUFTLEtBQUcsTUFBWixHQUFtQixNQUFuQixHQUEwQixPQUE5RDtBQUFzRXdXLHVCQUFPLENBQUNHLE9BQVIsR0FBZ0JOLE1BQU0sS0FBRyxNQUFULEdBQWdCO0FBQUN2ZCx5QkFBTyxFQUFDLENBQVQ7QUFBV2UsbUJBQUMsRUFBQ21HLFNBQVMsS0FBRyxNQUFaLEdBQW1CLE1BQW5CLEdBQTBCO0FBQXZDLGlCQUFoQixHQUE4RCxFQUE5RTtBQUFpRndXLHVCQUFPLENBQUMzYyxDQUFSLEdBQVV3YyxNQUFNLEtBQUcsTUFBVCxHQUFnQnJXLFNBQVMsS0FBRyxNQUFaLEdBQW1CLEtBQW5CLEdBQXlCLE1BQXpDLEdBQWdELElBQTFEO0FBQStEMFcseUJBQVMsQ0FBQy9TLElBQVYsR0FBZSxNQUFJLENBQUN6YyxNQUFMLENBQVlxTyxTQUFaLENBQXNCb08sSUFBckMsRUFBMEMrUyxTQUFTLENBQUNDLE9BQVYsR0FBa0JOLE1BQU0sS0FBRyxNQUFULEdBQWdCO0FBQUN2ZCx5QkFBTyxFQUFDLENBQVQ7QUFBV2UsbUJBQUMsRUFBQ21HLFNBQVMsS0FBRyxNQUFaLEdBQW1CLE9BQW5CLEdBQTJCO0FBQXhDLGlCQUFoQixHQUFnRSxFQUE1SDtBQUErSDBXLHlCQUFTLENBQUM3YyxDQUFWLEdBQVl3YyxNQUFNLEtBQUcsTUFBVCxHQUFnQnJXLFNBQVMsS0FBRyxNQUFaLEdBQW1CLE1BQW5CLEdBQTBCLE9BQTFDLEdBQWtELElBQTlEO0FBQW9FOztBQUNoZ0J5QyxzQkFBUSxDQUFDQyxFQUFULENBQVksTUFBSSxDQUFDcE4sR0FBTCxDQUFTMGdCLFFBQXJCLEVBQThCLE1BQUksQ0FBQzl1QixNQUFMLENBQVlxTyxTQUFaLENBQXNCQyxRQUFwRCxFQUE2RDhnQixZQUE3RDtBQUEyRTdULHNCQUFRLENBQUNDLEVBQVQsQ0FBWSxNQUFJLENBQUNwTixHQUFMLENBQVN5Z0IsT0FBckIsRUFBNkIsTUFBSSxDQUFDN3VCLE1BQUwsQ0FBWXFPLFNBQVosQ0FBc0JDLFFBQW5ELEVBQTREZ2hCLE9BQTVEO0FBQXFFL1Qsc0JBQVEsQ0FBQ0MsRUFBVCxDQUFZLE1BQUksQ0FBQ3BOLEdBQUwsQ0FBUzBILEtBQXJCLEVBQTJCLE1BQUksQ0FBQzlWLE1BQUwsQ0FBWXFPLFNBQVosQ0FBc0JDLFFBQXRCLEdBQStCLEdBQTFELEVBQThEa2hCLFNBQTlEO0FBQXlFalUsc0JBQVEsQ0FBQ0MsRUFBVCxDQUFZLE1BQUksQ0FBQ3BOLEdBQUwsQ0FBUzhMLE1BQXJCLEVBQTRCLE1BQUksQ0FBQ2xhLE1BQUwsQ0FBWXFPLFNBQVosQ0FBc0JDLFFBQWxELEVBQTJEaWhCLFVBQTNEO0FBQXdFLGFBRmpRLENBQVA7QUFFMlE7QUFOakU7QUFBQTtBQUFBLHNDQU92TjVtQixLQVB1TixFQU9qTjtBQUFDLG1CQUFPLEtBQUsrbUIsYUFBTCxDQUFtQixNQUFuQixDQUFQO0FBQW1DO0FBUDZLO0FBQUE7QUFBQSxzQ0FRdk4vbUIsS0FSdU4sRUFRak47QUFBQyxtQkFBTyxLQUFLK21CLGFBQUwsQ0FBbUIsTUFBbkIsQ0FBUDtBQUFtQztBQVI2SztBQUFBO0FBQUEsd0NBU3JOUCxNQVRxTixFQVM5TTtBQUFBOztBQUFDLG1CQUFPLElBQUluZSxPQUFKLENBQVksVUFBQ0MsT0FBRCxFQUFTQyxNQUFULEVBQWtCO0FBQUNxSyxzQkFBUSxDQUFDQyxFQUFULENBQVksTUFBSSxDQUFDcE4sR0FBTCxDQUFTMmdCLE9BQVQsQ0FBaUJELFFBQTdCLEVBQXNDLE1BQUksQ0FBQzl1QixNQUFMLENBQVlxTyxTQUFaLENBQXNCQyxRQUE1RCxFQUFxRTtBQUFDM0YscUJBQUssRUFBQ3dtQixNQUFNLEtBQUcsTUFBVCxHQUFnQixDQUFoQixHQUFrQixNQUFJLENBQUNudkIsTUFBTCxDQUFZcU8sU0FBWixDQUFzQkMsUUFBdEIsR0FBK0IsQ0FBeEQ7QUFBMERtTyxvQkFBSSxFQUFDLE1BQUksQ0FBQ3pjLE1BQUwsQ0FBWXFPLFNBQVosQ0FBc0JvTyxJQUFyRjtBQUEwRmdULHVCQUFPLEVBQUNOLE1BQU0sS0FBRyxNQUFULEdBQWdCO0FBQUNuaUIsbUJBQUMsRUFBQyxJQUFIO0FBQVEyRixtQkFBQyxFQUFDO0FBQVYsaUJBQWhCLEdBQW1DO0FBQUMzRixtQkFBQyxFQUFDLElBQUg7QUFBUTJGLG1CQUFDLEVBQUM7QUFBVixpQkFBckk7QUFBcUpBLGlCQUFDLEVBQUN3YyxNQUFNLEtBQUcsTUFBVCxHQUFnQixJQUFoQixHQUFxQixPQUE1SztBQUFvTHBHLDBCQUFVLEVBQUM5WDtBQUEvTCxlQUFyRTtBQUE4UXNLLHNCQUFRLENBQUNDLEVBQVQsQ0FBWSxNQUFJLENBQUNwTixHQUFMLENBQVMyZ0IsT0FBVCxDQUFpQkYsT0FBN0IsRUFBcUMsTUFBSSxDQUFDN3VCLE1BQUwsQ0FBWXFPLFNBQVosQ0FBc0JDLFFBQTNELEVBQW9FO0FBQUMzRixxQkFBSyxFQUFDd21CLE1BQU0sS0FBRyxNQUFULEdBQWdCLENBQWhCLEdBQWtCLE1BQUksQ0FBQ252QixNQUFMLENBQVlxTyxTQUFaLENBQXNCQyxRQUF0QixHQUErQixDQUF4RDtBQUEwRG1PLG9CQUFJLEVBQUMsTUFBSSxDQUFDemMsTUFBTCxDQUFZcU8sU0FBWixDQUFzQm9PLElBQXJGO0FBQTBGZ1QsdUJBQU8sRUFBQ04sTUFBTSxLQUFHLE1BQVQsR0FBZ0IsRUFBaEIsR0FBbUI7QUFBQ3ZkLHlCQUFPLEVBQUMsQ0FBVDtBQUFXZSxtQkFBQyxFQUFDO0FBQWIsaUJBQXJIO0FBQXlJQSxpQkFBQyxFQUFDd2MsTUFBTSxLQUFHLE1BQVQsR0FBZ0IsS0FBaEIsR0FBc0IsSUFBaks7QUFBc0t2ZCx1QkFBTyxFQUFDdWQsTUFBTSxLQUFHLE1BQVQsR0FBZ0IsQ0FBaEIsR0FBa0I7QUFBaE0sZUFBcEU7QUFBd1E1VCxzQkFBUSxDQUFDQyxFQUFULENBQVksQ0FBQyxNQUFJLENBQUNwTixHQUFMLENBQVMyZ0IsT0FBVCxDQUFpQmpaLEtBQWxCLEVBQXdCLE1BQUksQ0FBQzFILEdBQUwsQ0FBUzJnQixPQUFULENBQWlCOWIsT0FBekMsQ0FBWixFQUE4RCxNQUFJLENBQUNqVCxNQUFMLENBQVlxTyxTQUFaLENBQXNCQyxRQUFwRixFQUE2RjtBQUFDM0YscUJBQUssRUFBQ3dtQixNQUFNLEtBQUcsTUFBVCxHQUFnQixDQUFoQixHQUFrQixNQUFJLENBQUNudkIsTUFBTCxDQUFZcU8sU0FBWixDQUFzQkMsUUFBdEIsR0FBK0IsQ0FBeEQ7QUFBMERtTyxvQkFBSSxFQUFDLE1BQUksQ0FBQ3pjLE1BQUwsQ0FBWXFPLFNBQVosQ0FBc0JvTyxJQUFyRjtBQUEwRmdULHVCQUFPLEVBQUNOLE1BQU0sS0FBRyxNQUFULEdBQWdCLEVBQWhCLEdBQW1CO0FBQUN2ZCx5QkFBTyxFQUFDLENBQVQ7QUFBV2UsbUJBQUMsRUFBQztBQUFiLGlCQUFySDtBQUEwSUEsaUJBQUMsRUFBQ3djLE1BQU0sS0FBRyxNQUFULEdBQWdCLE1BQWhCLEdBQXVCLElBQW5LO0FBQXdLdmQsdUJBQU8sRUFBQ3VkLE1BQU0sS0FBRyxNQUFULEdBQWdCLENBQWhCLEdBQWtCO0FBQWxNLGVBQTdGO0FBQW9TLGFBQXoxQixDQUFQO0FBQW0yQjtBQVR0cEI7O0FBQUE7QUFBQTs7QUFBQSxVQVU3TmpoQixTQVY2TjtBQVVuTiwyQkFBWUMsRUFBWixFQUFlO0FBQUE7O0FBQUE7O0FBQUMsZUFBS0MsR0FBTCxHQUFTO0FBQUNELGNBQUUsRUFBQ0E7QUFBSixXQUFUO0FBQWlCLGVBQUtDLEdBQUwsQ0FBU2dCLFFBQVQsR0FBa0IsS0FBS2hCLEdBQUwsQ0FBU0QsRUFBVCxDQUFZZSxhQUFaLENBQTBCLHNCQUExQixDQUFsQjtBQUFvRSxlQUFLZCxHQUFMLENBQVNlLFFBQVQsR0FBa0IsS0FBS2YsR0FBTCxDQUFTRCxFQUFULENBQVllLGFBQVosQ0FBMEIsc0JBQTFCLENBQWxCO0FBQW9FLGVBQUtkLEdBQUwsQ0FBU3VoQixXQUFULEdBQXFCLEtBQUt2aEIsR0FBTCxDQUFTRCxFQUFULENBQVllLGFBQVosQ0FBMEIsbUJBQTFCLENBQXJCO0FBQW9FLGVBQUt0QyxNQUFMLEdBQVksRUFBWjtBQUFlaUMsZUFBSyxDQUFDQyxJQUFOLENBQVcsS0FBS1YsR0FBTCxDQUFTRCxFQUFULENBQVlZLGdCQUFaLENBQTZCLFFBQTdCLENBQVgsRUFBbURsQyxPQUFuRCxDQUEyRCxVQUFBK2lCLE9BQU87QUFBQSxtQkFBRSxNQUFJLENBQUNoakIsTUFBTCxDQUFZaE0sSUFBWixDQUFpQixJQUFJZ3VCLEtBQUosQ0FBVWdCLE9BQVYsQ0FBakIsQ0FBRjtBQUFBLFdBQWxFO0FBQTBHLGVBQUs1Z0IsV0FBTCxHQUFpQixLQUFLcEMsTUFBTCxDQUFZMUMsTUFBN0I7QUFBb0MsZUFBS21GLE9BQUwsR0FBYSxDQUFiO0FBQWUsZUFBS1QsSUFBTDtBQUFhOztBQVZuTjtBQUFBO0FBQUEsaUNBVzdOO0FBQUMsaUJBQUtoQyxNQUFMLENBQVksS0FBS3lDLE9BQWpCLEVBQTBCd2dCLFVBQTFCO0FBQXVDLGlCQUFLdGdCLFVBQUw7QUFBbUI7QUFYa0s7QUFBQTtBQUFBLHVDQVl2TjtBQUFBOztBQUFDLGlCQUFLbkIsR0FBTCxDQUFTZ0IsUUFBVCxDQUFrQnRFLGdCQUFsQixDQUFtQyxPQUFuQyxFQUEyQztBQUFBLHFCQUFJLE1BQUksQ0FBQ2lNLElBQUwsRUFBSjtBQUFBLGFBQTNDO0FBQTRELGlCQUFLM0ksR0FBTCxDQUFTZSxRQUFULENBQWtCckUsZ0JBQWxCLENBQW1DLE9BQW5DLEVBQTJDO0FBQUEscUJBQUksTUFBSSxDQUFDZ00sSUFBTCxFQUFKO0FBQUEsYUFBM0M7QUFBNEQsaUJBQUsxSSxHQUFMLENBQVN1aEIsV0FBVCxDQUFxQjdrQixnQkFBckIsQ0FBc0MsT0FBdEMsRUFBOEMsVUFBQ3lGLEVBQUQsRUFBTTtBQUFDLGtCQUFHLE1BQUksQ0FBQ0osV0FBUixFQUFvQjs7QUFBTyxrQkFBR0ksRUFBRSxDQUFDdEQsTUFBSCxDQUFVcUUsU0FBVixDQUFvQnlKLFFBQXBCLENBQTZCLHdCQUE3QixDQUFILEVBQTBEO0FBQUN4SyxrQkFBRSxDQUFDdEQsTUFBSCxDQUFVcUUsU0FBVixDQUFvQkMsTUFBcEIsQ0FBMkIsd0JBQTNCOztBQUFxRCxzQkFBSSxDQUFDdWUsV0FBTDtBQUFvQixlQUFwSSxNQUNqTjtBQUFDdmYsa0JBQUUsQ0FBQ3RELE1BQUgsQ0FBVXFFLFNBQVYsQ0FBb0JHLEdBQXBCLENBQXdCLHdCQUF4Qjs7QUFDTCxzQkFBSSxDQUFDc2UsWUFBTDtBQUFxQjtBQUFDLGFBRitHO0FBRTVHO0FBZDBNO0FBQUE7QUFBQSxpQ0FlN047QUFBQyxpQkFBSzFmLFFBQUwsQ0FBYyxNQUFkO0FBQXVCO0FBZnFNO0FBQUE7QUFBQSxpQ0FnQjdOO0FBQUMsaUJBQUtBLFFBQUwsQ0FBYyxPQUFkO0FBQXdCO0FBaEJvTTtBQUFBO0FBQUEseUNBaUJyTjtBQUFDLGlCQUFLcWYsYUFBTCxDQUFtQixPQUFuQjtBQUE2QjtBQWpCdUw7QUFBQTtBQUFBLHdDQWtCdE47QUFBQyxpQkFBS0EsYUFBTCxDQUFtQixNQUFuQjtBQUE0QjtBQWxCeUw7QUFBQTtBQUFBLHdDQW1Cck5QLE1BbkJxTixFQW1COU07QUFBQTs7QUFBQyxnQkFBRyxLQUFLaGYsV0FBUixFQUFvQjtBQUFPLGlCQUFLQSxXQUFMLEdBQWlCLElBQWpCO0FBQXNCLGdCQUFNNmYsVUFBVSxHQUFDYixNQUFNLEtBQUcsT0FBVCxHQUFpQixDQUFDLEtBQUt2aUIsTUFBTCxDQUFZLEtBQUt5QyxPQUFqQixFQUEwQjRnQixJQUExQixDQUErQixJQUEvQixDQUFELEVBQXNDLEtBQUtyakIsTUFBTCxDQUFZLEtBQUt5QyxPQUFqQixFQUEwQjZnQixXQUExQixFQUF0QyxDQUFqQixHQUFnRyxDQUFDLEtBQUt0akIsTUFBTCxDQUFZLEtBQUt5QyxPQUFqQixFQUEwQjhnQixJQUExQixDQUErQixNQUEvQixDQUFELEVBQXdDLEtBQUt2akIsTUFBTCxDQUFZLEtBQUt5QyxPQUFqQixFQUEwQitnQixXQUExQixFQUF4QyxDQUFqSDtBQUFrTSxpQkFBS0MsY0FBTCxDQUFvQmxCLE1BQXBCO0FBQTRCbmUsbUJBQU8sQ0FBQ3NmLEdBQVIsQ0FBWU4sVUFBWixFQUF3QmplLElBQXhCLENBQTZCO0FBQUEscUJBQUksT0FBSSxDQUFDNUIsV0FBTCxHQUFpQixLQUFyQjtBQUFBLGFBQTdCO0FBQTBEO0FBbkI1SDtBQUFBO0FBQUEseUNBb0JwTmdmLE1BcEJvTixFQW9CN007QUFBQTs7QUFBQzVULG9CQUFRLENBQUNDLEVBQVQsQ0FBWSxDQUFDLEtBQUtwTixHQUFMLENBQVNnQixRQUFWLEVBQW1CLEtBQUtoQixHQUFMLENBQVNlLFFBQTVCLENBQVosRUFBa0QsR0FBbEQsRUFBc0Q7QUFBQ3NOLGtCQUFJLEVBQUMsY0FBTjtBQUFxQjdLLHFCQUFPLEVBQUN1ZCxNQUFNLEtBQUcsT0FBVCxHQUFpQixDQUFqQixHQUFtQixDQUFoRDtBQUFrRG9CLHFCQUFPLEVBQUM7QUFBQSx1QkFBSSxPQUFJLENBQUNuaUIsR0FBTCxDQUFTZ0IsUUFBVCxDQUFrQjVDLEtBQWxCLENBQXdCZ2tCLGFBQXhCLEdBQXNDLE9BQUksQ0FBQ3BpQixHQUFMLENBQVNlLFFBQVQsQ0FBa0IzQyxLQUFsQixDQUF3QmdrQixhQUF4QixHQUFzQ3JCLE1BQU0sS0FBRyxPQUFULEdBQWlCLE1BQWpCLEdBQXdCLE1BQXhHO0FBQUE7QUFBMUQsYUFBdEQ7QUFBa087QUFwQnRCO0FBQUE7QUFBQSxtQ0FxQjFOclcsU0FyQjBOLEVBcUJoTjtBQUFBOztBQUFDLGdCQUFHLEtBQUszSSxXQUFSLEVBQW9CO0FBQU8saUJBQUtBLFdBQUwsR0FBaUIsSUFBakI7QUFBc0IsZ0JBQU1zZ0IsWUFBWSxHQUFDM1gsU0FBUyxLQUFHLE9BQVosR0FBb0IsS0FBS3pKLE9BQUwsR0FBYSxLQUFLTCxXQUFMLEdBQWlCLENBQTlCLEdBQWdDLEtBQUtLLE9BQUwsR0FBYSxDQUE3QyxHQUErQyxDQUFuRSxHQUFxRSxLQUFLQSxPQUFMLEdBQWEsQ0FBYixHQUFlLEtBQUtBLE9BQUwsR0FBYSxDQUE1QixHQUE4QixLQUFLTCxXQUFMLEdBQWlCLENBQXZJO0FBQXlJZ0MsbUJBQU8sQ0FBQ3NmLEdBQVIsQ0FBWSxDQUFDLEtBQUsxakIsTUFBTCxDQUFZLEtBQUt5QyxPQUFqQixFQUEwQjRnQixJQUExQixDQUErQm5YLFNBQS9CLENBQUQsRUFBMkMsS0FBS2xNLE1BQUwsQ0FBWTZqQixZQUFaLEVBQTBCTixJQUExQixDQUErQnJYLFNBQS9CLENBQTNDLENBQVosRUFBbUcvRyxJQUFuRyxDQUF3RyxZQUFJO0FBQUMscUJBQUksQ0FBQ25GLE1BQUwsQ0FBWSxPQUFJLENBQUN5QyxPQUFqQixFQUEwQndnQixVQUExQixDQUFxQyxLQUFyQzs7QUFBNEMscUJBQUksQ0FBQ3hnQixPQUFMLEdBQWFvaEIsWUFBYjtBQUEwQixxQkFBSSxDQUFDdGdCLFdBQUwsR0FBaUIsS0FBakI7O0FBQXVCLHFCQUFJLENBQUN2RCxNQUFMLENBQVksT0FBSSxDQUFDeUMsT0FBakIsRUFBMEJ3Z0IsVUFBMUI7QUFBd0MsYUFBbFA7QUFBcVA7QUFyQmhPOztBQUFBO0FBQUE7O0FBc0JuTyxVQUFNcm5CLFNBQVMsR0FBQyxJQUFJMEYsU0FBSixDQUFjMVAsUUFBUSxDQUFDMFEsYUFBVCxDQUF1Qix5Q0FBdkIsQ0FBZCxDQUFoQjtBQUFrRyxLQXRCUztBQXNCUDNQLHFCQUFpQixDQUFDQyxLQUFsQixDQUF3QkMsU0FBeEIsQ0FBa0MsaUVBQWxDLEVBQW9HLFVBQVNDLE1BQVQsRUFBZ0I7QUFBQ2pDLFlBQU0sQ0FBQywwQ0FBRCxDQUFOLENBQW1Ea0IsSUFBbkQsQ0FBd0QsWUFBVTtBQUFDLFlBQUk4SixRQUFRLEdBQUNoTCxNQUFNLENBQUMsSUFBRCxDQUFOLENBQWF3RCxJQUFiLENBQWtCLGVBQWxCLENBQWI7O0FBQWdELFlBQUcsT0FBT3dILFFBQVAsSUFBaUIsV0FBcEIsRUFBZ0M7QUFBQ0Esa0JBQVEsR0FBQyxLQUFUO0FBQWdCOztBQUM3WCxZQUFHQSxRQUFRLElBQUUsQ0FBYixFQUNBO0FBQUNBLGtCQUFRLEdBQUMsSUFBVDtBQUFlLFNBRGhCLE1BR0E7QUFBQ0Esa0JBQVEsR0FBQyxLQUFUO0FBQWdCOztBQUNqQixZQUFJa0QsS0FBSyxHQUFDbE8sTUFBTSxDQUFDLElBQUQsQ0FBTixDQUFhd0QsSUFBYixDQUFrQixZQUFsQixDQUFWOztBQUEwQyxZQUFHLE9BQU8wSyxLQUFQLElBQWMsV0FBakIsRUFBNkI7QUFBQ0EsZUFBSyxHQUFDLElBQU47QUFBWTs7QUFDcEYsWUFBSUMsVUFBVSxHQUFDbk8sTUFBTSxDQUFDLElBQUQsQ0FBTixDQUFhd0QsSUFBYixDQUFrQixpQkFBbEIsQ0FBZjs7QUFBb0QsWUFBRyxPQUFPMkssVUFBUCxJQUFtQixXQUF0QixFQUFrQztBQUFDQSxvQkFBVSxHQUFDLElBQVg7QUFBaUI7O0FBQ3hHLFlBQUdBLFVBQVUsSUFBRSxDQUFmLEVBQ0E7QUFBQ0Esb0JBQVUsR0FBQyxJQUFYO0FBQWlCLFNBRGxCLE1BR0E7QUFBQ0Esb0JBQVUsR0FBQyxLQUFYO0FBQWtCOztBQUNuQm5PLGNBQU0sQ0FBQyxJQUFELENBQU4sQ0FBYWl6QixXQUFiLENBQXlCO0FBQUN0bkIsY0FBSSxFQUFDLElBQU47QUFBV3VuQixnQkFBTSxFQUFDLElBQWxCO0FBQXVCblIsZUFBSyxFQUFDLENBQTdCO0FBQStCdFgsZ0JBQU0sRUFBQyxDQUF0QztBQUF3Q3VCLGtCQUFRLEVBQUNoQixRQUFqRDtBQUEwRDBULGNBQUksRUFBQ3ZRLFVBQS9EO0FBQTBFZ2xCLHlCQUFlLEVBQUNqbEIsS0FBMUY7QUFBZ0drbEIsb0JBQVUsRUFBQyxHQUEzRztBQUErR0Msb0JBQVUsRUFBQztBQUFDLGVBQUU7QUFBQ3RSLG1CQUFLLEVBQUM7QUFBUCxhQUFIO0FBQWEsaUJBQUk7QUFBQ0EsbUJBQUssRUFBQztBQUFQLGFBQWpCO0FBQTJCLGtCQUFLO0FBQUNBLG1CQUFLLEVBQUM7QUFBUDtBQUFoQztBQUExSCxTQUF6QjtBQUFpTSxPQVh3QjtBQVdyQixLQVhoRztBQVdrR2pnQixxQkFBaUIsQ0FBQ0MsS0FBbEIsQ0FBd0JDLFNBQXhCLENBQWtDLHNFQUFsQyxFQUF5RyxVQUFTQyxNQUFULEVBQWdCO0FBQUNqQyxZQUFNLENBQUMsZ0NBQUQsQ0FBTixDQUF5Q2tCLElBQXpDLENBQThDLFlBQVU7QUFBQ2xCLGNBQU0sQ0FBQyxJQUFELENBQU4sQ0FBYWdGLElBQWIsQ0FBa0IsZ0JBQWxCLEVBQW9DaUYsRUFBcEMsQ0FBdUMsQ0FBdkMsRUFBMEM5RixRQUExQyxDQUFtRCxRQUFuRDtBQUE2RCxZQUFJbXZCLEtBQUssR0FBQ3R6QixNQUFNLENBQUMsSUFBRCxDQUFOLENBQWFnRixJQUFiLENBQWtCLGdCQUFsQixFQUFvQ3lILE1BQTlDO0FBQXFELFlBQUltRixPQUFPLEdBQUMsQ0FBWjtBQUFjLFlBQUkyaEIsUUFBUSxHQUFDdnpCLE1BQU0sQ0FBQyxJQUFELENBQW5CO0FBQTBCQSxjQUFNLENBQUMsSUFBRCxDQUFOLENBQWFnRixJQUFiLENBQWtCLFlBQWxCLEVBQWdDbkUsRUFBaEMsQ0FBbUMsT0FBbkMsRUFBMkMsWUFBVTtBQUFDLGNBQUl3WSxJQUFJLEdBQUN6SCxPQUFUO0FBQWlCQSxpQkFBTyxHQUFDQSxPQUFPLEdBQUMsQ0FBaEI7QUFBa0I0aEIsa0JBQVEsQ0FBQ25hLElBQUQsRUFBTXpILE9BQU4sRUFBYzJoQixRQUFkLENBQVI7QUFBaUMsU0FBMUg7QUFBNEh2ekIsY0FBTSxDQUFDLElBQUQsQ0FBTixDQUFhZ0YsSUFBYixDQUFrQixXQUFsQixFQUErQm5FLEVBQS9CLENBQWtDLE9BQWxDLEVBQTBDLFlBQVU7QUFBQyxjQUFJeVksSUFBSSxHQUFDMUgsT0FBVDtBQUFpQkEsaUJBQU8sR0FBQ0EsT0FBTyxHQUFDLENBQWhCO0FBQWtCNGhCLGtCQUFRLENBQUNsYSxJQUFELEVBQU0xSCxPQUFOLEVBQWMyaEIsUUFBZCxDQUFSO0FBQWlDLFNBQXpIOztBQUEySCxpQkFBU0MsUUFBVCxDQUFrQmxhLElBQWxCLEVBQXVCRCxJQUF2QixFQUE0QmthLFFBQTVCLEVBQXFDO0FBQUMsY0FBSWxrQixLQUFLLEdBQUN1QyxPQUFWOztBQUFrQixjQUFHeUgsSUFBSSxHQUFDaWEsS0FBSyxHQUFDLENBQWQsRUFBZ0I7QUFBQ2prQixpQkFBSyxHQUFDLENBQU47QUFBUXVDLG1CQUFPLEdBQUMsQ0FBUjtBQUFXOztBQUN0MkIsY0FBR3lILElBQUksR0FBQyxDQUFSLEVBQVU7QUFBQ2hLLGlCQUFLLEdBQUNpa0IsS0FBSyxHQUFDLENBQVo7QUFBYzFoQixtQkFBTyxHQUFDMGhCLEtBQUssR0FBQyxDQUFkO0FBQWlCOztBQUMxQ0Msa0JBQVEsQ0FBQ3Z1QixJQUFULENBQWMsZ0JBQWQsRUFBZ0NpRixFQUFoQyxDQUFtQ3FQLElBQW5DLEVBQXlDL1gsV0FBekMsQ0FBcUQsUUFBckQ7QUFBK0RneUIsa0JBQVEsQ0FBQ3Z1QixJQUFULENBQWMsZ0JBQWQsRUFBZ0NpRixFQUFoQyxDQUFtQ29GLEtBQW5DLEVBQTBDbEwsUUFBMUMsQ0FBbUQsUUFBbkQ7QUFBNkQwRSxvQkFBVSxDQUFDLFlBQVUsQ0FBRSxDQUFiLEVBQWMsR0FBZCxDQUFWO0FBQThCO0FBQUMsT0FGcUs7QUFFbEssS0FGd0M7QUFFdEMvRyxxQkFBaUIsQ0FBQ0MsS0FBbEIsQ0FBd0JDLFNBQXhCLENBQWtDLCtEQUFsQyxFQUFrRyxVQUFTQyxNQUFULEVBQWdCO0FBQUNqQyxZQUFNLENBQUMsMkJBQUQsQ0FBTixDQUFvQ2tCLElBQXBDLENBQXlDLFlBQVU7QUFBQyxZQUFJNm9CLGFBQWEsR0FBQy9wQixNQUFNLENBQUMsSUFBRCxDQUF4QjtBQUErQixZQUFJZ3FCLGFBQWEsR0FBQyxFQUFsQjtBQUFxQkQscUJBQWEsQ0FBQy9rQixJQUFkLENBQW1CLGlCQUFuQixFQUFzQ25FLEVBQXRDLENBQXlDLE9BQXpDLEVBQWlELFlBQVU7QUFBQ2twQix1QkFBYSxDQUFDL2tCLElBQWQsQ0FBbUIsaUJBQW5CLEVBQXNDekQsV0FBdEMsQ0FBa0QsUUFBbEQ7QUFBNER2QixnQkFBTSxDQUFDLElBQUQsQ0FBTixDQUFhbUUsUUFBYixDQUFzQixRQUF0QjtBQUFnQzZsQix1QkFBYSxHQUFDaHFCLE1BQU0sQ0FBQyxJQUFELENBQU4sQ0FBYXdELElBQWIsQ0FBa0IsVUFBbEIsQ0FBZDtBQUE0Q2l3QixnQkFBTSxHQUFDenpCLE1BQU0sQ0FBQyxJQUFELENBQU4sQ0FBYXdELElBQWIsQ0FBa0IsV0FBbEIsQ0FBUDtBQUFzQyxjQUFJeW1CLE9BQU8sR0FBQ0YsYUFBYSxDQUFDL2tCLElBQWQsQ0FBbUIsaUNBQW5CLENBQVo7QUFBa0VpbEIsaUJBQU8sQ0FBQ0MsTUFBUixDQUFlLEdBQWYsRUFBbUIsQ0FBbkI7QUFBc0JELGlCQUFPLENBQUNqbEIsSUFBUixDQUFhLHlCQUFiLEVBQXdDL0QsR0FBeEMsQ0FBNEM7QUFBQ2tULG1CQUFPLEVBQUMsQ0FBVDtBQUFXZ1csbUJBQU8sRUFBQyxNQUFuQjtBQUEwQmxiLHFCQUFTLEVBQUM7QUFBcEMsV0FBNUM7QUFBK0ZnYixpQkFBTyxDQUFDamxCLElBQVIsQ0FBYSx5QkFBYixFQUF3Q29sQixHQUF4QyxDQUE0QyxNQUFJSixhQUFoRCxFQUErREssT0FBL0QsR0FBeUU5b0IsV0FBekUsQ0FBcUYsV0FBckY7QUFBa0dzSCxvQkFBVSxDQUFDLFlBQVU7QUFBQzdJLGtCQUFNLENBQUMsTUFBSWdxQixhQUFMLENBQU4sQ0FBMEIzZ0IsTUFBMUIsR0FBbUNsRixRQUFuQyxDQUE0QyxXQUE1QztBQUF5RG5FLGtCQUFNLENBQUMsTUFBSWdxQixhQUFMLENBQU4sQ0FBMEIvb0IsR0FBMUIsQ0FBOEI7QUFBQ2tULHFCQUFPLEVBQUMsQ0FBVDtBQUFXZ1cscUJBQU8sRUFBQyxPQUFuQjtBQUEyQmxiLHVCQUFTLEVBQUM7QUFBckMsYUFBOUI7QUFBa0ZnYixtQkFBTyxDQUFDamxCLElBQVIsQ0FBYSw4QkFBYixFQUE2Q3pELFdBQTdDLENBQXlELE1BQXpEO0FBQWlFLGdCQUFJbXlCLEtBQUssR0FBQ3pKLE9BQU8sQ0FBQ2psQixJQUFSLENBQWEsbUNBQWIsRUFBa0R5SCxNQUE1RDtBQUFtRXdkLG1CQUFPLENBQUNqbEIsSUFBUixDQUFhLG1DQUFiLEVBQWtEOUQsSUFBbEQsQ0FBdUQsVUFBUzhJLEtBQVQsRUFBZTtBQUFDLGtCQUFJMnBCLFNBQVMsR0FBQ2p5QixRQUFRLENBQUNzSSxLQUFLLEdBQUMsQ0FBUCxDQUF0Qjs7QUFBZ0Msa0JBQUcycEIsU0FBUyxHQUFDRixNQUFWLElBQWtCLENBQXJCLEVBQzF3QztBQUFDenpCLHNCQUFNLENBQUMsSUFBRCxDQUFOLENBQWFtRSxRQUFiLENBQXNCLE1BQXRCO0FBQStCOztBQUNoQyxrQkFBR3d2QixTQUFTLElBQUVELEtBQWQsRUFDQTtBQUFDN3FCLDBCQUFVLENBQUMsWUFBVTtBQUFDb2hCLHlCQUFPLENBQUNDLE1BQVIsQ0FBZSxHQUFmLEVBQW1CLENBQW5CO0FBQXVCLGlCQUFuQyxFQUFvQyxHQUFwQyxDQUFWO0FBQW9EO0FBQUMsYUFINm1DO0FBRzFtQyxXQUgrMEIsRUFHOTBCLEdBSDgwQixDQUFWO0FBRzl6QixTQUgyVDtBQUd4VCxPQUhnTjtBQUc3TSxLQUgwRjtBQUd4RnBvQixxQkFBaUIsQ0FBQ0MsS0FBbEIsQ0FBd0JDLFNBQXhCLENBQWtDLHVFQUFsQyxFQUEwRyxVQUFTQyxNQUFULEVBQWdCO0FBQUNqQyxZQUFNLENBQUMsbUNBQUQsQ0FBTixDQUE0Q2tCLElBQTVDLENBQWlELFlBQVU7QUFBQyxZQUFJNm9CLGFBQWEsR0FBQy9wQixNQUFNLENBQUMsSUFBRCxDQUF4QjtBQUErQixZQUFJZ3FCLGFBQWEsR0FBQyxFQUFsQjtBQUFxQkQscUJBQWEsQ0FBQy9rQixJQUFkLENBQW1CLGlDQUFuQixFQUFzRDBFLFlBQXRELEdBQXFFa3FCLE1BQXJFLENBQTRFLFlBQVU7QUFBQyxjQUFJQyxjQUFjLEdBQUM5SixhQUFhLENBQUMva0IsSUFBZCxDQUFtQixpQ0FBbkIsQ0FBbkI7QUFBeUUsY0FBSTdELFVBQVUsR0FBQzRvQixhQUFhLENBQUMva0IsSUFBZCxDQUFtQixxQ0FBbkIsQ0FBZjtBQUF5RTZ1Qix3QkFBYyxDQUFDNXlCLEdBQWYsQ0FBbUIsUUFBbkIsRUFBNEJFLFVBQVUsQ0FBQ1IsTUFBWCxLQUFvQixJQUFoRDtBQUF1RCxTQUFoUztBQUFrU1gsY0FBTSxDQUFDTixNQUFELENBQU4sQ0FBZWtDLE1BQWYsQ0FBc0IsWUFBVTtBQUFDbW9CLHVCQUFhLENBQUMva0IsSUFBZCxDQUFtQixpQ0FBbkIsRUFBc0Q5RCxJQUF0RCxDQUEyRCxZQUFVO0FBQUMsZ0JBQUlDLFVBQVUsR0FBQ25CLE1BQU0sQ0FBQyxJQUFELENBQU4sQ0FBYWdGLElBQWIsQ0FBa0IsS0FBbEIsQ0FBZjtBQUF3Q2hGLGtCQUFNLENBQUMsSUFBRCxDQUFOLENBQWFpQixHQUFiLENBQWlCLFFBQWpCLEVBQTBCRSxVQUFVLENBQUNSLE1BQVgsS0FBb0IsSUFBOUM7QUFBcUQsV0FBbks7QUFBc0ssU0FBdk07QUFBeU1vcEIscUJBQWEsQ0FBQy9rQixJQUFkLENBQW1CLGlCQUFuQixFQUFzQ25FLEVBQXRDLENBQXlDLE9BQXpDLEVBQWlELFlBQVU7QUFBQ2twQix1QkFBYSxDQUFDL2tCLElBQWQsQ0FBbUIsaUJBQW5CLEVBQXNDekQsV0FBdEMsQ0FBa0QsUUFBbEQ7QUFBNER2QixnQkFBTSxDQUFDLElBQUQsQ0FBTixDQUFhbUUsUUFBYixDQUFzQixRQUF0QjtBQUFnQzZsQix1QkFBYSxHQUFDaHFCLE1BQU0sQ0FBQyxJQUFELENBQU4sQ0FBYXdELElBQWIsQ0FBa0IsVUFBbEIsQ0FBZDtBQUE0Q2l3QixnQkFBTSxHQUFDenpCLE1BQU0sQ0FBQyxJQUFELENBQU4sQ0FBYXdELElBQWIsQ0FBa0IsV0FBbEIsQ0FBUDtBQUFzQyxjQUFJeW1CLE9BQU8sR0FBQ0YsYUFBYSxDQUFDL2tCLElBQWQsQ0FBbUIsaUNBQW5CLENBQVo7QUFBa0VpbEIsaUJBQU8sQ0FBQ0MsTUFBUixDQUFlLEdBQWYsRUFBbUIsQ0FBbkI7QUFBc0JELGlCQUFPLENBQUNqbEIsSUFBUixDQUFhLGlDQUFiLEVBQWdEL0QsR0FBaEQsQ0FBb0Q7QUFBQ2tULG1CQUFPLEVBQUMsQ0FBVDtBQUFXZ1csbUJBQU8sRUFBQyxNQUFuQjtBQUEwQmxiLHFCQUFTLEVBQUM7QUFBcEMsV0FBcEQ7QUFBdUdnYixpQkFBTyxDQUFDamxCLElBQVIsQ0FBYSxpQ0FBYixFQUFnRG9sQixHQUFoRCxDQUFvRCxNQUFJSixhQUF4RCxFQUF1RUssT0FBdkUsR0FBaUY5b0IsV0FBakYsQ0FBNkYsV0FBN0Y7QUFBMEdzSCxvQkFBVSxDQUFDLFlBQVU7QUFBQzdJLGtCQUFNLENBQUMsTUFBSWdxQixhQUFMLENBQU4sQ0FBMEIzZ0IsTUFBMUIsR0FBbUNsRixRQUFuQyxDQUE0QyxXQUE1QztBQUF5RG5FLGtCQUFNLENBQUMsTUFBSWdxQixhQUFMLENBQU4sQ0FBMEIvb0IsR0FBMUIsQ0FBOEI7QUFBQ2tULHFCQUFPLEVBQUMsQ0FBVDtBQUFXZ1cscUJBQU8sRUFBQyxPQUFuQjtBQUEyQmxiLHVCQUFTLEVBQUM7QUFBckMsYUFBOUI7QUFBa0ZnYixtQkFBTyxDQUFDamxCLElBQVIsQ0FBYSxzQ0FBYixFQUFxRHpELFdBQXJELENBQWlFLE1BQWpFO0FBQXlFLGdCQUFJbXlCLEtBQUssR0FBQ3pKLE9BQU8sQ0FBQ2psQixJQUFSLENBQWEsMkNBQWIsRUFBMER5SCxNQUFwRTtBQUEyRXdkLG1CQUFPLENBQUNqbEIsSUFBUixDQUFhLDJDQUFiLEVBQTBEOUQsSUFBMUQsQ0FBK0QsVUFBUzhJLEtBQVQsRUFBZTtBQUFDLGtCQUFJMnBCLFNBQVMsR0FBQ2p5QixRQUFRLENBQUNzSSxLQUFLLEdBQUMsQ0FBUCxDQUF0Qjs7QUFBZ0Msa0JBQUcycEIsU0FBUyxHQUFDRixNQUFWLElBQWtCLENBQXJCLEVBQ3J0RDtBQUFDenpCLHNCQUFNLENBQUMsSUFBRCxDQUFOLENBQWFtRSxRQUFiLENBQXNCLE1BQXRCO0FBQStCOztBQUNoQyxrQkFBR3d2QixTQUFTLElBQUVELEtBQWQsRUFDQTtBQUFDN3FCLDBCQUFVLENBQUMsWUFBVTtBQUFDb2hCLHlCQUFPLENBQUNDLE1BQVIsQ0FBZSxHQUFmLEVBQW1CLENBQW5CO0FBQXVCLGlCQUFuQyxFQUFvQyxHQUFwQyxDQUFWO0FBQW9EO0FBQUMsYUFIZ2pEO0FBRzdpRCxXQUhrd0MsRUFHandDLEdBSGl3QyxDQUFWO0FBR2p2QyxTQUg4dEI7QUFHM3RCLE9BSGdJO0FBRzdILEtBSEU7QUFHQXBvQixxQkFBaUIsQ0FBQ0MsS0FBbEIsQ0FBd0JDLFNBQXhCLENBQWtDLHFFQUFsQyxFQUF3RyxVQUFTQyxNQUFULEVBQWdCO0FBQUNqQyxZQUFNLENBQUMsbUNBQUQsQ0FBTixDQUE0Q2tCLElBQTVDLENBQWlELFlBQVU7QUFBQyxZQUFJNm9CLGFBQWEsR0FBQy9wQixNQUFNLENBQUMsSUFBRCxDQUF4QjtBQUErQixZQUFJZ3FCLGFBQWEsR0FBQyxFQUFsQjtBQUFxQkQscUJBQWEsQ0FBQy9rQixJQUFkLENBQW1CLGlDQUFuQixFQUFzRDBFLFlBQXRELEdBQXFFa3FCLE1BQXJFLENBQTRFLFlBQVU7QUFBQyxjQUFJQyxjQUFjLEdBQUM5SixhQUFhLENBQUMva0IsSUFBZCxDQUFtQixpQ0FBbkIsQ0FBbkI7QUFBeUUsY0FBSTdELFVBQVUsR0FBQzRvQixhQUFhLENBQUMva0IsSUFBZCxDQUFtQixxQ0FBbkIsQ0FBZjtBQUF5RTZ1Qix3QkFBYyxDQUFDNXlCLEdBQWYsQ0FBbUIsUUFBbkIsRUFBNEJFLFVBQVUsQ0FBQ1IsTUFBWCxLQUFvQixJQUFoRDtBQUF1RCxTQUFoUztBQUFrU1gsY0FBTSxDQUFDTixNQUFELENBQU4sQ0FBZWtDLE1BQWYsQ0FBc0IsWUFBVTtBQUFDbW9CLHVCQUFhLENBQUMva0IsSUFBZCxDQUFtQixpQ0FBbkIsRUFBc0Q5RCxJQUF0RCxDQUEyRCxZQUFVO0FBQUMsZ0JBQUlDLFVBQVUsR0FBQ25CLE1BQU0sQ0FBQyxJQUFELENBQU4sQ0FBYWdGLElBQWIsQ0FBa0IsS0FBbEIsQ0FBZjtBQUF3Q2hGLGtCQUFNLENBQUMsSUFBRCxDQUFOLENBQWFpQixHQUFiLENBQWlCLFFBQWpCLEVBQTBCRSxVQUFVLENBQUNSLE1BQVgsS0FBb0IsSUFBOUM7QUFBcUQsV0FBbks7QUFBc0ssU0FBdk07O0FBQXlNLFlBQUcsQ0FBQ216QixlQUFlLEVBQW5CLEVBQzV4QjtBQUFDLGNBQUlDLFNBQVMsR0FBQyxJQUFkOztBQUFtQixjQUFHbDBCLFNBQVMsQ0FBQ0MsU0FBVixDQUFvQkMsT0FBcEIsQ0FBNEIsUUFBNUIsS0FBdUMsQ0FBQyxDQUF4QyxJQUEyQ0YsU0FBUyxDQUFDQyxTQUFWLENBQW9CQyxPQUFwQixDQUE0QixRQUE1QixLQUF1QyxDQUFDLENBQXRGLEVBQXdGO0FBQUNnMEIscUJBQVMsR0FBQyxDQUFWO0FBQWE7O0FBQzFIaEssdUJBQWEsQ0FBQy9rQixJQUFkLENBQW1CLFlBQW5CLEVBQWlDd1IsSUFBakMsQ0FBc0M7QUFBQ3FELGlCQUFLLEVBQUNrYSxTQUFQO0FBQWlCenNCLHVCQUFXLEVBQUMsSUFBN0I7QUFBa0Mwc0IsaUJBQUssRUFBQyxJQUF4QztBQUE2Q0Msb0JBQVEsRUFBQztBQUF0RCxXQUF0QztBQUFrRzs7QUFDbEcsWUFBRyxDQUFDbnlCLGlCQUFpQixDQUFDSSxVQUFsQixFQUFKLEVBQ0E7QUFBQzZuQix1QkFBYSxDQUFDL2tCLElBQWQsQ0FBbUIsaUNBQW5CLEVBQXNEbkUsRUFBdEQsQ0FBeUQsT0FBekQsRUFBaUUsWUFBVTtBQUFDbkIsa0JBQU0sQ0FBQ3cwQixRQUFQLEdBQWdCbDBCLE1BQU0sQ0FBQyxJQUFELENBQU4sQ0FBYWdGLElBQWIsQ0FBa0IsR0FBbEIsRUFBdUJ3VixLQUF2QixHQUErQmhYLElBQS9CLENBQW9DLE1BQXBDLENBQWhCO0FBQTZELFdBQXpJO0FBQTRJOztBQUM3SXVtQixxQkFBYSxDQUFDL2tCLElBQWQsQ0FBbUIsaUJBQW5CLEVBQXNDbkUsRUFBdEMsQ0FBeUMsT0FBekMsRUFBaUQsWUFBVTtBQUFDa3BCLHVCQUFhLENBQUMva0IsSUFBZCxDQUFtQixpQkFBbkIsRUFBc0N6RCxXQUF0QyxDQUFrRCxRQUFsRDtBQUE0RHZCLGdCQUFNLENBQUMsSUFBRCxDQUFOLENBQWFtRSxRQUFiLENBQXNCLFFBQXRCO0FBQWdDNmxCLHVCQUFhLEdBQUNocUIsTUFBTSxDQUFDLElBQUQsQ0FBTixDQUFhd0QsSUFBYixDQUFrQixVQUFsQixDQUFkO0FBQTRDaXdCLGdCQUFNLEdBQUN6ekIsTUFBTSxDQUFDLElBQUQsQ0FBTixDQUFhd0QsSUFBYixDQUFrQixXQUFsQixDQUFQO0FBQXNDLGNBQUl5bUIsT0FBTyxHQUFDRixhQUFhLENBQUMva0IsSUFBZCxDQUFtQixpQ0FBbkIsQ0FBWjtBQUFrRWlsQixpQkFBTyxDQUFDQyxNQUFSLENBQWUsR0FBZixFQUFtQixDQUFuQjtBQUFzQkQsaUJBQU8sQ0FBQ2psQixJQUFSLENBQWEsaUNBQWIsRUFBZ0QvRCxHQUFoRCxDQUFvRDtBQUFDa1QsbUJBQU8sRUFBQyxDQUFUO0FBQVdnVyxtQkFBTyxFQUFDLE1BQW5CO0FBQTBCbGIscUJBQVMsRUFBQztBQUFwQyxXQUFwRDtBQUF1R2diLGlCQUFPLENBQUNqbEIsSUFBUixDQUFhLGlDQUFiLEVBQWdEb2xCLEdBQWhELENBQW9ELE1BQUlKLGFBQXhELEVBQXVFSyxPQUF2RSxHQUFpRjlvQixXQUFqRixDQUE2RixXQUE3RjtBQUEwR3NILG9CQUFVLENBQUMsWUFBVTtBQUFDN0ksa0JBQU0sQ0FBQyxNQUFJZ3FCLGFBQUwsQ0FBTixDQUEwQjNnQixNQUExQixHQUFtQ2xGLFFBQW5DLENBQTRDLFdBQTVDO0FBQXlEbkUsa0JBQU0sQ0FBQyxNQUFJZ3FCLGFBQUwsQ0FBTixDQUEwQi9vQixHQUExQixDQUE4QjtBQUFDa1QscUJBQU8sRUFBQyxDQUFUO0FBQVdnVyxxQkFBTyxFQUFDLE9BQW5CO0FBQTJCbGIsdUJBQVMsRUFBQztBQUFyQyxhQUE5QjtBQUFrRmdiLG1CQUFPLENBQUNqbEIsSUFBUixDQUFhLHNDQUFiLEVBQXFEekQsV0FBckQsQ0FBaUUsTUFBakU7QUFBeUUsZ0JBQUlteUIsS0FBSyxHQUFDekosT0FBTyxDQUFDamxCLElBQVIsQ0FBYSwyQ0FBYixFQUEwRHlILE1BQXBFO0FBQTJFd2QsbUJBQU8sQ0FBQ2psQixJQUFSLENBQWEsMkNBQWIsRUFBMEQ5RCxJQUExRCxDQUErRCxVQUFTOEksS0FBVCxFQUFlO0FBQUMsa0JBQUkycEIsU0FBUyxHQUFDanlCLFFBQVEsQ0FBQ3NJLEtBQUssR0FBQyxDQUFQLENBQXRCOztBQUFnQyxrQkFBRzJwQixTQUFTLEdBQUNGLE1BQVYsSUFBa0IsQ0FBckIsRUFDdjdCO0FBQUN6ekIsc0JBQU0sQ0FBQyxJQUFELENBQU4sQ0FBYW1FLFFBQWIsQ0FBc0IsTUFBdEI7QUFBK0I7O0FBQ2hDLGtCQUFHd3ZCLFNBQVMsSUFBRUQsS0FBZCxFQUNBO0FBQUM3cUIsMEJBQVUsQ0FBQyxZQUFVO0FBQUNvaEIseUJBQU8sQ0FBQ0MsTUFBUixDQUFlLEdBQWYsRUFBbUIsQ0FBbkI7QUFBdUIsaUJBQW5DLEVBQW9DLEdBQXBDLENBQVY7QUFBb0Q7QUFBQyxhQUhreEI7QUFHL3dCLFdBSG9lLEVBR25lLEdBSG1lLENBQVY7QUFHbmQsU0FIaEU7QUFHbUUsT0FSOEg7QUFRM0gsS0FSRTtBQVFBcG9CLHFCQUFpQixDQUFDQyxLQUFsQixDQUF3QkMsU0FBeEIsQ0FBa0Msa0VBQWxDLEVBQXFHLFVBQVNDLE1BQVQsRUFBZ0I7QUFBQ2pDLFlBQU0sQ0FBQyw4QkFBRCxDQUFOLENBQXVDa0IsSUFBdkMsQ0FBNEMsWUFBVTtBQUFDLFlBQUk2b0IsYUFBYSxHQUFDL3BCLE1BQU0sQ0FBQyxJQUFELENBQXhCO0FBQStCLFlBQUlncUIsYUFBYSxHQUFDLEVBQWxCO0FBQXFCRCxxQkFBYSxDQUFDL2tCLElBQWQsQ0FBbUIsaUJBQW5CLEVBQXNDbkUsRUFBdEMsQ0FBeUMsT0FBekMsRUFBaUQsWUFBVTtBQUFDa3BCLHVCQUFhLENBQUMva0IsSUFBZCxDQUFtQixpQkFBbkIsRUFBc0N6RCxXQUF0QyxDQUFrRCxRQUFsRDtBQUE0RHZCLGdCQUFNLENBQUMsSUFBRCxDQUFOLENBQWFtRSxRQUFiLENBQXNCLFFBQXRCO0FBQWdDNmxCLHVCQUFhLEdBQUNocUIsTUFBTSxDQUFDLElBQUQsQ0FBTixDQUFhd0QsSUFBYixDQUFrQixVQUFsQixDQUFkO0FBQTRDaXdCLGdCQUFNLEdBQUN6ekIsTUFBTSxDQUFDLElBQUQsQ0FBTixDQUFhd0QsSUFBYixDQUFrQixXQUFsQixDQUFQO0FBQXNDLGNBQUl5bUIsT0FBTyxHQUFDRixhQUFhLENBQUMva0IsSUFBZCxDQUFtQixvQ0FBbkIsQ0FBWjtBQUFxRWlsQixpQkFBTyxDQUFDQyxNQUFSLENBQWUsR0FBZixFQUFtQixDQUFuQjtBQUFzQkQsaUJBQU8sQ0FBQ2psQixJQUFSLENBQWEsaUNBQWIsRUFBZ0QvRCxHQUFoRCxDQUFvRDtBQUFDa1QsbUJBQU8sRUFBQyxDQUFUO0FBQVdnVyxtQkFBTyxFQUFDLE1BQW5CO0FBQTBCbGIscUJBQVMsRUFBQztBQUFwQyxXQUFwRDtBQUF1R2diLGlCQUFPLENBQUNqbEIsSUFBUixDQUFhLGlDQUFiLEVBQWdEb2xCLEdBQWhELENBQW9ELE1BQUlKLGFBQXhELEVBQXVFSyxPQUF2RSxHQUFpRjlvQixXQUFqRixDQUE2RixXQUE3RjtBQUEwR3NILG9CQUFVLENBQUMsWUFBVTtBQUFDN0ksa0JBQU0sQ0FBQyxNQUFJZ3FCLGFBQUwsQ0FBTixDQUEwQjdsQixRQUExQixDQUFtQyxXQUFuQztBQUFnRG5FLGtCQUFNLENBQUMsTUFBSWdxQixhQUFMLENBQU4sQ0FBMEIvb0IsR0FBMUIsQ0FBOEI7QUFBQ2tULHFCQUFPLEVBQUMsQ0FBVDtBQUFXZ1cscUJBQU8sRUFBQyxPQUFuQjtBQUEyQmxiLHVCQUFTLEVBQUM7QUFBckMsYUFBOUI7QUFBa0ZnYixtQkFBTyxDQUFDamxCLElBQVIsQ0FBYSxzQ0FBYixFQUFxRHpELFdBQXJELENBQWlFLE1BQWpFO0FBQXlFLGdCQUFJbXlCLEtBQUssR0FBQ3pKLE9BQU8sQ0FBQ2psQixJQUFSLENBQWEsMkNBQWIsRUFBMER5SCxNQUFwRTtBQUEyRXdkLG1CQUFPLENBQUNqbEIsSUFBUixDQUFhLDJDQUFiLEVBQTBEOUQsSUFBMUQsQ0FBK0QsVUFBUzhJLEtBQVQsRUFBZTtBQUFDLGtCQUFJMnBCLFNBQVMsR0FBQ2p5QixRQUFRLENBQUNzSSxLQUFLLEdBQUMsQ0FBUCxDQUF0Qjs7QUFBZ0Msa0JBQUcycEIsU0FBUyxHQUFDRixNQUFWLElBQWtCLENBQXJCLEVBQzF0QztBQUFDenpCLHNCQUFNLENBQUMsSUFBRCxDQUFOLENBQWFtRSxRQUFiLENBQXNCLE1BQXRCO0FBQStCOztBQUNoQyxrQkFBR3d2QixTQUFTLElBQUVELEtBQWQsRUFDQTtBQUFDN3FCLDBCQUFVLENBQUMsWUFBVTtBQUFDb2hCLHlCQUFPLENBQUNDLE1BQVIsQ0FBZSxHQUFmLEVBQW1CLENBQW5CO0FBQXVCLGlCQUFuQyxFQUFvQyxHQUFwQyxDQUFWO0FBQW9EO0FBQUMsYUFIcWpDO0FBR2xqQyxXQUhneEIsRUFHL3dCLEdBSCt3QixDQUFWO0FBRy92QixTQUh5TztBQUd0TyxPQUgySDtBQUd4SCxLQUhFO0FBR0Fwb0IscUJBQWlCLENBQUNDLEtBQWxCLENBQXdCQyxTQUF4QixDQUFrQyxrRUFBbEMsRUFBcUcsVUFBU0MsTUFBVCxFQUFnQjtBQUFDakMsWUFBTSxDQUFDLHNDQUFELENBQU4sQ0FBK0NrQixJQUEvQyxDQUFvRCxZQUFVO0FBQUMsWUFBSTZvQixhQUFhLEdBQUMvcEIsTUFBTSxDQUFDLElBQUQsQ0FBeEI7QUFBK0IsWUFBSWdxQixhQUFhLEdBQUMsRUFBbEI7QUFBcUJELHFCQUFhLENBQUMva0IsSUFBZCxDQUFtQixpQkFBbkIsRUFBc0NuRSxFQUF0QyxDQUF5QyxPQUF6QyxFQUFpRCxZQUFVO0FBQUNrcEIsdUJBQWEsQ0FBQy9rQixJQUFkLENBQW1CLGlCQUFuQixFQUFzQ3pELFdBQXRDLENBQWtELFFBQWxEO0FBQTREdkIsZ0JBQU0sQ0FBQyxJQUFELENBQU4sQ0FBYW1FLFFBQWIsQ0FBc0IsUUFBdEI7QUFBZ0M2bEIsdUJBQWEsR0FBQ2hxQixNQUFNLENBQUMsSUFBRCxDQUFOLENBQWF3RCxJQUFiLENBQWtCLFVBQWxCLENBQWQ7QUFBNENpd0IsZ0JBQU0sR0FBQ3p6QixNQUFNLENBQUMsSUFBRCxDQUFOLENBQWF3RCxJQUFiLENBQWtCLFdBQWxCLENBQVA7QUFBc0MsY0FBSXltQixPQUFPLEdBQUNGLGFBQWEsQ0FBQy9rQixJQUFkLENBQW1CLG9DQUFuQixDQUFaO0FBQXFFaWxCLGlCQUFPLENBQUNDLE1BQVIsQ0FBZSxHQUFmLEVBQW1CLENBQW5CO0FBQXNCRCxpQkFBTyxDQUFDamxCLElBQVIsQ0FBYSxpQ0FBYixFQUFnRC9ELEdBQWhELENBQW9EO0FBQUNrVCxtQkFBTyxFQUFDLENBQVQ7QUFBV2dXLG1CQUFPLEVBQUMsTUFBbkI7QUFBMEJsYixxQkFBUyxFQUFDO0FBQXBDLFdBQXBEO0FBQXVHZ2IsaUJBQU8sQ0FBQ2psQixJQUFSLENBQWEsaUNBQWIsRUFBZ0RvbEIsR0FBaEQsQ0FBb0QsTUFBSUosYUFBeEQsRUFBdUVLLE9BQXZFLEdBQWlGOW9CLFdBQWpGLENBQTZGLFdBQTdGO0FBQTBHc0gsb0JBQVUsQ0FBQyxZQUFVO0FBQUM3SSxrQkFBTSxDQUFDLE1BQUlncUIsYUFBTCxDQUFOLENBQTBCN2xCLFFBQTFCLENBQW1DLFdBQW5DO0FBQWdEbkUsa0JBQU0sQ0FBQyxNQUFJZ3FCLGFBQUwsQ0FBTixDQUEwQi9vQixHQUExQixDQUE4QjtBQUFDa1QscUJBQU8sRUFBQyxDQUFUO0FBQVdnVyxxQkFBTyxFQUFDLE9BQW5CO0FBQTJCbGIsdUJBQVMsRUFBQztBQUFyQyxhQUE5QjtBQUFrRmdiLG1CQUFPLENBQUNqbEIsSUFBUixDQUFhLHNDQUFiLEVBQXFEekQsV0FBckQsQ0FBaUUsTUFBakU7QUFBeUUsZ0JBQUlteUIsS0FBSyxHQUFDekosT0FBTyxDQUFDamxCLElBQVIsQ0FBYSwyQ0FBYixFQUEwRHlILE1BQXBFO0FBQTJFd2QsbUJBQU8sQ0FBQ2psQixJQUFSLENBQWEsMkNBQWIsRUFBMEQ5RCxJQUExRCxDQUErRCxVQUFTOEksS0FBVCxFQUFlO0FBQUMsa0JBQUkycEIsU0FBUyxHQUFDanlCLFFBQVEsQ0FBQ3NJLEtBQUssR0FBQyxDQUFQLENBQXRCOztBQUFnQyxrQkFBRzJwQixTQUFTLEdBQUNGLE1BQVYsSUFBa0IsQ0FBckIsRUFDbHVDO0FBQUN6ekIsc0JBQU0sQ0FBQyxJQUFELENBQU4sQ0FBYW1FLFFBQWIsQ0FBc0IsTUFBdEI7QUFBK0I7O0FBQ2hDLGtCQUFHd3ZCLFNBQVMsSUFBRUQsS0FBZCxFQUNBO0FBQUM3cUIsMEJBQVUsQ0FBQyxZQUFVO0FBQUNvaEIseUJBQU8sQ0FBQ0MsTUFBUixDQUFlLEdBQWYsRUFBbUIsQ0FBbkI7QUFBdUIsaUJBQW5DLEVBQW9DLEdBQXBDLENBQVY7QUFBb0Q7QUFBQyxhQUg2akM7QUFHMWpDLFdBSHd4QixFQUd2eEIsR0FIdXhCLENBQVY7QUFHdndCLFNBSGlQO0FBRzlPLE9BSDJIO0FBR3hILEtBSEU7QUFHQXBvQixxQkFBaUIsQ0FBQ0MsS0FBbEIsQ0FBd0JDLFNBQXhCLENBQWtDLGdFQUFsQyxFQUFtRyxVQUFTQyxNQUFULEVBQWdCO0FBQUNqQyxZQUFNLENBQUMsa0RBQUQsQ0FBTixDQUEyRGliLEtBQTNELENBQWlFLFlBQzdQO0FBQUNqYixjQUFNLENBQUMsSUFBRCxDQUFOLENBQWFpYyxRQUFiLENBQXNCLFVBQXRCLEVBQWtDOVgsUUFBbEMsQ0FBMkMsU0FBM0M7QUFBc0RuRSxjQUFNLENBQUMsSUFBRCxDQUFOLENBQWFpYyxRQUFiLENBQXNCLFVBQXRCLEVBQWtDOVgsUUFBbEMsQ0FBMkMsT0FBM0M7QUFBb0RuRSxjQUFNLENBQUMsSUFBRCxDQUFOLENBQWFpYyxRQUFiLENBQXNCLG1DQUF0QixFQUEyRDlYLFFBQTNELENBQW9FLFNBQXBFO0FBQStFbkUsY0FBTSxDQUFDLElBQUQsQ0FBTixDQUFhaWMsUUFBYixDQUFzQixtQ0FBdEIsRUFBMkQ5WCxRQUEzRCxDQUFvRSxPQUFwRTtBQUE4RSxPQUQ1RSxFQUM2RSxZQUN6UTtBQUFDbkUsY0FBTSxDQUFDLElBQUQsQ0FBTixDQUFhaWMsUUFBYixDQUFzQixVQUF0QixFQUFrQzFhLFdBQWxDLENBQThDLFNBQTlDO0FBQXlEdkIsY0FBTSxDQUFDLElBQUQsQ0FBTixDQUFhaWMsUUFBYixDQUFzQixVQUF0QixFQUFrQzFhLFdBQWxDLENBQThDLE9BQTlDO0FBQXVEdkIsY0FBTSxDQUFDLElBQUQsQ0FBTixDQUFhaWMsUUFBYixDQUFzQixtQ0FBdEIsRUFBMkQxYSxXQUEzRCxDQUF1RSxTQUF2RTtBQUFrRnZCLGNBQU0sQ0FBQyxJQUFELENBQU4sQ0FBYWljLFFBQWIsQ0FBc0IsbUNBQXRCLEVBQTJEMWEsV0FBM0QsQ0FBdUUsT0FBdkU7QUFBaUYsT0FGeEY7QUFFMEZ2QixZQUFNLENBQUMsa0RBQUQsQ0FBTixDQUEyRGljLFFBQTNELENBQW9FLGdCQUFwRSxFQUFzRmhCLEtBQXRGLENBQTRGLFlBQ2xYO0FBQUNqYixjQUFNLENBQUMsSUFBRCxDQUFOLENBQWFpSixJQUFiLEdBQW9COUUsUUFBcEIsQ0FBNkIsU0FBN0I7QUFBeUMsT0FENE8sRUFDM08sWUFDM0M7QUFBQ25FLGNBQU0sQ0FBQyxJQUFELENBQU4sQ0FBYWlKLElBQWIsR0FBb0IxSCxXQUFwQixDQUFnQyxTQUFoQztBQUE0QyxPQUZ5TztBQUV2T3ZCLFlBQU0sQ0FBQyxrREFBRCxDQUFOLENBQTJEaWMsUUFBM0QsQ0FBb0UsbUNBQXBFLEVBQXlHaEIsS0FBekcsQ0FBK0csWUFDOUo7QUFBQ2piLGNBQU0sQ0FBQyxJQUFELENBQU4sQ0FBYWlKLElBQWIsR0FBb0I5RSxRQUFwQixDQUE2QixTQUE3QjtBQUF5QyxPQURLLEVBQ0osWUFDM0M7QUFBQ25FLGNBQU0sQ0FBQyxJQUFELENBQU4sQ0FBYWlKLElBQWIsR0FBb0IxSCxXQUFwQixDQUFnQyxTQUFoQztBQUE0QyxPQUZFO0FBRUMsS0FOd0I7QUFNdEJPLHFCQUFpQixDQUFDQyxLQUFsQixDQUF3QkMsU0FBeEIsQ0FBa0MsMkRBQWxDLEVBQThGLFVBQVNDLE1BQVQsRUFBZ0I7QUFBQ2pDLFlBQU0sQ0FBQyx5Q0FBRCxDQUFOLENBQWtEa0IsSUFBbEQsQ0FBdUQsWUFBVTtBQUFDLFlBQUlpekIsWUFBWSxHQUFDbjBCLE1BQU0sQ0FBQyxJQUFELENBQXZCO0FBQThCLFlBQUlncUIsYUFBYSxHQUFDLEVBQWxCO0FBQXFCbUssb0JBQVksQ0FBQ252QixJQUFiLENBQWtCLGlCQUFsQixFQUFxQ25FLEVBQXJDLENBQXdDLE9BQXhDLEVBQWdELFlBQVU7QUFBQ3N6QixzQkFBWSxDQUFDbnZCLElBQWIsQ0FBa0IsaUJBQWxCLEVBQXFDekQsV0FBckMsQ0FBaUQsUUFBakQ7QUFBMkR2QixnQkFBTSxDQUFDLElBQUQsQ0FBTixDQUFhbUUsUUFBYixDQUFzQixRQUF0QjtBQUFnQzZsQix1QkFBYSxHQUFDaHFCLE1BQU0sQ0FBQyxJQUFELENBQU4sQ0FBYXdELElBQWIsQ0FBa0IsVUFBbEIsQ0FBZDtBQUE0Q2l3QixnQkFBTSxHQUFDenpCLE1BQU0sQ0FBQyxJQUFELENBQU4sQ0FBYXdELElBQWIsQ0FBa0IsV0FBbEIsQ0FBUDtBQUFzQyxjQUFJeW1CLE9BQU8sR0FBQ2tLLFlBQVksQ0FBQ252QixJQUFiLENBQWtCLG9DQUFsQixDQUFaO0FBQW9FaWxCLGlCQUFPLENBQUNDLE1BQVIsQ0FBZSxHQUFmLEVBQW1CLENBQW5CO0FBQXNCRCxpQkFBTyxDQUFDamxCLElBQVIsQ0FBYSxpQ0FBYixFQUFnRC9ELEdBQWhELENBQW9EO0FBQUNrVCxtQkFBTyxFQUFDLENBQVQ7QUFBV2dXLG1CQUFPLEVBQUMsTUFBbkI7QUFBMEJsYixxQkFBUyxFQUFDO0FBQXBDLFdBQXBEO0FBQXVHZ2IsaUJBQU8sQ0FBQ2psQixJQUFSLENBQWEsaUNBQWIsRUFBZ0RvbEIsR0FBaEQsQ0FBb0QsTUFBSUosYUFBeEQsRUFBdUVLLE9BQXZFLEdBQWlGOW9CLFdBQWpGLENBQTZGLFdBQTdGO0FBQTBHc0gsb0JBQVUsQ0FBQyxZQUFVO0FBQUM3SSxrQkFBTSxDQUFDLE1BQUlncUIsYUFBTCxDQUFOLENBQTBCN2xCLFFBQTFCLENBQW1DLFdBQW5DO0FBQWdEbkUsa0JBQU0sQ0FBQyxNQUFJZ3FCLGFBQUwsQ0FBTixDQUEwQi9vQixHQUExQixDQUE4QjtBQUFDa1QscUJBQU8sRUFBQyxDQUFUO0FBQVdnVyxxQkFBTyxFQUFDLE9BQW5CO0FBQTJCbGIsdUJBQVMsRUFBQztBQUFyQyxhQUE5QjtBQUFrRmdiLG1CQUFPLENBQUNqbEIsSUFBUixDQUFhLHNDQUFiLEVBQXFEekQsV0FBckQsQ0FBaUUsTUFBakU7QUFBeUUsZ0JBQUlteUIsS0FBSyxHQUFDekosT0FBTyxDQUFDamxCLElBQVIsQ0FBYSwyQ0FBYixFQUEwRHlILE1BQXBFO0FBQTJFd2QsbUJBQU8sQ0FBQ2psQixJQUFSLENBQWEsMkNBQWIsRUFBMEQ5RCxJQUExRCxDQUErRCxVQUFTOEksS0FBVCxFQUFlO0FBQUMsa0JBQUkycEIsU0FBUyxHQUFDanlCLFFBQVEsQ0FBQ3NJLEtBQUssR0FBQyxDQUFQLENBQXRCOztBQUFnQyxrQkFBRzJwQixTQUFTLEdBQUNGLE1BQVYsSUFBa0IsQ0FBckIsRUFDcHNDO0FBQUN6ekIsc0JBQU0sQ0FBQyxJQUFELENBQU4sQ0FBYW1FLFFBQWIsQ0FBc0IsTUFBdEI7QUFBK0I7O0FBQ2hDLGtCQUFHd3ZCLFNBQVMsSUFBRUQsS0FBZCxFQUNBO0FBQUM3cUIsMEJBQVUsQ0FBQyxZQUFVO0FBQUNvaEIseUJBQU8sQ0FBQ0MsTUFBUixDQUFlLEdBQWYsRUFBbUIsQ0FBbkI7QUFBdUIsaUJBQW5DLEVBQW9DLEdBQXBDLENBQVY7QUFBb0Q7QUFBQyxhQUgraEM7QUFHNWhDLFdBSDB2QixFQUd6dkIsR0FIeXZCLENBQVY7QUFHenVCLFNBSHNOO0FBR25OLE9BSDhGO0FBRzVGbHFCLFlBQU0sQ0FBQyxtRUFBRCxDQUFOLENBQTRFa0IsSUFBNUUsQ0FBaUYsWUFBVTtBQUFDLFlBQUlrekIsS0FBSyxHQUFDLEtBQUtDLHNCQUFMLENBQTRCLFlBQTVCLEVBQTBDLENBQTFDLENBQVY7QUFBdUQsWUFBSUMsRUFBRSxHQUFDdDBCLE1BQU0sQ0FBQ28wQixLQUFELENBQU4sQ0FBYy94QixJQUFkLENBQW1CLFVBQW5CLENBQVA7QUFBc0MsWUFBSWt5QixJQUFJLEdBQUN4ekIsUUFBUSxDQUFDOGlCLGFBQVQsQ0FBdUIsS0FBdkIsQ0FBVDtBQUF1QyxZQUFJMlEsT0FBTyxHQUFDenpCLFFBQVEsQ0FBQzhpQixhQUFULENBQXVCLEtBQXZCLENBQVo7QUFBMEMsWUFBSTRRLElBQUksR0FBQzF6QixRQUFRLENBQUM4aUIsYUFBVCxDQUF1QixLQUF2QixDQUFUO0FBQXVDLFlBQUk2USxJQUFJLEdBQUMzekIsUUFBUSxDQUFDOGlCLGFBQVQsQ0FBdUIsS0FBdkIsQ0FBVDtBQUF1QyxZQUFJOFEsSUFBSSxHQUFDNXpCLFFBQVEsQ0FBQzhpQixhQUFULENBQXVCLEtBQXZCLENBQVQ7QUFBdUMsWUFBSStRLElBQUksR0FBQzd6QixRQUFRLENBQUM4aUIsYUFBVCxDQUF1QixLQUF2QixDQUFUO0FBQXVDLFlBQUlnUixJQUFJLEdBQUM5ekIsUUFBUSxDQUFDOGlCLGFBQVQsQ0FBdUIsS0FBdkIsQ0FBVDtBQUF1QyxZQUFJaVIsTUFBTSxHQUFDLDJFQUF5RVIsRUFBekUsR0FBNEUsK0VBQXZGO0FBQXVLeFcsZ0JBQVEsQ0FBQ29PLEdBQVQsQ0FBYXFJLElBQWIsRUFBa0I7QUFBQ3R6QixhQUFHLEVBQUM7QUFBQ1UsaUJBQUssRUFBQyxDQUFQO0FBQVNoQixrQkFBTSxFQUFDLENBQWhCO0FBQWtCLDBCQUFhLHdCQUEvQjtBQUF3RCw2QkFBZ0Isd0JBQXhFO0FBQWlHLDJCQUFjLGtCQUEvRztBQUFrSSx1QkFBVSxFQUE1STtBQUErSWdJLG9CQUFRLEVBQUMsVUFBeEo7QUFBbUt0SSxlQUFHLEVBQUMsS0FBdks7QUFBNktrcEIsZ0JBQUksRUFBQyxLQUFsTDtBQUF3THZMLHNCQUFVLEVBQUMsTUFBbk07QUFBME0rVyxxQkFBUyxFQUFDO0FBQXBOO0FBQUwsU0FBbEI7QUFBcVBqWCxnQkFBUSxDQUFDb08sR0FBVCxDQUFhc0ksT0FBYixFQUFxQjtBQUFDdnpCLGFBQUcsRUFBQztBQUFDVSxpQkFBSyxFQUFDLEVBQVA7QUFBVWhCLGtCQUFNLEVBQUMsRUFBakI7QUFBb0JxMEIsa0JBQU0sRUFBQyxpQkFBM0I7QUFBNkMsNkJBQWdCLE9BQTdEO0FBQXFFLHVCQUFVLEVBQS9FO0FBQWtGcnNCLG9CQUFRLEVBQUMsVUFBM0Y7QUFBc0d0SSxlQUFHLEVBQUMsS0FBMUc7QUFBZ0hrcEIsZ0JBQUksRUFBQyxLQUFySDtBQUEySHJVLGFBQUMsRUFBQyxNQUE3SDtBQUFvSTNGLGFBQUMsRUFBQztBQUF0STtBQUFMLFNBQXJCO0FBQTBLdU8sZ0JBQVEsQ0FBQ29PLEdBQVQsQ0FBYXVJLElBQWIsRUFBa0I7QUFBQ3h6QixhQUFHLEVBQUM7QUFBQ1UsaUJBQUssRUFBQyxFQUFQO0FBQVVoQixrQkFBTSxFQUFDLEVBQWpCO0FBQW9CcTBCLGtCQUFNLEVBQUMsaUJBQTNCO0FBQTZDLDZCQUFnQixPQUE3RDtBQUFxRSx1QkFBVSxFQUEvRTtBQUFrRnJzQixvQkFBUSxFQUFDLFVBQTNGO0FBQXNHdEksZUFBRyxFQUFDLEtBQTFHO0FBQWdIa3BCLGdCQUFJLEVBQUMsS0FBckg7QUFBMkhyVSxhQUFDLEVBQUMsTUFBN0g7QUFBb0kzRixhQUFDLEVBQUM7QUFBdEk7QUFBTCxTQUFsQjtBQUF1S3VPLGdCQUFRLENBQUNvTyxHQUFULENBQWF3SSxJQUFiLEVBQWtCO0FBQUN6ekIsYUFBRyxFQUFDO0FBQUNVLGlCQUFLLEVBQUMsRUFBUDtBQUFVd1MsbUJBQU8sRUFBQyxDQUFsQjtBQUFvQnhULGtCQUFNLEVBQUMsRUFBM0I7QUFBOEIsZ0NBQW1CLE9BQWpEO0FBQXlELDZCQUFnQixPQUF6RTtBQUFpRix1QkFBVSxFQUEzRjtBQUE4RmdJLG9CQUFRLEVBQUMsVUFBdkc7QUFBa0h0SSxlQUFHLEVBQUMsS0FBdEg7QUFBNEhrcEIsZ0JBQUksRUFBQyxLQUFqSTtBQUF1SXJVLGFBQUMsRUFBQyxNQUF6STtBQUFnSjNGLGFBQUMsRUFBQztBQUFsSjtBQUFMLFNBQWxCO0FBQW1MdU8sZ0JBQVEsQ0FBQ29PLEdBQVQsQ0FBYXlJLElBQWIsRUFBa0I7QUFBQzF6QixhQUFHLEVBQUM7QUFBQ1UsaUJBQUssRUFBQyxHQUFQO0FBQVdoQixrQkFBTSxFQUFDLEdBQWxCO0FBQXNCa1osaUJBQUssRUFBQyxDQUE1QjtBQUE4QjFGLG1CQUFPLEVBQUMsQ0FBdEM7QUFBd0M4Z0IsMkJBQWUsRUFBQyxNQUF4RDtBQUErRCw2QkFBZ0IsT0FBL0U7QUFBdUYsdUJBQVUsRUFBakc7QUFBb0d0c0Isb0JBQVEsRUFBQyxVQUE3RztBQUF3SHRJLGVBQUcsRUFBQyxLQUE1SDtBQUFrSWtwQixnQkFBSSxFQUFDLEtBQXZJO0FBQTZJclUsYUFBQyxFQUFDLE1BQS9JO0FBQXNKM0YsYUFBQyxFQUFDO0FBQXhKO0FBQUwsU0FBbEI7QUFBeUx1TyxnQkFBUSxDQUFDb08sR0FBVCxDQUFhMEksSUFBYixFQUFrQjtBQUFDM3pCLGFBQUcsRUFBQztBQUFDVSxpQkFBSyxFQUFDLE1BQVA7QUFBY2hCLGtCQUFNLEVBQUMsTUFBckI7QUFBNEJ3VCxtQkFBTyxFQUFDLENBQXBDO0FBQXNDOGdCLDJCQUFlLEVBQUMsTUFBdEQ7QUFBNkQsdUJBQVUsRUFBdkU7QUFBMEV0c0Isb0JBQVEsRUFBQyxVQUFuRjtBQUE4RnRJLGVBQUcsRUFBQyxHQUFsRztBQUFzRzRyQixpQkFBSyxFQUFDO0FBQTVHO0FBQUwsU0FBbEI7QUFBMEluTyxnQkFBUSxDQUFDb08sR0FBVCxDQUFhMkksSUFBYixFQUFrQjtBQUFDNXpCLGFBQUcsRUFBQztBQUFDVSxpQkFBSyxFQUFDLE1BQVA7QUFBY2hCLGtCQUFNLEVBQUMsTUFBckI7QUFBNEJ1MEIscUJBQVMsRUFBQyxDQUF0QztBQUF3Qyx1QkFBVSxFQUFsRDtBQUFxRHZzQixvQkFBUSxFQUFDLFVBQTlEO0FBQXlFdEksZUFBRyxFQUFDLEdBQTdFO0FBQWlGa3BCLGdCQUFJLEVBQUM7QUFBdEY7QUFBTCxTQUFsQjtBQUFvSDZLLGFBQUssQ0FBQzlELFdBQU4sQ0FBa0JpRSxJQUFsQjtBQUF3QkgsYUFBSyxDQUFDOUQsV0FBTixDQUFrQmtFLE9BQWxCO0FBQTJCSixhQUFLLENBQUM5RCxXQUFOLENBQWtCbUUsSUFBbEI7QUFBd0JMLGFBQUssQ0FBQzlELFdBQU4sQ0FBa0JvRSxJQUFsQjtBQUF3Qk4sYUFBSyxDQUFDOUQsV0FBTixDQUFrQnFFLElBQWxCO0FBQXdCUCxhQUFLLENBQUM5RCxXQUFOLENBQWtCc0UsSUFBbEI7QUFBd0I1MEIsY0FBTSxDQUFDNjBCLElBQUQsQ0FBTixDQUFhN1UsSUFBYixDQUFrQjhVLE1BQWxCO0FBQTBCVixhQUFLLENBQUM5RCxXQUFOLENBQWtCdUUsSUFBbEI7QUFBd0I3MEIsY0FBTSxDQUFDbzBCLEtBQUQsQ0FBTixDQUFjdnpCLEVBQWQsQ0FBaUIsWUFBakIsRUFBOEIsVUFBUytULEtBQVQsRUFBZTtBQUFDdWdCLG1CQUFTLENBQUNDLFlBQVYsQ0FBdUIsQ0FBQ1YsSUFBRCxFQUFNRixPQUFOLEVBQWNDLElBQWQsQ0FBdkI7QUFBNENVLG1CQUFTLENBQUNwWCxFQUFWLENBQWEyVyxJQUFiLEVBQWtCLEdBQWxCLEVBQXNCO0FBQUN2Z0IsbUJBQU8sRUFBQyxHQUFUO0FBQWE2SyxnQkFBSSxFQUFDO0FBQWxCLFdBQXRCO0FBQWdGbVcsbUJBQVMsQ0FBQ3BYLEVBQVYsQ0FBYXlXLE9BQWIsRUFBcUIsR0FBckIsRUFBeUI7QUFBQzNhLGlCQUFLLEVBQUMsR0FBUDtBQUFXbUYsZ0JBQUksRUFBQztBQUFoQixXQUF6QjtBQUFpRm1XLG1CQUFTLENBQUNFLE1BQVYsQ0FBaUJaLElBQWpCLEVBQXNCLEdBQXRCLEVBQTBCO0FBQUM1YSxpQkFBSyxFQUFDLENBQVA7QUFBUzFGLG1CQUFPLEVBQUM7QUFBakIsV0FBMUIsRUFBZ0Q7QUFBQzBGLGlCQUFLLEVBQUMsQ0FBUDtBQUFTMUYsbUJBQU8sRUFBQyxDQUFqQjtBQUFtQjZLLGdCQUFJLEVBQUM7QUFBeEIsV0FBaEQ7QUFBK0dtVyxtQkFBUyxDQUFDcFgsRUFBVixDQUFhd1csSUFBYixFQUFrQixHQUFsQixFQUFzQjtBQUFDaGxCLGFBQUMsRUFBQyxDQUFIO0FBQUs0RSxtQkFBTyxFQUFDLENBQWI7QUFBZTZLLGdCQUFJLEVBQUMsMENBQXBCO0FBQStEc00sc0JBQVUsRUFBQyxzQkFBVTtBQUFDNkosdUJBQVMsQ0FBQ2pKLEdBQVYsQ0FBY3FJLElBQWQsRUFBbUI7QUFBQ2hsQixpQkFBQyxFQUFDLENBQUMsRUFBSjtBQUFPK2IsMEJBQVUsRUFBQyxzQkFBVTtBQUFDNkosMkJBQVMsQ0FBQ3BYLEVBQVYsQ0FBYXdXLElBQWIsRUFBa0IsR0FBbEIsRUFBc0I7QUFBQ2hsQixxQkFBQyxFQUFDLENBQUg7QUFBSzRFLDJCQUFPLEVBQUMsQ0FBYjtBQUFlNkssd0JBQUksRUFBQztBQUFwQixtQkFBdEI7QUFBeUY7QUFBdEgsZUFBbkI7QUFBNkk7QUFBbE8sV0FBdEI7QUFBNFAsU0FBdG1CO0FBQXdtQmhmLGNBQU0sQ0FBQ28wQixLQUFELENBQU4sQ0FBY3Z6QixFQUFkLENBQWlCLFlBQWpCLEVBQThCLFVBQVMrVCxLQUFULEVBQWU7QUFBQ3VnQixtQkFBUyxDQUFDQyxZQUFWLENBQXVCLENBQUNWLElBQUQsRUFBTUYsT0FBTixDQUF2QjtBQUF1Q1csbUJBQVMsQ0FBQ3BYLEVBQVYsQ0FBYTJXLElBQWIsRUFBa0IsR0FBbEIsRUFBc0I7QUFBQ3ZnQixtQkFBTyxFQUFDLENBQVQ7QUFBVzZLLGdCQUFJLEVBQUM7QUFBaEIsV0FBdEI7QUFBOEVtVyxtQkFBUyxDQUFDcFgsRUFBVixDQUFheVcsT0FBYixFQUFxQixHQUFyQixFQUF5QjtBQUFDM2EsaUJBQUssRUFBQyxDQUFQO0FBQVNtRixnQkFBSSxFQUFDO0FBQWQsV0FBekI7QUFBZ0YsU0FBblA7QUFBcVBoZixjQUFNLENBQUNvMEIsS0FBRCxDQUFOLENBQWN2ekIsRUFBZCxDQUFpQixPQUFqQixFQUF5QixVQUFTK1QsS0FBVCxFQUFlO0FBQUNBLGVBQUssQ0FBQ3hMLGNBQU47QUFBdUIrckIsbUJBQVMsQ0FBQ0UsTUFBVixDQUFpQlYsSUFBakIsRUFBc0IsSUFBdEIsRUFBMkI7QUFBQ3hnQixtQkFBTyxFQUFDLENBQVQ7QUFBVzBGLGlCQUFLLEVBQUM7QUFBakIsV0FBM0IsRUFBK0M7QUFBQ0EsaUJBQUssRUFBQyxDQUFQO0FBQVNtRixnQkFBSSxFQUFDLDBDQUFkO0FBQXlEc00sc0JBQVUsRUFBQyxzQkFBVTtBQUFDNkosdUJBQVMsQ0FBQ2pKLEdBQVYsQ0FBY3lJLElBQWQsRUFBbUI7QUFBQ3hnQix1QkFBTyxFQUFDLENBQVQ7QUFBVzBGLHFCQUFLLEVBQUMsQ0FBakI7QUFBbUIzTyxxQkFBSyxFQUFDLEdBQXpCO0FBQTZCb2dCLDBCQUFVLEVBQUMsc0JBQVU7QUFBQzZKLDJCQUFTLENBQUNFLE1BQVYsQ0FBaUJULElBQWpCLEVBQXNCLEdBQXRCLEVBQTBCO0FBQUNqekIseUJBQUssRUFBQztBQUFQLG1CQUExQixFQUEwQztBQUFDQSx5QkFBSyxFQUFDLENBQVA7QUFBU3FkLHdCQUFJLEVBQUM7QUFBZCxtQkFBMUM7QUFBdUY7QUFBMUksZUFBbkI7QUFBZ0ttVyx1QkFBUyxDQUFDakosR0FBVixDQUFjMEksSUFBZCxFQUFtQjtBQUFDemdCLHVCQUFPLEVBQUM7QUFBVCxlQUFuQjtBQUFpQ2doQix1QkFBUyxDQUFDakosR0FBVixDQUFjMkksSUFBZCxFQUFtQjtBQUFDSyx5QkFBUyxFQUFDO0FBQVgsZUFBbkI7QUFBb0M7QUFBcFQsV0FBL0M7QUFBdVcsU0FBdmE7QUFBMGEsT0FBaHZHO0FBQW12RyxLQUh0d0c7QUFHd3dHcHpCLHFCQUFpQixDQUFDQyxLQUFsQixDQUF3QkMsU0FBeEIsQ0FBa0Msb0VBQWxDLEVBQXVHLFVBQVNDLE1BQVQsRUFBZ0I7QUFBQ2pDLFlBQU0sQ0FBQyxzQkFBRCxDQUFOLENBQStCa0IsSUFBL0IsQ0FBb0MsWUFBVTtBQUFDLFlBQUl1ZixZQUFZLEdBQUMsQ0FBakI7QUFBbUJBLG9CQUFZLEdBQUMvZSxRQUFRLENBQUMxQixNQUFNLENBQUMsSUFBRCxDQUFOLENBQWF3RCxJQUFiLENBQWtCLGNBQWxCLElBQWtDLENBQW5DLENBQXJCO0FBQTJELFlBQUk4eEIsZUFBZSxHQUFDLElBQUlqcUIsTUFBSixDQUFXckwsTUFBTSxDQUFDLElBQUQsQ0FBakIsRUFBd0I7QUFBQzRMLG9CQUFVLEVBQUMsSUFBWjtBQUFpQjJwQix3QkFBYyxFQUFDLElBQWhDO0FBQXFDQyx1QkFBYSxFQUFDLE1BQW5EO0FBQTBEL3BCLHNCQUFZLEVBQUMsRUFBdkU7QUFBMEVtVixrQkFBUSxFQUFDO0FBQUNDLG1CQUFPLEVBQUMsSUFBVDtBQUFjQywwQkFBYyxFQUFDO0FBQTdCLFdBQW5GO0FBQXdIM1Msb0JBQVUsRUFBQztBQUFDdUMsY0FBRSxFQUFDO0FBQUosV0FBbkk7QUFBOEorUCxzQkFBWSxFQUFDQTtBQUEzSyxTQUF4QixDQUFwQjtBQUF1TyxPQUFwVztBQUF1VyxLQUEvZDtBQUFpZTNlLHFCQUFpQixDQUFDQyxLQUFsQixDQUF3QkMsU0FBeEIsQ0FBa0MsaUVBQWxDLEVBQW9HLFVBQVNDLE1BQVQsRUFBZ0I7QUFBQSxVQUFPa3ZCLEtBQVA7QUFBYSx1QkFBWXpnQixFQUFaLEVBQWU7QUFBQTs7QUFBQyxlQUFLQyxHQUFMLEdBQVM7QUFBQ0QsY0FBRSxFQUFDQTtBQUFKLFdBQVQ7QUFBaUIsZUFBS0MsR0FBTCxDQUFTOGtCLFFBQVQsR0FBa0IsS0FBSzlrQixHQUFMLENBQVNELEVBQVQsQ0FBWWUsYUFBWixDQUEwQixZQUExQixDQUFsQjtBQUEwRCxlQUFLaWtCLE9BQUwsR0FBYSxLQUFLL2tCLEdBQUwsQ0FBUzhrQixRQUFULENBQWtCMW1CLEtBQWxCLENBQXdCNG1CLGVBQXJDO0FBQXFELGVBQUtDLE1BQUw7QUFBZTs7QUFBNUs7QUFBQTtBQUFBLG1DQUN2NEg7QUFBQyxpQkFBS2psQixHQUFMLENBQVM4a0IsUUFBVCxDQUFrQmpqQixTQUFsQixHQUE0QiwwREFBbUQsS0FBSzdCLEdBQUwsQ0FBUzhrQixRQUFULENBQWtCMW1CLEtBQWxCLENBQXdCNG1CLGVBQTNFLGVBQXNHRSxNQUF0RyxDQUE2RyxDQUE3RyxDQUE1QjtBQUE0SSxpQkFBS2xsQixHQUFMLENBQVNtbEIsVUFBVCxHQUFvQjFrQixLQUFLLENBQUNDLElBQU4sQ0FBVyxLQUFLVixHQUFMLENBQVM4a0IsUUFBVCxDQUFrQm5rQixnQkFBbEIsQ0FBbUMsYUFBbkMsQ0FBWCxDQUFwQjtBQUFtRjtBQUR1cUg7QUFBQTtBQUFBLHdDQUVqNEh5a0IsT0FGaTRILEVBRTMySDtBQUFBOztBQUFBLGdCQUFkQyxHQUFjLHVFQUFWLENBQVU7QUFBQSxnQkFBUjlxQixLQUFRLHVFQUFGLENBQUU7QUFBQ3JDLHNCQUFVLENBQUM7QUFBQSxxQkFBSSxPQUFJLENBQUM4SCxHQUFMLENBQVNtbEIsVUFBVCxDQUFvQkUsR0FBcEIsRUFBeUJqbkIsS0FBekIsQ0FBK0I0bUIsZUFBL0IsR0FBK0NJLE9BQW5EO0FBQUEsYUFBRCxFQUE0RDdxQixLQUE1RCxDQUFWO0FBQThFO0FBRjR4SDs7QUFBQTtBQUFBOztBQUFBLFVBR3o0SCtxQixlQUh5NEg7QUFHejNILGlDQUFZdmxCLEVBQVosRUFBZTtBQUFBOztBQUFBOztBQUFDLGVBQUtDLEdBQUwsR0FBUztBQUFDRCxjQUFFLEVBQUNBO0FBQUosV0FBVDtBQUFpQixlQUFLQyxHQUFMLENBQVN4QixNQUFULEdBQWdCaUMsS0FBSyxDQUFDQyxJQUFOLENBQVcsS0FBS1YsR0FBTCxDQUFTRCxFQUFULENBQVlZLGdCQUFaLENBQTZCLFFBQTdCLENBQVgsQ0FBaEI7QUFBbUUsZUFBS0MsV0FBTCxHQUFpQixLQUFLWixHQUFMLENBQVN4QixNQUFULENBQWdCMUMsTUFBakM7QUFBd0MsZUFBSzBDLE1BQUwsR0FBWSxFQUFaO0FBQWUsZUFBS3dCLEdBQUwsQ0FBU3hCLE1BQVQsQ0FBZ0JDLE9BQWhCLENBQXdCLFVBQUFDLEtBQUs7QUFBQSxtQkFBRSxPQUFJLENBQUNGLE1BQUwsQ0FBWWhNLElBQVosQ0FBaUIsSUFBSWd1QixLQUFKLENBQVU5aEIsS0FBVixDQUFqQixDQUFGO0FBQUEsV0FBN0I7QUFBbUUsZUFBS3VDLE9BQUwsR0FBYSxDQUFiO0FBQWUsZUFBS3NrQixVQUFMLEdBQWdCLElBQWhCO0FBQXFCLGVBQUtDLGlCQUFMLEdBQXVCLENBQXZCO0FBQTBCOztBQUg2bEg7QUFBQTtBQUFBLGlDQUl4NEhDLFNBSnc0SCxFQUk5M0hDLE9BSjgzSCxFQUl0M0g7QUFBQTs7QUFBQyxtQkFBTyxJQUFJOWlCLE9BQUosQ0FBWSxVQUFDQyxPQUFELEVBQVNDLE1BQVQsRUFBa0I7QUFBQzJpQix1QkFBUyxDQUFDemxCLEdBQVYsQ0FBYzhrQixRQUFkLENBQXVCNWhCLFNBQXZCLENBQWlDRyxHQUFqQyxDQUFxQyxpQkFBckM7QUFBd0Qsa0JBQU1zaUIsZ0JBQWdCLEdBQUNGLFNBQVMsQ0FBQ1YsT0FBakM7QUFBeUMsa0JBQU1hLGNBQWMsR0FBQ0YsT0FBTyxDQUFDWCxPQUE3Qjs7QUFBcUMsbUJBQUksSUFBSTd3QixFQUFDLEdBQUMsT0FBSSxDQUFDc3hCLGlCQUFMLEdBQXVCLENBQWpDLEVBQW1DdHhCLEVBQUMsSUFBRSxDQUF0QyxFQUF3QyxFQUFFQSxFQUExQyxFQUE0QztBQUFDdXhCLHlCQUFTLENBQUNJLGFBQVYsQ0FBd0JELGNBQXhCLEVBQXVDMXhCLEVBQXZDLEVBQXlDLE9BQUksQ0FBQ3F4QixVQUFMLElBQWlCLE9BQUksQ0FBQ0MsaUJBQUwsR0FBdUIsQ0FBeEMsS0FBNEMsT0FBSSxDQUFDQSxpQkFBTCxHQUF1QnR4QixFQUF2QixHQUF5QixDQUFyRSxJQUF3RSxPQUFJLENBQUNxeEIsVUFBTCxJQUFpQixPQUFJLENBQUNDLGlCQUFMLEdBQXVCLENBQXhDLENBQWpIO0FBQThKOztBQUNqWnR0Qix3QkFBVSxDQUFDLFlBQUk7QUFBQ3V0Qix5QkFBUyxDQUFDemxCLEdBQVYsQ0FBYzhrQixRQUFkLENBQXVCNWhCLFNBQXZCLENBQWlDQyxNQUFqQyxDQUF3QyxpQkFBeEM7O0FBQTJELHFCQUFJLElBQUlqUCxHQUFDLEdBQUMsT0FBSSxDQUFDc3hCLGlCQUFMLEdBQXVCLENBQWpDLEVBQW1DdHhCLEdBQUMsSUFBRSxDQUF0QyxFQUF3QyxFQUFFQSxHQUExQyxFQUE0QztBQUFDdXhCLDJCQUFTLENBQUNJLGFBQVYsQ0FBd0JGLGdCQUF4QixFQUF5Q3p4QixHQUF6QyxFQUEyQyxDQUEzQztBQUErQzs7QUFDdksyTyx1QkFBTztBQUFJLGVBREQsRUFDRSxPQUFJLENBQUMwaUIsVUFEUCxDQUFWO0FBQzhCLGFBRkcsQ0FBUDtBQUVPO0FBTjgySDtBQUFBO0FBQUEsbUNBT3Q0SGpqQixHQVBzNEgsRUFPbDRIO0FBQUE7O0FBQUMsZ0JBQUcsS0FBS1AsV0FBUixFQUFvQjtBQUFPLGlCQUFLQSxXQUFMLEdBQWlCLElBQWpCO0FBQXNCLGdCQUFNNGMsVUFBVSxHQUFDcmMsR0FBRyxLQUFHLE1BQU4sR0FBYSxLQUFLckIsT0FBTCxHQUFhLEtBQUtMLFdBQUwsR0FBaUIsQ0FBOUIsR0FBZ0MsS0FBS0ssT0FBTCxHQUFhLENBQTdDLEdBQStDLENBQTVELEdBQThELEtBQUtBLE9BQUwsR0FBYSxDQUFiLEdBQWUsS0FBS0EsT0FBTCxHQUFhLENBQTVCLEdBQThCLEtBQUtMLFdBQUwsR0FBaUIsQ0FBOUg7QUFBZ0ksaUJBQUtrbEIsTUFBTCxDQUFZLEtBQUt0bkIsTUFBTCxDQUFZLEtBQUt5QyxPQUFqQixDQUFaLEVBQXNDLEtBQUt6QyxNQUFMLENBQVltZ0IsVUFBWixDQUF0QyxFQUErRGhiLElBQS9ELENBQW9FLFlBQUk7QUFBQyxxQkFBSSxDQUFDM0QsR0FBTCxDQUFTeEIsTUFBVCxDQUFnQixPQUFJLENBQUN5QyxPQUFyQixFQUE4QmlDLFNBQTlCLENBQXdDQyxNQUF4QyxDQUErQyxlQUEvQzs7QUFBZ0UscUJBQUksQ0FBQ2xDLE9BQUwsR0FBYTBkLFVBQWI7O0FBQXdCLHFCQUFJLENBQUMzZSxHQUFMLENBQVN4QixNQUFULENBQWdCLE9BQUksQ0FBQ3lDLE9BQXJCLEVBQThCaUMsU0FBOUIsQ0FBd0NHLEdBQXhDLENBQTRDLGVBQTVDOztBQUE2RCxxQkFBSSxDQUFDdEIsV0FBTCxHQUFpQixLQUFqQjtBQUF3QixhQUF0UDtBQUF5UDtBQVB1OUc7O0FBQUE7QUFBQTs7QUFRLzRIaEosa0JBQVksQ0FBQzNJLFFBQVEsQ0FBQ3VRLGdCQUFULENBQTBCLFlBQTFCLENBQUQsRUFBeUM7QUFBQ2tELGtCQUFVLEVBQUM7QUFBWixPQUF6QyxFQUEyRCxZQUFJO0FBQUN6VCxnQkFBUSxDQUFDaVUsSUFBVCxDQUFjbkIsU0FBZCxDQUF3QkMsTUFBeEIsQ0FBK0IsU0FBL0I7QUFBMEMsWUFBTS9JLFNBQVMsR0FBQyxJQUFJa3JCLGVBQUosQ0FBb0JsMUIsUUFBUSxDQUFDMFEsYUFBVCxDQUF1QixTQUF2QixDQUFwQixDQUFoQjtBQUF1RSxZQUFNRCxHQUFHLEdBQUNKLEtBQUssQ0FBQ0MsSUFBTixDQUFXdFEsUUFBUSxDQUFDdVEsZ0JBQVQsQ0FBMEIsbUJBQTFCLENBQVgsQ0FBVjtBQUFxRUUsV0FBRyxDQUFDLENBQUQsQ0FBSCxDQUFPbkUsZ0JBQVAsQ0FBd0IsT0FBeEIsRUFBZ0M7QUFBQSxpQkFBSXRDLFNBQVMsQ0FBQzZILFFBQVYsQ0FBbUIsTUFBbkIsQ0FBSjtBQUFBLFNBQWhDO0FBQWdFcEIsV0FBRyxDQUFDLENBQUQsQ0FBSCxDQUFPbkUsZ0JBQVAsQ0FBd0IsT0FBeEIsRUFBZ0M7QUFBQSxpQkFBSXRDLFNBQVMsQ0FBQzZILFFBQVYsQ0FBbUIsTUFBbkIsQ0FBSjtBQUFBLFNBQWhDO0FBQWlFLE9BQXZYLENBQVo7QUFBc1ksS0FScTVHO0FBUW41RzlRLHFCQUFpQixDQUFDQyxLQUFsQixDQUF3QkMsU0FBeEIsQ0FBa0MsNkRBQWxDLEVBQWdHLFVBQVNDLE1BQVQsRUFBZ0I7QUFBQ2pDLFlBQU0sQ0FBQywrQkFBRCxDQUFOLENBQXdDa0IsSUFBeEMsQ0FBNkMsWUFBVTtBQUFDLFlBQUlpekIsWUFBWSxHQUFDbjBCLE1BQU0sQ0FBQyxJQUFELENBQXZCO0FBQThCLFlBQUkwMkIsU0FBUyxHQUFDLENBQWQ7QUFBZ0IsWUFBSUMsS0FBSyxHQUFDLElBQUlDLEtBQUosRUFBVjtBQUFzQixZQUFJQyxNQUFNLEdBQUM3MkIsTUFBTSxDQUFDMjJCLEtBQUQsQ0FBakI7QUFBeUIsWUFBSUcsVUFBVSxHQUFDLEtBQWY7QUFBcUIsWUFBSUMsTUFBTSxHQUFDLENBQVg7QUFBYSxZQUFJQyxVQUFVLEdBQUM3QyxZQUFZLENBQUMzd0IsSUFBYixDQUFrQixlQUFsQixDQUFmO0FBQWtELFlBQUl5ekIsUUFBUSxHQUFDeHpCLElBQUksQ0FBQ0MsS0FBTCxDQUFXMUQsTUFBTSxDQUFDLE1BQUlnM0IsVUFBTCxDQUFOLENBQXVCN3NCLEdBQXZCLEVBQVgsQ0FBYjtBQUFzRCxZQUFNK3NCLE9BQU8sR0FBQy9DLFlBQVksQ0FBQ252QixJQUFiLENBQWtCLFNBQWxCLENBQWQ7QUFBQSxZQUEyQ215QixPQUFPLEdBQUNoRCxZQUFZLENBQUNudkIsSUFBYixDQUFrQixhQUFsQixDQUFuRDtBQUFBLFlBQW9Gb3lCLFdBQVcsR0FBQ2pELFlBQVksQ0FBQ252QixJQUFiLENBQWtCLG9CQUFsQixDQUFoRztBQUFBLFlBQXdJcXlCLE1BQU0sR0FBQ2xELFlBQVksQ0FBQ252QixJQUFiLENBQWtCLGVBQWxCLENBQS9JO0FBQUEsWUFBa0xzeUIsT0FBTyxHQUFDbkQsWUFBWSxDQUFDbnZCLElBQWIsQ0FBa0IsZ0JBQWxCLENBQTFMO0FBQUEsWUFBOE51eUIsS0FBSyxHQUFDcEQsWUFBWSxDQUFDbnZCLElBQWIsQ0FBa0Isd0JBQWxCLENBQXBPO0FBQUEsWUFBZ1J3eUIsS0FBSyxHQUFDckQsWUFBWSxDQUFDbnZCLElBQWIsQ0FBa0Isd0JBQWxCLENBQXRSO0FBQUEsWUFBa1V5eUIsS0FBSyxHQUFDdEQsWUFBWSxDQUFDbnZCLElBQWIsQ0FBa0Isd0JBQWxCLENBQXhVO0FBQUEsWUFBb1gweUIsU0FBUyxHQUFDdkQsWUFBWSxDQUFDbnZCLElBQWIsQ0FBa0Isa0JBQWxCLENBQTlYO0FBQUEsWUFBb2EyeUIsT0FBTyxHQUFDeEQsWUFBWSxDQUFDbnZCLElBQWIsQ0FBa0IseUJBQWxCLENBQTVhO0FBQUEsWUFBeWQ0eUIsS0FBSyxHQUFDekQsWUFBWSxDQUFDbnZCLElBQWIsQ0FBa0Isd0JBQWxCLENBQS9kO0FBQUEsWUFBMmdCNnlCLEtBQUssR0FBQzFELFlBQVksQ0FBQ252QixJQUFiLENBQWtCLHNCQUFsQixDQUFqaEI7QUFBQSxZQUEyakI4eUIsSUFBSSxHQUFDM0QsWUFBWSxDQUFDbnZCLElBQWIsQ0FBa0Isd0JBQWxCLENBQWhrQjs7QUFBNG1CLGlCQUFTK3lCLElBQVQsQ0FBY0MsT0FBZCxFQUFzQjtBQUFDQSxpQkFBTyxHQUFDN2IsSUFBSSxDQUFDOGIsS0FBTCxDQUFXRCxPQUFYLENBQVI7QUFBNEIsY0FBTUUsSUFBSSxHQUFDL2IsSUFBSSxDQUFDOGIsS0FBTCxDQUFXRCxPQUFPLEdBQUMsRUFBbkIsQ0FBWDtBQUFrQyxjQUFNRyxJQUFJLEdBQUNILE9BQU8sR0FBQyxFQUFuQjtBQUFzQixpQkFBT0UsSUFBSSxHQUFDLEdBQUwsSUFBVUMsSUFBSSxHQUFDLEVBQUwsR0FBUSxHQUFSLEdBQVksRUFBdEIsSUFBMEJBLElBQWpDO0FBQXVDOztBQUN0aEQsaUJBQVNDLFVBQVQsQ0FBb0JoNEIsTUFBcEIsRUFBMkJtMEIsSUFBM0IsRUFBZ0M7QUFBQ21DLG1CQUFTLElBQUV0MkIsTUFBWDtBQUFrQixjQUFHczJCLFNBQVMsR0FBQ08sUUFBUSxDQUFDeHFCLE1BQVQsR0FBZ0IsQ0FBN0IsRUFBK0JpcUIsU0FBUyxHQUFDLENBQVY7QUFBWSxjQUFHQSxTQUFTLEdBQUMsQ0FBYixFQUFlQSxTQUFTLEdBQUNPLFFBQVEsQ0FBQ3hxQixNQUFULEdBQWdCLENBQTFCO0FBQTRCb3FCLGdCQUFNLENBQUNyekIsSUFBUCxDQUFZLEtBQVosRUFBa0J5ekIsUUFBUSxDQUFDUCxTQUFELENBQVIsQ0FBb0IyQixHQUF0QztBQUEyQyxjQUFNQyxHQUFHLEdBQUNyQixRQUFRLENBQUNQLFNBQUQsQ0FBUixDQUFvQjZCLE1BQTlCO0FBQXFDbEIsZ0JBQU0sQ0FBQ21CLElBQVAsQ0FBWXZCLFFBQVEsQ0FBQ1AsU0FBRCxDQUFSLENBQW9CcmUsS0FBaEM7QUFBdUNpZixpQkFBTyxDQUFDa0IsSUFBUixDQUFhdkIsUUFBUSxDQUFDUCxTQUFELENBQVIsQ0FBb0IrQixNQUFqQztBQUF5Q1gsY0FBSSxDQUFDVSxJQUFMLENBQVUsTUFBVjtBQUFrQjNCLGdCQUFNLENBQUNoMkIsRUFBUCxDQUFVLGdCQUFWLEVBQTJCLFlBQVU7QUFBQ2kzQixnQkFBSSxDQUFDVSxJQUFMLENBQVVULElBQUksQ0FBQ3BCLEtBQUssQ0FBQzlsQixRQUFQLENBQWQ7QUFBZ0M2bkIscUJBQVMsQ0FBQyxDQUFELEVBQUcvQixLQUFLLENBQUM5bEIsUUFBVCxDQUFUO0FBQTZCLFdBQW5HO0FBQXFHZ21CLGdCQUFNLENBQUNoMkIsRUFBUCxDQUFVLE9BQVYsRUFBa0I7QUFBQSxtQkFBSXUzQixVQUFVLENBQUMsQ0FBQyxDQUFGLEVBQUksSUFBSixDQUFkO0FBQUEsV0FBbEI7QUFBMkMsY0FBSTlvQixHQUFHLEdBQUMsSUFBSXFwQixLQUFKLEVBQVI7QUFBb0IzNEIsZ0JBQU0sQ0FBQ3NQLEdBQUQsQ0FBTixDQUFZOUwsSUFBWixDQUFpQixLQUFqQixFQUF1QjgwQixHQUF2QixFQUE0QnozQixFQUE1QixDQUErQixNQUEvQixFQUFzQyxZQUFVO0FBQUNzMkIsbUJBQU8sQ0FBQzN6QixJQUFSLENBQWEsS0FBYixFQUFtQjgwQixHQUFuQjtBQUF3QmxCLHVCQUFXLENBQUNuMkIsR0FBWixDQUFnQixrQkFBaEIsRUFBbUMsVUFBUXEzQixHQUFSLEdBQVksSUFBL0M7QUFBc0QsV0FBL0gsRUFBaUl6M0IsRUFBakksQ0FBb0ksT0FBcEksRUFBNEksWUFBVTtBQUFDKzNCLG1CQUFPLENBQUNDLEdBQVIsQ0FBWSxtQkFBWjtBQUFrQyxXQUF6TDs7QUFBMkwsY0FBR3RFLElBQUgsRUFDMXBCO0FBQUNvQyxpQkFBSyxDQUFDcEMsSUFBTjtBQUFha0QsaUJBQUssQ0FBQ3p5QixJQUFOLENBQVcsR0FBWCxFQUFnQnpELFdBQWhCLENBQTRCLFNBQTVCLEVBQXVDNEMsUUFBdkMsQ0FBZ0QsVUFBaEQ7QUFBNkQsV0FEK2tCLE1BRzFwQjtBQUFDc3pCLGlCQUFLLENBQUN6eUIsSUFBTixDQUFXLEdBQVgsRUFBZ0J6RCxXQUFoQixDQUE0QixVQUE1QixFQUF3QzRDLFFBQXhDLENBQWlELFNBQWpEO0FBQTZEO0FBQUM7O0FBQy9ELGlCQUFTdTBCLFNBQVQsQ0FBbUJWLE9BQW5CLEVBQTJCbm5CLFFBQTNCLEVBQW9DO0FBQUMsY0FBTWlvQixPQUFPLEdBQUNkLE9BQU8sR0FBQ25uQixRQUFSLEdBQWlCLEdBQS9CO0FBQW1DZ25CLGVBQUssQ0FBQ1csSUFBTixDQUFXVCxJQUFJLENBQUNDLE9BQUQsQ0FBZjtBQUEwQkwsaUJBQU8sQ0FBQzEyQixHQUFSLENBQVksTUFBWixFQUFtQjYzQixPQUFPLEdBQUMsR0FBM0I7QUFBZ0NsQixlQUFLLENBQUMzMkIsR0FBTixDQUFVLE9BQVYsRUFBa0I2M0IsT0FBTyxHQUFDLEdBQTFCO0FBQWdDOztBQUNsS3JCLGFBQUssQ0FBQ3NCLEtBQU4sQ0FBWSxZQUFVO0FBQUMsY0FBR3BDLEtBQUssQ0FBQ3FDLFdBQU4sR0FBa0IsQ0FBbEIsSUFBcUIsQ0FBQ3JDLEtBQUssQ0FBQ3NDLE1BQS9CLEVBQXNDO0FBQUN0QyxpQkFBSyxDQUFDdUMsS0FBTjtBQUFjekIsaUJBQUssQ0FBQ3p5QixJQUFOLENBQVcsR0FBWCxFQUFnQnpELFdBQWhCLENBQTRCLFVBQTVCLEVBQXdDNEMsUUFBeEMsQ0FBaUQsU0FBakQ7QUFBNkQsV0FBbEgsTUFBc0g7QUFBQ3d5QixpQkFBSyxDQUFDcEMsSUFBTjtBQUFha0QsaUJBQUssQ0FBQ3p5QixJQUFOLENBQVcsR0FBWCxFQUFnQnpELFdBQWhCLENBQTRCLFNBQTVCLEVBQXVDNEMsUUFBdkMsQ0FBZ0QsVUFBaEQ7QUFBNkQ7QUFBQyxTQUF6TjtBQUEyTm96QixhQUFLLENBQUN3QixLQUFOLENBQVk7QUFBQSxpQkFBSVgsVUFBVSxDQUFDLENBQUMsQ0FBRixFQUFJLElBQUosQ0FBZDtBQUFBLFNBQVo7QUFBcUNaLGFBQUssQ0FBQ3VCLEtBQU4sQ0FBWTtBQUFBLGlCQUFJWCxVQUFVLENBQUMsQ0FBQyxDQUFGLEVBQUksSUFBSixDQUFkO0FBQUEsU0FBWjtBQUFxQ3ZCLGNBQU0sQ0FBQ2gyQixFQUFQLENBQVUsWUFBVixFQUF1QixZQUFJO0FBQUM2M0IsbUJBQVMsQ0FBQy9CLEtBQUssQ0FBQ3FDLFdBQVAsRUFBbUJyQyxLQUFLLENBQUM5bEIsUUFBekIsQ0FBVDtBQUE2QyxTQUF6RTtBQUEyRThtQixlQUFPLENBQUM5MkIsRUFBUixDQUFXLFdBQVgsRUFBdUIsWUFBVTtBQUFDaTJCLG9CQUFVLEdBQUMsSUFBWDtBQUFnQkgsZUFBSyxDQUFDdUMsS0FBTjtBQUFlLFNBQWpFO0FBQW1FaEMsZUFBTyxDQUFDcjJCLEVBQVIsQ0FBVyxXQUFYLEVBQXVCLFVBQVNzSSxDQUFULEVBQVc7QUFBQyxjQUFHMnRCLFVBQUgsRUFBYztBQUFDLGdCQUFNbjFCLEtBQUssR0FBQysxQixTQUFTLENBQUMvMUIsS0FBVixFQUFaO0FBQThCLGdCQUFJdzNCLElBQUksR0FBQ3pCLFNBQVMsQ0FBQ3QzQixNQUFWLEdBQW1CbXBCLElBQW5CLEdBQXdCcGdCLENBQUMsQ0FBQzRMLE9BQW5DO0FBQTJDLGdCQUFJK2pCLE9BQU8sR0FBQyxDQUFDSyxJQUFELEdBQU14M0IsS0FBTixHQUFZLEdBQXhCO0FBQTRCbTNCLG1CQUFPLEdBQUVBLE9BQU8sR0FBQyxDQUFSLEdBQVUsQ0FBVixHQUFhQSxPQUFPLEdBQUMsR0FBUixHQUFZLEdBQVosR0FBZ0JBLE9BQXRDO0FBQWdELGdCQUFJZCxPQUFPLEdBQUNjLE9BQU8sR0FBQyxHQUFSLEdBQVluQyxLQUFLLENBQUM5bEIsUUFBOUI7QUFBdUM2bkIscUJBQVMsQ0FBQ1YsT0FBRCxFQUFTckIsS0FBSyxDQUFDOWxCLFFBQWYsQ0FBVDtBQUFrQ2ttQixrQkFBTSxHQUFDaUIsT0FBUDtBQUFnQjtBQUFDLFNBQWpTLEVBQW1TbjNCLEVBQW5TLENBQXNTLG9CQUF0UyxFQUEyVCxZQUFVO0FBQUMsY0FBR2kyQixVQUFILEVBQWM7QUFBQ0Esc0JBQVUsR0FBQyxLQUFYO0FBQWlCSCxpQkFBSyxDQUFDcUMsV0FBTixHQUFrQmpDLE1BQWxCO0FBQXlCVSxpQkFBSyxDQUFDenlCLElBQU4sQ0FBVyxHQUFYLEVBQWdCekQsV0FBaEIsQ0FBNEIsVUFBNUIsRUFBd0M0QyxRQUF4QyxDQUFpRCxTQUFqRDtBQUE2RDtBQUFDLFNBQTdiO0FBQStiaTBCLGtCQUFVLENBQUMsQ0FBRCxFQUFHLEtBQUgsQ0FBVjtBQUFxQixPQU45WTtBQU1pWixLQU5sZ0I7QUFNb2dCdDJCLHFCQUFpQixDQUFDQyxLQUFsQixDQUF3QkMsU0FBeEIsQ0FBa0MsK0VBQWxDLEVBQWtILFVBQVNDLE1BQVQsRUFBZ0I7QUFBQSxVQUFPbTNCLDJCQUFQO0FBQW1DLCtDQUF1QjtBQUFBLGNBQVh4SyxPQUFXLHVFQUFILEVBQUc7O0FBQUE7O0FBQUMsY0FBTXlLLFNBQVMsR0FBQztBQUFDQyxvQkFBUSxFQUFDLHNEQUFWO0FBQWlFQyxpQkFBSyxFQUFDLHFCQUF2RTtBQUE2RnpYLGdCQUFJLEVBQUMsbUJBQWxHO0FBQXNIMFgsb0JBQVEsRUFBQztBQUEvSCxXQUFoQjtBQUF5SyxlQUFLQyxJQUFMLEdBQVUsQ0FBVjtBQUFZLGVBQUtDLFFBQUwsR0FBYzdMLE1BQU0sQ0FBQ2dCLE1BQVAsQ0FBYyxFQUFkLEVBQWlCd0ssU0FBakIsRUFBMkJ6SyxPQUEzQixDQUFkO0FBQWtELGVBQUsrSyxVQUFMO0FBQWtCLGVBQUt4b0IsSUFBTDtBQUFZLGVBQUt5b0IsZUFBTDtBQUF3Qjs7QUFBeFY7QUFBQTtBQUFBLHNDQUNuZ0M7QUFBQyxtQkFBTzc0QixRQUFRLENBQUN1USxnQkFBVCxDQUEwQixLQUFLb29CLFFBQUwsQ0FBY0gsS0FBeEMsQ0FBUDtBQUF1RDtBQUQyOEI7QUFBQTtBQUFBLHlDQUVoZ0M7QUFBQyxtQkFBT3g0QixRQUFRLENBQUN1USxnQkFBVCxDQUEwQixLQUFLb29CLFFBQUwsQ0FBY0YsUUFBeEMsQ0FBUDtBQUEwRDtBQUZxOEI7QUFBQTtBQUFBLG9DQUdyZ0M7QUFBQyxtQkFBT3o0QixRQUFRLENBQUMwUSxhQUFULENBQXVCLEtBQUtpb0IsUUFBTCxDQUFjNVgsSUFBckMsQ0FBUDtBQUFtRDtBQUhpOUI7QUFBQTtBQUFBLHdDQUlqZ0M7QUFBQyxtQkFBTy9nQixRQUFRLENBQUMwUSxhQUFULENBQXVCLEtBQUtpb0IsUUFBTCxDQUFjSixRQUFyQyxDQUFQO0FBQXVEO0FBSnk4QjtBQUFBO0FBQUEsaUNBS3hnQztBQUFDeGIsb0JBQVEsQ0FBQ29PLEdBQVQsQ0FBYSxLQUFLMk4sU0FBTCxFQUFiLEVBQThCO0FBQUMzRSx1QkFBUyxFQUFDLENBQVg7QUFBYXJiLG1CQUFLLEVBQUM7QUFBbkIsYUFBOUI7QUFBd0RpRSxvQkFBUSxDQUFDb08sR0FBVCxDQUFhLEtBQUsyTixTQUFMLEdBQWlCLENBQWpCLENBQWIsRUFBaUM7QUFBQzNFLHVCQUFTLEVBQUMsQ0FBWDtBQUFhcmIsbUJBQUssRUFBQztBQUFuQixhQUFqQztBQUF3RCxpQkFBS2lnQixTQUFMLEdBQWUsS0FBS0MsWUFBTCxHQUFvQnR0QixNQUFwQixHQUEyQixDQUExQztBQUE0QyxpQkFBS3V0QixxQkFBTCxDQUEyQixDQUEzQjtBQUErQjtBQUw0MEI7QUFBQTtBQUFBLHVDQU1sZ0M7QUFBQTs7QUFBQyxnQkFBR2g2QixNQUFNLENBQUNOLE1BQUQsQ0FBTixDQUFlaUMsS0FBZixLQUF1QixJQUExQixFQUNiO0FBQUMsa0JBQU1zNEIsVUFBVSxHQUFDLEtBQUtDLE9BQUwsR0FBZUMsWUFBaEM7QUFBNkMsa0JBQU1DLGNBQWMsR0FBQyxLQUFLQyxXQUFMLEdBQW1CRixZQUF4QztBQUFxRCxrQkFBTUcsV0FBVyxHQUFDLEtBQUtELFdBQUwsR0FBbUJyb0IscUJBQW5CLEVBQWxCO0FBQTZELGtCQUFNdW9CLFlBQVksR0FBQzc0QixRQUFRLENBQUM0NEIsV0FBVyxDQUFDajZCLEdBQWIsQ0FBM0I7QUFBNkMsbUJBQUtnNkIsV0FBTCxHQUFtQmh0QixnQkFBbkIsQ0FBb0MsV0FBcEMsRUFBZ0QsVUFBQXVILEtBQUssRUFBRTtBQUFDLHVCQUFJLENBQUM2a0IsSUFBTCxHQUFVLzNCLFFBQVEsQ0FBQ2tULEtBQUssQ0FBQ0UsS0FBTixHQUFZeWxCLFlBQWIsQ0FBUixHQUFtQyxPQUFJLENBQUNGLFdBQUwsR0FBbUIvUSxTQUFoRTtBQUEwRSxvQkFBSWxwQixNQUFNLEdBQUMsQ0FBQyxPQUFJLENBQUNxNUIsSUFBTixHQUFXVyxjQUFYLEdBQTBCSCxVQUFyQztBQUFnRG5jLHdCQUFRLENBQUNDLEVBQVQsQ0FBWSxPQUFJLENBQUNtYyxPQUFMLEVBQVosRUFBMkIsR0FBM0IsRUFBK0I7QUFBQ2hsQixtQkFBQyxFQUFDOVUsTUFBSDtBQUFVNGUsc0JBQUksRUFBQ3diLE1BQU0sQ0FBQ3RiO0FBQXRCLGlCQUEvQjtBQUFnRSxlQUFsUCxFQUFtUCxLQUFuUDtBQUEyUDtBQUFDO0FBUHFrQjtBQUFBO0FBQUEsNENBUTcvQjtBQUFBOztBQUFBLHVEQUFtQixLQUFLNmEsWUFBTCxFQUFuQjtBQUFBOztBQUFBO0FBQUMsa0VBQXNDO0FBQUEsb0JBQTVCVSxJQUE0QjtBQUFDQSxvQkFBSSxDQUFDcHRCLGdCQUFMLENBQXNCLFlBQXRCLEVBQW1DLFVBQUF5RixFQUFFLEVBQUU7QUFBQyxzQkFBSTRuQixTQUFTLEdBQUM1bkIsRUFBRSxDQUFDNm5CLGFBQUgsQ0FBaUJ6ZCxPQUFqQixDQUF5QjBkLE1BQXZDOztBQUE4Qyx5QkFBSSxDQUFDWixxQkFBTCxDQUEyQlUsU0FBM0I7O0FBQXNDNWMsMEJBQVEsQ0FBQ0MsRUFBVCxDQUFZakwsRUFBRSxDQUFDNm5CLGFBQWYsRUFBNkIsR0FBN0IsRUFBaUM7QUFBQ3pGLDZCQUFTLEVBQUM7QUFBWCxtQkFBakM7QUFBZ0RwWCwwQkFBUSxDQUFDQyxFQUFULENBQVksYUFBWixFQUEwQixHQUExQixFQUE4QjtBQUFDbVgsNkJBQVMsRUFBQyxDQUFYO0FBQWFyYix5QkFBSyxFQUFDO0FBQW5CLG1CQUE5Qjs7QUFBd0Qsc0JBQUcsQ0FBQyxPQUFJLENBQUNnZ0IsU0FBTCxHQUFpQmEsU0FBakIsRUFBNEI3bUIsU0FBNUIsQ0FBc0N5SixRQUF0QyxDQUErQyxZQUEvQyxDQUFKLEVBQWlFO0FBQUMsMkJBQUksQ0FBQ3VjLFNBQUwsR0FBaUJhLFNBQWpCLEVBQTRCN21CLFNBQTVCLENBQXNDRyxHQUF0QyxDQUEwQyxZQUExQztBQUF5RDs7QUFDeFo4SiwwQkFBUSxDQUFDQyxFQUFULENBQVksT0FBSSxDQUFDOGIsU0FBTCxHQUFpQmEsU0FBakIsQ0FBWixFQUF3QyxHQUF4QyxFQUE0QztBQUFDeEYsNkJBQVMsRUFBQyxDQUFYO0FBQWFyYix5QkFBSyxFQUFDO0FBQW5CLG1CQUE1QztBQUFvRSxpQkFEWDtBQUNjO0FBRHREO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDdUQ7QUFUczhCO0FBQUE7QUFBQSxnREFVeC9CeWEsRUFWdy9CLEVBVXIvQjtBQUFDQSxjQUFFLEdBQUM1eUIsUUFBUSxDQUFDNHlCLEVBQUQsQ0FBWDtBQUFnQixnQkFBSXVHLFlBQVksR0FBQyxLQUFLZixTQUFMLEdBQWV4RixFQUFoQztBQUFtQyxnQkFBSXdHLFlBQVksR0FBQ3A1QixRQUFRLENBQUM0eUIsRUFBRCxDQUF6Qjs7QUFBOEIsZ0JBQUd1RyxZQUFZLEdBQUMsQ0FBaEIsRUFBa0I7QUFBQyxtQkFBSSxJQUFJaDJCLEdBQUMsR0FBQyxDQUFWLEVBQVlBLEdBQUMsSUFBRWcyQixZQUFmLEVBQTRCaDJCLEdBQUMsRUFBN0IsRUFBZ0M7QUFBQyxvQkFBSXNQLE9BQU8sR0FBQyxNQUFJdFAsR0FBaEI7QUFBa0Isb0JBQUl6RSxNQUFNLEdBQUMsSUFBRXlFLEdBQWI7QUFBZWlaLHdCQUFRLENBQUNDLEVBQVQsQ0FBWSxLQUFLZ2MsWUFBTCxHQUFvQnpGLEVBQUUsR0FBQ3p2QixHQUF2QixDQUFaLEVBQXNDLEdBQXRDLEVBQTBDO0FBQUNxd0IsMkJBQVMsRUFBQy9nQixPQUFYO0FBQW1CNUUsbUJBQUMsRUFBQ25QLE1BQXJCO0FBQTRCNGUsc0JBQUksRUFBQ29OLE1BQU0sQ0FBQ2xOO0FBQXhDLGlCQUExQztBQUE2RjtBQUFDOztBQUM5UixnQkFBRzRiLFlBQVksR0FBQyxDQUFoQixFQUFrQjtBQUFDLG1CQUFJLElBQUlqMkIsR0FBQyxHQUFDLENBQVYsRUFBWUEsR0FBQyxJQUFFaTJCLFlBQWYsRUFBNEJqMkIsR0FBQyxFQUE3QixFQUFnQztBQUFDLG9CQUFJc1AsUUFBTyxHQUFDLE1BQUl0UCxHQUFoQjs7QUFBa0Isb0JBQUl6RSxPQUFNLEdBQUMsSUFBRXlFLEdBQWI7O0FBQWVpWix3QkFBUSxDQUFDQyxFQUFULENBQVksS0FBS2djLFlBQUwsR0FBb0J6RixFQUFFLEdBQUN6dkIsR0FBdkIsQ0FBWixFQUFzQyxHQUF0QyxFQUEwQztBQUFDcXdCLDJCQUFTLEVBQUMvZ0IsUUFBWDtBQUFtQjVFLG1CQUFDLEVBQUNuUCxPQUFyQjtBQUE0QjRlLHNCQUFJLEVBQUNvTixNQUFNLENBQUNsTjtBQUF4QyxpQkFBMUM7QUFBNkY7QUFBQztBQUFDO0FBWDAxQjs7QUFBQTtBQUFBOztBQVk5Z0MsVUFBSWthLDJCQUFKO0FBQW1DLEtBWnkyQjtBQVl2MkJ0M0IscUJBQWlCLENBQUNDLEtBQWxCLENBQXdCQyxTQUF4QixDQUFrQyw2RUFBbEMsRUFBZ0gsVUFBU0MsTUFBVCxFQUFnQjtBQUFDakMsWUFBTSxDQUFDLHVDQUFELENBQU4sQ0FBZ0RrQixJQUFoRCxDQUFxRCxZQUFVO0FBQUMsWUFBSTY1QixRQUFRLEdBQUMvNkIsTUFBTSxDQUFDLElBQUQsQ0FBTixDQUFhd0QsSUFBYixDQUFrQixJQUFsQixDQUFiO0FBQXFDLFlBQUl3M0IsZ0JBQWdCLEdBQUNqNkIsUUFBUSxDQUFDMFEsYUFBVCxDQUF1QixNQUFJc3BCLFFBQTNCLENBQXJCO0FBQTBELFlBQUlFLFVBQVUsR0FBQ2o3QixNQUFNLENBQUMsSUFBRCxDQUFOLENBQWF3RCxJQUFiLENBQWtCLGlCQUFsQixDQUFmO0FBQW9ELFlBQUkwM0IsU0FBUyxHQUFDLElBQUlDLGNBQUosQ0FBbUI7QUFBQ3pxQixZQUFFLEVBQUNzcUIsZ0JBQUo7QUFBcUJJLGtCQUFRLEVBQUMsYUFBOUI7QUFBNENDLGVBQUssRUFBQyxDQUFDLENBQUQsRUFBR0osVUFBSCxDQUFsRDtBQUFpRUssc0JBQVksRUFBQyxzQkFBU3oyQixDQUFULEVBQVc7QUFBQyxtQkFBTSxNQUFJQSxDQUFWO0FBQWEsV0FBdkc7QUFBd0drSyxlQUFLLEVBQUM7QUFBQ0UscUJBQVMsRUFBQyxDQUFDO0FBQUM0SyxtQkFBSyxFQUFDLENBQUMsR0FBRCxFQUFLLENBQUw7QUFBUCxhQUFELENBQVg7QUFBNkIxRixtQkFBTyxFQUFDLENBQUMsQ0FBRCxFQUFHLENBQUg7QUFBckMsV0FBOUc7QUFBMEpvbkIscUJBQVcsRUFBQztBQUF0SyxTQUFuQixDQUFkO0FBQStNLFlBQUlDLE1BQU0sR0FBQy8zQixJQUFJLENBQUNDLEtBQUwsQ0FBVzFELE1BQU0sQ0FBQyxJQUFELENBQU4sQ0FBYXdELElBQWIsQ0FBa0Isa0JBQWxCLENBQVgsQ0FBWDtBQUE2RCxZQUFJaTRCLFFBQVEsR0FBQyxJQUFJTixjQUFKLENBQW1CO0FBQUN6cUIsWUFBRSxFQUFDc3FCLGdCQUFKO0FBQXFCSSxrQkFBUSxFQUFDLFlBQTlCO0FBQTJDQyxlQUFLLEVBQUMsQ0FBQyxDQUFELEVBQUczNUIsUUFBUSxDQUFDdTVCLFVBQVUsR0FBQyxDQUFaLENBQVgsQ0FBakQ7QUFBNEVLLHNCQUFZLEVBQUMsc0JBQVN6MkIsQ0FBVCxFQUFXO0FBQUMsbUJBQU0sU0FBTzIyQixNQUFNLENBQUMzMkIsQ0FBRCxDQUFiLEdBQWlCLE9BQXZCO0FBQWdDLFdBQXJJO0FBQXNJb2Isa0JBQVEsRUFBQyxJQUEvSTtBQUFvSkYsaUJBQU8sRUFBQyxJQUE1SjtBQUFpS2hSLGVBQUssRUFBQztBQUFDb0YsbUJBQU8sRUFBQyxDQUFDLENBQUQsRUFBRyxDQUFIO0FBQVQsV0FBdks7QUFBdUxvbkIscUJBQVcsRUFBQztBQUFuTSxTQUFuQixDQUFiO0FBQTJPLFlBQUlHLFlBQVksR0FBQ2o0QixJQUFJLENBQUNDLEtBQUwsQ0FBVzFELE1BQU0sQ0FBQyxJQUFELENBQU4sQ0FBYXdELElBQWIsQ0FBa0Isd0JBQWxCLENBQVgsQ0FBakI7QUFBeUUsWUFBSW00QixVQUFVLEdBQUNsNEIsSUFBSSxDQUFDQyxLQUFMLENBQVcxRCxNQUFNLENBQUMsSUFBRCxDQUFOLENBQWF3RCxJQUFiLENBQWtCLHNCQUFsQixDQUFYLENBQWY7QUFBcUUsWUFBSW80QixPQUFPLEdBQUMsSUFBSVQsY0FBSixDQUFtQjtBQUFDenFCLFlBQUUsRUFBQ3NxQixnQkFBSjtBQUFxQkksa0JBQVEsRUFBQyxXQUE5QjtBQUEwQ0MsZUFBSyxFQUFDLENBQUMsQ0FBRCxFQUFHMzVCLFFBQVEsQ0FBQ3U1QixVQUFVLEdBQUMsQ0FBWixDQUFYLENBQWhEO0FBQTJFSyxzQkFBWSxFQUFDLHNCQUFTejJCLENBQVQsRUFBVztBQUFDLG1CQUFNLGVBQWE4MkIsVUFBVSxDQUFDOTJCLENBQUQsQ0FBdkIsR0FBMkIsNkJBQTNCLEdBQXlENjJCLFlBQVksQ0FBQzcyQixDQUFELENBQXJFLEdBQXlFLE1BQS9FO0FBQXVGLFdBQTNMO0FBQTRMb2Isa0JBQVEsRUFBQyxJQUFyTTtBQUEwTXNiLHFCQUFXLEVBQUM7QUFBdE4sU0FBbkIsQ0FBWjtBQUE2UCxZQUFJTSxZQUFZLEdBQUM3N0IsTUFBTSxDQUFDLElBQUQsQ0FBTixDQUFhd0QsSUFBYixDQUFrQixpQkFBbEIsQ0FBakI7QUFBc0QsWUFBSTJLLFVBQVUsR0FBQ3BOLFFBQVEsQ0FBQzBRLGFBQVQsQ0FBdUIsTUFBSW9xQixZQUEzQixDQUFmO0FBQXdELFlBQUlDLGVBQWUsR0FBQyxHQUFHeG1CLEtBQUgsQ0FBU0MsSUFBVCxDQUFjcEgsVUFBVSxDQUFDOE4sUUFBekIsQ0FBcEI7QUFBdUQsWUFBSThmLE1BQU0sR0FBQ3Q0QixJQUFJLENBQUNDLEtBQUwsQ0FBVzFELE1BQU0sQ0FBQyxJQUFELENBQU4sQ0FBYXdELElBQWIsQ0FBa0Isa0JBQWxCLENBQVgsQ0FBWDtBQUE2RCxZQUFJdzRCLFFBQVEsR0FBQyxJQUFJYixjQUFKLENBQW1CO0FBQUN6cUIsWUFBRSxFQUFDc3FCLGdCQUFKO0FBQXFCSSxrQkFBUSxFQUFDLFlBQTlCO0FBQTJDQyxlQUFLLEVBQUMsQ0FBQyxDQUFELEVBQUczNUIsUUFBUSxDQUFDdTVCLFVBQVUsR0FBQyxDQUFaLENBQVgsQ0FBakQ7QUFBNEVLLHNCQUFZLEVBQUMsc0JBQVN6MkIsQ0FBVCxFQUFXO0FBQUMsbUJBQU0sMEdBQXdHazNCLE1BQU0sQ0FBQ2wzQixDQUFELENBQTlHLEdBQWtILG1CQUF4SDtBQUE2SSxXQUFsUDtBQUFtUG8zQixjQUFJLEVBQUMsQ0FBQ2YsU0FBRCxFQUFXTyxRQUFYLEVBQW9CRyxPQUFwQixDQUF4UDtBQUFxUjdzQixlQUFLLEVBQUM7QUFBQywrQkFBa0I7QUFBQ0UsdUJBQVMsRUFBQyxDQUFDO0FBQUM0SyxxQkFBSyxFQUFDLENBQUMsR0FBRCxFQUFLLENBQUw7QUFBUCxlQUFEO0FBQVg7QUFBbkIsV0FBM1I7QUFBNlVxaUIsZ0JBQU0sRUFBQyxnQkFBU0MsUUFBVCxFQUFrQkMsUUFBbEIsRUFBMkI7QUFBQyxnQkFBRyxPQUFPQSxRQUFQLEtBQWtCLFdBQXJCLEVBQWlDO0FBQUNOLDZCQUFlLENBQUNNLFFBQUQsQ0FBZixDQUEwQnZvQixTQUExQixDQUFvQ0MsTUFBcEMsQ0FBMkMseUJBQTNDO0FBQXVFOztBQUN0OURnb0IsMkJBQWUsQ0FBQ0ssUUFBRCxDQUFmLENBQTBCdG9CLFNBQTFCLENBQW9DRyxHQUFwQyxDQUF3Qyx5QkFBeEM7QUFBb0U7QUFEeTdDLFNBQW5CLENBQWI7QUFDdDVDN0Ysa0JBQVUsQ0FBQ2QsZ0JBQVgsQ0FBNEIsT0FBNUIsRUFBb0MsVUFBU2xFLENBQVQsRUFBVztBQUFDLGNBQUdBLENBQUMsQ0FBQ3FHLE1BQUYsQ0FBUzZzQixPQUFULENBQWlCLG9CQUFqQixDQUFILEVBQTBDO0FBQUMsZ0JBQUlyeUIsS0FBSyxHQUFDOHhCLGVBQWUsQ0FBQy83QixPQUFoQixDQUF3Qm9KLENBQUMsQ0FBQ3FHLE1BQUYsQ0FBUzhzQixVQUFqQyxDQUFWO0FBQXVETixvQkFBUSxDQUFDTyxNQUFULENBQWdCdnlCLEtBQWhCO0FBQXdCO0FBQUMsU0FBM0s7QUFBOEssT0FEL0U7QUFDa0YsS0FEbk47QUFDcU5sSSxxQkFBaUIsQ0FBQ0MsS0FBbEIsQ0FBd0JDLFNBQXhCLENBQWtDLHlEQUFsQyxFQUE0RixVQUFTQyxNQUFULEVBQWdCO0FBQUMsVUFBSXU2QixXQUFXLEdBQUN4OEIsTUFBTSxDQUFDLFNBQUQsQ0FBTixDQUFrQnlNLE1BQWxDOztBQUF5QyxXQUFJNUgsQ0FBQyxHQUFDLENBQU4sRUFBUUEsQ0FBQyxHQUFDMjNCLFdBQVYsRUFBc0IzM0IsQ0FBQyxFQUF2QixFQUEwQjtBQUFDLFlBQUk0M0IsVUFBVSxHQUFDejhCLE1BQU0sQ0FBQyxTQUFELENBQU4sQ0FBa0JpSyxFQUFsQixDQUFxQixDQUFDcEYsQ0FBRCxDQUFyQixDQUFmO0FBQXlDLFlBQUk2M0IsV0FBVyxHQUFDMThCLE1BQU0sQ0FBQyxVQUFELENBQU4sQ0FBbUJpSyxFQUFuQixDQUFzQixDQUFDcEYsQ0FBRCxDQUF0QixDQUFoQjtBQUEyQyxZQUFJODNCLFFBQVEsR0FBQ0YsVUFBVSxDQUFDajVCLElBQVgsQ0FBZ0IsWUFBaEIsQ0FBYjtBQUEyQyxZQUFJbzVCLFNBQVMsR0FBQ0YsV0FBVyxDQUFDbDVCLElBQVosQ0FBaUIsWUFBakIsQ0FBZDtBQUE2Q2k1QixrQkFBVSxDQUFDeDdCLEdBQVgsQ0FBZSxrQkFBZixFQUFrQyxTQUFPMDdCLFFBQVAsR0FBZ0IsR0FBbEQ7QUFBdURELG1CQUFXLENBQUN6N0IsR0FBWixDQUFnQixrQkFBaEIsRUFBbUMsU0FBTzI3QixTQUFQLEdBQWlCLEdBQXBEO0FBQTBEOztBQUN4c0I1OEIsWUFBTSxDQUFDLG1CQUFELENBQU4sQ0FBNEJhLEVBQTVCLENBQStCLE9BQS9CLEVBQXVDLFlBQVU7QUFBQ2IsY0FBTSxDQUFDLElBQUQsQ0FBTixDQUFhc0ssT0FBYixDQUFxQixXQUFyQjtBQUFtQyxPQUFyRjtBQUF3RixLQURrSztBQUNoS3hJLHFCQUFpQixDQUFDQyxLQUFsQixDQUF3QkMsU0FBeEIsQ0FBa0MsNERBQWxDLEVBQStGLFVBQVNDLE1BQVQsRUFBZ0I7QUFBQyxVQUFJcXJCLFlBQVksR0FBQyxZQUFVO0FBQUMsaUJBQVNDLGdCQUFULENBQTBCL2QsTUFBMUIsRUFBaUNnZSxLQUFqQyxFQUF1QztBQUFDLGVBQUksSUFBSTNvQixDQUFDLEdBQUMsQ0FBVixFQUFZQSxDQUFDLEdBQUMyb0IsS0FBSyxDQUFDL2dCLE1BQXBCLEVBQTJCNUgsQ0FBQyxFQUE1QixFQUErQjtBQUFDLGdCQUFJNG9CLFVBQVUsR0FBQ0QsS0FBSyxDQUFDM29CLENBQUQsQ0FBcEI7QUFBd0I0b0Isc0JBQVUsQ0FBQ0MsVUFBWCxHQUFzQkQsVUFBVSxDQUFDQyxVQUFYLElBQXVCLEtBQTdDO0FBQW1ERCxzQkFBVSxDQUFDRSxZQUFYLEdBQXdCLElBQXhCO0FBQTZCLGdCQUFHLFdBQVVGLFVBQWIsRUFBd0JBLFVBQVUsQ0FBQ0csUUFBWCxHQUFvQixJQUFwQjtBQUF5QkMsa0JBQU0sQ0FBQ0MsY0FBUCxDQUFzQnRlLE1BQXRCLEVBQTZCaWUsVUFBVSxDQUFDTSxHQUF4QyxFQUE0Q04sVUFBNUM7QUFBeUQ7QUFBQzs7QUFBQSxlQUFPLFVBQVNPLFdBQVQsRUFBcUJDLFVBQXJCLEVBQWdDQyxXQUFoQyxFQUE0QztBQUFDLGNBQUdELFVBQUgsRUFBY1YsZ0JBQWdCLENBQUNTLFdBQVcsQ0FBQzlMLFNBQWIsRUFBdUIrTCxVQUF2QixDQUFoQjtBQUFtRCxjQUFHQyxXQUFILEVBQWVYLGdCQUFnQixDQUFDUyxXQUFELEVBQWFFLFdBQWIsQ0FBaEI7QUFBMEMsaUJBQU9GLFdBQVA7QUFBb0IsU0FBbE07QUFBb00sT0FBMWUsRUFBakI7O0FBQThmLGVBQVNHLGVBQVQsQ0FBeUJDLFFBQXpCLEVBQWtDSixXQUFsQyxFQUE4QztBQUFDLFlBQUcsRUFBRUksUUFBUSxZQUFZSixXQUF0QixDQUFILEVBQXNDO0FBQUMsZ0JBQU0sSUFBSUssU0FBSixDQUFjLG1DQUFkLENBQU47QUFBMEQ7QUFBQzs7QUFDejFCLFVBQUlDLE9BQU8sR0FBQzF0QixDQUFDLENBQUNsQixNQUFELENBQWI7QUFBc0IsVUFBSTZ1QixLQUFLLEdBQUMzdEIsQ0FBQyxDQUFDLE1BQUQsQ0FBWDs7QUFBb0IsVUFBSTZQLFNBQVMsR0FBQyxZQUFVO0FBQUMsaUJBQVNBLFNBQVQsR0FBb0I7QUFBQyxjQUFJNlIsS0FBSyxHQUFDLElBQVY7O0FBQWUsY0FBSWtNLFdBQVcsR0FBQ25lLFNBQVMsQ0FBQzVELE1BQVYsR0FBaUIsQ0FBakIsSUFBb0I0RCxTQUFTLENBQUMsQ0FBRCxDQUFULEtBQWVrSSxTQUFuQyxHQUE2Q2xJLFNBQVMsQ0FBQyxDQUFELENBQXRELEdBQTBELEVBQTFFOztBQUE2RThkLHlCQUFlLENBQUMsSUFBRCxFQUFNMWQsU0FBTixDQUFmOztBQUFnQyxjQUFJdkMsS0FBSyxHQUFDdE4sQ0FBQyxDQUFDLHNCQUFELENBQUQsQ0FBMEI0QyxJQUExQixDQUErQixlQUEvQixDQUFWO0FBQTBELGNBQUl3SSxRQUFRLEdBQUMsSUFBYjs7QUFBa0IsY0FBR2tDLEtBQUssSUFBRSxDQUFWLEVBQ2hTO0FBQUNBLGlCQUFLLEdBQUMsS0FBTjtBQUFZbEMsb0JBQVEsR0FBQyxLQUFUO0FBQWdCOztBQUM3QixjQUFJbUMsVUFBVSxHQUFDdk4sQ0FBQyxDQUFDLHNCQUFELENBQUQsQ0FBMEI0QyxJQUExQixDQUErQixpQkFBL0IsQ0FBZjs7QUFBaUUsY0FBRzJLLFVBQVUsSUFBRSxDQUFmLEVBQ2pFO0FBQUMsZ0JBQUlBLFVBQVUsR0FBQyxLQUFmO0FBQXNCLFdBRDBDLE1BR2pFO0FBQUMsZ0JBQUlBLFVBQVUsR0FBQyxJQUFmO0FBQXFCOztBQUN0QixjQUFJc2dCLGNBQWMsR0FBQztBQUFDeFIsZUFBRyxFQUFDcmMsQ0FBQyxDQUFDLHNCQUFELENBQU47QUFBK0I4dEIsc0JBQVUsRUFBQyxLQUExQztBQUFnREMsMEJBQWMsRUFBQyxLQUEvRDtBQUFxRTlkLG9CQUFRLEVBQUMzQyxLQUE5RTtBQUFvRmxDLG9CQUFRLEVBQUNBO0FBQTdGLFdBQW5CO0FBQTBILGNBQUk0aUIsT0FBTyxHQUFDZixNQUFNLENBQUNnQixNQUFQLENBQWMsRUFBZCxFQUFpQkosY0FBakIsRUFBZ0NELFdBQWhDLENBQVo7QUFBeUQsZUFBS3ZSLEdBQUwsR0FBUzJSLE9BQU8sQ0FBQzNSLEdBQWpCO0FBQXFCLGVBQUs2UixRQUFMLEdBQWMsS0FBSzdSLEdBQUwsQ0FBU2pZLElBQVQsQ0FBY3BFLENBQUMsQ0FBQyx1QkFBRCxDQUFmLEVBQTBDNkwsTUFBeEQ7QUFBK0QsZUFBS2lpQixVQUFMLEdBQWdCLEtBQUtJLFFBQUwsR0FBYyxDQUFkLEdBQWdCRixPQUFPLENBQUNGLFVBQXhCLEdBQW1DLEtBQW5EO0FBQXlELGVBQUtDLGNBQUwsR0FBb0J4Z0IsVUFBcEI7QUFBK0IsZUFBS3VGLFlBQUwsR0FBa0IsQ0FBbEI7QUFBb0IsZUFBS2hCLFdBQUwsR0FBaUIsS0FBakI7QUFBdUIsZUFBSzZILGlCQUFMLEdBQXVCLElBQXZCO0FBQTRCLGVBQUt1RSxhQUFMLEdBQW1COFAsT0FBTyxDQUFDL2QsUUFBM0I7QUFBb0MsZUFBS2tlLFFBQUw7QUFBYyxlQUFLbFMsU0FBTCxHQUFlLEtBQUtJLEdBQUwsQ0FBU2pZLElBQVQsQ0FBYyx3QkFBZCxDQUFmO0FBQXVELGVBQUtnSCxRQUFMLEdBQWMsS0FBSzhpQixRQUFMLEdBQWMsQ0FBZCxHQUFnQkYsT0FBTyxDQUFDNWlCLFFBQXhCLEdBQWlDLEtBQS9DO0FBQXFELGVBQUtpUixHQUFMLENBQVNwYyxFQUFULENBQVksT0FBWixFQUFvQixzQkFBcEIsRUFBMkMsVUFBUytULEtBQVQsRUFBZTtBQUFDLG1CQUFPME4sS0FBSyxDQUFDMUgsU0FBTixFQUFQO0FBQTBCLFdBQXJGO0FBQXVGLGVBQUtxQyxHQUFMLENBQVNwYyxFQUFULENBQVksT0FBWixFQUFvQixzQkFBcEIsRUFBMkMsVUFBUytULEtBQVQsRUFBZTtBQUFDLG1CQUFPME4sS0FBSyxDQUFDM0gsU0FBTixFQUFQO0FBQTBCLFdBQXJGO0FBQXVGLGVBQUtzQyxHQUFMLENBQVNwYyxFQUFULENBQVksT0FBWixFQUFvQixxQkFBcEIsRUFBMEMsVUFBUytULEtBQVQsRUFBZTtBQUFDLGdCQUFHLENBQUMwTixLQUFLLENBQUM1UCxXQUFWLEVBQXNCO0FBQUM0UCxtQkFBSyxDQUFDME0sWUFBTjs7QUFBcUIxTSxtQkFBSyxDQUFDOUYsU0FBTixDQUFnQjVILEtBQUssQ0FBQ3BGLE1BQU4sQ0FBYTBOLE9BQWIsQ0FBcUI3TixLQUFyQztBQUE2QztBQUFDLFdBQXBKO0FBQXNKLGVBQUs4QixJQUFMO0FBQWE7O0FBQ3I1Qm1jLG9CQUFZLENBQUM3YyxTQUFELEVBQVcsQ0FBQztBQUFDc2QsYUFBRyxFQUFDLE1BQUw7QUFBWXJWLGVBQUssRUFBQyxTQUFTdkgsSUFBVCxHQUFlO0FBQUMsaUJBQUtxTCxTQUFMLENBQWUsQ0FBZjs7QUFBa0IsZ0JBQUcsS0FBS3hRLFFBQVIsRUFBaUI7QUFBQyxtQkFBS2lqQixhQUFMO0FBQXNCOztBQUNwSCxnQkFBRyxLQUFLTixjQUFSLEVBQXVCO0FBQUMsa0JBQUlPLGdCQUFnQixHQUFDLEtBQUtKLFFBQTFCO0FBQW1DLGtCQUFJM2dCLFVBQVUsR0FBQyxpREFBZjs7QUFBaUUsbUJBQUksSUFBSXRKLENBQUMsR0FBQyxDQUFWLEVBQVlBLENBQUMsR0FBQyxLQUFLaXFCLFFBQW5CLEVBQTRCanFCLENBQUMsRUFBN0IsRUFBZ0M7QUFBQyxvQkFBSTRkLElBQUksR0FBQyxzREFBb0Q1ZCxDQUFDLEtBQUcsQ0FBSixHQUFNLFlBQU4sR0FBbUIsRUFBdkUsSUFBMkUsZUFBM0UsSUFBNEZBLENBQUMsR0FBQyxDQUE5RixJQUFpRyxHQUFqRyxJQUFzR0EsQ0FBQyxHQUFDLENBQXhHLElBQTJHLFNBQXBIO0FBQThIc0osMEJBQVUsR0FBQ0EsVUFBVSxHQUFDc1UsSUFBdEI7QUFBNEI7O0FBQ3ZUdFUsd0JBQVUsR0FBQ0EsVUFBVSxHQUFDLGNBQXRCO0FBQXFDLG1CQUFLOE8sR0FBTCxDQUFTN1ksTUFBVCxDQUFnQitKLFVBQWhCO0FBQTZCO0FBQUM7QUFGM0MsU0FBRCxFQUU4QztBQUFDNGYsYUFBRyxFQUFDLGNBQUw7QUFBb0JyVixlQUFLLEVBQUMsU0FBU3NXLFlBQVQsR0FBdUI7QUFBQyxnQkFBSUcsTUFBTSxHQUFDLElBQVg7O0FBQWdCLGlCQUFLemMsV0FBTCxHQUFpQixJQUFqQjtBQUFzQixpQkFBS21LLFNBQUwsQ0FBZXVTLElBQWYsQ0FBb0IsVUFBcEIsRUFBK0IsSUFBL0I7QUFBcUNDLHlCQUFhLENBQUMsS0FBS04sUUFBTixDQUFiO0FBQTZCbG1CLHNCQUFVLENBQUMsWUFBVTtBQUFDc21CLG9CQUFNLENBQUN6YyxXQUFQLEdBQW1CLEtBQW5COztBQUF5QnljLG9CQUFNLENBQUN0UyxTQUFQLENBQWlCdVMsSUFBakIsQ0FBc0IsVUFBdEIsRUFBaUMsS0FBakM7O0FBQXdDLGtCQUFHRCxNQUFNLENBQUNuakIsUUFBVixFQUFtQjtBQUFDbWpCLHNCQUFNLENBQUNGLGFBQVA7QUFBd0I7QUFBQyxhQUExSCxFQUEySCxLQUFLMVUsaUJBQWhJLENBQVY7QUFBOEo7QUFBeFQsU0FGOUMsRUFFd1c7QUFBQ3dULGFBQUcsRUFBQyxXQUFMO0FBQWlCclYsZUFBSyxFQUFDLFNBQVM4RCxTQUFULENBQW1CeFMsS0FBbkIsRUFBeUI7QUFBQyxpQkFBSzBKLFlBQUwsR0FBa0JoUyxRQUFRLENBQUNzSSxLQUFELENBQTFCOztBQUFrQyxnQkFBRyxLQUFLMEosWUFBTCxHQUFrQixLQUFLb2IsUUFBMUIsRUFBbUM7QUFBQyxtQkFBS3BiLFlBQUwsR0FBa0IsQ0FBbEI7QUFBcUI7O0FBQzNnQixnQkFBRyxLQUFLQSxZQUFMLEtBQW9CLENBQXZCLEVBQXlCO0FBQUMsbUJBQUtBLFlBQUwsR0FBa0IsS0FBS29iLFFBQXZCO0FBQWlDOztBQUMzRCxnQkFBSVEsVUFBVSxHQUFDLEtBQUtyUyxHQUFMLENBQVNqWSxJQUFULENBQWMsdUNBQXFDLEtBQUswTyxZQUExQyxHQUF1RCxJQUFyRSxDQUFmO0FBQTBGLGdCQUFJNmIsT0FBTyxHQUFDLEtBQUs3YixZQUFMLEtBQW9CLENBQXBCLEdBQXNCLEtBQUt1SixHQUFMLENBQVNqWSxJQUFULENBQWMsdUJBQWQsRUFBdUNzZSxJQUF2QyxFQUF0QixHQUFvRWdNLFVBQVUsQ0FBQ2hXLElBQVgsQ0FBZ0IsdUJBQWhCLENBQWhGO0FBQXlILGdCQUFJa1csT0FBTyxHQUFDLEtBQUs5YixZQUFMLEtBQW9CLEtBQUtvYixRQUF6QixHQUFrQyxLQUFLN1IsR0FBTCxDQUFTalksSUFBVCxDQUFjLHVCQUFkLEVBQXVDd1YsS0FBdkMsRUFBbEMsR0FBaUY4VSxVQUFVLENBQUNqVyxJQUFYLENBQWdCLHVCQUFoQixDQUE3RjtBQUFzSSxpQkFBSzRELEdBQUwsQ0FBU2pZLElBQVQsQ0FBYyx1QkFBZCxFQUF1Q3pELFdBQXZDLENBQW1ELDRCQUFuRDtBQUFpRixpQkFBSzBiLEdBQUwsQ0FBU2pZLElBQVQsQ0FBYyxxQkFBZCxFQUFxQ3pELFdBQXJDLENBQWlELFlBQWpEOztBQUErRCxnQkFBRyxLQUFLdXRCLFFBQUwsR0FBYyxDQUFqQixFQUFtQjtBQUFDUyxxQkFBTyxDQUFDcHJCLFFBQVIsQ0FBaUIsU0FBakI7QUFBNEJxckIscUJBQU8sQ0FBQ3JyQixRQUFSLENBQWlCLFNBQWpCO0FBQTZCOztBQUN0akJtckIsc0JBQVUsQ0FBQ25yQixRQUFYLENBQW9CLFlBQXBCO0FBQWtDLGlCQUFLOFksR0FBTCxDQUFTalksSUFBVCxDQUFjLHFDQUFtQyxLQUFLME8sWUFBeEMsR0FBcUQsSUFBbkUsRUFBeUV2UCxRQUF6RSxDQUFrRixZQUFsRjtBQUFpRztBQUg0UCxTQUZ4VyxFQUs4RztBQUFDNHBCLGFBQUcsRUFBQyxXQUFMO0FBQWlCclYsZUFBSyxFQUFDLFNBQVNrQyxTQUFULEdBQW9CO0FBQUMsaUJBQUtvVSxZQUFMO0FBQW9CLGlCQUFLeFMsU0FBTCxDQUFlLEtBQUs5SSxZQUFMLEdBQWtCLENBQWpDO0FBQXFDO0FBQXJHLFNBTDlHLEVBS3FOO0FBQUNxYSxhQUFHLEVBQUMsV0FBTDtBQUFpQnJWLGVBQUssRUFBQyxTQUFTaUMsU0FBVCxHQUFvQjtBQUFDLGlCQUFLcVUsWUFBTDtBQUFvQixpQkFBS3hTLFNBQUwsQ0FBZSxLQUFLOUksWUFBTCxHQUFrQixDQUFqQztBQUFxQztBQUFyRyxTQUxyTixFQUs0VDtBQUFDcWEsYUFBRyxFQUFDLGVBQUw7QUFBcUJyVixlQUFLLEVBQUMsU0FBU3VXLGFBQVQsR0FBd0I7QUFBQyxnQkFBSVEsTUFBTSxHQUFDLElBQVg7O0FBQWdCLGlCQUFLVixRQUFMLEdBQWNXLFdBQVcsQ0FBQyxZQUFVO0FBQUMsa0JBQUcsQ0FBQ0QsTUFBTSxDQUFDL2MsV0FBWCxFQUF1QjtBQUFDK2Msc0JBQU0sQ0FBQzdVLFNBQVA7QUFBb0I7QUFBQyxhQUF6RCxFQUEwRCxLQUFLa0UsYUFBL0QsQ0FBekI7QUFBd0c7QUFBNUssU0FMNVQsRUFLMGU7QUFBQ2lQLGFBQUcsRUFBQyxTQUFMO0FBQWVyVixlQUFLLEVBQUMsU0FBU2lYLE9BQVQsR0FBa0I7QUFBQyxpQkFBSzFTLEdBQUwsQ0FBUzJTLEdBQVQ7QUFBZ0I7QUFBeEQsU0FMMWUsQ0FBWCxDQUFaOztBQUs2akIsZUFBT25mLFNBQVA7QUFBa0IsT0FadmhCLEVBQWQ7O0FBWXdpQixPQUFDLFlBQVU7QUFBQyxZQUFJb2YsTUFBTSxHQUFDLEtBQVg7QUFBaUIsWUFBSUMsT0FBTyxHQUFDLElBQVo7O0FBQWlCLGlCQUFTQyxJQUFULEdBQWU7QUFBQyxjQUFJbkIsT0FBTyxHQUFDO0FBQUNELDBCQUFjLEVBQUM7QUFBaEIsV0FBWjtBQUFrQyxjQUFJcUIsU0FBUyxHQUFDLElBQUl2ZixTQUFKLENBQWNtZSxPQUFkLENBQWQ7QUFBc0M7O0FBQ3h0QixpQkFBU3FCLFlBQVQsR0FBdUI7QUFBQzFCLGVBQUssQ0FBQ3BxQixRQUFOLENBQWUsV0FBZjtBQUE0QjBFLG9CQUFVLENBQUMsWUFBVTtBQUFDMGxCLGlCQUFLLENBQUNwcUIsUUFBTixDQUFlLGFBQWY7QUFBK0IsV0FBM0MsRUFBNEMsR0FBNUMsQ0FBVjtBQUE0RDs7QUFDaEhtcUIsZUFBTyxDQUFDenRCLEVBQVIsQ0FBVyxNQUFYLEVBQWtCLFlBQVU7QUFBQyxjQUFHLENBQUNndkIsTUFBSixFQUFXO0FBQUNBLGtCQUFNLEdBQUMsSUFBUDtBQUFZRSxnQkFBSTtBQUFJO0FBQUMsU0FBOUQ7QUFBZ0VsbkIsa0JBQVUsQ0FBQyxZQUFVO0FBQUMsY0FBRyxDQUFDZ25CLE1BQUosRUFBVztBQUFDQSxrQkFBTSxHQUFDLElBQVA7QUFBWUUsZ0JBQUk7QUFBSTtBQUFDLFNBQTdDLEVBQThDRCxPQUE5QyxDQUFWO0FBQWlFRyxvQkFBWTtBQUFJLE9BRmljO0FBRTViLEtBZjVEO0FBZThEbnVCLHFCQUFpQixDQUFDQyxLQUFsQixDQUF3QkMsU0FBeEIsQ0FBa0MsNERBQWxDLEVBQStGLFVBQVNDLE1BQVQsRUFBZ0I7QUFBQ2pDLFlBQU0sQ0FBQyxpQkFBRCxDQUFOLENBQTBCNjhCLFdBQTFCLENBQXNDO0FBQUNsMEIsZ0JBQVEsRUFBQyxPQUFWO0FBQWtCbTBCLGdCQUFRLEVBQUMsSUFBM0I7QUFBZ0NDLHNCQUFjLEVBQUMsSUFBL0M7QUFBb0RDLGFBQUssRUFBQyxvQkFBMUQ7QUFBK0VDLGdCQUFRLEVBQUMsR0FBeEY7QUFBNEZDLGdCQUFRLEVBQUMsR0FBckc7QUFBeUdoeUIsYUFBSyxFQUFDLEVBQS9HO0FBQWtIcXdCLG1CQUFXLEVBQUM7QUFBOUgsT0FBdEM7QUFBNkssS0FBN1I7QUFBK1J6NUIscUJBQWlCLENBQUNDLEtBQWxCLENBQXdCQyxTQUF4QixDQUFrQyx1REFBbEMsRUFBMEYsVUFBU0MsTUFBVCxFQUFnQjtBQUFDakMsWUFBTSxDQUFDLDhCQUFELENBQU4sQ0FBdUNrQixJQUF2QyxDQUE0QyxZQUFVO0FBQUMsWUFBSWk4QixTQUFTLEdBQUNuOUIsTUFBTSxDQUFDLElBQUQsQ0FBTixDQUFhZ0YsSUFBYixDQUFrQixHQUFsQixDQUFkO0FBQXFDbTRCLGlCQUFTLENBQUN4dkIsSUFBVixDQUFlLE9BQWYsRUFBdUIsWUFBVTtBQUFDLGNBQUl5dkIsT0FBTyxHQUFDcDlCLE1BQU0sQ0FBQyxJQUFELENBQU4sQ0FBYXdELElBQWIsQ0FBa0IsV0FBbEIsQ0FBWjtBQUEyQ3hELGdCQUFNLENBQUMsTUFBSW85QixPQUFMLENBQU4sQ0FBb0JqNUIsUUFBcEIsQ0FBNkIsUUFBN0I7QUFBdUMsY0FBSXJELE9BQU8sSUFBRSxrQkFBaUJDLFFBQVEsQ0FBQ0MsZUFBNUIsQ0FBWDs7QUFBd0QsY0FBR0YsT0FBSCxFQUFXO0FBQUNkLGtCQUFNLENBQUMsTUFBSW85QixPQUFMLENBQU4sQ0FBb0JwNEIsSUFBcEIsQ0FBeUIsK0JBQXpCLEVBQTBEYixRQUExRCxDQUFtRSxPQUFuRTtBQUE2RTs7QUFDbjRCbkUsZ0JBQU0sQ0FBQyxNQUFELENBQU4sQ0FBZW1FLFFBQWYsQ0FBd0IsdUJBQXhCO0FBQWlEbkUsZ0JBQU0sQ0FBQyxNQUFELENBQU4sQ0FBZW1FLFFBQWYsQ0FBd0IsMkJBQXhCO0FBQXFEMEUsb0JBQVUsQ0FBQyxZQUFVO0FBQUM3SSxrQkFBTSxDQUFDLE1BQUlvOUIsT0FBTCxDQUFOLENBQW9CcDRCLElBQXBCLENBQXlCLG1DQUF6QixFQUE4REEsSUFBOUQsQ0FBbUUsU0FBbkUsRUFBOEVzRixPQUE5RSxDQUFzRixPQUF0RjtBQUFnRyxXQUE1RyxFQUE2RyxHQUE3RyxDQUFWO0FBQTZILFNBRDJaO0FBQ3haLE9BRDRUO0FBQzFUdEssWUFBTSxDQUFDLG1DQUFELENBQU4sQ0FBNENrQixJQUE1QyxDQUFpRCxZQUFVO0FBQUMsWUFBSW04QixPQUFPLEdBQUNyOUIsTUFBTSxDQUFDLElBQUQsQ0FBTixDQUFhd0QsSUFBYixDQUFrQixXQUFsQixDQUFaO0FBQTJDLFlBQUk4NUIsU0FBUyxHQUFDdDlCLE1BQU0sQ0FBQyxJQUFELENBQU4sQ0FBYWdGLElBQWIsQ0FBa0IsaUJBQWxCLENBQWQ7QUFBbUQsWUFBSXU0QixTQUFTLEdBQUN2OUIsTUFBTSxDQUFDLElBQUQsQ0FBTixDQUFhd0QsSUFBYixDQUFrQixhQUFsQixDQUFkO0FBQStDLFlBQUkxQyxPQUFPLElBQUUsa0JBQWlCQyxRQUFRLENBQUNDLGVBQTVCLENBQVg7O0FBQXdELFlBQUcsQ0FBQ0YsT0FBSixFQUFZO0FBQUN3OEIsbUJBQVMsQ0FBQ3o4QixFQUFWLENBQWEsT0FBYixFQUFxQixZQUFVO0FBQUMsZ0JBQUdiLE1BQU0sQ0FBQyxJQUFELENBQU4sQ0FBYW1LLEdBQWIsR0FBbUJzQyxNQUFuQixHQUEwQixDQUE3QixFQUN0aEI7QUFBQ3pNLG9CQUFNLENBQUN3OUIsSUFBUCxDQUFZO0FBQUNDLG1CQUFHLEVBQUNDLE1BQU0sQ0FBQ0MsT0FBWjtBQUFvQmw3QixvQkFBSSxFQUFDLE1BQXpCO0FBQWdDSixvQkFBSSxFQUFDLCtDQUE2Q3JDLE1BQU0sQ0FBQyxJQUFELENBQU4sQ0FBYTQ5QixTQUFiLEVBQWxGO0FBQTJHQyx1QkFBTyxFQUFDLGlCQUFTQyxPQUFULEVBQWlCO0FBQUM5OUIsd0JBQU0sQ0FBQyxNQUFJdTlCLFNBQUwsQ0FBTixDQUFzQnZkLElBQXRCLENBQTJCOGQsT0FBM0I7O0FBQW9DLHNCQUFHQSxPQUFPLElBQUUsRUFBWixFQUN0TDtBQUFDOTlCLDBCQUFNLENBQUMsTUFBSXU5QixTQUFMLENBQU4sQ0FBc0JwNUIsUUFBdEIsQ0FBK0IsU0FBL0I7QUFBMENuRSwwQkFBTSxDQUFDLE1BQUl1OUIsU0FBTCxDQUFOLENBQXNCN0ssSUFBdEI7QUFBNkIxeUIsMEJBQU0sQ0FBQyxNQUFJdTlCLFNBQUwsQ0FBTixDQUFzQnQ4QixHQUF0QixDQUEwQixTQUExQixFQUFvQyxFQUFwQztBQUF3Q2pCLDBCQUFNLENBQUMsTUFBSXU5QixTQUFKLEdBQWMsVUFBZixDQUFOLENBQWlDUSxTQUFqQyxDQUEyQyxZQUFVO0FBQUMvOUIsNEJBQU0sQ0FBQyxNQUFJdTlCLFNBQUwsQ0FBTixDQUFzQnA1QixRQUF0QixDQUErQixTQUEvQjtBQUEwQ25FLDRCQUFNLENBQUMsTUFBSXU5QixTQUFMLENBQU4sQ0FBc0IvNUIsSUFBdEIsQ0FBMkIsZ0JBQTNCLEVBQTRDLE1BQTVDO0FBQW9EMHdCLDhCQUFRLENBQUM4SixJQUFULEdBQWNoK0IsTUFBTSxDQUFDLElBQUQsQ0FBTixDQUFhd0QsSUFBYixDQUFrQixNQUFsQixDQUFkO0FBQXlDLHFCQUE3TDtBQUFnTSxtQkFEMUgsTUFHdEw7QUFBQ3hELDBCQUFNLENBQUMsTUFBSXU5QixTQUFMLENBQU4sQ0FBc0IvSyxJQUF0QjtBQUE4QjtBQUFDO0FBSG5CLGVBQVo7QUFHbUMsYUFKa2YsTUFNdGhCO0FBQUN4eUIsb0JBQU0sQ0FBQyxNQUFJdTlCLFNBQUwsQ0FBTixDQUFzQnZkLElBQXRCLENBQTJCLEVBQTNCO0FBQWdDO0FBQUMsV0FOb2Q7QUFNamQ7O0FBQ3JDc2QsaUJBQVMsQ0FBQzN2QixJQUFWLENBQWUsT0FBZixFQUF1QixZQUFVO0FBQUMzTixnQkFBTSxDQUFDLE1BQUl1OUIsU0FBTCxDQUFOLENBQXNCcDVCLFFBQXRCLENBQStCLFNBQS9CO0FBQTBDbkUsZ0JBQU0sQ0FBQyxNQUFJdTlCLFNBQUwsQ0FBTixDQUFzQi81QixJQUF0QixDQUEyQixnQkFBM0IsRUFBNEMsTUFBNUM7QUFBcUQsU0FBakk7QUFBbUk4NUIsaUJBQVMsQ0FBQzN2QixJQUFWLENBQWUsT0FBZixFQUF1QixZQUFVO0FBQUMzTixnQkFBTSxDQUFDLE1BQUl1OUIsU0FBTCxDQUFOLENBQXNCcDVCLFFBQXRCLENBQStCLFNBQS9CO0FBQTBDbTVCLG1CQUFTLENBQUNuNUIsUUFBVixDQUFtQixTQUFuQjtBQUErQixTQUEzRztBQUE2R201QixpQkFBUyxDQUFDM3ZCLElBQVYsQ0FBZSxNQUFmLEVBQXNCLFlBQVU7QUFBQzNOLGdCQUFNLENBQUMsTUFBSXU5QixTQUFMLENBQU4sQ0FBc0JoOEIsV0FBdEIsQ0FBa0MsU0FBbEM7QUFBNkMrN0IsbUJBQVMsQ0FBQy83QixXQUFWLENBQXNCLFNBQXRCO0FBQWtDLFNBQWhIO0FBQWtIdkIsY0FBTSxDQUFDLE1BQUlxOUIsT0FBTCxDQUFOLENBQW9CMXZCLElBQXBCLENBQXlCLE9BQXpCLEVBQWlDLFlBQVU7QUFBQyxjQUFHLENBQUMydkIsU0FBUyxDQUFDNzFCLFFBQVYsQ0FBbUIsU0FBbkIsQ0FBSixFQUM5WTtBQUFDLGdCQUFHekgsTUFBTSxDQUFDLE1BQUl1OUIsU0FBTCxDQUFOLENBQXNCLzVCLElBQXRCLENBQTJCLGdCQUEzQixLQUE4QyxNQUFqRCxFQUNEO0FBQUN4RCxvQkFBTSxDQUFDLE1BQUl1OUIsU0FBTCxDQUFOLENBQXNCaDhCLFdBQXRCLENBQWtDLFNBQWxDO0FBQThDOztBQUMvQ3ZCLGtCQUFNLENBQUMsTUFBSXE5QixPQUFMLENBQU4sQ0FBb0I5N0IsV0FBcEIsQ0FBZ0MsUUFBaEM7QUFBMEN2QixrQkFBTSxDQUFDLE1BQUQsQ0FBTixDQUFldUIsV0FBZixDQUEyQix1QkFBM0I7QUFBb0R2QixrQkFBTSxDQUFDLE1BQUQsQ0FBTixDQUFldUIsV0FBZixDQUEyQiwyQkFBM0I7QUFBeUQ7QUFBQyxTQUgwTTtBQUd2TSxPQVY2RTtBQVUxRSxLQVh5UjtBQVd2Uk8scUJBQWlCLENBQUNDLEtBQWxCLENBQXdCQyxTQUF4QixDQUFrQyw2REFBbEMsRUFBZ0csVUFBU0MsTUFBVCxFQUFnQjtBQUFDakMsWUFBTSxDQUFDLHVCQUFELENBQU4sQ0FBZ0NpK0IsU0FBaEMsQ0FBMEMsWUFBVTtBQUFDLFlBQUlDLFVBQVUsR0FBQ2wrQixNQUFNLENBQUMsSUFBRCxDQUFOLENBQWF3RCxJQUFiLENBQWtCLGFBQWxCLENBQWY7QUFBZ0R4RCxjQUFNLENBQUMsSUFBRCxDQUFOLENBQWFnRixJQUFiLENBQWtCLGNBQWxCLEVBQWtDL0QsR0FBbEMsQ0FBc0MsV0FBdEMsRUFBa0QsaUJBQWVpOUIsVUFBZixHQUEwQixLQUE1RTtBQUFvRixPQUF6TCxFQUEyTEMsVUFBM0wsQ0FBc00sWUFBVTtBQUFDbitCLGNBQU0sQ0FBQyxJQUFELENBQU4sQ0FBYWdGLElBQWIsQ0FBa0IsY0FBbEIsRUFBa0MvRCxHQUFsQyxDQUFzQyxXQUF0QyxFQUFrRCxpQkFBbEQ7QUFBc0UsT0FBdlI7QUFBeVJqQixZQUFNLENBQUMsdUJBQUQsQ0FBTixDQUFnQ2tCLElBQWhDLENBQXFDLFlBQVU7QUFBQyxZQUFJazlCLFlBQVksR0FBQ3ArQixNQUFNLENBQUMsSUFBRCxDQUFOLENBQWFnRixJQUFiLENBQWtCLGdCQUFsQixDQUFqQjtBQUFxRCxZQUFJcTVCLFVBQVUsR0FBQzM4QixRQUFRLENBQUMwOEIsWUFBWSxDQUFDejlCLE1BQWIsS0FBc0IsRUFBdkIsQ0FBdkI7QUFBa0RYLGNBQU0sQ0FBQyxJQUFELENBQU4sQ0FBYXdELElBQWIsQ0FBa0IsYUFBbEIsRUFBZ0M2NkIsVUFBaEM7QUFBNkMsT0FBcE07QUFBc01yK0IsWUFBTSxDQUFDTixNQUFELENBQU4sQ0FBZWtDLE1BQWYsQ0FBc0IsWUFBVTtBQUFDNUIsY0FBTSxDQUFDLHVCQUFELENBQU4sQ0FBZ0NrQixJQUFoQyxDQUFxQyxZQUFVO0FBQUMsY0FBSWs5QixZQUFZLEdBQUNwK0IsTUFBTSxDQUFDLElBQUQsQ0FBTixDQUFhZ0YsSUFBYixDQUFrQixnQkFBbEIsQ0FBakI7QUFBcUQsY0FBSXE1QixVQUFVLEdBQUMzOEIsUUFBUSxDQUFDMDhCLFlBQVksQ0FBQ3o5QixNQUFiLEtBQXNCLEVBQXZCLENBQXZCO0FBQWtEWCxnQkFBTSxDQUFDLElBQUQsQ0FBTixDQUFhd0QsSUFBYixDQUFrQixhQUFsQixFQUFnQzY2QixVQUFoQztBQUE2QyxTQUFwTTtBQUF1TSxPQUF4TztBQUEyTyxLQUEzekI7QUFBNnpCdjhCLHFCQUFpQixDQUFDQyxLQUFsQixDQUF3QkMsU0FBeEIsQ0FBa0MsOERBQWxDLEVBQWlHLFVBQVNDLE1BQVQsRUFBZ0I7QUFBQyxVQUFJcThCLEtBQUssR0FBQ2x0QixLQUFLLENBQUM4USxTQUFOLENBQWdCNU0sS0FBaEIsQ0FBc0JDLElBQXRCLENBQTJCeFUsUUFBUSxDQUFDdVEsZ0JBQVQsQ0FBMEIsWUFBMUIsQ0FBM0IsQ0FBVjtBQUE4RWd0QixXQUFLLENBQUNsdkIsT0FBTixDQUFjLFVBQVNtdkIsSUFBVCxFQUFjO0FBQUMsWUFBSUMsU0FBUyxHQUFDLElBQUlDLFNBQUosQ0FBY0YsSUFBZCxFQUFtQjtBQUFDemIsa0JBQVEsRUFBQyxJQUFWO0FBQWU0Yix5QkFBZSxFQUFDLENBQS9CO0FBQWlDaGIsZUFBSyxFQUFDNmEsSUFBSSxDQUFDaHFCLFlBQUwsQ0FBa0IsZ0JBQWxCLENBQXZDO0FBQTJFb3FCLHdCQUFjLEVBQUNKLElBQUksQ0FBQ2hxQixZQUFMLENBQWtCLGdCQUFsQixDQUExRjtBQUE4SHFxQixtQkFBUyxFQUFDTCxJQUFJLENBQUNocUIsWUFBTCxDQUFrQixvQkFBbEIsQ0FBeEk7QUFBZ0xzcUIsNEJBQWtCLEVBQUNOLElBQUksQ0FBQ2hxQixZQUFMLENBQWtCLG9CQUFsQjtBQUFuTSxTQUFuQixDQUFkO0FBQStRLE9BQTVTO0FBQThTLFVBQUl1cUIsY0FBYyxHQUFDLzlCLFFBQVEsQ0FBQzBRLGFBQVQsQ0FBdUIsWUFBdkIsQ0FBbkI7O0FBQXdELFVBQUcsQ0FBQyxDQUFDcXRCLGNBQUwsRUFBb0I7QUFBQ0Esc0JBQWMsQ0FBQ0MsUUFBZixHQUF3QixZQUFVO0FBQUMvK0IsZ0JBQU0sQ0FBQyxxQkFBRCxDQUFOLENBQThCa0IsSUFBOUIsQ0FBbUMsWUFBVTtBQUFDLGdCQUFHNDlCLGNBQWMsQ0FBQ0UsT0FBbEIsRUFDem1EO0FBQUMsa0JBQUlDLFdBQVcsR0FBQ2ovQixNQUFNLENBQUMsSUFBRCxDQUFOLENBQWF3RCxJQUFiLENBQWtCLGlCQUFsQixDQUFoQjtBQUFxRHhELG9CQUFNLENBQUMsSUFBRCxDQUFOLENBQWFnZ0IsSUFBYixDQUFrQmlmLFdBQWxCO0FBQStCai9CLG9CQUFNLENBQUMsMEJBQUQsQ0FBTixDQUFtQ21FLFFBQW5DLENBQTRDLE1BQTVDO0FBQW9EbkUsb0JBQU0sQ0FBQyx5QkFBRCxDQUFOLENBQWtDdUIsV0FBbEMsQ0FBOEMsTUFBOUM7QUFBdUQsYUFEeTZDLE1BR3ptRDtBQUFDLGtCQUFJMDlCLFdBQVcsR0FBQ2ovQixNQUFNLENBQUMsSUFBRCxDQUFOLENBQWF3RCxJQUFiLENBQWtCLGtCQUFsQixDQUFoQjtBQUFzRHhELG9CQUFNLENBQUMsSUFBRCxDQUFOLENBQWFnZ0IsSUFBYixDQUFrQmlmLFdBQWxCO0FBQStCai9CLG9CQUFNLENBQUMseUJBQUQsQ0FBTixDQUFrQ21FLFFBQWxDLENBQTJDLE1BQTNDO0FBQW1EbkUsb0JBQU0sQ0FBQywwQkFBRCxDQUFOLENBQW1DdUIsV0FBbkMsQ0FBK0MsTUFBL0M7QUFBd0Q7QUFBQyxXQUh5M0M7QUFHdDNDLFNBSG0xQztBQUdqMUM7QUFBQyxLQUhxeEI7QUFHbnhCTyxxQkFBaUIsQ0FBQ0MsS0FBbEIsQ0FBd0JDLFNBQXhCLENBQWtDLDBEQUFsQyxFQUE2RixVQUFTQyxNQUFULEVBQWdCO0FBQUNqQyxZQUFNLENBQUMsZUFBRCxDQUFOLENBQXdCNjhCLFdBQXhCLENBQW9DO0FBQUNsMEIsZ0JBQVEsRUFBQyxPQUFWO0FBQWtCbTBCLGdCQUFRLEVBQUMsSUFBM0I7QUFBZ0NDLHNCQUFjLEVBQUMsSUFBL0M7QUFBb0RDLGFBQUssRUFBQyxvQkFBMUQ7QUFBK0VDLGdCQUFRLEVBQUMsR0FBeEY7QUFBNEZDLGdCQUFRLEVBQUMsR0FBckc7QUFBeUdoeUIsYUFBSyxFQUFDLEVBQS9HO0FBQWtIcXdCLG1CQUFXLEVBQUM7QUFBOUgsT0FBcEM7QUFBMkssS0FBelI7QUFBMlJ6NUIscUJBQWlCLENBQUNDLEtBQWxCLENBQXdCQyxTQUF4QixDQUFrQyw0REFBbEMsRUFBK0YsVUFBU0MsTUFBVCxFQUFnQjtBQUFDakMsWUFBTSxDQUFDLG1DQUFELENBQU4sQ0FBNENrQixJQUE1QyxDQUFpRCxZQUFVO0FBQUMsWUFBSW84QixTQUFTLEdBQUN0OUIsTUFBTSxDQUFDLElBQUQsQ0FBTixDQUFhZ0YsSUFBYixDQUFrQixpQkFBbEIsQ0FBZDtBQUFtRCxZQUFJdTRCLFNBQVMsR0FBQ3Y5QixNQUFNLENBQUMsSUFBRCxDQUFOLENBQWF3RCxJQUFiLENBQWtCLGFBQWxCLENBQWQ7QUFBK0M4NUIsaUJBQVMsQ0FBQ3o4QixFQUFWLENBQWEsT0FBYixFQUFxQixZQUFVO0FBQUMsY0FBR2IsTUFBTSxDQUFDLElBQUQsQ0FBTixDQUFhbUssR0FBYixHQUFtQnNDLE1BQW5CLEdBQTBCLENBQTdCLEVBQ254QjtBQUFDek0sa0JBQU0sQ0FBQ3c5QixJQUFQLENBQVk7QUFBQ0MsaUJBQUcsRUFBQ0MsTUFBTSxDQUFDQyxPQUFaO0FBQW9CbDdCLGtCQUFJLEVBQUMsTUFBekI7QUFBZ0NKLGtCQUFJLEVBQUMsK0NBQTZDckMsTUFBTSxDQUFDLElBQUQsQ0FBTixDQUFhNDlCLFNBQWIsRUFBbEY7QUFBMkdDLHFCQUFPLEVBQUMsaUJBQVNDLE9BQVQsRUFBaUI7QUFBQzk5QixzQkFBTSxDQUFDLE1BQUl1OUIsU0FBTCxDQUFOLENBQXNCdmQsSUFBdEIsQ0FBMkI4ZCxPQUEzQjs7QUFBb0Msb0JBQUdBLE9BQU8sSUFBRSxFQUFaLEVBQ3RMO0FBQUM5OUIsd0JBQU0sQ0FBQyxNQUFJdTlCLFNBQUwsQ0FBTixDQUFzQnA1QixRQUF0QixDQUErQixTQUEvQjtBQUEwQ25FLHdCQUFNLENBQUMsTUFBSXU5QixTQUFMLENBQU4sQ0FBc0I3SyxJQUF0QjtBQUE2QjF5Qix3QkFBTSxDQUFDLE1BQUl1OUIsU0FBTCxDQUFOLENBQXNCdDhCLEdBQXRCLENBQTBCLFNBQTFCLEVBQW9DLEVBQXBDO0FBQXdDakIsd0JBQU0sQ0FBQyxNQUFJdTlCLFNBQUosR0FBYyxVQUFmLENBQU4sQ0FBaUNRLFNBQWpDLENBQTJDLFlBQVU7QUFBQy85QiwwQkFBTSxDQUFDLE1BQUl1OUIsU0FBTCxDQUFOLENBQXNCLzVCLElBQXRCLENBQTJCLGdCQUEzQixFQUE0QyxJQUE1QztBQUFtRCxtQkFBekc7QUFBNEcsaUJBRHRDLE1BR3RMO0FBQUN4RCx3QkFBTSxDQUFDLE1BQUl1OUIsU0FBTCxDQUFOLENBQXNCL0ssSUFBdEI7QUFBOEI7QUFBQztBQUhuQixhQUFaO0FBR21DO0FBQUMsU0FKOHNCO0FBSTVzQjhLLGlCQUFTLENBQUMzdkIsSUFBVixDQUFlLE9BQWYsRUFBdUIsWUFBVTtBQUFDM04sZ0JBQU0sQ0FBQyxNQUFJdTlCLFNBQUwsQ0FBTixDQUFzQnA1QixRQUF0QixDQUErQixTQUEvQjtBQUEyQyxTQUE3RTtBQUErRW01QixpQkFBUyxDQUFDM3ZCLElBQVYsQ0FBZSxNQUFmLEVBQXNCLFlBQVU7QUFBQyxjQUFHM04sTUFBTSxDQUFDLE1BQUl1OUIsU0FBTCxDQUFOLENBQXNCLzVCLElBQXRCLENBQTJCLGdCQUEzQixLQUE4QyxNQUFqRCxFQUN2SjtBQUFDeEQsa0JBQU0sQ0FBQyxNQUFJdTlCLFNBQUwsQ0FBTixDQUFzQmg4QixXQUF0QixDQUFrQyxTQUFsQztBQUE4QztBQUFDLFNBRHNFO0FBQ25FLE9BTGtpQjtBQUsvaEIsS0FMK2E7QUFLN2FPLHFCQUFpQixDQUFDQyxLQUFsQixDQUF3QkMsU0FBeEIsQ0FBa0MseUVBQWxDLEVBQTRHLFVBQVNDLE1BQVQsRUFBZ0I7QUFBQyxVQUFHSCxpQkFBaUIsQ0FBQ0ksVUFBbEIsRUFBSCxFQUFrQztBQUFDbEMsY0FBTSxDQUFDLGlCQUFELENBQU4sQ0FBMEJrL0IsVUFBMUIsQ0FBcUM7QUFBQ3R1QixtQkFBUyxFQUFDLE1BQVg7QUFBa0J1dUIsdUJBQWEsRUFBQyxJQUFoQztBQUFxQ0Msb0JBQVUsRUFBQyxDQUFoRDtBQUFrREMsa0JBQVEsRUFBQyxDQUEzRDtBQUE2RGhnQixrQkFBUSxFQUFDLENBQXRFO0FBQXdFdFUsbUJBQVMsRUFBQyxDQUFsRjtBQUFvRnUwQixvQkFBVSxFQUFDLEtBQS9GO0FBQXFHQyxzQkFBWSxFQUFDLEtBQWxIO0FBQXdIQyx3QkFBYyxFQUFDLElBQXZJO0FBQTRJdG9CLGNBQUksRUFBQztBQUFqSixTQUFyQztBQUEwTGxYLGNBQU0sQ0FBQyxzQ0FBRCxDQUFOLENBQStDa0IsSUFBL0MsQ0FBb0QsWUFBVTtBQUFDLGNBQUlQLE1BQU0sR0FBQ1gsTUFBTSxDQUFDLElBQUQsQ0FBTixDQUFhVyxNQUFiLEVBQVg7QUFBaUMsY0FBSTgrQixXQUFXLEdBQUN6L0IsTUFBTSxDQUFDLElBQUQsQ0FBTixDQUFhZ0YsSUFBYixDQUFrQixLQUFsQixFQUF5QnJFLE1BQXpCLEVBQWhCO0FBQWtELGNBQUlQLE1BQU0sR0FBQyxDQUFDTyxNQUFNLEdBQUM4K0IsV0FBUixJQUFxQixDQUFoQztBQUFrQ3ovQixnQkFBTSxDQUFDLElBQUQsQ0FBTixDQUFhZ0YsSUFBYixDQUFrQixLQUFsQixFQUF5Qi9ELEdBQXpCLENBQTZCLFlBQTdCLEVBQTBDYixNQUFNLEdBQUMsSUFBakQ7QUFBd0QsU0FBNU87QUFBK087QUFBQyxLQUExa0I7QUFBNGtCMEIscUJBQWlCLENBQUNDLEtBQWxCLENBQXdCQyxTQUF4QixDQUFrQyw4REFBbEMsRUFBaUcsVUFBU0MsTUFBVCxFQUFnQjtBQUFDakMsWUFBTSxDQUFDLDJCQUFELENBQU4sQ0FBb0NrQixJQUFwQyxDQUF5QyxZQUFVO0FBQUMsWUFBSXd2QixXQUFXLEdBQUMxd0IsTUFBTSxDQUFDLElBQUQsQ0FBTixDQUFhd2EsS0FBYixFQUFoQjtBQUFxQyxZQUFJa2xCLGdCQUFnQixHQUFDMS9CLE1BQU0sQ0FBQyxJQUFELENBQU4sQ0FBYXdELElBQWIsQ0FBa0IsZ0JBQWxCLENBQXJCO0FBQXlELFlBQUltOEIsYUFBYSxHQUFDMy9CLE1BQU0sQ0FBQyxJQUFELENBQU4sQ0FBYXdELElBQWIsQ0FBa0IsZ0JBQWxCLENBQWxCO0FBQXNELFlBQUlvOEIsZUFBZSxHQUFDbCtCLFFBQVEsQ0FBQzFCLE1BQU0sQ0FBQyxJQUFELENBQU4sQ0FBYXdELElBQWIsQ0FBa0IsaUJBQWxCLENBQUQsQ0FBNUI7QUFBbUUsWUFBSXE4QixlQUFlLEdBQUNuK0IsUUFBUSxDQUFDMUIsTUFBTSxDQUFDLElBQUQsQ0FBTixDQUFhd0QsSUFBYixDQUFrQix1QkFBbEIsQ0FBRCxDQUE1QjtBQUF5RSxZQUFJczhCLGtCQUFrQixHQUFDcCtCLFFBQVEsQ0FBQzFCLE1BQU0sQ0FBQyxJQUFELENBQU4sQ0FBYXdELElBQWIsQ0FBa0IsMEJBQWxCLENBQUQsQ0FBL0I7O0FBQStFLFlBQUdtOEIsYUFBYSxJQUFFLFVBQWxCLEVBQTZCO0FBQUNBLHVCQUFhLEdBQUMsTUFBZDtBQUFxQjs7QUFDNXNDLFlBQUlJLFlBQVksR0FBQ3JQLFdBQVcsQ0FBQ3NQLEtBQVosQ0FBa0I7QUFBQ0MsbUJBQVMsRUFBQ04sYUFBWDtBQUF5Qk8sY0FBSSxFQUFDO0FBQTlCLFNBQWxCLENBQWpCOztBQUEwRSxZQUFHbGdDLE1BQU0sQ0FBQyxJQUFELENBQU4sQ0FBYXlILFFBQWIsQ0FBc0IsaUJBQXRCLENBQUgsRUFBNEM7QUFBQ3M0QixzQkFBWSxDQUFDNytCLElBQWIsQ0FBa0IsVUFBUzJELENBQVQsRUFBVztBQUFDLGdCQUFJczdCLFlBQVksR0FBQ25nQyxNQUFNLENBQUMsSUFBRCxDQUF2QjtBQUE4QixnQkFBSW9nQyxXQUFXLEdBQUNELFlBQVksQ0FBQzNILElBQWIsRUFBaEI7QUFBcUMsV0FBakc7QUFBb0c7O0FBQzNOLFlBQUc5SCxXQUFXLENBQUN4d0IsWUFBWixFQUFILEVBQThCO0FBQUM2L0Isc0JBQVksQ0FBQzcrQixJQUFiLENBQWtCLFVBQVMyRCxDQUFULEVBQVc7QUFBQyxnQkFBSXc3QixVQUFVLEdBQUMzK0IsUUFBUSxDQUFDbStCLGVBQWUsR0FBRWg3QixDQUFDLEdBQUMrNkIsZUFBcEIsQ0FBdkI7O0FBQTZELGdCQUFHRixnQkFBZ0IsSUFBRSxVQUFyQixFQUFnQztBQUFDVyx3QkFBVSxHQUFDMytCLFFBQVEsQ0FBQ20rQixlQUFlLEdBQUNELGVBQWpCLENBQW5CO0FBQXNEOztBQUNqTjUvQixrQkFBTSxDQUFDLElBQUQsQ0FBTixDQUFhMnFCLEtBQWIsQ0FBbUIsVUFBU3RSLElBQVQsRUFBYztBQUFDclosb0JBQU0sQ0FBQyxJQUFELENBQU4sQ0FBYWlCLEdBQWIsQ0FBaUI7QUFBQyxvQ0FBbUJvL0IsVUFBVSxHQUFDLElBQS9CO0FBQW9DLHVDQUFzQlAsa0JBQWtCLEdBQUMsSUFBN0U7QUFBa0YsNkJBQVksaURBQTlGO0FBQWdKLDJCQUFVO0FBQTFKLGVBQWpCO0FBQWdMLGFBQWxOO0FBQW9OLFdBRHJMO0FBQ3dMOztBQUN2TjkvQixjQUFNLENBQUNOLE1BQUQsQ0FBTixDQUFlbUIsRUFBZixDQUFrQixlQUFsQixFQUFrQyxZQUFVO0FBQUMsY0FBRzZ2QixXQUFXLENBQUN4d0IsWUFBWixFQUFILEVBQThCO0FBQUM2L0Isd0JBQVksQ0FBQzcrQixJQUFiLENBQWtCLFVBQVMyRCxDQUFULEVBQVc7QUFBQyxrQkFBSXc3QixVQUFVLEdBQUMzK0IsUUFBUSxDQUFDbStCLGVBQWUsR0FBRWg3QixDQUFDLEdBQUMrNkIsZUFBcEIsQ0FBdkI7O0FBQTZELGtCQUFHRixnQkFBZ0IsSUFBRSxVQUFyQixFQUFnQztBQUFDVywwQkFBVSxHQUFDMytCLFFBQVEsQ0FBQ20rQixlQUFlLEdBQUNELGVBQWpCLENBQW5CO0FBQXNEOztBQUM5UDUvQixvQkFBTSxDQUFDLElBQUQsQ0FBTixDQUFhMnFCLEtBQWIsQ0FBbUIsVUFBU3RSLElBQVQsRUFBYztBQUFDclosc0JBQU0sQ0FBQyxJQUFELENBQU4sQ0FBYWlCLEdBQWIsQ0FBaUI7QUFBQyxzQ0FBbUJvL0IsVUFBVSxHQUFDLElBQS9CO0FBQW9DLHlDQUFzQlAsa0JBQWtCLEdBQUMsSUFBN0U7QUFBa0YsK0JBQVksaURBQTlGO0FBQWdKLDZCQUFVO0FBQTFKLGlCQUFqQjtBQUFnTCxlQUFsTjtBQUFvTixhQUR4STtBQUMySTtBQUFDLFNBRHhOO0FBQzJOLE9BTDJoQjtBQUt4aEIsS0FMc2E7QUFLcGFoK0IscUJBQWlCLENBQUNDLEtBQWxCLENBQXdCQyxTQUF4QixDQUFrQyxrRUFBbEMsRUFBcUcsVUFBU0MsTUFBVCxFQUFnQjtBQUFDakMsWUFBTSxDQUFDLCtCQUFELENBQU4sQ0FBd0NrQixJQUF4QyxDQUE2QyxZQUFVO0FBQUMsWUFBSW8vQixhQUFhLEdBQUN0Z0MsTUFBTSxDQUFDLElBQUQsQ0FBTixDQUFhd0QsSUFBYixDQUFrQixnQkFBbEIsQ0FBbEI7QUFBc0R4RCxjQUFNLENBQUMsSUFBRCxDQUFOLENBQWF1Z0MsZ0JBQWIsQ0FBOEI7QUFBQ0QsdUJBQWEsRUFBQ0E7QUFBZixTQUE5QjtBQUE4RCxPQUE1SztBQUErSyxLQUFyUztBQUF1U3grQixxQkFBaUIsQ0FBQ0MsS0FBbEIsQ0FBd0JDLFNBQXhCLENBQWtDLHFFQUFsQyxFQUF3RyxVQUFTQyxNQUFULEVBQWdCO0FBQUNqQyxZQUFNLENBQUMsOENBQUQsQ0FBTixDQUF1RGtCLElBQXZELENBQTRELFlBQVU7QUFBQyxZQUFJOEosUUFBUSxHQUFDaEwsTUFBTSxDQUFDLElBQUQsQ0FBTixDQUFhd0QsSUFBYixDQUFrQixlQUFsQixDQUFiOztBQUFnRCxZQUFHLE9BQU93SCxRQUFQLElBQWlCLFdBQXBCLEVBQWdDO0FBQUNBLGtCQUFRLEdBQUMsS0FBVDtBQUFnQjs7QUFDeHlCLFlBQUdBLFFBQVEsSUFBRSxDQUFiLEVBQ0E7QUFBQ0Esa0JBQVEsR0FBQyxJQUFUO0FBQWUsU0FEaEIsTUFHQTtBQUFDQSxrQkFBUSxHQUFDLEtBQVQ7QUFBZ0I7O0FBQ2pCLFlBQUlrRCxLQUFLLEdBQUNsTyxNQUFNLENBQUMsSUFBRCxDQUFOLENBQWF3RCxJQUFiLENBQWtCLFlBQWxCLENBQVY7O0FBQTBDLFlBQUcsT0FBTzBLLEtBQVAsSUFBYyxXQUFqQixFQUE2QjtBQUFDQSxlQUFLLEdBQUMsSUFBTjtBQUFZOztBQUNwRixZQUFJQyxVQUFVLEdBQUNuTyxNQUFNLENBQUMsSUFBRCxDQUFOLENBQWF3RCxJQUFiLENBQWtCLGlCQUFsQixDQUFmOztBQUFvRCxZQUFHLE9BQU8ySyxVQUFQLElBQW1CLFdBQXRCLEVBQWtDO0FBQUNBLG9CQUFVLEdBQUMsSUFBWDtBQUFpQjs7QUFDeEcsWUFBR0EsVUFBVSxJQUFFLENBQWYsRUFDQTtBQUFDQSxvQkFBVSxHQUFDLElBQVg7QUFBaUIsU0FEbEIsTUFHQTtBQUFDQSxvQkFBVSxHQUFDLEtBQVg7QUFBa0I7O0FBQ25Cbk8sY0FBTSxDQUFDLElBQUQsQ0FBTixDQUFhaXpCLFdBQWIsQ0FBeUI7QUFBQ3RuQixjQUFJLEVBQUMsSUFBTjtBQUFXdW5CLGdCQUFNLEVBQUMsSUFBbEI7QUFBdUJuUixlQUFLLEVBQUMsQ0FBN0I7QUFBK0J0WCxnQkFBTSxFQUFDLENBQXRDO0FBQXdDdUIsa0JBQVEsRUFBQ2hCLFFBQWpEO0FBQTBEMFQsY0FBSSxFQUFDdlEsVUFBL0Q7QUFBMEVnbEIseUJBQWUsRUFBQ2psQixLQUExRjtBQUFnR2tsQixvQkFBVSxFQUFDLEdBQTNHO0FBQStHQyxvQkFBVSxFQUFDO0FBQUMsZUFBRTtBQUFDdFIsbUJBQUssRUFBQztBQUFQLGFBQUg7QUFBYSxpQkFBSTtBQUFDQSxtQkFBSyxFQUFDO0FBQVAsYUFBakI7QUFBMkIsa0JBQUs7QUFBQ0EsbUJBQUssRUFBQztBQUFQO0FBQWhDO0FBQTFILFNBQXpCO0FBQWlNLE9BWCtiO0FBVzViLEtBWG1VO0FBV2hVLEdBL2NxTztBQStjbE8sQ0EvY3FOLEVBK2NuTi9oQixNQS9jbU4sRSIsImZpbGUiOiIwLmpzIiwic291cmNlc0NvbnRlbnQiOlsiZnVuY3Rpb24gaXNUb3VjaERldmljZSgpe3JldHVybidvbnRvdWNoc3RhcnQnaW4gd2luZG93fHwnb25tc2dlc3R1cmVjaGFuZ2UnaW4gd2luZG93O31cbmZ1bmN0aW9uIGlzTW9iaWxlRGV2aWNlKCl7cmV0dXJuKHR5cGVvZiB3aW5kb3cub3JpZW50YXRpb24hPT1cInVuZGVmaW5lZFwiKXx8KG5hdmlnYXRvci51c2VyQWdlbnQuaW5kZXhPZignSUVNb2JpbGUnKSE9PS0xKTt9O2pRdWVyeS5mbi5pc0luVmlld3BvcnQ9ZnVuY3Rpb24oKXt2YXIgZWxlbWVudFRvcD1qUXVlcnkodGhpcykub2Zmc2V0KCkudG9wO3ZhciBlbGVtZW50Qm90dG9tPWVsZW1lbnRUb3AralF1ZXJ5KHRoaXMpLm91dGVySGVpZ2h0KCk7dmFyIHZpZXdwb3J0VG9wPWpRdWVyeSh3aW5kb3cpLnNjcm9sbFRvcCgpO3ZhciB2aWV3cG9ydEJvdHRvbT12aWV3cG9ydFRvcCtqUXVlcnkod2luZG93KS5oZWlnaHQoKTtyZXR1cm4gZWxlbWVudEJvdHRvbT52aWV3cG9ydFRvcCYmZWxlbWVudFRvcDx2aWV3cG9ydEJvdHRvbTt9OyhmdW5jdGlvbigkKXskKHdpbmRvdykub24oJ2VsZW1lbnRvci9mcm9udGVuZC9pbml0JyxmdW5jdGlvbigpe3ZhciBpc1RvdWNoPSgnb250b3VjaHN0YXJ0J2luIGRvY3VtZW50LmRvY3VtZW50RWxlbWVudCk7aWYoaXNUb3VjaCl7alF1ZXJ5KCcjZm9vdGVyLXdyYXBwZXInKS5jc3MoJ292ZXJmbG93LXgnLCdoaWRkZW4nKTt9XG5qUXVlcnkoXCJpbWcubGF6eVwiKS5lYWNoKGZ1bmN0aW9uKCl7dmFyIGN1cnJlbnRJbWc9alF1ZXJ5KHRoaXMpO2pRdWVyeSh0aGlzKS5MYXp5KHtvbkZpbmlzaGVkQWxsOmZ1bmN0aW9uKCl7Y3VycmVudEltZy5wYXJlbnQoXCJkaXYucG9zdC1mZWF0dXJlZC1pbWFnZS1ob3ZlclwiKS5yZW1vdmVDbGFzcyhcImxhenlcIik7Y3VycmVudEltZy5wYXJlbnQoJy5ncmFuZHJlc3RhdXJhbnRfZ2FsbGVyeV9saWdodGJveCcpLnBhcmVudChcImRpdi5nYWxsZXJ5LWdyaWQtaXRlbVwiKS5yZW1vdmVDbGFzcyhcImxhenlcIik7fX0pO30pO2pRdWVyeSgnI3BhZ2UtY29udGVudC13cmFwcGVyIC5lbGVtZW50b3Itd2lkZ2V0LWltYWdlLmFuaW1hdGlvbicpLmVhY2goZnVuY3Rpb24oKXtqUXVlcnkodGhpcykuc21vb3ZlKHtvZmZzZXQ6JzMwJSd9KTt9KTt2YXIgYm9keUJHQ29sb3I9alF1ZXJ5KCdib2R5JykuY3NzKCdiYWNrZ3JvdW5kLWNvbG9yJyk7aWYoYm9keUJHQ29sb3IhPScnKVxue2pRdWVyeSgnI3dyYXBwZXInKS5jc3MoJ2JhY2tncm91bmQtY29sb3InLGJvZHlCR0NvbG9yKTt9XG5pZihwYXJzZUludChqUXVlcnkod2luZG93KS53aWR0aCgpKTw1MDEpXG57alF1ZXJ5KFwic2VjdGlvbi5lbGVtZW50b3Itc2VjdGlvbi1oZWlnaHQtbWluLWhlaWdodCAuZWxlbWVudG9yLWNvbnRhaW5lclwiKS5lYWNoKGZ1bmN0aW9uKCl7alF1ZXJ5KHRoaXMpLmhlaWdodCgnYXV0bycpO30pO31cbmpRdWVyeSh3aW5kb3cpLnJlc2l6ZShmdW5jdGlvbigpe2pRdWVyeShcInNlY3Rpb24uZWxlbWVudG9yLXNlY3Rpb24taGVpZ2h0LW1pbi1oZWlnaHQgLmVsZW1lbnRvci1jb250YWluZXJcIikuZWFjaChmdW5jdGlvbigpe3ZhciBjdXJyZW50U2VjdGlvbj1qUXVlcnkodGhpcyk7aWYocGFyc2VJbnQoalF1ZXJ5KHdpbmRvdykud2lkdGgoKSk8NTAxKVxue2N1cnJlbnRTZWN0aW9uLmhlaWdodCgnYXV0bycpO31cbmVsc2VcbntjdXJyZW50U2VjdGlvbi5oZWlnaHQoJycpO319KTt9KTtlbGVtZW50b3JGcm9udGVuZC5ob29rcy5hZGRBY3Rpb24oJ2Zyb250ZW5kL2VsZW1lbnRfcmVhZHkvZ2xvYmFsJyxmdW5jdGlvbigkc2NvcGUpe2lmKGVsZW1lbnRvckZyb250ZW5kLmlzRWRpdE1vZGUoKSlcbnt2YXIgZWxlbWVudFNldHRpbmdzPXt9O3ZhciBtb2RlbENJRD0kc2NvcGUuZGF0YSgnbW9kZWwtY2lkJyk7dmFyIHNldHRpbmdzPWVsZW1lbnRvckZyb250ZW5kLmNvbmZpZy5lbGVtZW50cy5kYXRhW21vZGVsQ0lEXTtpZih0eXBlb2Ygc2V0dGluZ3MhPSd1bmRlZmluZWQnKVxue3ZhciB0eXBlPXNldHRpbmdzLmF0dHJpYnV0ZXMud2lkZ2V0VHlwZXx8c2V0dGluZ3MuYXR0cmlidXRlcy5lbFR5cGUsc2V0dGluZ3NLZXlzPWVsZW1lbnRvckZyb250ZW5kLmNvbmZpZy5lbGVtZW50cy5rZXlzW3R5cGVdO2lmKCFzZXR0aW5nc0tleXMpe3NldHRpbmdzS2V5cz1lbGVtZW50b3JGcm9udGVuZC5jb25maWcuZWxlbWVudHMua2V5c1t0eXBlXT1bXTtqUXVlcnkuZWFjaChzZXR0aW5ncy5jb250cm9scyxmdW5jdGlvbihuYW1lLGNvbnRyb2wpe2lmKGNvbnRyb2wuZnJvbnRlbmRfYXZhaWxhYmxlKXtzZXR0aW5nc0tleXMucHVzaChuYW1lKTt9fSk7fVxualF1ZXJ5LmVhY2goc2V0dGluZ3MuZ2V0QWN0aXZlQ29udHJvbHMoKSxmdW5jdGlvbihjb250cm9sS2V5KXtpZigtMSE9PXNldHRpbmdzS2V5cy5pbmRleE9mKGNvbnRyb2xLZXkpKXtlbGVtZW50U2V0dGluZ3NbY29udHJvbEtleV09c2V0dGluZ3MuYXR0cmlidXRlc1tjb250cm9sS2V5XTt9fSk7dmFyIHdpZGdldEV4dD1lbGVtZW50U2V0dGluZ3M7fX1cbmVsc2Vcbnt2YXIgd2lkZ2V0RXh0T2JqPSRzY29wZS5hdHRyKCdkYXRhLXNldHRpbmdzJyk7aWYodHlwZW9mIHdpZGdldEV4dE9iaiE9J3VuZGVmaW5lZCcpXG57dmFyIHdpZGdldEV4dD1KU09OLnBhcnNlKHdpZGdldEV4dE9iaik7fX1cbmlmKHR5cGVvZiB3aWRnZXRFeHQhPSd1bmRlZmluZWQnKVxue2lmKHR5cGVvZiB3aWRnZXRFeHQuZ3JhbmRyZXN0YXVyYW50X2V4dF9pc19iYWNrZ3JvdW5kX3BhcmFsbGF4IT0ndW5kZWZpbmVkJyYmd2lkZ2V0RXh0LmdyYW5kcmVzdGF1cmFudF9leHRfaXNfYmFja2dyb3VuZF9wYXJhbGxheD09J3RydWUnKVxue2lmKHR5cGVvZiB3aWRnZXRFeHQuYmFja2dyb3VuZF9iYWNrZ3JvdW5kIT0ndW5kZWZpbmVkJyYmd2lkZ2V0RXh0LmJhY2tncm91bmRfYmFja2dyb3VuZD09J2NsYXNzaWMnKVxue2lmKCFlbGVtZW50b3JGcm9udGVuZC5pc0VkaXRNb2RlKCkpXG57dmFyIHdpZGdldEJnPSRzY29wZS5jc3MoJ2JhY2tncm91bmQtaW1hZ2UnKTt3aWRnZXRCZz13aWRnZXRCZy5yZXBsYWNlKCd1cmwoJywnJykucmVwbGFjZSgnKScsJycpLnJlcGxhY2UoL1xcXCIvZ2ksXCJcIik7aWYod2lkZ2V0QmchPScnKVxue3ZhciBqYXJhbGxheFNjcm9sbFNwZWVkPTAuNTtpZih0eXBlb2Ygd2lkZ2V0RXh0LmdyYW5kcmVzdGF1cmFudF9leHRfaXNfYmFja2dyb3VuZF9wYXJhbGxheF9zcGVlZC5zaXplIT0ndW5kZWZpbmVkJylcbntqYXJhbGxheFNjcm9sbFNwZWVkPXBhcnNlRmxvYXQod2lkZ2V0RXh0LmdyYW5kcmVzdGF1cmFudF9leHRfaXNfYmFja2dyb3VuZF9wYXJhbGxheF9zcGVlZC5zaXplKTt9XG4kc2NvcGUuYWRkQ2xhc3MoJ2phcmFsbGF4Jyk7JHNjb3BlLmFwcGVuZCgnPGltZyBjbGFzcz1cImphcmFsbGF4LWltZ1wiIHNyYz1cIicrd2lkZ2V0QmcrJ1wiLz4nKTskc2NvcGUuamFyYWxsYXgoe3NwZWVkOmphcmFsbGF4U2Nyb2xsU3BlZWR9KTtpZighaXNNb2JpbGVEZXZpY2UoKSlcbnskc2NvcGUuY3NzKCdiYWNrZ3JvdW5kLWltYWdlJywnbm9uZScpO31cbmpRdWVyeSh3aW5kb3cpLnJlc2l6ZShmdW5jdGlvbigpe2lmKCFpc01vYmlsZURldmljZSgpKVxueyRzY29wZS5jc3MoJ2JhY2tncm91bmQtaW1hZ2UnLCdub25lJyk7fVxuZWxzZVxueyRzY29wZS5jc3MoJ2JhY2tncm91bmQtaW1hZ2UnLCd1cmwoJyt3aWRnZXRCZysnKScpO319KTt9fX19XG5pZih0eXBlb2Ygd2lkZ2V0RXh0LmdyYW5kcmVzdGF1cmFudF9leHRfaXNfZmFkZW91dF9hbmltYXRpb24hPSd1bmRlZmluZWQnJiZ3aWRnZXRFeHQuZ3JhbmRyZXN0YXVyYW50X2V4dF9pc19mYWRlb3V0X2FuaW1hdGlvbj09J3RydWUnKVxue3ZhciBzY3JvbGxWZWxvY2l0eT1wYXJzZUZsb2F0KHdpZGdldEV4dC5ncmFuZHJlc3RhdXJhbnRfZXh0X2lzX2ZhZGVvdXRfYW5pbWF0aW9uX3ZlbG9jaXR5LnNpemUpO3ZhciBzY3JvbGxEaXJlY3Rpb249d2lkZ2V0RXh0LmdyYW5kcmVzdGF1cmFudF9leHRfaXNfZmFkZW91dF9hbmltYXRpb25fZGlyZWN0aW9uO2pRdWVyeSh3aW5kb3cpLnNjcm9sbChmdW5jdGlvbihpKXt2YXIgc2Nyb2xsVmFyPWpRdWVyeSh3aW5kb3cpLnNjcm9sbFRvcCgpO3ZhciBzY3JvbGxQeD0tKHNjcm9sbFZlbG9jaXR5KnNjcm9sbFZhcik7aWYoc2Nyb2xsRGlyZWN0aW9uPT0ndXAnKVxue3Njcm9sbFB4PS0oc2Nyb2xsVmVsb2NpdHkqc2Nyb2xsVmFyKTt9XG5lbHNlIGlmKHNjcm9sbERpcmVjdGlvbj09J2Rvd24nKVxue3Njcm9sbFB4PXNjcm9sbFZlbG9jaXR5KnNjcm9sbFZhcjt9XG5lbHNlXG57c2Nyb2xsUHg9MDt9XG4kc2NvcGUuZmluZCgnLmVsZW1lbnRvci13aWRnZXQtY29udGFpbmVyJykuY3NzKHsndHJhbnNmb3JtJzpcInRyYW5zbGF0ZVkoXCIrc2Nyb2xsUHgrXCJweClcIn0pOyRzY29wZS5maW5kKCcuZWxlbWVudG9yLXdpZGdldC1jb250YWluZXInKS5jc3MoeydvcGFjaXR5JzooMTAwLShzY3JvbGxWYXIvNCkpLzEwMH0pO30pfVxuaWYodHlwZW9mIHdpZGdldEV4dC5ncmFuZHJlc3RhdXJhbnRfZXh0X2lzX3Njcm9sbG1lIT0ndW5kZWZpbmVkJyYmd2lkZ2V0RXh0LmdyYW5kcmVzdGF1cmFudF9leHRfaXNfc2Nyb2xsbWU9PSd0cnVlJylcbnt2YXIgc2Nyb2xsQXJncz17fTtpZih0eXBlb2Ygd2lkZ2V0RXh0LmdyYW5kcmVzdGF1cmFudF9leHRfc2Nyb2xsbWVfc2NhbGV4LnNpemUhPSd1bmRlZmluZWQnJiZ3aWRnZXRFeHQuZ3JhbmRyZXN0YXVyYW50X2V4dF9zY3JvbGxtZV9zY2FsZXguc2l6ZSE9MSlcbntzY3JvbGxBcmdzWydzY2FsZVgnXT13aWRnZXRFeHQuZ3JhbmRyZXN0YXVyYW50X2V4dF9zY3JvbGxtZV9zY2FsZXguc2l6ZTt9XG5pZih0eXBlb2Ygd2lkZ2V0RXh0LmdyYW5kcmVzdGF1cmFudF9leHRfc2Nyb2xsbWVfc2NhbGV5LnNpemUhPSd1bmRlZmluZWQnJiZ3aWRnZXRFeHQuZ3JhbmRyZXN0YXVyYW50X2V4dF9zY3JvbGxtZV9zY2FsZXkuc2l6ZSE9MSlcbntzY3JvbGxBcmdzWydzY2FsZVknXT13aWRnZXRFeHQuZ3JhbmRyZXN0YXVyYW50X2V4dF9zY3JvbGxtZV9zY2FsZXkuc2l6ZTt9XG5pZih0eXBlb2Ygd2lkZ2V0RXh0LmdyYW5kcmVzdGF1cmFudF9leHRfc2Nyb2xsbWVfc2NhbGV6LnNpemUhPSd1bmRlZmluZWQnJiZ3aWRnZXRFeHQuZ3JhbmRyZXN0YXVyYW50X2V4dF9zY3JvbGxtZV9zY2FsZXouc2l6ZSE9MSlcbntzY3JvbGxBcmdzWydzY2FsZVonXT13aWRnZXRFeHQuZ3JhbmRyZXN0YXVyYW50X2V4dF9zY3JvbGxtZV9zY2FsZXouc2l6ZTt9XG5pZih0eXBlb2Ygd2lkZ2V0RXh0LmdyYW5kcmVzdGF1cmFudF9leHRfc2Nyb2xsbWVfcm90YXRleC5zaXplIT0ndW5kZWZpbmVkJyYmd2lkZ2V0RXh0LmdyYW5kcmVzdGF1cmFudF9leHRfc2Nyb2xsbWVfcm90YXRleC5zaXplIT0wKVxue3Njcm9sbEFyZ3NbJ3JvdGF0ZVgnXT13aWRnZXRFeHQuZ3JhbmRyZXN0YXVyYW50X2V4dF9zY3JvbGxtZV9yb3RhdGV4LnNpemU7fVxuaWYodHlwZW9mIHdpZGdldEV4dC5ncmFuZHJlc3RhdXJhbnRfZXh0X3Njcm9sbG1lX3JvdGF0ZXkuc2l6ZSE9J3VuZGVmaW5lZCcmJndpZGdldEV4dC5ncmFuZHJlc3RhdXJhbnRfZXh0X3Njcm9sbG1lX3JvdGF0ZXkuc2l6ZSE9MClcbntzY3JvbGxBcmdzWydyb3RhdGVZJ109d2lkZ2V0RXh0LmdyYW5kcmVzdGF1cmFudF9leHRfc2Nyb2xsbWVfcm90YXRleS5zaXplO31cbmlmKHR5cGVvZiB3aWRnZXRFeHQuZ3JhbmRyZXN0YXVyYW50X2V4dF9zY3JvbGxtZV9yb3RhdGV6LnNpemUhPSd1bmRlZmluZWQnJiZ3aWRnZXRFeHQuZ3JhbmRyZXN0YXVyYW50X2V4dF9zY3JvbGxtZV9yb3RhdGV6LnNpemUhPTApXG57c2Nyb2xsQXJnc1sncm90YXRlWSddPXdpZGdldEV4dC5ncmFuZHJlc3RhdXJhbnRfZXh0X3Njcm9sbG1lX3JvdGF0ZXouc2l6ZTt9XG5pZih0eXBlb2Ygd2lkZ2V0RXh0LmdyYW5kcmVzdGF1cmFudF9leHRfc2Nyb2xsbWVfdHJhbnNsYXRleC5zaXplIT0ndW5kZWZpbmVkJyYmd2lkZ2V0RXh0LmdyYW5kcmVzdGF1cmFudF9leHRfc2Nyb2xsbWVfdHJhbnNsYXRleC5zaXplIT0wKVxue3Njcm9sbEFyZ3NbJ3gnXT13aWRnZXRFeHQuZ3JhbmRyZXN0YXVyYW50X2V4dF9zY3JvbGxtZV90cmFuc2xhdGV4LnNpemU7fVxuaWYodHlwZW9mIHdpZGdldEV4dC5ncmFuZHJlc3RhdXJhbnRfZXh0X3Njcm9sbG1lX3RyYW5zbGF0ZXkuc2l6ZSE9J3VuZGVmaW5lZCcmJndpZGdldEV4dC5ncmFuZHJlc3RhdXJhbnRfZXh0X3Njcm9sbG1lX3RyYW5zbGF0ZXkuc2l6ZSE9MClcbntzY3JvbGxBcmdzWyd5J109d2lkZ2V0RXh0LmdyYW5kcmVzdGF1cmFudF9leHRfc2Nyb2xsbWVfdHJhbnNsYXRleS5zaXplO31cbmlmKHR5cGVvZiB3aWRnZXRFeHQuZ3JhbmRyZXN0YXVyYW50X2V4dF9zY3JvbGxtZV90cmFuc2xhdGV6LnNpemUhPSd1bmRlZmluZWQnJiZ3aWRnZXRFeHQuZ3JhbmRyZXN0YXVyYW50X2V4dF9zY3JvbGxtZV90cmFuc2xhdGV6LnNpemUhPTApXG57c2Nyb2xsQXJnc1sneiddPXdpZGdldEV4dC5ncmFuZHJlc3RhdXJhbnRfZXh0X3Njcm9sbG1lX3RyYW5zbGF0ZXouc2l6ZTt9XG5pZih0eXBlb2Ygd2lkZ2V0RXh0LmdyYW5kcmVzdGF1cmFudF9leHRfc2Nyb2xsbWVfc21vb3RobmVzcy5zaXplIT0ndW5kZWZpbmVkJylcbntzY3JvbGxBcmdzWydzbW9vdGhuZXNzJ109d2lkZ2V0RXh0LmdyYW5kcmVzdGF1cmFudF9leHRfc2Nyb2xsbWVfc21vb3RobmVzcy5zaXplO31cbiRzY29wZS5hdHRyKCdkYXRhLXBhcmFsbGF4JyxKU09OLnN0cmluZ2lmeShzY3JvbGxBcmdzKSk7aWYodHlwZW9mIHdpZGdldEV4dC5ncmFuZHJlc3RhdXJhbnRfZXh0X3Njcm9sbG1lX2Rpc2FibGUhPSd1bmRlZmluZWQnKVxue2lmKHdpZGdldEV4dC5ncmFuZHJlc3RhdXJhbnRfZXh0X3Njcm9sbG1lX2Rpc2FibGU9PSdtb2JpbGUnKVxue2lmKHBhcnNlSW50KGpRdWVyeSh3aW5kb3cpLndpZHRoKCkpPDUwMSlcbnskc2NvcGUuYWRkQ2xhc3MoJ25vYW5pbWF0aW9uJyk7fX1cbmlmKHdpZGdldEV4dC5ncmFuZHJlc3RhdXJhbnRfZXh0X3Njcm9sbG1lX2Rpc2FibGU9PSd0YWJsZXQnKVxue2lmKHBhcnNlSW50KGpRdWVyeSh3aW5kb3cpLndpZHRoKCkpPDc2OSlcbnskc2NvcGUuYWRkQ2xhc3MoJ25vYW5pbWF0aW9uJyk7fX1cbmpRdWVyeSh3aW5kb3cpLnJlc2l6ZShmdW5jdGlvbigpe2lmKHdpZGdldEV4dC5ncmFuZHJlc3RhdXJhbnRfZXh0X3Njcm9sbG1lX2Rpc2FibGU9PSdtb2JpbGUnKVxue2lmKGlzTW9iaWxlRGV2aWNlKCl8fHBhcnNlSW50KGpRdWVyeSh3aW5kb3cpLndpZHRoKCkpPDUwMSlcbnskc2NvcGUuYWRkQ2xhc3MoJ25vYW5pbWF0aW9uJyk7fVxuZWxzZVxueyRzY29wZS5yZW1vdmVDbGFzcygnbm9hbmltYXRpb24nKTt9fVxuaWYod2lkZ2V0RXh0LmdyYW5kcmVzdGF1cmFudF9leHRfc2Nyb2xsbWVfZGlzYWJsZT09J3RhYmxldCcpXG57aWYocGFyc2VJbnQoalF1ZXJ5KHdpbmRvdykud2lkdGgoKSk8NzY5KVxueyRzY29wZS5hZGRDbGFzcygnbm9hbmltYXRpb24nKTt9XG5lbHNlXG57JHNjb3BlLnJlbW92ZUNsYXNzKCdub2FuaW1hdGlvbicpO319fSk7fX1cbmlmKHR5cGVvZiB3aWRnZXRFeHQuZ3JhbmRyZXN0YXVyYW50X2V4dF9pc19zbW9vdmUhPSd1bmRlZmluZWQnJiZ3aWRnZXRFeHQuZ3JhbmRyZXN0YXVyYW50X2V4dF9pc19zbW9vdmU9PSd0cnVlJylcbnskc2NvcGUuYWRkQ2xhc3MoJ2luaXQtc21vb3ZlJyk7JHNjb3BlLnNtb292ZSh7bWluX3dpZHRoOnBhcnNlSW50KHdpZGdldEV4dC5ncmFuZHJlc3RhdXJhbnRfZXh0X3Ntb292ZV9kaXNhYmxlKSxzY2FsZVg6d2lkZ2V0RXh0LmdyYW5kcmVzdGF1cmFudF9leHRfc21vb3ZlX3NjYWxleC5zaXplLHNjYWxlWTp3aWRnZXRFeHQuZ3JhbmRyZXN0YXVyYW50X2V4dF9zbW9vdmVfc2NhbGV5LnNpemUscm90YXRlWDpwYXJzZUludCh3aWRnZXRFeHQuZ3JhbmRyZXN0YXVyYW50X2V4dF9zbW9vdmVfcm90YXRleC5zaXplKSsnZGVnJyxyb3RhdGVZOnBhcnNlSW50KHdpZGdldEV4dC5ncmFuZHJlc3RhdXJhbnRfZXh0X3Ntb292ZV9yb3RhdGV5LnNpemUpKydkZWcnLHJvdGF0ZVo6cGFyc2VJbnQod2lkZ2V0RXh0LmdyYW5kcmVzdGF1cmFudF9leHRfc21vb3ZlX3JvdGF0ZXouc2l6ZSkrJ2RlZycsbW92ZVg6cGFyc2VJbnQod2lkZ2V0RXh0LmdyYW5kcmVzdGF1cmFudF9leHRfc21vb3ZlX3RyYW5zbGF0ZXguc2l6ZSkrJ3B4Jyxtb3ZlWTpwYXJzZUludCh3aWRnZXRFeHQuZ3JhbmRyZXN0YXVyYW50X2V4dF9zbW9vdmVfdHJhbnNsYXRleS5zaXplKSsncHgnLG1vdmVaOnBhcnNlSW50KHdpZGdldEV4dC5ncmFuZHJlc3RhdXJhbnRfZXh0X3Ntb292ZV90cmFuc2xhdGV6LnNpemUpKydweCcsc2tld1g6cGFyc2VJbnQod2lkZ2V0RXh0LmdyYW5kcmVzdGF1cmFudF9leHRfc21vb3ZlX3NrZXd4LnNpemUpKydkZWcnLHNrZXdZOnBhcnNlSW50KHdpZGdldEV4dC5ncmFuZHJlc3RhdXJhbnRfZXh0X3Ntb292ZV9za2V3eS5zaXplKSsnZGVnJyxwZXJzcGVjdGl2ZTpwYXJzZUludCh3aWRnZXRFeHQuZ3JhbmRyZXN0YXVyYW50X2V4dF9zbW9vdmVfcGVyc3BlY3RpdmUuc2l6ZSksb2Zmc2V0OictMTAlJyx9KTtpZih0eXBlb2Ygd2lkZ2V0RXh0LmdyYW5kcmVzdGF1cmFudF9leHRfc21vb3ZlX2R1cmF0aW9uIT0ndW5kZWZpbmVkJylcbnskc2NvcGUuY3NzKCd0cmFuc2l0aW9uLWR1cmF0aW9uJyxwYXJzZUludCh3aWRnZXRFeHQuZ3JhbmRyZXN0YXVyYW50X2V4dF9zbW9vdmVfZHVyYXRpb24pKydtcycpO31cbnZhciB3aWR0aD1qUXVlcnkod2luZG93KS53aWR0aCgpO2lmKHdpZGdldEV4dC5ncmFuZHJlc3RhdXJhbnRfZXh0X3Ntb292ZV9kaXNhYmxlPj13aWR0aCl7aWYoISRzY29wZS5oYXNDbGFzcygnc21vb3ZlZCcpKVxueyRzY29wZS5hZGRDbGFzcygnbm8tc21vb3ZlZCcpO31cbnJldHVybiBmYWxzZTt9fVxuaWYodHlwZW9mIHdpZGdldEV4dC5ncmFuZHJlc3RhdXJhbnRfZXh0X2lzX3BhcmFsbGF4X21vdXNlIT0ndW5kZWZpbmVkJyYmd2lkZ2V0RXh0LmdyYW5kcmVzdGF1cmFudF9leHRfaXNfcGFyYWxsYXhfbW91c2U9PSd0cnVlJylcbnt2YXIgZWxlbWVudElEPSRzY29wZS5hdHRyKCdkYXRhLWlkJyk7JHNjb3BlLmZpbmQoJy5lbGVtZW50b3Itd2lkZ2V0LWNvbnRhaW5lcicpLmF0dHIoJ2RhdGEtZGVwdGgnLHBhcnNlRmxvYXQod2lkZ2V0RXh0LmdyYW5kcmVzdGF1cmFudF9leHRfaXNfcGFyYWxsYXhfbW91c2VfZGVwdGguc2l6ZSkpOyRzY29wZS5hdHRyKCdJRCcsJ3BhcmFsbGF4LScrZWxlbWVudElEKTt2YXIgcGFyZW50RWxlbWVudD1kb2N1bWVudC5nZXRFbGVtZW50QnlJZCgncGFyYWxsYXgtJytlbGVtZW50SUQpO3ZhciBwYXJhbGxheD1uZXcgUGFyYWxsYXgocGFyZW50RWxlbWVudCx7cmVsYXRpdmVJbnB1dDp0cnVlfSk7aWYoZWxlbWVudG9yRnJvbnRlbmQuaXNFZGl0TW9kZSgpKVxue2lmKCRzY29wZS53aWR0aCgpPT0wKVxueyRzY29wZS5jc3MoJ3dpZHRoJywnMTAwJScpO31cbmlmKCRzY29wZS5oZWlnaHQoKT09MClcbnskc2NvcGUuY3NzKCdoZWlnaHQnLCcxMDAlJyk7fX19XG5pZih0eXBlb2Ygd2lkZ2V0RXh0LmdyYW5kcmVzdGF1cmFudF9leHRfaXNfaW5maW5pdGUhPSd1bmRlZmluZWQnJiZ3aWRnZXRFeHQuZ3JhbmRyZXN0YXVyYW50X2V4dF9pc19pbmZpbml0ZT09J3RydWUnKVxue3ZhciBhbmltYXRpb25DbGFzcz0nJzt2YXIga2V5ZnJhbWVOYW1lPScnO3ZhciBhbmltYXRpb25DU1M9Jyc7aWYodHlwZW9mIHdpZGdldEV4dC5ncmFuZHJlc3RhdXJhbnRfZXh0X2luZmluaXRlX2FuaW1hdGlvbiE9J3VuZGVmaW5lZCcpXG57YW5pbWF0aW9uQ2xhc3M9d2lkZ2V0RXh0LmdyYW5kcmVzdGF1cmFudF9leHRfaW5maW5pdGVfYW5pbWF0aW9uO3N3aXRjaChhbmltYXRpb25DbGFzcyl7Y2FzZSdpZl9zd2luZzEnOmtleWZyYW1lTmFtZT0nc3dpbmcnO2JyZWFrO2Nhc2UnaWZfc3dpbmcyJzprZXlmcmFtZU5hbWU9J3N3aW5nMic7YnJlYWs7Y2FzZSdpZl93YXZlJzprZXlmcmFtZU5hbWU9J3dhdmUnO2JyZWFrO2Nhc2UnaWZfdGlsdCc6a2V5ZnJhbWVOYW1lPSd0aWx0JzticmVhaztjYXNlJ2lmX2JvdW5jZSc6a2V5ZnJhbWVOYW1lPSdib3VuY2UnO2JyZWFrO2Nhc2UnaWZfc2NhbGUnOmtleWZyYW1lTmFtZT0nc2NhbGUnO2JyZWFrO2Nhc2UnaWZfc3Bpbic6a2V5ZnJhbWVOYW1lPSdzcGluJzticmVhazt9XG5hbmltYXRpb25DU1MrPWtleWZyYW1lTmFtZSsnICc7fVxuaWYodHlwZW9mIHdpZGdldEV4dC5ncmFuZHJlc3RhdXJhbnRfZXh0X2luZmluaXRlX2R1cmF0aW9uIT0ndW5kZWZpbmVkJylcbnthbmltYXRpb25DU1MrPXdpZGdldEV4dC5ncmFuZHJlc3RhdXJhbnRfZXh0X2luZmluaXRlX2R1cmF0aW9uKydzICc7fVxuYW5pbWF0aW9uQ1NTKz0naW5maW5pdGUgYWx0ZXJuYXRlICc7aWYodHlwZW9mIHdpZGdldEV4dC5ncmFuZHJlc3RhdXJhbnRfZXh0X2luZmluaXRlX2Vhc2luZyE9J3VuZGVmaW5lZCcpXG57YW5pbWF0aW9uQ1NTKz0nY3ViaWMtYmV6aWVyKCcrd2lkZ2V0RXh0LmdyYW5kcmVzdGF1cmFudF9leHRfaW5maW5pdGVfZWFzaW5nKycpJzt9XG4kc2NvcGUuY3NzKHsnYW5pbWF0aW9uJzphbmltYXRpb25DU1MsfSk7JHNjb3BlLmFkZENsYXNzKGFuaW1hdGlvbkNsYXNzKTt9XG5pZih0eXBlb2Ygd2lkZ2V0RXh0LmdyYW5kcmVzdGF1cmFudF9leHRfaXNfYmFja2dyb3VuZF9vbl9zY3JvbGwhPSd1bmRlZmluZWQnJiZ3aWRnZXRFeHQuZ3JhbmRyZXN0YXVyYW50X2V4dF9pc19iYWNrZ3JvdW5kX29uX3Njcm9sbD09J3RydWUnKVxue3ZhciBib2R5QmFja2dyb3VuZD1qUXVlcnkoJ2JvZHknKS5jc3MoJ2JhY2tncm91bmQtY29sb3InKTt2YXIgcG9zaXRpb249alF1ZXJ5KHdpbmRvdykuc2Nyb2xsVG9wKCk7alF1ZXJ5KHdpbmRvdykub24oXCJzY3JvbGwgdG91Y2htb3ZlXCIsZnVuY3Rpb24oKXtjbGVhclRpbWVvdXQoJC5kYXRhKHRoaXMsJ3Njcm9sbFRpbWVyJykpOyQuZGF0YSh0aGlzLCdzY3JvbGxUaW1lcicsc2V0VGltZW91dChmdW5jdGlvbigpe2pRdWVyeSgnYm9keScpLmF0dHIoJ2RhdGEtc2Nyb2xsZW5kJyxqUXVlcnkod2luZG93KS5zY3JvbGxUb3AoKSk7fSwyNTApKTt2YXIgc2Nyb2xsPWpRdWVyeSh3aW5kb3cpLnNjcm9sbFRvcCgpO3ZhciBwb3NpdGlvbj1qUXVlcnkoJ2JvZHknKS5hdHRyKCdkYXRhLXNjcm9sbGVuZCcpO3ZhciB3aW5kb3dIZWlnaHQ9alF1ZXJ5KHdpbmRvdykuaGVpZ2h0KCk7dmFyIHdpbmRvd0hlaWdodE9mZnNldD1wYXJzZUludCh3aW5kb3dIZWlnaHQvMik7dmFyIGVsZW1lbnRUb3A9JHNjb3BlLnBvc2l0aW9uKCkudG9wLXdpbmRvd0hlaWdodE9mZnNldDt2YXIgZWxlbWVudEJvdHRvbT1lbGVtZW50VG9wKyRzY29wZS5vdXRlckhlaWdodCh0cnVlKTtpZihzY3JvbGw+cG9zaXRpb24pe2lmKGpRdWVyeShkb2N1bWVudCkuc2Nyb2xsVG9wKCk+PWVsZW1lbnRUb3AmJmpRdWVyeShkb2N1bWVudCkuc2Nyb2xsVG9wKCk8PWVsZW1lbnRCb3R0b20pe2pRdWVyeSgnI3dyYXBwZXInKS5jc3MoJ2JhY2tncm91bmQtY29sb3InLHdpZGdldEV4dC5ncmFuZHJlc3RhdXJhbnRfZXh0X2JhY2tncm91bmRfb25fc2Nyb2xsX2NvbG9yKTtqUXVlcnkoJy5lbGVtZW50b3Itc2hhcGUgLmVsZW1lbnRvci1zaGFwZS1maWxsJykuY3NzKCdmaWxsJyx3aWRnZXRFeHQuZ3JhbmRyZXN0YXVyYW50X2V4dF9iYWNrZ3JvdW5kX29uX3Njcm9sbF9jb2xvcik7fVxuaWYoalF1ZXJ5KGRvY3VtZW50KS5zY3JvbGxUb3AoKT5lbGVtZW50Qm90dG9tKXtqUXVlcnkoJyN3cmFwcGVyJykuY3NzKCdiYWNrZ3JvdW5kLWNvbG9yJyxib2R5QmFja2dyb3VuZCk7alF1ZXJ5KCcuZWxlbWVudG9yLXNoYXBlIC5lbGVtZW50b3Itc2hhcGUtZmlsbCcpLmNzcygnZmlsbCcsYm9keUJhY2tncm91bmQpO319ZWxzZXtpZihqUXVlcnkoZG9jdW1lbnQpLnNjcm9sbFRvcCgpPD1lbGVtZW50Qm90dG9tJiZqUXVlcnkoZG9jdW1lbnQpLnNjcm9sbFRvcCgpPj1lbGVtZW50VG9wKXtzZXRUaW1lb3V0KGZ1bmN0aW9uKCl7alF1ZXJ5KCcjd3JhcHBlcicpLmNzcygnYmFja2dyb3VuZC1jb2xvcicsd2lkZ2V0RXh0LmdyYW5kcmVzdGF1cmFudF9leHRfYmFja2dyb3VuZF9vbl9zY3JvbGxfY29sb3IpLnN0b3AoKTtqUXVlcnkoJy5lbGVtZW50b3Itc2hhcGUgLmVsZW1lbnRvci1zaGFwZS1maWxsJykuY3NzKCdmaWxsJyx3aWRnZXRFeHQuZ3JhbmRyZXN0YXVyYW50X2V4dF9iYWNrZ3JvdW5kX29uX3Njcm9sbF9jb2xvcikuc3RvcCgpO30sMTAwKTt9XG5pZihqUXVlcnkoZG9jdW1lbnQpLnNjcm9sbFRvcCgpPCRzY29wZS5wb3NpdGlvbigpLnRvcCl7alF1ZXJ5KCcjd3JhcHBlcicpLmNzcygnYmFja2dyb3VuZC1jb2xvcicsYm9keUJhY2tncm91bmQpO2pRdWVyeSgnLmVsZW1lbnRvci1zaGFwZSAuZWxlbWVudG9yLXNoYXBlLWZpbGwnKS5jc3MoJ2ZpbGwnLGJvZHlCYWNrZ3JvdW5kKTt9fX0pO31cbmlmKHR5cGVvZiB3aWRnZXRFeHQuZ3JhbmRyZXN0YXVyYW50X2V4dF9saW5rX3Jlc2VydmF0aW9uIT0ndW5kZWZpbmVkJyYmd2lkZ2V0RXh0LmdyYW5kcmVzdGF1cmFudF9leHRfbGlua19yZXNlcnZhdGlvbj09J3RydWUnKVxueyRzY29wZS5vbignY2xpY2snLGZ1bmN0aW9uKGUpe2UucHJldmVudERlZmF1bHQoKTtqUXVlcnkoJyNyZXNlcnZhdGlvbl93cmFwcGVyJykuZmFkZUluKCk7alF1ZXJ5KCdib2R5JykucmVtb3ZlQ2xhc3MoJ2pzX25hdicpO2pRdWVyeSgnYm9keScpLmFkZENsYXNzKCdvdmVyZmxvd19oaWRkZW4nKTtqUXVlcnkoJ2h0bWwnKS5hZGRDbGFzcygnb3ZlcmZsb3dfaGlkZGVuJyk7fSk7fVxuaWYodHlwZW9mIHdpZGdldEV4dC5ncmFuZHJlc3RhdXJhbnRfZXh0X2xpbmtfc2lkZW1lbnUhPSd1bmRlZmluZWQnJiZ3aWRnZXRFeHQuZ3JhbmRyZXN0YXVyYW50X2V4dF9saW5rX3NpZGVtZW51PT0ndHJ1ZScpXG57JHNjb3BlLm9uKCdjbGljaycsZnVuY3Rpb24oZSl7ZS5wcmV2ZW50RGVmYXVsdCgpO2pRdWVyeSgnYm9keSxodG1sJykuYW5pbWF0ZSh7c2Nyb2xsVG9wOjB9LDEwMCk7alF1ZXJ5KCdib2R5JykudG9nZ2xlQ2xhc3MoJ2pzX25hdicpO30pO319fSk7ZWxlbWVudG9yRnJvbnRlbmQuaG9va3MuYWRkQWN0aW9uKCdmcm9udGVuZC9lbGVtZW50X3JlYWR5L2dyYW5kcmVzdGF1cmFudC1ibG9nLXBvc3RzLmRlZmF1bHQnLGZ1bmN0aW9uKCRzY29wZSl7alF1ZXJ5KGZ1bmN0aW9uKCQpe2pRdWVyeShcImltZy5sYXp5XCIpLmVhY2goZnVuY3Rpb24oKXt2YXIgY3VycmVudEltZz1qUXVlcnkodGhpcyk7alF1ZXJ5KHRoaXMpLkxhenkoe29uRmluaXNoZWRBbGw6ZnVuY3Rpb24oKXtjdXJyZW50SW1nLnBhcmVudChcImRpdi5wb3N0LWZlYXR1cmVkLWltYWdlLWhvdmVyXCIpLnJlbW92ZUNsYXNzKFwibGF6eVwiKTt9LH0pO30pO2pRdWVyeShcIi5sYXlvdXQtbWFzb25yeVwiKS5lYWNoKGZ1bmN0aW9uKCl7dmFyIGdyaWQ9alF1ZXJ5KHRoaXMpO2dyaWQuaW1hZ2VzTG9hZGVkKCkuZG9uZShmdW5jdGlvbigpe2dyaWQubWFzb25yeSh7aXRlbVNlbGVjdG9yOlwiLmJsb2ctcG9zdHMtbWFzb25yeVwiLGNvbHVtbldpZHRoOlwiLmJsb2ctcG9zdHMtbWFzb25yeVwiLGd1dHRlcjo0NX0pO2pRdWVyeShcIi5sYXlvdXQtbWFzb25yeSAuYmxvZy1wb3N0cy1tYXNvbnJ5XCIpLmVhY2goZnVuY3Rpb24oaW5kZXgpe3NldFRpbWVvdXQoZnVuY3Rpb24oKXtqUXVlcnkoXCIubGF5b3V0LW1hc29ucnkgLmJsb2ctcG9zdHMtbWFzb25yeVwiKS5lcShpbmRleCkuYWRkQ2xhc3MoXCJpcy1zaG93aW5nXCIpO30sMjUwKmluZGV4KTt9KTt9KTtqUXVlcnkoXCIubGF5b3V0LW1hc29ucnkgaW1nLmxhenlfbWFzb25yeVwiKS5lYWNoKGZ1bmN0aW9uKCl7dmFyIGN1cnJlbnRJbWc9alF1ZXJ5KHRoaXMpO2N1cnJlbnRJbWcucGFyZW50KFwiZGl2LnBvc3QtZmVhdHVyZWQtaW1hZ2UtaG92ZXJcIikucmVtb3ZlQ2xhc3MoXCJsYXp5XCIpO2pRdWVyeSh0aGlzKS5MYXp5KHtvbkZpbmlzaGVkQWxsOmZ1bmN0aW9uKCl7Z3JpZC5tYXNvbnJ5KHtpdGVtU2VsZWN0b3I6XCIuYmxvZy1wb3N0cy1tYXNvbnJ5XCIsY29sdW1uV2lkdGg6XCIuYmxvZy1wb3N0cy1tYXNvbnJ5XCIsZ3V0dGVyOjQ1fSk7fSx9KTt9KTt9KTtqUXVlcnkoXCIubGF5b3V0LW1ldHJvX21hc29ucnlcIikuZWFjaChmdW5jdGlvbigpe3ZhciBncmlkPWpRdWVyeSh0aGlzKTtncmlkLmltYWdlc0xvYWRlZCgpLmRvbmUoZnVuY3Rpb24oKXtncmlkLm1hc29ucnkoe2l0ZW1TZWxlY3RvcjpcIi5ibG9nLXBvc3RzLW1ldHJvXCIsY29sdW1uV2lkdGg6XCIuYmxvZy1wb3N0cy1tZXRyb1wiLGd1dHRlcjo0MH0pO2pRdWVyeShcIi5sYXlvdXQtbWV0cm9fbWFzb25yeSAuYmxvZy1wb3N0cy1tZXRyb1wiKS5lYWNoKGZ1bmN0aW9uKGluZGV4KXtzZXRUaW1lb3V0KGZ1bmN0aW9uKCl7alF1ZXJ5KFwiLmxheW91dC1tZXRyb19tYXNvbnJ5IC5ibG9nLXBvc3RzLW1ldHJvXCIpLmVxKGluZGV4KS5hZGRDbGFzcyhcImlzLXNob3dpbmdcIik7fSwxMDAqaW5kZXgpO30pO30pO2pRdWVyeShcIi5wb3N0LW1ldHJvLWxlZnQtd3JhcHBlciBpbWcubGF6eV9tYXNvbnJ5LCAubGF5b3V0LW1ldHJvX21hc29ucnkgaW1nLmxhenlfbWFzb25yeVwiKS5lYWNoKGZ1bmN0aW9uKCl7dmFyIGN1cnJlbnRJbWc9alF1ZXJ5KHRoaXMpO2N1cnJlbnRJbWcucGFyZW50KFwiZGl2LnBvc3QtZmVhdHVyZWQtaW1hZ2UtaG92ZXJcIikucmVtb3ZlQ2xhc3MoXCJsYXp5XCIpO2pRdWVyeSh0aGlzKS5MYXp5KHtvbkZpbmlzaGVkQWxsOmZ1bmN0aW9uKCl7Z3JpZC5tYXNvbnJ5KHtpdGVtU2VsZWN0b3I6XCIuYmxvZy1wb3N0cy1tZXRyb1wiLGNvbHVtbldpZHRoOlwiLmJsb2ctcG9zdHMtbWV0cm9cIixndXR0ZXI6NDB9KTt9LH0pO30pO30pO3ZhciBtZW51TGF5b3V0PWpRdWVyeSgnI3BwX21lbnVfbGF5b3V0JykudmFsKCk7aWYobWVudUxheW91dCE9J2xlZnRtZW51JylcbntqUXVlcnkoXCIucG9zdC1tZXRyby1sZWZ0LXdyYXBwZXJcIikuc3RpY2tfaW5fcGFyZW50KHtvZmZzZXRfdG9wOjEyMH0pO31cbmVsc2VcbntqUXVlcnkoXCIucG9zdC1tZXRyby1sZWZ0LXdyYXBwZXJcIikuc3RpY2tfaW5fcGFyZW50KHtvZmZzZXRfdG9wOjQwfSk7fVxuaWYoalF1ZXJ5KHdpbmRvdykud2lkdGgoKTw9NzY4fHxpc1RvdWNoRGV2aWNlKCkpXG57alF1ZXJ5KFwiLnBvc3QtbWV0cm8tbGVmdC13cmFwcGVyXCIpLnRyaWdnZXIoXCJzdGlja3lfa2l0OmRldGFjaFwiKTt9fSk7fSk7ZWxlbWVudG9yRnJvbnRlbmQuaG9va3MuYWRkQWN0aW9uKCdmcm9udGVuZC9lbGVtZW50X3JlYWR5L2dyYW5kcmVzdGF1cmFudC1nYWxsZXJ5LWdyaWQuZGVmYXVsdCcsZnVuY3Rpb24oJHNjb3BlKXtqUXVlcnkoXCJpbWcubGF6eVwiKS5lYWNoKGZ1bmN0aW9uKCl7dmFyIGN1cnJlbnRJbWc9alF1ZXJ5KHRoaXMpO2pRdWVyeSh0aGlzKS5MYXp5KHtvbkZpbmlzaGVkQWxsOmZ1bmN0aW9uKCl7Y3VycmVudEltZy5wYXJlbnQoXCJkaXYucG9zdC1mZWF0dXJlZC1pbWFnZS1ob3ZlclwiKS5yZW1vdmVDbGFzcyhcImxhenlcIik7Y3VycmVudEltZy5wYXJlbnQoJy5ncmFuZHJlc3RhdXJhbnRfZ2FsbGVyeV9saWdodGJveCcpLnBhcmVudChcImRpdi5nYWxsZXJ5LWdyaWQtaXRlbVwiKS5yZW1vdmVDbGFzcyhcImxhenlcIik7Y3VycmVudEltZy5wYXJlbnQoXCJkaXYuZ2FsbGVyeS1ncmlkLWl0ZW1cIikucmVtb3ZlQ2xhc3MoXCJsYXp5XCIpO319KTt9KTt9KTtlbGVtZW50b3JGcm9udGVuZC5ob29rcy5hZGRBY3Rpb24oJ2Zyb250ZW5kL2VsZW1lbnRfcmVhZHkvZ3JhbmRyZXN0YXVyYW50LWdhbGxlcnktbWFzb25yeS5kZWZhdWx0JyxmdW5jdGlvbigkc2NvcGUpe2pRdWVyeShmdW5jdGlvbigkKXtqUXVlcnkoXCIuZ3JhbmRyZXN0YXVyYW50LWdhbGxlcnktZ3JpZC1jb250ZW50LXdyYXBwZXIuZG8tbWFzb25yeVwiKS5lYWNoKGZ1bmN0aW9uKCl7dmFyIGdyaWQ9alF1ZXJ5KHRoaXMpO3ZhciBjb2xzPWdyaWQuYXR0cignZGF0YS1jb2xzJyk7aWYoIWdyaWQuaGFzQ2xhc3MoJ2hhcy1uby1zcGFjZScpKVxue3ZhciBndXR0ZXI9MzA7aWYoY29scz40KVxue2d1dHRlcj0yMDt9fVxuZWxzZVxue2d1dHRlcj0wO31cbmdyaWQuaW1hZ2VzTG9hZGVkKCkuZG9uZShmdW5jdGlvbigpe2dyaWQubWFzb25yeSh7aXRlbVNlbGVjdG9yOlwiLmdhbGxlcnktZ3JpZC1pdGVtXCIsY29sdW1uV2lkdGg6XCIuZ2FsbGVyeS1ncmlkLWl0ZW1cIixndXR0ZXI6Z3V0dGVyfSk7alF1ZXJ5KFwiLmdyYW5kcmVzdGF1cmFudC1nYWxsZXJ5LWdyaWQtY29udGVudC13cmFwcGVyLmRvLW1hc29ucnkgLmdhbGxlcnktZ3JpZC1pdGVtXCIpLmVhY2goZnVuY3Rpb24oaW5kZXgpe3NldFRpbWVvdXQoZnVuY3Rpb24oKXtqUXVlcnkoXCIuZG8tbWFzb25yeSAuZ2FsbGVyeS1ncmlkLWl0ZW1cIikuZXEoaW5kZXgpLmFkZENsYXNzKFwiaXMtc2hvd2luZ1wiKTt9LDEwMCppbmRleCk7fSk7fSk7alF1ZXJ5KFwiLmdyYW5kcmVzdGF1cmFudC1nYWxsZXJ5LWdyaWQtY29udGVudC13cmFwcGVyLmRvLW1hc29ucnkgaW1nLmxhenlfbWFzb25yeVwiKS5lYWNoKGZ1bmN0aW9uKCl7dmFyIGN1cnJlbnRJbWc9alF1ZXJ5KHRoaXMpO2N1cnJlbnRJbWcucGFyZW50KFwiZGl2LnBvc3QtZmVhdHVyZWQtaW1hZ2UtaG92ZXJcIikucmVtb3ZlQ2xhc3MoXCJsYXp5XCIpO3ZhciBjb2xzPWdyaWQuYXR0cignZGF0YS1jb2xzJyk7aWYoIWdyaWQuaGFzQ2xhc3MoJ2hhcy1uby1zcGFjZScpKVxue3ZhciBndXR0ZXI9NDA7aWYoY29scz40KVxue2d1dHRlcj0zMDt9fVxuZWxzZVxue2d1dHRlcj0wO31cbmpRdWVyeSh0aGlzKS5MYXp5KHtvbkZpbmlzaGVkQWxsOmZ1bmN0aW9uKCl7Z3JpZC5tYXNvbnJ5KHtpdGVtU2VsZWN0b3I6XCIuZ2FsbGVyeS1ncmlkLWl0ZW1cIixjb2x1bW5XaWR0aDpcIi5nYWxsZXJ5LWdyaWQtaXRlbVwiLGd1dHRlcjpndXR0ZXJ9KTt9LH0pO30pO30pO30pO30pO2VsZW1lbnRvckZyb250ZW5kLmhvb2tzLmFkZEFjdGlvbignZnJvbnRlbmQvZWxlbWVudF9yZWFkeS9ncmFuZHJlc3RhdXJhbnQtZ2FsbGVyeS1qdXN0aWZpZWQuZGVmYXVsdCcsZnVuY3Rpb24oJHNjb3BlKXtqUXVlcnkoZnVuY3Rpb24oJCl7alF1ZXJ5KFwiaW1nLmxhenlcIikuZWFjaChmdW5jdGlvbigpe3ZhciBjdXJyZW50SW1nPWpRdWVyeSh0aGlzKTtqUXVlcnkodGhpcykuTGF6eSh7b25GaW5pc2hlZEFsbDpmdW5jdGlvbigpe2N1cnJlbnRJbWcucGFyZW50KFwiZGl2LnBvc3QtZmVhdHVyZWQtaW1hZ2UtaG92ZXJcIikucmVtb3ZlQ2xhc3MoXCJsYXp5XCIpO319KTt9KTtqUXVlcnkoXCIuZ3JhbmRyZXN0YXVyYW50LWdhbGxlcnktZ3JpZC1jb250ZW50LXdyYXBwZXIuZG8tanVzdGlmaWVkXCIpLmVhY2goZnVuY3Rpb24oKXt2YXIgZ3JpZD1qUXVlcnkodGhpcyk7dmFyIHJvd0hlaWdodD1ncmlkLmF0dHIoJ2RhdGEtcm93X2hlaWdodCcpO3ZhciBtYXJnaW49Z3JpZC5hdHRyKCdkYXRhLW1hcmdpbicpO3ZhciBqdXN0aWZ5TGFzdFJvdz1ncmlkLmF0dHIoJ2RhdGEtanVzdGlmeV9sYXN0X3JvdycpO3ZhciBqdXN0aWZ5TGFzdFJvd1N0cj0nbm9qdXN0aWZ5JztpZihqdXN0aWZ5TGFzdFJvdz09J3llcycpXG57anVzdGlmeUxhc3RSb3dTdHI9J2p1c3RpZnknO31cbmdyaWQuanVzdGlmaWVkR2FsbGVyeSh7cm93SGVpZ2h0OnJvd0hlaWdodCxtYXJnaW5zOm1hcmdpbixsYXN0Um93Omp1c3RpZnlMYXN0Um93U3RyfSk7fSk7fSk7fSk7ZWxlbWVudG9yRnJvbnRlbmQuaG9va3MuYWRkQWN0aW9uKCdmcm9udGVuZC9lbGVtZW50X3JlYWR5L2dyYW5kcmVzdGF1cmFudC1nYWxsZXJ5LWZ1bGxzY3JlZW4uZGVmYXVsdCcsZnVuY3Rpb24oJHNjb3BlKXtqUXVlcnkoZnVuY3Rpb24oJCl7alF1ZXJ5KCdib2R5JykuYWRkQ2xhc3MoJ2VsZW1lbnRvci1mdWxsc2NyZWVuJyk7dmFyIHNsaWRlc2hvdz1qUXVlcnkoJy5mdWxsc2NyZWVuLWdhbGxlcnknKTt2YXIgYXV0b1BsYXk9c2xpZGVzaG93LmF0dHIoJ2RhdGEtYXV0b3BsYXknKTt2YXIgYXV0b1BsYXlBcnI9ZmFsc2U7aWYodHlwZW9mIGF1dG9QbGF5IT1cInVuZGVmaW5lZFwiKXthdXRvUGxheUFycj17ZGVsYXk6YXV0b1BsYXl9O31cbnZhciBlZmZlY3Q9c2xpZGVzaG93LmF0dHIoJ2RhdGEtZWZmZWN0Jyk7aWYodHlwZW9mIGVmZmVjdD09XCJ1bmRlZmluZWRcIil7ZWZmZWN0PSdzbGlkZSc7fVxudmFyIHNwZWVkPXNsaWRlc2hvdy5hdHRyKCdkYXRhLXNwZWVkJyk7aWYodHlwZW9mIHNwZWVkPT1cInVuZGVmaW5lZFwiKXtzcGVlZD00MDA7fVxudmFyIGdhbGxlcnlUb3A9bmV3IFN3aXBlcignLmZ1bGxzY3JlZW4tZ2FsbGVyeScse25hdmlnYXRpb246e25leHRFbDonLnN3aXBlci1idXR0b24tbmV4dCcscHJldkVsOicuc3dpcGVyLWJ1dHRvbi1wcmV2Jyx9LHNwYWNlQmV0d2VlbjowLGtleWJvYXJkQ29udHJvbDp0cnVlLHNwZWVkOnBhcnNlSW50KHNwZWVkKSxsb29wOnRydWUsZWZmZWN0OmVmZmVjdCxncmFiQ3Vyc29yOnRydWUscHJlbG9hZEltYWdlczpmYWxzZSxsYXp5Ontsb2FkUHJldk5leHQ6dHJ1ZSx9LGF1dG9wbGF5OmF1dG9QbGF5QXJyfSk7fSk7fSk7ZWxlbWVudG9yRnJvbnRlbmQuaG9va3MuYWRkQWN0aW9uKCdmcm9udGVuZC9lbGVtZW50X3JlYWR5L2dyYW5kcmVzdGF1cmFudC1zbGlkZXItdmVydGljYWwtcGFyYWxsYXguZGVmYXVsdCcsZnVuY3Rpb24oJHNjb3BlKXtqUXVlcnkoZnVuY3Rpb24oJCl7alF1ZXJ5KCdib2R5JykuYWRkQ2xhc3MoJ2VsZW1lbnRvci1mdWxsc2NyZWVuJyk7dmFyIHRpY2tpbmc9ZmFsc2U7dmFyIGlzRmlyZWZveD0vRmlyZWZveC9pLnRlc3QobmF2aWdhdG9yLnVzZXJBZ2VudCk7dmFyIGlzSWU9L01TSUUvaS50ZXN0KG5hdmlnYXRvci51c2VyQWdlbnQpfHwvVHJpZGVudC4qcnZcXDoxMVxcLi9pLnRlc3QobmF2aWdhdG9yLnVzZXJBZ2VudCk7dmFyIHNjcm9sbFNlbnNpdGl2aXR5U2V0dGluZz0zMDt2YXIgc2xpZGVEdXJhdGlvblNldHRpbmc9ODAwO3ZhciBjdXJyZW50U2xpZGVOdW1iZXI9MDt2YXIgdG90YWxTbGlkZU51bWJlcj1qUXVlcnkoJy5wYXJhbGxheC1zbGlkZS1iYWNrZ3JvdW5kJykubGVuZ3RoO2Z1bmN0aW9uIHBhcmFsbGF4U2Nyb2xsKGV2dCl7aWYoaXNGaXJlZm94KXtkZWx0YT1ldnQuZGV0YWlsKi0xMjA7fWVsc2UgaWYoaXNJZSl7ZGVsdGE9LWV2dC5kZWx0YVk7fWVsc2V7ZGVsdGE9ZXZ0LndoZWVsRGVsdGE7fVxuaWYodGlja2luZyE9dHJ1ZSl7aWYoZGVsdGE8PS1zY3JvbGxTZW5zaXRpdml0eVNldHRpbmcpe3RpY2tpbmc9dHJ1ZTtpZihjdXJyZW50U2xpZGVOdW1iZXIhPT10b3RhbFNsaWRlTnVtYmVyLTEpe2N1cnJlbnRTbGlkZU51bWJlcisrO25leHRJdGVtKCk7fVxuc2xpZGVEdXJhdGlvblRpbWVvdXQoc2xpZGVEdXJhdGlvblNldHRpbmcpO31cbmlmKGRlbHRhPj1zY3JvbGxTZW5zaXRpdml0eVNldHRpbmcpe3RpY2tpbmc9dHJ1ZTtpZihjdXJyZW50U2xpZGVOdW1iZXIhPT0wKXtjdXJyZW50U2xpZGVOdW1iZXItLTt9XG5wcmV2aW91c0l0ZW0oKTtzbGlkZUR1cmF0aW9uVGltZW91dChzbGlkZUR1cmF0aW9uU2V0dGluZyk7fX19XG5mdW5jdGlvbiBzbGlkZUR1cmF0aW9uVGltZW91dChzbGlkZUR1cmF0aW9uKXtzZXRUaW1lb3V0KGZ1bmN0aW9uKCl7dGlja2luZz1mYWxzZTt9LHNsaWRlRHVyYXRpb24pO31cbnZhciBtb3VzZXdoZWVsRXZlbnQ9aXNGaXJlZm94PydET01Nb3VzZVNjcm9sbCc6J3doZWVsJzt3aW5kb3cuYWRkRXZlbnRMaXN0ZW5lcihtb3VzZXdoZWVsRXZlbnQscGFyYWxsYXhTY3JvbGwsZmFsc2UpO2Z1bmN0aW9uIG5leHRJdGVtKCl7dmFyICRwcmV2aW91c1NsaWRlPWpRdWVyeSgnLnBhcmFsbGF4LXNsaWRlLWJhY2tncm91bmQnKS5lcShjdXJyZW50U2xpZGVOdW1iZXItMSk7JHByZXZpb3VzU2xpZGUuY3NzKCd0cmFuc2Zvcm0nLCd0cmFuc2xhdGUzZCgwLC0xMzB2aCwwKScpLmZpbmQoJy5wYXJhbGxheC1zbGlkZS1jb250ZW50LXdyYXBwZXInKS5jc3MoJ3RyYW5zZm9ybScsJ3RyYW5zbGF0ZVkoNDB2aCknKTtjdXJyZW50U2xpZGVUcmFuc2l0aW9uKCk7fVxuZnVuY3Rpb24gcHJldmlvdXNJdGVtKCl7dmFyICRwcmV2aW91c1NsaWRlPWpRdWVyeSgnLnBhcmFsbGF4LXNsaWRlLWJhY2tncm91bmQnKS5lcShjdXJyZW50U2xpZGVOdW1iZXIrMSk7JHByZXZpb3VzU2xpZGUuY3NzKCd0cmFuc2Zvcm0nLCd0cmFuc2xhdGUzZCgwLDMwdmgsMCknKS5maW5kKCcucGFyYWxsYXgtc2xpZGUtY29udGVudC13cmFwcGVyJykuY3NzKCd0cmFuc2Zvcm0nLCd0cmFuc2xhdGVZKDMwdmgpJyk7Y3VycmVudFNsaWRlVHJhbnNpdGlvbigpO31cbmZ1bmN0aW9uIGN1cnJlbnRTbGlkZVRyYW5zaXRpb24oKXt2YXIgJGN1cnJlbnRTbGlkZT1qUXVlcnkoJy5wYXJhbGxheC1zbGlkZS1iYWNrZ3JvdW5kJykuZXEoY3VycmVudFNsaWRlTnVtYmVyKTskY3VycmVudFNsaWRlLmNzcygndHJhbnNmb3JtJywndHJhbnNsYXRlM2QoMCwtMTV2aCwwKScpLmZpbmQoJy5wYXJhbGxheC1zbGlkZS1jb250ZW50LXdyYXBwZXInKS5jc3MoJ3RyYW5zZm9ybScsJ3RyYW5zbGF0ZVkoMTV2aCknKTs7fVxualF1ZXJ5KCdib2R5Jykub24oJ3RvdWNobW92ZScsZnVuY3Rpb24oZSl7ZS5wcmV2ZW50RGVmYXVsdCgpO2Uuc3RvcFByb3BhZ2F0aW9uKCk7cmV0dXJuIGZhbHNlO30pO3ZhciB0cztqUXVlcnkoZG9jdW1lbnQpLmJpbmQoJ3RvdWNoc3RhcnQnLGZ1bmN0aW9uKGUpe3RzPWUub3JpZ2luYWxFdmVudC50b3VjaGVzWzBdLmNsaWVudFk7fSk7alF1ZXJ5KGRvY3VtZW50KS5iaW5kKCd0b3VjaGVuZCcsZnVuY3Rpb24oZSl7dmFyIHRlPWUub3JpZ2luYWxFdmVudC5jaGFuZ2VkVG91Y2hlc1swXS5jbGllbnRZO2lmKHRzPnRlKzUpe2lmKGN1cnJlbnRTbGlkZU51bWJlciE9PXRvdGFsU2xpZGVOdW1iZXItMSl7Y3VycmVudFNsaWRlTnVtYmVyKys7bmV4dEl0ZW0oKTt9XG5zbGlkZUR1cmF0aW9uVGltZW91dChzbGlkZUR1cmF0aW9uU2V0dGluZyk7fWVsc2UgaWYodHM8dGUtNSl7aWYoY3VycmVudFNsaWRlTnVtYmVyIT09MCl7Y3VycmVudFNsaWRlTnVtYmVyLS07fVxucHJldmlvdXNJdGVtKCk7c2xpZGVEdXJhdGlvblRpbWVvdXQoc2xpZGVEdXJhdGlvblNldHRpbmcpO319KTt9KTt9KTtlbGVtZW50b3JGcm9udGVuZC5ob29rcy5hZGRBY3Rpb24oJ2Zyb250ZW5kL2VsZW1lbnRfcmVhZHkvZ3JhbmRyZXN0YXVyYW50LWdhbGxlcnktaG9yaXpvbnRhbC5kZWZhdWx0JyxmdW5jdGlvbigkc2NvcGUpe2pRdWVyeSgnYm9keScpLmFkZENsYXNzKCdnYWxsZXJ5LWhvcml6b250YWwnKTtqUXVlcnkoXCIuaG9yaXpvbnRhbC1nYWxsZXJ5LXdyYXBwZXJcIikuZWFjaChmdW5jdGlvbigpe3ZhciAkY2Fyb3VzZWw9alF1ZXJ5KHRoaXMpO3ZhciB0aW1lcj0kY2Fyb3VzZWwuYXR0cignZGF0YS1hdXRvcGxheScpO2lmKHRpbWVyPT0wKVxue3RpbWVyPWZhbHNlO31cbnZhciBsb29wPSRjYXJvdXNlbC5hdHRyKCdkYXRhLWxvb3AnKTt2YXIgbmF2aWdhdGlvbj0kY2Fyb3VzZWwuYXR0cignZGF0YS1uYXZpZ2F0aW9uJyk7aWYobmF2aWdhdGlvbj09MClcbntuYXZpZ2F0aW9uPWZhbHNlO31cbnZhciBwYWdpbmF0aW9uPSRjYXJvdXNlbC5hdHRyKCdkYXRhLXBhZ2luYXRpb24nKTtpZihwYWdpbmF0aW9uPT0wKVxue3BhZ2luYXRpb249ZmFsc2U7fVxuJGNhcm91c2VsLmZsaWNraXR5KHtwZXJjZW50UG9zaXRpb246ZmFsc2UsaW1hZ2VzTG9hZGVkOmZhbHNlLHNlbGVjdGVkQXR0cmFjdGlvbjowLjAxLGZyaWN0aW9uOjAuMixsYXp5TG9hZDo1LHBhdXNlQXV0b1BsYXlPbkhvdmVyOnRydWUsYXV0b1BsYXk6dGltZXIsY29udGFpbjp0cnVlLHByZXZOZXh0QnV0dG9uczpuYXZpZ2F0aW9uLHBhZ2VEb3RzOnBhZ2luYXRpb259KTt2YXIgcGFyYWxsYXg9JGNhcm91c2VsLmF0dHIoJ2RhdGEtcGFyYWxsYXgnKTtpZihwYXJhbGxheD09MSlcbnt2YXIgJGltZ3M9JGNhcm91c2VsLmZpbmQoJy5ob3Jpem9udGFsLWdhbGxlcnktY2VsbCBpbWcnKTt2YXIgZG9jU3R5bGU9ZG9jdW1lbnQuZG9jdW1lbnRFbGVtZW50LnN0eWxlO3ZhciB0cmFuc2Zvcm1Qcm9wPXR5cGVvZiBkb2NTdHlsZS50cmFuc2Zvcm09PSdzdHJpbmcnPyd0cmFuc2Zvcm0nOidXZWJraXRUcmFuc2Zvcm0nO3ZhciBmbGt0eT0kY2Fyb3VzZWwuZGF0YSgnZmxpY2tpdHknKTskY2Fyb3VzZWwub24oJ3Njcm9sbC5mbGlja2l0eScsZnVuY3Rpb24oKXtmbGt0eS5zbGlkZXMuZm9yRWFjaChmdW5jdGlvbihzbGlkZSxpKXt2YXIgaW1nPSRpbWdzW2ldO3ZhciB4PShzbGlkZS50YXJnZXQrZmxrdHkueCkqLTEvMztpbWcuc3R5bGVbdHJhbnNmb3JtUHJvcF09J3RyYW5zbGF0ZVgoJyt4KydweCknO30pO30pO31cbnZhciBmdWxsc2NyZWVuPSRjYXJvdXNlbC5hdHRyKCdkYXRhLWZ1bGxzY3JlZW4nKTtpZihmdWxsc2NyZWVuIT0wKVxue2pRdWVyeSgnYm9keScpLmFkZENsYXNzKCdlbGVtZW50b3ItZnVsbHNjcmVlbicpO3ZhciBtZW51SGVpZ2h0PXBhcnNlSW50KGpRdWVyeSgnI3dyYXBwZXInKS5jc3MoJ3BhZGRpbmdUb3AnKSk7dmFyIGRvY3VtZW50SGVpZ2h0PWpRdWVyeSh3aW5kb3cpLmlubmVySGVpZ2h0KCk7dmFyIHNsaWRlckhlaWdodD1wYXJzZUludChkb2N1bWVudEhlaWdodC1tZW51SGVpZ2h0KTskY2Fyb3VzZWwuZmluZCgnLmhvcml6b250YWwtZ2FsbGVyeS1jZWxsJykuY3NzKCdoZWlnaHQnLHNsaWRlckhlaWdodCsncHgnKTskY2Fyb3VzZWwuZmluZCgnLmhvcml6b250YWwtZ2FsbGVyeS1jZWxsLWltZycpLmNzcygnaGVpZ2h0JyxzbGlkZXJIZWlnaHQrJ3B4Jyk7JGNhcm91c2VsLmZsaWNraXR5KCdyZXNpemUnKTtqUXVlcnkod2luZG93KS5yZXNpemUoZnVuY3Rpb24oKXt2YXIgbWVudUhlaWdodD1wYXJzZUludChqUXVlcnkoJyN3cmFwcGVyJykuY3NzKCdwYWRkaW5nVG9wJykpO3ZhciBkb2N1bWVudEhlaWdodD1qUXVlcnkod2luZG93KS5pbm5lckhlaWdodCgpO3ZhciBzbGlkZXJIZWlnaHQ9cGFyc2VJbnQoZG9jdW1lbnRIZWlnaHQtbWVudUhlaWdodCk7JGNhcm91c2VsLmZpbmQoJy5ob3Jpem9udGFsLWdhbGxlcnktY2VsbCcpLmNzcygnaGVpZ2h0JyxzbGlkZXJIZWlnaHQrJ3B4Jyk7JGNhcm91c2VsLmZpbmQoJy5ob3Jpem9udGFsLWdhbGxlcnktY2VsbC1pbWcnKS5jc3MoJ2hlaWdodCcsc2xpZGVySGVpZ2h0KydweCcpOyRjYXJvdXNlbC5mbGlja2l0eSgncmVzaXplJyk7fSk7fX0pO30pO2VsZW1lbnRvckZyb250ZW5kLmhvb2tzLmFkZEFjdGlvbignZnJvbnRlbmQvZWxlbWVudF9yZWFkeS9ncmFuZHJlc3RhdXJhbnQtc2xpZGVyLWhvcml6b250YWwuZGVmYXVsdCcsZnVuY3Rpb24oJHNjb3BlKXtqUXVlcnkoXCIuaG9yaXpvbnRhbC1zbGlkZXItd3JhcHBlclwiKS5lYWNoKGZ1bmN0aW9uKCl7dmFyICRjYXJvdXNlbD1qUXVlcnkodGhpcyk7dmFyIHRpbWVyPSRjYXJvdXNlbC5hdHRyKCdkYXRhLWF1dG9wbGF5Jyk7aWYodGltZXI9PTApXG57dGltZXI9ZmFsc2U7fVxudmFyIGxvb3A9JGNhcm91c2VsLmF0dHIoJ2RhdGEtbG9vcCcpO3ZhciBuYXZpZ2F0aW9uPSRjYXJvdXNlbC5hdHRyKCdkYXRhLW5hdmlnYXRpb24nKTtpZihuYXZpZ2F0aW9uPT0wKVxue25hdmlnYXRpb249ZmFsc2U7fVxudmFyIHBhZ2luYXRpb249JGNhcm91c2VsLmF0dHIoJ2RhdGEtcGFnaW5hdGlvbicpO2lmKHBhZ2luYXRpb249PTApXG57cGFnaW5hdGlvbj1mYWxzZTt9XG4kY2Fyb3VzZWwuZmxpY2tpdHkoe3BlcmNlbnRQb3NpdGlvbjpmYWxzZSxpbWFnZXNMb2FkZWQ6dHJ1ZSxwYXVzZUF1dG9QbGF5T25Ib3Zlcjp0cnVlLGF1dG9QbGF5OnRpbWVyLGNvbnRhaW46dHJ1ZSxwcmV2TmV4dEJ1dHRvbnM6bmF2aWdhdGlvbixwYWdlRG90czpwYWdpbmF0aW9ufSk7dmFyIGZ1bGxzY3JlZW49JGNhcm91c2VsLmF0dHIoJ2RhdGEtZnVsbHNjcmVlbicpO2lmKGZ1bGxzY3JlZW4hPTApXG57alF1ZXJ5KCdib2R5JykuYWRkQ2xhc3MoJ2VsZW1lbnRvci1mdWxsc2NyZWVuJyk7dmFyIG1lbnVIZWlnaHQ9cGFyc2VJbnQoalF1ZXJ5KCcjd3JhcHBlcicpLmNzcygncGFkZGluZ1RvcCcpKTt2YXIgZG9jdW1lbnRIZWlnaHQ9alF1ZXJ5KHdpbmRvdykuaW5uZXJIZWlnaHQoKTt2YXIgc2xpZGVySGVpZ2h0PXBhcnNlSW50KGRvY3VtZW50SGVpZ2h0LW1lbnVIZWlnaHQpOyRjYXJvdXNlbC5maW5kKCcuaG9yaXpvbnRhbC1zbGlkZXItY2VsbCcpLmNzcygnaGVpZ2h0JyxzbGlkZXJIZWlnaHQrJ3B4Jyk7JGNhcm91c2VsLmZsaWNraXR5KCdyZXNpemUnKTtqUXVlcnkod2luZG93KS5yZXNpemUoZnVuY3Rpb24oKXt2YXIgbWVudUhlaWdodD1wYXJzZUludChqUXVlcnkoJyN3cmFwcGVyJykuY3NzKCdwYWRkaW5nVG9wJykpO3ZhciBkb2N1bWVudEhlaWdodD1qUXVlcnkod2luZG93KS5pbm5lckhlaWdodCgpO3ZhciBzbGlkZXJIZWlnaHQ9cGFyc2VJbnQoZG9jdW1lbnRIZWlnaHQtbWVudUhlaWdodCk7JGNhcm91c2VsLmZpbmQoJy5ob3Jpem9udGFsLXNsaWRlci1jZWxsJykuY3NzKCdoZWlnaHQnLHNsaWRlckhlaWdodCsncHgnKTskY2Fyb3VzZWwuZmxpY2tpdHkoJ3Jlc2l6ZScpO30pO319KTt9KTtlbGVtZW50b3JGcm9udGVuZC5ob29rcy5hZGRBY3Rpb24oJ2Zyb250ZW5kL2VsZW1lbnRfcmVhZHkvZ3JhbmRyZXN0YXVyYW50LXNsaWRlci1hbmltYXRlZC1mcmFtZS5kZWZhdWx0JyxmdW5jdGlvbigkc2NvcGUpe2Z1bmN0aW9uIGRlYm91bmNlKGZ1bmMsd2FpdCxpbW1lZGlhdGUpe3ZhciB0aW1lb3V0O3JldHVybiBmdW5jdGlvbigpe3ZhciBjb250ZXh0PXRoaXMsYXJncz1hcmd1bWVudHM7dmFyIGxhdGVyPWZ1bmN0aW9uKCl7dGltZW91dD1udWxsO2lmKCFpbW1lZGlhdGUpZnVuYy5hcHBseShjb250ZXh0LGFyZ3MpO307dmFyIGNhbGxOb3c9aW1tZWRpYXRlJiYhdGltZW91dDtjbGVhclRpbWVvdXQodGltZW91dCk7dGltZW91dD1zZXRUaW1lb3V0KGxhdGVyLHdhaXQpO2lmKGNhbGxOb3cpZnVuYy5hcHBseShjb250ZXh0LGFyZ3MpO307fTtjbGFzcyBTbGlkZXNob3d7Y29uc3RydWN0b3IoZWwpe3RoaXMuRE9NPXt9O3RoaXMuRE9NLmVsPWVsO3RoaXMuc2V0dGluZ3M9e2FuaW1hdGlvbjp7c2xpZGVzOntkdXJhdGlvbjo2MDAsZWFzaW5nOidlYXNlT3V0UXVpbnQnfSxzaGFwZTp7ZHVyYXRpb246MzAwLGVhc2luZzp7aW46J2Vhc2VPdXRRdWludCcsb3V0OidlYXNlT3V0UXVhZCd9fX0sZnJhbWVGaWxsOnNsaWRlc2hvd0ZyYW1lQ29sb3J9XG50aGlzLmluaXQoKTt9XG5pbml0KCl7aWYodGhpcy5ET00uZWwpXG57dGhpcy5ET00uc2xpZGVzPUFycmF5LmZyb20odGhpcy5ET00uZWwucXVlcnlTZWxlY3RvckFsbCgnLnNsaWRlcyA+IC5zbGlkZScpKTt0aGlzLnNsaWRlc1RvdGFsPXRoaXMuRE9NLnNsaWRlcy5sZW5ndGg7dGhpcy5ET00ubmF2PXRoaXMuRE9NLmVsLnF1ZXJ5U2VsZWN0b3IoJy5zbGlkZW5hdicpO3RoaXMuRE9NLm5leHRDdHJsPXRoaXMuRE9NLm5hdi5xdWVyeVNlbGVjdG9yKCcuc2xpZGVuYXYtaXRlbS0tbmV4dCcpO3RoaXMuRE9NLnByZXZDdHJsPXRoaXMuRE9NLm5hdi5xdWVyeVNlbGVjdG9yKCcuc2xpZGVuYXYtaXRlbS0tcHJldicpO3RoaXMuY3VycmVudD0wO3RoaXMuY3JlYXRlRnJhbWUoKTt0aGlzLmluaXRFdmVudHMoKTt9fVxuY3JlYXRlRnJhbWUoKXt0aGlzLnJlY3Q9dGhpcy5ET00uZWwuZ2V0Qm91bmRpbmdDbGllbnRSZWN0KCk7dGhpcy5mcmFtZVNpemU9dGhpcy5yZWN0LndpZHRoLzEyO3RoaXMucGF0aHM9e2luaXRpYWw6dGhpcy5jYWxjdWxhdGVQYXRoKCdpbml0aWFsJyksZmluYWw6dGhpcy5jYWxjdWxhdGVQYXRoKCdmaW5hbCcpfTt0aGlzLkRPTS5zdmc9ZG9jdW1lbnQuY3JlYXRlRWxlbWVudE5TKCdodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZycsJ3N2ZycpO3RoaXMuRE9NLnN2Zy5zZXRBdHRyaWJ1dGUoJ2NsYXNzJywnc2hhcGUnKTt0aGlzLkRPTS5zdmcuc2V0QXR0cmlidXRlKCd3aWR0aCcsJzEwMCUnKTt0aGlzLkRPTS5zdmcuc2V0QXR0cmlidXRlKCdoZWlnaHQnLCcxMDAlJyk7dGhpcy5ET00uc3ZnLnNldEF0dHJpYnV0ZSgndmlld2JveCcsYDAgMCAke3RoaXMucmVjdC53aWR0aH0ke3RoaXMucmVjdC5oZWlnaHR9YCk7dGhpcy5ET00uc3ZnLmlubmVySFRNTD1gPHBhdGggZmlsbD1cIiR7dGhpcy5zZXR0aW5ncy5mcmFtZUZpbGx9XCJkPVwiJHt0aGlzLnBhdGhzLmluaXRpYWx9XCIvPmA7dGhpcy5ET00uZWwuaW5zZXJ0QmVmb3JlKHRoaXMuRE9NLnN2Zyx0aGlzLkRPTS5uYXYpO3RoaXMuRE9NLnNoYXBlPXRoaXMuRE9NLnN2Zy5xdWVyeVNlbGVjdG9yKCdwYXRoJyk7fVxudXBkYXRlRnJhbWUoKXt0aGlzLnBhdGhzLmluaXRpYWw9dGhpcy5jYWxjdWxhdGVQYXRoKCdpbml0aWFsJyk7dGhpcy5wYXRocy5maW5hbD10aGlzLmNhbGN1bGF0ZVBhdGgoJ2ZpbmFsJyk7dGhpcy5ET00uc3ZnLnNldEF0dHJpYnV0ZSgndmlld2JveCcsYDAgMCAke3RoaXMucmVjdC53aWR0aH0ke3RoaXMucmVjdC5oZWlnaHR9YCk7dGhpcy5ET00uc2hhcGUuc2V0QXR0cmlidXRlKCdkJyx0aGlzLmlzQW5pbWF0aW5nP3RoaXMucGF0aHMuZmluYWw6dGhpcy5wYXRocy5pbml0aWFsKTt9XG5jYWxjdWxhdGVQYXRoKHBhdGg9J2luaXRpYWwnKXtyZXR1cm4gcGF0aD09PSdpbml0aWFsJz9gTSAwLDAgMCwke3RoaXMucmVjdC5oZWlnaHR9JHt0aGlzLnJlY3Qud2lkdGh9LCR7dGhpcy5yZWN0LmhlaWdodH0ke3RoaXMucmVjdC53aWR0aH0sMCAwLDAgWiBNIDAsMCAke3RoaXMucmVjdC53aWR0aH0sMCAke3RoaXMucmVjdC53aWR0aH0sJHt0aGlzLnJlY3QuaGVpZ2h0fTAsJHt0aGlzLnJlY3QuaGVpZ2h0fVpgOmBNIDAsMCAwLCR7dGhpcy5yZWN0LmhlaWdodH0ke3RoaXMucmVjdC53aWR0aH0sJHt0aGlzLnJlY3QuaGVpZ2h0fSR7dGhpcy5yZWN0LndpZHRofSwwIDAsMCBaIE0gJHt0aGlzLmZyYW1lU2l6ZX0sJHt0aGlzLmZyYW1lU2l6ZX0ke3RoaXMucmVjdC53aWR0aC10aGlzLmZyYW1lU2l6ZX0sJHt0aGlzLmZyYW1lU2l6ZX0ke3RoaXMucmVjdC53aWR0aC10aGlzLmZyYW1lU2l6ZX0sJHt0aGlzLnJlY3QuaGVpZ2h0LXRoaXMuZnJhbWVTaXplfSR7dGhpcy5mcmFtZVNpemV9LCR7dGhpcy5yZWN0LmhlaWdodC10aGlzLmZyYW1lU2l6ZX1aYDt9XG5pbml0RXZlbnRzKCl7dGhpcy5ET00ubmV4dEN0cmwuYWRkRXZlbnRMaXN0ZW5lcignY2xpY2snLCgpPT50aGlzLm5hdmlnYXRlKCduZXh0JykpO3RoaXMuRE9NLnByZXZDdHJsLmFkZEV2ZW50TGlzdGVuZXIoJ2NsaWNrJywoKT0+dGhpcy5uYXZpZ2F0ZSgncHJldicpKTt3aW5kb3cuYWRkRXZlbnRMaXN0ZW5lcigncmVzaXplJyxkZWJvdW5jZSgoKT0+e3RoaXMucmVjdD10aGlzLkRPTS5lbC5nZXRCb3VuZGluZ0NsaWVudFJlY3QoKTt0aGlzLnVwZGF0ZUZyYW1lKCk7fSwyMCkpO2RvY3VtZW50LmFkZEV2ZW50TGlzdGVuZXIoJ2tleWRvd24nLChldik9Pntjb25zdCBrZXlDb2RlPWV2LmtleUNvZGV8fGV2LndoaWNoO2lmKGtleUNvZGU9PT0zNyl7dGhpcy5uYXZpZ2F0ZSgncHJldicpO31cbmVsc2UgaWYoa2V5Q29kZT09PTM5KXt0aGlzLm5hdmlnYXRlKCduZXh0Jyk7fX0pO31cbm5hdmlnYXRlKGRpcj0nbmV4dCcpe2lmKHRoaXMuaXNBbmltYXRpbmcpcmV0dXJuIGZhbHNlO3RoaXMuaXNBbmltYXRpbmc9dHJ1ZTtjb25zdCBhbmltYXRlU2hhcGVJbj1hbmltZSh7dGFyZ2V0czp0aGlzLkRPTS5zaGFwZSxkdXJhdGlvbjp0aGlzLnNldHRpbmdzLmFuaW1hdGlvbi5zaGFwZS5kdXJhdGlvbixlYXNpbmc6dGhpcy5zZXR0aW5ncy5hbmltYXRpb24uc2hhcGUuZWFzaW5nLmluLGQ6dGhpcy5wYXRocy5maW5hbH0pO2NvbnN0IGFuaW1hdGVTbGlkZXM9KCk9PntyZXR1cm4gbmV3IFByb21pc2UoKHJlc29sdmUscmVqZWN0KT0+e2NvbnN0IGN1cnJlbnRTbGlkZT10aGlzLkRPTS5zbGlkZXNbdGhpcy5jdXJyZW50XTthbmltZSh7dGFyZ2V0czpjdXJyZW50U2xpZGUsZHVyYXRpb246dGhpcy5zZXR0aW5ncy5hbmltYXRpb24uc2xpZGVzLmR1cmF0aW9uLGVhc2luZzp0aGlzLnNldHRpbmdzLmFuaW1hdGlvbi5zbGlkZXMuZWFzaW5nLHRyYW5zbGF0ZVg6ZGlyPT09J25leHQnPy0xKnRoaXMucmVjdC53aWR0aDp0aGlzLnJlY3Qud2lkdGgsY29tcGxldGU6KCk9PntjdXJyZW50U2xpZGUuY2xhc3NMaXN0LnJlbW92ZSgnc2xpZGUtY3VycmVudCcpO3Jlc29sdmUoKTt9fSk7dGhpcy5jdXJyZW50PWRpcj09PSduZXh0Jz90aGlzLmN1cnJlbnQ8dGhpcy5zbGlkZXNUb3RhbC0xP3RoaXMuY3VycmVudCsxOjA6dGhpcy5jdXJyZW50PjA/dGhpcy5jdXJyZW50LTE6dGhpcy5zbGlkZXNUb3RhbC0xO2NvbnN0IG5ld1NsaWRlPXRoaXMuRE9NLnNsaWRlc1t0aGlzLmN1cnJlbnRdO25ld1NsaWRlLmNsYXNzTGlzdC5hZGQoJ3NsaWRlLWN1cnJlbnQnKTthbmltZSh7dGFyZ2V0czpuZXdTbGlkZSxkdXJhdGlvbjp0aGlzLnNldHRpbmdzLmFuaW1hdGlvbi5zbGlkZXMuZHVyYXRpb24sZWFzaW5nOnRoaXMuc2V0dGluZ3MuYW5pbWF0aW9uLnNsaWRlcy5lYXNpbmcsdHJhbnNsYXRlWDpbZGlyPT09J25leHQnP3RoaXMucmVjdC53aWR0aDotMSp0aGlzLnJlY3Qud2lkdGgsMF19KTtjb25zdCBuZXdTbGlkZUltZz1uZXdTbGlkZS5xdWVyeVNlbGVjdG9yKCcuc2xpZGUtaW1nJyk7YW5pbWUucmVtb3ZlKG5ld1NsaWRlSW1nKTthbmltZSh7dGFyZ2V0czpuZXdTbGlkZUltZyxkdXJhdGlvbjp0aGlzLnNldHRpbmdzLmFuaW1hdGlvbi5zbGlkZXMuZHVyYXRpb24qNCxlYXNpbmc6dGhpcy5zZXR0aW5ncy5hbmltYXRpb24uc2xpZGVzLmVhc2luZyx0cmFuc2xhdGVYOltkaXI9PT0nbmV4dCc/MjAwOi0yMDAsMF19KTthbmltZSh7dGFyZ2V0czpbbmV3U2xpZGUucXVlcnlTZWxlY3RvcignLnNsaWRlLXRpdGxlJyksbmV3U2xpZGUucXVlcnlTZWxlY3RvcignLnNsaWRlLWRlc2MnKSxuZXdTbGlkZS5xdWVyeVNlbGVjdG9yKCcuc2xpZGUtbGluaycpXSxkdXJhdGlvbjp0aGlzLnNldHRpbmdzLmFuaW1hdGlvbi5zbGlkZXMuZHVyYXRpb24qMixlYXNpbmc6dGhpcy5zZXR0aW5ncy5hbmltYXRpb24uc2xpZGVzLmVhc2luZyxkZWxheToodCxpKT0+aSoxMDArMTAwLHRyYW5zbGF0ZVg6W2Rpcj09PSduZXh0Jz8zMDA6LTMwMCwwXSxvcGFjaXR5OlswLDFdfSk7fSk7fTtjb25zdCBhbmltYXRlU2hhcGVPdXQ9KCk9PnthbmltZSh7dGFyZ2V0czp0aGlzLkRPTS5zaGFwZSxkdXJhdGlvbjp0aGlzLnNldHRpbmdzLmFuaW1hdGlvbi5zaGFwZS5kdXJhdGlvbixkZWxheToxNTAsZWFzaW5nOnRoaXMuc2V0dGluZ3MuYW5pbWF0aW9uLnNoYXBlLmVhc2luZy5vdXQsZDp0aGlzLnBhdGhzLmluaXRpYWwsY29tcGxldGU6KCk9PnRoaXMuaXNBbmltYXRpbmc9ZmFsc2V9KTt9XG5hbmltYXRlU2hhcGVJbi5maW5pc2hlZC50aGVuKGFuaW1hdGVTbGlkZXMpLnRoZW4oYW5pbWF0ZVNoYXBlT3V0KTt9fTt2YXIgc2xpZGVzaG93PWRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoJy5zbGlkZXNob3cnKTtpZihzbGlkZXNob3cpXG57dmFyIHNsaWRlc2hvd0ZyYW1lQ29sb3I9c2xpZGVzaG93LmdldEF0dHJpYnV0ZSgnZGF0YS1iYWNrZ3JvdW5kJyk7bmV3IFNsaWRlc2hvdyhzbGlkZXNob3cpO2ltYWdlc0xvYWRlZCgnLnNsaWRlLWltZycse2JhY2tncm91bmQ6dHJ1ZX0pO319KTtlbGVtZW50b3JGcm9udGVuZC5ob29rcy5hZGRBY3Rpb24oJ2Zyb250ZW5kL2VsZW1lbnRfcmVhZHkvZ3JhbmRyZXN0YXVyYW50LXNsaWRlci1yb29tLmRlZmF1bHQnLGZ1bmN0aW9uKCRzY29wZSl7alF1ZXJ5KCdib2R5JykuYWRkQ2xhc3MoJ2VsZW1lbnRvci1mdWxsc2NyZWVuJyk7alF1ZXJ5KCdib2R5JykuYWRkQ2xhc3MoJ3Jvb20nKTtmdW5jdGlvbiBkZWJvdW5jZShmdW5jLHdhaXQsaW1tZWRpYXRlKXt2YXIgdGltZW91dDtyZXR1cm4gZnVuY3Rpb24oKXt2YXIgY29udGV4dD10aGlzLGFyZ3M9YXJndW1lbnRzO3ZhciBsYXRlcj1mdW5jdGlvbigpe3RpbWVvdXQ9bnVsbDtpZighaW1tZWRpYXRlKWZ1bmMuYXBwbHkoY29udGV4dCxhcmdzKTt9O3ZhciBjYWxsTm93PWltbWVkaWF0ZSYmIXRpbWVvdXQ7Y2xlYXJUaW1lb3V0KHRpbWVvdXQpO3RpbWVvdXQ9c2V0VGltZW91dChsYXRlcix3YWl0KTtpZihjYWxsTm93KWZ1bmMuYXBwbHkoY29udGV4dCxhcmdzKTt9O307ZnVuY3Rpb24gZ2V0TW91c2VQb3MoZSl7dmFyIHBvc3g9MDt2YXIgcG9zeT0wO2lmKCFlKXZhciBlPXdpbmRvdy5ldmVudDtpZihlLnBhZ2VYfHxlLnBhZ2VZKXtwb3N4PWUucGFnZVg7cG9zeT1lLnBhZ2VZO31cbmVsc2UgaWYoZS5jbGllbnRYfHxlLmNsaWVudFkpe3Bvc3g9ZS5jbGllbnRYK2RvY3VtZW50LmJvZHkuc2Nyb2xsTGVmdFxuK2RvY3VtZW50LmRvY3VtZW50RWxlbWVudC5zY3JvbGxMZWZ0O3Bvc3k9ZS5jbGllbnRZK2RvY3VtZW50LmJvZHkuc2Nyb2xsVG9wXG4rZG9jdW1lbnQuZG9jdW1lbnRFbGVtZW50LnNjcm9sbFRvcDt9XG5yZXR1cm57eDpwb3N4LHk6cG9zeX19XG52YXIgRE9NPXt9O0RPTS5sb2FkZXI9ZG9jdW1lbnQucXVlcnlTZWxlY3RvcignLnJvb20tc2xpZGVyLXdyYXBwZXIgLm92ZXJsYXktLWxvYWRlcicpO0RPTS5zY3JvbGxlcj1kb2N1bWVudC5xdWVyeVNlbGVjdG9yKCcucm9vbS1zbGlkZXItd3JhcHBlciAuY29udGFpbmVyID4gLnNjcm9sbGVyJyk7aWYoRE9NLnNjcm9sbGVyKVxue0RPTS5yb29tcz1bXS5zbGljZS5jYWxsKERPTS5zY3JvbGxlci5xdWVyeVNlbGVjdG9yQWxsKCcucm9vbScpKTt9XG5lbHNlXG57RE9NLnJvb21zPXt9O31cbkRPTS5jb250ZW50PWRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoJy5yb29tLXNsaWRlci13cmFwcGVyIC5jb250ZW50Jyk7aWYoRE9NLmNvbnRlbnQpXG57RE9NLm5hdj17bGVmdEN0cmw6RE9NLmNvbnRlbnQucXVlcnlTZWxlY3RvcignLnJvb20tc2xpZGVyLXdyYXBwZXIgbmF2ID4gLmJ0bi0tbmF2LWxlZnQnKSxyaWdodEN0cmw6RE9NLmNvbnRlbnQucXVlcnlTZWxlY3RvcignLnJvb20tc2xpZGVyLXdyYXBwZXIgbmF2ID4gLmJ0bi0tbmF2LXJpZ2h0Jyl9O0RPTS5zbGlkZXM9W10uc2xpY2UuY2FsbChET00uY29udGVudC5xdWVyeVNlbGVjdG9yQWxsKCcuc2xpZGVzID4gLnNsaWRlJykpO31cbnZhciBjdXJyZW50Um9vbT0wLHRvdGFsUm9vbXM9RE9NLnJvb21zLmxlbmd0aCxpbml0VHJhbnNmb3JtPXt0cmFuc2xhdGVYOjAsdHJhbnNsYXRlWTowLHRyYW5zbGF0ZVo6JzUwMHB4Jyxyb3RhdGVYOjAscm90YXRlWTowLHJvdGF0ZVo6MH0scmVzZXRUcmFuc2Zvcm09e3RyYW5zbGF0ZVg6MCx0cmFuc2xhdGVZOjAsdHJhbnNsYXRlWjowLHJvdGF0ZVg6MCxyb3RhdGVZOjAscm90YXRlWjowfSxtZW51VHJhbnNmb3JtPXt0cmFuc2xhdGVYOjAsdHJhbnNsYXRlWTonMTUwJScsdHJhbnNsYXRlWjowLHJvdGF0ZVg6JzE1ZGVnJyxyb3RhdGVZOjAscm90YXRlWjowfSxtZW51VHJhbnNmb3JtPXt0cmFuc2xhdGVYOjAsdHJhbnNsYXRlWTonNTAlJyx0cmFuc2xhdGVaOjAscm90YXRlWDonLTEwZGVnJyxyb3RhdGVZOjAscm90YXRlWjowfSxpbmZvVHJhbnNmb3JtPXt0cmFuc2xhdGVYOjAsdHJhbnNsYXRlWTowLHRyYW5zbGF0ZVo6JzIwMHB4Jyxyb3RhdGVYOicyZGVnJyxyb3RhdGVZOjAscm90YXRlWjonNGRlZyd9LGluaXRUcmFuc2l0aW9uPXtzcGVlZDonMC45cycsZWFzaW5nOidlYXNlJ30scm9vbVRyYW5zaXRpb249e3NwZWVkOicwLjRzJyxlYXNpbmc6J2Vhc2UnfSxtZW51VHJhbnNpdGlvbj17c3BlZWQ6JzEuNXMnLGVhc2luZzonY3ViaWMtYmV6aWVyKDAuMiwxLDAuMywxKSd9LGluZm9UcmFuc2l0aW9uPXtzcGVlZDonMTVzJyxlYXNpbmc6J2N1YmljLWJlemllcigwLjMsMSwwLjMsMSknfSx0aWx0VHJhbnNpdGlvbj17c3BlZWQ6JzAuMnMnLGVhc2luZzonZWFzZS1vdXQnfSx0aWx0PWZhbHNlLHRpbHRSb3RhdGlvbj17cm90YXRlWDoxLHJvdGF0ZVk6LTN9LG9uRW5kVHJhbnNpdGlvbj1mdW5jdGlvbihlbCxjYWxsYmFjayl7dmFyIG9uRW5kQ2FsbGJhY2tGbj1mdW5jdGlvbihldil7dGhpcy5yZW1vdmVFdmVudExpc3RlbmVyKCd0cmFuc2l0aW9uZW5kJyxvbkVuZENhbGxiYWNrRm4pO2lmKGNhbGxiYWNrJiZ0eXBlb2YgY2FsbGJhY2s9PT0nZnVuY3Rpb24nKXtjYWxsYmFjay5jYWxsKCk7fX07ZWwuYWRkRXZlbnRMaXN0ZW5lcigndHJhbnNpdGlvbmVuZCcsb25FbmRDYWxsYmFja0ZuKTt9LHdpbj17d2lkdGg6d2luZG93LmlubmVyV2lkdGgsaGVpZ2h0OndpbmRvdy5pbm5lckhlaWdodH0saXNNb3ZpbmcsaXNOYXZpZ2F0aW5nO2Z1bmN0aW9uIGluaXQoKXttb3ZlKHt0cmFuc2l0aW9uOmluaXRUcmFuc2l0aW9uLHRyYW5zZm9ybTppbml0VHJhbnNmb3JtfSkudGhlbihmdW5jdGlvbigpe2luaXRUaWx0KCk7fSk7c2hvd1NsaWRlKDEwMCk7aW5pdEV2ZW50cygpO31cbmZ1bmN0aW9uIGluaXRUaWx0KCl7YXBwbHlSb29tVHJhbnNpdGlvbih0aWx0VHJhbnNpdGlvbik7dGlsdD10cnVlO31cbmZ1bmN0aW9uIHJlbW92ZVRpbHQoKXt0aWx0PWZhbHNlO31cbmZ1bmN0aW9uIG1vdmUob3B0cyl7cmV0dXJuIG5ldyBQcm9taXNlKGZ1bmN0aW9uKHJlc29sdmUscmVqZWN0KXtpZihpc01vdmluZyYmIW9wdHMuc3RvcFRyYW5zaXRpb24pe3JldHVybiBmYWxzZTt9XG5pc01vdmluZz10cnVlO2lmKG9wdHMudHJhbnNpdGlvbil7YXBwbHlSb29tVHJhbnNpdGlvbihvcHRzLnRyYW5zaXRpb24pO31cbmlmKG9wdHMudHJhbnNmb3JtKXthcHBseVJvb21UcmFuc2Zvcm0ob3B0cy50cmFuc2Zvcm0pO3ZhciBvbkVuZEZuPWZ1bmN0aW9uKCl7aXNNb3Zpbmc9ZmFsc2U7cmVzb2x2ZSgpO307b25FbmRUcmFuc2l0aW9uKERPTS5zY3JvbGxlcixvbkVuZEZuKTt9XG5lbHNle3Jlc29sdmUoKTt9fSk7fVxuZnVuY3Rpb24gaW5pdEV2ZW50cygpe3ZhciBvbk1vdXNlTW92ZUZuPWZ1bmN0aW9uKGV2KXtyZXF1ZXN0QW5pbWF0aW9uRnJhbWUoZnVuY3Rpb24oKXtpZighdGlsdClyZXR1cm4gZmFsc2U7dmFyIG1vdXNlcG9zPWdldE1vdXNlUG9zKGV2KSxyb3RYPXRpbHRSb3RhdGlvbi5yb3RhdGVYP2luaXRUcmFuc2Zvcm0ucm90YXRlWC0oMip0aWx0Um90YXRpb24ucm90YXRlWC93aW4uaGVpZ2h0Km1vdXNlcG9zLnktdGlsdFJvdGF0aW9uLnJvdGF0ZVgpOjAscm90WT10aWx0Um90YXRpb24ucm90YXRlWT9pbml0VHJhbnNmb3JtLnJvdGF0ZVktKDIqdGlsdFJvdGF0aW9uLnJvdGF0ZVkvd2luLndpZHRoKm1vdXNlcG9zLngtdGlsdFJvdGF0aW9uLnJvdGF0ZVkpOjA7YXBwbHlSb29tVHJhbnNmb3JtKHsndHJhbnNsYXRlWCc6aW5pdFRyYW5zZm9ybS50cmFuc2xhdGVYLCd0cmFuc2xhdGVZJzppbml0VHJhbnNmb3JtLnRyYW5zbGF0ZVksJ3RyYW5zbGF0ZVonOmluaXRUcmFuc2Zvcm0udHJhbnNsYXRlWiwncm90YXRlWCc6cm90WCsnZGVnJywncm90YXRlWSc6cm90WSsnZGVnJywncm90YXRlWic6aW5pdFRyYW5zZm9ybS5yb3RhdGVafSk7fSk7fSxkZWJvdW5jZVJlc2l6ZUZuPWRlYm91bmNlKGZ1bmN0aW9uKCl7d2luPXt3aWR0aDp3aW5kb3cuaW5uZXJXaWR0aCxoZWlnaHQ6d2luZG93LmlubmVySGVpZ2h0fTt9LDEwKTtkb2N1bWVudC5hZGRFdmVudExpc3RlbmVyKCdtb3VzZW1vdmUnLG9uTW91c2VNb3ZlRm4pO3dpbmRvdy5hZGRFdmVudExpc3RlbmVyKCdyZXNpemUnLGRlYm91bmNlUmVzaXplRm4pO3ZhciBvbk5hdmlnYXRlUHJldkZuPWZ1bmN0aW9uKCl7bmF2aWdhdGUoJ3ByZXYnKTt9LG9uTmF2aWdhdGVOZXh0Rm49ZnVuY3Rpb24oKXtuYXZpZ2F0ZSgnbmV4dCcpO307aWYoRE9NLm5hdi5sZWZ0Q3RybCYmRE9NLm5hdi5yaWdodEN0cmwpXG57RE9NLm5hdi5sZWZ0Q3RybC5hZGRFdmVudExpc3RlbmVyKCdjbGljaycsb25OYXZpZ2F0ZVByZXZGbik7RE9NLm5hdi5yaWdodEN0cmwuYWRkRXZlbnRMaXN0ZW5lcignY2xpY2snLG9uTmF2aWdhdGVOZXh0Rm4pO319XG5mdW5jdGlvbiBhcHBseVJvb21UcmFuc2Zvcm0odHJhbnNmb3JtKXtET00uc2Nyb2xsZXIuc3R5bGUudHJhbnNmb3JtPSd0cmFuc2xhdGUzZCgnK3RyYW5zZm9ybS50cmFuc2xhdGVYKycsICcrdHJhbnNmb3JtLnRyYW5zbGF0ZVkrJywgJyt0cmFuc2Zvcm0udHJhbnNsYXRlWisnKSAnKydyb3RhdGUzZCgxLDAsMCwnK3RyYW5zZm9ybS5yb3RhdGVYKycpIHJvdGF0ZTNkKDAsMSwwLCcrdHJhbnNmb3JtLnJvdGF0ZVkrJykgcm90YXRlM2QoMCwwLDEsJyt0cmFuc2Zvcm0ucm90YXRlWisnKSc7fVxuZnVuY3Rpb24gYXBwbHlSb29tVHJhbnNpdGlvbih0cmFuc2l0aW9uKXtET00uc2Nyb2xsZXIuc3R5bGUudHJhbnNpdGlvbj10cmFuc2l0aW9uPT09J25vbmUnP3RyYW5zaXRpb246J3RyYW5zZm9ybSAnK3RyYW5zaXRpb24uc3BlZWQrJyAnK3RyYW5zaXRpb24uZWFzaW5nO31cbmZ1bmN0aW9uIHRvZ2dsZVNsaWRlKGRpcixkZWxheSl7dmFyIHNsaWRlPURPTS5zbGlkZXNbY3VycmVudFJvb21dLG5hbWU9c2xpZGUucXVlcnlTZWxlY3RvcignLnNsaWRlLW5hbWUnKSx0aXRsZT1zbGlkZS5xdWVyeVNlbGVjdG9yKCcuc2xpZGUtdGl0bGUnKSxkYXRlPXNsaWRlLnF1ZXJ5U2VsZWN0b3IoJy5zbGlkZS1kYXRlJyk7ZGVsYXk9ZGVsYXkhPT11bmRlZmluZWQ/ZGVsYXk6MDthbmltZS5yZW1vdmUoW25hbWUsdGl0bGUsZGF0ZV0pO3ZhciBhbmltZU9wdHM9e3RhcmdldHM6W25hbWUsdGl0bGUsZGF0ZV0sZHVyYXRpb246ZGlyPT09J2luJz80MDA6NDAwLGRlbGF5OmZ1bmN0aW9uKHQsaSxjKXtyZXR1cm4gZGVsYXkrNzUraSo3NTt9LGVhc2luZzpbMC4yNSwwLjEsMC4yNSwxXSxvcGFjaXR5Ont2YWx1ZTpkaXI9PT0naW4nP1swLDFdOlsxLDBdLGR1cmF0aW9uOmRpcj09PSdpbic/NTUwOjI1MH0sdHJhbnNsYXRlWTpmdW5jdGlvbih0LGkpe3JldHVybiBkaXI9PT0naW4nP1sxNTAsMF06WzAsLTE1MF07fX07aWYoZGlyPT09J2luJyl7YW5pbWVPcHRzLmJlZ2luPWZ1bmN0aW9uKCl7c2xpZGUuY2xhc3NMaXN0LmFkZCgnc2xpZGUtY3VycmVudCcpO307fVxuZWxzZXthbmltZU9wdHMuY29tcGxldGU9ZnVuY3Rpb24oKXtzbGlkZS5jbGFzc0xpc3QucmVtb3ZlKCdzbGlkZS1jdXJyZW50Jyk7fTt9XG5hbmltZShhbmltZU9wdHMpO31cbmZ1bmN0aW9uIHNob3dTbGlkZShkZWxheSl7dG9nZ2xlU2xpZGUoJ2luJyxkZWxheSk7fVxuZnVuY3Rpb24gaGlkZVNsaWRlKGRlbGF5KXt0b2dnbGVTbGlkZSgnb3V0JyxkZWxheSk7fVxuZnVuY3Rpb24gbmF2aWdhdGUoZGlyKXtpZihpc01vdmluZ3x8aXNOYXZpZ2F0aW5nKXtyZXR1cm4gZmFsc2U7fVxuaXNOYXZpZ2F0aW5nPXRydWU7dmFyIHJvb209RE9NLnJvb21zW2N1cnJlbnRSb29tXTtyZW1vdmVUaWx0KCk7aGlkZVNsaWRlKCk7aWYoZGlyPT09J25leHQnKXtjdXJyZW50Um9vbT1jdXJyZW50Um9vbTx0b3RhbFJvb21zLTE/Y3VycmVudFJvb20rMTowO31cbmVsc2V7Y3VycmVudFJvb209Y3VycmVudFJvb20+MD9jdXJyZW50Um9vbS0xOnRvdGFsUm9vbXMtMTt9XG52YXIgbmV4dFJvb209RE9NLnJvb21zW2N1cnJlbnRSb29tXTtuZXh0Um9vbS5zdHlsZS50cmFuc2Zvcm09J3RyYW5zbGF0ZTNkKCcrKGRpcj09PSduZXh0Jz8xMDA6LTEwMCkrJyUsMCwwKSB0cmFuc2xhdGUzZCgnKyhkaXI9PT0nbmV4dCc/MTotMSkrJ3B4LDAsMCknO25leHRSb29tLnN0eWxlLm9wYWNpdHk9MTttb3ZlKHt0cmFuc2l0aW9uOnJvb21UcmFuc2l0aW9uLHRyYW5zZm9ybTpyZXNldFRyYW5zZm9ybX0pLnRoZW4oZnVuY3Rpb24oKXtyZXR1cm4gbW92ZSh7dHJhbnNmb3JtOnt0cmFuc2xhdGVYOihkaXI9PT0nbmV4dCc/LTEwMDoxMDApKyclJyx0cmFuc2xhdGVZOjAsdHJhbnNsYXRlWjowLHJvdGF0ZVg6MCxyb3RhdGVZOjAscm90YXRlWjowfX0pO30pLnRoZW4oZnVuY3Rpb24oKXtuZXh0Um9vbS5jbGFzc0xpc3QuYWRkKCdyb29tLS1jdXJyZW50Jyk7cm9vbS5jbGFzc0xpc3QucmVtb3ZlKCdyb29tLS1jdXJyZW50Jyk7cm9vbS5zdHlsZS5vcGFjaXR5PTA7c2hvd1NsaWRlKCk7cmV0dXJuIG1vdmUoe3RyYW5zZm9ybTp7dHJhbnNsYXRlWDooZGlyPT09J25leHQnPy0xMDA6MTAwKSsnJScsdHJhbnNsYXRlWTowLHRyYW5zbGF0ZVo6JzUwMHB4Jyxyb3RhdGVYOjAscm90YXRlWTowLHJvdGF0ZVo6MH19KTt9KS50aGVuKGZ1bmN0aW9uKCl7YXBwbHlSb29tVHJhbnNpdGlvbignbm9uZScpO25leHRSb29tLnN0eWxlLnRyYW5zZm9ybT0ndHJhbnNsYXRlM2QoMCwwLDApJzthcHBseVJvb21UcmFuc2Zvcm0oaW5pdFRyYW5zZm9ybSk7c2V0VGltZW91dChmdW5jdGlvbigpe2luaXRUaWx0KCk7fSw2MCk7aXNOYXZpZ2F0aW5nPWZhbHNlO30pO31cbmZ1bmN0aW9uIGFkZEFkamFjZW50Um9vbXMoKXt2YXIgcm9vbT1ET00ucm9vbXNbY3VycmVudFJvb21dLG5leHRSb29tPURPTS5yb29tc1tjdXJyZW50Um9vbTx0b3RhbFJvb21zLTE/Y3VycmVudFJvb20rMTowXSxwcmV2Um9vbT1ET00ucm9vbXNbY3VycmVudFJvb20+MD9jdXJyZW50Um9vbS0xOnRvdGFsUm9vbXMtMV07bmV4dFJvb20uc3R5bGUudHJhbnNmb3JtPSd0cmFuc2xhdGUzZCgxMDAlLDAsMCkgdHJhbnNsYXRlM2QoM3B4LDAsMCknO25leHRSb29tLnN0eWxlLm9wYWNpdHk9MTtwcmV2Um9vbS5zdHlsZS50cmFuc2Zvcm09J3RyYW5zbGF0ZTNkKC0xMDAlLDAsMCkgdHJhbnNsYXRlM2QoLTNweCwwLDApJztwcmV2Um9vbS5zdHlsZS5vcGFjaXR5PTE7fVxuZnVuY3Rpb24gcmVtb3ZlQWRqYWNlbnRSb29tcygpe3ZhciByb29tPURPTS5yb29tc1tjdXJyZW50Um9vbV0sbmV4dFJvb209RE9NLnJvb21zW2N1cnJlbnRSb29tPHRvdGFsUm9vbXMtMT9jdXJyZW50Um9vbSsxOjBdLHByZXZSb29tPURPTS5yb29tc1tjdXJyZW50Um9vbT4wP2N1cnJlbnRSb29tLTE6dG90YWxSb29tcy0xXTtuZXh0Um9vbS5zdHlsZS50cmFuc2Zvcm09J25vbmUnO25leHRSb29tLnN0eWxlLm9wYWNpdHk9MDtwcmV2Um9vbS5zdHlsZS50cmFuc2Zvcm09J25vbmUnO3ByZXZSb29tLnN0eWxlLm9wYWNpdHk9MDt9XG5pZihET00uc2Nyb2xsZXIpXG57aW1hZ2VzTG9hZGVkKERPTS5zY3JvbGxlcixmdW5jdGlvbigpe3ZhciBleHRyYWRlbGF5PTEwMDA7YW5pbWUoe3RhcmdldHM6RE9NLmxvYWRlcixkdXJhdGlvbjo2MDAsZWFzaW5nOidlYXNlSW5PdXRDdWJpYycsZGVsYXk6ZXh0cmFkZWxheSx0cmFuc2xhdGVZOictMTAwJScsYmVnaW46ZnVuY3Rpb24oKXtpbml0KCk7fSxjb21wbGV0ZTpmdW5jdGlvbigpe0RPTS5sb2FkZXIuY2xhc3NMaXN0LnJlbW92ZSgnb3ZlcmxheS0tYWN0aXZlJyk7fX0pO30pO319KTtlbGVtZW50b3JGcm9udGVuZC5ob29rcy5hZGRBY3Rpb24oJ2Zyb250ZW5kL2VsZW1lbnRfcmVhZHkvZ3JhbmRyZXN0YXVyYW50LXNsaWRlci1tdWx0aS1sYXlvdXRzLmRlZmF1bHQnLGZ1bmN0aW9uKCRzY29wZSl7ZG9jdW1lbnQuZG9jdW1lbnRFbGVtZW50LmNsYXNzTmFtZT0nanMnO3ZhciBzbGlkZXNob3c9bmV3IE1MU2xpZGVzaG93KGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoJy5zbGlkZXNob3cnKSk7aWYoZG9jdW1lbnQucXVlcnlTZWxlY3RvcignI25leHQtc2xpZGUnKSlcbntkb2N1bWVudC5xdWVyeVNlbGVjdG9yKCcjbmV4dC1zbGlkZScpLmFkZEV2ZW50TGlzdGVuZXIoJ2NsaWNrJyxmdW5jdGlvbigpe3NsaWRlc2hvdy5uZXh0KCk7fSk7fVxuaWYoZG9jdW1lbnQucXVlcnlTZWxlY3RvcignI3ByZXYtc2xpZGUnKSlcbntkb2N1bWVudC5xdWVyeVNlbGVjdG9yKCcjcHJldi1zbGlkZScpLmFkZEV2ZW50TGlzdGVuZXIoJ2NsaWNrJyxmdW5jdGlvbigpe3NsaWRlc2hvdy5wcmV2KCk7fSk7fX0pO2VsZW1lbnRvckZyb250ZW5kLmhvb2tzLmFkZEFjdGlvbignZnJvbnRlbmQvZWxlbWVudF9yZWFkeS9ncmFuZHJlc3RhdXJhbnQtc2xpZGVyLXZlbG8uZGVmYXVsdCcsZnVuY3Rpb24oJHNjb3BlKXtqUXVlcnkoJ2JvZHknKS5hZGRDbGFzcygnZWxlbWVudG9yLWZ1bGxzY3JlZW4nKTt2YXIgc2NhbGVEb3duQW1udD0wLjc7dmFyIGJveFNoYWRvd0FtbnQ9JzQwcHgnOyQuVmVsb2NpdHkuUmVnaXN0ZXJFZmZlY3QoXCJ0cmFuc2xhdGVVcFwiLHtkZWZhdWx0RHVyYXRpb246MSxjYWxsczpbW3t0cmFuc2xhdGVZOictMTAwJSd9LDFdXX0pOyQuVmVsb2NpdHkuUmVnaXN0ZXJFZmZlY3QoXCJ0cmFuc2xhdGVEb3duXCIse2RlZmF1bHREdXJhdGlvbjoxLGNhbGxzOltbe3RyYW5zbGF0ZVk6JzEwMCUnfSwxXV19KTskLlZlbG9jaXR5LlJlZ2lzdGVyRWZmZWN0KFwidHJhbnNsYXRlTm9uZVwiLHtkZWZhdWx0RHVyYXRpb246MSxjYWxsczpbW3t0cmFuc2xhdGVZOicwJyxvcGFjaXR5OicxJyxzY2FsZTonMScsfSwxXV19KTskLlZlbG9jaXR5LlJlZ2lzdGVyRWZmZWN0KFwic2NhbGVEb3duXCIse2RlZmF1bHREdXJhdGlvbjoxLGNhbGxzOltbe29wYWNpdHk6JzAnLHNjYWxlOicwLjcnLH0sMV1dfSk7JC5WZWxvY2l0eS5SZWdpc3RlckVmZmVjdChcInNjYWxlRG93bi5tb3ZlVXBcIix7ZGVmYXVsdER1cmF0aW9uOjEsY2FsbHM6W1t7dHJhbnNsYXRlWTonMCUnLHNjYWxlOnNjYWxlRG93bkFtbnQsfSwwLjIwXSxbe3RyYW5zbGF0ZVk6Jy0xMDAlJ30sMC42MF0sW3t0cmFuc2xhdGVZOictMTAwJScsc2NhbGU6JzEnLH0sMC4yMF1dfSk7JC5WZWxvY2l0eS5SZWdpc3RlckVmZmVjdChcInNjYWxlRG93bi5tb3ZlVXAuc2Nyb2xsXCIse2RlZmF1bHREdXJhdGlvbjoxLGNhbGxzOltbe3RyYW5zbGF0ZVk6Jy0xMDAlJyxzY2FsZTpzY2FsZURvd25BbW50LH0sMC42MF0sW3t0cmFuc2xhdGVZOictMTAwJScsc2NhbGU6JzEnLH0sMC40MF1dfSk7JC5WZWxvY2l0eS5SZWdpc3RlckVmZmVjdChcInNjYWxlVXAubW92ZVVwXCIse2RlZmF1bHREdXJhdGlvbjoxLGNhbGxzOltbe3RyYW5zbGF0ZVk6JzkwJScsc2NhbGU6c2NhbGVEb3duQW1udCx9LDAuMjBdLFt7dHJhbnNsYXRlWTonMCUnfSwwLjYwXSxbe3RyYW5zbGF0ZVk6JzAlJyxzY2FsZTonMScsfSwwLjIwXV19KTskLlZlbG9jaXR5LlJlZ2lzdGVyRWZmZWN0KFwic2NhbGVVcC5tb3ZlVXAuc2Nyb2xsXCIse2RlZmF1bHREdXJhdGlvbjoxLGNhbGxzOltbe3RyYW5zbGF0ZVk6JzAlJyxzY2FsZTpzY2FsZURvd25BbW50LH0sMC42MF0sW3t0cmFuc2xhdGVZOicwJScsc2NhbGU6JzEnLH0sMC40MF1dfSk7JC5WZWxvY2l0eS5SZWdpc3RlckVmZmVjdChcInNjYWxlRG93bi5tb3ZlRG93blwiLHtkZWZhdWx0RHVyYXRpb246MSxjYWxsczpbW3t0cmFuc2xhdGVZOicwJScsc2NhbGU6c2NhbGVEb3duQW1udCx9LDAuMjBdLFt7dHJhbnNsYXRlWTonMTAwJSd9LDAuNjBdLFt7dHJhbnNsYXRlWTonMTAwJScsc2NhbGU6JzEnLH0sMC4yMF1dfSk7JC5WZWxvY2l0eS5SZWdpc3RlckVmZmVjdChcInNjYWxlRG93bi5tb3ZlRG93bi5zY3JvbGxcIix7ZGVmYXVsdER1cmF0aW9uOjEsY2FsbHM6W1t7fSwwLjYwXSxbe3RyYW5zbGF0ZVk6JzEwMCUnLHNjYWxlOicxJyx9LDAuNDBdXX0pOyQuVmVsb2NpdHkuUmVnaXN0ZXJFZmZlY3QoXCJzY2FsZVVwLm1vdmVEb3duXCIse2RlZmF1bHREdXJhdGlvbjoxLGNhbGxzOltbe3RyYW5zbGF0ZVk6Jy05MCUnLHNjYWxlOnNjYWxlRG93bkFtbnQsfSwwLjIwXSxbe3RyYW5zbGF0ZVk6JzAlJ30sMC42MF0sW3t0cmFuc2xhdGVZOicwJScsc2NhbGU6JzEnLH0sMC4yMF1dfSk7dmFyIFZlbG9TbGlkZXI9KGZ1bmN0aW9uKCl7dmFyIHNldHRpbmdzPXt2ZWxvSW5pdDokKCcudmVsby1zbGlkZXMnKS5kYXRhKCd2ZWxvLXNsaWRlcicpLCR2ZWxvU2xpZGU6JCgnLnZlbG8tc2xpZGUnKSx2ZWxvU2xpZGVCZzonLnZlbG8tc2xpZGUtYmcnLG5hdlByZXY6JCgnLnZlbG8tc2xpZGVzLW5hdicpLmZpbmQoJ2EuanMtdmVsby1zbGlkZXMtcHJldicpLG5hdk5leHQ6JCgnLnZlbG8tc2xpZGVzLW5hdicpLmZpbmQoJ2EuanMtdmVsby1zbGlkZXMtbmV4dCcpLHZlbG9CdG46JCgnLnZlbG8tc2xpZGUtYnRuJyksZGVsdGE6MCxzY3JvbGxUaHJlc2hvbGQ6NyxjdXJyZW50U2xpZGU6MSxhbmltYXRpbmc6ZmFsc2UsYW5pbWF0aW9uRHVyYXRpb246MjAwMH07dmFyIGRlbHRhPTAsYW5pbWF0aW5nPWZhbHNlO3JldHVybntpbml0OmZ1bmN0aW9uKCl7dGhpcy5iaW5kKCk7fSxiaW5kOmZ1bmN0aW9uKCl7c2V0dGluZ3MuJHZlbG9TbGlkZS5maXJzdCgpLmFkZENsYXNzKCdpcy1hY3RpdmUnKTtpZihzZXR0aW5ncy52ZWxvSW5pdD09J29uJyl7VmVsb1NsaWRlci5pbml0U2Nyb2xsSmFjaygpOyQod2luZG93KS5vbignRE9NTW91c2VTY3JvbGwgbW91c2V3aGVlbCcsVmVsb1NsaWRlci5zY3JvbGxKYWNraW5nKTt9XG5zZXR0aW5ncy5uYXZQcmV2Lm9uKCdjbGljaycsVmVsb1NsaWRlci5wcmV2U2xpZGUpO3NldHRpbmdzLm5hdk5leHQub24oJ2NsaWNrJyxWZWxvU2xpZGVyLm5leHRTbGlkZSk7JChkb2N1bWVudCkub24oJ2tleWRvd24nLGZ1bmN0aW9uKGUpe3ZhciBrZXlOZXh0PShlLndoaWNoPT0zOXx8ZS53aGljaD09NDApLGtleVByZXY9KGUud2hpY2g9PTM3fHxlLndoaWNoPT0zOCk7aWYoa2V5TmV4dCYmIXNldHRpbmdzLm5hdk5leHQuaGFzQ2xhc3MoJ2luYWN0aXZlJykpe2UucHJldmVudERlZmF1bHQoKTtWZWxvU2xpZGVyLm5leHRTbGlkZSgpO31lbHNlIGlmKGtleVByZXYmJighc2V0dGluZ3MubmF2UHJldi5oYXNDbGFzcygnaW5hY3RpdmUnKSkpe2UucHJldmVudERlZmF1bHQoKTtWZWxvU2xpZGVyLnByZXZTbGlkZSgpO319KTtqUXVlcnkoJ2JvZHknKS5vbigndG91Y2htb3ZlJyxmdW5jdGlvbihlKXtlLnByZXZlbnREZWZhdWx0KCk7ZS5zdG9wUHJvcGFnYXRpb24oKTtyZXR1cm4gZmFsc2U7fSk7dmFyIHRzO2pRdWVyeShkb2N1bWVudCkuYmluZCgndG91Y2hzdGFydCcsZnVuY3Rpb24oZSl7dHM9ZS5vcmlnaW5hbEV2ZW50LnRvdWNoZXNbMF0uY2xpZW50WTt9KTtqUXVlcnkoZG9jdW1lbnQpLmJpbmQoJ3RvdWNoZW5kJyxmdW5jdGlvbihlKXt2YXIgdGU9ZS5vcmlnaW5hbEV2ZW50LmNoYW5nZWRUb3VjaGVzWzBdLmNsaWVudFk7aWYodHM+dGUrNSl7VmVsb1NsaWRlci5uZXh0U2xpZGUoKTt9ZWxzZSBpZih0czx0ZS01KXtWZWxvU2xpZGVyLnByZXZTbGlkZSgpO319KTtWZWxvU2xpZGVyLmNoZWNrTmF2aWdhdGlvbigpO1ZlbG9TbGlkZXIuaG92ZXJBbmltYXRpb24oKTt9LGhvdmVyQW5pbWF0aW9uOmZ1bmN0aW9uKCl7c2V0dGluZ3MudmVsb0J0bi5ob3ZlcihmdW5jdGlvbigpeyQodGhpcykuY2xvc2VzdChzZXR0aW5ncy4kdmVsb1NsaWRlKS50b2dnbGVDbGFzcygnaXMtaG92ZXJpbmcnKTt9KTt9LHNldEFuaW1hdGlvbjpmdW5jdGlvbihtaWRTdGVwLGRpcmVjdGlvbil7dmFyIGFuaW1hdGlvblZpc2libGU9J3RyYW5zbGF0ZU5vbmUnLGFuaW1hdGlvblRvcD0ndHJhbnNsYXRlVXAnLGFuaW1hdGlvbkJvdHRvbT0ndHJhbnNsYXRlRG93bicsZWFzaW5nPSdlYXNlJyxhbmltRHVyYXRpb249c2V0dGluZ3MuYW5pbWF0aW9uRHVyYXRpb247aWYobWlkU3RlcCl7YW5pbWF0aW9uVmlzaWJsZT0nc2NhbGVVcC5tb3ZlVXAuc2Nyb2xsJzthbmltYXRpb25Ub3A9J3NjYWxlRG93bi5tb3ZlVXAuc2Nyb2xsJzthbmltYXRpb25Cb3R0b209J3NjYWxlRG93bi5tb3ZlRG93bi5zY3JvbGwnO31lbHNle2FuaW1hdGlvblZpc2libGU9KGRpcmVjdGlvbj09J25leHQnKT8nc2NhbGVVcC5tb3ZlVXAnOidzY2FsZVVwLm1vdmVEb3duJzthbmltYXRpb25Ub3A9J3NjYWxlRG93bi5tb3ZlVXAnO2FuaW1hdGlvbkJvdHRvbT0nc2NhbGVEb3duLm1vdmVEb3duJzt9XG5yZXR1cm5bYW5pbWF0aW9uVmlzaWJsZSxhbmltYXRpb25Ub3AsYW5pbWF0aW9uQm90dG9tLGFuaW1EdXJhdGlvbixlYXNpbmddO30saW5pdFNjcm9sbEphY2s6ZnVuY3Rpb24oKXt2YXIgdmlzaWJsZVNsaWRlPXNldHRpbmdzLiR2ZWxvU2xpZGUuZmlsdGVyKCcuaXMtYWN0aXZlJyksdG9wU2VjdGlvbj12aXNpYmxlU2xpZGUucHJldkFsbChzZXR0aW5ncy4kdmVsb1NsaWRlKSxib3R0b21TZWN0aW9uPXZpc2libGVTbGlkZS5uZXh0QWxsKHNldHRpbmdzLiR2ZWxvU2xpZGUpLGFuaW1hdGlvblBhcmFtcz1WZWxvU2xpZGVyLnNldEFuaW1hdGlvbihmYWxzZSksYW5pbWF0aW9uVmlzaWJsZT1hbmltYXRpb25QYXJhbXNbMF0sYW5pbWF0aW9uVG9wPWFuaW1hdGlvblBhcmFtc1sxXSxhbmltYXRpb25Cb3R0b209YW5pbWF0aW9uUGFyYW1zWzJdO3Zpc2libGVTbGlkZS5jaGlsZHJlbignZGl2JykudmVsb2NpdHkoYW5pbWF0aW9uVmlzaWJsZSwxLGZ1bmN0aW9uKCl7dmlzaWJsZVNsaWRlLmNzcygnb3BhY2l0eScsMSk7dG9wU2VjdGlvbi5jc3MoJ29wYWNpdHknLDEpO2JvdHRvbVNlY3Rpb24uY3NzKCdvcGFjaXR5JywxKTt9KTt0b3BTZWN0aW9uLmNoaWxkcmVuKCdkaXYnKS52ZWxvY2l0eShhbmltYXRpb25Ub3AsMCk7Ym90dG9tU2VjdGlvbi5jaGlsZHJlbignZGl2JykudmVsb2NpdHkoYW5pbWF0aW9uQm90dG9tLDApO30sc2Nyb2xsSmFja2luZzpmdW5jdGlvbihlKXtpZighalF1ZXJ5KCdib2R5JykuaGFzQ2xhc3MoJ2pzX25hdicpKVxue2lmKGUub3JpZ2luYWxFdmVudC5kZXRhaWw8MHx8ZS5vcmlnaW5hbEV2ZW50LndoZWVsRGVsdGE+MCl7ZGVsdGEtLTsoTWF0aC5hYnMoZGVsdGEpPj1zZXR0aW5ncy5zY3JvbGxUaHJlc2hvbGQpJiZWZWxvU2xpZGVyLnByZXZTbGlkZSgpO31lbHNle2RlbHRhKys7KGRlbHRhPj1zZXR0aW5ncy5zY3JvbGxUaHJlc2hvbGQpJiZWZWxvU2xpZGVyLm5leHRTbGlkZSgpO31cbnJldHVybiBmYWxzZTt9fSxwcmV2U2xpZGU6ZnVuY3Rpb24oZSl7dHlwZW9mIGUhPT0ndW5kZWZpbmVkJyYmZS5wcmV2ZW50RGVmYXVsdCgpO3ZhciB2aXNpYmxlU2xpZGU9c2V0dGluZ3MuJHZlbG9TbGlkZS5maWx0ZXIoJy5pcy1hY3RpdmUnKSxhbmltYXRpb25QYXJhbXM9VmVsb1NsaWRlci5zZXRBbmltYXRpb24obWlkU3RlcCwncHJldicpLG1pZFN0ZXA9ZmFsc2U7dmlzaWJsZVNsaWRlPW1pZFN0ZXA/dmlzaWJsZVNsaWRlLm5leHQoc2V0dGluZ3MuJHZlbG9TbGlkZSk6dmlzaWJsZVNsaWRlO2lmKCFhbmltYXRpbmcmJiF2aXNpYmxlU2xpZGUuaXMoXCI6Zmlyc3QtY2hpbGRcIikpe2FuaW1hdGluZz10cnVlO3Zpc2libGVTbGlkZS5yZW1vdmVDbGFzcygnaXMtYWN0aXZlJykuY2hpbGRyZW4oc2V0dGluZ3MudmVsb1NsaWRlQmcpLnZlbG9jaXR5KGFuaW1hdGlvblBhcmFtc1syXSxhbmltYXRpb25QYXJhbXNbM10sYW5pbWF0aW9uUGFyYW1zWzRdKS5lbmQoKS5wcmV2KHNldHRpbmdzLiR2ZWxvU2xpZGUpLmFkZENsYXNzKCdpcy1hY3RpdmUnKS5jaGlsZHJlbihzZXR0aW5ncy52ZWxvU2xpZGVCZykudmVsb2NpdHkoYW5pbWF0aW9uUGFyYW1zWzBdLGFuaW1hdGlvblBhcmFtc1szXSxhbmltYXRpb25QYXJhbXNbNF0sZnVuY3Rpb24oKXthbmltYXRpbmc9ZmFsc2U7fSk7Y3VycmVudFNsaWRlPXNldHRpbmdzLmN1cnJlbnRTbGlkZS0xO31cblZlbG9TbGlkZXIucmVzZXRTY3JvbGwoKTt9LG5leHRTbGlkZTpmdW5jdGlvbihlKXt0eXBlb2YgZSE9PSd1bmRlZmluZWQnJiZlLnByZXZlbnREZWZhdWx0KCk7dmFyIHZpc2libGVTbGlkZT1zZXR0aW5ncy4kdmVsb1NsaWRlLmZpbHRlcignLmlzLWFjdGl2ZScpLGFuaW1hdGlvblBhcmFtcz1WZWxvU2xpZGVyLnNldEFuaW1hdGlvbihtaWRTdGVwLCduZXh0JyksbWlkU3RlcD1mYWxzZTtpZighYW5pbWF0aW5nJiYhdmlzaWJsZVNsaWRlLmlzKFwiOmxhc3Qtb2YtdHlwZVwiKSl7YW5pbWF0aW5nPXRydWU7dmlzaWJsZVNsaWRlLnJlbW92ZUNsYXNzKCdpcy1hY3RpdmUnKS5jaGlsZHJlbihzZXR0aW5ncy52ZWxvU2xpZGVCZykudmVsb2NpdHkoYW5pbWF0aW9uUGFyYW1zWzFdLGFuaW1hdGlvblBhcmFtc1szXSkuZW5kKCkubmV4dChzZXR0aW5ncy4kdmVsb1NsaWRlKS5hZGRDbGFzcygnaXMtYWN0aXZlJykuY2hpbGRyZW4oc2V0dGluZ3MudmVsb1NsaWRlQmcpLnZlbG9jaXR5KGFuaW1hdGlvblBhcmFtc1swXSxhbmltYXRpb25QYXJhbXNbM10sZnVuY3Rpb24oKXthbmltYXRpbmc9ZmFsc2U7fSk7Y3VycmVudFNsaWRlPXNldHRpbmdzLmN1cnJlbnRTbGlkZSsxO31cblZlbG9TbGlkZXIucmVzZXRTY3JvbGwoKTt9LHJlc2V0U2Nyb2xsOmZ1bmN0aW9uKCl7ZGVsdGE9MDtWZWxvU2xpZGVyLmNoZWNrTmF2aWdhdGlvbigpO30sY2hlY2tOYXZpZ2F0aW9uOmZ1bmN0aW9uKCl7KHNldHRpbmdzLiR2ZWxvU2xpZGUuZmlsdGVyKCcuaXMtYWN0aXZlJykuaXMoJzpmaXJzdC1vZi10eXBlJykpP3NldHRpbmdzLm5hdlByZXYuYWRkQ2xhc3MoJ2luYWN0aXZlJyk6c2V0dGluZ3MubmF2UHJldi5yZW1vdmVDbGFzcygnaW5hY3RpdmUnKTsoc2V0dGluZ3MuJHZlbG9TbGlkZS5maWx0ZXIoJy5pcy1hY3RpdmUnKS5pcygnOmxhc3Qtb2YtdHlwZScpKT9zZXR0aW5ncy5uYXZOZXh0LmFkZENsYXNzKCdpbmFjdGl2ZScpOnNldHRpbmdzLm5hdk5leHQucmVtb3ZlQ2xhc3MoJ2luYWN0aXZlJyk7fSx9O30pKCk7VmVsb1NsaWRlci5pbml0KCk7fSk7ZWxlbWVudG9yRnJvbnRlbmQuaG9va3MuYWRkQWN0aW9uKCdmcm9udGVuZC9lbGVtZW50X3JlYWR5L2dyYW5kcmVzdGF1cmFudC1zbGlkZXItcG9wb3V0LmRlZmF1bHQnLGZ1bmN0aW9uKCRzY29wZSl7alF1ZXJ5KCdib2R5JykuYWRkQ2xhc3MoJ2VsZW1lbnRvci1mdWxsc2NyZWVuJyk7ZnVuY3Rpb24gZ29Ub1NsaWRlKG51bWJlcil7JCgnLnNsaWRlci1zbGlkZScpLnJlbW92ZUNsYXNzKCdzbGlkZXItc2xpZGUtLWFjdGl2ZScpOyQoJy5zbGlkZXItc2xpZGVbZGF0YS1zbGlkZT0nK251bWJlcisnXScpLmFkZENsYXNzKCdzbGlkZXItc2xpZGUtLWFjdGl2ZScpO31cbiQoJy5zbGlkZXJfX25leHQsIC5nby10by1uZXh0Jykub24oJ2NsaWNrJyxmdW5jdGlvbigpe3ZhciBjdXJyZW50U2xpZGU9TnVtYmVyKCQoJy5zbGlkZXItc2xpZGUtLWFjdGl2ZScpLmRhdGEoJ3NsaWRlJykpO3ZhciB0b3RhbFNsaWRlcz0kKCcuc2xpZGVyLXNsaWRlJykubGVuZ3RoO2N1cnJlbnRTbGlkZSsrXG5pZihjdXJyZW50U2xpZGU+dG90YWxTbGlkZXMpe2N1cnJlbnRTbGlkZT0xO31cbmdvVG9TbGlkZShjdXJyZW50U2xpZGUpO30pO2Zvcih2YXIgaT0xO2k8PSQoJy5zbGlkZXItc2xpZGUnKS5sZW5ndGg7aSsrKXskKCcuc2xpZGVyX19pbmRpY2F0b3JzJykuYXBwZW5kKCc8ZGl2IGNsYXNzPVwic2xpZGVyX19pbmRpY2F0b3JcIiBkYXRhLXNsaWRlPVwiJytpKydcIj48L2Rpdj4nKX1cbnNldFRpbWVvdXQoZnVuY3Rpb24oKXskKCcuc2xpZGVyLXdyYXAnKS5hZGRDbGFzcygnc2xpZGVyLXdyYXAtLWhhY2tlZCcpO30sMTAwMCk7fSk7ZWxlbWVudG9yRnJvbnRlbmQuaG9va3MuYWRkQWN0aW9uKCdmcm9udGVuZC9lbGVtZW50X3JlYWR5L2dyYW5kcmVzdGF1cmFudC1zbGlkZXItY2xpcC1wYXRoLmRlZmF1bHQnLGZ1bmN0aW9uKCRzY29wZSl7alF1ZXJ5KCdib2R5JykuYWRkQ2xhc3MoJ2VsZW1lbnRvci1mdWxsc2NyZWVuJyk7KGZ1bmN0aW9uKCl7dmFyICRzbGlkZXM9ZG9jdW1lbnQucXVlcnlTZWxlY3RvckFsbCgnLnNsaWRlJyk7dmFyICRjb250cm9scz1kb2N1bWVudC5xdWVyeVNlbGVjdG9yQWxsKCcuc2xpZGVyLWNvbnRyb2wnKTt2YXIgbnVtT2ZTbGlkZXM9JHNsaWRlcy5sZW5ndGg7dmFyIHNsaWRpbmdBVD0xMzAwO3ZhciBzbGlkaW5nQmxvY2tlZD1mYWxzZTtbXS5zbGljZS5jYWxsKCRzbGlkZXMpLmZvckVhY2goZnVuY3Rpb24oJGVsLGluZGV4KXt2YXIgaT1pbmRleCsxOyRlbC5jbGFzc0xpc3QuYWRkKCdzbGlkZS0nK2kpOyRlbC5kYXRhc2V0LnNsaWRlPWk7fSk7W10uc2xpY2UuY2FsbCgkY29udHJvbHMpLmZvckVhY2goZnVuY3Rpb24oJGVsKXskZWwuYWRkRXZlbnRMaXN0ZW5lcignY2xpY2snLGNvbnRyb2xDbGlja0hhbmRsZXIpO30pO2Z1bmN0aW9uIGNvbnRyb2xDbGlja0hhbmRsZXIoKXtpZihzbGlkaW5nQmxvY2tlZClyZXR1cm47c2xpZGluZ0Jsb2NrZWQ9dHJ1ZTt2YXIgJGNvbnRyb2w9dGhpczt2YXIgaXNSaWdodD0kY29udHJvbC5jbGFzc0xpc3QuY29udGFpbnMoJ20tLXJpZ2h0Jyk7dmFyICRjdXJBY3RpdmU9ZG9jdW1lbnQucXVlcnlTZWxlY3RvcignLnNsaWRlLnMtLWFjdGl2ZScpO3ZhciBpbmRleD0rJGN1ckFjdGl2ZS5kYXRhc2V0LnNsaWRlOyhpc1JpZ2h0KT9pbmRleCsrOmluZGV4LS07aWYoaW5kZXg8MSlpbmRleD1udW1PZlNsaWRlcztpZihpbmRleD5udW1PZlNsaWRlcylpbmRleD0xO3ZhciAkbmV3QWN0aXZlPWRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoJy5zbGlkZS0nK2luZGV4KTskY29udHJvbC5jbGFzc0xpc3QuYWRkKCdhLS1yb3RhdGlvbicpOyRjdXJBY3RpdmUuY2xhc3NMaXN0LnJlbW92ZSgncy0tYWN0aXZlJywncy0tYWN0aXZlLXByZXYnKTtkb2N1bWVudC5xdWVyeVNlbGVjdG9yKCcuc2xpZGUucy0tcHJldicpLmNsYXNzTGlzdC5yZW1vdmUoJ3MtLXByZXYnKTskbmV3QWN0aXZlLmNsYXNzTGlzdC5hZGQoJ3MtLWFjdGl2ZScpO2lmKCFpc1JpZ2h0KSRuZXdBY3RpdmUuY2xhc3NMaXN0LmFkZCgncy0tYWN0aXZlLXByZXYnKTt2YXIgcHJldkluZGV4PWluZGV4LTE7aWYocHJldkluZGV4PDEpcHJldkluZGV4PW51bU9mU2xpZGVzO2RvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoJy5zbGlkZS0nK3ByZXZJbmRleCkuY2xhc3NMaXN0LmFkZCgncy0tcHJldicpO3NldFRpbWVvdXQoZnVuY3Rpb24oKXskY29udHJvbC5jbGFzc0xpc3QucmVtb3ZlKCdhLS1yb3RhdGlvbicpO3NsaWRpbmdCbG9ja2VkPWZhbHNlO30sc2xpZGluZ0FUKjAuNzUpO307fSgpKTt9KTtlbGVtZW50b3JGcm9udGVuZC5ob29rcy5hZGRBY3Rpb24oJ2Zyb250ZW5kL2VsZW1lbnRfcmVhZHkvZ3JhbmRyZXN0YXVyYW50LWdhbGxlcnktcHJldmlldy5kZWZhdWx0JyxmdW5jdGlvbigkc2NvcGUpe2pRdWVyeSgnYm9keScpLmFkZENsYXNzKCdlbGVtZW50b3ItZnVsbHNjcmVlbicpO2pRdWVyeSgnYm9keScpLmFkZENsYXNzKCdncmFuZHJlc3RhdXJhbnQtZ2FsbGVyeS1wcmV2aWV3Jyk7dmFyICRzbGlkZXI9JCgnLnNsaWRlcicpO3ZhciAkc2xpY2tUcmFjaz0kKCcuc2xpY2stdHJhY2snKTt2YXIgJHNsaWNrQ3VycmVudD0kKCcuc2xpY2stY3VycmVudCcpO3ZhciBzbGlkZUR1cmF0aW9uPTkwMDskc2xpZGVyLm9uKCdpbml0JyxmdW5jdGlvbihzbGljayl7VHdlZW5NYXgudG8oJCgnLnNsaWNrLXRyYWNrJyksMC45LHttYXJnaW5MZWZ0OjB9KTtUd2Vlbk1heC50bygkKCcuc2xpY2stYWN0aXZlJyksMC45LHt4OjAsekluZGV4OjJ9KTt9KTskc2xpZGVyLm9uKCdiZWZvcmVDaGFuZ2UnLGZ1bmN0aW9uKGV2ZW50LHNsaWNrLGN1cnJlbnRTbGlkZSxuZXh0U2xpZGUpe1R3ZWVuTWF4LnRvKCQoJy5zbGljay10cmFjaycpLDAuOSx7bWFyZ2luTGVmdDowfSk7VHdlZW5NYXgudG8oJCgnLnNsaWNrLWFjdGl2ZScpLDAuOSx7eDowfSk7fSk7JHNsaWRlci5vbignYWZ0ZXJDaGFuZ2UnLGZ1bmN0aW9uKGV2ZW50LHNsaWNrLGN1cnJlbnRTbGlkZSl7VHdlZW5NYXgudG8oJCgnLnNsaWNrLXRyYWNrJyksMC45LHttYXJnaW5MZWZ0OjB9KTskKCcuc2xpY2stc2xpZGUnKS5jc3MoJ3otaW5kZXgnLCcxJyk7VHdlZW5NYXgudG8oJCgnLnNsaWNrLWFjdGl2ZScpLDAuOSx7eDowLHpJbmRleDoyfSk7fSk7dmFyICRzbGlkZXJQYWdpbmF0aW9uPSQoJy5zbGlkZXInKS5hdHRyKCdkYXRhLXBhZ2luYXRpb24nKTt2YXIgZG90c1Zhcj1mYWxzZTtpZigkc2xpZGVyUGFnaW5hdGlvbj09J3llcycpXG57ZG90c1Zhcj10cnVlO31cbnZhciAkc2xpZGVyTmF2aWdhdGlvbj0kKCcuc2xpZGVyJykuYXR0cignZGF0YS1uYXZpZ2F0aW9uJyk7dmFyIGFycm93c1Zhcj1mYWxzZTtpZigkc2xpZGVyTmF2aWdhdGlvbj09J3llcycpXG57YXJyb3dzVmFyPXRydWU7fVxudmFyICRzbGlkZXJBdXRvUGxheT0kKCcuc2xpZGVyJykuYXR0cignZGF0YS1hdXRvcGxheScpO3ZhciBhdXRvUGxheVZhcj1mYWxzZTt2YXIgYXV0b1BsYXlUaW1lVmFyPTA7aWYodHlwZW9mICRzbGlkZXJBdXRvUGxheSE9XCJ1bmRlZmluZWRcIil7YXV0b1BsYXlWYXI9dHJ1ZTthdXRvUGxheVRpbWVWYXI9JHNsaWRlckF1dG9QbGF5O31cbiRzbGlkZXIuc2xpY2soe3NwZWVkOnNsaWRlRHVyYXRpb24sdG91Y2hNb3ZlOnRydWUsZG90czpkb3RzVmFyLGFycm93czphcnJvd3NWYXIsd2FpdEZvckFuaW1hdGU6dHJ1ZSx1c2VUcmFuc2Zvcm06dHJ1ZSxhdXRvcGxheTphdXRvUGxheVZhcixhdXRvcGxheVNwZWVkOmF1dG9QbGF5VGltZVZhcixjc3NFYXNlOidjdWJpYy1iZXppZXIoMC40NTUsIDAuMDMwLCAwLjEzMCwgMS4wMDApJ30pXG4kKCcuc2xpY2stcHJldicpLm9uKCdtb3VzZWVudGVyJyxmdW5jdGlvbigpe1R3ZWVuTWF4LnRvKCQoJy5zbGljay10cmFjaycpLDAuNix7bWFyZ2luTGVmdDpcIjE4MHB4XCIsZWFzZTpRdWFkLmVhc2VPdXR9KTtUd2Vlbk1heC50bygkKCcuc2xpY2stY3VycmVudCcpLDAuNix7eDotMTAwLGVhc2U6UXVhZC5lYXNlT3V0fSk7fSk7JCgnLnNsaWNrLXByZXYnKS5vbignbW91c2VsZWF2ZScsZnVuY3Rpb24oKXtUd2Vlbk1heC50bygkKCcuc2xpY2stdHJhY2snKSwwLjQse21hcmdpbkxlZnQ6MCxlYXNlOlNpbmUuZWFzZUluT3V0fSk7VHdlZW5NYXgudG8oJCgnLnNsaWNrLWN1cnJlbnQnKSwwLjQse3g6MCxlYXNlOlNpbmUuZWFzZUluT3V0fSk7fSk7JCgnLnNsaWNrLW5leHQnKS5vbignbW91c2VlbnRlcicsZnVuY3Rpb24oKXtUd2Vlbk1heC50bygkKCcuc2xpY2stdHJhY2snKSwwLjYse21hcmdpbkxlZnQ6XCItMTgwcHhcIixlYXNlOlF1YWQuZWFzZU91dH0pO1R3ZWVuTWF4LnRvKCQoJy5zbGljay1jdXJyZW50JyksMC42LHt4OjEwMCxlYXNlOlF1YWQuZWFzZU91dH0pO30pOyQoJy5zbGljay1uZXh0Jykub24oJ21vdXNlbGVhdmUnLGZ1bmN0aW9uKCl7VHdlZW5NYXgudG8oJCgnLnNsaWNrLXRyYWNrJyksMC40LHttYXJnaW5MZWZ0OjAsZWFzZTpRdWFkLmVhc2VJbk91dH0pO1R3ZWVuTWF4LnRvKCQoJy5zbGljay1jdXJyZW50JyksMC40LHt4OjAsZWFzZTpRdWFkLmVhc2VJbk91dH0pO30pO30pO2VsZW1lbnRvckZyb250ZW5kLmhvb2tzLmFkZEFjdGlvbignZnJvbnRlbmQvZWxlbWVudF9yZWFkeS9ncmFuZHJlc3RhdXJhbnQtc2xpZGVyLXNwbGl0LXNsaWNrLmRlZmF1bHQnLGZ1bmN0aW9uKCRzY29wZSl7alF1ZXJ5KCdib2R5JykuYWRkQ2xhc3MoJ2VsZW1lbnRvci1mdWxsc2NyZWVuJyk7dmFyICRzbGlkZXI9JCgnLnNsaWRlc2hvdyAuc2xpZGVyJyksbWF4SXRlbXM9JCgnLml0ZW0nLCRzbGlkZXIpLmxlbmd0aCxkcmFnZ2luZz1mYWxzZSx0cmFja2luZyxyaWdodFRyYWNraW5nOyRzbGlkZXJSaWdodD0kKCcuc2xpZGVzaG93JykuY2xvbmUoKS5hZGRDbGFzcygnc2xpZGVzaG93LXJpZ2h0JykuYXBwZW5kVG8oJCgnLnNwbGl0LXNsaWRlc2hvdycpKTtyaWdodEl0ZW1zPSQoJy5pdGVtJywkc2xpZGVyUmlnaHQpLnRvQXJyYXkoKTtyZXZlcnNlSXRlbXM9cmlnaHRJdGVtcy5yZXZlcnNlKCk7JCgnLnNsaWRlcicsJHNsaWRlclJpZ2h0KS5odG1sKCcnKTtmb3IoaT0wO2k8bWF4SXRlbXM7aSsrKXskKHJldmVyc2VJdGVtc1tpXSkuYXBwZW5kVG8oJCgnLnNsaWRlcicsJHNsaWRlclJpZ2h0KSk7fVxuJHNsaWRlci5hZGRDbGFzcygnc2xpZGVzaG93LWxlZnQnKTskKCcuc2xpZGVzaG93LWxlZnQnKS5zbGljayh7dmVydGljYWw6dHJ1ZSx2ZXJ0aWNhbFN3aXBpbmc6dHJ1ZSxhcnJvd3M6ZmFsc2UsaW5maW5pdGU6dHJ1ZSxkb3RzOnRydWUsc3BlZWQ6MTAwMCxjc3NFYXNlOidjdWJpYy1iZXppZXIoMC43LCAwLCAwLjMsIDEpJ30pLm9uKCdiZWZvcmVDaGFuZ2UnLGZ1bmN0aW9uKGV2ZW50LHNsaWNrLGN1cnJlbnRTbGlkZSxuZXh0U2xpZGUpe2lmKGN1cnJlbnRTbGlkZT5uZXh0U2xpZGUmJm5leHRTbGlkZT09MCYmY3VycmVudFNsaWRlPT1tYXhJdGVtcy0xKXskKCcuc2xpZGVzaG93LXJpZ2h0IC5zbGlkZXInKS5zbGljaygnc2xpY2tHb1RvJywtMSk7JCgnLnNsaWRlc2hvdy10ZXh0Jykuc2xpY2soJ3NsaWNrR29UbycsbWF4SXRlbXMpO31lbHNlIGlmKGN1cnJlbnRTbGlkZTxuZXh0U2xpZGUmJmN1cnJlbnRTbGlkZT09MCYmbmV4dFNsaWRlPT1tYXhJdGVtcy0xKXskKCcuc2xpZGVzaG93LXJpZ2h0IC5zbGlkZXInKS5zbGljaygnc2xpY2tHb1RvJyxtYXhJdGVtcyk7JCgnLnNsaWRlc2hvdy10ZXh0Jykuc2xpY2soJ3NsaWNrR29UbycsLTEpO31lbHNleyQoJy5zbGlkZXNob3ctcmlnaHQgLnNsaWRlcicpLnNsaWNrKCdzbGlja0dvVG8nLG1heEl0ZW1zLTEtbmV4dFNsaWRlKTskKCcuc2xpZGVzaG93LXRleHQnKS5zbGljaygnc2xpY2tHb1RvJyxuZXh0U2xpZGUpO319KS5vbihcIm1vdXNld2hlZWxcIixmdW5jdGlvbihldmVudCl7ZXZlbnQucHJldmVudERlZmF1bHQoKTtpZihldmVudC5kZWx0YVg+MHx8ZXZlbnQuZGVsdGFZPDApeyQodGhpcykuc2xpY2soJ3NsaWNrTmV4dCcpO31lbHNlIGlmKGV2ZW50LmRlbHRhWDwwfHxldmVudC5kZWx0YVk+MCl7JCh0aGlzKS5zbGljaygnc2xpY2tQcmV2Jyk7fTt9KS5vbignbW91c2Vkb3duIHRvdWNoc3RhcnQnLGZ1bmN0aW9uKCl7ZHJhZ2dpbmc9dHJ1ZTt0cmFja2luZz0kKCcuc2xpY2stdHJhY2snLCRzbGlkZXIpLmNzcygndHJhbnNmb3JtJyk7dHJhY2tpbmc9cGFyc2VJbnQodHJhY2tpbmcuc3BsaXQoJywnKVs1XSk7cmlnaHRUcmFja2luZz0kKCcuc2xpZGVzaG93LXJpZ2h0IC5zbGljay10cmFjaycpLmNzcygndHJhbnNmb3JtJyk7cmlnaHRUcmFja2luZz1wYXJzZUludChyaWdodFRyYWNraW5nLnNwbGl0KCcsJylbNV0pO30pLm9uKCdtb3VzZW1vdmUgdG91Y2htb3ZlJyxmdW5jdGlvbigpe2lmKGRyYWdnaW5nKXtuZXdUcmFja2luZz0kKCcuc2xpZGVzaG93LWxlZnQgLnNsaWNrLXRyYWNrJykuY3NzKCd0cmFuc2Zvcm0nKTtuZXdUcmFja2luZz1wYXJzZUludChuZXdUcmFja2luZy5zcGxpdCgnLCcpWzVdKTtkaWZmVHJhY2tpbmc9bmV3VHJhY2tpbmctdHJhY2tpbmc7JCgnLnNsaWRlc2hvdy1yaWdodCAuc2xpY2stdHJhY2snKS5jc3Moeyd0cmFuc2Zvcm0nOidtYXRyaXgoMSwgMCwgMCwgMSwgMCwgJysocmlnaHRUcmFja2luZy1kaWZmVHJhY2tpbmcpKycpJ30pO319KS5vbignbW91c2VsZWF2ZSB0b3VjaGVuZCBtb3VzZXVwJyxmdW5jdGlvbigpe2RyYWdnaW5nPWZhbHNlO30pOyQoJy5zbGlkZXNob3ctcmlnaHQgLnNsaWRlcicpLnNsaWNrKHtzd2lwZTpmYWxzZSx2ZXJ0aWNhbDp0cnVlLGFycm93czpmYWxzZSxpbmZpbml0ZTp0cnVlLHNwZWVkOjk1MCxjc3NFYXNlOidjdWJpYy1iZXppZXIoMC43LCAwLCAwLjMsIDEpJyxpbml0aWFsU2xpZGU6bWF4SXRlbXMtMX0pOyQoJy5zbGlkZXNob3ctdGV4dCcpLnNsaWNrKHtzd2lwZTpmYWxzZSx2ZXJ0aWNhbDp0cnVlLGFycm93czpmYWxzZSxpbmZpbml0ZTp0cnVlLHNwZWVkOjkwMCxjc3NFYXNlOidjdWJpYy1iZXppZXIoMC43LCAwLCAwLjMsIDEpJ30pO30pO2VsZW1lbnRvckZyb250ZW5kLmhvb2tzLmFkZEFjdGlvbignZnJvbnRlbmQvZWxlbWVudF9yZWFkeS9ncmFuZHJlc3RhdXJhbnQtc2xpZGVyLXRyYW5zaXRpb25zLmRlZmF1bHQnLGZ1bmN0aW9uKCRzY29wZSl7alF1ZXJ5KCdib2R5JykuYWRkQ2xhc3MoJ2VsZW1lbnRvci1mdWxsc2NyZWVuJyk7dmFyIG15U3dpcGVyPW5ldyBTd2lwZXIoXCIuc3dpcGVyLWNvbnRhaW5lclwiLHtkaXJlY3Rpb246XCJ2ZXJ0aWNhbFwiLGxvb3A6dHJ1ZSxwYWdpbmF0aW9uOntlbDonLnN3aXBlci1wYWdpbmF0aW9uJyx0eXBlOididWxsZXRzJyxjbGlja2FibGU6dHJ1ZX0sa2V5Ym9hcmQ6e2VuYWJsZWQ6dHJ1ZSxvbmx5SW5WaWV3cG9ydDpmYWxzZSx9LGdyYWJDdXJzb3I6dHJ1ZSxzcGVlZDoxMDAwLHBhZ2luYXRpb25DbGlja2FibGU6dHJ1ZSxwYXJhbGxheDp0cnVlLGF1dG9wbGF5OmZhbHNlLGVmZmVjdDpcInNsaWRlXCIsbW91c2V3aGVlbDp7aW52ZXJ0OmZhbHNlLH0sfSk7fSk7ZWxlbWVudG9yRnJvbnRlbmQuaG9va3MuYWRkQWN0aW9uKCdmcm9udGVuZC9lbGVtZW50X3JlYWR5L2dyYW5kcmVzdGF1cmFudC1zbGlkZXItcHJvcGVydHktY2xpcC5kZWZhdWx0JyxmdW5jdGlvbigkc2NvcGUpe2pRdWVyeShcIi5zbGlkZXItcHJvcGVydHktY2xpcC13cmFwcGVyXCIpLmVhY2goZnVuY3Rpb24oKXt2YXIgc2xpZGVyPWpRdWVyeSh0aGlzKS5maW5kKFwiLnNsaWRlclwiKSxzbGlkZXM9c2xpZGVyLmZpbmQoJ2xpJyksbmF2PXNsaWRlci5maW5kKCduYXYnKTtzbGlkZXMuZXEoMCkuYWRkQ2xhc3MoJ2N1cnJlbnQnKTtuYXYuY2hpbGRyZW4oJ2EnKS5lcSgwKS5hZGRDbGFzcygnY3VycmVudC1kb3QnKTtuYXYub24oJ2NsaWNrJywnYScsZnVuY3Rpb24oZXZlbnQpe2V2ZW50LnByZXZlbnREZWZhdWx0KCk7JCh0aGlzKS5hZGRDbGFzcygnY3VycmVudC1kb3QnKS5zaWJsaW5ncygpLnJlbW92ZUNsYXNzKCdjdXJyZW50LWRvdCcpO3NsaWRlcy5lcSgkKHRoaXMpLmluZGV4KCkpLmFkZENsYXNzKCdjdXJyZW50JykucmVtb3ZlQ2xhc3MoJ3ByZXYnKS5zaWJsaW5ncygpLnJlbW92ZUNsYXNzKCdjdXJyZW50Jyk7c2xpZGVzLmVxKCQodGhpcykuaW5kZXgoKSkucHJldkFsbCgpLmFkZENsYXNzKCdwcmV2Jyk7c2xpZGVzLmVxKCQodGhpcykuaW5kZXgoKSkubmV4dEFsbCgpLnJlbW92ZUNsYXNzKCdwcmV2Jyk7fSk7fSk7fSk7ZWxlbWVudG9yRnJvbnRlbmQuaG9va3MuYWRkQWN0aW9uKCdmcm9udGVuZC9lbGVtZW50X3JlYWR5L2dyYW5kcmVzdGF1cmFudC1zbGlkZXItc2xpY2UuZGVmYXVsdCcsZnVuY3Rpb24oJHNjb3BlKXtqUXVlcnkoXCIuc2xpY2Utc2xpZGUtY29udGFpbmVyXCIpLmVhY2goZnVuY3Rpb24oKXt2YXIgc2xpZGU9alF1ZXJ5KCcuc2xpZGUnKTt2YXIgbmF2UHJldj1qUXVlcnkoJy5qcy1wcmV2Jyk7dmFyIG5hdk5leHQ9alF1ZXJ5KCcuanMtbmV4dCcpO3ZhciBTbGljZVNsaWRlcj17c2V0dGluZ3M6e2RlbHRhOjAsY3VycmVudFNsaWRlSW5kZXg6MCxzY3JvbGxUaHJlc2hvbGQ6NDAsc2xpZGVzOnNsaWRlLG51bVNsaWRlczpzbGlkZS5sZW5ndGgsbmF2UHJldjpuYXZQcmV2LG5hdk5leHQ6bmF2TmV4dCx9LGluaXQ6ZnVuY3Rpb24oKXtzPXRoaXMuc2V0dGluZ3M7dGhpcy5iaW5kRXZlbnRzKCk7fSxiaW5kRXZlbnRzOmZ1bmN0aW9uKCl7cy5uYXZQcmV2Lm9uKHsnY2xpY2snOlNsaWNlU2xpZGVyLnByZXZTbGlkZX0pO3MubmF2TmV4dC5vbih7J2NsaWNrJzpTbGljZVNsaWRlci5uZXh0U2xpZGV9KTskKGRvY3VtZW50KS5rZXl1cChmdW5jdGlvbihlKXtpZigoZS53aGljaD09PTM3KXx8KGUud2hpY2g9PT0zOCkpe1NsaWNlU2xpZGVyLnByZXZTbGlkZSgpO31cbmlmKChlLndoaWNoPT09MzkpfHwoZS53aGljaD09PTQwKSl7U2xpY2VTbGlkZXIubmV4dFNsaWRlKCk7fX0pO30saGFuZGxlU2Nyb2xsOmZ1bmN0aW9uKGUpe2lmKGUub3JpZ2luYWxFdmVudC5kZXRhaWw8MHx8ZS5vcmlnaW5hbEV2ZW50LndoZWVsRGVsdGE+MCl7cy5kZWx0YS0tO2lmKE1hdGguYWJzKHMuZGVsdGEpPj1zLnNjcm9sbFRocmVzaG9sZCl7U2xpY2VTbGlkZXIucHJldlNsaWRlKCk7fX1cbmVsc2V7cy5kZWx0YSsrO2lmKHMuZGVsdGE+PXMuc2Nyb2xsVGhyZXNob2xkKXtTbGljZVNsaWRlci5uZXh0U2xpZGUoKTt9fVxucmV0dXJuIGZhbHNlO30sc2hvd1NsaWRlOmZ1bmN0aW9uKCl7cy5kZWx0YT0wO2lmKCQoJ2JvZHknKS5oYXNDbGFzcygnaXMtc2xpZGluZycpKXtyZXR1cm47fVxucy5zbGlkZXMuZWFjaChmdW5jdGlvbihpLHNsaWRlKXskKHNsaWRlKS50b2dnbGVDbGFzcygnaXMtYWN0aXZlJywoaT09PXMuY3VycmVudFNsaWRlSW5kZXgpKTskKHNsaWRlKS50b2dnbGVDbGFzcygnaXMtcHJldicsKGk9PT1zLmN1cnJlbnRTbGlkZUluZGV4LTEpKTskKHNsaWRlKS50b2dnbGVDbGFzcygnaXMtbmV4dCcsKGk9PT1zLmN1cnJlbnRTbGlkZUluZGV4KzEpKTskKCdib2R5JykuYWRkQ2xhc3MoJ2lzLXNsaWRpbmcnKTtzZXRUaW1lb3V0KGZ1bmN0aW9uKCl7JCgnYm9keScpLnJlbW92ZUNsYXNzKCdpcy1zbGlkaW5nJyk7fSwxMDAwKTt9KTt9LHByZXZTbGlkZTpmdW5jdGlvbigpe2lmKHMuY3VycmVudFNsaWRlSW5kZXg8PTApe3MuY3VycmVudFNsaWRlSW5kZXg9cy5udW1TbGlkZXM7fVxucy5jdXJyZW50U2xpZGVJbmRleC0tO1NsaWNlU2xpZGVyLnNob3dTbGlkZSgpO30sbmV4dFNsaWRlOmZ1bmN0aW9uKCl7cy5jdXJyZW50U2xpZGVJbmRleCsrO2lmKHMuY3VycmVudFNsaWRlSW5kZXg+PXMubnVtU2xpZGVzKXtzLmN1cnJlbnRTbGlkZUluZGV4PTA7fVxuU2xpY2VTbGlkZXIuc2hvd1NsaWRlKCk7fSx9O1NsaWNlU2xpZGVyLmluaXQoKTt9KTt9KTtlbGVtZW50b3JGcm9udGVuZC5ob29rcy5hZGRBY3Rpb24oJ2Zyb250ZW5kL2VsZW1lbnRfcmVhZHkvZ3JhbmRyZXN0YXVyYW50LXNsaWRlci1mbGlwLmRlZmF1bHQnLGZ1bmN0aW9uKCRzY29wZSl7alF1ZXJ5KCdib2R5JykuYWRkQ2xhc3MoJ2VsZW1lbnRvci1mdWxsc2NyZWVuJyk7alF1ZXJ5KCdib2R5JykuYWRkQ2xhc3MoJ2VsZW1lbnRvci1vdmVyZmxvdycpO3ZhciBHYWxsZXJ5PShmdW5jdGlvbigpe3ZhciBzY3JvbGxUaW1lSWQ7dmFyIHBvc0xlZnQ9MDtmdW5jdGlvbiBHYWxsZXJ5KGNvbmZpZyl7dGhpcy5saXN0PSQoY29uZmlnLmxpc3QpO3RoaXMuaXRlbXM9dGhpcy5saXN0LmZpbmQoJ2xpJyk7dGhpcy5pdGVtV2lkdGg9dGhpcy5pdGVtcy5vdXRlcldpZHRoKCk7fTtHYWxsZXJ5LnByb3RvdHlwZT17Y29uc3RydWN0b3I6R2FsbGVyeSxpbml0OmZ1bmN0aW9uKCl7dGhpcy5zZXRHYWxsZXJ5V2lkdGgoKTt0aGlzLmV2ZW50TWFuYWdlcigpO3JldHVybiB0aGlzO30sZXZlbnRNYW5hZ2VyOmZ1bmN0aW9uKCl7dmFyIF90aGlzPXRoaXM7JChcImh0bWwsIGJvZHlcIikub24oJ21vdXNld2hlZWwnLGZ1bmN0aW9uKGV2ZW50KXtjbGVhclRpbWVvdXQoc2Nyb2xsVGltZUlkKTtzY3JvbGxUaW1lSWQ9c2V0VGltZW91dChvblNjcm9sbEV2ZW50SGFuZGxlci5iaW5kKHRoaXMsZXZlbnQsX3RoaXMuaXRlbVdpZHRoKSwwKTt9KTt9LHNldEdhbGxlcnlXaWR0aDpmdW5jdGlvbigpe3RoaXMubGlzdC5jc3MoJ3dpZHRoJyx0aGlzLmdldEdhbGxlcnlXaWR0aCgpKTt0aGlzLmxpc3QuY3NzKCdvdmVyZmxvdycsJ3Njcm9sbCcpO30sZ2V0R2FsbGVyeVdpZHRoOmZ1bmN0aW9uKCl7dmFyIHdpZHRoPTA7dGhpcy5pdGVtcy5lYWNoKGZ1bmN0aW9uKGluZGV4LGl0ZW0pe3dpZHRoKz0kKHRoaXMpLm91dGVyV2lkdGgoKTt9KTtyZXR1cm4gd2lkdGg7fX07ZnVuY3Rpb24gb25TY3JvbGxFdmVudEhhbmRsZXIoZXZlbnQsd2lkdGgpe2lmKGV2ZW50LmRlbHRhWT4wKXt0aGlzLnNjcm9sbExlZnQtPXdpZHRoLzIwO31lbHNle3RoaXMuc2Nyb2xsTGVmdCs9d2lkdGgvMjA7fVxuZXZlbnQucHJldmVudERlZmF1bHQoKTt9O3JldHVybiBHYWxsZXJ5O30pKCk7JChkb2N1bWVudCkucmVhZHkoZnVuY3Rpb24oKXt2YXIgZ2FsbGVyeT1uZXcgR2FsbGVyeSh7bGlzdDonLmZsaXAtc2xpZGUtY29udGFpbmVyIC5jb250YWluZXIgLmdhbGxlcnknfSkuaW5pdCgpO2pRdWVyeSgnLmZsaXAtc2xpZGUtY29udGFpbmVyJykuY3NzKCdvdmVyZmxvdycsJ3Njcm9sbCcpO2pRdWVyeSgnYm9keScpLmNzcygnb3ZlcmZsb3cteCcsJ3Njcm9sbCcpO30pO30pO2VsZW1lbnRvckZyb250ZW5kLmhvb2tzLmFkZEFjdGlvbignZnJvbnRlbmQvZWxlbWVudF9yZWFkeS9ncmFuZHJlc3RhdXJhbnQtc2xpZGVyLXNwbGl0LWNhcm91c2VsLmRlZmF1bHQnLGZ1bmN0aW9uKCRzY29wZSl7alF1ZXJ5KFwiLnNwbGl0LWNhcm91c2VsLXNsaWRlci13cmFwcGVyXCIpLmVhY2goZnVuY3Rpb24oKXt2YXIgJGNhcm91c2VsPWpRdWVyeSh0aGlzKTt2YXIgZnVsbHNjcmVlbj0kY2Fyb3VzZWwuYXR0cignZGF0YS1mdWxsc2NyZWVuJyk7aWYoZnVsbHNjcmVlbiE9MClcbntqUXVlcnkoJ2JvZHknKS5hZGRDbGFzcygnZWxlbWVudG9yLWZ1bGxzY3JlZW4nKTt2YXIgbWVudUhlaWdodD1wYXJzZUludChqUXVlcnkoJyN3cmFwcGVyJykuY3NzKCdwYWRkaW5nVG9wJykpO3ZhciBkb2N1bWVudEhlaWdodD1qUXVlcnkod2luZG93KS5pbm5lckhlaWdodCgpO3ZhciBzbGlkZXJIZWlnaHQ9cGFyc2VJbnQoZG9jdW1lbnRIZWlnaHQtbWVudUhlaWdodCk7JGNhcm91c2VsLmNzcygnaGVpZ2h0JyxzbGlkZXJIZWlnaHQrJ3B4Jyk7alF1ZXJ5KHdpbmRvdykucmVzaXplKGZ1bmN0aW9uKCl7dmFyIG1lbnVIZWlnaHQ9cGFyc2VJbnQoalF1ZXJ5KCcjd3JhcHBlcicpLmNzcygncGFkZGluZ1RvcCcpKTt2YXIgZG9jdW1lbnRIZWlnaHQ9alF1ZXJ5KHdpbmRvdykuaW5uZXJIZWlnaHQoKTt2YXIgc2xpZGVySGVpZ2h0PXBhcnNlSW50KGRvY3VtZW50SGVpZ2h0LW1lbnVIZWlnaHQpOyRjYXJvdXNlbC5jc3MoJ2hlaWdodCcsc2xpZGVySGVpZ2h0KydweCcpO30pO31cbnZhciBhY3RpdmVJbmRleD0wO3ZhciBsaW1pdD0wO3ZhciBkaXNhYmxlZD1mYWxzZTt2YXIgJHN0YWdlPXZvaWQgMDt2YXIgJGNvbnRyb2xzPXZvaWQgMDt2YXIgY2FudmFzPWZhbHNlO3ZhciBTUElOX0ZPUldBUkRfQ0xBU1M9J2pzLXNwaW4tZndkJzt2YXIgU1BJTl9CQUNLV0FSRF9DTEFTUz0nanMtc3Bpbi1id2QnO3ZhciBESVNBQkxFX1RSQU5TSVRJT05TX0NMQVNTPSdqcy10cmFuc2l0aW9ucy1kaXNhYmxlZCc7dmFyIFNQSU5fRFVSPTEwMDA7dmFyIGFwcGVuZENvbnRyb2xzPWZ1bmN0aW9uIGFwcGVuZENvbnRyb2xzKCl7Zm9yKHZhciBpPTA7aTxsaW1pdDtpKyspeyQoJy5jYXJvdXNlbC1jb250cm9sJykuYXBwZW5kKCc8YSBocmVmPVwiI1wiIGRhdGEtaW5kZXg9XCInK2krJ1wiPjwvYT4nKTt9XG52YXIgaGVpZ2h0PSQoJy5jYXJvdXNlbC1jb250cm9sJykuY2hpbGRyZW4oKS5sYXN0KCkub3V0ZXJIZWlnaHQoKTskKCcuY2Fyb3VzZWwtY29udHJvbCcpLmNzcygnaGVpZ2h0JywzMCtsaW1pdCpoZWlnaHQpOyRjb250cm9scz0kKCcuY2Fyb3VzZWwtY29udHJvbCcpLmNoaWxkcmVuKCk7JGNvbnRyb2xzLmVxKGFjdGl2ZUluZGV4KS5hZGRDbGFzcygnYWN0aXZlJyk7fTt2YXIgc2V0SW5kZXhlcz1mdW5jdGlvbiBzZXRJbmRleGVzKCl7JCgnLnNwaW5uZXInKS5jaGlsZHJlbigpLmVhY2goZnVuY3Rpb24oaSxlbCl7JChlbCkuYXR0cignZGF0YS1pbmRleCcsaSk7bGltaXQrKzt9KTt9O3ZhciBkdXBsaWNhdGVTcGlubmVyPWZ1bmN0aW9uIGR1cGxpY2F0ZVNwaW5uZXIoKXt2YXIgJGVsPSQoJy5zcGlubmVyJykucGFyZW50KCk7dmFyIGh0bWw9JCgnLnNwaW5uZXInKS5wYXJlbnQoKS5odG1sKCk7JGVsLmFwcGVuZChodG1sKTskKCcuc3Bpbm5lcicpLmxhc3QoKS5hZGRDbGFzcygnc3Bpbm5lci0tcmlnaHQnKTskKCcuc3Bpbm5lci0tcmlnaHQnKS5yZW1vdmVDbGFzcygnc3Bpbm5lci0tbGVmdCcpO307dmFyIHBhaW50RmFjZXM9ZnVuY3Rpb24gcGFpbnRGYWNlcygpeyQoJy5zcGlubmVyLWZhY2UnKS5lYWNoKGZ1bmN0aW9uKGksZWwpe3ZhciAkZWw9JChlbCk7dmFyIGNvbG9yPSQoZWwpLmF0dHIoJ2RhdGEtYmcnKTskZWwuY2hpbGRyZW4oKS5jc3MoJ2JhY2tncm91bmRJbWFnZScsJ3VybCgnK2dldEJhc2U2NFBpeGVsQnlDb2xvcihjb2xvcikrJyknKTt9KTt9O3ZhciBnZXRCYXNlNjRQaXhlbEJ5Q29sb3I9ZnVuY3Rpb24gZ2V0QmFzZTY0UGl4ZWxCeUNvbG9yKGhleCl7aWYoIWNhbnZhcyl7Y2FudmFzPWRvY3VtZW50LmNyZWF0ZUVsZW1lbnQoJ2NhbnZhcycpO2NhbnZhcy5oZWlnaHQ9MTtjYW52YXMud2lkdGg9MTt9XG5pZihjYW52YXMuZ2V0Q29udGV4dCl7dmFyIGN0eD1jYW52YXMuZ2V0Q29udGV4dCgnMmQnKTtjdHguZmlsbFN0eWxlPWhleDtjdHguZmlsbFJlY3QoMCwwLDEsMSk7cmV0dXJuIGNhbnZhcy50b0RhdGFVUkwoKTt9XG5yZXR1cm4gZmFsc2U7fTt2YXIgcHJlcGFyZURvbT1mdW5jdGlvbiBwcmVwYXJlRG9tKCl7c2V0SW5kZXhlcygpO3BhaW50RmFjZXMoKTtkdXBsaWNhdGVTcGlubmVyKCk7YXBwZW5kQ29udHJvbHMoKTt9O3ZhciBzcGluPWZ1bmN0aW9uIHNwaW4oKXt2YXIgaW5jPWFyZ3VtZW50cy5sZW5ndGg+MCYmYXJndW1lbnRzWzBdIT09dW5kZWZpbmVkP2FyZ3VtZW50c1swXToxO2lmKGRpc2FibGVkKXJldHVybjtpZighaW5jKXJldHVybjthY3RpdmVJbmRleCs9aW5jO2Rpc2FibGVkPXRydWU7aWYoYWN0aXZlSW5kZXg+PWxpbWl0KXthY3RpdmVJbmRleD0wO31cbmlmKGFjdGl2ZUluZGV4PDApe2FjdGl2ZUluZGV4PWxpbWl0LTE7fVxudmFyICRhY3RpdmVFbHM9JCgnLnNwaW5uZXItZmFjZS5qcy1hY3RpdmUnKTt2YXIgJG5leHRFbHM9JCgnLnNwaW5uZXItZmFjZVtkYXRhLWluZGV4PScrYWN0aXZlSW5kZXgrJ10nKTskbmV4dEVscy5hZGRDbGFzcygnanMtbmV4dCcpO2lmKGluYz4wKXskc3RhZ2UuYWRkQ2xhc3MoU1BJTl9GT1JXQVJEX0NMQVNTKTt9ZWxzZXskc3RhZ2UuYWRkQ2xhc3MoU1BJTl9CQUNLV0FSRF9DTEFTUyk7fVxuJGNvbnRyb2xzLnJlbW92ZUNsYXNzKCdhY3RpdmUnKTskY29udHJvbHMuZXEoYWN0aXZlSW5kZXgpLmFkZENsYXNzKCdhY3RpdmUnKTtzZXRUaW1lb3V0KGZ1bmN0aW9uKCl7c3BpbkNhbGxiYWNrKGluYyk7fSxTUElOX0RVUixpbmMpO307dmFyIHNwaW5DYWxsYmFjaz1mdW5jdGlvbiBzcGluQ2FsbGJhY2soaW5jKXskKCcuanMtYWN0aXZlJykucmVtb3ZlQ2xhc3MoJ2pzLWFjdGl2ZScpOyQoJy5qcy1uZXh0JykucmVtb3ZlQ2xhc3MoJ2pzLW5leHQnKS5hZGRDbGFzcygnanMtYWN0aXZlJyk7JHN0YWdlLmFkZENsYXNzKERJU0FCTEVfVFJBTlNJVElPTlNfQ0xBU1MpLnJlbW92ZUNsYXNzKFNQSU5fRk9SV0FSRF9DTEFTUykucmVtb3ZlQ2xhc3MoU1BJTl9CQUNLV0FSRF9DTEFTUyk7JCgnLmpzLWFjdGl2ZScpLmVhY2goZnVuY3Rpb24oaSxlbCl7dmFyICRlbD0kKGVsKTskZWwucHJlcGVuZFRvKCRlbC5wYXJlbnQoKSk7fSk7c2V0VGltZW91dChmdW5jdGlvbigpeyRzdGFnZS5yZW1vdmVDbGFzcyhESVNBQkxFX1RSQU5TSVRJT05TX0NMQVNTKTtkaXNhYmxlZD1mYWxzZTt9LDEwMCk7fTt2YXIgYXR0YWNoTGlzdGVuZXJzPWZ1bmN0aW9uIGF0dGFjaExpc3RlbmVycygpe2RvY3VtZW50Lm9ua2V5dXA9ZnVuY3Rpb24oZSl7c3dpdGNoKGUua2V5Q29kZSl7Y2FzZSAzODpzcGluKC0xKTticmVhaztjYXNlIDQwOnNwaW4oMSk7YnJlYWs7fX07JGNhcm91c2VsLmJpbmQoJ0RPTU1vdXNlU2Nyb2xsJyxmdW5jdGlvbihlKXtpZihlLm9yaWdpbmFsRXZlbnQuZGV0YWlsPjApe3NwaW4oMSk7fWVsc2V7c3BpbigtMSk7fVxucmV0dXJuIGZhbHNlO30pOyRjYXJvdXNlbC5iaW5kKCdtb3VzZXdoZWVsJyxmdW5jdGlvbihlKXtpZihlLm9yaWdpbmFsRXZlbnQud2hlZWxEZWx0YTwwKXtzcGluKDEpO31lbHNle3NwaW4oLTEpO31cbnJldHVybiBmYWxzZTt9KTtqUXVlcnkoJ2JvZHknKS5vbigndG91Y2htb3ZlJyxmdW5jdGlvbihlKXtlLnByZXZlbnREZWZhdWx0KCk7ZS5zdG9wUHJvcGFnYXRpb24oKTtyZXR1cm4gZmFsc2U7fSk7dmFyIHRzOyRjYXJvdXNlbC5iaW5kKCd0b3VjaHN0YXJ0JyxmdW5jdGlvbihlKXt0cz1lLm9yaWdpbmFsRXZlbnQudG91Y2hlc1swXS5jbGllbnRZO30pOyRjYXJvdXNlbC5iaW5kKCd0b3VjaGVuZCcsZnVuY3Rpb24oZSl7dmFyIHRlPWUub3JpZ2luYWxFdmVudC5jaGFuZ2VkVG91Y2hlc1swXS5jbGllbnRZO2lmKHRzPnRlKzUpe3NwaW4oMSk7fWVsc2UgaWYodHM8dGUtNSl7c3BpbigtMSk7fX0pOyRjb250cm9scy5vbignY2xpY2snLGZ1bmN0aW9uKGUpe2UucHJldmVudERlZmF1bHQoKTtpZihkaXNhYmxlZClyZXR1cm47dmFyICRlbD0kKGUudGFyZ2V0KTt2YXIgdG9JbmRleD1wYXJzZUludCgkZWwuYXR0cignZGF0YS1pbmRleCcpLDEwKTtzcGluKHRvSW5kZXgtYWN0aXZlSW5kZXgpO30pO307dmFyIGFzc2lnbkVscz1mdW5jdGlvbiBhc3NpZ25FbHMoKXskc3RhZ2U9JCgnLmNhcm91c2VsLXN0YWdlJyk7fTt2YXIgaW5pdD1mdW5jdGlvbiBpbml0KCl7YXNzaWduRWxzKCk7cHJlcGFyZURvbSgpO2F0dGFjaExpc3RlbmVycygpO307JChmdW5jdGlvbigpe2luaXQoKTt9KTt9KTt9KTtlbGVtZW50b3JGcm9udGVuZC5ob29rcy5hZGRBY3Rpb24oJ2Zyb250ZW5kL2VsZW1lbnRfcmVhZHkvZ3JhbmRyZXN0YXVyYW50LWhvcml6b250YWwtdGltZWxpbmUuZGVmYXVsdCcsZnVuY3Rpb24oJHNjb3BlKXt2YXIgdGltZWxpbmVzPWpRdWVyeSgnLmNkLWhvcml6b250YWwtdGltZWxpbmUnKTt2YXIgZXZlbnRzTWluRGlzdGFuY2U9dGltZWxpbmVzLmF0dHIoJ2RhdGEtc3BhY2luZycpO2lmKGV2ZW50c01pbkRpc3RhbmNlPT0nJylcbntldmVudHNNaW5EaXN0YW5jZT02MDt9XG4odGltZWxpbmVzLmxlbmd0aD4wKSYmaW5pdFRpbWVsaW5lKHRpbWVsaW5lcyk7ZnVuY3Rpb24gaW5pdFRpbWVsaW5lKHRpbWVsaW5lcyl7dGltZWxpbmVzLmVhY2goZnVuY3Rpb24oKXt2YXIgdGltZWxpbmU9alF1ZXJ5KHRoaXMpLHRpbWVsaW5lQ29tcG9uZW50cz17fTt0aW1lbGluZUNvbXBvbmVudHNbJ3RpbWVsaW5lV3JhcHBlciddPXRpbWVsaW5lLmZpbmQoJy5ldmVudHMtd3JhcHBlcicpO3RpbWVsaW5lQ29tcG9uZW50c1snZXZlbnRzV3JhcHBlciddPXRpbWVsaW5lQ29tcG9uZW50c1sndGltZWxpbmVXcmFwcGVyJ10uY2hpbGRyZW4oJy5ldmVudHMnKTt0aW1lbGluZUNvbXBvbmVudHNbJ2ZpbGxpbmdMaW5lJ109dGltZWxpbmVDb21wb25lbnRzWydldmVudHNXcmFwcGVyJ10uY2hpbGRyZW4oJy5maWxsaW5nLWxpbmUnKTt0aW1lbGluZUNvbXBvbmVudHNbJ3RpbWVsaW5lRXZlbnRzJ109dGltZWxpbmVDb21wb25lbnRzWydldmVudHNXcmFwcGVyJ10uZmluZCgnYScpO3RpbWVsaW5lQ29tcG9uZW50c1sndGltZWxpbmVEYXRlcyddPXBhcnNlRGF0ZSh0aW1lbGluZUNvbXBvbmVudHNbJ3RpbWVsaW5lRXZlbnRzJ10pO3RpbWVsaW5lQ29tcG9uZW50c1snZXZlbnRzTWluTGFwc2UnXT1taW5MYXBzZSh0aW1lbGluZUNvbXBvbmVudHNbJ3RpbWVsaW5lRGF0ZXMnXSk7dGltZWxpbmVDb21wb25lbnRzWyd0aW1lbGluZU5hdmlnYXRpb24nXT10aW1lbGluZS5maW5kKCcuY2QtdGltZWxpbmUtbmF2aWdhdGlvbicpO3RpbWVsaW5lQ29tcG9uZW50c1snZXZlbnRzQ29udGVudCddPXRpbWVsaW5lLmNoaWxkcmVuKCcuZXZlbnRzLWNvbnRlbnQnKTtzZXREYXRlUG9zaXRpb24odGltZWxpbmVDb21wb25lbnRzLGV2ZW50c01pbkRpc3RhbmNlKTt2YXIgdGltZWxpbmVUb3RXaWR0aD1zZXRUaW1lbGluZVdpZHRoKHRpbWVsaW5lQ29tcG9uZW50cyxldmVudHNNaW5EaXN0YW5jZSk7dGltZWxpbmUuYWRkQ2xhc3MoJ2xvYWRlZCcpO3RpbWVsaW5lQ29tcG9uZW50c1sndGltZWxpbmVOYXZpZ2F0aW9uJ10ub24oJ2NsaWNrJywnLm5leHQnLGZ1bmN0aW9uKGV2ZW50KXtldmVudC5wcmV2ZW50RGVmYXVsdCgpO3VwZGF0ZVNsaWRlKHRpbWVsaW5lQ29tcG9uZW50cyx0aW1lbGluZVRvdFdpZHRoLCduZXh0Jyk7fSk7dGltZWxpbmVDb21wb25lbnRzWyd0aW1lbGluZU5hdmlnYXRpb24nXS5vbignY2xpY2snLCcucHJldicsZnVuY3Rpb24oZXZlbnQpe2V2ZW50LnByZXZlbnREZWZhdWx0KCk7dXBkYXRlU2xpZGUodGltZWxpbmVDb21wb25lbnRzLHRpbWVsaW5lVG90V2lkdGgsJ3ByZXYnKTt9KTt0aW1lbGluZUNvbXBvbmVudHNbJ2V2ZW50c1dyYXBwZXInXS5vbignY2xpY2snLCdhJyxmdW5jdGlvbihldmVudCl7ZXZlbnQucHJldmVudERlZmF1bHQoKTt0aW1lbGluZUNvbXBvbmVudHNbJ3RpbWVsaW5lRXZlbnRzJ10ucmVtb3ZlQ2xhc3MoJ3NlbGVjdGVkJyk7alF1ZXJ5KHRoaXMpLmFkZENsYXNzKCdzZWxlY3RlZCcpO3VwZGF0ZU9sZGVyRXZlbnRzKGpRdWVyeSh0aGlzKSk7dXBkYXRlRmlsbGluZyhqUXVlcnkodGhpcyksdGltZWxpbmVDb21wb25lbnRzWydmaWxsaW5nTGluZSddLHRpbWVsaW5lVG90V2lkdGgpO3VwZGF0ZVZpc2libGVDb250ZW50KGpRdWVyeSh0aGlzKSx0aW1lbGluZUNvbXBvbmVudHNbJ2V2ZW50c0NvbnRlbnQnXSk7fSk7dGltZWxpbmVDb21wb25lbnRzWydldmVudHNDb250ZW50J10ub24oJ3N3aXBlbGVmdCcsZnVuY3Rpb24oKXt2YXIgbXE9Y2hlY2tNUSgpOyhtcT09J21vYmlsZScpJiZzaG93TmV3Q29udGVudCh0aW1lbGluZUNvbXBvbmVudHMsdGltZWxpbmVUb3RXaWR0aCwnbmV4dCcpO30pO3RpbWVsaW5lQ29tcG9uZW50c1snZXZlbnRzQ29udGVudCddLm9uKCdzd2lwZXJpZ2h0JyxmdW5jdGlvbigpe3ZhciBtcT1jaGVja01RKCk7KG1xPT0nbW9iaWxlJykmJnNob3dOZXdDb250ZW50KHRpbWVsaW5lQ29tcG9uZW50cyx0aW1lbGluZVRvdFdpZHRoLCdwcmV2Jyk7fSk7alF1ZXJ5KGRvY3VtZW50KS5rZXl1cChmdW5jdGlvbihldmVudCl7aWYoZXZlbnQud2hpY2g9PSczNycmJmVsZW1lbnRJblZpZXdwb3J0KHRpbWVsaW5lLmdldCgwKSkpe3Nob3dOZXdDb250ZW50KHRpbWVsaW5lQ29tcG9uZW50cyx0aW1lbGluZVRvdFdpZHRoLCdwcmV2Jyk7fWVsc2UgaWYoZXZlbnQud2hpY2g9PSczOScmJmVsZW1lbnRJblZpZXdwb3J0KHRpbWVsaW5lLmdldCgwKSkpe3Nob3dOZXdDb250ZW50KHRpbWVsaW5lQ29tcG9uZW50cyx0aW1lbGluZVRvdFdpZHRoLCduZXh0Jyk7fX0pO30pO31cbmZ1bmN0aW9uIHVwZGF0ZVNsaWRlKHRpbWVsaW5lQ29tcG9uZW50cyx0aW1lbGluZVRvdFdpZHRoLHN0cmluZyl7dmFyIHRyYW5zbGF0ZVZhbHVlPWdldFRyYW5zbGF0ZVZhbHVlKHRpbWVsaW5lQ29tcG9uZW50c1snZXZlbnRzV3JhcHBlciddKSx3cmFwcGVyV2lkdGg9TnVtYmVyKHRpbWVsaW5lQ29tcG9uZW50c1sndGltZWxpbmVXcmFwcGVyJ10uY3NzKCd3aWR0aCcpLnJlcGxhY2UoJ3B4JywnJykpOyhzdHJpbmc9PSduZXh0Jyk/dHJhbnNsYXRlVGltZWxpbmUodGltZWxpbmVDb21wb25lbnRzLHRyYW5zbGF0ZVZhbHVlLXdyYXBwZXJXaWR0aCtldmVudHNNaW5EaXN0YW5jZSx3cmFwcGVyV2lkdGgtdGltZWxpbmVUb3RXaWR0aCk6dHJhbnNsYXRlVGltZWxpbmUodGltZWxpbmVDb21wb25lbnRzLHRyYW5zbGF0ZVZhbHVlK3dyYXBwZXJXaWR0aC1ldmVudHNNaW5EaXN0YW5jZSk7fVxuZnVuY3Rpb24gc2hvd05ld0NvbnRlbnQodGltZWxpbmVDb21wb25lbnRzLHRpbWVsaW5lVG90V2lkdGgsc3RyaW5nKXt2YXIgdmlzaWJsZUNvbnRlbnQ9dGltZWxpbmVDb21wb25lbnRzWydldmVudHNDb250ZW50J10uZmluZCgnLnNlbGVjdGVkJyksbmV3Q29udGVudD0oc3RyaW5nPT0nbmV4dCcpP3Zpc2libGVDb250ZW50Lm5leHQoKTp2aXNpYmxlQ29udGVudC5wcmV2KCk7aWYobmV3Q29udGVudC5sZW5ndGg+MCl7dmFyIHNlbGVjdGVkRGF0ZT10aW1lbGluZUNvbXBvbmVudHNbJ2V2ZW50c1dyYXBwZXInXS5maW5kKCcuc2VsZWN0ZWQnKSxuZXdFdmVudD0oc3RyaW5nPT0nbmV4dCcpP3NlbGVjdGVkRGF0ZS5wYXJlbnQoJ2xpJykubmV4dCgnbGknKS5jaGlsZHJlbignYScpOnNlbGVjdGVkRGF0ZS5wYXJlbnQoJ2xpJykucHJldignbGknKS5jaGlsZHJlbignYScpO3VwZGF0ZUZpbGxpbmcobmV3RXZlbnQsdGltZWxpbmVDb21wb25lbnRzWydmaWxsaW5nTGluZSddLHRpbWVsaW5lVG90V2lkdGgpO3VwZGF0ZVZpc2libGVDb250ZW50KG5ld0V2ZW50LHRpbWVsaW5lQ29tcG9uZW50c1snZXZlbnRzQ29udGVudCddKTtuZXdFdmVudC5hZGRDbGFzcygnc2VsZWN0ZWQnKTtzZWxlY3RlZERhdGUucmVtb3ZlQ2xhc3MoJ3NlbGVjdGVkJyk7dXBkYXRlT2xkZXJFdmVudHMobmV3RXZlbnQpO3VwZGF0ZVRpbWVsaW5lUG9zaXRpb24oc3RyaW5nLG5ld0V2ZW50LHRpbWVsaW5lQ29tcG9uZW50cyx0aW1lbGluZVRvdFdpZHRoKTt9fVxuZnVuY3Rpb24gdXBkYXRlVGltZWxpbmVQb3NpdGlvbihzdHJpbmcsZXZlbnQsdGltZWxpbmVDb21wb25lbnRzLHRpbWVsaW5lVG90V2lkdGgpe3ZhciBldmVudFN0eWxlPXdpbmRvdy5nZXRDb21wdXRlZFN0eWxlKGV2ZW50LmdldCgwKSxudWxsKSxldmVudExlZnQ9TnVtYmVyKGV2ZW50U3R5bGUuZ2V0UHJvcGVydHlWYWx1ZShcImxlZnRcIikucmVwbGFjZSgncHgnLCcnKSksdGltZWxpbmVXaWR0aD1OdW1iZXIodGltZWxpbmVDb21wb25lbnRzWyd0aW1lbGluZVdyYXBwZXInXS5jc3MoJ3dpZHRoJykucmVwbGFjZSgncHgnLCcnKSksdGltZWxpbmVUb3RXaWR0aD1OdW1iZXIodGltZWxpbmVDb21wb25lbnRzWydldmVudHNXcmFwcGVyJ10uY3NzKCd3aWR0aCcpLnJlcGxhY2UoJ3B4JywnJykpO3ZhciB0aW1lbGluZVRyYW5zbGF0ZT1nZXRUcmFuc2xhdGVWYWx1ZSh0aW1lbGluZUNvbXBvbmVudHNbJ2V2ZW50c1dyYXBwZXInXSk7aWYoKHN0cmluZz09J25leHQnJiZldmVudExlZnQ+dGltZWxpbmVXaWR0aC10aW1lbGluZVRyYW5zbGF0ZSl8fChzdHJpbmc9PSdwcmV2JyYmZXZlbnRMZWZ0PC10aW1lbGluZVRyYW5zbGF0ZSkpe3RyYW5zbGF0ZVRpbWVsaW5lKHRpbWVsaW5lQ29tcG9uZW50cywtZXZlbnRMZWZ0K3RpbWVsaW5lV2lkdGgvMix0aW1lbGluZVdpZHRoLXRpbWVsaW5lVG90V2lkdGgpO319XG5mdW5jdGlvbiB0cmFuc2xhdGVUaW1lbGluZSh0aW1lbGluZUNvbXBvbmVudHMsdmFsdWUsdG90V2lkdGgpe3ZhciBldmVudHNXcmFwcGVyPXRpbWVsaW5lQ29tcG9uZW50c1snZXZlbnRzV3JhcHBlciddLmdldCgwKTt2YWx1ZT0odmFsdWU+MCk/MDp2YWx1ZTt2YWx1ZT0oISh0eXBlb2YgdG90V2lkdGg9PT0ndW5kZWZpbmVkJykmJnZhbHVlPHRvdFdpZHRoKT90b3RXaWR0aDp2YWx1ZTtzZXRUcmFuc2Zvcm1WYWx1ZShldmVudHNXcmFwcGVyLCd0cmFuc2xhdGVYJyx2YWx1ZSsncHgnKTsodmFsdWU9PTApP3RpbWVsaW5lQ29tcG9uZW50c1sndGltZWxpbmVOYXZpZ2F0aW9uJ10uZmluZCgnLnByZXYnKS5hZGRDbGFzcygnaW5hY3RpdmUnKTp0aW1lbGluZUNvbXBvbmVudHNbJ3RpbWVsaW5lTmF2aWdhdGlvbiddLmZpbmQoJy5wcmV2JykucmVtb3ZlQ2xhc3MoJ2luYWN0aXZlJyk7KHZhbHVlPT10b3RXaWR0aCk/dGltZWxpbmVDb21wb25lbnRzWyd0aW1lbGluZU5hdmlnYXRpb24nXS5maW5kKCcubmV4dCcpLmFkZENsYXNzKCdpbmFjdGl2ZScpOnRpbWVsaW5lQ29tcG9uZW50c1sndGltZWxpbmVOYXZpZ2F0aW9uJ10uZmluZCgnLm5leHQnKS5yZW1vdmVDbGFzcygnaW5hY3RpdmUnKTt9XG5mdW5jdGlvbiB1cGRhdGVGaWxsaW5nKHNlbGVjdGVkRXZlbnQsZmlsbGluZyx0b3RXaWR0aCl7dmFyIGV2ZW50U3R5bGU9d2luZG93LmdldENvbXB1dGVkU3R5bGUoc2VsZWN0ZWRFdmVudC5nZXQoMCksbnVsbCksZXZlbnRMZWZ0PWV2ZW50U3R5bGUuZ2V0UHJvcGVydHlWYWx1ZShcImxlZnRcIiksZXZlbnRXaWR0aD1ldmVudFN0eWxlLmdldFByb3BlcnR5VmFsdWUoXCJ3aWR0aFwiKTtldmVudExlZnQ9TnVtYmVyKGV2ZW50TGVmdC5yZXBsYWNlKCdweCcsJycpKStOdW1iZXIoZXZlbnRXaWR0aC5yZXBsYWNlKCdweCcsJycpKS8yO3ZhciBzY2FsZVZhbHVlPWV2ZW50TGVmdC90b3RXaWR0aDtzZXRUcmFuc2Zvcm1WYWx1ZShmaWxsaW5nLmdldCgwKSwnc2NhbGVYJyxzY2FsZVZhbHVlKTt9XG5mdW5jdGlvbiBzZXREYXRlUG9zaXRpb24odGltZWxpbmVDb21wb25lbnRzLG1pbil7Zm9yKGk9MDtpPHRpbWVsaW5lQ29tcG9uZW50c1sndGltZWxpbmVEYXRlcyddLmxlbmd0aDtpKyspe3ZhciBkaXN0YW5jZT1kYXlkaWZmKHRpbWVsaW5lQ29tcG9uZW50c1sndGltZWxpbmVEYXRlcyddWzBdLHRpbWVsaW5lQ29tcG9uZW50c1sndGltZWxpbmVEYXRlcyddW2ldKSxkaXN0YW5jZU5vcm09TWF0aC5yb3VuZChkaXN0YW5jZS90aW1lbGluZUNvbXBvbmVudHNbJ2V2ZW50c01pbkxhcHNlJ10pKzI7dGltZWxpbmVDb21wb25lbnRzWyd0aW1lbGluZUV2ZW50cyddLmVxKGkpLmNzcygnbGVmdCcsZGlzdGFuY2VOb3JtKm1pbisncHgnKTt9fVxuZnVuY3Rpb24gc2V0VGltZWxpbmVXaWR0aCh0aW1lbGluZUNvbXBvbmVudHMsd2lkdGgpe3ZhciB0aW1lU3Bhbj1kYXlkaWZmKHRpbWVsaW5lQ29tcG9uZW50c1sndGltZWxpbmVEYXRlcyddWzBdLHRpbWVsaW5lQ29tcG9uZW50c1sndGltZWxpbmVEYXRlcyddW3RpbWVsaW5lQ29tcG9uZW50c1sndGltZWxpbmVEYXRlcyddLmxlbmd0aC0xXSksdGltZVNwYW5Ob3JtPXRpbWVTcGFuL3RpbWVsaW5lQ29tcG9uZW50c1snZXZlbnRzTWluTGFwc2UnXSx0aW1lU3Bhbk5vcm09TWF0aC5yb3VuZCh0aW1lU3Bhbk5vcm0pKzQsdG90YWxXaWR0aD10aW1lU3Bhbk5vcm0qd2lkdGg7dGltZWxpbmVDb21wb25lbnRzWydldmVudHNXcmFwcGVyJ10uY3NzKCd3aWR0aCcsdG90YWxXaWR0aCsncHgnKTt1cGRhdGVGaWxsaW5nKHRpbWVsaW5lQ29tcG9uZW50c1sndGltZWxpbmVFdmVudHMnXS5lcSgwKSx0aW1lbGluZUNvbXBvbmVudHNbJ2ZpbGxpbmdMaW5lJ10sdG90YWxXaWR0aCk7cmV0dXJuIHRvdGFsV2lkdGg7fVxuZnVuY3Rpb24gdXBkYXRlVmlzaWJsZUNvbnRlbnQoZXZlbnQsZXZlbnRzQ29udGVudCl7dmFyIGV2ZW50RGF0ZT1ldmVudC5kYXRhKCdkYXRlJyksdmlzaWJsZUNvbnRlbnQ9ZXZlbnRzQ29udGVudC5maW5kKCcuc2VsZWN0ZWQnKSxzZWxlY3RlZENvbnRlbnQ9ZXZlbnRzQ29udGVudC5maW5kKCdbZGF0YS1kYXRlPVwiJytldmVudERhdGUrJ1wiXScpLHNlbGVjdGVkQ29udGVudEhlaWdodD1zZWxlY3RlZENvbnRlbnQuaGVpZ2h0KCk7aWYoc2VsZWN0ZWRDb250ZW50LmluZGV4KCk+dmlzaWJsZUNvbnRlbnQuaW5kZXgoKSl7dmFyIGNsYXNzRW5ldGVyaW5nPSdzZWxlY3RlZCBlbnRlci1yaWdodCcsY2xhc3NMZWF2aW5nPSdsZWF2ZS1sZWZ0Jzt9ZWxzZXt2YXIgY2xhc3NFbmV0ZXJpbmc9J3NlbGVjdGVkIGVudGVyLWxlZnQnLGNsYXNzTGVhdmluZz0nbGVhdmUtcmlnaHQnO31cbnNlbGVjdGVkQ29udGVudC5hdHRyKCdjbGFzcycsY2xhc3NFbmV0ZXJpbmcpO3Zpc2libGVDb250ZW50LmF0dHIoJ2NsYXNzJyxjbGFzc0xlYXZpbmcpLm9uZSgnd2Via2l0QW5pbWF0aW9uRW5kIG9hbmltYXRpb25lbmQgbXNBbmltYXRpb25FbmQgYW5pbWF0aW9uZW5kJyxmdW5jdGlvbigpe3Zpc2libGVDb250ZW50LnJlbW92ZUNsYXNzKCdsZWF2ZS1yaWdodCBsZWF2ZS1sZWZ0Jyk7c2VsZWN0ZWRDb250ZW50LnJlbW92ZUNsYXNzKCdlbnRlci1sZWZ0IGVudGVyLXJpZ2h0Jyk7fSk7ZXZlbnRzQ29udGVudC5jc3MoJ2hlaWdodCcsc2VsZWN0ZWRDb250ZW50SGVpZ2h0KydweCcpO31cbmZ1bmN0aW9uIHVwZGF0ZU9sZGVyRXZlbnRzKGV2ZW50KXtldmVudC5wYXJlbnQoJ2xpJykucHJldkFsbCgnbGknKS5jaGlsZHJlbignYScpLmFkZENsYXNzKCdvbGRlci1ldmVudCcpLmVuZCgpLmVuZCgpLm5leHRBbGwoJ2xpJykuY2hpbGRyZW4oJ2EnKS5yZW1vdmVDbGFzcygnb2xkZXItZXZlbnQnKTt9XG5mdW5jdGlvbiBnZXRUcmFuc2xhdGVWYWx1ZSh0aW1lbGluZSl7dmFyIHRpbWVsaW5lU3R5bGU9d2luZG93LmdldENvbXB1dGVkU3R5bGUodGltZWxpbmUuZ2V0KDApLG51bGwpLHRpbWVsaW5lVHJhbnNsYXRlPXRpbWVsaW5lU3R5bGUuZ2V0UHJvcGVydHlWYWx1ZShcIi13ZWJraXQtdHJhbnNmb3JtXCIpfHx0aW1lbGluZVN0eWxlLmdldFByb3BlcnR5VmFsdWUoXCItbW96LXRyYW5zZm9ybVwiKXx8dGltZWxpbmVTdHlsZS5nZXRQcm9wZXJ0eVZhbHVlKFwiLW1zLXRyYW5zZm9ybVwiKXx8dGltZWxpbmVTdHlsZS5nZXRQcm9wZXJ0eVZhbHVlKFwiLW8tdHJhbnNmb3JtXCIpfHx0aW1lbGluZVN0eWxlLmdldFByb3BlcnR5VmFsdWUoXCJ0cmFuc2Zvcm1cIik7aWYodGltZWxpbmVUcmFuc2xhdGUuaW5kZXhPZignKCcpPj0wKXt2YXIgdGltZWxpbmVUcmFuc2xhdGU9dGltZWxpbmVUcmFuc2xhdGUuc3BsaXQoJygnKVsxXTt0aW1lbGluZVRyYW5zbGF0ZT10aW1lbGluZVRyYW5zbGF0ZS5zcGxpdCgnKScpWzBdO3RpbWVsaW5lVHJhbnNsYXRlPXRpbWVsaW5lVHJhbnNsYXRlLnNwbGl0KCcsJyk7dmFyIHRyYW5zbGF0ZVZhbHVlPXRpbWVsaW5lVHJhbnNsYXRlWzRdO31lbHNle3ZhciB0cmFuc2xhdGVWYWx1ZT0wO31cbnJldHVybiBOdW1iZXIodHJhbnNsYXRlVmFsdWUpO31cbmZ1bmN0aW9uIHNldFRyYW5zZm9ybVZhbHVlKGVsZW1lbnQscHJvcGVydHksdmFsdWUpe2VsZW1lbnQuc3R5bGVbXCItd2Via2l0LXRyYW5zZm9ybVwiXT1wcm9wZXJ0eStcIihcIit2YWx1ZStcIilcIjtlbGVtZW50LnN0eWxlW1wiLW1vei10cmFuc2Zvcm1cIl09cHJvcGVydHkrXCIoXCIrdmFsdWUrXCIpXCI7ZWxlbWVudC5zdHlsZVtcIi1tcy10cmFuc2Zvcm1cIl09cHJvcGVydHkrXCIoXCIrdmFsdWUrXCIpXCI7ZWxlbWVudC5zdHlsZVtcIi1vLXRyYW5zZm9ybVwiXT1wcm9wZXJ0eStcIihcIit2YWx1ZStcIilcIjtlbGVtZW50LnN0eWxlW1widHJhbnNmb3JtXCJdPXByb3BlcnR5K1wiKFwiK3ZhbHVlK1wiKVwiO31cbmZ1bmN0aW9uIHBhcnNlRGF0ZShldmVudHMpe3ZhciBkYXRlQXJyYXlzPVtdO2V2ZW50cy5lYWNoKGZ1bmN0aW9uKCl7dmFyIGRhdGVDb21wPWpRdWVyeSh0aGlzKS5kYXRhKCdkYXRlJykuc3BsaXQoJy8nKSxuZXdEYXRlPW5ldyBEYXRlKGRhdGVDb21wWzJdLGRhdGVDb21wWzFdLTEsZGF0ZUNvbXBbMF0pO2RhdGVBcnJheXMucHVzaChuZXdEYXRlKTt9KTtyZXR1cm4gZGF0ZUFycmF5czt9XG5mdW5jdGlvbiBwYXJzZURhdGUyKGV2ZW50cyl7dmFyIGRhdGVBcnJheXM9W107ZXZlbnRzLmVhY2goZnVuY3Rpb24oKXt2YXIgc2luZ2xlRGF0ZT1qUXVlcnkodGhpcyksZGF0ZUNvbXA9c2luZ2xlRGF0ZS5kYXRhKCdkYXRlJykuc3BsaXQoJ1QnKTtpZihkYXRlQ29tcC5sZW5ndGg+MSl7dmFyIGRheUNvbXA9ZGF0ZUNvbXBbMF0uc3BsaXQoJy8nKSx0aW1lQ29tcD1kYXRlQ29tcFsxXS5zcGxpdCgnOicpO31lbHNlIGlmKGRhdGVDb21wWzBdLmluZGV4T2YoJzonKT49MCl7dmFyIGRheUNvbXA9W1wiMjAwMFwiLFwiMFwiLFwiMFwiXSx0aW1lQ29tcD1kYXRlQ29tcFswXS5zcGxpdCgnOicpO31lbHNle3ZhciBkYXlDb21wPWRhdGVDb21wWzBdLnNwbGl0KCcvJyksdGltZUNvbXA9W1wiMFwiLFwiMFwiXTt9XG52YXIgbmV3RGF0ZT1uZXcgRGF0ZShkYXlDb21wWzJdLGRheUNvbXBbMV0tMSxkYXlDb21wWzBdLHRpbWVDb21wWzBdLHRpbWVDb21wWzFdKTtkYXRlQXJyYXlzLnB1c2gobmV3RGF0ZSk7fSk7cmV0dXJuIGRhdGVBcnJheXM7fVxuZnVuY3Rpb24gZGF5ZGlmZihmaXJzdCxzZWNvbmQpe3JldHVybiBNYXRoLnJvdW5kKChzZWNvbmQtZmlyc3QpKTt9XG5mdW5jdGlvbiBtaW5MYXBzZShkYXRlcyl7dmFyIGRhdGVEaXN0YW5jZXM9W107Zm9yKGk9MTtpPGRhdGVzLmxlbmd0aDtpKyspe3ZhciBkaXN0YW5jZT1kYXlkaWZmKGRhdGVzW2ktMV0sZGF0ZXNbaV0pO2RhdGVEaXN0YW5jZXMucHVzaChkaXN0YW5jZSk7fVxucmV0dXJuIE1hdGgubWluLmFwcGx5KG51bGwsZGF0ZURpc3RhbmNlcyk7fVxuZnVuY3Rpb24gZWxlbWVudEluVmlld3BvcnQoZWwpe3ZhciB0b3A9ZWwub2Zmc2V0VG9wO3ZhciBsZWZ0PWVsLm9mZnNldExlZnQ7dmFyIHdpZHRoPWVsLm9mZnNldFdpZHRoO3ZhciBoZWlnaHQ9ZWwub2Zmc2V0SGVpZ2h0O3doaWxlKGVsLm9mZnNldFBhcmVudCl7ZWw9ZWwub2Zmc2V0UGFyZW50O3RvcCs9ZWwub2Zmc2V0VG9wO2xlZnQrPWVsLm9mZnNldExlZnQ7fVxucmV0dXJuKHRvcDwod2luZG93LnBhZ2VZT2Zmc2V0K3dpbmRvdy5pbm5lckhlaWdodCkmJmxlZnQ8KHdpbmRvdy5wYWdlWE9mZnNldCt3aW5kb3cuaW5uZXJXaWR0aCkmJih0b3AraGVpZ2h0KT53aW5kb3cucGFnZVlPZmZzZXQmJihsZWZ0K3dpZHRoKT53aW5kb3cucGFnZVhPZmZzZXQpO31cbmZ1bmN0aW9uIGNoZWNrTVEoKXtyZXR1cm4gd2luZG93LmdldENvbXB1dGVkU3R5bGUoZG9jdW1lbnQucXVlcnlTZWxlY3RvcignLmNkLWhvcml6b250YWwtdGltZWxpbmUnKSwnOjpiZWZvcmUnKS5nZXRQcm9wZXJ0eVZhbHVlKCdjb250ZW50JykucmVwbGFjZSgvJy9nLFwiXCIpLnJlcGxhY2UoL1wiL2csXCJcIik7fX0pO2VsZW1lbnRvckZyb250ZW5kLmhvb2tzLmFkZEFjdGlvbignZnJvbnRlbmQvZWxlbWVudF9yZWFkeS9ncmFuZHJlc3RhdXJhbnQtYmFja2dyb3VuZC1saXN0LmRlZmF1bHQnLGZ1bmN0aW9uKCRzY29wZSl7alF1ZXJ5KFwiLmJhY2tncm91bmQtbGlzdC13cmFwcGVyXCIpLmVhY2goZnVuY3Rpb24oKXt2YXIgcGFyZW50RGl2PWpRdWVyeSh0aGlzKTtwYXJlbnREaXYuY2hpbGRyZW4oJy5iYWNrZ3JvdW5kLWxpc3QtY29sdW1uJykuaG92ZXIoZnVuY3Rpb24oKXtwYXJlbnREaXYuZmluZCgnLmJhY2tncm91bmQtbGlzdC1pbWcnKS5yZW1vdmVDbGFzcygnaG92ZXInKTtqUXVlcnkodGhpcykubmV4dCgnLmJhY2tncm91bmQtbGlzdC1pbWcnKS5hZGRDbGFzcygnaG92ZXInKTt9KTt9KTt9KTtlbGVtZW50b3JGcm9udGVuZC5ob29rcy5hZGRBY3Rpb24oJ2Zyb250ZW5kL2VsZW1lbnRfcmVhZHkvZ3JhbmRyZXN0YXVyYW50LXBvcnRmb2xpby1tYXNvbnJ5LmRlZmF1bHQnLGZ1bmN0aW9uKCRzY29wZSl7alF1ZXJ5KGZ1bmN0aW9uKCQpe2pRdWVyeShcIi5wb3J0Zm9saW8tbWFzb25yeS1jb250ZW50LXdyYXBwZXIuZG8tbWFzb25yeVwiKS5lYWNoKGZ1bmN0aW9uKCl7dmFyIGdyaWQ9alF1ZXJ5KHRoaXMpO3ZhciBjb2xzPWdyaWQuYXR0cignZGF0YS1jb2xzJyk7Y29scz1wYXJzZUludChjb2xzKTt2YXIgZ3V0dGVyPTUwO3N3aXRjaChjb2xzKVxue2Nhc2UgMjpndXR0ZXI9NTA7YnJlYWs7Y2FzZSAzOmd1dHRlcj00MDticmVhaztjYXNlIDQ6Z3V0dGVyPTMwO2JyZWFrO2Nhc2UgNTpndXR0ZXI9MjA7YnJlYWs7fVxuZ3JpZC5pbWFnZXNMb2FkZWQoKS5kb25lKGZ1bmN0aW9uKCl7Z3JpZC5tYXNvbnJ5KHtpdGVtU2VsZWN0b3I6XCIuZ2FsbGVyeS1ncmlkLWl0ZW1cIixjb2x1bW5XaWR0aDpcIi5nYWxsZXJ5LWdyaWQtaXRlbVwiLGd1dHRlcjpndXR0ZXJ9KTt9KTtqUXVlcnkoXCIucG9ydGZvbGlvLW1hc29ucnktY29udGVudC13cmFwcGVyLmRvLW1hc29ucnkgaW1nLmxhenlfbWFzb25yeVwiKS5lYWNoKGZ1bmN0aW9uKCl7dmFyIGN1cnJlbnRJbWc9alF1ZXJ5KHRoaXMpO2N1cnJlbnRJbWcucGFyZW50KFwiZGl2LnBvc3QtZmVhdHVyZWQtaW1hZ2UtaG92ZXJcIikucmVtb3ZlQ2xhc3MoXCJsYXp5XCIpO3ZhciBjb2xzPWdyaWQuYXR0cignZGF0YS1jb2xzJyk7dmFyIGd1dHRlcj01MDtzd2l0Y2goY29scylcbntjYXNlIDI6Z3V0dGVyPTUwO2JyZWFrO2Nhc2UgMzpndXR0ZXI9NDA7YnJlYWs7Y2FzZSA0Omd1dHRlcj0zMDticmVhaztjYXNlIDU6Z3V0dGVyPTIwO2JyZWFrO31cbmpRdWVyeSh0aGlzKS5MYXp5KHtvbkZpbmlzaGVkQWxsOmZ1bmN0aW9uKCl7Z3JpZC5tYXNvbnJ5KHtpdGVtU2VsZWN0b3I6XCIucG9ydGZvbGlvLW1hc29ucnktZ3JpZC13cmFwcGVyLnNjYWxlLWFubVwiLGNvbHVtbldpZHRoOlwiLnBvcnRmb2xpby1tYXNvbnJ5LWdyaWQtd3JhcHBlci5zY2FsZS1hbm1cIixndXR0ZXI6Z3V0dGVyfSk7fSx9KTt9KTtqUXVlcnkoXCIucG9ydGZvbGlvLW1hc29ucnktY29udGFpbmVyXCIpLmVhY2goZnVuY3Rpb24oKXt2YXIgY29udGFpbmRlckRpdj1qUXVlcnkodGhpcyk7dmFyIHNlbGVjdGVkQ2xhc3M9XCJcIjtjb250YWluZGVyRGl2LmZpbmQoXCIuZmlsdGVyLXRhZy1idG5cIikub24oJ2NsaWNrJyxmdW5jdGlvbigpe2NvbnRhaW5kZXJEaXYuZmluZChcIi5maWx0ZXItdGFnLWJ0blwiKS5yZW1vdmVDbGFzcyhcImFjdGl2ZVwiKTtqUXVlcnkodGhpcykuYWRkQ2xhc3MoXCJhY3RpdmVcIik7c2VsZWN0ZWRDbGFzcz1qUXVlcnkodGhpcykuYXR0cihcImRhdGEtcmVsXCIpO3ZhciBncmlkRGl2PWNvbnRhaW5kZXJEaXYuZmluZChcIi5wb3J0Zm9saW8tbWFzb25yeS1jb250ZW50LXdyYXBwZXIuZG8tbWFzb25yeVwiKTtncmlkRGl2LmZhZGVUbygxMDAsMCk7Z3JpZERpdi5maW5kKFwiLnBvcnRmb2xpby1tYXNvbnJ5LWdyaWQtd3JhcHBlclwiKS5jc3Moe29wYWNpdHk6MCxkaXNwbGF5Oidub25lJyx0cmFuc2Zvcm06J3NjYWxlKDAuMCknfSk7Z3JpZERpdi5maW5kKFwiLnBvcnRmb2xpby1tYXNvbnJ5LWdyaWQtd3JhcHBlclwiKS5ub3QoXCIuXCIrc2VsZWN0ZWRDbGFzcykuZmFkZU91dCgpLnJlbW92ZUNsYXNzKCdzY2FsZS1hbm0nKTtzZXRUaW1lb3V0KGZ1bmN0aW9uKCl7alF1ZXJ5KFwiLlwiK3NlbGVjdGVkQ2xhc3MpLmZhZGVJbigpLmFkZENsYXNzKCdzY2FsZS1hbm0nKTtqUXVlcnkoXCIuXCIrc2VsZWN0ZWRDbGFzcykuY3NzKHtvcGFjaXR5OjEsZGlzcGxheTonYmxvY2snLHRyYW5zZm9ybTonc2NhbGUoMSwxKSd9KTtncmlkRGl2Lm1hc29ucnkoJ2Rlc3Ryb3knKTt2YXIgJGdyaWQ9Z3JpZERpdi5tYXNvbnJ5KHtpdGVtU2VsZWN0b3I6XCIucG9ydGZvbGlvLW1hc29ucnktZ3JpZC13cmFwcGVyLnNjYWxlLWFubVwiLGNvbHVtbldpZHRoOlwiLnBvcnRmb2xpby1tYXNvbnJ5LWdyaWQtd3JhcHBlci5zY2FsZS1hbm1cIixndXR0ZXI6Z3V0dGVyfSk7JGdyaWQubWFzb25yeSgncmVsb2FkSXRlbXMnKTtzZXRUaW1lb3V0KGZ1bmN0aW9uKCl7Z3JpZERpdi5mYWRlVG8oMzAwLDEpO30sMzAwKTt9LDMwMCk7fSk7fSk7fSk7fSk7fSk7ZWxlbWVudG9yRnJvbnRlbmQuaG9va3MuYWRkQWN0aW9uKCdmcm9udGVuZC9lbGVtZW50X3JlYWR5L2dyYW5kcmVzdGF1cmFudC1wb3J0Zm9saW8tbWFzb25yeS1ncmlkLmRlZmF1bHQnLGZ1bmN0aW9uKCRzY29wZSl7alF1ZXJ5KGZ1bmN0aW9uKCQpe2pRdWVyeShcIi5wb3J0Zm9saW8tbWFzb25yeS1jb250ZW50LXdyYXBwZXIuZG8tbWFzb25yeVwiKS5lYWNoKGZ1bmN0aW9uKCl7dmFyIGdyaWQ9alF1ZXJ5KHRoaXMpO3ZhciBjb2xzPWdyaWQuYXR0cignZGF0YS1jb2xzJyk7Y29scz1wYXJzZUludChjb2xzKTt2YXIgZ3V0dGVyPTUwO3N3aXRjaChjb2xzKVxue2Nhc2UgMjpndXR0ZXI9NTA7YnJlYWs7Y2FzZSAzOmd1dHRlcj00MDticmVhaztjYXNlIDQ6Z3V0dGVyPTMwO2JyZWFrO2Nhc2UgNTpndXR0ZXI9MjA7YnJlYWs7fVxuZ3JpZC5pbWFnZXNMb2FkZWQoKS5kb25lKGZ1bmN0aW9uKCl7Z3JpZC5tYXNvbnJ5KHtpdGVtU2VsZWN0b3I6XCIucG9ydGZvbGlvLW1hc29ucnktZ3JpZC13cmFwcGVyLnNjYWxlLWFubVwiLGNvbHVtbldpZHRoOlwiLnBvcnRmb2xpby1tYXNvbnJ5LWdyaWQtd3JhcHBlci5zY2FsZS1hbm1cIixndXR0ZXI6Z3V0dGVyfSk7fSk7alF1ZXJ5KFwiLnBvcnRmb2xpby1tYXNvbnJ5LWNvbnRlbnQtd3JhcHBlci5kby1tYXNvbnJ5IGltZy5sYXp5X21hc29ucnlcIikuZWFjaChmdW5jdGlvbigpe3ZhciBjdXJyZW50SW1nPWpRdWVyeSh0aGlzKTtjdXJyZW50SW1nLnBhcmVudChcImRpdi5wb3N0LWZlYXR1cmVkLWltYWdlLWhvdmVyXCIpLnJlbW92ZUNsYXNzKFwibGF6eVwiKTt2YXIgY29scz1ncmlkLmF0dHIoJ2RhdGEtY29scycpO3ZhciBndXR0ZXI9NTA7c3dpdGNoKGNvbHMpXG57Y2FzZSAyOmd1dHRlcj01MDticmVhaztjYXNlIDM6Z3V0dGVyPTQwO2JyZWFrO2Nhc2UgNDpndXR0ZXI9MzA7YnJlYWs7Y2FzZSA1Omd1dHRlcj0yMDticmVhazt9XG5qUXVlcnkodGhpcykuTGF6eSh7b25GaW5pc2hlZEFsbDpmdW5jdGlvbigpe2dyaWQubWFzb25yeSh7aXRlbVNlbGVjdG9yOlwiLmdhbGxlcnktZ3JpZC1pdGVtXCIsY29sdW1uV2lkdGg6XCIuZ2FsbGVyeS1ncmlkLWl0ZW1cIixndXR0ZXI6Z3V0dGVyfSk7fSx9KTt9KTtqUXVlcnkoXCIucG9ydGZvbGlvLW1hc29ucnktY29udGFpbmVyXCIpLmVhY2goZnVuY3Rpb24oKXt2YXIgY29udGFpbmRlckRpdj1qUXVlcnkodGhpcyk7dmFyIHNlbGVjdGVkQ2xhc3M9XCJcIjtjb250YWluZGVyRGl2LmZpbmQoXCIuZmlsdGVyLXRhZy1idG5cIikub24oJ2NsaWNrJyxmdW5jdGlvbigpe2NvbnRhaW5kZXJEaXYuZmluZChcIi5maWx0ZXItdGFnLWJ0blwiKS5yZW1vdmVDbGFzcyhcImFjdGl2ZVwiKTtqUXVlcnkodGhpcykuYWRkQ2xhc3MoXCJhY3RpdmVcIik7c2VsZWN0ZWRDbGFzcz1qUXVlcnkodGhpcykuYXR0cihcImRhdGEtcmVsXCIpO3ZhciBncmlkRGl2PWNvbnRhaW5kZXJEaXYuZmluZChcIi5wb3J0Zm9saW8tbWFzb25yeS1jb250ZW50LXdyYXBwZXIuZG8tbWFzb25yeVwiKTtncmlkRGl2LmZhZGVUbygxMDAsMCk7Z3JpZERpdi5maW5kKFwiLnBvcnRmb2xpby1tYXNvbnJ5LWdyaWQtd3JhcHBlclwiKS5jc3Moe29wYWNpdHk6MCxkaXNwbGF5Oidub25lJyx0cmFuc2Zvcm06J3NjYWxlKDAuMCknfSk7Z3JpZERpdi5maW5kKFwiLnBvcnRmb2xpby1tYXNvbnJ5LWdyaWQtd3JhcHBlclwiKS5ub3QoXCIuXCIrc2VsZWN0ZWRDbGFzcykuZmFkZU91dCgpLnJlbW92ZUNsYXNzKCdzY2FsZS1hbm0nKTtzZXRUaW1lb3V0KGZ1bmN0aW9uKCl7alF1ZXJ5KFwiLlwiK3NlbGVjdGVkQ2xhc3MpLmZhZGVJbigpLmFkZENsYXNzKCdzY2FsZS1hbm0nKTtqUXVlcnkoXCIuXCIrc2VsZWN0ZWRDbGFzcykuY3NzKHtvcGFjaXR5OjEsZGlzcGxheTonYmxvY2snLHRyYW5zZm9ybTonc2NhbGUoMSwxKSd9KTtncmlkRGl2Lm1hc29ucnkoJ2Rlc3Ryb3knKTt2YXIgJGdyaWQ9Z3JpZERpdi5tYXNvbnJ5KHtpdGVtU2VsZWN0b3I6XCIucG9ydGZvbGlvLW1hc29ucnktZ3JpZC13cmFwcGVyLnNjYWxlLWFubVwiLGNvbHVtbldpZHRoOlwiLnBvcnRmb2xpby1tYXNvbnJ5LWdyaWQtd3JhcHBlci5zY2FsZS1hbm1cIixndXR0ZXI6Z3V0dGVyfSk7JGdyaWQubWFzb25yeSgncmVsb2FkSXRlbXMnKTtzZXRUaW1lb3V0KGZ1bmN0aW9uKCl7Z3JpZERpdi5mYWRlVG8oMzAwLDEpO30sMzAwKTt9LDMwMCk7fSk7fSk7fSk7fSk7fSk7ZWxlbWVudG9yRnJvbnRlbmQuaG9va3MuYWRkQWN0aW9uKCdmcm9udGVuZC9lbGVtZW50X3JlYWR5L2dyYW5kcmVzdGF1cmFudC1wb3J0Zm9saW8tdGltZWxpbmUuZGVmYXVsdCcsZnVuY3Rpb24oJHNjb3BlKXt2YXIgdGltZWxpbmVzPWpRdWVyeSgnLmNkLWhvcml6b250YWwtdGltZWxpbmUnKTt2YXIgZXZlbnRzTWluRGlzdGFuY2U9dGltZWxpbmVzLmF0dHIoJ2RhdGEtc3BhY2luZycpO2lmKGV2ZW50c01pbkRpc3RhbmNlPT0nJylcbntldmVudHNNaW5EaXN0YW5jZT02MDt9XG4odGltZWxpbmVzLmxlbmd0aD4wKSYmaW5pdFRpbWVsaW5lKHRpbWVsaW5lcyk7ZnVuY3Rpb24gaW5pdFRpbWVsaW5lKHRpbWVsaW5lcyl7dGltZWxpbmVzLmVhY2goZnVuY3Rpb24oKXt2YXIgdGltZWxpbmU9alF1ZXJ5KHRoaXMpLHRpbWVsaW5lQ29tcG9uZW50cz17fTt0aW1lbGluZUNvbXBvbmVudHNbJ3RpbWVsaW5lV3JhcHBlciddPXRpbWVsaW5lLmZpbmQoJy5ldmVudHMtd3JhcHBlcicpO3RpbWVsaW5lQ29tcG9uZW50c1snZXZlbnRzV3JhcHBlciddPXRpbWVsaW5lQ29tcG9uZW50c1sndGltZWxpbmVXcmFwcGVyJ10uY2hpbGRyZW4oJy5ldmVudHMnKTt0aW1lbGluZUNvbXBvbmVudHNbJ2ZpbGxpbmdMaW5lJ109dGltZWxpbmVDb21wb25lbnRzWydldmVudHNXcmFwcGVyJ10uY2hpbGRyZW4oJy5maWxsaW5nLWxpbmUnKTt0aW1lbGluZUNvbXBvbmVudHNbJ3RpbWVsaW5lRXZlbnRzJ109dGltZWxpbmVDb21wb25lbnRzWydldmVudHNXcmFwcGVyJ10uZmluZCgnYScpO3RpbWVsaW5lQ29tcG9uZW50c1sndGltZWxpbmVEYXRlcyddPXBhcnNlRGF0ZSh0aW1lbGluZUNvbXBvbmVudHNbJ3RpbWVsaW5lRXZlbnRzJ10pO3RpbWVsaW5lQ29tcG9uZW50c1snZXZlbnRzTWluTGFwc2UnXT1taW5MYXBzZSh0aW1lbGluZUNvbXBvbmVudHNbJ3RpbWVsaW5lRGF0ZXMnXSk7dGltZWxpbmVDb21wb25lbnRzWyd0aW1lbGluZU5hdmlnYXRpb24nXT10aW1lbGluZS5maW5kKCcuY2QtdGltZWxpbmUtbmF2aWdhdGlvbicpO3RpbWVsaW5lQ29tcG9uZW50c1snZXZlbnRzQ29udGVudCddPXRpbWVsaW5lLmNoaWxkcmVuKCcuZXZlbnRzLWNvbnRlbnQnKTtzZXREYXRlUG9zaXRpb24odGltZWxpbmVDb21wb25lbnRzLGV2ZW50c01pbkRpc3RhbmNlKTt2YXIgdGltZWxpbmVUb3RXaWR0aD1zZXRUaW1lbGluZVdpZHRoKHRpbWVsaW5lQ29tcG9uZW50cyxldmVudHNNaW5EaXN0YW5jZSk7dGltZWxpbmUuYWRkQ2xhc3MoJ2xvYWRlZCcpO3RpbWVsaW5lQ29tcG9uZW50c1sndGltZWxpbmVOYXZpZ2F0aW9uJ10ub24oJ2NsaWNrJywnLm5leHQnLGZ1bmN0aW9uKGV2ZW50KXtldmVudC5wcmV2ZW50RGVmYXVsdCgpO3VwZGF0ZVNsaWRlKHRpbWVsaW5lQ29tcG9uZW50cyx0aW1lbGluZVRvdFdpZHRoLCduZXh0Jyk7fSk7dGltZWxpbmVDb21wb25lbnRzWyd0aW1lbGluZU5hdmlnYXRpb24nXS5vbignY2xpY2snLCcucHJldicsZnVuY3Rpb24oZXZlbnQpe2V2ZW50LnByZXZlbnREZWZhdWx0KCk7dXBkYXRlU2xpZGUodGltZWxpbmVDb21wb25lbnRzLHRpbWVsaW5lVG90V2lkdGgsJ3ByZXYnKTt9KTt0aW1lbGluZUNvbXBvbmVudHNbJ2V2ZW50c1dyYXBwZXInXS5vbignY2xpY2snLCdhJyxmdW5jdGlvbihldmVudCl7ZXZlbnQucHJldmVudERlZmF1bHQoKTt0aW1lbGluZUNvbXBvbmVudHNbJ3RpbWVsaW5lRXZlbnRzJ10ucmVtb3ZlQ2xhc3MoJ3NlbGVjdGVkJyk7alF1ZXJ5KHRoaXMpLmFkZENsYXNzKCdzZWxlY3RlZCcpO3VwZGF0ZU9sZGVyRXZlbnRzKGpRdWVyeSh0aGlzKSk7dXBkYXRlRmlsbGluZyhqUXVlcnkodGhpcyksdGltZWxpbmVDb21wb25lbnRzWydmaWxsaW5nTGluZSddLHRpbWVsaW5lVG90V2lkdGgpO3VwZGF0ZVZpc2libGVDb250ZW50KGpRdWVyeSh0aGlzKSx0aW1lbGluZUNvbXBvbmVudHNbJ2V2ZW50c0NvbnRlbnQnXSk7fSk7dGltZWxpbmVDb21wb25lbnRzWydldmVudHNDb250ZW50J10ub24oJ3N3aXBlbGVmdCcsZnVuY3Rpb24oKXt2YXIgbXE9Y2hlY2tNUSgpOyhtcT09J21vYmlsZScpJiZzaG93TmV3Q29udGVudCh0aW1lbGluZUNvbXBvbmVudHMsdGltZWxpbmVUb3RXaWR0aCwnbmV4dCcpO30pO3RpbWVsaW5lQ29tcG9uZW50c1snZXZlbnRzQ29udGVudCddLm9uKCdzd2lwZXJpZ2h0JyxmdW5jdGlvbigpe3ZhciBtcT1jaGVja01RKCk7KG1xPT0nbW9iaWxlJykmJnNob3dOZXdDb250ZW50KHRpbWVsaW5lQ29tcG9uZW50cyx0aW1lbGluZVRvdFdpZHRoLCdwcmV2Jyk7fSk7alF1ZXJ5KGRvY3VtZW50KS5rZXl1cChmdW5jdGlvbihldmVudCl7aWYoZXZlbnQud2hpY2g9PSczNycmJmVsZW1lbnRJblZpZXdwb3J0KHRpbWVsaW5lLmdldCgwKSkpe3Nob3dOZXdDb250ZW50KHRpbWVsaW5lQ29tcG9uZW50cyx0aW1lbGluZVRvdFdpZHRoLCdwcmV2Jyk7fWVsc2UgaWYoZXZlbnQud2hpY2g9PSczOScmJmVsZW1lbnRJblZpZXdwb3J0KHRpbWVsaW5lLmdldCgwKSkpe3Nob3dOZXdDb250ZW50KHRpbWVsaW5lQ29tcG9uZW50cyx0aW1lbGluZVRvdFdpZHRoLCduZXh0Jyk7fX0pO30pO31cbmZ1bmN0aW9uIHVwZGF0ZVNsaWRlKHRpbWVsaW5lQ29tcG9uZW50cyx0aW1lbGluZVRvdFdpZHRoLHN0cmluZyl7dmFyIHRyYW5zbGF0ZVZhbHVlPWdldFRyYW5zbGF0ZVZhbHVlKHRpbWVsaW5lQ29tcG9uZW50c1snZXZlbnRzV3JhcHBlciddKSx3cmFwcGVyV2lkdGg9TnVtYmVyKHRpbWVsaW5lQ29tcG9uZW50c1sndGltZWxpbmVXcmFwcGVyJ10uY3NzKCd3aWR0aCcpLnJlcGxhY2UoJ3B4JywnJykpOyhzdHJpbmc9PSduZXh0Jyk/dHJhbnNsYXRlVGltZWxpbmUodGltZWxpbmVDb21wb25lbnRzLHRyYW5zbGF0ZVZhbHVlLXdyYXBwZXJXaWR0aCtldmVudHNNaW5EaXN0YW5jZSx3cmFwcGVyV2lkdGgtdGltZWxpbmVUb3RXaWR0aCk6dHJhbnNsYXRlVGltZWxpbmUodGltZWxpbmVDb21wb25lbnRzLHRyYW5zbGF0ZVZhbHVlK3dyYXBwZXJXaWR0aC1ldmVudHNNaW5EaXN0YW5jZSk7fVxuZnVuY3Rpb24gc2hvd05ld0NvbnRlbnQodGltZWxpbmVDb21wb25lbnRzLHRpbWVsaW5lVG90V2lkdGgsc3RyaW5nKXt2YXIgdmlzaWJsZUNvbnRlbnQ9dGltZWxpbmVDb21wb25lbnRzWydldmVudHNDb250ZW50J10uZmluZCgnLnNlbGVjdGVkJyksbmV3Q29udGVudD0oc3RyaW5nPT0nbmV4dCcpP3Zpc2libGVDb250ZW50Lm5leHQoKTp2aXNpYmxlQ29udGVudC5wcmV2KCk7aWYobmV3Q29udGVudC5sZW5ndGg+MCl7dmFyIHNlbGVjdGVkRGF0ZT10aW1lbGluZUNvbXBvbmVudHNbJ2V2ZW50c1dyYXBwZXInXS5maW5kKCcuc2VsZWN0ZWQnKSxuZXdFdmVudD0oc3RyaW5nPT0nbmV4dCcpP3NlbGVjdGVkRGF0ZS5wYXJlbnQoJ2xpJykubmV4dCgnbGknKS5jaGlsZHJlbignYScpOnNlbGVjdGVkRGF0ZS5wYXJlbnQoJ2xpJykucHJldignbGknKS5jaGlsZHJlbignYScpO3VwZGF0ZUZpbGxpbmcobmV3RXZlbnQsdGltZWxpbmVDb21wb25lbnRzWydmaWxsaW5nTGluZSddLHRpbWVsaW5lVG90V2lkdGgpO3VwZGF0ZVZpc2libGVDb250ZW50KG5ld0V2ZW50LHRpbWVsaW5lQ29tcG9uZW50c1snZXZlbnRzQ29udGVudCddKTtuZXdFdmVudC5hZGRDbGFzcygnc2VsZWN0ZWQnKTtzZWxlY3RlZERhdGUucmVtb3ZlQ2xhc3MoJ3NlbGVjdGVkJyk7dXBkYXRlT2xkZXJFdmVudHMobmV3RXZlbnQpO3VwZGF0ZVRpbWVsaW5lUG9zaXRpb24oc3RyaW5nLG5ld0V2ZW50LHRpbWVsaW5lQ29tcG9uZW50cyx0aW1lbGluZVRvdFdpZHRoKTt9fVxuZnVuY3Rpb24gdXBkYXRlVGltZWxpbmVQb3NpdGlvbihzdHJpbmcsZXZlbnQsdGltZWxpbmVDb21wb25lbnRzLHRpbWVsaW5lVG90V2lkdGgpe3ZhciBldmVudFN0eWxlPXdpbmRvdy5nZXRDb21wdXRlZFN0eWxlKGV2ZW50LmdldCgwKSxudWxsKSxldmVudExlZnQ9TnVtYmVyKGV2ZW50U3R5bGUuZ2V0UHJvcGVydHlWYWx1ZShcImxlZnRcIikucmVwbGFjZSgncHgnLCcnKSksdGltZWxpbmVXaWR0aD1OdW1iZXIodGltZWxpbmVDb21wb25lbnRzWyd0aW1lbGluZVdyYXBwZXInXS5jc3MoJ3dpZHRoJykucmVwbGFjZSgncHgnLCcnKSksdGltZWxpbmVUb3RXaWR0aD1OdW1iZXIodGltZWxpbmVDb21wb25lbnRzWydldmVudHNXcmFwcGVyJ10uY3NzKCd3aWR0aCcpLnJlcGxhY2UoJ3B4JywnJykpO3ZhciB0aW1lbGluZVRyYW5zbGF0ZT1nZXRUcmFuc2xhdGVWYWx1ZSh0aW1lbGluZUNvbXBvbmVudHNbJ2V2ZW50c1dyYXBwZXInXSk7aWYoKHN0cmluZz09J25leHQnJiZldmVudExlZnQ+dGltZWxpbmVXaWR0aC10aW1lbGluZVRyYW5zbGF0ZSl8fChzdHJpbmc9PSdwcmV2JyYmZXZlbnRMZWZ0PC10aW1lbGluZVRyYW5zbGF0ZSkpe3RyYW5zbGF0ZVRpbWVsaW5lKHRpbWVsaW5lQ29tcG9uZW50cywtZXZlbnRMZWZ0K3RpbWVsaW5lV2lkdGgvMix0aW1lbGluZVdpZHRoLXRpbWVsaW5lVG90V2lkdGgpO319XG5mdW5jdGlvbiB0cmFuc2xhdGVUaW1lbGluZSh0aW1lbGluZUNvbXBvbmVudHMsdmFsdWUsdG90V2lkdGgpe3ZhciBldmVudHNXcmFwcGVyPXRpbWVsaW5lQ29tcG9uZW50c1snZXZlbnRzV3JhcHBlciddLmdldCgwKTt2YWx1ZT0odmFsdWU+MCk/MDp2YWx1ZTt2YWx1ZT0oISh0eXBlb2YgdG90V2lkdGg9PT0ndW5kZWZpbmVkJykmJnZhbHVlPHRvdFdpZHRoKT90b3RXaWR0aDp2YWx1ZTtzZXRUcmFuc2Zvcm1WYWx1ZShldmVudHNXcmFwcGVyLCd0cmFuc2xhdGVYJyx2YWx1ZSsncHgnKTsodmFsdWU9PTApP3RpbWVsaW5lQ29tcG9uZW50c1sndGltZWxpbmVOYXZpZ2F0aW9uJ10uZmluZCgnLnByZXYnKS5hZGRDbGFzcygnaW5hY3RpdmUnKTp0aW1lbGluZUNvbXBvbmVudHNbJ3RpbWVsaW5lTmF2aWdhdGlvbiddLmZpbmQoJy5wcmV2JykucmVtb3ZlQ2xhc3MoJ2luYWN0aXZlJyk7KHZhbHVlPT10b3RXaWR0aCk/dGltZWxpbmVDb21wb25lbnRzWyd0aW1lbGluZU5hdmlnYXRpb24nXS5maW5kKCcubmV4dCcpLmFkZENsYXNzKCdpbmFjdGl2ZScpOnRpbWVsaW5lQ29tcG9uZW50c1sndGltZWxpbmVOYXZpZ2F0aW9uJ10uZmluZCgnLm5leHQnKS5yZW1vdmVDbGFzcygnaW5hY3RpdmUnKTt9XG5mdW5jdGlvbiB1cGRhdGVGaWxsaW5nKHNlbGVjdGVkRXZlbnQsZmlsbGluZyx0b3RXaWR0aCl7dmFyIGV2ZW50U3R5bGU9d2luZG93LmdldENvbXB1dGVkU3R5bGUoc2VsZWN0ZWRFdmVudC5nZXQoMCksbnVsbCksZXZlbnRMZWZ0PWV2ZW50U3R5bGUuZ2V0UHJvcGVydHlWYWx1ZShcImxlZnRcIiksZXZlbnRXaWR0aD1ldmVudFN0eWxlLmdldFByb3BlcnR5VmFsdWUoXCJ3aWR0aFwiKTtldmVudExlZnQ9TnVtYmVyKGV2ZW50TGVmdC5yZXBsYWNlKCdweCcsJycpKStOdW1iZXIoZXZlbnRXaWR0aC5yZXBsYWNlKCdweCcsJycpKS8yO3ZhciBzY2FsZVZhbHVlPWV2ZW50TGVmdC90b3RXaWR0aDtzZXRUcmFuc2Zvcm1WYWx1ZShmaWxsaW5nLmdldCgwKSwnc2NhbGVYJyxzY2FsZVZhbHVlKTt9XG5mdW5jdGlvbiBzZXREYXRlUG9zaXRpb24odGltZWxpbmVDb21wb25lbnRzLG1pbil7Zm9yKGk9MDtpPHRpbWVsaW5lQ29tcG9uZW50c1sndGltZWxpbmVEYXRlcyddLmxlbmd0aDtpKyspe3ZhciBkaXN0YW5jZT1kYXlkaWZmKHRpbWVsaW5lQ29tcG9uZW50c1sndGltZWxpbmVEYXRlcyddWzBdLHRpbWVsaW5lQ29tcG9uZW50c1sndGltZWxpbmVEYXRlcyddW2ldKSxkaXN0YW5jZU5vcm09TWF0aC5yb3VuZChkaXN0YW5jZS90aW1lbGluZUNvbXBvbmVudHNbJ2V2ZW50c01pbkxhcHNlJ10pKzI7dGltZWxpbmVDb21wb25lbnRzWyd0aW1lbGluZUV2ZW50cyddLmVxKGkpLmNzcygnbGVmdCcsZGlzdGFuY2VOb3JtKm1pbisncHgnKTt9fVxuZnVuY3Rpb24gc2V0VGltZWxpbmVXaWR0aCh0aW1lbGluZUNvbXBvbmVudHMsd2lkdGgpe3ZhciB0aW1lU3Bhbj1kYXlkaWZmKHRpbWVsaW5lQ29tcG9uZW50c1sndGltZWxpbmVEYXRlcyddWzBdLHRpbWVsaW5lQ29tcG9uZW50c1sndGltZWxpbmVEYXRlcyddW3RpbWVsaW5lQ29tcG9uZW50c1sndGltZWxpbmVEYXRlcyddLmxlbmd0aC0xXSksdGltZVNwYW5Ob3JtPXRpbWVTcGFuL3RpbWVsaW5lQ29tcG9uZW50c1snZXZlbnRzTWluTGFwc2UnXSx0aW1lU3Bhbk5vcm09TWF0aC5yb3VuZCh0aW1lU3Bhbk5vcm0pKzQsdG90YWxXaWR0aD10aW1lU3Bhbk5vcm0qd2lkdGg7dGltZWxpbmVDb21wb25lbnRzWydldmVudHNXcmFwcGVyJ10uY3NzKCd3aWR0aCcsdG90YWxXaWR0aCsncHgnKTt1cGRhdGVGaWxsaW5nKHRpbWVsaW5lQ29tcG9uZW50c1sndGltZWxpbmVFdmVudHMnXS5lcSgwKSx0aW1lbGluZUNvbXBvbmVudHNbJ2ZpbGxpbmdMaW5lJ10sdG90YWxXaWR0aCk7cmV0dXJuIHRvdGFsV2lkdGg7fVxuZnVuY3Rpb24gdXBkYXRlVmlzaWJsZUNvbnRlbnQoZXZlbnQsZXZlbnRzQ29udGVudCl7dmFyIGV2ZW50RGF0ZT1ldmVudC5kYXRhKCdkYXRlJyksdmlzaWJsZUNvbnRlbnQ9ZXZlbnRzQ29udGVudC5maW5kKCcuc2VsZWN0ZWQnKSxzZWxlY3RlZENvbnRlbnQ9ZXZlbnRzQ29udGVudC5maW5kKCdbZGF0YS1kYXRlPVwiJytldmVudERhdGUrJ1wiXScpLHNlbGVjdGVkQ29udGVudEhlaWdodD1zZWxlY3RlZENvbnRlbnQuaGVpZ2h0KCk7aWYoc2VsZWN0ZWRDb250ZW50LmluZGV4KCk+dmlzaWJsZUNvbnRlbnQuaW5kZXgoKSl7dmFyIGNsYXNzRW5ldGVyaW5nPSdzZWxlY3RlZCBlbnRlci1yaWdodCcsY2xhc3NMZWF2aW5nPSdsZWF2ZS1sZWZ0Jzt9ZWxzZXt2YXIgY2xhc3NFbmV0ZXJpbmc9J3NlbGVjdGVkIGVudGVyLWxlZnQnLGNsYXNzTGVhdmluZz0nbGVhdmUtcmlnaHQnO31cbnNlbGVjdGVkQ29udGVudC5hdHRyKCdjbGFzcycsY2xhc3NFbmV0ZXJpbmcpO3Zpc2libGVDb250ZW50LmF0dHIoJ2NsYXNzJyxjbGFzc0xlYXZpbmcpLm9uZSgnd2Via2l0QW5pbWF0aW9uRW5kIG9hbmltYXRpb25lbmQgbXNBbmltYXRpb25FbmQgYW5pbWF0aW9uZW5kJyxmdW5jdGlvbigpe3Zpc2libGVDb250ZW50LnJlbW92ZUNsYXNzKCdsZWF2ZS1yaWdodCBsZWF2ZS1sZWZ0Jyk7c2VsZWN0ZWRDb250ZW50LnJlbW92ZUNsYXNzKCdlbnRlci1sZWZ0IGVudGVyLXJpZ2h0Jyk7fSk7ZXZlbnRzQ29udGVudC5jc3MoJ2hlaWdodCcsc2VsZWN0ZWRDb250ZW50SGVpZ2h0KydweCcpO31cbmZ1bmN0aW9uIHVwZGF0ZU9sZGVyRXZlbnRzKGV2ZW50KXtldmVudC5wYXJlbnQoJ2xpJykucHJldkFsbCgnbGknKS5jaGlsZHJlbignYScpLmFkZENsYXNzKCdvbGRlci1ldmVudCcpLmVuZCgpLmVuZCgpLm5leHRBbGwoJ2xpJykuY2hpbGRyZW4oJ2EnKS5yZW1vdmVDbGFzcygnb2xkZXItZXZlbnQnKTt9XG5mdW5jdGlvbiBnZXRUcmFuc2xhdGVWYWx1ZSh0aW1lbGluZSl7dmFyIHRpbWVsaW5lU3R5bGU9d2luZG93LmdldENvbXB1dGVkU3R5bGUodGltZWxpbmUuZ2V0KDApLG51bGwpLHRpbWVsaW5lVHJhbnNsYXRlPXRpbWVsaW5lU3R5bGUuZ2V0UHJvcGVydHlWYWx1ZShcIi13ZWJraXQtdHJhbnNmb3JtXCIpfHx0aW1lbGluZVN0eWxlLmdldFByb3BlcnR5VmFsdWUoXCItbW96LXRyYW5zZm9ybVwiKXx8dGltZWxpbmVTdHlsZS5nZXRQcm9wZXJ0eVZhbHVlKFwiLW1zLXRyYW5zZm9ybVwiKXx8dGltZWxpbmVTdHlsZS5nZXRQcm9wZXJ0eVZhbHVlKFwiLW8tdHJhbnNmb3JtXCIpfHx0aW1lbGluZVN0eWxlLmdldFByb3BlcnR5VmFsdWUoXCJ0cmFuc2Zvcm1cIik7aWYodGltZWxpbmVUcmFuc2xhdGUuaW5kZXhPZignKCcpPj0wKXt2YXIgdGltZWxpbmVUcmFuc2xhdGU9dGltZWxpbmVUcmFuc2xhdGUuc3BsaXQoJygnKVsxXTt0aW1lbGluZVRyYW5zbGF0ZT10aW1lbGluZVRyYW5zbGF0ZS5zcGxpdCgnKScpWzBdO3RpbWVsaW5lVHJhbnNsYXRlPXRpbWVsaW5lVHJhbnNsYXRlLnNwbGl0KCcsJyk7dmFyIHRyYW5zbGF0ZVZhbHVlPXRpbWVsaW5lVHJhbnNsYXRlWzRdO31lbHNle3ZhciB0cmFuc2xhdGVWYWx1ZT0wO31cbnJldHVybiBOdW1iZXIodHJhbnNsYXRlVmFsdWUpO31cbmZ1bmN0aW9uIHNldFRyYW5zZm9ybVZhbHVlKGVsZW1lbnQscHJvcGVydHksdmFsdWUpe2VsZW1lbnQuc3R5bGVbXCItd2Via2l0LXRyYW5zZm9ybVwiXT1wcm9wZXJ0eStcIihcIit2YWx1ZStcIilcIjtlbGVtZW50LnN0eWxlW1wiLW1vei10cmFuc2Zvcm1cIl09cHJvcGVydHkrXCIoXCIrdmFsdWUrXCIpXCI7ZWxlbWVudC5zdHlsZVtcIi1tcy10cmFuc2Zvcm1cIl09cHJvcGVydHkrXCIoXCIrdmFsdWUrXCIpXCI7ZWxlbWVudC5zdHlsZVtcIi1vLXRyYW5zZm9ybVwiXT1wcm9wZXJ0eStcIihcIit2YWx1ZStcIilcIjtlbGVtZW50LnN0eWxlW1widHJhbnNmb3JtXCJdPXByb3BlcnR5K1wiKFwiK3ZhbHVlK1wiKVwiO31cbmZ1bmN0aW9uIHBhcnNlRGF0ZShldmVudHMpe3ZhciBkYXRlQXJyYXlzPVtdO2V2ZW50cy5lYWNoKGZ1bmN0aW9uKCl7dmFyIGRhdGVDb21wPWpRdWVyeSh0aGlzKS5kYXRhKCdkYXRlJykuc3BsaXQoJy8nKSxuZXdEYXRlPW5ldyBEYXRlKGRhdGVDb21wWzJdLGRhdGVDb21wWzFdLTEsZGF0ZUNvbXBbMF0pO2RhdGVBcnJheXMucHVzaChuZXdEYXRlKTt9KTtyZXR1cm4gZGF0ZUFycmF5czt9XG5mdW5jdGlvbiBwYXJzZURhdGUyKGV2ZW50cyl7dmFyIGRhdGVBcnJheXM9W107ZXZlbnRzLmVhY2goZnVuY3Rpb24oKXt2YXIgc2luZ2xlRGF0ZT1qUXVlcnkodGhpcyksZGF0ZUNvbXA9c2luZ2xlRGF0ZS5kYXRhKCdkYXRlJykuc3BsaXQoJ1QnKTtpZihkYXRlQ29tcC5sZW5ndGg+MSl7dmFyIGRheUNvbXA9ZGF0ZUNvbXBbMF0uc3BsaXQoJy8nKSx0aW1lQ29tcD1kYXRlQ29tcFsxXS5zcGxpdCgnOicpO31lbHNlIGlmKGRhdGVDb21wWzBdLmluZGV4T2YoJzonKT49MCl7dmFyIGRheUNvbXA9W1wiMjAwMFwiLFwiMFwiLFwiMFwiXSx0aW1lQ29tcD1kYXRlQ29tcFswXS5zcGxpdCgnOicpO31lbHNle3ZhciBkYXlDb21wPWRhdGVDb21wWzBdLnNwbGl0KCcvJyksdGltZUNvbXA9W1wiMFwiLFwiMFwiXTt9XG52YXIgbmV3RGF0ZT1uZXcgRGF0ZShkYXlDb21wWzJdLGRheUNvbXBbMV0tMSxkYXlDb21wWzBdLHRpbWVDb21wWzBdLHRpbWVDb21wWzFdKTtkYXRlQXJyYXlzLnB1c2gobmV3RGF0ZSk7fSk7cmV0dXJuIGRhdGVBcnJheXM7fVxuZnVuY3Rpb24gZGF5ZGlmZihmaXJzdCxzZWNvbmQpe3JldHVybiBNYXRoLnJvdW5kKChzZWNvbmQtZmlyc3QpKTt9XG5mdW5jdGlvbiBtaW5MYXBzZShkYXRlcyl7dmFyIGRhdGVEaXN0YW5jZXM9W107Zm9yKGk9MTtpPGRhdGVzLmxlbmd0aDtpKyspe3ZhciBkaXN0YW5jZT1kYXlkaWZmKGRhdGVzW2ktMV0sZGF0ZXNbaV0pO2RhdGVEaXN0YW5jZXMucHVzaChkaXN0YW5jZSk7fVxucmV0dXJuIE1hdGgubWluLmFwcGx5KG51bGwsZGF0ZURpc3RhbmNlcyk7fVxuZnVuY3Rpb24gZWxlbWVudEluVmlld3BvcnQoZWwpe3ZhciB0b3A9ZWwub2Zmc2V0VG9wO3ZhciBsZWZ0PWVsLm9mZnNldExlZnQ7dmFyIHdpZHRoPWVsLm9mZnNldFdpZHRoO3ZhciBoZWlnaHQ9ZWwub2Zmc2V0SGVpZ2h0O3doaWxlKGVsLm9mZnNldFBhcmVudCl7ZWw9ZWwub2Zmc2V0UGFyZW50O3RvcCs9ZWwub2Zmc2V0VG9wO2xlZnQrPWVsLm9mZnNldExlZnQ7fVxucmV0dXJuKHRvcDwod2luZG93LnBhZ2VZT2Zmc2V0K3dpbmRvdy5pbm5lckhlaWdodCkmJmxlZnQ8KHdpbmRvdy5wYWdlWE9mZnNldCt3aW5kb3cuaW5uZXJXaWR0aCkmJih0b3AraGVpZ2h0KT53aW5kb3cucGFnZVlPZmZzZXQmJihsZWZ0K3dpZHRoKT53aW5kb3cucGFnZVhPZmZzZXQpO31cbmZ1bmN0aW9uIGNoZWNrTVEoKXtyZXR1cm4gd2luZG93LmdldENvbXB1dGVkU3R5bGUoZG9jdW1lbnQucXVlcnlTZWxlY3RvcignLmNkLWhvcml6b250YWwtdGltZWxpbmUnKSwnOjpiZWZvcmUnKS5nZXRQcm9wZXJ0eVZhbHVlKCdjb250ZW50JykucmVwbGFjZSgvJy9nLFwiXCIpLnJlcGxhY2UoL1wiL2csXCJcIik7fX0pO2VsZW1lbnRvckZyb250ZW5kLmhvb2tzLmFkZEFjdGlvbignZnJvbnRlbmQvZWxlbWVudF9yZWFkeS9ncmFuZHJlc3RhdXJhbnQtcG9ydGZvbGlvLXRpbWVsaW5lLXZlcnRpY2FsLmRlZmF1bHQnLGZ1bmN0aW9uKCRzY29wZSl7alF1ZXJ5KFwiLnBvcnRmb2xpby10aW1lbGluZS12ZXJ0aWNhbC1jb250ZW50LXdyYXBwZXJcIikuZWFjaChmdW5jdGlvbigpe3ZhciBzbGlkZXNob3c9alF1ZXJ5KHRoaXMpO3ZhciBhdXRvUGxheT1zbGlkZXNob3cuYXR0cignZGF0YS1hdXRvcGxheScpO3ZhciBhdXRvUGxheUFycj1mYWxzZTtpZih0eXBlb2YgYXV0b1BsYXkhPVwidW5kZWZpbmVkXCIpe2F1dG9QbGF5QXJyPXtkZWxheTphdXRvUGxheX07fVxudmFyIHNwZWVkPXNsaWRlc2hvdy5hdHRyKCdkYXRhLXNwZWVkJyk7aWYodHlwZW9mIHNwZWVkPT1cInVuZGVmaW5lZFwiKXtzcGVlZD0xNjAwO31cbnZhciB0aW1lbGluZVN3aXBlcj1uZXcgU3dpcGVyKCcucG9ydGZvbGlvLXRpbWVsaW5lLXZlcnRpY2FsLWNvbnRlbnQtd3JhcHBlciAudGltZWxpbmUgLnN3aXBlci1jb250YWluZXInLHtkaXJlY3Rpb246J3ZlcnRpY2FsJyxsb29wOmZhbHNlLHNwZWVkOnBhcnNlSW50KHNwZWVkKSxhdXRvcGxheTphdXRvUGxheUFycixwYWdpbmF0aW9uOntlbDonLnN3aXBlci1wYWdpbmF0aW9uJyx0eXBlOididWxsZXRzJyxyZW5kZXJCdWxsZXQ6ZnVuY3Rpb24oaW5kZXgsY2xhc3NOYW1lKXt2YXIgeWVhcj1kb2N1bWVudC5xdWVyeVNlbGVjdG9yQWxsKCcuc3dpcGVyLXNsaWRlJylbaW5kZXhdLmdldEF0dHJpYnV0ZSgnZGF0YS15ZWFyJyk7cmV0dXJuJzxzcGFuIGNsYXNzPVwiJytjbGFzc05hbWUrJ1wiPicreWVhcisnPC9zcGFuPic7O30sY2xpY2thYmxlOnRydWV9LG5hdmlnYXRpb246e25leHRFbDonLnN3aXBlci1idXR0b24tbmV4dCcscHJldkVsOicuc3dpcGVyLWJ1dHRvbi1wcmV2Jyx9LGJyZWFrcG9pbnRzOns3Njg6e2RpcmVjdGlvbjonaG9yaXpvbnRhbCcsfX0sb246e2luaXQ6ZnVuY3Rpb24oKXtzbGlkZXNob3cuZGVsYXkoMTAwKS5xdWV1ZShmdW5jdGlvbihuZXh0KXtqUXVlcnkodGhpcykuYWRkQ2xhc3MoJ2FjdGl2ZScpO30pO30sfX0pO30pO30pO2VsZW1lbnRvckZyb250ZW5kLmhvb2tzLmFkZEFjdGlvbignZnJvbnRlbmQvZWxlbWVudF9yZWFkeS9ncmFuZHJlc3RhdXJhbnQtc2xpZGVyLXBhcmFsbGF4LmRlZmF1bHQnLGZ1bmN0aW9uKCRzY29wZSl7dmFyIHNsaWRlc2hvdz1qUXVlcnkoJy5zbGlkZXItcGFyYWxsYXgtd3JhcHBlcicpO3ZhciB0aW1lcj1zbGlkZXNob3cuYXR0cignZGF0YS1hdXRvcGxheScpO3ZhciBhdXRvcGxheT10cnVlO2lmKHRpbWVyPT0wKVxue3RpbWVyPWZhbHNlO2F1dG9wbGF5PWZhbHNlO31cbnZhciBwYWdpbmF0aW9uPXNsaWRlc2hvdy5hdHRyKCdkYXRhLXBhZ2luYXRpb24nKTtpZihwYWdpbmF0aW9uPT0wKVxue3ZhciBwYWdpbmF0aW9uPWZhbHNlO31cbmVsc2Vcbnt2YXIgcGFnaW5hdGlvbj10cnVlO31cbnZhciBuYXZpZ2F0aW9uPXNsaWRlc2hvdy5hdHRyKCdkYXRhLW5hdmlnYXRpb24nKTtpZihuYXZpZ2F0aW9uPT0wKVxue3ZhciBuYXZpZ2F0aW9uPWZhbHNlO31cbmVsc2Vcbnt2YXIgbmF2aWdhdGlvbj10cnVlO31cbnZhciBzbGlkZXNob3dEdXJhdGlvbj10aW1lcjtmdW5jdGlvbiBzbGlkZXNob3dTd2l0Y2goc2xpZGVzaG93LGluZGV4LGF1dG8pe2lmKHNsaWRlc2hvdy5kYXRhKCd3YWl0JykpcmV0dXJuO3ZhciBzbGlkZXM9c2xpZGVzaG93LmZpbmQoJy5zbGlkZScpO3ZhciBwYWdlcz1zbGlkZXNob3cuZmluZCgnLnBhZ2luYXRpb24nKTt2YXIgYWN0aXZlU2xpZGU9c2xpZGVzLmZpbHRlcignLmlzLWFjdGl2ZScpO3ZhciBhY3RpdmVTbGlkZUltYWdlPWFjdGl2ZVNsaWRlLmZpbmQoJy5pbWFnZS1jb250YWluZXInKTt2YXIgbmV3U2xpZGU9c2xpZGVzLmVxKGluZGV4KTt2YXIgbmV3U2xpZGVJbWFnZT1uZXdTbGlkZS5maW5kKCcuaW1hZ2UtY29udGFpbmVyJyk7dmFyIG5ld1NsaWRlQ29udGVudD1uZXdTbGlkZS5maW5kKCcuc2xpZGUtY29udGVudCcpO3ZhciBuZXdTbGlkZUVsZW1lbnRzPW5ld1NsaWRlLmZpbmQoJy5jYXB0aW9uID4gKicpO2lmKG5ld1NsaWRlLmlzKGFjdGl2ZVNsaWRlKSlyZXR1cm47bmV3U2xpZGUuYWRkQ2xhc3MoJ2lzLW5ldycpO3ZhciB0aW1lb3V0PXNsaWRlc2hvdy5kYXRhKCd0aW1lb3V0Jyk7Y2xlYXJUaW1lb3V0KHRpbWVvdXQpO3NsaWRlc2hvdy5kYXRhKCd3YWl0Jyx0cnVlKTt2YXIgdHJhbnNpdGlvbj1zbGlkZXNob3cuYXR0cignZGF0YS10cmFuc2l0aW9uJyk7aWYodHJhbnNpdGlvbj09J2ZhZGUnKXtuZXdTbGlkZS5jc3Moe2Rpc3BsYXk6J2Jsb2NrJyx6SW5kZXg6Mn0pO25ld1NsaWRlSW1hZ2UuY3NzKHtvcGFjaXR5OjB9KTtUd2Vlbk1heC50byhuZXdTbGlkZUltYWdlLDEse2FscGhhOjEsb25Db21wbGV0ZTpmdW5jdGlvbigpe25ld1NsaWRlLmFkZENsYXNzKCdpcy1hY3RpdmUnKS5yZW1vdmVDbGFzcygnaXMtbmV3Jyk7YWN0aXZlU2xpZGUucmVtb3ZlQ2xhc3MoJ2lzLWFjdGl2ZScpO25ld1NsaWRlLmNzcyh7ZGlzcGxheTonJyx6SW5kZXg6Jyd9KTtuZXdTbGlkZUltYWdlLmNzcyh7b3BhY2l0eTonJ30pO3NsaWRlc2hvdy5maW5kKCcucGFnaW5hdGlvbicpLnRyaWdnZXIoJ2NoZWNrJyk7c2xpZGVzaG93LmRhdGEoJ3dhaXQnLGZhbHNlKTtpZihhdXRvKXt0aW1lb3V0PXNldFRpbWVvdXQoZnVuY3Rpb24oKXtzbGlkZXNob3dOZXh0KHNsaWRlc2hvdyxmYWxzZSx0cnVlKTt9LHNsaWRlc2hvd0R1cmF0aW9uKTtzbGlkZXNob3cuZGF0YSgndGltZW91dCcsdGltZW91dCk7fX19KTt9ZWxzZXtpZihuZXdTbGlkZS5pbmRleCgpPmFjdGl2ZVNsaWRlLmluZGV4KCkpe3ZhciBuZXdTbGlkZVJpZ2h0PTA7dmFyIG5ld1NsaWRlTGVmdD0nYXV0byc7dmFyIG5ld1NsaWRlSW1hZ2VSaWdodD0tc2xpZGVzaG93LndpZHRoKCkvODt2YXIgbmV3U2xpZGVJbWFnZUxlZnQ9J2F1dG8nO3ZhciBuZXdTbGlkZUltYWdlVG9SaWdodD0wO3ZhciBuZXdTbGlkZUltYWdlVG9MZWZ0PSdhdXRvJzt2YXIgbmV3U2xpZGVDb250ZW50TGVmdD0nYXV0byc7dmFyIG5ld1NsaWRlQ29udGVudFJpZ2h0PTA7dmFyIGFjdGl2ZVNsaWRlSW1hZ2VMZWZ0PS1zbGlkZXNob3cud2lkdGgoKS80O31lbHNle3ZhciBuZXdTbGlkZVJpZ2h0PScnO3ZhciBuZXdTbGlkZUxlZnQ9MDt2YXIgbmV3U2xpZGVJbWFnZVJpZ2h0PSdhdXRvJzt2YXIgbmV3U2xpZGVJbWFnZUxlZnQ9LXNsaWRlc2hvdy53aWR0aCgpLzg7dmFyIG5ld1NsaWRlSW1hZ2VUb1JpZ2h0PScnO3ZhciBuZXdTbGlkZUltYWdlVG9MZWZ0PTA7dmFyIG5ld1NsaWRlQ29udGVudExlZnQ9MDt2YXIgbmV3U2xpZGVDb250ZW50UmlnaHQ9J2F1dG8nO3ZhciBhY3RpdmVTbGlkZUltYWdlTGVmdD1zbGlkZXNob3cud2lkdGgoKS80O31cbm5ld1NsaWRlLmNzcyh7ZGlzcGxheTonYmxvY2snLHdpZHRoOjAscmlnaHQ6bmV3U2xpZGVSaWdodCxsZWZ0Om5ld1NsaWRlTGVmdCx6SW5kZXg6Mn0pO25ld1NsaWRlSW1hZ2UuY3NzKHt3aWR0aDpzbGlkZXNob3cud2lkdGgoKSxyaWdodDpuZXdTbGlkZUltYWdlUmlnaHQsbGVmdDpuZXdTbGlkZUltYWdlTGVmdH0pO25ld1NsaWRlQ29udGVudC5jc3Moe3dpZHRoOnNsaWRlc2hvdy53aWR0aCgpLGxlZnQ6bmV3U2xpZGVDb250ZW50TGVmdCxyaWdodDpuZXdTbGlkZUNvbnRlbnRSaWdodH0pO2FjdGl2ZVNsaWRlSW1hZ2UuY3NzKHtsZWZ0OjB9KTtUd2Vlbk1heC5zZXQobmV3U2xpZGVFbGVtZW50cyx7eToyMCxmb3JjZTNEOnRydWV9KTtUd2Vlbk1heC50byhhY3RpdmVTbGlkZUltYWdlLDEse2xlZnQ6YWN0aXZlU2xpZGVJbWFnZUxlZnQsZWFzZTpQb3dlcjMuZWFzZUluT3V0fSk7VHdlZW5NYXgudG8obmV3U2xpZGUsMSx7d2lkdGg6c2xpZGVzaG93LndpZHRoKCksZWFzZTpQb3dlcjMuZWFzZUluT3V0fSk7VHdlZW5NYXgudG8obmV3U2xpZGVJbWFnZSwxLHtyaWdodDpuZXdTbGlkZUltYWdlVG9SaWdodCxsZWZ0Om5ld1NsaWRlSW1hZ2VUb0xlZnQsZWFzZTpQb3dlcjMuZWFzZUluT3V0fSk7VHdlZW5NYXguc3RhZ2dlckZyb21UbyhuZXdTbGlkZUVsZW1lbnRzLDAuOCx7YWxwaGE6MCx5OjYwfSx7YWxwaGE6MSx5OjAsZWFzZTpQb3dlcjMuZWFzZU91dCxmb3JjZTNEOnRydWUsZGVsYXk6MC42fSwwLjEsZnVuY3Rpb24oKXtuZXdTbGlkZS5hZGRDbGFzcygnaXMtYWN0aXZlJykucmVtb3ZlQ2xhc3MoJ2lzLW5ldycpO2FjdGl2ZVNsaWRlLnJlbW92ZUNsYXNzKCdpcy1hY3RpdmUnKTtuZXdTbGlkZS5jc3Moe2Rpc3BsYXk6Jycsd2lkdGg6JycsbGVmdDonJyx6SW5kZXg6Jyd9KTtuZXdTbGlkZUltYWdlLmNzcyh7d2lkdGg6JycscmlnaHQ6JycsbGVmdDonJ30pO25ld1NsaWRlQ29udGVudC5jc3Moe3dpZHRoOicnLGxlZnQ6Jyd9KTtuZXdTbGlkZUVsZW1lbnRzLmNzcyh7b3BhY2l0eTonJyx0cmFuc2Zvcm06Jyd9KTthY3RpdmVTbGlkZUltYWdlLmNzcyh7bGVmdDonJ30pO3NsaWRlc2hvdy5maW5kKCcucGFnaW5hdGlvbicpLnRyaWdnZXIoJ2NoZWNrJyk7c2xpZGVzaG93LmRhdGEoJ3dhaXQnLGZhbHNlKTtpZihhdXRvKXt0aW1lb3V0PXNldFRpbWVvdXQoZnVuY3Rpb24oKXtzbGlkZXNob3dOZXh0KHNsaWRlc2hvdyxmYWxzZSx0cnVlKTt9LHNsaWRlc2hvd0R1cmF0aW9uKTtzbGlkZXNob3cuZGF0YSgndGltZW91dCcsdGltZW91dCk7fX0pO319XG5mdW5jdGlvbiBzbGlkZXNob3dOZXh0KHNsaWRlc2hvdyxwcmV2aW91cyxhdXRvKXt2YXIgc2xpZGVzPXNsaWRlc2hvdy5maW5kKCcuc2xpZGUnKTt2YXIgYWN0aXZlU2xpZGU9c2xpZGVzLmZpbHRlcignLmlzLWFjdGl2ZScpO3ZhciBuZXdTbGlkZT1udWxsO2lmKHByZXZpb3VzKXtuZXdTbGlkZT1hY3RpdmVTbGlkZS5wcmV2KCcuc2xpZGUnKTtpZihuZXdTbGlkZS5sZW5ndGg9PT0wKXtuZXdTbGlkZT1zbGlkZXMubGFzdCgpO319ZWxzZXtuZXdTbGlkZT1hY3RpdmVTbGlkZS5uZXh0KCcuc2xpZGUnKTtpZihuZXdTbGlkZS5sZW5ndGg9PTApXG5uZXdTbGlkZT1zbGlkZXMuZmlsdGVyKCcuc2xpZGUnKS5maXJzdCgpO31cbnNsaWRlc2hvd1N3aXRjaChzbGlkZXNob3csbmV3U2xpZGUuaW5kZXgoKSxhdXRvKTt9XG5mdW5jdGlvbiBob21lU2xpZGVzaG93cGFyYWxsYXgoKXt2YXIgc2Nyb2xsVG9wPWpRdWVyeSh3aW5kb3cpLnNjcm9sbFRvcCgpO2lmKHNjcm9sbFRvcD53aW5kb3dIZWlnaHQpcmV0dXJuO3ZhciBpbm5lcj1zbGlkZXNob3cuZmluZCgnLnNsaWRlc2hvdy1pbm5lcicpO3ZhciBuZXdIZWlnaHQ9d2luZG93SGVpZ2h0LShzY3JvbGxUb3AvMik7dmFyIG5ld1RvcD1zY3JvbGxUb3AqMC44O2lubmVyLmNzcyh7dHJhbnNmb3JtOid0cmFuc2xhdGVZKCcrbmV3VG9wKydweCknLGhlaWdodDpuZXdIZWlnaHR9KTt9XG5qUXVlcnkoZG9jdW1lbnQpLnJlYWR5KGZ1bmN0aW9uKCl7alF1ZXJ5KCcuc2xpZGVyLXBhcmFsbGF4LXdyYXBwZXIgLnNsaWRlJykuYWRkQ2xhc3MoJ2lzLWxvYWRlZCcpO2pRdWVyeSgnLnNsaWRlci1wYXJhbGxheC13cmFwcGVyIC5hcnJvd3MgLmFycm93Jykub24oJ2NsaWNrJyxmdW5jdGlvbigpe3NsaWRlc2hvd05leHQoalF1ZXJ5KHRoaXMpLmNsb3Nlc3QoJy5zbGlkZXItcGFyYWxsYXgtd3JhcHBlcicpLGpRdWVyeSh0aGlzKS5oYXNDbGFzcygncHJldicpKTt9KTtqUXVlcnkoJy5zbGlkZXItcGFyYWxsYXgtd3JhcHBlciAucGFnaW5hdGlvbiAuaXRlbScpLm9uKCdjbGljaycsZnVuY3Rpb24oKXtzbGlkZXNob3dTd2l0Y2goalF1ZXJ5KHRoaXMpLmNsb3Nlc3QoJy5zbGlkZXItcGFyYWxsYXgtd3JhcHBlcicpLGpRdWVyeSh0aGlzKS5pbmRleCgpKTt9KTtqUXVlcnkoJy5zbGlkZXItcGFyYWxsYXgtd3JhcHBlciAucGFnaW5hdGlvbicpLm9uKCdjaGVjaycsZnVuY3Rpb24oKXt2YXIgc2xpZGVzaG93PWpRdWVyeSh0aGlzKS5jbG9zZXN0KCcuc2xpZGVyLXBhcmFsbGF4LXdyYXBwZXInKTt2YXIgcGFnZXM9alF1ZXJ5KHRoaXMpLmZpbmQoJy5pdGVtJyk7dmFyIGluZGV4PXNsaWRlc2hvdy5maW5kKCcuc2xpZGVyX3BhcmFsbGF4X3NsaWRlcyAuaXMtYWN0aXZlJykuaW5kZXgoKTtwYWdlcy5yZW1vdmVDbGFzcygnaXMtYWN0aXZlJyk7cGFnZXMuZXEoaW5kZXgpLmFkZENsYXNzKCdpcy1hY3RpdmUnKTt9KTtpZihhdXRvcGxheSlcbnt2YXIgdGltZW91dD1zZXRUaW1lb3V0KGZ1bmN0aW9uKCl7c2xpZGVzaG93TmV4dChzbGlkZXNob3csZmFsc2UsdHJ1ZSk7fSxzbGlkZXNob3dEdXJhdGlvbik7c2xpZGVzaG93LmRhdGEoJ3RpbWVvdXQnLHRpbWVvdXQpO319KTt9KTtlbGVtZW50b3JGcm9udGVuZC5ob29rcy5hZGRBY3Rpb24oJ2Zyb250ZW5kL2VsZW1lbnRfcmVhZHkvZ3JhbmRyZXN0YXVyYW50LWRpc3RvcnRpb24tZ3JpZC5kZWZhdWx0JyxmdW5jdGlvbigkc2NvcGUpe0FycmF5LmZyb20oZG9jdW1lbnQucXVlcnlTZWxlY3RvckFsbCgnLmRpc3RvcnRpb24tZ3JpZC1pdGVtLWltZycpKS5mb3JFYWNoKChlbCk9Pntjb25zdCBpbWdzPUFycmF5LmZyb20oZWwucXVlcnlTZWxlY3RvckFsbCgnaW1nJykpO25ldyBob3ZlckVmZmVjdCh7cGFyZW50OmVsLGludGVuc2l0eTplbC5kYXRhc2V0LmludGVuc2l0eXx8dW5kZWZpbmVkLHNwZWVkSW46ZWwuZGF0YXNldC5zcGVlZGlufHx1bmRlZmluZWQsc3BlZWRPdXQ6ZWwuZGF0YXNldC5zcGVlZG91dHx8dW5kZWZpbmVkLGVhc2luZzplbC5kYXRhc2V0LmVhc2luZ3x8dW5kZWZpbmVkLGhvdmVyOmVsLmRhdGFzZXQuaG92ZXJ8fHVuZGVmaW5lZCxpbWFnZTE6aW1nc1swXS5nZXRBdHRyaWJ1dGUoJ3NyYycpLGltYWdlMjppbWdzWzFdLmdldEF0dHJpYnV0ZSgnc3JjJyksZGlzcGxhY2VtZW50SW1hZ2U6ZWwuZGF0YXNldC5kaXNwbGFjZW1lbnR9KTt9KTt9KTtlbGVtZW50b3JGcm9udGVuZC5ob29rcy5hZGRBY3Rpb24oJ2Zyb250ZW5kL2VsZW1lbnRfcmVhZHkvZ3JhbmRyZXN0YXVyYW50LXNsaWRlci1hbmltYXRlZC5kZWZhdWx0JyxmdW5jdGlvbigkc2NvcGUpe3ZhciBfY3JlYXRlQ2xhc3M9ZnVuY3Rpb24oKXtmdW5jdGlvbiBkZWZpbmVQcm9wZXJ0aWVzKHRhcmdldCxwcm9wcyl7Zm9yKHZhciBpPTA7aTxwcm9wcy5sZW5ndGg7aSsrKXt2YXIgZGVzY3JpcHRvcj1wcm9wc1tpXTtkZXNjcmlwdG9yLmVudW1lcmFibGU9ZGVzY3JpcHRvci5lbnVtZXJhYmxlfHxmYWxzZTtkZXNjcmlwdG9yLmNvbmZpZ3VyYWJsZT10cnVlO2lmKFwidmFsdWVcImluIGRlc2NyaXB0b3IpZGVzY3JpcHRvci53cml0YWJsZT10cnVlO09iamVjdC5kZWZpbmVQcm9wZXJ0eSh0YXJnZXQsZGVzY3JpcHRvci5rZXksZGVzY3JpcHRvcik7fX1yZXR1cm4gZnVuY3Rpb24oQ29uc3RydWN0b3IscHJvdG9Qcm9wcyxzdGF0aWNQcm9wcyl7aWYocHJvdG9Qcm9wcylkZWZpbmVQcm9wZXJ0aWVzKENvbnN0cnVjdG9yLnByb3RvdHlwZSxwcm90b1Byb3BzKTtpZihzdGF0aWNQcm9wcylkZWZpbmVQcm9wZXJ0aWVzKENvbnN0cnVjdG9yLHN0YXRpY1Byb3BzKTtyZXR1cm4gQ29uc3RydWN0b3I7fTt9KCk7ZnVuY3Rpb24gX2NsYXNzQ2FsbENoZWNrKGluc3RhbmNlLENvbnN0cnVjdG9yKXtpZighKGluc3RhbmNlIGluc3RhbmNlb2YgQ29uc3RydWN0b3IpKXt0aHJvdyBuZXcgVHlwZUVycm9yKFwiQ2Fubm90IGNhbGwgYSBjbGFzcyBhcyBhIGZ1bmN0aW9uXCIpO319XG52YXIgJHdpbmRvdz1qUXVlcnkod2luZG93KTt2YXIgJGJvZHk9alF1ZXJ5KCdib2R5Jyk7dmFyIFNsaWRlc2hvdz1mdW5jdGlvbigpe2Z1bmN0aW9uIFNsaWRlc2hvdygpe3ZhciBfdGhpcz10aGlzO3ZhciB1c2VyT3B0aW9ucz1hcmd1bWVudHMubGVuZ3RoPjAmJmFyZ3VtZW50c1swXSE9PXVuZGVmaW5lZD9hcmd1bWVudHNbMF06e307X2NsYXNzQ2FsbENoZWNrKHRoaXMsU2xpZGVzaG93KTt2YXIgZGVmYXVsdE9wdGlvbnM9eyRlbDpqUXVlcnkoJy5hbmltYXRlZC1zbGlkZXItd3JhcHBlcicpLHNob3dBcnJvd3M6ZmFsc2Usc2hvd3BhZ2luYXRpb246dHJ1ZSxkdXJhdGlvbjoxMDAwMCxhdXRvcGxheTp0cnVlfTt2YXIgb3B0aW9ucz1PYmplY3QuYXNzaWduKHt9LGRlZmF1bHRPcHRpb25zLHVzZXJPcHRpb25zKTt0aGlzLiRlbD1vcHRpb25zLiRlbDt0aGlzLm1heFNsaWRlPXRoaXMuJGVsLmZpbmQoalF1ZXJ5KCcuanMtc2xpZGVyLWhvbWUtc2xpZGUnKSkubGVuZ3RoO3RoaXMuc2hvd0Fycm93cz10aGlzLm1heFNsaWRlPjE/b3B0aW9ucy5zaG93QXJyb3dzOmZhbHNlO3RoaXMuc2hvd3BhZ2luYXRpb249b3B0aW9ucy5zaG93cGFnaW5hdGlvbjt0aGlzLmN1cnJlbnRTbGlkZT0xO3RoaXMuaXNBbmltYXRpbmc9ZmFsc2U7dGhpcy5hbmltYXRpb25EdXJhdGlvbj0xMjAwO3RoaXMuYXV0b3BsYXlTcGVlZD1vcHRpb25zLmR1cmF0aW9uO3RoaXMuaW50ZXJ2YWw7dGhpcy4kY29udHJvbHM9dGhpcy4kZWwuZmluZCgnLmpzLXNsaWRlci1ob21lLWJ1dHRvbicpO3RoaXMuYXV0b3BsYXk9dGhpcy5tYXhTbGlkZT4xP29wdGlvbnMuYXV0b3BsYXk6ZmFsc2U7dGhpcy5hdXRvcGxheT1mYWxzZTt0aGlzLiRlbC5vbignY2xpY2snLCcuanMtc2xpZGVyLWhvbWUtbmV4dCcsZnVuY3Rpb24oZXZlbnQpe3JldHVybiBfdGhpcy5uZXh0U2xpZGUoKTt9KTt0aGlzLiRlbC5vbignY2xpY2snLCcuanMtc2xpZGVyLWhvbWUtcHJldicsZnVuY3Rpb24oZXZlbnQpe3JldHVybiBfdGhpcy5wcmV2U2xpZGUoKTt9KTt0aGlzLiRlbC5vbignY2xpY2snLCcuanMtcGFnaW5hdGlvbi1pdGVtJyxmdW5jdGlvbihldmVudCl7aWYoIV90aGlzLmlzQW5pbWF0aW5nKXtfdGhpcy5wcmV2ZW50Q2xpY2soKTtfdGhpcy5nb1RvU2xpZGUoZXZlbnQudGFyZ2V0LmRhdGFzZXQuc2xpZGUpO319KTt0aGlzLmluaXQoKTt9XG5fY3JlYXRlQ2xhc3MoU2xpZGVzaG93LFt7a2V5Oidpbml0Jyx2YWx1ZTpmdW5jdGlvbiBpbml0KCl7dGhpcy5nb1RvU2xpZGUoMSk7aWYodGhpcy5hdXRvcGxheSl7dGhpcy5zdGFydEF1dG9wbGF5KCk7fVxuaWYodGhpcy5zaG93cGFnaW5hdGlvbil7dmFyIHBhZ2luYXRpb25OdW1iZXI9dGhpcy5tYXhTbGlkZTt2YXIgcGFnaW5hdGlvbj0nPGRpdiBjbGFzcz1cInBhZ2luYXRpb25cIj48ZGl2IGNsYXNzPVwiY29udGFpbmVyXCI+Jztmb3IodmFyIGk9MDtpPHRoaXMubWF4U2xpZGU7aSsrKXt2YXIgaXRlbT0nPHNwYW4gY2xhc3M9XCJwYWdpbmF0aW9uLWl0ZW0ganMtcGFnaW5hdGlvbi1pdGVtICcrKGk9PT0wPydpcy1jdXJyZW50JzonJykrJ1wiIGRhdGEtc2xpZGU9JysoaSsxKSsnPicrKGkrMSkrJzwvc3Bhbj4nO3BhZ2luYXRpb249cGFnaW5hdGlvbitpdGVtO31cbnBhZ2luYXRpb249cGFnaW5hdGlvbisnPC9kaXY+PC9kaXY+Jzt0aGlzLiRlbC5hcHBlbmQocGFnaW5hdGlvbik7fX19LHtrZXk6J3ByZXZlbnRDbGljaycsdmFsdWU6ZnVuY3Rpb24gcHJldmVudENsaWNrKCl7dmFyIF90aGlzMj10aGlzO3RoaXMuaXNBbmltYXRpbmc9dHJ1ZTt0aGlzLiRjb250cm9scy5wcm9wKCdkaXNhYmxlZCcsdHJ1ZSk7Y2xlYXJJbnRlcnZhbCh0aGlzLmludGVydmFsKTtzZXRUaW1lb3V0KGZ1bmN0aW9uKCl7X3RoaXMyLmlzQW5pbWF0aW5nPWZhbHNlO190aGlzMi4kY29udHJvbHMucHJvcCgnZGlzYWJsZWQnLGZhbHNlKTtpZihfdGhpczIuYXV0b3BsYXkpe190aGlzMi5zdGFydEF1dG9wbGF5KCk7fX0sdGhpcy5hbmltYXRpb25EdXJhdGlvbik7fX0se2tleTonZ29Ub1NsaWRlJyx2YWx1ZTpmdW5jdGlvbiBnb1RvU2xpZGUoaW5kZXgpe3RoaXMuY3VycmVudFNsaWRlPXBhcnNlSW50KGluZGV4KTtpZih0aGlzLmN1cnJlbnRTbGlkZT50aGlzLm1heFNsaWRlKXt0aGlzLmN1cnJlbnRTbGlkZT0xO31cbmlmKHRoaXMuY3VycmVudFNsaWRlPT09MCl7dGhpcy5jdXJyZW50U2xpZGU9dGhpcy5tYXhTbGlkZTt9XG52YXIgbmV3Q3VycmVudD10aGlzLiRlbC5maW5kKCcuanMtc2xpZGVyLWhvbWUtc2xpZGVbZGF0YS1zbGlkZT1cIicrdGhpcy5jdXJyZW50U2xpZGUrJ1wiXScpO3ZhciBuZXdwcmV2PXRoaXMuY3VycmVudFNsaWRlPT09MT90aGlzLiRlbC5maW5kKCcuanMtc2xpZGVyLWhvbWUtc2xpZGUnKS5sYXN0KCk6bmV3Q3VycmVudC5wcmV2KCcuanMtc2xpZGVyLWhvbWUtc2xpZGUnKTt2YXIgbmV3TmV4dD10aGlzLmN1cnJlbnRTbGlkZT09PXRoaXMubWF4U2xpZGU/dGhpcy4kZWwuZmluZCgnLmpzLXNsaWRlci1ob21lLXNsaWRlJykuZmlyc3QoKTpuZXdDdXJyZW50Lm5leHQoJy5qcy1zbGlkZXItaG9tZS1zbGlkZScpO3RoaXMuJGVsLmZpbmQoJy5qcy1zbGlkZXItaG9tZS1zbGlkZScpLnJlbW92ZUNsYXNzKCdpcy1wcmV2IGlzLW5leHQgaXMtY3VycmVudCcpO3RoaXMuJGVsLmZpbmQoJy5qcy1wYWdpbmF0aW9uLWl0ZW0nKS5yZW1vdmVDbGFzcygnaXMtY3VycmVudCcpO2lmKHRoaXMubWF4U2xpZGU+MSl7bmV3cHJldi5hZGRDbGFzcygnaXMtcHJldicpO25ld05leHQuYWRkQ2xhc3MoJ2lzLW5leHQnKTt9XG5uZXdDdXJyZW50LmFkZENsYXNzKCdpcy1jdXJyZW50Jyk7dGhpcy4kZWwuZmluZCgnLmpzLXBhZ2luYXRpb24taXRlbVtkYXRhLXNsaWRlPVwiJyt0aGlzLmN1cnJlbnRTbGlkZSsnXCJdJykuYWRkQ2xhc3MoJ2lzLWN1cnJlbnQnKTt9fSx7a2V5OiduZXh0U2xpZGUnLHZhbHVlOmZ1bmN0aW9uIG5leHRTbGlkZSgpe3RoaXMucHJldmVudENsaWNrKCk7dGhpcy5nb1RvU2xpZGUodGhpcy5jdXJyZW50U2xpZGUrMSk7fX0se2tleToncHJldlNsaWRlJyx2YWx1ZTpmdW5jdGlvbiBwcmV2U2xpZGUoKXt0aGlzLnByZXZlbnRDbGljaygpO3RoaXMuZ29Ub1NsaWRlKHRoaXMuY3VycmVudFNsaWRlLTEpO319LHtrZXk6J3N0YXJ0QXV0b3BsYXknLHZhbHVlOmZ1bmN0aW9uIHN0YXJ0QXV0b3BsYXkoKXt2YXIgX3RoaXMzPXRoaXM7dGhpcy5pbnRlcnZhbD1zZXRJbnRlcnZhbChmdW5jdGlvbigpe2lmKCFfdGhpczMuaXNBbmltYXRpbmcpe190aGlzMy5uZXh0U2xpZGUoKTt9fSx0aGlzLmF1dG9wbGF5U3BlZWQpO319LHtrZXk6J2Rlc3Ryb3knLHZhbHVlOmZ1bmN0aW9uIGRlc3Ryb3koKXt0aGlzLiRlbC5vZmYoKTt9fV0pO3JldHVybiBTbGlkZXNob3c7fSgpOyhmdW5jdGlvbigpe3ZhciBsb2FkZWQ9ZmFsc2U7dmFyIG1heExvYWQ9MzAwMDtmdW5jdGlvbiBsb2FkKCl7dmFyIG9wdGlvbnM9e3Nob3dwYWdpbmF0aW9uOnRydWV9O3ZhciBzbGlkZVNob3c9bmV3IFNsaWRlc2hvdyhvcHRpb25zKTt9XG5mdW5jdGlvbiBhZGRMb2FkQ2xhc3MoKXskYm9keS5hZGRDbGFzcygnaXMtbG9hZGVkJyk7c2V0VGltZW91dChmdW5jdGlvbigpeyRib2R5LmFkZENsYXNzKCdpcy1hbmltYXRlZCcpO30sNjAwKTt9XG4kd2luZG93Lm9uKCdsb2FkJyxmdW5jdGlvbigpe2lmKCFsb2FkZWQpe2xvYWRlZD10cnVlO2xvYWQoKTt9fSk7c2V0VGltZW91dChmdW5jdGlvbigpe2lmKCFsb2FkZWQpe2xvYWRlZD10cnVlO2xvYWQoKTt9fSxtYXhMb2FkKTthZGRMb2FkQ2xhc3MoKTt9KSgpO30pO2VsZW1lbnRvckZyb250ZW5kLmhvb2tzLmFkZEFjdGlvbignZnJvbnRlbmQvZWxlbWVudF9yZWFkeS9ncmFuZHJlc3RhdXJhbnQtc2xpZGVyLWZhZGV1cC5kZWZhdWx0JyxmdW5jdGlvbigkc2NvcGUpe2Z1bmN0aW9uIGluaXQoaXRlbSl7dmFyIGl0ZW1zPWl0ZW0ucXVlcnlTZWxlY3RvckFsbCgnbGknKSxjdXJyZW50PTAsYXV0b1VwZGF0ZT10cnVlO3ZhciBuYXY9ZG9jdW1lbnQuY3JlYXRlRWxlbWVudCgnbmF2Jyk7bmF2LmNsYXNzTmFtZT0nbmF2X2Fycm93cyc7dmFyIHByZXZidG49ZG9jdW1lbnQuY3JlYXRlRWxlbWVudCgnYnV0dG9uJyk7cHJldmJ0bi5jbGFzc05hbWU9J3ByZXYnO3ByZXZidG4uc2V0QXR0cmlidXRlKCdhcmlhLWxhYmVsJywnUHJldicpO3ZhciBuZXh0YnRuPWRvY3VtZW50LmNyZWF0ZUVsZW1lbnQoJ2J1dHRvbicpO25leHRidG4uY2xhc3NOYW1lPSduZXh0JztuZXh0YnRuLnNldEF0dHJpYnV0ZSgnYXJpYS1sYWJlbCcsJ05leHQnKTt2YXIgY291bnRlcj1kb2N1bWVudC5jcmVhdGVFbGVtZW50KCdkaXYnKTtjb3VudGVyLmNsYXNzTmFtZT0nY291bnRlcic7Y291bnRlci5pbm5lckhUTUw9XCI8c3Bhbj4xPC9zcGFuPjxzcGFuPlwiK2l0ZW1zLmxlbmd0aCtcIjwvc3Bhbj5cIjtpZihpdGVtcy5sZW5ndGg+MSl7bmF2LmFwcGVuZENoaWxkKHByZXZidG4pO25hdi5hcHBlbmRDaGlsZChjb3VudGVyKTtuYXYuYXBwZW5kQ2hpbGQobmV4dGJ0bik7aXRlbS5hcHBlbmRDaGlsZChuYXYpO31cbml0ZW1zW2N1cnJlbnRdLmNsYXNzTmFtZT1cImN1cnJlbnRcIjtpZihpdGVtcy5sZW5ndGg+MSlpdGVtc1tpdGVtcy5sZW5ndGgtMV0uY2xhc3NOYW1lPVwicHJldi1zbGlkZVwiO3ZhciBuYXZpZ2F0ZT1mdW5jdGlvbihkaXIpe2l0ZW1zW2N1cnJlbnRdLmNsYXNzTmFtZT1cIlwiO2lmKGRpcj09PSdyaWdodCcpe2N1cnJlbnQ9Y3VycmVudDxpdGVtcy5sZW5ndGgtMT9jdXJyZW50KzE6MDt9ZWxzZXtjdXJyZW50PWN1cnJlbnQ+MD9jdXJyZW50LTE6aXRlbXMubGVuZ3RoLTE7fVxudmFyIG5leHRDdXJyZW50PWN1cnJlbnQ8aXRlbXMubGVuZ3RoLTE/Y3VycmVudCsxOjAscHJldkN1cnJlbnQ9Y3VycmVudD4wP2N1cnJlbnQtMTppdGVtcy5sZW5ndGgtMTtpdGVtc1tjdXJyZW50XS5jbGFzc05hbWU9XCJjdXJyZW50XCI7aXRlbXNbcHJldkN1cnJlbnRdLmNsYXNzTmFtZT1cInByZXYtc2xpZGVcIjtpdGVtc1tuZXh0Q3VycmVudF0uY2xhc3NOYW1lPVwiXCI7Y291bnRlci5maXJzdENoaWxkLnRleHRDb250ZW50PWN1cnJlbnQrMTt9XG5pdGVtLmFkZEV2ZW50TGlzdGVuZXIoJ21vdXNlZW50ZXInLGZ1bmN0aW9uKCl7YXV0b1VwZGF0ZT1mYWxzZTt9KTtpdGVtLmFkZEV2ZW50TGlzdGVuZXIoJ21vdXNlbGVhdmUnLGZ1bmN0aW9uKCl7YXV0b1VwZGF0ZT10cnVlO30pO3ByZXZidG4uYWRkRXZlbnRMaXN0ZW5lcignY2xpY2snLGZ1bmN0aW9uKCl7bmF2aWdhdGUoJ2xlZnQnKTt9KTtuZXh0YnRuLmFkZEV2ZW50TGlzdGVuZXIoJ2NsaWNrJyxmdW5jdGlvbigpe25hdmlnYXRlKCdyaWdodCcpO30pO2RvY3VtZW50LmFkZEV2ZW50TGlzdGVuZXIoJ2tleWRvd24nLGZ1bmN0aW9uKGV2KXt2YXIga2V5Q29kZT1ldi5rZXlDb2RlfHxldi53aGljaDtzd2l0Y2goa2V5Q29kZSl7Y2FzZSAzNzpuYXZpZ2F0ZSgnbGVmdCcpO2JyZWFrO2Nhc2UgMzk6bmF2aWdhdGUoJ3JpZ2h0Jyk7YnJlYWs7fX0pO2l0ZW0uYWRkRXZlbnRMaXN0ZW5lcigndG91Y2hzdGFydCcsaGFuZGxlVG91Y2hTdGFydCxmYWxzZSk7aXRlbS5hZGRFdmVudExpc3RlbmVyKCd0b3VjaG1vdmUnLGhhbmRsZVRvdWNoTW92ZSxmYWxzZSk7dmFyIHhEb3duPW51bGw7dmFyIHlEb3duPW51bGw7ZnVuY3Rpb24gaGFuZGxlVG91Y2hTdGFydChldnQpe3hEb3duPWV2dC50b3VjaGVzWzBdLmNsaWVudFg7eURvd249ZXZ0LnRvdWNoZXNbMF0uY2xpZW50WTt9O2Z1bmN0aW9uIGhhbmRsZVRvdWNoTW92ZShldnQpe2lmKCF4RG93bnx8IXlEb3duKXtyZXR1cm47fVxudmFyIHhVcD1ldnQudG91Y2hlc1swXS5jbGllbnRYO3ZhciB5VXA9ZXZ0LnRvdWNoZXNbMF0uY2xpZW50WTt2YXIgeERpZmY9eERvd24teFVwO3ZhciB5RGlmZj15RG93bi15VXA7aWYoTWF0aC5hYnMoeERpZmYpPk1hdGguYWJzKHlEaWZmKSl7aWYoeERpZmY+MCl7bmF2aWdhdGUoJ3JpZ2h0Jyk7fWVsc2V7bmF2aWdhdGUoJ2xlZnQnKTt9fVxueERvd249bnVsbDt5RG93bj1udWxsO307fVxuW10uc2xpY2UuY2FsbChkb2N1bWVudC5xdWVyeVNlbGVjdG9yQWxsKCcuZmFkZXVwLXNsaWRlci13cmFwcGVyJykpLmZvckVhY2goZnVuY3Rpb24oaXRlbSl7aW5pdChpdGVtKTt9KTt9KTtlbGVtZW50b3JGcm9udGVuZC5ob29rcy5hZGRBY3Rpb24oJ2Zyb250ZW5kL2VsZW1lbnRfcmVhZHkvZ3JhbmRyZXN0YXVyYW50LXNsaWRlci1tb3Rpb24tcmV2ZWFsLmRlZmF1bHQnLGZ1bmN0aW9uKCRzY29wZSl7Y2xhc3MgU2xpZGV7Y29uc3RydWN0b3IoZWwpe3RoaXMuRE9NPXtlbDplbH07dGhpcy5ET00uaW1nV3JhcD10aGlzLkRPTS5lbC5xdWVyeVNlbGVjdG9yKCcuc2xpZGUtaW1nLXdyYXAnKTt0aGlzLkRPTS5yZXZlYWxlcj10aGlzLkRPTS5pbWdXcmFwLnF1ZXJ5U2VsZWN0b3IoJy5zbGlkZS1pbWctcmV2ZWFsJyk7dGhpcy5ET00udGl0bGU9dGhpcy5ET00uZWwucXVlcnlTZWxlY3RvcignLnNsaWRlLXRpdGxlJyk7dGhpcy5ET00ubnVtYmVyPXRoaXMuRE9NLmVsLnF1ZXJ5U2VsZWN0b3IoJy5zbGlkZS1udW1iZXInKTt0aGlzLkRPTS5wcmV2aWV3PXtpbWdXcmFwOnRoaXMuRE9NLmVsLnF1ZXJ5U2VsZWN0b3IoJy5wcmV2aWV3LWltZy13cmFwJykscmV2ZWFsZXI6dGhpcy5ET00uZWwucXVlcnlTZWxlY3RvcignLnByZXZpZXctaW1nLXdyYXAgPiAucHJldmlldy1pbWctcmV2ZWFsJyksdGl0bGU6dGhpcy5ET00uZWwucXVlcnlTZWxlY3RvcignLnByZXZpZXctdGl0bGUnKSxjb250ZW50OnRoaXMuRE9NLmVsLnF1ZXJ5U2VsZWN0b3IoJy5wcmV2aWV3LWNvbnRlbnQnKX07dGhpcy5jb25maWc9e2FuaW1hdGlvbjp7ZHVyYXRpb246MC42LGVhc2U6RXhwby5lYXNlT3V0fX07fVxuc2V0Q3VycmVudChpc0N1cnJlbnQ9dHJ1ZSl7dGhpcy5ET00uZWwuY2xhc3NMaXN0W2lzQ3VycmVudD8nYWRkJzoncmVtb3ZlJ10oJ3NsaWRlLWN1cnJlbnQnKTt9XG5oaWRlKGRpcmVjdGlvbil7cmV0dXJuIHRoaXMudG9nZ2xlKCdoaWRlJyxkaXJlY3Rpb24pO31cbnNob3coZGlyZWN0aW9uKXtyZXR1cm4gdGhpcy50b2dnbGUoJ3Nob3cnLGRpcmVjdGlvbik7fVxudG9nZ2xlKGFjdGlvbixkaXJlY3Rpb24pe3JldHVybiBuZXcgUHJvbWlzZSgocmVzb2x2ZSxyZWplY3QpPT57bGV0IHJldmVhbGVyT3B0cz17ZGVsYXk6YWN0aW9uPT09J2hpZGUnPzA6dGhpcy5jb25maWcuYW5pbWF0aW9uLmR1cmF0aW9uLzIsZWFzZTp0aGlzLmNvbmZpZy5hbmltYXRpb24uZWFzZSxvbkNvbXBsZXRlOnJlc29sdmV9O2NvbnN0IGNvbW1vbk9wdHM9e2RlbGF5OmFjdGlvbj09PSdoaWRlJz8wOnRoaXMuY29uZmlnLmFuaW1hdGlvbi5kdXJhdGlvbi8yLGVhc2U6dGhpcy5jb25maWcuYW5pbWF0aW9uLmVhc2Usb3BhY2l0eTphY3Rpb249PT0naGlkZSc/MDoxfTtsZXQgaW1nT3B0cz1PYmplY3QuYXNzaWduKHt9LGNvbW1vbk9wdHMpO2xldCBudW1iZXJPcHRzPU9iamVjdC5hc3NpZ24oe30sY29tbW9uT3B0cyk7bGV0IHRpdGxlT3B0cz1PYmplY3QuYXNzaWduKHt9LGNvbW1vbk9wdHMpO2lmKGRpcmVjdGlvbj09PSdsZWZ0J3x8ZGlyZWN0aW9uPT09J3JpZ2h0Jyl7cmV2ZWFsZXJPcHRzLnN0YXJ0QXQ9YWN0aW9uPT09J2hpZGUnP3t4OmRpcmVjdGlvbj09PSdsZWZ0Jz8nLTEwMCUnOicxMDAlJyx5OicwJSd9Ont4OicwJScseTonMCUnfTtyZXZlYWxlck9wdHMueD1hY3Rpb249PT0naGlkZSc/JzAlJzpkaXJlY3Rpb249PT0nbGVmdCc/JzEwMCUnOictMTAwJSc7aW1nT3B0cy5zdGFydEF0PWFjdGlvbj09PSdzaG93Jz97b3BhY2l0eTowLHg6ZGlyZWN0aW9uPT09J2xlZnQnPyctMjAlJzonMjAlJ306e307aW1nT3B0cy54PWFjdGlvbj09PSdoaWRlJz9kaXJlY3Rpb249PT0nbGVmdCc/JzIwJSc6Jy0yMCUnOicwJSc7dGl0bGVPcHRzLnN0YXJ0QXQ9YWN0aW9uPT09J3Nob3cnP3tvcGFjaXR5OjEsc2NhbGU6MC4yLHg6ZGlyZWN0aW9uPT09J2xlZnQnPyctMjAwJSc6JzIwMCUnfTp7fTt0aXRsZU9wdHMueD1hY3Rpb249PT0naGlkZSc/ZGlyZWN0aW9uPT09J2xlZnQnPycyMDAlJzonLTIwMCUnOicwJSc7dGl0bGVPcHRzLnNjYWxlPWFjdGlvbj09PSdoaWRlJz8wLjI6MTtudW1iZXJPcHRzLnN0YXJ0QXQ9YWN0aW9uPT09J3Nob3cnP3tvcGFjaXR5OjEseDpkaXJlY3Rpb249PT0nbGVmdCc/Jy01MCUnOic1MCUnfTp7fTtudW1iZXJPcHRzLng9YWN0aW9uPT09J2hpZGUnP2RpcmVjdGlvbj09PSdsZWZ0Jz8nNTAlJzonLTUwJSc6JzAlJzt9XG5lbHNle3JldmVhbGVyT3B0cy5zdGFydEF0PWFjdGlvbj09PSdoaWRlJz97eDonMCUnLHk6ZGlyZWN0aW9uPT09J2Rvd24nPyctMTAwJSc6JzEwMCUnfTp7eDonMCUnLHk6JzAlJ307cmV2ZWFsZXJPcHRzLnk9YWN0aW9uPT09J2hpZGUnPycwJSc6ZGlyZWN0aW9uPT09J2Rvd24nPycxMDAlJzonLTEwMCUnO2ltZ09wdHMuc3RhcnRBdD1hY3Rpb249PT0nc2hvdyc/e29wYWNpdHk6MSx5OmRpcmVjdGlvbj09PSdkb3duJz8nLTEwJSc6JzEwJSd9Ont9O2ltZ09wdHMueT1hY3Rpb249PT0naGlkZSc/ZGlyZWN0aW9uPT09J2Rvd24nPycxMCUnOictMTAlJzonMCUnO3RpdGxlT3B0cy5lYXNlPXRoaXMuY29uZmlnLmFuaW1hdGlvbi5lYXNlLHRpdGxlT3B0cy5zdGFydEF0PWFjdGlvbj09PSdzaG93Jz97b3BhY2l0eToxLHk6ZGlyZWN0aW9uPT09J2Rvd24nPyctMTAwJSc6JzEwMCUnfTp7fTt0aXRsZU9wdHMueT1hY3Rpb249PT0naGlkZSc/ZGlyZWN0aW9uPT09J2Rvd24nPycxMDAlJzonLTEwMCUnOicwJSc7fVxuVHdlZW5NYXgudG8odGhpcy5ET00ucmV2ZWFsZXIsdGhpcy5jb25maWcuYW5pbWF0aW9uLmR1cmF0aW9uLHJldmVhbGVyT3B0cyk7VHdlZW5NYXgudG8odGhpcy5ET00uaW1nV3JhcCx0aGlzLmNvbmZpZy5hbmltYXRpb24uZHVyYXRpb24saW1nT3B0cyk7VHdlZW5NYXgudG8odGhpcy5ET00udGl0bGUsdGhpcy5jb25maWcuYW5pbWF0aW9uLmR1cmF0aW9uKjEuNSx0aXRsZU9wdHMpO1R3ZWVuTWF4LnRvKHRoaXMuRE9NLm51bWJlcix0aGlzLmNvbmZpZy5hbmltYXRpb24uZHVyYXRpb24sbnVtYmVyT3B0cyk7fSk7fVxuaGlkZVByZXZpZXcoZGVsYXkpe3JldHVybiB0aGlzLnRvZ2dsZVByZXZpZXcoJ2hpZGUnKTt9XG5zaG93cHJldmlldyhkZWxheSl7cmV0dXJuIHRoaXMudG9nZ2xlUHJldmlldygnc2hvdycpO31cbnRvZ2dsZVByZXZpZXcoYWN0aW9uKXtyZXR1cm4gbmV3IFByb21pc2UoKHJlc29sdmUscmVqZWN0KT0+e1R3ZWVuTWF4LnRvKHRoaXMuRE9NLnByZXZpZXcucmV2ZWFsZXIsdGhpcy5jb25maWcuYW5pbWF0aW9uLmR1cmF0aW9uLHtkZWxheTphY3Rpb249PT0naGlkZSc/MDp0aGlzLmNvbmZpZy5hbmltYXRpb24uZHVyYXRpb24vMixlYXNlOnRoaXMuY29uZmlnLmFuaW1hdGlvbi5lYXNlLHN0YXJ0QXQ6YWN0aW9uPT09J2hpZGUnP3t4OicwJScseTonLTEwMCUnfTp7eDonMCUnLHk6JzAlJ30seTphY3Rpb249PT0naGlkZSc/JzAlJzonLTEwMCUnLG9uQ29tcGxldGU6cmVzb2x2ZX0pO1R3ZWVuTWF4LnRvKHRoaXMuRE9NLnByZXZpZXcuaW1nV3JhcCx0aGlzLmNvbmZpZy5hbmltYXRpb24uZHVyYXRpb24se2RlbGF5OmFjdGlvbj09PSdoaWRlJz8wOnRoaXMuY29uZmlnLmFuaW1hdGlvbi5kdXJhdGlvbi8yLGVhc2U6dGhpcy5jb25maWcuYW5pbWF0aW9uLmVhc2Usc3RhcnRBdDphY3Rpb249PT0naGlkZSc/e306e29wYWNpdHk6MCx5OicyMCUnfSx5OmFjdGlvbj09PSdoaWRlJz8nMjAlJzonMCUnLG9wYWNpdHk6YWN0aW9uPT09J2hpZGUnPzA6MX0pO1R3ZWVuTWF4LnRvKFt0aGlzLkRPTS5wcmV2aWV3LnRpdGxlLHRoaXMuRE9NLnByZXZpZXcuY29udGVudF0sdGhpcy5jb25maWcuYW5pbWF0aW9uLmR1cmF0aW9uLHtkZWxheTphY3Rpb249PT0naGlkZSc/MDp0aGlzLmNvbmZpZy5hbmltYXRpb24uZHVyYXRpb24vMixlYXNlOnRoaXMuY29uZmlnLmFuaW1hdGlvbi5lYXNlLHN0YXJ0QXQ6YWN0aW9uPT09J2hpZGUnP3t9OntvcGFjaXR5OjAseTonMjAwJSd9LHk6YWN0aW9uPT09J2hpZGUnPycyMDAlJzonMCUnLG9wYWNpdHk6YWN0aW9uPT09J2hpZGUnPzA6MX0pO30pO319XG5jbGFzcyBTbGlkZXNob3d7Y29uc3RydWN0b3IoZWwpe3RoaXMuRE9NPXtlbDplbH07dGhpcy5ET00ucHJldkN0cmw9dGhpcy5ET00uZWwucXVlcnlTZWxlY3RvcignLnNsaWRlbmF2LWl0ZW0tLXByZXYnKTt0aGlzLkRPTS5uZXh0Q3RybD10aGlzLkRPTS5lbC5xdWVyeVNlbGVjdG9yKCcuc2xpZGVuYXYtaXRlbS0tbmV4dCcpO3RoaXMuRE9NLnByZXZpZXdDdHJsPXRoaXMuRE9NLmVsLnF1ZXJ5U2VsZWN0b3IoJy5zbGlkZW5hdi1wcmV2aWV3Jyk7dGhpcy5zbGlkZXM9W107QXJyYXkuZnJvbSh0aGlzLkRPTS5lbC5xdWVyeVNlbGVjdG9yQWxsKCcuc2xpZGUnKSkuZm9yRWFjaChzbGlkZUVsPT50aGlzLnNsaWRlcy5wdXNoKG5ldyBTbGlkZShzbGlkZUVsKSkpO3RoaXMuc2xpZGVzVG90YWw9dGhpcy5zbGlkZXMubGVuZ3RoO3RoaXMuY3VycmVudD0wO3RoaXMuaW5pdCgpO31cbmluaXQoKXt0aGlzLnNsaWRlc1t0aGlzLmN1cnJlbnRdLnNldEN1cnJlbnQoKTt0aGlzLmluaXRFdmVudHMoKTt9XG5pbml0RXZlbnRzKCl7dGhpcy5ET00ucHJldkN0cmwuYWRkRXZlbnRMaXN0ZW5lcignY2xpY2snLCgpPT50aGlzLnByZXYoKSk7dGhpcy5ET00ubmV4dEN0cmwuYWRkRXZlbnRMaXN0ZW5lcignY2xpY2snLCgpPT50aGlzLm5leHQoKSk7dGhpcy5ET00ucHJldmlld0N0cmwuYWRkRXZlbnRMaXN0ZW5lcignY2xpY2snLChldik9PntpZih0aGlzLmlzQW5pbWF0aW5nKXJldHVybjtpZihldi50YXJnZXQuY2xhc3NMaXN0LmNvbnRhaW5zKCdzbGlkZW5hdi1wcmV2aWV3LS1vcGVuJykpe2V2LnRhcmdldC5jbGFzc0xpc3QucmVtb3ZlKCdzbGlkZW5hdi1wcmV2aWV3LS1vcGVuJyk7dGhpcy5leGl0UHJldmlldygpO31cbmVsc2V7ZXYudGFyZ2V0LmNsYXNzTGlzdC5hZGQoJ3NsaWRlbmF2LXByZXZpZXctLW9wZW4nKVxudGhpcy5lbnRlclByZXZpZXcoKTt9fSk7fVxucHJldigpe3RoaXMubmF2aWdhdGUoJ2xlZnQnKTt9XG5uZXh0KCl7dGhpcy5uYXZpZ2F0ZSgncmlnaHQnKTt9XG5lbnRlclByZXZpZXcoKXt0aGlzLnRvZ2dsZVByZXZpZXcoJ2VudGVyJyk7fVxuZXhpdFByZXZpZXcoKXt0aGlzLnRvZ2dsZVByZXZpZXcoJ2V4aXQnKTt9XG50b2dnbGVQcmV2aWV3KGFjdGlvbil7aWYodGhpcy5pc0FuaW1hdGluZylyZXR1cm47dGhpcy5pc0FuaW1hdGluZz10cnVlO2NvbnN0IHByb2Nlc3Npbmc9YWN0aW9uPT09J2VudGVyJz9bdGhpcy5zbGlkZXNbdGhpcy5jdXJyZW50XS5oaWRlKCd1cCcpLHRoaXMuc2xpZGVzW3RoaXMuY3VycmVudF0uc2hvd3ByZXZpZXcoKV06W3RoaXMuc2xpZGVzW3RoaXMuY3VycmVudF0uc2hvdygnZG93bicpLHRoaXMuc2xpZGVzW3RoaXMuY3VycmVudF0uaGlkZVByZXZpZXcoKV07dGhpcy50b2dnbGVOYXZDdHJscyhhY3Rpb24pO1Byb21pc2UuYWxsKHByb2Nlc3NpbmcpLnRoZW4oKCk9PnRoaXMuaXNBbmltYXRpbmc9ZmFsc2UpO31cbnRvZ2dsZU5hdkN0cmxzKGFjdGlvbil7VHdlZW5NYXgudG8oW3RoaXMuRE9NLnByZXZDdHJsLHRoaXMuRE9NLm5leHRDdHJsXSwwLjUse2Vhc2U6J0V4cG8uZWFzZU91dCcsb3BhY2l0eTphY3Rpb249PT0nZW50ZXInPzA6MSxvblN0YXJ0OigpPT50aGlzLkRPTS5wcmV2Q3RybC5zdHlsZS5wb2ludGVyRXZlbnRzPXRoaXMuRE9NLm5leHRDdHJsLnN0eWxlLnBvaW50ZXJFdmVudHM9YWN0aW9uPT09J2VudGVyJz8nbm9uZSc6J2F1dG8nfSk7fVxubmF2aWdhdGUoZGlyZWN0aW9uKXtpZih0aGlzLmlzQW5pbWF0aW5nKXJldHVybjt0aGlzLmlzQW5pbWF0aW5nPXRydWU7Y29uc3QgbmV4dFNsaWRlUG9zPWRpcmVjdGlvbj09PSdyaWdodCc/dGhpcy5jdXJyZW50PHRoaXMuc2xpZGVzVG90YWwtMT90aGlzLmN1cnJlbnQrMTowOnRoaXMuY3VycmVudD4wP3RoaXMuY3VycmVudC0xOnRoaXMuc2xpZGVzVG90YWwtMTtQcm9taXNlLmFsbChbdGhpcy5zbGlkZXNbdGhpcy5jdXJyZW50XS5oaWRlKGRpcmVjdGlvbiksdGhpcy5zbGlkZXNbbmV4dFNsaWRlUG9zXS5zaG93KGRpcmVjdGlvbildKS50aGVuKCgpPT57dGhpcy5zbGlkZXNbdGhpcy5jdXJyZW50XS5zZXRDdXJyZW50KGZhbHNlKTt0aGlzLmN1cnJlbnQ9bmV4dFNsaWRlUG9zO3RoaXMuaXNBbmltYXRpbmc9ZmFsc2U7dGhpcy5zbGlkZXNbdGhpcy5jdXJyZW50XS5zZXRDdXJyZW50KCk7fSk7fX1cbmNvbnN0IHNsaWRlc2hvdz1uZXcgU2xpZGVzaG93KGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoJy5tb3Rpb24tcmV2ZWFsLXNsaWRlci13cmFwcGVyLnNsaWRlc2hvdycpKTt9KTtlbGVtZW50b3JGcm9udGVuZC5ob29rcy5hZGRBY3Rpb24oJ2Zyb250ZW5kL2VsZW1lbnRfcmVhZHkvZ3JhbmRyZXN0YXVyYW50LXRlc3RpbW9uaWFsLWNhcmQuZGVmYXVsdCcsZnVuY3Rpb24oJHNjb3BlKXtqUXVlcnkoXCIudGVzdGltb25pYWxzLWNhcmQtd3JhcHBlciAub3dsLWNhcm91c2VsXCIpLmVhY2goZnVuY3Rpb24oKXt2YXIgYXV0b1BsYXk9alF1ZXJ5KHRoaXMpLmF0dHIoJ2RhdGEtYXV0b3BsYXknKTtpZih0eXBlb2YgYXV0b1BsYXk9PVwidW5kZWZpbmVkXCIpe2F1dG9QbGF5PWZhbHNlO31cbmlmKGF1dG9QbGF5PT0xKVxue2F1dG9QbGF5PXRydWU7fVxuZWxzZVxue2F1dG9QbGF5PWZhbHNlO31cbnZhciB0aW1lcj1qUXVlcnkodGhpcykuYXR0cignZGF0YS10aW1lcicpO2lmKHR5cGVvZiB0aW1lcj09XCJ1bmRlZmluZWRcIil7dGltZXI9ODAwMDt9XG52YXIgcGFnaW5hdGlvbj1qUXVlcnkodGhpcykuYXR0cignZGF0YS1wYWdpbmF0aW9uJyk7aWYodHlwZW9mIHBhZ2luYXRpb249PVwidW5kZWZpbmVkXCIpe3BhZ2luYXRpb249dHJ1ZTt9XG5pZihwYWdpbmF0aW9uPT0xKVxue3BhZ2luYXRpb249dHJ1ZTt9XG5lbHNlXG57cGFnaW5hdGlvbj1mYWxzZTt9XG5qUXVlcnkodGhpcykub3dsQ2Fyb3VzZWwoe2xvb3A6dHJ1ZSxjZW50ZXI6dHJ1ZSxpdGVtczozLG1hcmdpbjowLGF1dG9wbGF5OmF1dG9QbGF5LGRvdHM6cGFnaW5hdGlvbixhdXRvcGxheVRpbWVvdXQ6dGltZXIsc21hcnRTcGVlZDo0NTAscmVzcG9uc2l2ZTp7MDp7aXRlbXM6MX0sNzY4OntpdGVtczoyfSwxMTcwOntpdGVtczozfX19KTt9KTt9KTtlbGVtZW50b3JGcm9udGVuZC5ob29rcy5hZGRBY3Rpb24oJ2Zyb250ZW5kL2VsZW1lbnRfcmVhZHkvZ3JhbmRyZXN0YXVyYW50LXNsaWRlci1pbWFnZS1jYXJvdXNlbC5kZWZhdWx0JyxmdW5jdGlvbigkc2NvcGUpe2pRdWVyeShcIi5pbWFnZS1jYXJvdXNlbC1zbGlkZXItd3JhcHBlclwiKS5lYWNoKGZ1bmN0aW9uKCl7alF1ZXJ5KHRoaXMpLmZpbmQoJy5jYXJvdXNlbC1pdGVtJykuZXEoMCkuYWRkQ2xhc3MoJ2FjdGl2ZScpO3ZhciB0b3RhbD1qUXVlcnkodGhpcykuZmluZCgnLmNhcm91c2VsLWl0ZW0nKS5sZW5ndGg7dmFyIGN1cnJlbnQ9MDt2YXIgc2xpZGVPYmo9alF1ZXJ5KHRoaXMpO2pRdWVyeSh0aGlzKS5maW5kKCcjbW92ZVJpZ2h0Jykub24oJ2NsaWNrJyxmdW5jdGlvbigpe3ZhciBuZXh0PWN1cnJlbnQ7Y3VycmVudD1jdXJyZW50KzE7c2V0U2xpZGUobmV4dCxjdXJyZW50LHNsaWRlT2JqKTt9KTtqUXVlcnkodGhpcykuZmluZCgnI21vdmVMZWZ0Jykub24oJ2NsaWNrJyxmdW5jdGlvbigpe3ZhciBwcmV2PWN1cnJlbnQ7Y3VycmVudD1jdXJyZW50LTE7c2V0U2xpZGUocHJldixjdXJyZW50LHNsaWRlT2JqKTt9KTtmdW5jdGlvbiBzZXRTbGlkZShwcmV2LG5leHQsc2xpZGVPYmope3ZhciBzbGlkZT1jdXJyZW50O2lmKG5leHQ+dG90YWwtMSl7c2xpZGU9MDtjdXJyZW50PTA7fVxuaWYobmV4dDwwKXtzbGlkZT10b3RhbC0xO2N1cnJlbnQ9dG90YWwtMTt9XG5zbGlkZU9iai5maW5kKCcuY2Fyb3VzZWwtaXRlbScpLmVxKHByZXYpLnJlbW92ZUNsYXNzKCdhY3RpdmUnKTtzbGlkZU9iai5maW5kKCcuY2Fyb3VzZWwtaXRlbScpLmVxKHNsaWRlKS5hZGRDbGFzcygnYWN0aXZlJyk7c2V0VGltZW91dChmdW5jdGlvbigpe30sODAwKTt9fSk7fSk7ZWxlbWVudG9yRnJvbnRlbmQuaG9va3MuYWRkQWN0aW9uKCdmcm9udGVuZC9lbGVtZW50X3JlYWR5L2dyYW5kcmVzdGF1cmFudC1wb3J0Zm9saW8tZ3JpZC5kZWZhdWx0JyxmdW5jdGlvbigkc2NvcGUpe2pRdWVyeShcIi5wb3J0Zm9saW9fZ3JpZF9jb250YWluZXJcIikuZWFjaChmdW5jdGlvbigpe3ZhciBjb250YWluZGVyRGl2PWpRdWVyeSh0aGlzKTt2YXIgc2VsZWN0ZWRDbGFzcz1cIlwiO2NvbnRhaW5kZXJEaXYuZmluZChcIi5maWx0ZXItdGFnLWJ0blwiKS5vbignY2xpY2snLGZ1bmN0aW9uKCl7Y29udGFpbmRlckRpdi5maW5kKFwiLmZpbHRlci10YWctYnRuXCIpLnJlbW92ZUNsYXNzKFwiYWN0aXZlXCIpO2pRdWVyeSh0aGlzKS5hZGRDbGFzcyhcImFjdGl2ZVwiKTtzZWxlY3RlZENsYXNzPWpRdWVyeSh0aGlzKS5hdHRyKFwiZGF0YS1yZWxcIik7Y29sdW1uPWpRdWVyeSh0aGlzKS5hdHRyKFwiZGF0YS1jb2xzXCIpO3ZhciBncmlkRGl2PWNvbnRhaW5kZXJEaXYuZmluZChcIi5wb3J0Zm9saW8tZ3JpZC1jb250ZW50LXdyYXBwZXJcIik7Z3JpZERpdi5mYWRlVG8oMTAwLDApO2dyaWREaXYuZmluZChcIi5wb3J0Zm9saW8tZ3JpZC13cmFwcGVyXCIpLmNzcyh7b3BhY2l0eTowLGRpc3BsYXk6J25vbmUnLHRyYW5zZm9ybTonc2NhbGUoMC4wKSd9KTtncmlkRGl2LmZpbmQoXCIucG9ydGZvbGlvLWdyaWQtd3JhcHBlclwiKS5ub3QoXCIuXCIrc2VsZWN0ZWRDbGFzcykuZmFkZU91dCgpLnJlbW92ZUNsYXNzKCdzY2FsZS1hbm0nKTtzZXRUaW1lb3V0KGZ1bmN0aW9uKCl7alF1ZXJ5KFwiLlwiK3NlbGVjdGVkQ2xhc3MpLmZhZGVJbigpLmFkZENsYXNzKCdzY2FsZS1hbm0nKTtqUXVlcnkoXCIuXCIrc2VsZWN0ZWRDbGFzcykuY3NzKHtvcGFjaXR5OjEsZGlzcGxheTonYmxvY2snLHRyYW5zZm9ybTonc2NhbGUoMSwxKSd9KTtncmlkRGl2LmZpbmQoXCIucG9ydGZvbGlvLWdyaWQtd3JhcHBlci5sYXN0XCIpLnJlbW92ZUNsYXNzKCdsYXN0Jyk7dmFyIGNvdW50PWdyaWREaXYuZmluZChcIi5wb3J0Zm9saW8tZ3JpZC13cmFwcGVyLnNjYWxlLWFubVwiKS5sZW5ndGg7Z3JpZERpdi5maW5kKFwiLnBvcnRmb2xpby1ncmlkLXdyYXBwZXIuc2NhbGUtYW5tXCIpLmVhY2goZnVuY3Rpb24oaW5kZXgpe3ZhciBsYXN0SW5kZXg9cGFyc2VJbnQoaW5kZXgrMSk7aWYobGFzdEluZGV4JWNvbHVtbj09MClcbntqUXVlcnkodGhpcykuYWRkQ2xhc3MoJ2xhc3QnKTt9XG5pZihsYXN0SW5kZXg9PWNvdW50KVxue3NldFRpbWVvdXQoZnVuY3Rpb24oKXtncmlkRGl2LmZhZGVUbygzMDAsMSk7fSwzMDApO319KTt9LDMwMCk7fSk7fSk7fSk7ZWxlbWVudG9yRnJvbnRlbmQuaG9va3MuYWRkQWN0aW9uKCdmcm9udGVuZC9lbGVtZW50X3JlYWR5L2dyYW5kcmVzdGF1cmFudC1wb3J0Zm9saW8tZ3JpZC1vdmVybGF5LmRlZmF1bHQnLGZ1bmN0aW9uKCRzY29wZSl7alF1ZXJ5KFwiLnBvcnRmb2xpby1ncmlkLW92ZXJsYXktY29udGFpbmVyXCIpLmVhY2goZnVuY3Rpb24oKXt2YXIgY29udGFpbmRlckRpdj1qUXVlcnkodGhpcyk7dmFyIHNlbGVjdGVkQ2xhc3M9XCJcIjtjb250YWluZGVyRGl2LmZpbmQoXCIucG9ydGZvbGlvLWdyaWQtd3JhcHBlci1vdmVybGF5XCIpLmltYWdlc0xvYWRlZCgpLmFsd2F5cyhmdW5jdGlvbigpe3ZhciBjdXJyZW50V3JhcHBlcj1jb250YWluZGVyRGl2LmZpbmQoXCIucG9ydGZvbGlvLWdyaWQtd3JhcHBlci1vdmVybGF5XCIpO3ZhciBjdXJyZW50SW1nPWNvbnRhaW5kZXJEaXYuZmluZChcIi5wb3J0Zm9saW8tZ3JpZC13cmFwcGVyLW92ZXJsYXkgaW1nXCIpO2N1cnJlbnRXcmFwcGVyLmNzcygnaGVpZ2h0JyxjdXJyZW50SW1nLmhlaWdodCgpKydweCcpO30pO2pRdWVyeSh3aW5kb3cpLnJlc2l6ZShmdW5jdGlvbigpe2NvbnRhaW5kZXJEaXYuZmluZChcIi5wb3J0Zm9saW8tZ3JpZC13cmFwcGVyLW92ZXJsYXlcIikuZWFjaChmdW5jdGlvbigpe3ZhciBjdXJyZW50SW1nPWpRdWVyeSh0aGlzKS5maW5kKFwiaW1nXCIpO2pRdWVyeSh0aGlzKS5jc3MoJ2hlaWdodCcsY3VycmVudEltZy5oZWlnaHQoKSsncHgnKTt9KTt9KTtjb250YWluZGVyRGl2LmZpbmQoXCIuZmlsdGVyLXRhZy1idG5cIikub24oJ2NsaWNrJyxmdW5jdGlvbigpe2NvbnRhaW5kZXJEaXYuZmluZChcIi5maWx0ZXItdGFnLWJ0blwiKS5yZW1vdmVDbGFzcyhcImFjdGl2ZVwiKTtqUXVlcnkodGhpcykuYWRkQ2xhc3MoXCJhY3RpdmVcIik7c2VsZWN0ZWRDbGFzcz1qUXVlcnkodGhpcykuYXR0cihcImRhdGEtcmVsXCIpO2NvbHVtbj1qUXVlcnkodGhpcykuYXR0cihcImRhdGEtY29sc1wiKTt2YXIgZ3JpZERpdj1jb250YWluZGVyRGl2LmZpbmQoXCIucG9ydGZvbGlvLWdyaWQtY29udGVudC13cmFwcGVyXCIpO2dyaWREaXYuZmFkZVRvKDEwMCwwKTtncmlkRGl2LmZpbmQoXCIucG9ydGZvbGlvLWdyaWQtd3JhcHBlci1vdmVybGF5XCIpLmNzcyh7b3BhY2l0eTowLGRpc3BsYXk6J25vbmUnLHRyYW5zZm9ybTonc2NhbGUoMC4wKSd9KTtncmlkRGl2LmZpbmQoXCIucG9ydGZvbGlvLWdyaWQtd3JhcHBlci1vdmVybGF5XCIpLm5vdChcIi5cIitzZWxlY3RlZENsYXNzKS5mYWRlT3V0KCkucmVtb3ZlQ2xhc3MoJ3NjYWxlLWFubScpO3NldFRpbWVvdXQoZnVuY3Rpb24oKXtqUXVlcnkoXCIuXCIrc2VsZWN0ZWRDbGFzcykuZmFkZUluKCkuYWRkQ2xhc3MoJ3NjYWxlLWFubScpO2pRdWVyeShcIi5cIitzZWxlY3RlZENsYXNzKS5jc3Moe29wYWNpdHk6MSxkaXNwbGF5OidibG9jaycsdHJhbnNmb3JtOidzY2FsZSgxLDEpJ30pO2dyaWREaXYuZmluZChcIi5wb3J0Zm9saW8tZ3JpZC13cmFwcGVyLW92ZXJsYXkubGFzdFwiKS5yZW1vdmVDbGFzcygnbGFzdCcpO3ZhciBjb3VudD1ncmlkRGl2LmZpbmQoXCIucG9ydGZvbGlvLWdyaWQtd3JhcHBlci1vdmVybGF5LnNjYWxlLWFubVwiKS5sZW5ndGg7Z3JpZERpdi5maW5kKFwiLnBvcnRmb2xpby1ncmlkLXdyYXBwZXItb3ZlcmxheS5zY2FsZS1hbm1cIikuZWFjaChmdW5jdGlvbihpbmRleCl7dmFyIGxhc3RJbmRleD1wYXJzZUludChpbmRleCsxKTtpZihsYXN0SW5kZXglY29sdW1uPT0wKVxue2pRdWVyeSh0aGlzKS5hZGRDbGFzcygnbGFzdCcpO31cbmlmKGxhc3RJbmRleD09Y291bnQpXG57c2V0VGltZW91dChmdW5jdGlvbigpe2dyaWREaXYuZmFkZVRvKDMwMCwxKTt9LDMwMCk7fX0pO30sMzAwKTt9KTt9KTt9KTtlbGVtZW50b3JGcm9udGVuZC5ob29rcy5hZGRBY3Rpb24oJ2Zyb250ZW5kL2VsZW1lbnRfcmVhZHkvZ3JhbmRyZXN0YXVyYW50LXBvcnRmb2xpby0zZC1vdmVybGF5LmRlZmF1bHQnLGZ1bmN0aW9uKCRzY29wZSl7alF1ZXJ5KFwiLnBvcnRmb2xpby1ncmlkLW92ZXJsYXktY29udGFpbmVyXCIpLmVhY2goZnVuY3Rpb24oKXt2YXIgY29udGFpbmRlckRpdj1qUXVlcnkodGhpcyk7dmFyIHNlbGVjdGVkQ2xhc3M9XCJcIjtjb250YWluZGVyRGl2LmZpbmQoXCIucG9ydGZvbGlvLWdyaWQtd3JhcHBlci1vdmVybGF5XCIpLmltYWdlc0xvYWRlZCgpLmFsd2F5cyhmdW5jdGlvbigpe3ZhciBjdXJyZW50V3JhcHBlcj1jb250YWluZGVyRGl2LmZpbmQoXCIucG9ydGZvbGlvLWdyaWQtd3JhcHBlci1vdmVybGF5XCIpO3ZhciBjdXJyZW50SW1nPWNvbnRhaW5kZXJEaXYuZmluZChcIi5wb3J0Zm9saW8tZ3JpZC13cmFwcGVyLW92ZXJsYXkgaW1nXCIpO2N1cnJlbnRXcmFwcGVyLmNzcygnaGVpZ2h0JyxjdXJyZW50SW1nLmhlaWdodCgpKydweCcpO30pO2pRdWVyeSh3aW5kb3cpLnJlc2l6ZShmdW5jdGlvbigpe2NvbnRhaW5kZXJEaXYuZmluZChcIi5wb3J0Zm9saW8tZ3JpZC13cmFwcGVyLW92ZXJsYXlcIikuZWFjaChmdW5jdGlvbigpe3ZhciBjdXJyZW50SW1nPWpRdWVyeSh0aGlzKS5maW5kKFwiaW1nXCIpO2pRdWVyeSh0aGlzKS5jc3MoJ2hlaWdodCcsY3VycmVudEltZy5oZWlnaHQoKSsncHgnKTt9KTt9KTtpZighaXNfdG91Y2hfZGV2aWNlKCkpXG57dmFyIHNjYWxlVGlsdD0xLjE1O2lmKG5hdmlnYXRvci51c2VyQWdlbnQuaW5kZXhPZignU2FmYXJpJykhPS0xJiZuYXZpZ2F0b3IudXNlckFnZW50LmluZGV4T2YoJ0Nocm9tZScpPT0tMSl7c2NhbGVUaWx0PTE7fVxuY29udGFpbmRlckRpdi5maW5kKFwiLmdyaWRfdGlsdFwiKS50aWx0KHtzY2FsZTpzY2FsZVRpbHQscGVyc3BlY3RpdmU6MjAwMCxnbGFyZTp0cnVlLG1heEdsYXJlOi41fSk7fVxuaWYoIWVsZW1lbnRvckZyb250ZW5kLmlzRWRpdE1vZGUoKSlcbntjb250YWluZGVyRGl2LmZpbmQoXCIucG9ydGZvbGlvLWdyaWQtd3JhcHBlci1vdmVybGF5XCIpLm9uKCdjbGljaycsZnVuY3Rpb24oKXt3aW5kb3cubG9jYXRpb249alF1ZXJ5KHRoaXMpLmZpbmQoXCJhXCIpLmZpcnN0KCkuYXR0cihcImhyZWZcIik7fSk7fVxuY29udGFpbmRlckRpdi5maW5kKFwiLmZpbHRlci10YWctYnRuXCIpLm9uKCdjbGljaycsZnVuY3Rpb24oKXtjb250YWluZGVyRGl2LmZpbmQoXCIuZmlsdGVyLXRhZy1idG5cIikucmVtb3ZlQ2xhc3MoXCJhY3RpdmVcIik7alF1ZXJ5KHRoaXMpLmFkZENsYXNzKFwiYWN0aXZlXCIpO3NlbGVjdGVkQ2xhc3M9alF1ZXJ5KHRoaXMpLmF0dHIoXCJkYXRhLXJlbFwiKTtjb2x1bW49alF1ZXJ5KHRoaXMpLmF0dHIoXCJkYXRhLWNvbHNcIik7dmFyIGdyaWREaXY9Y29udGFpbmRlckRpdi5maW5kKFwiLnBvcnRmb2xpby1ncmlkLWNvbnRlbnQtd3JhcHBlclwiKTtncmlkRGl2LmZhZGVUbygxMDAsMCk7Z3JpZERpdi5maW5kKFwiLnBvcnRmb2xpby1ncmlkLXdyYXBwZXItb3ZlcmxheVwiKS5jc3Moe29wYWNpdHk6MCxkaXNwbGF5Oidub25lJyx0cmFuc2Zvcm06J3NjYWxlKDAuMCknfSk7Z3JpZERpdi5maW5kKFwiLnBvcnRmb2xpby1ncmlkLXdyYXBwZXItb3ZlcmxheVwiKS5ub3QoXCIuXCIrc2VsZWN0ZWRDbGFzcykuZmFkZU91dCgpLnJlbW92ZUNsYXNzKCdzY2FsZS1hbm0nKTtzZXRUaW1lb3V0KGZ1bmN0aW9uKCl7alF1ZXJ5KFwiLlwiK3NlbGVjdGVkQ2xhc3MpLmZhZGVJbigpLmFkZENsYXNzKCdzY2FsZS1hbm0nKTtqUXVlcnkoXCIuXCIrc2VsZWN0ZWRDbGFzcykuY3NzKHtvcGFjaXR5OjEsZGlzcGxheTonYmxvY2snLHRyYW5zZm9ybTonc2NhbGUoMSwxKSd9KTtncmlkRGl2LmZpbmQoXCIucG9ydGZvbGlvLWdyaWQtd3JhcHBlci1vdmVybGF5Lmxhc3RcIikucmVtb3ZlQ2xhc3MoJ2xhc3QnKTt2YXIgY291bnQ9Z3JpZERpdi5maW5kKFwiLnBvcnRmb2xpby1ncmlkLXdyYXBwZXItb3ZlcmxheS5zY2FsZS1hbm1cIikubGVuZ3RoO2dyaWREaXYuZmluZChcIi5wb3J0Zm9saW8tZ3JpZC13cmFwcGVyLW92ZXJsYXkuc2NhbGUtYW5tXCIpLmVhY2goZnVuY3Rpb24oaW5kZXgpe3ZhciBsYXN0SW5kZXg9cGFyc2VJbnQoaW5kZXgrMSk7aWYobGFzdEluZGV4JWNvbHVtbj09MClcbntqUXVlcnkodGhpcykuYWRkQ2xhc3MoJ2xhc3QnKTt9XG5pZihsYXN0SW5kZXg9PWNvdW50KVxue3NldFRpbWVvdXQoZnVuY3Rpb24oKXtncmlkRGl2LmZhZGVUbygzMDAsMSk7fSwzMDApO319KTt9LDMwMCk7fSk7fSk7fSk7ZWxlbWVudG9yRnJvbnRlbmQuaG9va3MuYWRkQWN0aW9uKCdmcm9udGVuZC9lbGVtZW50X3JlYWR5L2dyYW5kcmVzdGF1cmFudC1wb3J0Zm9saW8tY2xhc3NpYy5kZWZhdWx0JyxmdW5jdGlvbigkc2NvcGUpe2pRdWVyeShcIi5wb3J0Zm9saW8tY2xhc3NpYy1jb250YWluZXJcIikuZWFjaChmdW5jdGlvbigpe3ZhciBjb250YWluZGVyRGl2PWpRdWVyeSh0aGlzKTt2YXIgc2VsZWN0ZWRDbGFzcz1cIlwiO2NvbnRhaW5kZXJEaXYuZmluZChcIi5maWx0ZXItdGFnLWJ0blwiKS5vbignY2xpY2snLGZ1bmN0aW9uKCl7Y29udGFpbmRlckRpdi5maW5kKFwiLmZpbHRlci10YWctYnRuXCIpLnJlbW92ZUNsYXNzKFwiYWN0aXZlXCIpO2pRdWVyeSh0aGlzKS5hZGRDbGFzcyhcImFjdGl2ZVwiKTtzZWxlY3RlZENsYXNzPWpRdWVyeSh0aGlzKS5hdHRyKFwiZGF0YS1yZWxcIik7Y29sdW1uPWpRdWVyeSh0aGlzKS5hdHRyKFwiZGF0YS1jb2xzXCIpO3ZhciBncmlkRGl2PWNvbnRhaW5kZXJEaXYuZmluZChcIi5wb3J0Zm9saW8tY2xhc3NpYy1jb250ZW50LXdyYXBwZXJcIik7Z3JpZERpdi5mYWRlVG8oMTAwLDApO2dyaWREaXYuZmluZChcIi5wb3J0Zm9saW8tY2xhc3NpYy1ncmlkLXdyYXBwZXJcIikuY3NzKHtvcGFjaXR5OjAsZGlzcGxheTonbm9uZScsdHJhbnNmb3JtOidzY2FsZSgwLjApJ30pO2dyaWREaXYuZmluZChcIi5wb3J0Zm9saW8tY2xhc3NpYy1ncmlkLXdyYXBwZXJcIikubm90KFwiLlwiK3NlbGVjdGVkQ2xhc3MpLmZhZGVPdXQoKS5yZW1vdmVDbGFzcygnc2NhbGUtYW5tJyk7c2V0VGltZW91dChmdW5jdGlvbigpe2pRdWVyeShcIi5cIitzZWxlY3RlZENsYXNzKS5hZGRDbGFzcygnc2NhbGUtYW5tJyk7alF1ZXJ5KFwiLlwiK3NlbGVjdGVkQ2xhc3MpLmNzcyh7b3BhY2l0eToxLGRpc3BsYXk6J2Jsb2NrJyx0cmFuc2Zvcm06J3NjYWxlKDEsMSknfSk7Z3JpZERpdi5maW5kKFwiLnBvcnRmb2xpby1jbGFzc2ljLWdyaWQtd3JhcHBlci5sYXN0XCIpLnJlbW92ZUNsYXNzKCdsYXN0Jyk7dmFyIGNvdW50PWdyaWREaXYuZmluZChcIi5wb3J0Zm9saW8tY2xhc3NpYy1ncmlkLXdyYXBwZXIuc2NhbGUtYW5tXCIpLmxlbmd0aDtncmlkRGl2LmZpbmQoXCIucG9ydGZvbGlvLWNsYXNzaWMtZ3JpZC13cmFwcGVyLnNjYWxlLWFubVwiKS5lYWNoKGZ1bmN0aW9uKGluZGV4KXt2YXIgbGFzdEluZGV4PXBhcnNlSW50KGluZGV4KzEpO2lmKGxhc3RJbmRleCVjb2x1bW49PTApXG57alF1ZXJ5KHRoaXMpLmFkZENsYXNzKCdsYXN0Jyk7fVxuaWYobGFzdEluZGV4PT1jb3VudClcbntzZXRUaW1lb3V0KGZ1bmN0aW9uKCl7Z3JpZERpdi5mYWRlVG8oMzAwLDEpO30sMzAwKTt9fSk7fSwzMDApO30pO30pO30pO2VsZW1lbnRvckZyb250ZW5kLmhvb2tzLmFkZEFjdGlvbignZnJvbnRlbmQvZWxlbWVudF9yZWFkeS9ncmFuZHJlc3RhdXJhbnQtcG9ydGZvbGlvLWNvbnRhaW4uZGVmYXVsdCcsZnVuY3Rpb24oJHNjb3BlKXtqUXVlcnkoXCIucG9ydGZvbGlvLWNsYXNzaWMtY29udGFpbmVyLmNvbnRhaW5cIikuZWFjaChmdW5jdGlvbigpe3ZhciBjb250YWluZGVyRGl2PWpRdWVyeSh0aGlzKTt2YXIgc2VsZWN0ZWRDbGFzcz1cIlwiO2NvbnRhaW5kZXJEaXYuZmluZChcIi5maWx0ZXItdGFnLWJ0blwiKS5vbignY2xpY2snLGZ1bmN0aW9uKCl7Y29udGFpbmRlckRpdi5maW5kKFwiLmZpbHRlci10YWctYnRuXCIpLnJlbW92ZUNsYXNzKFwiYWN0aXZlXCIpO2pRdWVyeSh0aGlzKS5hZGRDbGFzcyhcImFjdGl2ZVwiKTtzZWxlY3RlZENsYXNzPWpRdWVyeSh0aGlzKS5hdHRyKFwiZGF0YS1yZWxcIik7Y29sdW1uPWpRdWVyeSh0aGlzKS5hdHRyKFwiZGF0YS1jb2xzXCIpO3ZhciBncmlkRGl2PWNvbnRhaW5kZXJEaXYuZmluZChcIi5wb3J0Zm9saW8tY2xhc3NpYy1jb250ZW50LXdyYXBwZXJcIik7Z3JpZERpdi5mYWRlVG8oMTAwLDApO2dyaWREaXYuZmluZChcIi5wb3J0Zm9saW8tY2xhc3NpYy1ncmlkLXdyYXBwZXJcIikuY3NzKHtvcGFjaXR5OjAsZGlzcGxheTonbm9uZScsdHJhbnNmb3JtOidzY2FsZSgwLjApJ30pO2dyaWREaXYuZmluZChcIi5wb3J0Zm9saW8tY2xhc3NpYy1ncmlkLXdyYXBwZXJcIikubm90KFwiLlwiK3NlbGVjdGVkQ2xhc3MpLmZhZGVPdXQoKS5yZW1vdmVDbGFzcygnc2NhbGUtYW5tJyk7c2V0VGltZW91dChmdW5jdGlvbigpe2pRdWVyeShcIi5cIitzZWxlY3RlZENsYXNzKS5hZGRDbGFzcygnc2NhbGUtYW5tJyk7alF1ZXJ5KFwiLlwiK3NlbGVjdGVkQ2xhc3MpLmNzcyh7b3BhY2l0eToxLGRpc3BsYXk6J2Jsb2NrJyx0cmFuc2Zvcm06J3NjYWxlKDEsMSknfSk7Z3JpZERpdi5maW5kKFwiLnBvcnRmb2xpby1jbGFzc2ljLWdyaWQtd3JhcHBlci5sYXN0XCIpLnJlbW92ZUNsYXNzKCdsYXN0Jyk7dmFyIGNvdW50PWdyaWREaXYuZmluZChcIi5wb3J0Zm9saW8tY2xhc3NpYy1ncmlkLXdyYXBwZXIuc2NhbGUtYW5tXCIpLmxlbmd0aDtncmlkRGl2LmZpbmQoXCIucG9ydGZvbGlvLWNsYXNzaWMtZ3JpZC13cmFwcGVyLnNjYWxlLWFubVwiKS5lYWNoKGZ1bmN0aW9uKGluZGV4KXt2YXIgbGFzdEluZGV4PXBhcnNlSW50KGluZGV4KzEpO2lmKGxhc3RJbmRleCVjb2x1bW49PTApXG57alF1ZXJ5KHRoaXMpLmFkZENsYXNzKCdsYXN0Jyk7fVxuaWYobGFzdEluZGV4PT1jb3VudClcbntzZXRUaW1lb3V0KGZ1bmN0aW9uKCl7Z3JpZERpdi5mYWRlVG8oMzAwLDEpO30sMzAwKTt9fSk7fSwzMDApO30pO30pO30pO2VsZW1lbnRvckZyb250ZW5kLmhvb2tzLmFkZEFjdGlvbignZnJvbnRlbmQvZWxlbWVudF9yZWFkeS9ncmFuZHJlc3RhdXJhbnQtbmF2aWdhdGlvbi1tZW51LmRlZmF1bHQnLGZ1bmN0aW9uKCRzY29wZSl7alF1ZXJ5KCcudGhlbWVnb29kcy1uYXZpZ2F0aW9uLXdyYXBwZXIgLm5hdiBsaS5tZW51LWl0ZW0nKS5ob3ZlcihmdW5jdGlvbigpXG57alF1ZXJ5KHRoaXMpLmNoaWxkcmVuKCd1bDpmaXJzdCcpLmFkZENsYXNzKCd2aXNpYmxlJyk7alF1ZXJ5KHRoaXMpLmNoaWxkcmVuKCd1bDpmaXJzdCcpLmFkZENsYXNzKCdob3ZlcicpO2pRdWVyeSh0aGlzKS5jaGlsZHJlbignLmVsZW1lbnRvci1tZWdhbWVudS13cmFwcGVyOmZpcnN0JykuYWRkQ2xhc3MoJ3Zpc2libGUnKTtqUXVlcnkodGhpcykuY2hpbGRyZW4oJy5lbGVtZW50b3ItbWVnYW1lbnUtd3JhcHBlcjpmaXJzdCcpLmFkZENsYXNzKCdob3ZlcicpO30sZnVuY3Rpb24oKVxue2pRdWVyeSh0aGlzKS5jaGlsZHJlbigndWw6Zmlyc3QnKS5yZW1vdmVDbGFzcygndmlzaWJsZScpO2pRdWVyeSh0aGlzKS5jaGlsZHJlbigndWw6Zmlyc3QnKS5yZW1vdmVDbGFzcygnaG92ZXInKTtqUXVlcnkodGhpcykuY2hpbGRyZW4oJy5lbGVtZW50b3ItbWVnYW1lbnUtd3JhcHBlcjpmaXJzdCcpLnJlbW92ZUNsYXNzKCd2aXNpYmxlJyk7alF1ZXJ5KHRoaXMpLmNoaWxkcmVuKCcuZWxlbWVudG9yLW1lZ2FtZW51LXdyYXBwZXI6Zmlyc3QnKS5yZW1vdmVDbGFzcygnaG92ZXInKTt9KTtqUXVlcnkoJy50aGVtZWdvb2RzLW5hdmlnYXRpb24td3JhcHBlciAubmF2IGxpLm1lbnUtaXRlbScpLmNoaWxkcmVuKCd1bDpmaXJzdC5ob3ZlcicpLmhvdmVyKGZ1bmN0aW9uKClcbntqUXVlcnkodGhpcykuc3RvcCgpLmFkZENsYXNzKCd2aXNpYmxlJyk7fSxmdW5jdGlvbigpXG57alF1ZXJ5KHRoaXMpLnN0b3AoKS5yZW1vdmVDbGFzcygndmlzaWJsZScpO30pO2pRdWVyeSgnLnRoZW1lZ29vZHMtbmF2aWdhdGlvbi13cmFwcGVyIC5uYXYgbGkubWVudS1pdGVtJykuY2hpbGRyZW4oJy5lbGVtZW50b3ItbWVnYW1lbnUtd3JhcHBlci5ob3ZlcicpLmhvdmVyKGZ1bmN0aW9uKClcbntqUXVlcnkodGhpcykuc3RvcCgpLmFkZENsYXNzKCd2aXNpYmxlJyk7fSxmdW5jdGlvbigpXG57alF1ZXJ5KHRoaXMpLnN0b3AoKS5yZW1vdmVDbGFzcygndmlzaWJsZScpO30pO30pO2VsZW1lbnRvckZyb250ZW5kLmhvb2tzLmFkZEFjdGlvbignZnJvbnRlbmQvZWxlbWVudF9yZWFkeS9ncmFuZHJlc3RhdXJhbnQtdmlkZW8tZ3JpZC5kZWZhdWx0JyxmdW5jdGlvbigkc2NvcGUpe2pRdWVyeShcIi5wb3J0Zm9saW8tY2xhc3NpYy1jb250YWluZXIudmlkZW8tZ3JpZFwiKS5lYWNoKGZ1bmN0aW9uKCl7dmFyIGNvbnRhaW5lckRpdj1qUXVlcnkodGhpcyk7dmFyIHNlbGVjdGVkQ2xhc3M9XCJcIjtjb250YWluZXJEaXYuZmluZChcIi5maWx0ZXItdGFnLWJ0blwiKS5vbignY2xpY2snLGZ1bmN0aW9uKCl7Y29udGFpbmVyRGl2LmZpbmQoXCIuZmlsdGVyLXRhZy1idG5cIikucmVtb3ZlQ2xhc3MoXCJhY3RpdmVcIik7alF1ZXJ5KHRoaXMpLmFkZENsYXNzKFwiYWN0aXZlXCIpO3NlbGVjdGVkQ2xhc3M9alF1ZXJ5KHRoaXMpLmF0dHIoXCJkYXRhLXJlbFwiKTtjb2x1bW49alF1ZXJ5KHRoaXMpLmF0dHIoXCJkYXRhLWNvbHNcIik7dmFyIGdyaWREaXY9Y29udGFpbmVyRGl2LmZpbmQoXCIucG9ydGZvbGlvLWNsYXNzaWMtY29udGVudC13cmFwcGVyXCIpO2dyaWREaXYuZmFkZVRvKDEwMCwwKTtncmlkRGl2LmZpbmQoXCIucG9ydGZvbGlvLWNsYXNzaWMtZ3JpZC13cmFwcGVyXCIpLmNzcyh7b3BhY2l0eTowLGRpc3BsYXk6J25vbmUnLHRyYW5zZm9ybTonc2NhbGUoMC4wKSd9KTtncmlkRGl2LmZpbmQoXCIucG9ydGZvbGlvLWNsYXNzaWMtZ3JpZC13cmFwcGVyXCIpLm5vdChcIi5cIitzZWxlY3RlZENsYXNzKS5mYWRlT3V0KCkucmVtb3ZlQ2xhc3MoJ3NjYWxlLWFubScpO3NldFRpbWVvdXQoZnVuY3Rpb24oKXtqUXVlcnkoXCIuXCIrc2VsZWN0ZWRDbGFzcykuYWRkQ2xhc3MoJ3NjYWxlLWFubScpO2pRdWVyeShcIi5cIitzZWxlY3RlZENsYXNzKS5jc3Moe29wYWNpdHk6MSxkaXNwbGF5OidibG9jaycsdHJhbnNmb3JtOidzY2FsZSgxLDEpJ30pO2dyaWREaXYuZmluZChcIi5wb3J0Zm9saW8tY2xhc3NpYy1ncmlkLXdyYXBwZXIubGFzdFwiKS5yZW1vdmVDbGFzcygnbGFzdCcpO3ZhciBjb3VudD1ncmlkRGl2LmZpbmQoXCIucG9ydGZvbGlvLWNsYXNzaWMtZ3JpZC13cmFwcGVyLnNjYWxlLWFubVwiKS5sZW5ndGg7Z3JpZERpdi5maW5kKFwiLnBvcnRmb2xpby1jbGFzc2ljLWdyaWQtd3JhcHBlci5zY2FsZS1hbm1cIikuZWFjaChmdW5jdGlvbihpbmRleCl7dmFyIGxhc3RJbmRleD1wYXJzZUludChpbmRleCsxKTtpZihsYXN0SW5kZXglY29sdW1uPT0wKVxue2pRdWVyeSh0aGlzKS5hZGRDbGFzcygnbGFzdCcpO31cbmlmKGxhc3RJbmRleD09Y291bnQpXG57c2V0VGltZW91dChmdW5jdGlvbigpe2dyaWREaXYuZmFkZVRvKDMwMCwxKTt9LDMwMCk7fX0pO30sMzAwKTt9KTt9KTtqUXVlcnkoXCIucG9ydGZvbGlvLWNsYXNzaWMtY29udGVudC13cmFwcGVyLnZpZGVvLWdyaWQgLnZpZGVvLWdyaWQtd3JhcHBlclwiKS5lYWNoKGZ1bmN0aW9uKCl7dmFyIHZpZGVvPXRoaXMuZ2V0RWxlbWVudHNCeUNsYXNzTmFtZShcInZpZGVvLWNhcmRcIilbMF07dmFyIGlkPWpRdWVyeSh2aWRlbykuZGF0YSgndmlkZW8taWQnKTt2YXIgcGxheT1kb2N1bWVudC5jcmVhdGVFbGVtZW50KFwiZGl2XCIpO3ZhciBvdXRsaW5lPWRvY3VtZW50LmNyZWF0ZUVsZW1lbnQoXCJkaXZcIik7dmFyIHJpbmc9ZG9jdW1lbnQuY3JlYXRlRWxlbWVudChcImRpdlwiKTt2YXIgZmlsbD1kb2N1bWVudC5jcmVhdGVFbGVtZW50KFwiZGl2XCIpO3ZhciBtYXNrPWRvY3VtZW50LmNyZWF0ZUVsZW1lbnQoXCJkaXZcIik7dmFyIHdpcGU9ZG9jdW1lbnQuY3JlYXRlRWxlbWVudChcImRpdlwiKTt2YXIgd3JhcD1kb2N1bWVudC5jcmVhdGVFbGVtZW50KFwiZGl2XCIpO3ZhciBpZnJhbWU9JzxpZnJhbWUgd2lkdGg9XCIxMDAlXCIgaGVpZ2h0PVwiMTAwJVwiIHNyYz1cImh0dHBzOi8vd3d3LnlvdXR1YmUuY29tL2VtYmVkLycraWQrJ1wiIGZyYW1lYm9yZGVyPVwiMFwiIGFsbG93PVwiYXV0b3BsYXk7IGVuY3J5cHRlZC1tZWRpYVwiIGFsbG93ZnVsbHNjcmVlbj48L2lmcmFtZT4nO1R3ZWVuTWF4LnNldChwbGF5LHtjc3M6e3dpZHRoOjAsaGVpZ2h0OjAsXCJib3JkZXItdG9wXCI6XCIxMHB4IHNvbGlkIHRyYW5zcGFyZW50XCIsXCJib3JkZXItYm90dG9tXCI6XCIxMHB4IHNvbGlkIHRyYW5zcGFyZW50XCIsXCJib3JkZXItbGVmdFwiOlwiMTRweCBzb2xpZCB3aGl0ZVwiLFwiei1pbmRleFwiOjUwLHBvc2l0aW9uOidhYnNvbHV0ZScsdG9wOlwiNTAlXCIsbGVmdDpcIjUwJVwiLG1hcmdpbkxlZnQ6XCItNHB4XCIsbWFyZ2luVG9wOlwiLTlweFwifX0pO1R3ZWVuTWF4LnNldChvdXRsaW5lLHtjc3M6e3dpZHRoOjY1LGhlaWdodDo2NSxib3JkZXI6XCIzcHggc29saWQgd2hpdGVcIixcImJvcmRlci1yYWRpdXNcIjpcIjk5OXB4XCIsXCJ6LWluZGV4XCI6NTAscG9zaXRpb246J2Fic29sdXRlJyx0b3A6XCI1MCVcIixsZWZ0OlwiNTAlXCIseTpcIi01MCVcIix4OlwiLTUwJVwifX0pO1R3ZWVuTWF4LnNldChyaW5nLHtjc3M6e3dpZHRoOjY1LGhlaWdodDo2NSxib3JkZXI6XCIxcHggc29saWQgd2hpdGVcIixcImJvcmRlci1yYWRpdXNcIjpcIjk5OXB4XCIsXCJ6LWluZGV4XCI6NTAscG9zaXRpb246J2Fic29sdXRlJyx0b3A6XCI1MCVcIixsZWZ0OlwiNTAlXCIseTpcIi01MCVcIix4OlwiLTUwJVwifX0pO1R3ZWVuTWF4LnNldChmaWxsLHtjc3M6e3dpZHRoOjY1LG9wYWNpdHk6MCxoZWlnaHQ6NjUsXCJiYWNrZ3JvdW5kLWNvbG9yXCI6XCJ3aGl0ZVwiLFwiYm9yZGVyLXJhZGl1c1wiOlwiOTk5cHhcIixcInotaW5kZXhcIjo1MCxwb3NpdGlvbjonYWJzb2x1dGUnLHRvcDpcIjUwJVwiLGxlZnQ6XCI1MCVcIix5OlwiLTUwJVwiLHg6XCItNTAlXCJ9fSk7VHdlZW5NYXguc2V0KG1hc2sse2Nzczp7d2lkdGg6NjAwLGhlaWdodDo2MDAsc2NhbGU6MSxvcGFjaXR5OjAsYmFja2dyb3VuZENvbG9yOlwiI2ZmZlwiLFwiYm9yZGVyLXJhZGl1c1wiOlwiMzAwcHhcIixcInotaW5kZXhcIjo1MSxwb3NpdGlvbjonYWJzb2x1dGUnLHRvcDpcIjUwJVwiLGxlZnQ6XCI1MCVcIix5OlwiLTUwJVwiLHg6XCItNTAlXCJ9fSk7VHdlZW5NYXguc2V0KHdpcGUse2Nzczp7d2lkdGg6XCIxMDAlXCIsaGVpZ2h0OlwiMTAwJVwiLG9wYWNpdHk6MCxiYWNrZ3JvdW5kQ29sb3I6XCIjZmZmXCIsXCJ6LWluZGV4XCI6NTMscG9zaXRpb246J2Fic29sdXRlJyx0b3A6XCIwXCIscmlnaHQ6XCIwXCJ9fSk7VHdlZW5NYXguc2V0KHdyYXAse2Nzczp7d2lkdGg6XCIxMDAlXCIsaGVpZ2h0OlwiMTAwJVwiLGF1dG9BbHBoYTowLFwiei1pbmRleFwiOjUyLHBvc2l0aW9uOidhYnNvbHV0ZScsdG9wOlwiMFwiLGxlZnQ6XCIwXCJ9fSk7dmlkZW8uYXBwZW5kQ2hpbGQocGxheSk7dmlkZW8uYXBwZW5kQ2hpbGQob3V0bGluZSk7dmlkZW8uYXBwZW5kQ2hpbGQocmluZyk7dmlkZW8uYXBwZW5kQ2hpbGQoZmlsbCk7dmlkZW8uYXBwZW5kQ2hpbGQobWFzayk7dmlkZW8uYXBwZW5kQ2hpbGQod2lwZSk7alF1ZXJ5KHdyYXApLmh0bWwoaWZyYW1lKTt2aWRlby5hcHBlbmRDaGlsZCh3cmFwKTtqUXVlcnkodmlkZW8pLm9uKFwibW91c2VlbnRlclwiLGZ1bmN0aW9uKGV2ZW50KXtUd2VlbkxpdGUua2lsbFR3ZWVuc09mKFtmaWxsLG91dGxpbmUscmluZ10pO1R3ZWVuTGl0ZS50byhmaWxsLDAuNCx7b3BhY2l0eTowLjIsZWFzZTpcImN1YmljLWJlemllcigwLjM5LCAwLjU3NSwgMC41NjUsIDEpXCJ9KTtUd2VlbkxpdGUudG8ob3V0bGluZSwwLjQse3NjYWxlOjAuOSxlYXNlOlwiY3ViaWMtYmV6aWVyKDAuMzksIDAuNTc1LCAwLjU2NSwgMSlcIn0pO1R3ZWVuTGl0ZS5mcm9tVG8ocmluZywwLjcse3NjYWxlOjEsb3BhY2l0eTowLjV9LHtzY2FsZToyLG9wYWNpdHk6MCxlYXNlOlwiY3ViaWMtYmV6aWVyKDAuMTY1LCAwLjg0LCAwLjQ0LCAxKVwifSk7VHdlZW5MaXRlLnRvKHBsYXksMC4yLHt4Ojgsb3BhY2l0eTowLGVhc2U6XCJjdWJpYy1iZXppZXIoMC41NTAsIDAuMDU1LCAwLjY3NSwgMC4xOTApXCIsb25Db21wbGV0ZTpmdW5jdGlvbigpe1R3ZWVuTGl0ZS5zZXQocGxheSx7eDotMTIsb25Db21wbGV0ZTpmdW5jdGlvbigpe1R3ZWVuTGl0ZS50byhwbGF5LDAuNCx7eDowLG9wYWNpdHk6MSxlYXNlOlwiY3ViaWMtYmV6aWVyKDAuMjE1LCAwLjYxMCwgMC4zNTUsIDEuMDAwKVwiLH0pO319KTt9fSk7fSk7alF1ZXJ5KHZpZGVvKS5vbihcIm1vdXNlbGVhdmVcIixmdW5jdGlvbihldmVudCl7VHdlZW5MaXRlLmtpbGxUd2VlbnNPZihbZmlsbCxvdXRsaW5lXSk7VHdlZW5MaXRlLnRvKGZpbGwsMC40LHtvcGFjaXR5OjAsZWFzZTpcImN1YmljLWJlemllcigwLjM5LCAwLjU3NSwgMC41NjUsIDEpXCJ9KTtUd2VlbkxpdGUudG8ob3V0bGluZSwwLjQse3NjYWxlOjEsZWFzZTpcImN1YmljLWJlemllcigwLjM5LCAwLjU3NSwgMC41NjUsIDEpXCJ9KTt9KTtqUXVlcnkodmlkZW8pLm9uKFwiY2xpY2tcIixmdW5jdGlvbihldmVudCl7ZXZlbnQucHJldmVudERlZmF1bHQoKTtUd2VlbkxpdGUuZnJvbVRvKG1hc2ssMC4zNSx7b3BhY2l0eToxLHNjYWxlOjB9LHtzY2FsZToxLGVhc2U6XCJjdWJpYy1iZXppZXIoMC41NTAsIDAuMDU1LCAwLjY3NSwgMC4xOTApXCIsb25Db21wbGV0ZTpmdW5jdGlvbigpe1R3ZWVuTGl0ZS5zZXQobWFzayx7b3BhY2l0eTowLHNjYWxlOjAsZGVsYXk6MC41LG9uQ29tcGxldGU6ZnVuY3Rpb24oKXtUd2VlbkxpdGUuZnJvbVRvKHdpcGUsMC40LHt3aWR0aDpcIjEwMCVcIix9LHt3aWR0aDowLGVhc2U6XCJjdWJpYy1iZXppZXIoLjQyLDAsLjU4LDEpXCJ9KTt9fSk7VHdlZW5MaXRlLnNldCh3aXBlLHtvcGFjaXR5OjEsfSk7VHdlZW5MaXRlLnNldCh3cmFwLHthdXRvQWxwaGE6MSx9KTt9fSk7fSk7fSk7fSk7ZWxlbWVudG9yRnJvbnRlbmQuaG9va3MuYWRkQWN0aW9uKCdmcm9udGVuZC9lbGVtZW50X3JlYWR5L2dyYW5kcmVzdGF1cmFudC1wb3J0Zm9saW8tY292ZXJmbG93LmRlZmF1bHQnLGZ1bmN0aW9uKCRzY29wZSl7alF1ZXJ5KFwiLnBvcnRmb2xpby1jb3ZlcmZsb3dcIikuZWFjaChmdW5jdGlvbigpe3ZhciBpbml0aWFsU2xpZGU9MDtpbml0aWFsU2xpZGU9cGFyc2VJbnQoalF1ZXJ5KHRoaXMpLmF0dHIoJ2RhdGEtaW5pdGlhbCcpLTEpO3ZhciBjb3ZlcmZsb3dTd2lwZXI9bmV3IFN3aXBlcihqUXVlcnkodGhpcykse2dyYWJDdXJzb3I6dHJ1ZSxjZW50ZXJlZFNsaWRlczp0cnVlLHNsaWRlc1BlclZpZXc6J2F1dG8nLHNwYWNlQmV0d2Vlbjo1MCxrZXlib2FyZDp7ZW5hYmxlZDp0cnVlLG9ubHlJblZpZXdwb3J0OmZhbHNlLH0scGFnaW5hdGlvbjp7ZWw6Jy5zd2lwZXItcGFnaW5hdGlvbicsfSxpbml0aWFsU2xpZGU6aW5pdGlhbFNsaWRlfSk7fSk7fSk7ZWxlbWVudG9yRnJvbnRlbmQuaG9va3MuYWRkQWN0aW9uKCdmcm9udGVuZC9lbGVtZW50X3JlYWR5L2dyYW5kcmVzdGF1cmFudC1nbGl0Y2gtc2xpZGVzaG93LmRlZmF1bHQnLGZ1bmN0aW9uKCRzY29wZSl7Y2xhc3MgU2xpZGV7Y29uc3RydWN0b3IoZWwpe3RoaXMuRE9NPXtlbDplbH07dGhpcy5ET00uc2xpZGVJbWc9dGhpcy5ET00uZWwucXVlcnlTZWxlY3RvcignLnNsaWRlLWltZycpO3RoaXMuYmdJbWFnZT10aGlzLkRPTS5zbGlkZUltZy5zdHlsZS5iYWNrZ3JvdW5kSW1hZ2U7dGhpcy5sYXlvdXQoKTt9XG5sYXlvdXQoKXt0aGlzLkRPTS5zbGlkZUltZy5pbm5lckhUTUw9YDxkaXYgY2xhc3M9J2dsaXRjaC1pbWcnc3R5bGU9J2JhY2tncm91bmQtaW1hZ2U6ICR7dGhpcy5ET00uc2xpZGVJbWcuc3R5bGUuYmFja2dyb3VuZEltYWdlfTsnPjwvZGl2PmAucmVwZWF0KDUpO3RoaXMuRE9NLmdsaXRjaEltZ3M9QXJyYXkuZnJvbSh0aGlzLkRPTS5zbGlkZUltZy5xdWVyeVNlbGVjdG9yQWxsKCcuZ2xpdGNoLWltZycpKTt9XG5jaGFuZ2VCR0ltYWdlKGJnaW1hZ2UscG9zPTAsZGVsYXk9MCl7c2V0VGltZW91dCgoKT0+dGhpcy5ET00uZ2xpdGNoSW1nc1twb3NdLnN0eWxlLmJhY2tncm91bmRJbWFnZT1iZ2ltYWdlLGRlbGF5KTt9fVxuY2xhc3MgR2xpdGNoU2xpZGVzaG93e2NvbnN0cnVjdG9yKGVsKXt0aGlzLkRPTT17ZWw6ZWx9O3RoaXMuRE9NLnNsaWRlcz1BcnJheS5mcm9tKHRoaXMuRE9NLmVsLnF1ZXJ5U2VsZWN0b3JBbGwoJy5zbGlkZScpKTt0aGlzLnNsaWRlc1RvdGFsPXRoaXMuRE9NLnNsaWRlcy5sZW5ndGg7dGhpcy5zbGlkZXM9W107dGhpcy5ET00uc2xpZGVzLmZvckVhY2goc2xpZGU9PnRoaXMuc2xpZGVzLnB1c2gobmV3IFNsaWRlKHNsaWRlKSkpO3RoaXMuY3VycmVudD0wO3RoaXMuZ2xpdGNoVGltZT0xODAwO3RoaXMudG90YWxHbGl0Y2hTbGljZXM9NTt9XG5nbGl0Y2goc2xpZGVGcm9tLHNsaWRlVG8pe3JldHVybiBuZXcgUHJvbWlzZSgocmVzb2x2ZSxyZWplY3QpPT57c2xpZGVGcm9tLkRPTS5zbGlkZUltZy5jbGFzc0xpc3QuYWRkKCdnbGl0Y2gtLWFuaW1hdGUnKTtjb25zdCBzbGlkZUZyb21CR0ltYWdlPXNsaWRlRnJvbS5iZ0ltYWdlO2NvbnN0IHNsaWRlVG9CR0ltYWdlPXNsaWRlVG8uYmdJbWFnZTtmb3IobGV0IGk9dGhpcy50b3RhbEdsaXRjaFNsaWNlcy0xO2k+PTA7LS1pKXtzbGlkZUZyb20uY2hhbmdlQkdJbWFnZShzbGlkZVRvQkdJbWFnZSxpLHRoaXMuZ2xpdGNoVGltZS8odGhpcy50b3RhbEdsaXRjaFNsaWNlcysxKSoodGhpcy50b3RhbEdsaXRjaFNsaWNlcy1pLTEpK3RoaXMuZ2xpdGNoVGltZS8odGhpcy50b3RhbEdsaXRjaFNsaWNlcysxKSk7fVxuc2V0VGltZW91dCgoKT0+e3NsaWRlRnJvbS5ET00uc2xpZGVJbWcuY2xhc3NMaXN0LnJlbW92ZSgnZ2xpdGNoLS1hbmltYXRlJyk7Zm9yKGxldCBpPXRoaXMudG90YWxHbGl0Y2hTbGljZXMtMTtpPj0wOy0taSl7c2xpZGVGcm9tLmNoYW5nZUJHSW1hZ2Uoc2xpZGVGcm9tQkdJbWFnZSxpLDApO31cbnJlc29sdmUoKTt9LHRoaXMuZ2xpdGNoVGltZSk7fSk7fVxubmF2aWdhdGUoZGlyKXtpZih0aGlzLmlzQW5pbWF0aW5nKXJldHVybjt0aGlzLmlzQW5pbWF0aW5nPXRydWU7Y29uc3QgbmV3Q3VycmVudD1kaXI9PT0nbmV4dCc/dGhpcy5jdXJyZW50PHRoaXMuc2xpZGVzVG90YWwtMT90aGlzLmN1cnJlbnQrMTowOnRoaXMuY3VycmVudD4wP3RoaXMuY3VycmVudC0xOnRoaXMuc2xpZGVzVG90YWwtMTt0aGlzLmdsaXRjaCh0aGlzLnNsaWRlc1t0aGlzLmN1cnJlbnRdLHRoaXMuc2xpZGVzW25ld0N1cnJlbnRdKS50aGVuKCgpPT57dGhpcy5ET00uc2xpZGVzW3RoaXMuY3VycmVudF0uY2xhc3NMaXN0LnJlbW92ZSgnc2xpZGUtY3VycmVudCcpO3RoaXMuY3VycmVudD1uZXdDdXJyZW50O3RoaXMuRE9NLnNsaWRlc1t0aGlzLmN1cnJlbnRdLmNsYXNzTGlzdC5hZGQoJ3NsaWRlLWN1cnJlbnQnKTt0aGlzLmlzQW5pbWF0aW5nPWZhbHNlO30pO319XG5pbWFnZXNMb2FkZWQoZG9jdW1lbnQucXVlcnlTZWxlY3RvckFsbCgnLnNsaWRlLWltZycpLHtiYWNrZ3JvdW5kOnRydWV9LCgpPT57ZG9jdW1lbnQuYm9keS5jbGFzc0xpc3QucmVtb3ZlKCdsb2FkaW5nJyk7Y29uc3Qgc2xpZGVzaG93PW5ldyBHbGl0Y2hTbGlkZXNob3coZG9jdW1lbnQucXVlcnlTZWxlY3RvcignLnNsaWRlcycpKTtjb25zdCBuYXY9QXJyYXkuZnJvbShkb2N1bWVudC5xdWVyeVNlbGVjdG9yQWxsKCcuc2xpZGUtbmF2LWJ1dHRvbicpKTtuYXZbMF0uYWRkRXZlbnRMaXN0ZW5lcignY2xpY2snLCgpPT5zbGlkZXNob3cubmF2aWdhdGUoJ3ByZXYnKSk7bmF2WzFdLmFkZEV2ZW50TGlzdGVuZXIoJ2NsaWNrJywoKT0+c2xpZGVzaG93Lm5hdmlnYXRlKCduZXh0JykpO30pO30pO2VsZW1lbnRvckZyb250ZW5kLmhvb2tzLmFkZEFjdGlvbignZnJvbnRlbmQvZWxlbWVudF9yZWFkeS9ncmFuZHJlc3RhdXJhbnQtbXVzaWMtcGxheWVyLmRlZmF1bHQnLGZ1bmN0aW9uKCRzY29wZSl7alF1ZXJ5KFwiLmdyYW5kcmVzdGF1cmFudC1tdXNpYy1wbGF5ZXJcIikuZWFjaChmdW5jdGlvbigpe3ZhciBjb250YWluZXJEaXY9alF1ZXJ5KHRoaXMpO3ZhciBzb25nSW5kZXg9MDt2YXIgYXVkaW89bmV3IEF1ZGlvKCk7dmFyICRhdWRpbz1qUXVlcnkoYXVkaW8pO3ZhciBoYW5kbGVEcmFnPWZhbHNlO3ZhciB0aW1lSW49MDt2YXIgc29uZ0xpc3RJRD1jb250YWluZXJEaXYuYXR0cignZGF0YS1zb25nbGlzdCcpO3ZhciBwbGF5bGlzdD1KU09OLnBhcnNlKGpRdWVyeSgnIycrc29uZ0xpc3RJRCkudmFsKCkpO2NvbnN0ICRwbGF5ZXI9Y29udGFpbmVyRGl2LmZpbmQoJy5wbGF5ZXInKSwkcG9zdGVyPWNvbnRhaW5lckRpdi5maW5kKCcucGxheWVyLWltZycpLCRiYWNrZ3JvdW5kPWNvbnRhaW5lckRpdi5maW5kKCcucGxheWVyLWJhY2tncm91bmQnKSwkdGl0bGU9Y29udGFpbmVyRGl2LmZpbmQoJy5wbGF5ZXItdGl0bGUnKSwkYXJ0aXN0PWNvbnRhaW5lckRpdi5maW5kKCcucGxheWVyLWFydGlzdCcpLCRwcmV2PWNvbnRhaW5lckRpdi5maW5kKCcucGxheWVyLWNvbnRyb2xzX19wcmV2JyksJG5leHQ9Y29udGFpbmVyRGl2LmZpbmQoJy5wbGF5ZXItY29udHJvbHNfX25leHQnKSwkcGxheT1jb250YWluZXJEaXYuZmluZCgnLnBsYXllci1jb250cm9sc19fcGxheScpLCRzY3J1YmJlcj1jb250YWluZXJEaXYuZmluZCgnLnBsYXllci1zY3J1YmJlcicpLCRoYW5kbGU9Y29udGFpbmVyRGl2LmZpbmQoJy5wbGF5ZXItc2NydWJiZXItaGFuZGxlJyksJGZpbGw9Y29udGFpbmVyRGl2LmZpbmQoJy5wbGF5ZXItc2NydWJiZXJfX2ZpbGwnKSwkdGltZT1jb250YWluZXJEaXYuZmluZCgnLnBsYXllci10aW1lX19wbGF5ZWQnKSwkZHVyPWNvbnRhaW5lckRpdi5maW5kKCcucGxheWVyLXRpbWVfX2R1cmF0aW9uJyk7ZnVuY3Rpb24gdGltZShzZWNvbmRzKXtzZWNvbmRzPU1hdGguZmxvb3Ioc2Vjb25kcyk7Y29uc3QgbWlucz1NYXRoLmZsb29yKHNlY29uZHMvNjApO2NvbnN0IHNlY3M9c2Vjb25kcyU2MDtyZXR1cm4gbWlucytcIjpcIisoc2VjczwxMD9cIjBcIjpcIlwiKStzZWNzO31cbmZ1bmN0aW9uIGNoYW5nZVNvbmcob2Zmc2V0LHBsYXkpe3NvbmdJbmRleCs9b2Zmc2V0O2lmKHNvbmdJbmRleD5wbGF5bGlzdC5sZW5ndGgtMSlzb25nSW5kZXg9MDtpZihzb25nSW5kZXg8MClzb25nSW5kZXg9cGxheWxpc3QubGVuZ3RoLTE7JGF1ZGlvLmF0dHIoJ3NyYycscGxheWxpc3Rbc29uZ0luZGV4XS5tcDMpO2NvbnN0IHNyYz1wbGF5bGlzdFtzb25nSW5kZXhdLnBvc3RlcjskdGl0bGUudGV4dChwbGF5bGlzdFtzb25nSW5kZXhdLnRpdGxlKTskYXJ0aXN0LnRleHQocGxheWxpc3Rbc29uZ0luZGV4XS5hcnRpc3QpOyRkdXIudGV4dChcIi06LS1cIik7JGF1ZGlvLm9uKCdsb2FkZWRtZXRhZGF0YScsZnVuY3Rpb24oKXskZHVyLnRleHQodGltZShhdWRpby5kdXJhdGlvbikpO3NldEhhbmRsZSgwLGF1ZGlvLmR1cmF0aW9uKTt9KTskYXVkaW8ub24oJ2VuZGVkJywoKT0+Y2hhbmdlU29uZygrMSx0cnVlKSk7dmFyIGltZz1uZXcgSW1hZ2UoKTtqUXVlcnkoaW1nKS5hdHRyKFwic3JjXCIsc3JjKS5vbihcImxvYWRcIixmdW5jdGlvbigpeyRwb3N0ZXIuYXR0cignc3JjJyxzcmMpOyRiYWNrZ3JvdW5kLmNzcygnYmFja2dyb3VuZC1pbWFnZScsJ3VybChcIicrc3JjKydcIiknKTt9KS5vbihcImVycm9yXCIsZnVuY3Rpb24oKXtjb25zb2xlLmxvZyhcIkNhbm5vdCBmaW5kIGltYWdlXCIpO30pO2lmKHBsYXkpXG57YXVkaW8ucGxheSgpOyRwbGF5LmZpbmQoJ2knKS5yZW1vdmVDbGFzcygnZmEtcGxheScpLmFkZENsYXNzKCdmYS1wYXVzZScpO31cbmVsc2VcbnskcGxheS5maW5kKCdpJykucmVtb3ZlQ2xhc3MoJ2ZhLXBhdXNlJykuYWRkQ2xhc3MoJ2ZhLXBsYXknKTt9fVxuZnVuY3Rpb24gc2V0SGFuZGxlKHNlY29uZHMsZHVyYXRpb24pe2NvbnN0IHBlcmNlbnQ9c2Vjb25kcy9kdXJhdGlvbioxMDA7JHRpbWUudGV4dCh0aW1lKHNlY29uZHMpKTskaGFuZGxlLmNzcygnbGVmdCcscGVyY2VudCtcIiVcIik7JGZpbGwuY3NzKCd3aWR0aCcscGVyY2VudCtcIiVcIik7fVxuJHBsYXkuY2xpY2soZnVuY3Rpb24oKXtpZihhdWRpby5jdXJyZW50VGltZT4wJiYhYXVkaW8ucGF1c2VkKXthdWRpby5wYXVzZSgpOyRwbGF5LmZpbmQoJ2knKS5yZW1vdmVDbGFzcygnZmEtcGF1c2UnKS5hZGRDbGFzcygnZmEtcGxheScpO31lbHNle2F1ZGlvLnBsYXkoKTskcGxheS5maW5kKCdpJykucmVtb3ZlQ2xhc3MoJ2ZhLXBsYXknKS5hZGRDbGFzcygnZmEtcGF1c2UnKTt9fSk7JHByZXYuY2xpY2soKCk9PmNoYW5nZVNvbmcoLTEsdHJ1ZSkpOyRuZXh0LmNsaWNrKCgpPT5jaGFuZ2VTb25nKCsxLHRydWUpKTskYXVkaW8ub24oXCJ0aW1ldXBkYXRlXCIsKCk9PntzZXRIYW5kbGUoYXVkaW8uY3VycmVudFRpbWUsYXVkaW8uZHVyYXRpb24pO30pOyRoYW5kbGUub24oJ21vdXNlZG93bicsZnVuY3Rpb24oKXtoYW5kbGVEcmFnPXRydWU7YXVkaW8ucGF1c2UoKTt9KTskcGxheWVyLm9uKCdtb3VzZW1vdmUnLGZ1bmN0aW9uKGUpe2lmKGhhbmRsZURyYWcpe2NvbnN0IHdpZHRoPSRzY3J1YmJlci53aWR0aCgpO3ZhciBkaWZmPSRzY3J1YmJlci5vZmZzZXQoKS5sZWZ0LWUuY2xpZW50WDt2YXIgcGVyY2VudD0tZGlmZi93aWR0aCoxMDA7cGVyY2VudD0ocGVyY2VudDwwPzA6KHBlcmNlbnQ+MTAwPzEwMDpwZXJjZW50KSk7dmFyIHNlY29uZHM9cGVyY2VudC8xMDAqYXVkaW8uZHVyYXRpb247c2V0SGFuZGxlKHNlY29uZHMsYXVkaW8uZHVyYXRpb24pO3RpbWVJbj1zZWNvbmRzO319KS5vbignbW91c2V1cCBtb3VzZWxlYXZlJyxmdW5jdGlvbigpe2lmKGhhbmRsZURyYWcpe2hhbmRsZURyYWc9ZmFsc2U7YXVkaW8uY3VycmVudFRpbWU9dGltZUluOyRwbGF5LmZpbmQoJ2knKS5yZW1vdmVDbGFzcygnZmEtcGF1c2UnKS5hZGRDbGFzcygnZmEtcGxheScpO319KTtjaGFuZ2VTb25nKDAsZmFsc2UpO30pO30pO2VsZW1lbnRvckZyb250ZW5kLmhvb2tzLmFkZEFjdGlvbignZnJvbnRlbmQvZWxlbWVudF9yZWFkeS9ncmFuZHJlc3RhdXJhbnQtbW91c2UtZHJpdmVuLXZlcnRpY2FsLWNhcm91c2VsLmRlZmF1bHQnLGZ1bmN0aW9uKCRzY29wZSl7Y2xhc3MgVmVydGljYWxNb3VzZURyaXZlbkNhcm91c2Vse2NvbnN0cnVjdG9yKG9wdGlvbnM9e30pe2NvbnN0IF9kZWZhdWx0cz17Y2Fyb3VzZWw6XCIubW91c2UtZHJpdmVuLXZlcnRpY2FsLWNhcm91c2VsLXdyYXBwZXIgLmpzLWNhcm91c2VsXCIsYmdJbWc6XCIuanMtY2Fyb3VzZWwtYmctaW1nXCIsbGlzdDpcIi5qcy1jYXJvdXNlbC1saXN0XCIsbGlzdEl0ZW06XCIuanMtY2Fyb3VzZWwtbGlzdC1pdGVtXCJ9O3RoaXMucG9zWT0wO3RoaXMuZGVmYXVsdHM9T2JqZWN0LmFzc2lnbih7fSxfZGVmYXVsdHMsb3B0aW9ucyk7dGhpcy5pbml0Q3Vyc29yKCk7dGhpcy5pbml0KCk7dGhpcy5iZ0ltZ0NvbnRyb2xsZXIoKTt9XG5nZXRCZ0ltZ3MoKXtyZXR1cm4gZG9jdW1lbnQucXVlcnlTZWxlY3RvckFsbCh0aGlzLmRlZmF1bHRzLmJnSW1nKTt9XG5nZXRMaXN0SXRlbXMoKXtyZXR1cm4gZG9jdW1lbnQucXVlcnlTZWxlY3RvckFsbCh0aGlzLmRlZmF1bHRzLmxpc3RJdGVtKTt9XG5nZXRMaXN0KCl7cmV0dXJuIGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IodGhpcy5kZWZhdWx0cy5saXN0KTt9XG5nZXRDYXJvdXNlbCgpe3JldHVybiBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKHRoaXMuZGVmYXVsdHMuY2Fyb3VzZWwpO31cbmluaXQoKXtUd2Vlbk1heC5zZXQodGhpcy5nZXRCZ0ltZ3MoKSx7YXV0b0FscGhhOjAsc2NhbGU6MS4wNX0pO1R3ZWVuTWF4LnNldCh0aGlzLmdldEJnSW1ncygpWzBdLHthdXRvQWxwaGE6MSxzY2FsZToxfSk7dGhpcy5saXN0SXRlbXM9dGhpcy5nZXRMaXN0SXRlbXMoKS5sZW5ndGgtMTt0aGlzLmxpc3RPcGFjaXR5Q29udHJvbGxlcigwKTt9XG5pbml0Q3Vyc29yKCl7aWYoalF1ZXJ5KHdpbmRvdykud2lkdGgoKT4xMDI0KVxue2NvbnN0IGxpc3RIZWlnaHQ9dGhpcy5nZXRMaXN0KCkuY2xpZW50SGVpZ2h0O2NvbnN0IGNhcm91c2VsSGVpZ2h0PXRoaXMuZ2V0Q2Fyb3VzZWwoKS5jbGllbnRIZWlnaHQ7Y29uc3QgY2Fyb3VzZWxQb3M9dGhpcy5nZXRDYXJvdXNlbCgpLmdldEJvdW5kaW5nQ2xpZW50UmVjdCgpO2NvbnN0IGNhcm91c2VsUG9zWT1wYXJzZUludChjYXJvdXNlbFBvcy50b3ApO3RoaXMuZ2V0Q2Fyb3VzZWwoKS5hZGRFdmVudExpc3RlbmVyKFwibW91c2Vtb3ZlXCIsZXZlbnQ9Pnt0aGlzLnBvc1k9cGFyc2VJbnQoZXZlbnQucGFnZVktY2Fyb3VzZWxQb3NZKS10aGlzLmdldENhcm91c2VsKCkub2Zmc2V0VG9wO2xldCBvZmZzZXQ9LXRoaXMucG9zWS9jYXJvdXNlbEhlaWdodCpsaXN0SGVpZ2h0O1R3ZWVuTWF4LnRvKHRoaXMuZ2V0TGlzdCgpLDAuMyx7eTpvZmZzZXQsZWFzZTpQb3dlcjQuZWFzZU91dH0pO30sZmFsc2UpO319XG5iZ0ltZ0NvbnRyb2xsZXIoKXtmb3IoY29uc3QgbGluayBvZiB0aGlzLmdldExpc3RJdGVtcygpKXtsaW5rLmFkZEV2ZW50TGlzdGVuZXIoXCJtb3VzZWVudGVyXCIsZXY9PntsZXQgY3VycmVudElkPWV2LmN1cnJlbnRUYXJnZXQuZGF0YXNldC5pdGVtSWQ7dGhpcy5saXN0T3BhY2l0eUNvbnRyb2xsZXIoY3VycmVudElkKTtUd2Vlbk1heC50byhldi5jdXJyZW50VGFyZ2V0LDAuMyx7YXV0b0FscGhhOjF9KTtUd2Vlbk1heC50byhcIi5pcy12aXNpYmxlXCIsMC4yLHthdXRvQWxwaGE6MCxzY2FsZToxLjA1fSk7aWYoIXRoaXMuZ2V0QmdJbWdzKClbY3VycmVudElkXS5jbGFzc0xpc3QuY29udGFpbnMoXCJpcy12aXNpYmxlXCIpKXt0aGlzLmdldEJnSW1ncygpW2N1cnJlbnRJZF0uY2xhc3NMaXN0LmFkZChcImlzLXZpc2libGVcIik7fVxuVHdlZW5NYXgudG8odGhpcy5nZXRCZ0ltZ3MoKVtjdXJyZW50SWRdLDAuNix7YXV0b0FscGhhOjEsc2NhbGU6MX0pO30pO319XG5saXN0T3BhY2l0eUNvbnRyb2xsZXIoaWQpe2lkPXBhcnNlSW50KGlkKTtsZXQgYWJvdmVDdXJyZW50PXRoaXMubGlzdEl0ZW1zLWlkO2xldCBiZWxvd0N1cnJlbnQ9cGFyc2VJbnQoaWQpO2lmKGFib3ZlQ3VycmVudD4wKXtmb3IobGV0IGk9MTtpPD1hYm92ZUN1cnJlbnQ7aSsrKXtsZXQgb3BhY2l0eT0wLjUvaTtsZXQgb2Zmc2V0PTUqaTtUd2Vlbk1heC50byh0aGlzLmdldExpc3RJdGVtcygpW2lkK2ldLDAuNSx7YXV0b0FscGhhOm9wYWNpdHkseDpvZmZzZXQsZWFzZTpQb3dlcjMuZWFzZU91dH0pO319XG5pZihiZWxvd0N1cnJlbnQ+MCl7Zm9yKGxldCBpPTA7aTw9YmVsb3dDdXJyZW50O2krKyl7bGV0IG9wYWNpdHk9MC41L2k7bGV0IG9mZnNldD01Kmk7VHdlZW5NYXgudG8odGhpcy5nZXRMaXN0SXRlbXMoKVtpZC1pXSwwLjUse2F1dG9BbHBoYTpvcGFjaXR5LHg6b2Zmc2V0LGVhc2U6UG93ZXIzLmVhc2VPdXR9KTt9fX19XG5uZXcgVmVydGljYWxNb3VzZURyaXZlbkNhcm91c2VsKCk7fSk7ZWxlbWVudG9yRnJvbnRlbmQuaG9va3MuYWRkQWN0aW9uKCdmcm9udGVuZC9lbGVtZW50X3JlYWR5L2dyYW5kcmVzdGF1cmFudC1zbGlkZXItc3luY2hyb25pemVkLWNhcm91c2VsLmRlZmF1bHQnLGZ1bmN0aW9uKCRzY29wZSl7alF1ZXJ5KFwiLnN5bmNocm9uaXplZC1jYXJvdXNlbC1zbGlkZXItd3JhcHBlclwiKS5lYWNoKGZ1bmN0aW9uKCl7dmFyIHNsaWRlcklEPWpRdWVyeSh0aGlzKS5hdHRyKCdpZCcpO3ZhciBzbGlkZXJzQ29udGFpbmVyPWRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoXCIjXCIrc2xpZGVySUQpO3ZhciBjb3VudFNsaWRlPWpRdWVyeSh0aGlzKS5hdHRyKCdkYXRhLWNvdW50c2xpZGUnKTt2YXIgbXNOdW1iZXJzPW5ldyBNb21lbnR1bVNsaWRlcih7ZWw6c2xpZGVyc0NvbnRhaW5lcixjc3NDbGFzczpcIm1zLS1udW1iZXJzXCIscmFuZ2U6WzEsY291bnRTbGlkZV0scmFuZ2VDb250ZW50OmZ1bmN0aW9uKGkpe3JldHVyblwiMFwiK2k7fSxzdHlsZTp7dHJhbnNmb3JtOlt7c2NhbGU6WzAuNCwxXX1dLG9wYWNpdHk6WzAsMV19LGludGVyYWN0aXZlOmZhbHNlfSk7dmFyIHRpdGxlcz1KU09OLnBhcnNlKGpRdWVyeSh0aGlzKS5hdHRyKCdkYXRhLXNsaWRldGl0bGVzJykpO3ZhciBtc1RpdGxlcz1uZXcgTW9tZW50dW1TbGlkZXIoe2VsOnNsaWRlcnNDb250YWluZXIsY3NzQ2xhc3M6XCJtcy0tdGl0bGVzXCIscmFuZ2U6WzAscGFyc2VJbnQoY291bnRTbGlkZS0xKV0scmFuZ2VDb250ZW50OmZ1bmN0aW9uKGkpe3JldHVyblwiPGgzPlwiK3RpdGxlc1tpXStcIjwvaDM+XCI7fSx2ZXJ0aWNhbDp0cnVlLHJldmVyc2U6dHJ1ZSxzdHlsZTp7b3BhY2l0eTpbMCwxXX0saW50ZXJhY3RpdmU6ZmFsc2V9KTt2YXIgYnV0dG9uVGl0bGVzPUpTT04ucGFyc2UoalF1ZXJ5KHRoaXMpLmF0dHIoJ2RhdGEtc2xpZGVidXR0b250aXRsZXMnKSk7dmFyIGJ1dHRvblVybHM9SlNPTi5wYXJzZShqUXVlcnkodGhpcykuYXR0cignZGF0YS1zbGlkZWJ1dHRvbnVybHMnKSk7dmFyIG1zTGlua3M9bmV3IE1vbWVudHVtU2xpZGVyKHtlbDpzbGlkZXJzQ29udGFpbmVyLGNzc0NsYXNzOlwibXMtLWxpbmtzXCIscmFuZ2U6WzAscGFyc2VJbnQoY291bnRTbGlkZS0xKV0scmFuZ2VDb250ZW50OmZ1bmN0aW9uKGkpe3JldHVyblwiPGEgaHJlZj1cXFwiXCIrYnV0dG9uVXJsc1tpXStcIlxcXCIgY2xhc3M9XFxcIm1zLXNsaWRlLWxpbmtcXFwiPlwiK2J1dHRvblRpdGxlc1tpXStcIjwvYT5cIjt9LHZlcnRpY2FsOnRydWUsaW50ZXJhY3RpdmU6ZmFsc2V9KTt2YXIgcGFnaW5hdGlvbklEPWpRdWVyeSh0aGlzKS5hdHRyKCdkYXRhLXBhZ2luYXRpb24nKTt2YXIgcGFnaW5hdGlvbj1kb2N1bWVudC5xdWVyeVNlbGVjdG9yKFwiI1wiK3BhZ2luYXRpb25JRCk7dmFyIHBhZ2luYXRpb25JdGVtcz1bXS5zbGljZS5jYWxsKHBhZ2luYXRpb24uY2hpbGRyZW4pO3ZhciBpbWFnZXM9SlNPTi5wYXJzZShqUXVlcnkodGhpcykuYXR0cignZGF0YS1zbGlkZWltYWdlcycpKTt2YXIgbXNJbWFnZXM9bmV3IE1vbWVudHVtU2xpZGVyKHtlbDpzbGlkZXJzQ29udGFpbmVyLGNzc0NsYXNzOlwibXMtLWltYWdlc1wiLHJhbmdlOlswLHBhcnNlSW50KGNvdW50U2xpZGUtMSldLHJhbmdlQ29udGVudDpmdW5jdGlvbihpKXtyZXR1cm5cIjxkaXYgY2xhc3M9XFxcIm1zLXNsaWRlLWltYWdlLWNvbnRhaW5lclxcXCI+PGRpdiBjbGFzcz1cXFwibXMtc2xpZGUtaW1hZ2VcXFwiIHN0eWxlPVxcXCJiYWNrZ3JvdW5kLWltYWdlOiB1cmwoJ1wiK2ltYWdlc1tpXStcIicpXFxcIj48L2Rpdj48L2Rpdj5cIjt9LHN5bmM6W21zTnVtYmVycyxtc1RpdGxlcyxtc0xpbmtzXSxzdHlsZTp7XCIubXMtc2xpZGUtaW1hZ2VcIjp7dHJhbnNmb3JtOlt7c2NhbGU6WzEuNSwxXX1dfX0sY2hhbmdlOmZ1bmN0aW9uKG5ld0luZGV4LG9sZEluZGV4KXtpZih0eXBlb2Ygb2xkSW5kZXghPT1cInVuZGVmaW5lZFwiKXtwYWdpbmF0aW9uSXRlbXNbb2xkSW5kZXhdLmNsYXNzTGlzdC5yZW1vdmUoXCJwYWdpbmF0aW9uLWl0ZW0tLWFjdGl2ZVwiKTt9XG5wYWdpbmF0aW9uSXRlbXNbbmV3SW5kZXhdLmNsYXNzTGlzdC5hZGQoXCJwYWdpbmF0aW9uLWl0ZW0tLWFjdGl2ZVwiKTt9fSk7cGFnaW5hdGlvbi5hZGRFdmVudExpc3RlbmVyKFwiY2xpY2tcIixmdW5jdGlvbihlKXtpZihlLnRhcmdldC5tYXRjaGVzKFwiLnBhZ2luYXRpb24tYnV0dG9uXCIpKXt2YXIgaW5kZXg9cGFnaW5hdGlvbkl0ZW1zLmluZGV4T2YoZS50YXJnZXQucGFyZW50Tm9kZSk7bXNJbWFnZXMuc2VsZWN0KGluZGV4KTt9fSk7fSk7fSk7ZWxlbWVudG9yRnJvbnRlbmQuaG9va3MuYWRkQWN0aW9uKCdmcm9udGVuZC9lbGVtZW50X3JlYWR5L2dyYW5kcmVzdGF1cmFudC1mbGlwLWJveC5kZWZhdWx0JyxmdW5jdGlvbigkc2NvcGUpe3ZhciBjb3VudFNxdWFyZT1qUXVlcnkoJy5zcXVhcmUnKS5sZW5ndGg7Zm9yKGk9MDtpPGNvdW50U3F1YXJlO2krKyl7dmFyIGZpcnN0SW1hZ2U9alF1ZXJ5KCcuc3F1YXJlJykuZXEoW2ldKTt2YXIgc2Vjb25kSW1hZ2U9alF1ZXJ5KCcuc3F1YXJlMicpLmVxKFtpXSk7dmFyIGdldEltYWdlPWZpcnN0SW1hZ2UuYXR0cignZGF0YS1pbWFnZScpO3ZhciBnZXRJbWFnZTI9c2Vjb25kSW1hZ2UuYXR0cignZGF0YS1pbWFnZScpO2ZpcnN0SW1hZ2UuY3NzKCdiYWNrZ3JvdW5kLWltYWdlJywndXJsKCcrZ2V0SW1hZ2UrJyknKTtzZWNvbmRJbWFnZS5jc3MoJ2JhY2tncm91bmQtaW1hZ2UnLCd1cmwoJytnZXRJbWFnZTIrJyknKTt9XG5qUXVlcnkoJy5mbGlwLWJveC13cmFwcGVyJykub24oJ2NsaWNrJyxmdW5jdGlvbigpe2pRdWVyeSh0aGlzKS50cmlnZ2VyKFwibW91c2VvdmVyXCIpO30pO30pO2VsZW1lbnRvckZyb250ZW5kLmhvb2tzLmFkZEFjdGlvbignZnJvbnRlbmQvZWxlbWVudF9yZWFkeS9ncmFuZHJlc3RhdXJhbnQtc2xpZGVyLXpvb20uZGVmYXVsdCcsZnVuY3Rpb24oJHNjb3BlKXt2YXIgX2NyZWF0ZUNsYXNzPWZ1bmN0aW9uKCl7ZnVuY3Rpb24gZGVmaW5lUHJvcGVydGllcyh0YXJnZXQscHJvcHMpe2Zvcih2YXIgaT0wO2k8cHJvcHMubGVuZ3RoO2krKyl7dmFyIGRlc2NyaXB0b3I9cHJvcHNbaV07ZGVzY3JpcHRvci5lbnVtZXJhYmxlPWRlc2NyaXB0b3IuZW51bWVyYWJsZXx8ZmFsc2U7ZGVzY3JpcHRvci5jb25maWd1cmFibGU9dHJ1ZTtpZihcInZhbHVlXCJpbiBkZXNjcmlwdG9yKWRlc2NyaXB0b3Iud3JpdGFibGU9dHJ1ZTtPYmplY3QuZGVmaW5lUHJvcGVydHkodGFyZ2V0LGRlc2NyaXB0b3Iua2V5LGRlc2NyaXB0b3IpO319cmV0dXJuIGZ1bmN0aW9uKENvbnN0cnVjdG9yLHByb3RvUHJvcHMsc3RhdGljUHJvcHMpe2lmKHByb3RvUHJvcHMpZGVmaW5lUHJvcGVydGllcyhDb25zdHJ1Y3Rvci5wcm90b3R5cGUscHJvdG9Qcm9wcyk7aWYoc3RhdGljUHJvcHMpZGVmaW5lUHJvcGVydGllcyhDb25zdHJ1Y3RvcixzdGF0aWNQcm9wcyk7cmV0dXJuIENvbnN0cnVjdG9yO307fSgpO2Z1bmN0aW9uIF9jbGFzc0NhbGxDaGVjayhpbnN0YW5jZSxDb25zdHJ1Y3Rvcil7aWYoIShpbnN0YW5jZSBpbnN0YW5jZW9mIENvbnN0cnVjdG9yKSl7dGhyb3cgbmV3IFR5cGVFcnJvcihcIkNhbm5vdCBjYWxsIGEgY2xhc3MgYXMgYSBmdW5jdGlvblwiKTt9fVxudmFyICR3aW5kb3c9JCh3aW5kb3cpO3ZhciAkYm9keT0kKCdib2R5Jyk7dmFyIFNsaWRlc2hvdz1mdW5jdGlvbigpe2Z1bmN0aW9uIFNsaWRlc2hvdygpe3ZhciBfdGhpcz10aGlzO3ZhciB1c2VyT3B0aW9ucz1hcmd1bWVudHMubGVuZ3RoPjAmJmFyZ3VtZW50c1swXSE9PXVuZGVmaW5lZD9hcmd1bWVudHNbMF06e307X2NsYXNzQ2FsbENoZWNrKHRoaXMsU2xpZGVzaG93KTt2YXIgdGltZXI9JCgnLnNsaWRlci16b29tLXdyYXBwZXInKS5hdHRyKCdkYXRhLWF1dG9wbGF5Jyk7dmFyIGF1dG9wbGF5PXRydWU7aWYodGltZXI9PTApXG57dGltZXI9ZmFsc2U7YXV0b3BsYXk9ZmFsc2U7fVxudmFyIHBhZ2luYXRpb249JCgnLnNsaWRlci16b29tLXdyYXBwZXInKS5hdHRyKCdkYXRhLXBhZ2luYXRpb24nKTtpZihwYWdpbmF0aW9uPT0wKVxue3ZhciBwYWdpbmF0aW9uPWZhbHNlO31cbmVsc2Vcbnt2YXIgcGFnaW5hdGlvbj10cnVlO31cbnZhciBkZWZhdWx0T3B0aW9ucz17JGVsOiQoJy5zbGlkZXItem9vbS13cmFwcGVyJyksc2hvd0Fycm93czpmYWxzZSxzaG93cGFnaW5hdGlvbjpmYWxzZSxkdXJhdGlvbjp0aW1lcixhdXRvcGxheTphdXRvcGxheX07dmFyIG9wdGlvbnM9T2JqZWN0LmFzc2lnbih7fSxkZWZhdWx0T3B0aW9ucyx1c2VyT3B0aW9ucyk7dGhpcy4kZWw9b3B0aW9ucy4kZWw7dGhpcy5tYXhTbGlkZT10aGlzLiRlbC5maW5kKCQoJy5qcy1zbGlkZXItaG9tZS1zbGlkZScpKS5sZW5ndGg7dGhpcy5zaG93QXJyb3dzPXRoaXMubWF4U2xpZGU+MT9vcHRpb25zLnNob3dBcnJvd3M6ZmFsc2U7dGhpcy5zaG93cGFnaW5hdGlvbj1wYWdpbmF0aW9uO3RoaXMuY3VycmVudFNsaWRlPTE7dGhpcy5pc0FuaW1hdGluZz1mYWxzZTt0aGlzLmFuaW1hdGlvbkR1cmF0aW9uPTEyMDA7dGhpcy5hdXRvcGxheVNwZWVkPW9wdGlvbnMuZHVyYXRpb247dGhpcy5pbnRlcnZhbDt0aGlzLiRjb250cm9scz10aGlzLiRlbC5maW5kKCcuanMtc2xpZGVyLWhvbWUtYnV0dG9uJyk7dGhpcy5hdXRvcGxheT10aGlzLm1heFNsaWRlPjE/b3B0aW9ucy5hdXRvcGxheTpmYWxzZTt0aGlzLiRlbC5vbignY2xpY2snLCcuanMtc2xpZGVyLWhvbWUtbmV4dCcsZnVuY3Rpb24oZXZlbnQpe3JldHVybiBfdGhpcy5uZXh0U2xpZGUoKTt9KTt0aGlzLiRlbC5vbignY2xpY2snLCcuanMtc2xpZGVyLWhvbWUtcHJldicsZnVuY3Rpb24oZXZlbnQpe3JldHVybiBfdGhpcy5wcmV2U2xpZGUoKTt9KTt0aGlzLiRlbC5vbignY2xpY2snLCcuanMtcGFnaW5hdGlvbi1pdGVtJyxmdW5jdGlvbihldmVudCl7aWYoIV90aGlzLmlzQW5pbWF0aW5nKXtfdGhpcy5wcmV2ZW50Q2xpY2soKTtfdGhpcy5nb1RvU2xpZGUoZXZlbnQudGFyZ2V0LmRhdGFzZXQuc2xpZGUpO319KTt0aGlzLmluaXQoKTt9XG5fY3JlYXRlQ2xhc3MoU2xpZGVzaG93LFt7a2V5Oidpbml0Jyx2YWx1ZTpmdW5jdGlvbiBpbml0KCl7dGhpcy5nb1RvU2xpZGUoMSk7aWYodGhpcy5hdXRvcGxheSl7dGhpcy5zdGFydEF1dG9wbGF5KCk7fVxuaWYodGhpcy5zaG93cGFnaW5hdGlvbil7dmFyIHBhZ2luYXRpb25OdW1iZXI9dGhpcy5tYXhTbGlkZTt2YXIgcGFnaW5hdGlvbj0nPGRpdiBjbGFzcz1cInBhZ2luYXRpb25cIj48ZGl2IGNsYXNzPVwiY29udGFpbmVyXCI+Jztmb3IodmFyIGk9MDtpPHRoaXMubWF4U2xpZGU7aSsrKXt2YXIgaXRlbT0nPHNwYW4gY2xhc3M9XCJwYWdpbmF0aW9uLWl0ZW0ganMtcGFnaW5hdGlvbi1pdGVtICcrKGk9PT0wPydpcy1jdXJyZW50JzonJykrJ1wiIGRhdGEtc2xpZGU9JysoaSsxKSsnPicrKGkrMSkrJzwvc3Bhbj4nO3BhZ2luYXRpb249cGFnaW5hdGlvbitpdGVtO31cbnBhZ2luYXRpb249cGFnaW5hdGlvbisnPC9kaXY+PC9kaXY+Jzt0aGlzLiRlbC5hcHBlbmQocGFnaW5hdGlvbik7fX19LHtrZXk6J3ByZXZlbnRDbGljaycsdmFsdWU6ZnVuY3Rpb24gcHJldmVudENsaWNrKCl7dmFyIF90aGlzMj10aGlzO3RoaXMuaXNBbmltYXRpbmc9dHJ1ZTt0aGlzLiRjb250cm9scy5wcm9wKCdkaXNhYmxlZCcsdHJ1ZSk7Y2xlYXJJbnRlcnZhbCh0aGlzLmludGVydmFsKTtzZXRUaW1lb3V0KGZ1bmN0aW9uKCl7X3RoaXMyLmlzQW5pbWF0aW5nPWZhbHNlO190aGlzMi4kY29udHJvbHMucHJvcCgnZGlzYWJsZWQnLGZhbHNlKTtpZihfdGhpczIuYXV0b3BsYXkpe190aGlzMi5zdGFydEF1dG9wbGF5KCk7fX0sdGhpcy5hbmltYXRpb25EdXJhdGlvbik7fX0se2tleTonZ29Ub1NsaWRlJyx2YWx1ZTpmdW5jdGlvbiBnb1RvU2xpZGUoaW5kZXgpe3RoaXMuY3VycmVudFNsaWRlPXBhcnNlSW50KGluZGV4KTtpZih0aGlzLmN1cnJlbnRTbGlkZT50aGlzLm1heFNsaWRlKXt0aGlzLmN1cnJlbnRTbGlkZT0xO31cbmlmKHRoaXMuY3VycmVudFNsaWRlPT09MCl7dGhpcy5jdXJyZW50U2xpZGU9dGhpcy5tYXhTbGlkZTt9XG52YXIgbmV3Q3VycmVudD10aGlzLiRlbC5maW5kKCcuanMtc2xpZGVyLWhvbWUtc2xpZGVbZGF0YS1zbGlkZT1cIicrdGhpcy5jdXJyZW50U2xpZGUrJ1wiXScpO3ZhciBuZXdwcmV2PXRoaXMuY3VycmVudFNsaWRlPT09MT90aGlzLiRlbC5maW5kKCcuanMtc2xpZGVyLWhvbWUtc2xpZGUnKS5sYXN0KCk6bmV3Q3VycmVudC5wcmV2KCcuanMtc2xpZGVyLWhvbWUtc2xpZGUnKTt2YXIgbmV3TmV4dD10aGlzLmN1cnJlbnRTbGlkZT09PXRoaXMubWF4U2xpZGU/dGhpcy4kZWwuZmluZCgnLmpzLXNsaWRlci1ob21lLXNsaWRlJykuZmlyc3QoKTpuZXdDdXJyZW50Lm5leHQoJy5qcy1zbGlkZXItaG9tZS1zbGlkZScpO3RoaXMuJGVsLmZpbmQoJy5qcy1zbGlkZXItaG9tZS1zbGlkZScpLnJlbW92ZUNsYXNzKCdpcy1wcmV2IGlzLW5leHQgaXMtY3VycmVudCcpO3RoaXMuJGVsLmZpbmQoJy5qcy1wYWdpbmF0aW9uLWl0ZW0nKS5yZW1vdmVDbGFzcygnaXMtY3VycmVudCcpO2lmKHRoaXMubWF4U2xpZGU+MSl7bmV3cHJldi5hZGRDbGFzcygnaXMtcHJldicpO25ld05leHQuYWRkQ2xhc3MoJ2lzLW5leHQnKTt9XG5uZXdDdXJyZW50LmFkZENsYXNzKCdpcy1jdXJyZW50Jyk7dGhpcy4kZWwuZmluZCgnLmpzLXBhZ2luYXRpb24taXRlbVtkYXRhLXNsaWRlPVwiJyt0aGlzLmN1cnJlbnRTbGlkZSsnXCJdJykuYWRkQ2xhc3MoJ2lzLWN1cnJlbnQnKTt9fSx7a2V5OiduZXh0U2xpZGUnLHZhbHVlOmZ1bmN0aW9uIG5leHRTbGlkZSgpe3RoaXMucHJldmVudENsaWNrKCk7dGhpcy5nb1RvU2xpZGUodGhpcy5jdXJyZW50U2xpZGUrMSk7fX0se2tleToncHJldlNsaWRlJyx2YWx1ZTpmdW5jdGlvbiBwcmV2U2xpZGUoKXt0aGlzLnByZXZlbnRDbGljaygpO3RoaXMuZ29Ub1NsaWRlKHRoaXMuY3VycmVudFNsaWRlLTEpO319LHtrZXk6J3N0YXJ0QXV0b3BsYXknLHZhbHVlOmZ1bmN0aW9uIHN0YXJ0QXV0b3BsYXkoKXt2YXIgX3RoaXMzPXRoaXM7dGhpcy5pbnRlcnZhbD1zZXRJbnRlcnZhbChmdW5jdGlvbigpe2lmKCFfdGhpczMuaXNBbmltYXRpbmcpe190aGlzMy5uZXh0U2xpZGUoKTt9fSx0aGlzLmF1dG9wbGF5U3BlZWQpO319LHtrZXk6J2Rlc3Ryb3knLHZhbHVlOmZ1bmN0aW9uIGRlc3Ryb3koKXt0aGlzLiRlbC5vZmYoKTt9fV0pO3JldHVybiBTbGlkZXNob3c7fSgpOyhmdW5jdGlvbigpe3ZhciBsb2FkZWQ9ZmFsc2U7dmFyIG1heExvYWQ9MzAwMDtmdW5jdGlvbiBsb2FkKCl7dmFyIG9wdGlvbnM9e3Nob3dwYWdpbmF0aW9uOnRydWV9O3ZhciBzbGlkZVNob3c9bmV3IFNsaWRlc2hvdyhvcHRpb25zKTt9XG5mdW5jdGlvbiBhZGRMb2FkQ2xhc3MoKXskYm9keS5hZGRDbGFzcygnaXMtbG9hZGVkJyk7c2V0VGltZW91dChmdW5jdGlvbigpeyRib2R5LmFkZENsYXNzKCdpcy1hbmltYXRlZCcpO30sNjAwKTt9XG4kd2luZG93Lm9uKCdsb2FkJyxmdW5jdGlvbigpe2lmKCFsb2FkZWQpe2xvYWRlZD10cnVlO2xvYWQoKTt9fSk7c2V0VGltZW91dChmdW5jdGlvbigpe2lmKCFsb2FkZWQpe2xvYWRlZD10cnVlO2xvYWQoKTt9fSxtYXhMb2FkKTthZGRMb2FkQ2xhc3MoKTt9KSgpO30pO2VsZW1lbnRvckZyb250ZW5kLmhvb2tzLmFkZEFjdGlvbignZnJvbnRlbmQvZWxlbWVudF9yZWFkeS9ncmFuZHJlc3RhdXJhbnQtY291cnNlLWdyaWQuZGVmYXVsdCcsZnVuY3Rpb24oJHNjb3BlKXtqUXVlcnkoJy5jb3Vyc2VfdG9vbHRpcCcpLnRvb2x0aXBzdGVyKHtwb3NpdGlvbjoncmlnaHQnLG11bHRpcGxlOnRydWUsY29udGVudENsb25pbmc6dHJ1ZSx0aGVtZTondG9vbHRpcHN0ZXItc2hhZG93JyxtaW5XaWR0aDozMDAsbWF4V2lkdGg6MzAwLGRlbGF5OjUwLGludGVyYWN0aXZlOnRydWUsfSk7fSk7ZWxlbWVudG9yRnJvbnRlbmQuaG9va3MuYWRkQWN0aW9uKCdmcm9udGVuZC9lbGVtZW50X3JlYWR5L2dyYW5kcmVzdGF1cmFudC1zZWFyY2guZGVmYXVsdCcsZnVuY3Rpb24oJHNjb3BlKXtqUXVlcnkoXCIuZ3JhbmRyZXN0YXVyYW50LXNlYXJjaC1pY29uXCIpLmVhY2goZnVuY3Rpb24oKXt2YXIgaWNvbklucHV0PWpRdWVyeSh0aGlzKS5maW5kKCdhJyk7aWNvbklucHV0LmJpbmQoJ2NsaWNrJyxmdW5jdGlvbigpe3ZhciBvcGVuRGl2PWpRdWVyeSh0aGlzKS5hdHRyKCdkYXRhLW9wZW4nKTtqUXVlcnkoJyMnK29wZW5EaXYpLmFkZENsYXNzKCdhY3RpdmUnKTt2YXIgaXNUb3VjaD0oJ29udG91Y2hzdGFydCdpbiBkb2N1bWVudC5kb2N1bWVudEVsZW1lbnQpO2lmKGlzVG91Y2gpe2pRdWVyeSgnIycrb3BlbkRpdikuZmluZCgnLmdyYW5kcmVzdGF1cmFudC1zZWFyY2gtaW5uZXInKS5hZGRDbGFzcygndG91Y2gnKTt9XG5qUXVlcnkoJ2JvZHknKS5hZGRDbGFzcygnZWxlbWVudG9yLW5vLW92ZXJmbG93Jyk7alF1ZXJ5KCdib2R5JykuYWRkQ2xhc3MoJ2VsZW1lbnRvci1zZWFyY2gtYWN0aXZhdGUnKTtzZXRUaW1lb3V0KGZ1bmN0aW9uKCl7alF1ZXJ5KCcjJytvcGVuRGl2KS5maW5kKCcudGdfc2VhcmNoX2Zvcm0uYXV0b2NvbXBsZXRlX2Zvcm0nKS5maW5kKCdpbnB1dCNzJykudHJpZ2dlcignZm9jdXMnKTt9LDMwMCk7fSk7fSk7alF1ZXJ5KFwiLnRnX3NlYXJjaF9mb3JtLmF1dG9jb21wbGV0ZV9mb3JtXCIpLmVhY2goZnVuY3Rpb24oKXt2YXIgd3JhcHBlcj1qUXVlcnkodGhpcykuYXR0cignZGF0YS1vcGVuJyk7dmFyIGZvcm1JbnB1dD1qUXVlcnkodGhpcykuZmluZCgnaW5wdXRbbmFtZT1cInNcIl0nKTt2YXIgcmVzdWx0RGl2PWpRdWVyeSh0aGlzKS5hdHRyKCdkYXRhLXJlc3VsdCcpO3ZhciBpc1RvdWNoPSgnb250b3VjaHN0YXJ0J2luIGRvY3VtZW50LmRvY3VtZW50RWxlbWVudCk7aWYoIWlzVG91Y2gpe2Zvcm1JbnB1dC5vbignaW5wdXQnLGZ1bmN0aW9uKCl7aWYoalF1ZXJ5KHRoaXMpLnZhbCgpLmxlbmd0aD4xKVxue2pRdWVyeS5hamF4KHt1cmw6dGdBamF4LmFqYXh1cmwsdHlwZTonUE9TVCcsZGF0YTonYWN0aW9uPWdyYW5kcmVzdGF1cmFudF9hamF4X3NlYXJjaF9yZXN1bHQmJytqUXVlcnkodGhpcykuc2VyaWFsaXplKCksc3VjY2VzczpmdW5jdGlvbihyZXN1bHRzKXtqUXVlcnkoXCIjXCIrcmVzdWx0RGl2KS5odG1sKHJlc3VsdHMpO2lmKHJlc3VsdHMhPScnKVxue2pRdWVyeShcIiNcIityZXN1bHREaXYpLmFkZENsYXNzKCd2aXNpYmxlJyk7alF1ZXJ5KFwiI1wiK3Jlc3VsdERpdikuc2hvdygpO2pRdWVyeShcIiNcIityZXN1bHREaXYpLmNzcygnei1pbmRleCcsOTkpO2pRdWVyeShcIiNcIityZXN1bHREaXYrXCIgdWwgbGkgYVwiKS5tb3VzZWRvd24oZnVuY3Rpb24oKXtqUXVlcnkoXCIjXCIrcmVzdWx0RGl2KS5hZGRDbGFzcygndmlzaWJsZScpO2pRdWVyeShcIiNcIityZXN1bHREaXYpLmF0dHIoJ2RhdGEtbW91c2Vkb3duJywndHJ1ZScpO2xvY2F0aW9uLmhyZWY9alF1ZXJ5KHRoaXMpLmF0dHIoJ2hyZWYnKTt9KTt9XG5lbHNlXG57alF1ZXJ5KFwiI1wiK3Jlc3VsdERpdikuaGlkZSgpO319fSk7fVxuZWxzZVxue2pRdWVyeShcIiNcIityZXN1bHREaXYpLmh0bWwoJycpO319KTt9XG5mb3JtSW5wdXQuYmluZCgnY2xpY2snLGZ1bmN0aW9uKCl7alF1ZXJ5KFwiI1wiK3Jlc3VsdERpdikuYWRkQ2xhc3MoJ3Zpc2libGUnKTtqUXVlcnkoXCIjXCIrcmVzdWx0RGl2KS5hdHRyKCdkYXRhLW1vdXNlZG93bicsJ3RydWUnKTt9KTtmb3JtSW5wdXQuYmluZCgnZm9jdXMnLGZ1bmN0aW9uKCl7alF1ZXJ5KFwiI1wiK3Jlc3VsdERpdikuYWRkQ2xhc3MoJ3Zpc2libGUnKTtmb3JtSW5wdXQuYWRkQ2xhc3MoJ3Zpc2libGUnKTt9KTtmb3JtSW5wdXQuYmluZCgnYmx1cicsZnVuY3Rpb24oKXtqUXVlcnkoXCIjXCIrcmVzdWx0RGl2KS5yZW1vdmVDbGFzcygndmlzaWJsZScpO2Zvcm1JbnB1dC5yZW1vdmVDbGFzcygndmlzaWJsZScpO30pO2pRdWVyeShcIiNcIit3cmFwcGVyKS5iaW5kKCdjbGljaycsZnVuY3Rpb24oKXtpZighZm9ybUlucHV0Lmhhc0NsYXNzKCd2aXNpYmxlJykpXG57aWYoalF1ZXJ5KFwiI1wiK3Jlc3VsdERpdikuYXR0cignZGF0YS1tb3VzZWRvd24nKSE9J3RydWUnKVxue2pRdWVyeShcIiNcIityZXN1bHREaXYpLnJlbW92ZUNsYXNzKCd2aXNpYmxlJyk7fVxualF1ZXJ5KCcjJyt3cmFwcGVyKS5yZW1vdmVDbGFzcygnYWN0aXZlJyk7alF1ZXJ5KCdib2R5JykucmVtb3ZlQ2xhc3MoJ2VsZW1lbnRvci1uby1vdmVyZmxvdycpO2pRdWVyeSgnYm9keScpLnJlbW92ZUNsYXNzKCdlbGVtZW50b3Itc2VhcmNoLWFjdGl2YXRlJyk7fX0pO30pO30pO2VsZW1lbnRvckZyb250ZW5kLmhvb2tzLmFkZEFjdGlvbignZnJvbnRlbmQvZWxlbWVudF9yZWFkeS9ncmFuZHJlc3RhdXJhbnQtc2VydmljZS1ncmlkLmRlZmF1bHQnLGZ1bmN0aW9uKCRzY29wZSl7alF1ZXJ5KFwiLnNlcnZpY2UtZ3JpZC13cmFwcGVyXCIpLm1vdXNlb3ZlcihmdW5jdGlvbigpe3ZhciBkYXRhSG92ZXJZPWpRdWVyeSh0aGlzKS5hdHRyKCdkYXRhLWhvdmVyWScpO2pRdWVyeSh0aGlzKS5maW5kKCcuaGVhZGVyLXdyYXAnKS5jc3MoJ3RyYW5zZm9ybScsJ3RyYW5zbGF0ZVkoLScrZGF0YUhvdmVyWSsncHgpJyk7fSkubW91c2VsZWF2ZShmdW5jdGlvbigpe2pRdWVyeSh0aGlzKS5maW5kKCcuaGVhZGVyLXdyYXAnKS5jc3MoJ3RyYW5zZm9ybScsJ3RyYW5zbGF0ZVkoMHB4KScpO30pO2pRdWVyeShcIi5zZXJ2aWNlLWdyaWQtd3JhcHBlclwiKS5lYWNoKGZ1bmN0aW9uKCl7dmFyIGhvdmVyQ29udGVudD1qUXVlcnkodGhpcykuZmluZCgnLmhvdmVyLWNvbnRlbnQnKTt2YXIgaG92ZXJNb3ZlWT1wYXJzZUludChob3ZlckNvbnRlbnQuaGVpZ2h0KCkrMTApO2pRdWVyeSh0aGlzKS5hdHRyKCdkYXRhLWhvdmVyWScsaG92ZXJNb3ZlWSk7fSk7alF1ZXJ5KHdpbmRvdykucmVzaXplKGZ1bmN0aW9uKCl7alF1ZXJ5KFwiLnNlcnZpY2UtZ3JpZC13cmFwcGVyXCIpLmVhY2goZnVuY3Rpb24oKXt2YXIgaG92ZXJDb250ZW50PWpRdWVyeSh0aGlzKS5maW5kKCcuaG92ZXItY29udGVudCcpO3ZhciBob3Zlck1vdmVZPXBhcnNlSW50KGhvdmVyQ29udGVudC5oZWlnaHQoKSsxMCk7alF1ZXJ5KHRoaXMpLmF0dHIoJ2RhdGEtaG92ZXJZJyxob3Zlck1vdmVZKTt9KTt9KTt9KTtlbGVtZW50b3JGcm9udGVuZC5ob29rcy5hZGRBY3Rpb24oJ2Zyb250ZW5kL2VsZW1lbnRfcmVhZHkvZ3JhbmRyZXN0YXVyYW50LXByaWNpbmctdGFibGUuZGVmYXVsdCcsZnVuY3Rpb24oJHNjb3BlKXt2YXIgZWxlbXM9QXJyYXkucHJvdG90eXBlLnNsaWNlLmNhbGwoZG9jdW1lbnQucXVlcnlTZWxlY3RvckFsbCgnLmpzLXN3aXRjaCcpKTtlbGVtcy5mb3JFYWNoKGZ1bmN0aW9uKGVsZW0pe3ZhciBzd2l0Y2hlcnk9bmV3IFN3aXRjaGVyeShlbGVtLHtkaXNhYmxlZDp0cnVlLGRpc2FibGVkT3BhY2l0eToxLGNvbG9yOmVsZW0uZ2V0QXR0cmlidXRlKCdkYXRhLXN3aXRjaC1iZycpLHNlY29uZGFyeUNvbG9yOmVsZW0uZ2V0QXR0cmlidXRlKCdkYXRhLXN3aXRjaC1iZycpLGphY2tDb2xvcjplbGVtLmdldEF0dHJpYnV0ZSgnZGF0YS1zd2l0Y2gtYnV0dG9uJyksamFja1NlY29uZGFyeUNvbG9yOmVsZW0uZ2V0QXR0cmlidXRlKCdkYXRhLXN3aXRjaC1idXR0b24nKX0pO30pO3ZhciBjaGFuZ2VDaGVja2JveD1kb2N1bWVudC5xdWVyeVNlbGVjdG9yKCcuanMtc3dpdGNoJyk7aWYoISFjaGFuZ2VDaGVja2JveCl7Y2hhbmdlQ2hlY2tib3gub25jaGFuZ2U9ZnVuY3Rpb24oKXtqUXVlcnkoXCIucHJpY2luZy1wbGFuLXByaWNlXCIpLmVhY2goZnVuY3Rpb24oKXtpZihjaGFuZ2VDaGVja2JveC5jaGVja2VkKVxue3ZhciBwcmljaW5nUGxhbj1qUXVlcnkodGhpcykuYXR0cignZGF0YS1wcmljZS15ZWFyJyk7alF1ZXJ5KHRoaXMpLmh0bWwocHJpY2luZ1BsYW4pO2pRdWVyeSgnLnByaWNpbmctcGxhbi11bml0LW1vbnRoJykuYWRkQ2xhc3MoJ2hpZGUnKTtqUXVlcnkoJy5wcmljaW5nLXBsYW4tdW5pdC15ZWFyJykucmVtb3ZlQ2xhc3MoJ2hpZGUnKTt9XG5lbHNlXG57dmFyIHByaWNpbmdQbGFuPWpRdWVyeSh0aGlzKS5hdHRyKCdkYXRhLXByaWNlLW1vbnRoJyk7alF1ZXJ5KHRoaXMpLmh0bWwocHJpY2luZ1BsYW4pO2pRdWVyeSgnLnByaWNpbmctcGxhbi11bml0LXllYXInKS5hZGRDbGFzcygnaGlkZScpO2pRdWVyeSgnLnByaWNpbmctcGxhbi11bml0LW1vbnRoJykucmVtb3ZlQ2xhc3MoJ2hpZGUnKTt9fSk7fTt9fSk7ZWxlbWVudG9yRnJvbnRlbmQuaG9va3MuYWRkQWN0aW9uKCdmcm9udGVuZC9lbGVtZW50X3JlYWR5L2dyYW5kcmVzdGF1cmFudC1mb29kLW1lbnUuZGVmYXVsdCcsZnVuY3Rpb24oJHNjb3BlKXtqUXVlcnkoJy5mb29kLXRvb2x0aXAnKS50b29sdGlwc3Rlcih7cG9zaXRpb246J3JpZ2h0JyxtdWx0aXBsZTp0cnVlLGNvbnRlbnRDbG9uaW5nOnRydWUsdGhlbWU6J3Rvb2x0aXBzdGVyLXNoYWRvdycsbWluV2lkdGg6MzAwLG1heFdpZHRoOjMwMCxkZWxheTo1MCxpbnRlcmFjdGl2ZTp0cnVlLH0pO30pO2VsZW1lbnRvckZyb250ZW5kLmhvb2tzLmFkZEFjdGlvbignZnJvbnRlbmQvZWxlbWVudF9yZWFkeS9ncmFuZHJlc3RhdXJhbnQtc2VhcmNoLWZvcm0uZGVmYXVsdCcsZnVuY3Rpb24oJHNjb3BlKXtqUXVlcnkoXCIudGctc2VhcmNoLWZvcm0uYXV0b2NvbXBsZXRlLWZvcm1cIikuZWFjaChmdW5jdGlvbigpe3ZhciBmb3JtSW5wdXQ9alF1ZXJ5KHRoaXMpLmZpbmQoJ2lucHV0W25hbWU9XCJzXCJdJyk7dmFyIHJlc3VsdERpdj1qUXVlcnkodGhpcykuYXR0cignZGF0YS1yZXN1bHQnKTtmb3JtSW5wdXQub24oJ2lucHV0JyxmdW5jdGlvbigpe2lmKGpRdWVyeSh0aGlzKS52YWwoKS5sZW5ndGg+MSlcbntqUXVlcnkuYWpheCh7dXJsOnRnQWpheC5hamF4dXJsLHR5cGU6J1BPU1QnLGRhdGE6J2FjdGlvbj1ncmFuZHJlc3RhdXJhbnRfYWpheF9zZWFyY2hfcmVzdWx0JicralF1ZXJ5KHRoaXMpLnNlcmlhbGl6ZSgpLHN1Y2Nlc3M6ZnVuY3Rpb24ocmVzdWx0cyl7alF1ZXJ5KFwiI1wiK3Jlc3VsdERpdikuaHRtbChyZXN1bHRzKTtpZihyZXN1bHRzIT0nJylcbntqUXVlcnkoXCIjXCIrcmVzdWx0RGl2KS5hZGRDbGFzcygndmlzaWJsZScpO2pRdWVyeShcIiNcIityZXN1bHREaXYpLnNob3coKTtqUXVlcnkoXCIjXCIrcmVzdWx0RGl2KS5jc3MoJ3otaW5kZXgnLDk5KTtqUXVlcnkoXCIjXCIrcmVzdWx0RGl2K1wiIHVsIGxpIGFcIikubW91c2Vkb3duKGZ1bmN0aW9uKCl7alF1ZXJ5KFwiI1wiK3Jlc3VsdERpdikuYXR0cignZGF0YS1tb3VzZWRvd24nLHRydWUpO30pO31cbmVsc2VcbntqUXVlcnkoXCIjXCIrcmVzdWx0RGl2KS5oaWRlKCk7fX19KTt9fSk7Zm9ybUlucHV0LmJpbmQoJ2ZvY3VzJyxmdW5jdGlvbigpe2pRdWVyeShcIiNcIityZXN1bHREaXYpLmFkZENsYXNzKCd2aXNpYmxlJyk7fSk7Zm9ybUlucHV0LmJpbmQoJ2JsdXInLGZ1bmN0aW9uKCl7aWYoalF1ZXJ5KFwiI1wiK3Jlc3VsdERpdikuYXR0cignZGF0YS1tb3VzZWRvd24nKSE9J3RydWUnKVxue2pRdWVyeShcIiNcIityZXN1bHREaXYpLnJlbW92ZUNsYXNzKCd2aXNpYmxlJyk7fX0pO30pO30pO2VsZW1lbnRvckZyb250ZW5kLmhvb2tzLmFkZEFjdGlvbignZnJvbnRlbmQvZWxlbWVudF9yZWFkeS9ncmFuZHJlc3RhdXJhbnQtY2xhc3NpYy1mb29kLW1lbnUtc2xpZGVyLmRlZmF1bHQnLGZ1bmN0aW9uKCRzY29wZSl7aWYoZWxlbWVudG9yRnJvbnRlbmQuaXNFZGl0TW9kZSgpKXtqUXVlcnkoJy5zbGlkZXJfd3JhcHBlcicpLmZsZXhzbGlkZXIoe2FuaW1hdGlvbjpcImZhZGVcIixhbmltYXRpb25Mb29wOnRydWUsaXRlbU1hcmdpbjowLG1pbkl0ZW1zOjEsbWF4SXRlbXM6MSxzbGlkZXNob3c6MCxjb250cm9sTmF2OmZhbHNlLHNtb290aEhlaWdodDpmYWxzZSxzbGlkZXNob3dTcGVlZDo1MDAwLG1vdmU6MX0pO2pRdWVyeSgnLnNsaWRlcl93cmFwcGVyLnBvcnRmb2xpbyAuc2xpZGVzIGxpJykuZWFjaChmdW5jdGlvbigpe3ZhciBoZWlnaHQ9alF1ZXJ5KHRoaXMpLmhlaWdodCgpO3ZhciBpbWFnZUhlaWdodD1qUXVlcnkodGhpcykuZmluZCgnaW1nJykuaGVpZ2h0KCk7dmFyIG9mZnNldD0oaGVpZ2h0LWltYWdlSGVpZ2h0KS8yO2pRdWVyeSh0aGlzKS5maW5kKCdpbWcnKS5jc3MoJ21hcmdpbi10b3AnLG9mZnNldCsncHgnKTt9KTt9fSk7ZWxlbWVudG9yRnJvbnRlbmQuaG9va3MuYWRkQWN0aW9uKCdmcm9udGVuZC9lbGVtZW50X3JlYWR5L2dyYW5kcmVzdGF1cmFudC1hbmltYXRlZC10ZXh0LmRlZmF1bHQnLGZ1bmN0aW9uKCRzY29wZSl7alF1ZXJ5KFwiLnRoZW1lZ29vZHMtYW5pbWF0ZWQtdGV4dFwiKS5lYWNoKGZ1bmN0aW9uKCl7dmFyIHRleHRDb250ZW50PWpRdWVyeSh0aGlzKS5maXJzdCgpO3ZhciBkZWxpbWl0ZXJUeXBlT3JpPWpRdWVyeSh0aGlzKS5hdHRyKCdkYXRhLWRlbGltaXRlcicpO3ZhciBkZWxpbWl0ZXJUeXBlPWpRdWVyeSh0aGlzKS5hdHRyKCdkYXRhLWRlbGltaXRlcicpO3ZhciB0cmFuc2l0aW9uU3BlZWQ9cGFyc2VJbnQoalF1ZXJ5KHRoaXMpLmF0dHIoJ2RhdGEtdHJhbnNpdGlvbicpKTt2YXIgdHJhbnNpdGlvbkRlbGF5PXBhcnNlSW50KGpRdWVyeSh0aGlzKS5hdHRyKCdkYXRhLXRyYW5zaXRpb24tZGVsYXknKSk7dmFyIHRyYW5zaXRpb25EdXJhdGlvbj1wYXJzZUludChqUXVlcnkodGhpcykuYXR0cignZGF0YS10cmFuc2l0aW9uLWR1cmF0aW9uJykpO2lmKGRlbGltaXRlclR5cGU9PSdzZW50ZW5jZScpe2RlbGltaXRlclR5cGU9J3dvcmQnfVxudmFyIGFuaW1hdGVkVGV4dD10ZXh0Q29udGVudC5ibGFzdCh7ZGVsaW1pdGVyOmRlbGltaXRlclR5cGUsYXJpYTpmYWxzZSx9KTtpZihqUXVlcnkodGhpcykuaGFzQ2xhc3MoJ292ZXJmbG93LWhpZGRlbicpKXthbmltYXRlZFRleHQuZWFjaChmdW5jdGlvbihpKXt2YXIgdGV4dEVhY2hTcGFuPWpRdWVyeSh0aGlzKTt2YXIgaW5pdGlhbFRleHQ9dGV4dEVhY2hTcGFuLnRleHQoKTt9KTt9XG5pZih0ZXh0Q29udGVudC5pc0luVmlld3BvcnQoKSl7YW5pbWF0ZWRUZXh0LmVhY2goZnVuY3Rpb24oaSl7dmFyIGRlbGF5U3BlZWQ9cGFyc2VJbnQodHJhbnNpdGlvbkRlbGF5KyhpKnRyYW5zaXRpb25TcGVlZCkpO2lmKGRlbGltaXRlclR5cGVPcmk9PSdzZW50ZW5jZScpe2RlbGF5U3BlZWQ9cGFyc2VJbnQodHJhbnNpdGlvbkRlbGF5K3RyYW5zaXRpb25TcGVlZCk7fVxualF1ZXJ5KHRoaXMpLnF1ZXVlKGZ1bmN0aW9uKG5leHQpe2pRdWVyeSh0aGlzKS5jc3Moeyd0cmFuc2l0aW9uLWRlbGF5JzpkZWxheVNwZWVkKydtcycsJ3RyYW5zaXRpb24tZHVyYXRpb24nOnRyYW5zaXRpb25EdXJhdGlvbisnbXMnLCd0cmFuc2Zvcm0nOid0cmFuc2xhdGVYKDBweCkgdHJhbnNsYXRlWSgwcHgpIHRyYW5zbGF0ZVooMHB4KScsJ29wYWNpdHknOjF9KTt9KX0pO31cbmpRdWVyeSh3aW5kb3cpLm9uKCdyZXNpemUgc2Nyb2xsJyxmdW5jdGlvbigpe2lmKHRleHRDb250ZW50LmlzSW5WaWV3cG9ydCgpKXthbmltYXRlZFRleHQuZWFjaChmdW5jdGlvbihpKXt2YXIgZGVsYXlTcGVlZD1wYXJzZUludCh0cmFuc2l0aW9uRGVsYXkrKGkqdHJhbnNpdGlvblNwZWVkKSk7aWYoZGVsaW1pdGVyVHlwZU9yaT09J3NlbnRlbmNlJyl7ZGVsYXlTcGVlZD1wYXJzZUludCh0cmFuc2l0aW9uRGVsYXkrdHJhbnNpdGlvblNwZWVkKTt9XG5qUXVlcnkodGhpcykucXVldWUoZnVuY3Rpb24obmV4dCl7alF1ZXJ5KHRoaXMpLmNzcyh7J3RyYW5zaXRpb24tZGVsYXknOmRlbGF5U3BlZWQrJ21zJywndHJhbnNpdGlvbi1kdXJhdGlvbic6dHJhbnNpdGlvbkR1cmF0aW9uKydtcycsJ3RyYW5zZm9ybSc6J3RyYW5zbGF0ZVgoMHB4KSB0cmFuc2xhdGVZKDBweCkgdHJhbnNsYXRlWigwcHgpJywnb3BhY2l0eSc6MX0pO30pfSk7fX0pO30pO30pO2VsZW1lbnRvckZyb250ZW5kLmhvb2tzLmFkZEFjdGlvbignZnJvbnRlbmQvZWxlbWVudF9yZWFkeS9ncmFuZHJlc3RhdXJhbnQtYW5pbWF0ZWQtaGVhZGxpbmUuZGVmYXVsdCcsZnVuY3Rpb24oJHNjb3BlKXtqUXVlcnkoXCIudGhlbWVnb29kcy1hbmltYXRlZC1oZWFkbGluZVwiKS5lYWNoKGZ1bmN0aW9uKCl7dmFyIGFuaW1hdGlvblR5cGU9alF1ZXJ5KHRoaXMpLmF0dHIoJ2RhdGEtYW5pbWF0aW9uJyk7alF1ZXJ5KHRoaXMpLmFuaW1hdGVkSGVhZGxpbmUoe2FuaW1hdGlvblR5cGU6YW5pbWF0aW9uVHlwZX0pO30pO30pO2VsZW1lbnRvckZyb250ZW5kLmhvb2tzLmFkZEFjdGlvbignZnJvbnRlbmQvZWxlbWVudF9yZWFkeS9ncmFuZHJlc3RhdXJhbnQtdGVzdGltb25pYWwtY2Fyb3VzZWwuZGVmYXVsdCcsZnVuY3Rpb24oJHNjb3BlKXtqUXVlcnkoXCIudGVzdGltb25pYWxzLWNhcm91c2VsLXdyYXBwZXIgLm93bC1jYXJvdXNlbFwiKS5lYWNoKGZ1bmN0aW9uKCl7dmFyIGF1dG9QbGF5PWpRdWVyeSh0aGlzKS5hdHRyKCdkYXRhLWF1dG9wbGF5Jyk7aWYodHlwZW9mIGF1dG9QbGF5PT1cInVuZGVmaW5lZFwiKXthdXRvUGxheT1mYWxzZTt9XG5pZihhdXRvUGxheT09MSlcbnthdXRvUGxheT10cnVlO31cbmVsc2VcbnthdXRvUGxheT1mYWxzZTt9XG52YXIgdGltZXI9alF1ZXJ5KHRoaXMpLmF0dHIoJ2RhdGEtdGltZXInKTtpZih0eXBlb2YgdGltZXI9PVwidW5kZWZpbmVkXCIpe3RpbWVyPTgwMDA7fVxudmFyIHBhZ2luYXRpb249alF1ZXJ5KHRoaXMpLmF0dHIoJ2RhdGEtcGFnaW5hdGlvbicpO2lmKHR5cGVvZiBwYWdpbmF0aW9uPT1cInVuZGVmaW5lZFwiKXtwYWdpbmF0aW9uPXRydWU7fVxuaWYocGFnaW5hdGlvbj09MSlcbntwYWdpbmF0aW9uPXRydWU7fVxuZWxzZVxue3BhZ2luYXRpb249ZmFsc2U7fVxualF1ZXJ5KHRoaXMpLm93bENhcm91c2VsKHtsb29wOnRydWUsY2VudGVyOnRydWUsaXRlbXM6MyxtYXJnaW46MCxhdXRvcGxheTphdXRvUGxheSxkb3RzOnBhZ2luYXRpb24sYXV0b3BsYXlUaW1lb3V0OnRpbWVyLHNtYXJ0U3BlZWQ6NDUwLHJlc3BvbnNpdmU6ezA6e2l0ZW1zOjF9LDc2ODp7aXRlbXM6Mn0sMTE3MDp7aXRlbXM6M319fSk7fSk7fSk7fSk7fSkoalF1ZXJ5KTsiXSwic291cmNlUm9vdCI6IiJ9