<footer class="commision-padding">
    <div class="footer-bg"><img src="img/fire5.png" alt="fire"></div>
    <div class="commision">
        <div class="container">
            <div class="center">
                <div class="title center">
                    <h2>Làm cách nào để<br>
                        nhận một tác phẩm</h2>
                </div>
            </div>
            <div class="comision-wrapper">
                <div class="row">
                    <div class="col-12 col-lg-7 full">
                        <div class="contact-wrapper">
                            <p>Nếu bạn quan tâm nghệ thuật xăm hình, hãy vui lòng <a href=""><strong>liên hệ với Studio Minh tatto</strong></a> và chúng tôi sẽ sắp xếp một buổi hẹn nhiều trải nghiệm thú vị và thân mật.</p>
                            <p>Đây cũng là một cơ hội tốt để chúng ta cùng nhau trao đổi về các ý tưởng cũng như cảm nhận luồng năng lượng giữa khách hàng và artist.</p>
                        </div>
                    </div>
                    <div class="col-12 col-lg-5">
                        <div class="info-wrapper">
                            <p>Chúng tôi sử dụng những dụng cụ tốt nhất và có những nghệ sĩ xăm tài năng nhất? Lời quảng cáo chung chung mà tất cả tiệm xăm sử dụng có lẽ sẽ không mang lại nhiều ý nghĩa với bạn. </br> Nghệ thuật xấu hay đẹp sẽ khác nhau trong mắt của mỗi người và chính bạn là người đánh giá điều đó khi xem những tác phẩm được thực hiện tại tiệm xăm của chúng tôi. </br>Điều khác biệt mà chúng tôi tự hào là <strong>trải nghiệm của bạn mỗi khi đến với chúng tôi</strong> và cái đó thì sẽ khó để diễn tả bằng lời. Có một điều chúng tôi có thể đảm bảo khi bạn đến với Studio Minh Tatto là bạn sẽ <strong>không có rủi ro về tài chính</strong>. Hãy đến để thấy sự khác biệt và <strong>chỉ trả tiền khi bạn hài lòng</strong> với kết quả bạn nhận được.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
