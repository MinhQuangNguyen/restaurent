<section class="intro">
    <div class="smoke reveal fade-anim-default" data-delay="100"><img src="img/white-smoke.png" alt="fire"></div>
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="row">
                    <div class="intro-image reveal fade-anim-zoom" data-delay="700">
                        <img width="1047" height="698" src="/img/wallpaperflare.com_wallpaper.jpg" class="obj-fit-cover wp-post-image"
                            alt="Studio Minh Tatto at May Fine Art Studio Vienna" loading="lazy"
                            srcset="img/wallpaperflare.com_wallpaper.jpg"
                            sizes="(max-width: 1047px) 100vw, 1047px"> </div>
                    <div class="col-12 col-lg-7 intro-order no-padding">
                        <div class="intro-wrapper">
                            <div class="title">
                                <h1 class="reveal text-anim-bottom" data-delay="300">
                                    <span>Studio Minh Tatto</span></h1>
                            </div>
                            <div class="intro-text">
                                <div class="reveal text-anim-top" data-delay="500">
                                    <p>Hướng đến việc cung cấp một trải nghiệm đáng nhớ. Studio Minh Tatto là một phòng xăm và xỏ khuyên,được đánh giá là một studio tatto nổi bật trong nội thành Hà Nội. Studio Minh Tatto là cái nôi và nguồn cảm hứng để nuôi dưỡng nghệ thuật. Nếu bạn là người yêu nghệ thuật, hãy <a href=""><strong>liên hệ với tôi</strong></a>, để tôi có thể tạo lên một tác phẩm nghệ thuật vào trong con người của bạn.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
