<div class="art-stories">
    <div class="container">
        <div class="center">
            <div class="title center">
                <h2 class="reveal text-anim-bottom"><span>Câu chuyện về tác phẩm</span></h2>
            </div>
        </div>
        <div class="stories">
            <div class="fire1">
                <div class="reveal fade-anim-zoom"><img src="/img/fire1-1.png" alt="fire"></div>
            </div>
            <div class="row">
                <div class="col-12">
                    <div class="row justify-content-end">
                        <div class="about-story left-story">
                            <div class="story-title in">
                                <h3 class="reveal text-anim-bottom" data-delay="500">
                                    <span>Animals</span></h3>
                            </div>
                            <div class="story-text">
                                <div class="reveal text-anim-top" data-delay="700">
                                    <p>Biểu tượng Hổ gợi lên những ý tưởng về Sức mạnh và sự dũng mãnh. Trong đạo phật, sức mạnh của Hổ còn tượng trưng cho sức mạnh của đức tin, của nỗ lực tinh thần, vượt qua  tội lỗi.</p>
                                </div>
                            </div>
                            <div class="reveal fade-anim-default" data-delay="900">
                                <a href="/animals" class="more-info">Đọc câu chuyện</a>
                            </div>
                        </div>
                        <div class="col-12 col-lg-8">
                            <div class="img-wrapper">
                                <div class="image-title reveal letter-animation image1">
                                    <ul class="layer-items1" data-relative-input="true" data-method="itemParallax1"
                                        style="transform: translate3d(0px, 0px, 0px) rotate(0.0001deg); transform-style: preserve-3d; backface-visibility: hidden; position: relative; pointer-events: none;">
                                        <li class="layer-3 layer" data-depth="0.15"
                                            style="transform: translate3d(6.8px, 40.2px, 0px); transform-style: preserve-3d; backface-visibility: hidden; position: relative; display: block; left: 0px; top: 0px;">
                                            <span style="color:#981c20">Animals</span></li>
                                        <li class="layer" data-depth="0.30"
                                            style="transform: translate3d(13.5px, 80.4px, 0px); transform-style: preserve-3d; backface-visibility: hidden; position: absolute; display: block; left: 0px; top: 0px;">
                                            <div class="circle1 circle reveal fade-anim-zoom" data-delay="100">
                                                <img src="/img/trace1-1.png" alt="brush">
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                                <div class="img-content">
                                    <div class="reveal fade-anim-zoom" data-delay="900">
                                        <img width="810" height="810" src="/img/tatto5.jpg"
                                            class="story-image obj-fit-cover wp-post-image"
                                            alt="Tatto Animals" loading="lazy"
                                            srcset="/img/tatto5.jpg"
                                            sizes="(max-width: 810px) 100vw, 810px">
                                        <div class="dimension left-dimension">Kích thước<br>
                                            99x99cm</div>
                                    </div>
                                    <div class="part-image">
                                        <a href="/img/tatto5.jpg"
                                            data-fancybox="gallery" class="no-barba reveal fade-anim-left"
                                            data-delay="300"><img src="/img/tatto5.jpg" alt="Tatto Animals"></a>
                                        <a href="/img/tatto6.jpg"
                                            data-fancybox="gallery" class="no-barba reveal fade-anim-left"
                                            data-delay="500"><img src="/img/tatto6.jpg" alt="Tatto Animals"></a>
                                        <a href="/img/tatto7.jpg"
                                            data-fancybox="gallery" class="no-barba reveal fade-anim-left"
                                            data-delay="700"><img src="/img/tatto7.jpg" alt="Tatto Animals"></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="stories">
            <div class="fire2">
                <div class="reveal fade-anim-zoom"><img src="/img/fire-helena.png" alt=""></div>
            </div>
            <div class="row">
                <div class="col-12">
                    <div class="row ">
                        <div class="about-story right-story">
                            <div class="story-title in">
                                <h3 class="reveal text-anim-bottom" data-delay="500">
                                    <span>Dark Angel</span></h3>
                            </div>
                            <div class="story-text">
                                <div class="reveal text-anim-top" data-delay="700">
                                    <p>
                                        Thần chết là biểu tượng cho sự chết chóc, mát mát. Có người lại xem nó như một hình ảnh buồn trong cuộc sống. Nhưng cũng có nhiều người cho rằng thần chết là biểu tượng của sức mạnh. Không phải ai cũng có thể lựa chọn cho mình hình xăm này.
                                    </p>
                                </div>
                            </div>
                            <div class="reveal fade-anim-default" data-delay="900">
                                <a href="/dark-angel" class="more-info">Đọc câu chuyện</a>
                            </div>
                        </div>
                        <div class="col-12 col-lg-8">
                            <div class="img-wrapper">
                                <div class="image-title reveal letter-animation image2">
                                    <ul class="layer-items2" data-relative-input="true" data-method="itemParallax2"
                                        style="transform: translate3d(0px, 0px, 0px) rotate(0.0001deg); transform-style: preserve-3d; backface-visibility: hidden; position: relative; pointer-events: none;">
                                        <li class="layer-3 layer" data-depth="0.15"
                                            style="transform: translate3d(10.6px, 79.3px, 0px); transform-style: preserve-3d; backface-visibility: hidden; position: relative; display: block; left: 0px; top: 0px;">
                                            <span style="color:#e59d82">Dark Angel</span>
                                        </li>
                                        <li class="layer" data-depth="0.30"
                                            style="transform: translate3d(21.2px, 158.7px, 0px); transform-style: preserve-3d; backface-visibility: hidden; position: absolute; display: block; left: 0px; top: 0px;">
                                            <div class="circle2 circle reveal fade-anim-zoom" data-delay="100">
                                                <img src="/img/tockice1.png" alt="">
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                                <div class="img-content">
                                    <div class="reveal fade-anim-zoom" data-delay="900">
                                        <img width="810" height="810" src="/img/tatto1.jpg"
                                        class="story-image obj-fit-cover wp-post-image"
                                        alt="Tatto Seven deadly sins " loading="lazy"
                                        srcset="/img/tatto1.jpg"
                                        sizes="(max-width: 810px) 100vw, 810px">
                                        <div class="dimension right-dimension">Kích thước<br>
                                            60x40 cm</div>
                                    </div>
                                    <div class="part-image">
                                        <a href="/img/tatto2.jpg"
                                            data-fancybox="gallery" class="no-barba reveal fade-anim-left"
                                            data-delay="300"><img src="/img/tatto2.jpg" alt="Tatto Seven deadly sins "></a>
                                        <a href="/img/tatto3.jpg"
                                            data-fancybox="gallery" class="no-barba reveal fade-anim-left"
                                            data-delay="500"><img src="/img/tatto3.jpg" alt="Tatto Seven deadly sins "></a>
                                        <a href="/img/tatto4.jpg"
                                            data-fancybox="gallery" class="no-barba reveal fade-anim-left"
                                            data-delay="700"><img src="/img/tatto4.jpg" alt="Tatto Seven deadly sins "></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="stories">
            <div class="fire3">
                <div class="reveal fade-anim-zoom"><img src="/img/fire-first-day.png" alt=""></div>
            </div>
            <div class="row">
                <div class="col-12">
                    <div class="row justify-content-end">
                        <div class="about-story left-story">
                            <div class="story-title in">
                                <h3 class="reveal text-anim-bottom" data-delay="500">
                                    <span>Ultimate Warrior</span></h3>
                            </div>
                            <div class="story-text">
                                <div class="reveal text-anim-top" data-delay="700">
                                    <p>Ý nghĩa hình xăm Samurai là đại diện cho sự công bằng, lòng can đảm, sự hướng thiện, sự tôn trọng, danh dự và lòng trung thành của những bậc chính nhân quân tử. Vì Samurai là những người bảo vệ thời đại Edo xưa. Rất được nể trọng</p>
                                </div>
                            </div>
                            <div class="reveal fade-anim-default" data-delay="900">
                                <a href="/ultimate-warrior" class="more-info">Đọc câu chuyện</a>
                            </div>
                        </div>
                        <div class="col-12 col-lg-8">
                            <div class="img-wrapper">
                                <div class="image-title reveal letter-animation image3">
                                    <ul class="layer-items3" data-relative-input="true" data-method="itemParallax3"
                                        style="transform: translate3d(0px, 0px, 0px) rotate(0.0001deg); transform-style: preserve-3d; backface-visibility: hidden; position: relative; pointer-events: none;">
                                        <li class="layer-3 layer" data-depth="0.15"
                                            style="transform: translate3d(10.7px, 114.5px, 0px); transform-style: preserve-3d; backface-visibility: hidden; position: relative; display: block; left: 0px; top: 0px;">
                                            <span style="color:#36d080">Ultimate Warrior</span>
                                        <li class="layer" data-depth="0.30"
                                            style="transform: translate3d(21.5px, 229px, 0px); transform-style: preserve-3d; backface-visibility: hidden; position: absolute; display: block; left: 0px; top: 0px;">
                                            <div class="circle3 circle reveal fade-anim-zoom" data-delay="100">
                                                <img src="/img/tockice2.png" alt="Ultimate Warrior">
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                                <div class="img-content">
                                    <div class="reveal fade-anim-zoom" data-delay="900">
                                        <img width="1200" height="1200" src="/img/tatto8.jpg"
                                            class="story-image obj-fit-cover wp-post-image"
                                            alt="Oil painting First Day by Damir May" loading="lazy"
                                            srcset="/img/tatto8.jpg"
                                            sizes="(max-width: 1200px) 100vw, 1200px">
                                        <div class="dimension left-dimension">Kích thước<br>
                                            200 x 275 cm</div>
                                    </div>
                                    <div class="part-image">
                                        <a href="/img/tatto9.jpg"
                                            data-fancybox="gallery" class="no-barba reveal fade-anim-left"
                                            data-delay="300"><img src="/img/tatto9.jpg"
                                                alt="Ultimate Warrior"></a>
                                        <a href="/img/tatto10.jpg"
                                            data-fancybox="gallery" class="no-barba reveal fade-anim-left"
                                            data-delay="500"><img src="/img/tatto10.jpg"
                                                alt="Ultimate Warrior"></a>
                                        <a href="/img/tatto11.jpg"
                                            data-fancybox="gallery" class="no-barba reveal fade-anim-left"
                                            data-delay="700"><img src="/img/tatto11.jpg"
                                                alt="Ultimate Warrior"></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="stories">
            <div class="fire4">
                <div class="reveal fade-anim-zoom"><img src="/img/fire2-1.png" alt="fire"></div>
            </div>
            <div class="row">
                <div class="col-12">
                    <div class="row ">
                        <div class="about-story right-story">
                            <div class="story-title in">
                                <h3 class="reveal text-anim-bottom" data-delay="500">
                                    <span>Devil's Fairy</span></h3>
                            </div>
                            <div class="story-text">
                                <div class="reveal text-anim-top" data-delay="700">
                                    <p>Quỷ – biểu tượng của một thế lực đen tối mang sức mạnh tàn bạo, hủy diệt tiềm ẩn trong cuộc sống. Nhắc đến mặt quỷ, chúng ta sẽ nghĩ ngay đến sự dữ dằn, hung tợn của nó như thế nào.</p>
                                </div>
                            </div>
                            <div class="reveal fade-anim-default" data-delay="900">
                                <a href="/devil-fairy" class="more-info">Đọc câu chuyện</a>
                            </div>
                        </div>
                        <div class="col-12 col-lg-8">
                            <div class="img-wrapper">
                                <div class="image-title reveal letter-animation image4">
                                    <ul class="layer-items4" data-relative-input="true" data-method="itemParallax4"
                                        style="transform: translate3d(0px, 0px, 0px) rotate(0.0001deg); transform-style: preserve-3d; backface-visibility: hidden; position: relative; pointer-events: none;">
                                        <li class="layer-3 layer" data-depth="0.15" style="transform: translate3d(13.1px, 153.5px, 0px); transform-style: preserve-3d; backface-visibility: hidden; position: relative; display: block; left: 0px; top: 0px;">
                                            <span style="color:#8c5620">Devil's Fairy</span>
                                        </li>
                                        <li class="layer" data-depth="0.30"
                                            style="transform: translate3d(26.1px, 307px, 0px); transform-style: preserve-3d; backface-visibility: hidden; position: absolute; display: block; left: 0px; top: 0px;">
                                            <div class="circle4 circle reveal fade-anim-zoom" data-delay="100">
                                                <img src="/img/trace2-1.png" alt="brush">
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                                <div class="img-content">
                                    <div class="reveal fade-anim-zoom" data-delay="900">
                                        <img width="777" height="957" src="/img/tatto14.jpg"
                                            class="story-image obj-fit-cover wp-post-image" alt="Devil's fairy"
                                            loading="lazy"
                                            srcset="/img/tatto14.jpg"
                                            sizes="(max-width: 777px) 100vw, 777px">
                                        <div class="dimension right-dimension">Kích thước<br>
                                            40x30cm<br>
                                        </div>
                                    </div>
                                    <div class="part-image">
                                        <a href="/img/tatto14.jpg"
                                            data-fancybox="gallery" class="no-barba reveal fade-anim-left"
                                            data-delay="300"><img src="/img/tatto14.jpg"
                                                alt="Devil's fairy"></a>
                                        <a href="/img/tatto12.jpg"
                                            data-fancybox="gallery" class="no-barba reveal fade-anim-left"
                                            data-delay="500"><img src="/img/tatto12.jpg"
                                                alt="Devil's fairy"></a>
                                        <a href="/img/tatto13.jpg"
                                            data-fancybox="gallery" class="no-barba reveal fade-anim-left"
                                            data-delay="700"><img src="/img/tatto13.jpg"
                                                alt="Devil's fairy"></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="stories">
            <div class="fire5">
                <div class="reveal fade-anim-zoom"><img src="/img/fire3-1.png" alt="fire"></div>
            </div>
            <div class="row">
                <div class="col-12">
                    <div class="row justify-content-end">
                        <div class="about-story left-story">
                            <div class="story-title in">
                                <h3 class="reveal text-anim-bottom" data-delay="500">
                                    <span>The Last Time</span></h3>
                            </div>
                            <div class="story-text">
                                <div class="reveal text-anim-top" data-delay="700">
                                    <p>
                                        Thời gian không là vô giá, bạn không thể mua nó và cũng không thể quay ngược thời gian. Bạn có thể thấy rõ thời gian trôi qua như thế nào nhưng bạn không bao giờ có thể ngăn chặn được nó. Thời gian được biểu thị thông qua một chiếc đồng hồ và khi đồng hồ in trên cơ thể bạn sẽ mang triết lý của thời gian vô tận.
                                    </p>
                                </div>
                            </div>
                            <div class="reveal fade-anim-default" data-delay="900">
                                <a href="/the-last-time" class="more-info">Đọc câu chuyện</a>
                            </div>
                        </div>
                        <div class="col-12 col-lg-8">
                            <div class="img-wrapper">
                                <div class="image-title reveal letter-animation image5">
                                    <ul class="layer-items5" data-relative-input="true" data-method="itemParallax5"
                                        style="transform: translate3d(0px, 0px, 0px) rotate(0.0001deg); transform-style: preserve-3d; backface-visibility: hidden; position: relative; pointer-events: none;">
                                        <li class="layer-3 layer" data-depth="0.15"
                                            style="transform: translate3d(14.6px, 192.7px, 0px); transform-style: preserve-3d; backface-visibility: hidden; position: relative; display: block; left: 0px; top: 0px;">
                                            <span style="color:#c89a4c">The Last Time</span>
                                        </li>
                                        <li class="layer" data-depth="0.30"
                                            style="transform: translate3d(29.3px, 385.5px, 0px); transform-style: preserve-3d; backface-visibility: hidden; position: absolute; display: block; left: 0px; top: 0px;">
                                            <div class="circle5 circle reveal fade-anim-zoom" data-delay="100">
                                                <img src="img/trace3-1.png" alt="brush">
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                                <div class="img-content">
                                    <div class="reveal fade-anim-zoom" data-delay="900">
                                        <img width="807" height="808" src="/img/tatto15.jpg"
                                            class="story-image obj-fit-cover wp-post-image"
                                            alt="The Last Time" loading="lazy"
                                            srcset="/img/tatto15.jpg"
                                            sizes="(max-width: 807px) 100vw, 807px">
                                        <div class="dimension left-dimension">Kích thước<br>
                                            45x90cm</div>
                                    </div>
                                    <div class="part-image">
                                        <a href="/img/tatto16.jpg"
                                            data-fancybox="gallery" class="no-barba reveal fade-anim-left"
                                            data-delay="300"><img src="/img/tatto16.jpg"
                                                alt="The Last Time"></a>
                                        <a href="/img/tatto17.jpg"
                                            data-fancybox="gallery" class="no-barba reveal fade-anim-left"
                                            data-delay="500"><img src="/img/tatto17.jpg" alt="The Last Time"></a>
                                        <a href="/img/tatto18.jpg"
                                            data-fancybox="gallery" class="no-barba reveal fade-anim-left"
                                            data-delay="700"><img src="/img/tatto18.jpg"
                                                alt="The Last Time"></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="stories">
            <div class="fire6">
                <div class="reveal fade-anim-zoom"><img src="/img/fire4-1.png" alt="fire"></div>
            </div>
            <div class="row">
                <div class="col-12">
                    <div class="row ">
                        <div class="about-story right-story">
                            <div class="story-title in">
                                <h3 class="reveal text-anim-bottom" data-delay="500">
                                    <span>Fallen Angel</span></h3>
                            </div>
                            <div class="story-text">
                                <div class="reveal text-anim-top" data-delay="700">
                                    <p>Hình xăm thiên thần sa ngã mang ý nghĩa tích cực là sự cố gắng và nỗ lực vực qua những khó khăn, vấp ngã của cuộc sống để thay đổi số phận, thay đổi bản thân, khắc phục lỗi lầm và hoàn thiện mình hơn</p>
                                </div>
                            </div>
                            <div class="reveal fade-anim-default" data-delay="900">
                                <a href="/fallen-angel" class="more-info">Đọc câu chuyện</a>
                            </div>
                        </div>
                        <div class="col-12 col-lg-8">
                            <div class="img-wrapper">
                                <div class="image-title reveal letter-animation image6">
                                    <ul class="layer-items6" data-relative-input="true" data-method="itemParallax6"
                                        style="transform: translate3d(0px, 0px, 0px) rotate(0.0001deg); transform-style: preserve-3d; backface-visibility: hidden; position: relative; pointer-events: none;">
                                        <li class="layer-3 layer" data-depth="0.15"
                                            style="transform: translate3d(5.1px, 230.1px, 0px); transform-style: preserve-3d; backface-visibility: hidden; position: relative; display: block; left: 0px; top: 0px;">
                                            <span style="color:#9b261c">Fallen Angel</span>
                                        </li>
                                        <li class="layer" data-depth="0.30"
                                            style="transform: translate3d(10.2px, 460.2px, 0px); transform-style: preserve-3d; backface-visibility: hidden; position: absolute; display: block; left: 0px; top: 0px;">
                                            <div class="circle6 circle reveal fade-anim-zoom" data-delay="100">
                                                <img src="/img/trace4-1.png" alt="Fallen angel">
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                                <div class="img-content">
                                    <div class="reveal fade-anim-zoom" data-delay="900">
                                        <img width="814" height="814" src="/img/tatto19.jpg"
                                            class="story-image obj-fit-cover wp-post-image"
                                            alt="Fallen angel" loading="lazy"
                                            srcset="/img/tatto19.jpg"
                                            sizes="(max-width: 814px) 100vw, 814px">
                                        <div class="dimension right-dimension">Kích thước<br>
                                            60x40cm</div>
                                    </div>
                                    <div class="part-image">
                                        <a href="/img/tatto19.jpg"
                                            data-fancybox="gallery" class="no-barba reveal fade-anim-left"
                                            data-delay="300"><img src="/img/tatto19.jpg"
                                                alt="Fallen angel"></a>
                                        <a href="/img/tatto20.jpg"
                                            data-fancybox="gallery" class="no-barba reveal fade-anim-left"
                                            data-delay="500"><img src="/img/tatto20.jpg"
                                                alt="Fallen angel"></a>
                                        <a href="/img/tatto21.jpg" data-fancybox="gallery"
                                            class="no-barba reveal fade-anim-left" data-delay="700"><img
                                                src="/img/tatto21.jpg" alt="Fallen angel"></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
