@extends('frontend.layouts.iconic')

@section('title', __('Home'))

@section('content')
@include('frontend.home.includes.intro')
@include('frontend.home.includes.stories')
@include('frontend.home.includes.commision')
@endsection
