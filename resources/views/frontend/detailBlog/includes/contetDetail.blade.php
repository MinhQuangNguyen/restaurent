<section class="showcase" data-scroll-section=""
    style="transform: matrix3d(1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); visibility: visible;">
    
    @include('frontend.home.includes.logo')
   
    <div class="container">
        <div class="row">
            <div class="col-md-7 image">
                <div class="featured-image ">
                    <div class="reveal fade-anim-bottom is-inview" data-delay="300" data-delay-mobile="300"
                        data-scroll="" data-scroll-call="reveal">
                        <img width="900" height="439" src="/img/zenn.jpg" alt="" sizes="(max-width: 900px) 100vw, 900px" class="lazyloaded" data-ll-status="loaded">
                    </div>
                    <div class="work-stickers">
                        <svg class="work-sticker">
                            <use xlink:href="#work-sticker"></use>
                        </svg>
                    </div>
                </div>
                <div class="showcase-info">
                    <div class="showcase-title reveal text-anim-bottom is-inview" data-scroll=""
                        data-delay="500" data-delay-mobile="600" data-scroll-call="reveal">
                        <h2>Giới thiệu</h2>
                    </div>
                    <div class="showcase-small-info reveal fade-anim-top is-inview" data-scroll=""
                        data-delay="800" data-delay-mobile="800" data-scroll-call="reveal">
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed convallis tristique ante, maximus tincidunt nibh tempus vel. Vivamus at turpis sit amet felis fringilla placerat. Etiam posuere et libero eu suscipit.</p>
                    </div>
                    <div class="stickers-mobile">
                        <div class="row">
                            <div class="col-3 sticker-padding">
                                <div class="sticker reveal fade-anim-left-btn" data-scroll="" data-delay-mobile="200"
                                    data-scroll-call="reveal">
                                    <img src="/img/ui.png"
                                    alt="awards" width="220" height="220">
                                </div>
                            </div>
                            <div class="col-3 sticker-padding">
                                <div class="sticker reveal fade-anim-left-btn" data-scroll="" data-delay-mobile="500"
                                    data-scroll-call="reveal">
                                    <img src="/img/ux.png"
                                    alt="awards" width="220" height="220"
                                    data-lazy-src="">
                                </div>
                            </div>
                            <div class="col-3 sticker-padding">
                                <div class="sticker reveal fade-anim-left-btn" data-scroll="" data-delay-mobile="800"
                                    data-scroll-call="reveal">
                                    <img src="/img/innovation.png"
                                    alt="awards" width="220" height="220"
                                    data-lazy-src="/img/innovation.png">
                                </div>
                            </div>
                            <div class="col-3 sticker-padding">
                                <div class="sticker reveal fade-anim-left-btn" data-scroll="" data-delay-mobile="1100"
                                    data-scroll-call="reveal">
                                    <img src="/img/special-desing.png"
                                    alt="awards" width="220" height="220"
                                    data-lazy-src="/img/special-desing.png">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="showcase-project">
                        <a href="" class="showcase-site-link" target="_blank"
                            rel="noreferrer"
                            onclick="gtag(&#39;event&#39;, &#39;click&#39;,  { event_category: &#39;outbound&#39;, event_action: &#39;click&#39;, event_label: &#39;Zenn’s Foto&#39;});">Xem chi tiết dự án</a>
                    </div>
                </div>
            </div>
            <div class="col-md-5 name">
                <div class="decoration">
                    <div class="decoration-text">
                        <div data-scroll="" data-scroll-speed="-3" data-scroll-delay="0.10" class="is-inview"
                            style="transform: matrix3d(1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, -99.75, 0, 1);">
                            <h1>Zenn’s Foto</h1>
                        </div>
                    </div>
                    <div class="stickers">
                        <div class="row">
                            <div class="col-md-3 sticker-padding">
                                <div class="sticker reveal fade-anim-left-btn is-inview" data-scroll=""
                                    data-delay="200" data-scroll-call="reveal">
                                    <img src="/img/ui.png" alt="awards" width="220"
                                    height="220" class="lazyloaded" data-ll-status="loaded">
                                </div>
                            </div>
                            <div class="col-md-3 sticker-padding">
                                <div class="sticker reveal fade-anim-left-btn is-inview" data-scroll=""
                                    data-delay="500" data-scroll-call="reveal">
                                    <img src="/img/ux.png" alt="awards" width="220"
                                            height="220" class="lazyloaded" data-ll-status="loaded">
                                </div>
                            </div>
                            <div class="col-md-3 sticker-padding">
                                <div class="sticker reveal fade-anim-left-btn is-inview" data-scroll=""
                                    data-delay="800" data-scroll-call="reveal">
                                    <img src="/img/innovation.png" alt="awards"
                                    width="220" height="220" class="lazyloaded" data-ll-status="loaded">
                                </div>
                            </div>
                            <div class="col-md-3 sticker-padding">
                                <div class="sticker reveal fade-anim-left-btn is-inview" data-scroll=""
                                    data-delay="1100" data-scroll-call="reveal">
                                    <img src="/img/special-desing.png" alt="awards"
                                            width="220" height="220" class="lazyloaded" data-ll-status="loaded">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="full-hd-image reveal fade-anim-bottom is-inview" data-delay="300" data-delay-mobile="300"
            data-scroll="" data-scroll-call="reveal">
            <img src="/img/zenn-2.jpg" width="1500" height="732" alt="Zenn&#39;s Foto"
            class="lazyloaded" data-ll-status="loaded">
        </div>
        <div class="showcase-images">
            <div class="row">
                <div class="col-md-6">
                    <div class="more-images reveal fade-anim-bottom is-inview" data-delay="300"
                        data-delay-mobile="200" data-scroll="" data-scroll-call="reveal">
                        <img src="/img/zenn-3.jpg" width="700" height="1114"
                        alt="Zenn&#39;s Foto" class="lazyloaded" data-ll-status="loaded">
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="more-images second reveal fade-anim-bottom is-inview" data-delay="300"
                        data-delay-mobile="200" data-scroll="" data-scroll-call="reveal">
                        <img src="/img/zenn-4.jpg" width="700" height="1057"
                        alt="Zenn&#39;s Foto" class="lazyloaded" data-ll-status="loaded">
                    </div>
                </div>
            </div>
        </div>
        <div class="next-project center">
            <a href="" class="btn TEST">
                <div class="btn-wrapper">
                    <div class="button active">Dự án kế tiếp</div>
                    <div class="button hover">Dự án kế tiếp</div>
                </div>
            </a>
            <a href="" class="next-project-name">
                Don Iron </a>
        </div>
    </div>
</section>

@include('frontend.home.includes.team')