<div class="art-story" style="margin-bottom: 0px;">
    <div class="container">
        <div class="story">
            <div class="fire1">
                <div class="reveal fade-anim-zoom"><img src="/img/fire4-1.png" alt="fire"></div>
            </div>
            <div class="row">
                <div class="col-12">
                    <div class="row">
                        <div class="col-12 col-lg-6 order-2">
                            <div class="story-description">
                                <div class="story-title in">
                                    <h3 class="reveal text-anim-bottom" data-delay="500"><span>Fallen Angel</span></h3>
                                </div>
                                <div class="story-text">
                                    <div class="reveal fade-anim-top-small" data-delay="700">
                                        <p>Hình xăm thiên thần sa ngã mang ý nghĩa tích cực là sự cố gắng và nỗ lực vực qua những khó khăn, vấp ngã của cuộc sống để thay đổi số phận, thay đổi bản thân, khắc phục lỗi lầm và hoàn thiện mình hơn.</p>
                                        <p class="full-description">This breed of dog, known to be active and
                                            Thiên thần sa ngã có nhiều thiết kế khác nhau như hướng mắt nhìn về bầu trời hồi tưởng quá khứ, hình ảnh quỳ gối với đôi cánh gãy. Trông khá tiêu cực, thể hiện sự thất bại, vấp ngã, đau đớn và tổn thương sâu sắc. Hình ảnh gợi đến những nốt trầm của cuộc sống trong mỗi con người mà ai cũng phải trải qua để trưởng thành.
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="image-info">
                            <div class="img-wrapper">
                                <div class="image-title reveal letter-animation image1">
                                    <ul class="layer-items6" data-relative-input="true" data-method="itemParallax6"
                                        style="transform: translate3d(0px, 0px, 0px) rotate(0.0001deg); transform-style: preserve-3d; backface-visibility: hidden; position: relative; pointer-events: none;">
                                        <li class="layer-3 layer" data-depth="0.15"
                                            style="transform: translate3d(5.1px, 230.1px, 0px); transform-style: preserve-3d; backface-visibility: hidden; position: relative; display: block; left: 0px; top: 0px;">
                                            <span style="color:#9b261c">Fallen Angel</span>
                                        </li>
                                        <li class="layer" data-depth="0.30"
                                            style="transform: translate3d(10.2px, 460.2px, 0px); transform-style: preserve-3d; backface-visibility: hidden; position: absolute; display: block; left: 0px; top: 0px;">
                                            <div class="circle6 circle reveal fade-anim-zoom" data-delay="100">
                                                <img src="/img/trace4-1.png" alt="Fallen angel">
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                                <div class="img-content">
                                    <div class="reveal fade-anim-zoom" data-delay="900">
                                        <img width="814" height="814" src="/img/tatto19.jpg"
                                            class="story-image obj-fit-cover wp-post-image"
                                            alt="Fallen angel" loading="lazy"
                                            srcset="/img/tatto19.jpg"
                                            sizes="(max-width: 814px) 100vw, 814px">
                                        <div class="dimension right-dimension">Kích thước<br>
                                            60x40cm</div>
                                    </div>
                                    <div class="part-image">
                                        <a href="/img/tatto19.jpg"
                                            data-fancybox="gallery" class="no-barba reveal fade-anim-left"
                                            data-delay="300"><img src="/img/tatto19.jpg"
                                                alt="Fallen angel"></a>
                                        <a href="/img/tatto20.jpg"
                                            data-fancybox="gallery" class="no-barba reveal fade-anim-left"
                                            data-delay="500"><img src="/img/tatto20.jpg"
                                                alt="Fallen angel"></a>
                                        <a href="/img/tatto21.jpg" data-fancybox="gallery"
                                            class="no-barba reveal fade-anim-left" data-delay="700"><img
                                                src="/img/tatto21.jpg" alt="Fallen angel"></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="center">
    <div class="title center">
        <a href="/" class="backto">
            <h2 class="reveal text-anim-bottom"><span>Quay lại những câu chuyện</span></h2>
        </a>
    </div>
</div>
