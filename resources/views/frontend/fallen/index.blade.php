@extends('frontend.layouts.iconic')

@section('title', __('Fallen Angel'))

@section('content')
@include('frontend.fallen.includes.detail')
@include('frontend.home.includes.commision')
@endsection
