<div class="art-story" style="margin-bottom: 0px;">
    <div class="container">
        <div class="story">
            <div class="fire1">
                <div class="reveal fade-anim-zoom"><img src="/img/fire2-1.png" alt="fire"></div>
            </div>
            <div class="row">
                <div class="col-12">
                    <div class="row">
                        <div class="col-12 col-lg-6 order-2">
                            <div class="story-description">
                                <div class="story-title in">
                                    <h3 class="reveal text-anim-bottom" data-delay="500"><span>The Last Time</span></h3>
                                </div>
                                <div class="story-text">
                                    <div class="reveal fade-anim-top-small" data-delay="700">
                                        <p>
                                            Thời gian không là vô giá, bạn không thể mua nó và cũng không thể quay ngược thời gian. Bạn có thể thấy rõ thời gian trôi qua như thế nào nhưng bạn không bao giờ có thể ngăn chặn được nó. Thời gian được biểu thị thông qua một chiếc đồng hồ và khi đồng hồ in trên cơ thể bạn sẽ mang triết lý của thời gian vô tận.
                                        </p>
                                        <p class="full-description">Hình xăm đồng hồ nhắc nhở chúng ta sống trọn vẹn từng giây phút và hãy làm cho cuộc sống trở nên đáng sống bằng cách nhận ra giá trị của từng khoảnh khắc thời gian.</p>
                                        <p class="full-description"> Tình yêu bất tận còn được thể hiện thông qua hình xăm đồng hồ như kết hợp với hình ảnh hồi ức cùng cha mẹ, người thương bằng cách kết hợp tên, ngày sinh và khuôn mặt của người đó.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="image-info">
                            <div class="img-wrapper">
                                <div class="image-title reveal letter-animation image1">
                                    <ul class="layer-items5" data-relative-input="true" data-method="itemParallax5"
                                        style="transform: translate3d(0px, 0px, 0px) rotate(0.0001deg); transform-style: preserve-3d; backface-visibility: hidden; position: relative; pointer-events: none;">
                                        <li class="layer-3 layer" data-depth="0.15"
                                            style="transform: translate3d(14.6px, 192.7px, 0px); transform-style: preserve-3d; backface-visibility: hidden; position: relative; display: block; left: 0px; top: 0px;">
                                            <span style="color:#c89a4c">The Last Time</span>
                                        </li>
                                        <li class="layer" data-depth="0.30"
                                            style="transform: translate3d(29.3px, 385.5px, 0px); transform-style: preserve-3d; backface-visibility: hidden; position: absolute; display: block; left: 0px; top: 0px;">
                                            <div class="circle5 circle reveal fade-anim-zoom" data-delay="100">
                                                <img src="img/trace3-1.png" alt="brush">
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                                <div class="img-content">
                                    <div class="reveal fade-anim-zoom" data-delay="900">
                                        <img width="807" height="808" src="/img/tatto15.jpg"
                                            class="story-image obj-fit-cover wp-post-image" alt="The Last Time"
                                            loading="lazy" srcset="/img/tatto15.jpg"
                                            sizes="(max-width: 807px) 100vw, 807px">
                                        <div class="dimension left-dimension">Kích thước<br>
                                            45x90cm</div>
                                    </div>
                                    <div class="part-image">
                                        <a href="/img/tatto16.jpg" data-fancybox="gallery"
                                            class="no-barba reveal fade-anim-left" data-delay="300"><img
                                                src="/img/tatto16.jpg" alt="The Last Time"></a>
                                        <a href="/img/tatto17.jpg" data-fancybox="gallery"
                                            class="no-barba reveal fade-anim-left" data-delay="500"><img
                                                src="/img/tatto17.jpg" alt="The Last Time"></a>
                                        <a href="/img/tatto18.jpg" data-fancybox="gallery"
                                            class="no-barba reveal fade-anim-left" data-delay="700"><img
                                                src="/img/tatto18.jpg" alt="The Last Time"></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="center">
    <div class="title center">
        <a href="/" class="backto">
            <h2 class="reveal text-anim-bottom"><span>Quay lại những câu chuyện</span></h2>
        </a>
    </div>
</div>
