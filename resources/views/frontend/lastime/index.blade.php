@extends('frontend.layouts.iconic')

@section('title', __('The Last Time'))

@section('content')
@include('frontend.lastime.includes.detail')
@include('frontend.home.includes.commision')
@endsection
