@extends('frontend.layouts.iconic')

@section('title', __('Animals'))

@section('content')
@include('frontend.animals.includes.detail')
@include('frontend.home.includes.commision')
@endsection
