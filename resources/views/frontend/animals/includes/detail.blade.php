<div class="art-story" style="margin-bottom: 0px;">
    <div class="container">
        <div class="story">
            <div class="fire1">
                <div class="reveal fade-anim-zoom"><img src="/img/fire1-1.png"
                        alt="fire"></div>
            </div>
            <div class="row">
                <div class="col-12">
                    <div class="row">
                        <div class="col-12 col-lg-6 order-2">
                            <div class="story-description">
                                <div class="story-title in">
                                    <h3 class="reveal text-anim-bottom" data-delay="500"><span>Animals</span></h3>
                                </div>
                                <div class="story-text">
                                    <div class="reveal fade-anim-top-small" data-delay="700">
                                        <p>Biểu tượng Hổ gợi lên những ý tưởng về Sức mạnh và sự dũng mãnh. Trong đạo phật, sức mạnh của Hổ còn tượng trưng cho sức mạnh của đức tin, của nỗ lực tinh thần, vượt qua  tội lỗi.</p>
                                        <p class="full-description">This breed of dog, known to be active and
                                            Hổ là biểu trưng cho sức mạnh, thường được dùng như một thứ bùa hộ mệnh, bảo vệ gia đình… để chống lại các thế lực ma quỉ. Hình Xăm Con Hổ gợi lên những ý tưởng về sức mạnh và tính hung dữ. Nó là biểu tượng của đẳng cấp chiến binh. 5 con hổ là biểu tượng của sức mạnh che chở. Hổ trắng là 1 dấu hiệu của đức độ nhà vua. Hình xăm con Hổ tượng trưng cho ý thức trở nên u tối,do bị tràn ngập bởi những ham muốn sơ đẳng nổi lên ko kìm giữ được.
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="image-info">
                            <div class="img-wrapper">
                                <div class="image-title reveal letter-animation image1">
                                    <ul class="layer-items1" data-relative-input="true" data-method="itemParallax1"
                                        style="transform: translate3d(0px, 0px, 0px) rotate(0.0001deg); transform-style: preserve-3d; backface-visibility: hidden; position: relative; pointer-events: none;">
                                        <li class="layer-3 layer" data-depth="0.15"
                                            style="transform: translate3d(6.8px, 40.2px, 0px); transform-style: preserve-3d; backface-visibility: hidden; position: relative; display: block; left: 0px; top: 0px;">
                                            <span style="color:#981c20">Animals</span></li>
                                        <li class="layer" data-depth="0.30"
                                            style="transform: translate3d(13.5px, 80.4px, 0px); transform-style: preserve-3d; backface-visibility: hidden; position: absolute; display: block; left: 0px; top: 0px;">
                                            <div class="circle1 circle reveal fade-anim-zoom" data-delay="100">
                                                <img src="img/trace1-1.png" alt="brush">
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                                <div class="img-content">
                                    <div class="reveal fade-anim-zoom" data-delay="900">
                                        <img width="810" height="810" src="/img/tatto5.jpg"
                                            class="story-image obj-fit-cover wp-post-image" alt="Tatto Animals"
                                            loading="lazy" srcset="/img/tatto5.jpg"
                                            sizes="(max-width: 810px) 100vw, 810px">
                                        <div class="dimension left-dimension">Kích thước<br>
                                            99x99cm</div>
                                    </div>
                                    <div class="part-image">
                                        <a href="/img/tatto5.jpg" data-fancybox="gallery"
                                            class="no-barba reveal fade-anim-left" data-delay="300"><img
                                                src="/img/tatto5.jpg" alt="Tatto Animals"></a>
                                        <a href="/img/tatto6.jpg" data-fancybox="gallery"
                                            class="no-barba reveal fade-anim-left" data-delay="500"><img
                                                src="/img/tatto6.jpg" alt="Tatto Animals"></a>
                                        <a href="/img/tatto7.jpg" data-fancybox="gallery"
                                            class="no-barba reveal fade-anim-left" data-delay="700"><img
                                                src="/img/tatto7.jpg" alt="Tatto Animals"></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="center">
    <div class="title center">
        <a href="/" class="backto">
            <h2 class="reveal text-anim-bottom"><span>Quay lại những câu chuyện</span></h2>
        </a>
    </div>
</div>
