@extends('frontend.layouts.iconic')

@section('title', __('Khách hàng'))

@section('content')
@include('frontend.customer.includes.intro')
@include('frontend.customer.includes.review')
@endsection
