<section class="intro">
    <div class="smoke reveal fade-anim-default" data-delay="100"><img src="img/white-smoke.png" alt="fire"></div>
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="row">
                    <div class="intro-image reveal fade-anim-zoom" data-delay="700">
                        <img width="1047" height="698" src="/img/art-sculptures-renaissance-statues.jpg" class="obj-fit-cover wp-post-image"
                            alt="Studio Minh Tatto at May Fine Art Studio Vienna" loading="lazy"
                            srcset="/img/art-sculptures-renaissance-statues.jpg"
                            sizes="(max-width: 1047px) 100vw, 1047px"> </div>
                    <div class="col-12 col-lg-7 intro-order no-padding">
                        <div class="intro-wrapper">
                            <div class="title">
                                <h1 class="reveal text-anim-bottom" data-delay="300">
                                    <span>Trải nghiệm khách hàng</span></h1>
                            </div>
                            <div class="intro-text">
                                <div class="reveal text-anim-top" data-delay="500">
                                    <p>Đến với Studio Minh Tatto, quý khách hàng được trải nghiệm một văn hóa xăm độc đáo với nhiều kiểu thiết kế khác nhau. Không gian được thiết kế theo kiểu Âu Mỹ, ấm cúng nhưng rất thoáng mát. Hãy <a href=""><strong>liên hệ với Studio Minh Tatto</strong></a> để chúng tôi có thể trao tác phẩm nghệ thuật tuyệt vời đế cho các bạn. </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
