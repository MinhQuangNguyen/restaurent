
<section class="reviews">
  <div class="container">
    <div class="row ">
      <div class="no-padding col-lg-6 col-md-7 col-sm-9 ofsset-lg-1 ">
        <div class="review">
          <h2 class="reveal text-anim-bottom" data-delay="300"><span>Minh Quang Nguyen</span></h2>
          <div class="review-text">
            <img  src="img/quote.png" class="quote" alt="quote">
            <div class="reveal text-anim-top text-index" data-delay="500">
              <p class="interest">(Giám đốc điều hành Studio Media Iconic)</p>
            </div>
            <div class="reveal text-anim-top text-index" data-delay="500">
              <p>Tác phẩm xăm hình chân dung của Studio Minh Tatto là một tác phẩm tuyệt vời, mang vẻ đẹp thuần dã và cá tính mạnh. Các đường vẽ dứt khoát, mang lại cảm giác chắc tay trong mỗi đường vẽ.</p>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="row justify-content-end">
      <div class="no-padding col-lg-6 col-md-7 col-sm-9 offset-lg-1-right ">
        <div class="review">
          <h2 class="reveal text-anim-bottom" data-delay="300"><span>Vũ Thị Hồng Ngọc</span></h2>
          <div class="review-text">
            <img  src="img/quote.png" class="quote" alt="quote">
            <div class="reveal text-anim-top text-index" data-delay="500">
              <p class="interest">(Giám đốc Học viện Nghệ thuật Thiên thần, Florence, Việt Nam)</p>
            </div>
            <div class="reveal text-anim-top text-index" data-delay="500">
              <p>Studio Minh Tatto mang lại cảm hứng nghệ thuật mạnh mẽ với phong cách Dark Greek Gods, các Artist tại Studio Minh Tatto rất chuyên nghiệp và thân thiện. Tôi rất hài lòng khi sở hữu một tác phẩm nghệ thuật.</p>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="row ">
      <div class="no-padding col-lg-6 col-md-7 col-sm-9 ofsset-lg-1 ">
        <div class="review">
          <h2 class="reveal text-anim-bottom" data-delay="300"><span>Hoàng Văn Sơn</span></h2>
          <div class="review-text">
            <img  src="img/quote.png" class="quote" alt="quote">
            <div class="reveal text-anim-top text-index" data-delay="500">
              <p class="interest">(nhà sử học nghệ thuật, giám tuyển, nhà phê bình nghệ thuật)</p>
            </div>
            <div class="reveal text-anim-top text-index" data-delay="500">
              <p>Tại Studio Minh Tatto mọi ý tưởng đều trở thành đột phá trong mỗi tác phẩm, tôi rất hài lòng khi được trải nghiệm tại studio.</p>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="row justify-content-end">
      <div class="no-padding col-lg-6 col-md-7 col-sm-9 offset-lg-1-right ">
        <div class="review">
          <h2 class="reveal text-anim-bottom" data-delay="300"><span>Hoàng Thùy Linh</span></h2>
          <div class="review-text">
            <img  src="img/quote.png" class="quote" alt="quote">
            <div class="reveal text-anim-top text-index" data-delay="500">
              <p class="interest">(Ca sĩ, diễn viên)</p>
            </div>
            <div class="reveal text-anim-top text-index" data-delay="500">
              <p>Thật bất ngờ khi trao đổi các ý tưởng với Artist của Studio Minh Tatto, mọi thứ trở lên rất mới mẻ và độc lập trong mỗi tác phẩm. Tôi sở hữu một tác phẩm được thiết kế riêng. Tôi rất vui và hài lòng khi trải nghiệm tại Studio Minh Tatto.</p>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="row ">
      <div class="no-padding col-lg-6 col-md-7 col-sm-9 ofsset-lg-1 ">
        <div class="review">
          <div class="fire"><img  src="img/fire-about.png" alt="fire"></div>
          <h2 class="reveal text-anim-bottom" data-delay="300"><span>Nguyễn Ánh Tuyết</span></h2>
          <div class="review-text">
            <img  src="img/quote.png" class="quote" alt="quote">
            <div class="reveal text-anim-top text-index" data-delay="500">
              <p class="interest">(Giám đốc điều hành chuỗi cửa hàng thời)</p>
            </div>
            <div class="reveal text-anim-top text-index" data-delay="500">
              <p>Các tác phẩm của Studio Minh Tatto đều là nguồn cảm hứng nghệ thuật tuyệt vời, các nhân tôi đánh giá cao về Studio Minh Tatto từ phòng xăm cho đến các Artist. Một nơi đáng để trao niềm tin.</p>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="row justify-content-end">
      <div class="no-padding col-lg-6 col-md-7 col-sm-9 offset-lg-1-right ">
        <div class="review">
          <h2 class="reveal text-anim-bottom" data-delay="300"><span>Trịnh Cao Sơn</span></h2>
          <div class="review-text">
            <img  src="img/quote.png" class="quote" alt="quote">
            <div class="reveal text-anim-top text-index" data-delay="500">
              <p class="interest">(Biên tập viên đài truyền hình)</p>
            </div>
            <div class="reveal text-anim-top text-index" data-delay="500">
              <p>Kỹ thuật xăm của mỗi Artist tại Studio Minh Tatto đều rất chuyện nghiệp, xăm không có cảm giác quá đau, vết thương lành rất nhanh cũng như hình xăm rất nét sau khi phục hồi hoàn toàn.</p>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="row ">
      <div class="no-padding col-lg-6 col-md-7 col-sm-9 ofsset-lg-1 ">
        <div class="review">
          <h2 class="reveal text-anim-bottom" data-delay="300"><span>Lê Gia Hưng</span></h2>
          <div class="review-text">
            <img  src="img/quote.png" class="quote" alt="quote">
            <div class="reveal text-anim-top text-index" data-delay="500">
              <p class="interest">(Hoạt động trong Đại học sự phậm Nghệ Thuật Trung Ương)</p>
            </div>
            <div class="reveal text-anim-top text-index" data-delay="500">
              <p>Kiến thức tại Artist Studio Minh Tatto rất tốt, tôi rất bất ngờ khi được Artist tư vấn cho mỗi ý tưởng của tôi. Trải nghiệm rất hài lòng khi ở Studio Minh Tatto.</p>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="row justify-content-end">
      <div class="no-padding col-lg-6 col-md-7 col-sm-9 offset-lg-1-right ">
        <div class="review">
          <div class="fire1"><img  src="img/fire4.png" alt="fire"></div>
          <h2 class="reveal text-anim-bottom" data-delay="300"><span>Lê Vĩnh Bảo</span></h2>
          <div class="review-text">
            <img  src="img/quote.png" class="quote" alt="quote">
            <div class="reveal text-anim-top text-index" data-delay="500">
              <p class="interest">(nhà sử học nghệ thuật, giám tuyển, nhà phê bình nghệ thuật, tác giả)</p>
            </div>
            <div class="reveal text-anim-top text-index" data-delay="500">
              <p>Các đường vẽ của Artist Studio Minh Tatto rất uyển chuyển và cứng cáp, tạo ra một tác phẩm nghệ thuật trên da rất tuyệt vời. Tôi cảm thấy vinh dự khi có một tác phẩm nghệ thuật chân dùng trên chính cơ thể của tôi.</p>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="row ">
      <div class="no-padding col-lg-6 col-md-7 col-sm-9 ofsset-lg-1 ">
        <div class="review">
          <h2 class="reveal text-anim-bottom" data-delay="300"><span>Phạm Gia Huy</span></h2>
          <div class="review-text">
            <img  src="img/quote.png" class="quote" alt="quote">
            <div class="reveal text-anim-top text-index" data-delay="500">
              <p class="interest">(Giám đốc điều hành, Nhà xuất bản Mỹ thuật Buckingham)</p>
            </div>
            <div class="reveal text-anim-top text-index" data-delay="500">
              <p>Lần đầu tiên thôi nhìn thấy một hình vẽ mà Artist của Studio Minh Tatto thiết kế dành riêng cho tôi. Tôi có thể thành thật nói rằng tôi đã há hốc mồm khi thấy vẻ đẹp thuần khiết, mềm mại và rực rỡ của tác phẩm. Tôi rất vui khi được giới thiệu cho những người bạn về tác phẩm của tôi. Bây giờ tôi chỉ có thể nói rằng, tôi cảm thấy đặc biệt khi trải nghiệm tại Studio Minh Tatto</p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>