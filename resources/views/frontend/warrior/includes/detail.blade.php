<div class="art-story" style="margin-bottom: 0px;">
    <div class="container">
        <div class="story">
            <div class="fire1">
                <div class="reveal fade-anim-zoom"><img src="/img/fire-first-day.png" alt=""></div>
            </div>
            <div class="row">
                <div class="col-12">
                    <div class="row">
                        <div class="col-12 col-lg-6 order-2">
                            <div class="story-description">
                                <div class="story-title in">
                                    <h3 class="reveal text-anim-bottom" data-delay="500"><span>Ultimate Warrior</span></h3>
                                </div>
                                <div class="story-text">
                                    <div class="reveal fade-anim-top-small" data-delay="700">
                                        <p>Ý nghĩa hình xăm Samurai  là đại diện cho sự công bằng, lòng can đảm, sự hướng thiện, sự tôn trọng, danh dự và lòng trung thành của những bậc chính nhân quân tử. Vì Samurai là những người bảo vệ thời đại Edo xưa. Rất được nể trọng.</p>
                                        <p class="full-description">
                                            Một trong số những ý nghĩa hình xăm chiến binh Samurai có được chính là triết lý về sự công bằng, lẽ phải trong xã hội cổ đại xa xưa.
                                            Thể hiện một con người chính trực trong cả hàng động lẫn lời nói và công lý được tạo nên từ chính sự công bằng trong xã hội này.
                                            Với Samurai chỉ có màu trắng và đen biểu hiện của sự thật và dối trá, đúng và sai.
                                            Cho thấy đó là người có tâm hồn trong sáng, hướng thiện và luôn sẵn sàng chiến đấu để tìm thấy sự thật, công lý mà không ngại bất kỳ thế lực nào.
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="image-info">
                            <div class="img-wrapper">
                                <div class="image-title reveal letter-animation image1">
                                    <ul class="layer-items3" data-relative-input="true" data-method="itemParallax3"
                                    style="transform: translate3d(0px, 0px, 0px) rotate(0.0001deg); transform-style: preserve-3d; backface-visibility: hidden; position: relative; pointer-events: none;">
                                    <li class="layer-3 layer" data-depth="0.15"
                                        style="transform: translate3d(10.7px, 114.5px, 0px); transform-style: preserve-3d; backface-visibility: hidden; position: relative; display: block; left: 0px; top: 0px;">
                                        <span style="color:#36d080">Ultimate Warrior</span>
                                    <li class="layer" data-depth="0.30"
                                        style="transform: translate3d(21.5px, 229px, 0px); transform-style: preserve-3d; backface-visibility: hidden; position: absolute; display: block; left: 0px; top: 0px;">
                                        <div class="circle3 circle reveal fade-anim-zoom" data-delay="100">
                                            <img src="/img/tockice2.png" alt="Ultimate Warrior">
                                        </div>
                                    </li>
                                </ul>
                                </div>
                                <div class="img-content">
                                    <div class="reveal fade-anim-zoom" data-delay="900">
                                        <img width="1200" height="1200" src="/img/tatto8.jpg"
                                            class="story-image obj-fit-cover wp-post-image"
                                            alt="Oil painting First Day by Damir May" loading="lazy"
                                            srcset="/img/tatto8.jpg"
                                            sizes="(max-width: 1200px) 100vw, 1200px">
                                        <div class="dimension left-dimension">Kích thước<br>
                                            200 x 275 cm</div>
                                    </div>
                                    <div class="part-image">
                                        <a href="/img/tatto9.jpg"
                                            data-fancybox="gallery" class="no-barba reveal fade-anim-left"
                                            data-delay="300"><img src="/img/tatto9.jpg"
                                                alt="Ultimate Warrior"></a>
                                        <a href="/img/tatto10.jpg"
                                            data-fancybox="gallery" class="no-barba reveal fade-anim-left"
                                            data-delay="500"><img src="/img/tatto10.jpg"
                                                alt="Ultimate Warrior"></a>
                                        <a href="/img/tatto11.jpg"
                                            data-fancybox="gallery" class="no-barba reveal fade-anim-left"
                                            data-delay="700"><img src="/img/tatto11.jpg"
                                                alt="Ultimate Warrior"></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="center">
    <div class="title center">
        <a href="/" class="backto">
            <h2 class="reveal text-anim-bottom"><span>Quay lại những câu chuyện</span></h2>
        </a>
    </div>
</div>
