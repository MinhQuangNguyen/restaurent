@extends('frontend.layouts.iconic')

@section('title', __('Ultimate Warrior'))

@section('content')
@include('frontend.warrior.includes.detail')
@include('frontend.home.includes.commision')
@endsection
