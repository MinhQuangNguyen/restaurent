<div class="potraits">
    <div class="container">
        <div class="row">
            @foreach($posts as $post)
                <div class="col-12 col-lg-4 col-md-6">
                    <div class="portrait-wrapper">
                        <a href="{{ asset($post->image) }}" data-fancybox="gallery"
                            class="no-barba" data-caption="{{ $post->title }} &lt;br&gt; {{ $post->tagser }}">
                            <div class="portrait">
                                <div class="portrait-image">
                                    <img src="{{ asset($post->image) }}" class="obj-fit-cover"
                                        alt="{{ $post->title }}">
                                </div>
                                <div class="portrait-name">
                                    Chế tác bởi </br> {{ $post->author }}</div>
                            </div>
                        </a>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
</div>
