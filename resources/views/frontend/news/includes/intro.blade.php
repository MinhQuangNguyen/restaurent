<section class="intro">
    <div class="smoke reveal fade-anim-default" data-delay="100"><img src="img/white-smoke.png" alt="fire">
    </div>
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="row">
                    <div class="intro-image reveal fade-anim-zoom nopicture" data-delay="700">
                    </div>
                    <div class="col-12 col-lg-7 intro-order no-padding">
                        <div class="intro-wrapper">
                            <div class="title">
                                <h1 class="reveal text-anim-bottom" data-delay="300">
                                    <span>Studio Minh Tatto</span></h1>
                            </div>
                            <div class="intro-text">
                                <div class="reveal text-anim-top" data-delay="500">
                                    <p>Chúng tôi pheo phong cách chủ đạo Âu Mỹ, với thể loại chân dung, loài vật, bóng tối và các thiết kế không gian âm. Chúng tôi sử dụng các thiết bị, mực tiên tiến nhất hiện nay cũng như các artist chuyên nghiệp sẵn sàng phục vụ các bạn. Hãy <a href=""><strong>liên hệ với Studio Minh Tatto</strong></a> để trải nghiệm.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>