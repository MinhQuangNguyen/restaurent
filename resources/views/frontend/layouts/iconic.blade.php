<!doctype html>
<html lang="{{ htmlLang() }}" @langrtl dir="rtl" @endlangrtl>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ appName() }} | @yield('title')</title>
    <meta name="author" content="@yield('meta_author', 'Studio Minh Tatto')">

    <meta property="og:locale" content="{{ htmlLang() }}" />
    <meta property="og:title" content="@yield('title', appName())" />
    <meta property="og:type" content="website" />
    <meta property="og:url" content="{{ request()->fullUrl() }}" />
    <meta property="og:description" name="description" content="Studio Minh Tatto, chuyên thiết kế hình xăm bóng tối phong cách Âu Mỹ" />
    <meta property="og:site_name" content="{{ appURL() }}" />
    <meta property="og:image" content="/img/meta.png" />
    <link rel="icon" sizes="94x94" type="image/gif" href="/img/flaticon.png"/>
    @yield('meta')
    @stack('before-styles')
    <link href="{{ mix('css/frontend.css') }}" rel="stylesheet">
    <livewire:styles />
    @stack('after-styles')
</head>

<body>


    <div id="app">
        <div id="page-wrapper">
            <div id="barba-wrapper" aria-live="polite">
                <div class="home page-template page-template-tpl-home page-template-tpl-home-php page page-id-7 barba-container single-author singular"
                    data-namespace="page">
                    <main id="barba-wrapper" data-barba="container" data-barba-namespace="home">
                        <div id="js-scroll">
                            @include('frontend.includes.header', ['menu' => $menu])

                            @yield('content')
                            @include('frontend.includes.footer')
                        </div>
                    </main>
                </div>
            </div>
        </div>
    </div>
    <!--app-->

    @stack('before-scripts')
    <script src='{{ asset('js/jquery.min.js') }}'></script>
    <script src='{{ asset('js/script.js') }}'></script>
    <script src='{{ asset('js/jquery.fancybox.min.js') }}'></script>
    <livewire:scripts />
    @stack('after-scripts')

</body>

</html>
