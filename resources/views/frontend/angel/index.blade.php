@extends('frontend.layouts.iconic')

@section('title', __('Dark Angel'))

@section('content')
@include('frontend.angel.includes.detail')
@include('frontend.home.includes.commision')
@endsection
