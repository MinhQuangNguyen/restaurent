<div class="art-story" style="margin-bottom: 0px;">
    <div class="container">
        <div class="story">
            <div class="fire1">
                <div class="reveal fade-anim-zoom"><img src="/img/fire-helena.png" alt=""></div>
            </div>
            <div class="row">
                <div class="col-12">
                    <div class="row">
                        <div class="col-12 col-lg-6 order-2">
                            <div class="story-description">
                                <div class="story-title in">
                                    <h3 class="reveal text-anim-bottom" data-delay="500"><span>Dark Angel</span></h3>
                                </div>
                                <div class="story-text">
                                    <div class="reveal fade-anim-top-small" data-delay="700">
                                        <p>
                                            Thần chết là biểu tượng cho sự chết chóc, mát mát. Có người lại xem nó như một hình ảnh buồn trong cuộc sống. Nhưng cũng có nhiều người cho rằng thần chết là biểu tượng của sức mạnh. Không phải ai cũng có thể lựa chọn cho mình hình xăm này.
                                        </p>
                                        <p class="full-description">
                                            Hình xăm thần chết còn mang một ý nghĩa dũng cảm. Vì chỉ có những người kiên cường, dũng cảm mới dám xăm hình này lên cơ thể của mình. Bởi cái chết là nỗi sợ lớn nhất với mỗi con người. Đặc biệt là đối với những kẻ hèn nhát và sợ sệt. Ngoài ra, nó còn tôn vinh cái tôi của bản thân. Phải luôn tuân thủ với những quy tắc mà bản thân đã đề ra.
                                        </p>
                                        <p class="full-description">
                                            Có rất nhiều kiểu hình xăm thần chết khác nhau. Và cách thể hiện chúng như thế nào cũng do mỗi người khác nhau thể hiện nó. Đồng thời hình xăm có đẹp hay không cũng ohuj thuộc vào tay nghề của mỗi người thợ khác nhau. Một số vị trí xăm trên cơ thể đẹp và độc đáo. Có thể toát lên được bản linh của bản thân. Cũng như khiến nhiều người phải trầm trồ đến nó. Như là xăm kín lưng, kín ngực. Hoặc là xăm ở ngay bắp tay sẽ khẳng định được cái tôi mạnh mẽ của bản thân. Và muốn mọi người nhìn vào phải trầm trồ về nó.
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="image-info">
                            <div class="img-wrapper">
                                <div class="image-title reveal letter-animation image1">
                                    <ul class="layer-items2" data-relative-input="true" data-method="itemParallax2"
                                        style="transform: translate3d(0px, 0px, 0px) rotate(0.0001deg); transform-style: preserve-3d; backface-visibility: hidden; position: relative; pointer-events: none;">
                                        <li class="layer-3 layer" data-depth="0.15"
                                            style="transform: translate3d(10.6px, 79.3px, 0px); transform-style: preserve-3d; backface-visibility: hidden; position: relative; display: block; left: 0px; top: 0px;">
                                            <span style="color:#e59d82">Dark Angel</span>
                                        </li>
                                        <li class="layer" data-depth="0.30"
                                            style="transform: translate3d(21.2px, 158.7px, 0px); transform-style: preserve-3d; backface-visibility: hidden; position: absolute; display: block; left: 0px; top: 0px;">
                                            <div class="circle2 circle reveal fade-anim-zoom" data-delay="100">
                                                <img src="/img/tockice1.png" alt="">
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                                <div class="img-content">
                                    <div class="reveal fade-anim-zoom" data-delay="900">
                                        <img width="810" height="810" src="/img/tatto1.jpg"
                                        class="story-image obj-fit-cover wp-post-image"
                                        alt="Tatto Seven deadly sins " loading="lazy"
                                        srcset="/img/tatto1.jpg"
                                        sizes="(max-width: 810px) 100vw, 810px">
                                        <div class="dimension right-dimension">Kích thước<br>
                                            60x40 cm</div>
                                    </div>
                                    <div class="part-image">
                                        <a href="/img/tatto2.jpg"
                                            data-fancybox="gallery" class="no-barba reveal fade-anim-left"
                                            data-delay="300"><img src="/img/tatto2.jpg" alt="Tatto Seven deadly sins "></a>
                                        <a href="/img/tatto3.jpg"
                                            data-fancybox="gallery" class="no-barba reveal fade-anim-left"
                                            data-delay="500"><img src="/img/tatto3.jpg" alt="Tatto Seven deadly sins "></a>
                                        <a href="/img/tatto4.jpg"
                                            data-fancybox="gallery" class="no-barba reveal fade-anim-left"
                                            data-delay="700"><img src="/img/tatto4.jpg" alt="Tatto Seven deadly sins "></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="center">
    <div class="title center">
        <a href="/" class="backto">
            <h2 class="reveal text-anim-bottom"><span>Quay lại những câu chuyện</span></h2>
        </a>
    </div>
</div>
