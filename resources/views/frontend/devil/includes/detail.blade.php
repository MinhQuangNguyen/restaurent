<div class="art-story" style="margin-bottom: 0px;">
    <div class="container">
        <div class="story">
            <div class="fire1">
                <div class="reveal fade-anim-zoom"><img src="/img/fire2-1.png" alt="fire"></div>
            </div>
            <div class="row">
                <div class="col-12">
                    <div class="row">
                        <div class="col-12 col-lg-6 order-2">
                            <div class="story-description">
                                <div class="story-title in">
                                    <h3 class="reveal text-anim-bottom" data-delay="500"><span>Devil's Fairy</span></h3>
                                </div>
                                <div class="story-text">
                                    <div class="reveal fade-anim-top-small" data-delay="700">
                                        <p>Quỷ – biểu tượng của một thế lực đen tối mang sức mạnh tàn bạo, hủy diệt tiềm ẩn trong cuộc sống. Nhắc đến mặt quỷ, chúng ta sẽ nghĩ ngay đến sự dữ dằn, hung tợn của nó như thế nào.</p>
                                        <p class="full-description">
                                            Nhiều người lâu nay vẫn lầm tưởng rằng hình xăm mặt quỷ là biểu tượng cho sự xấu xa, sự tàn bạo, hung ác. Bởi vậy, khi gắn với hình tượng này, người ta sẽ nghĩ nó mang lại sự chết chóc, xấu xa,tàn ác và cả sự xúi quẩy.  Đó là lí do mọi người thường tìm đủ mọi cách, biện pháp để xua đuổi ma quỷ như một thứ tà dịch ra khỏi cuộc sống của mình. Và tất nhiên, khi con người làm ra những điều độc ác, người ta lại lấy chính hình tượng của quỷ ra để so sánh.
                                        </p>
                                        <p class="full-description">
                                            Tuy vậy, trên thực tế, xét từ nguồn gốc của hình tượng, quỷ được sáng tạo ra như một ý nghĩa tích cực trong cuộc sống, như một phép thử nhằm giúp con người sống tốt hơn đúng với chứ NGƯỜI viết hoa hơn. Do đó, hình xăm mặt quỷ  vì vậy mà cũng chứa đựng những ý nghĩa tích cực nhất định có thể bạn chưa biết.
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="image-info">
                            <div class="img-wrapper">
                                <div class="image-title reveal letter-animation image1">
                                    <ul class="layer-items4" data-relative-input="true" data-method="itemParallax4"
                                        style="transform: translate3d(0px, 0px, 0px) rotate(0.0001deg); transform-style: preserve-3d; backface-visibility: hidden; position: relative; pointer-events: none;">
                                        <li class="layer-3 layer" data-depth="0.15" style="transform: translate3d(13.1px, 153.5px, 0px); transform-style: preserve-3d; backface-visibility: hidden; position: relative; display: block; left: 0px; top: 0px;">
                                            <span style="color:#8c5620">Devil's Fairy</span>
                                        </li>
                                        <li class="layer" data-depth="0.30"
                                            style="transform: translate3d(26.1px, 307px, 0px); transform-style: preserve-3d; backface-visibility: hidden; position: absolute; display: block; left: 0px; top: 0px;">
                                            <div class="circle4 circle reveal fade-anim-zoom" data-delay="100">
                                                <img src="/img/trace2-1.png" alt="brush">
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                                <div class="img-content">
                                    <div class="reveal fade-anim-zoom" data-delay="900">
                                        <img width="777" height="957" src="/img/tatto14.jpg"
                                            class="story-image obj-fit-cover wp-post-image" alt="Devil's fairy"
                                            loading="lazy"
                                            srcset="/img/tatto14.jpg"
                                            sizes="(max-width: 777px) 100vw, 777px">
                                        <div class="dimension right-dimension">Kích thước<br>
                                            40x30cm<br>
                                        </div>
                                    </div>
                                    <div class="part-image">
                                        <a href="/img/tatto14.jpg"
                                            data-fancybox="gallery" class="no-barba reveal fade-anim-left"
                                            data-delay="300"><img src="/img/tatto14.jpg"
                                                alt="Devil's fairy"></a>
                                        <a href="/img/tatto12.jpg"
                                            data-fancybox="gallery" class="no-barba reveal fade-anim-left"
                                            data-delay="500"><img src="/img/tatto12.jpg"
                                                alt="Devil's fairy"></a>
                                        <a href="/img/tatto13.jpg"
                                            data-fancybox="gallery" class="no-barba reveal fade-anim-left"
                                            data-delay="700"><img src="/img/tatto13.jpg"
                                                alt="Devil's fairy"></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="center">
    <div class="title center">
        <a href="/" class="backto">
            <h2 class="reveal text-anim-bottom"><span>Quay lại những câu chuyện</span></h2>
        </a>
    </div>
</div>
