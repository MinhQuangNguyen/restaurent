@extends('frontend.layouts.iconic')

@section('title', __('Contact'))

@section('content')
@include('frontend.contact.includes.intro')
@include('frontend.contact.includes.contact')
@endsection
