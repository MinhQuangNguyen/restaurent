<section class="intro">
    <div class="smoke reveal fade-anim-default" data-delay="100"><img src="/img/white-smoke.png" alt="fire"></div>
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="row">
                    <div class="intro-image reveal fade-anim-zoom" data-delay="700">
                        <img width="1047" height="698" src="/img/angel-statue-marble-church.jpg" class="obj-fit-cover wp-post-image"
                            alt="Studio Minh Tatto at May Fine Art Studio Vienna" loading="lazy"
                            srcset="/img/angel-statue-marble-church.jpg"
                            sizes="(max-width: 1047px) 100vw, 1047px"> </div>
                    <div class="col-12 col-lg-7 intro-order no-padding">
                        <div class="intro-wrapper">
                            <div class="title">
                                <h1 class="reveal text-anim-bottom" data-delay="300"><span>Liên hệ với tôi</span></h1>
                            </div>
                            <div class="intro-text">
                                <div class="reveal text-anim-top" data-delay="500">
                                    <p>Studio Minh Tatto luôn sẵn sàng phục vụ các bạn yêu nghệ thuật cũng như các bạn muốn nhận một tác phẩm độc đáo mang tính độc quyền cho mỗi thiết kế hình xăm. Studio Minh Tatto tự hào là một trong những studio tatto hàng đầu tại Việt Nam theo phong cách Âu Mỹ. Chúng tôi rất vui khi được bạn liên hệ, Hứa hẹn sẽ mang lại cho các bạn một trải nghiệm tuyệt vời.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
