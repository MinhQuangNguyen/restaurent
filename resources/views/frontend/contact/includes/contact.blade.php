<div class="contact">
    <div class="container">
        <div class="row">
            <div class="col-12 col-lg-6">
                <div class="row">
                    <div class="col-12 col-lg-6 col-md-6 col-sm-6">
                        <div class="social-information">
                            <ul>
                                <li>
                                    <a href="" target="_blank">
                                        <svg class="whatsapp">
                                            <use xlink:href="#whatsapp"></use>
                                        </svg>
                                        whatsapp </a>
                                </li>
                                <li>
                                    <a href="" target="_blank">
                                        <svg class="facebook">
                                            <use xlink:href="#facebook"></use>
                                        </svg>
                                        facebook </a>
                                </li>
                                <li>
                                    <a href="" target="_blank">
                                        <svg class="instagram">
                                            <use xlink:href="#instagram"></use>
                                        </svg>
                                        instagram </a>
                                </li>
                                <li>
                                    <a href="" target="_blank">
                                        <svg class="youtube">
                                            <use xlink:href="#youtube"></use>
                                        </svg>
                                        youtube </a>
                                </li>
                            </ul>
                            <a href="https://www.google.com/maps/place/12+Khu%E1%BA%A5t+Duy+Ti%E1%BA%BFn,+Thanh+Xu%C3%A2n+B%E1%BA%AFc,+Thanh+Xu%C3%A2n,+H%C3%A0+N%E1%BB%99i,+Vi%E1%BB%87t+Nam/@20.9935323,105.8000186,17z/data=!3m1!4b1!4m5!3m4!1s0x3135acbfac608051:0x4d0dd9816456b506!8m2!3d20.9935273!4d105.8022073"
                                class="map-btn" target="_blank">Google Map</a>
                        </div>
                    </div>
                    <div class="col-12 col-lg-6 col-md-6 col-sm-6">
                        <div class="adress-info">
                            <p>
                                Tầng 10,<br>
                                Tòa Zentower,<br>
                                12 Khuất Duy Tiến,<br>
                                Thanh Xuân Trung,<br>
                                TP.Hà Nội<br></p>
                            <a href="mailto:hotro@mediaiconic.co">hotro@mediaiconic.com</a>
                            <a href="tel:0345736211">(+84).345.763.211</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-12 col-lg-6">
                <div class="contact-form">
                    <div role="form" class="wpcf7" id="wpcf7-f169-o1" lang="en-US" dir="ltr">
                        <div class="screen-reader-response">
                            <p role="status" aria-live="polite" aria-atomic="true"></p>
                            <ul></ul>
                        </div>
                        @if ($message = Session::get('success'))
                            <div class="alert alert-success alert-block">
                                    <strong>{{ $message }}</strong>
                            </div>
                        @endif
                        <x-forms.post :action="route('frontend.contact.store')">
                            <div style="display: none;">
                                <input type="hidden" name="_wpcf7" value="169">
                                <input type="hidden" name="_wpcf7_version" value="5.4">
                                <input type="hidden" name="_wpcf7_locale" value="en_US">
                                <input type="hidden" name="_wpcf7_unit_tag" value="wpcf7-f169-o1">
                                <input type="hidden" name="_wpcf7_container_post" value="0">
                                <input type="hidden" name="_wpcf7_posted_data_hash" value="">
                            </div>
                            <div class="row">
                                <div class="col-6 col-lg-6 col-md-6">
                                    <span class="wpcf7-form-control-wrap namesurname"><input type="text"
                                            name="name" value="" size="40"
                                            class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required"
                                            placeholder="Họ và tên" required></span>
                                </div>
                                <div class="col-6 col-lg-6 col-md-6">
                                    <span class="wpcf7-form-control-wrap phone"><input type="tel" name="phone" value=""
                                            size="40"
                                            class="wpcf7-form-control wpcf7-text wpcf7-tel wpcf7-validates-as-required wpcf7-validates-as-tel"
                                            placeholder="Số điện thoại" required></span>
                                </div>
                                <div class="col-12">
                                    <span class="wpcf7-form-control-wrap email"><input type="email" name="email"
                                            value="" size="40"
                                            class="wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email"
                                            placeholder="E-mail" required></span>
                                </div>
                                <div class="col-12">
                                    <span class="wpcf7-form-control-wrap your-msg"><textarea name="content" cols="40"
                                            rows="10"
                                            class="wpcf7-form-control wpcf7-textarea wpcf7-validates-as-required"
                                            placeholder="Lời nhắn" required></textarea></span>
                                </div>
                                <div class="col-12">
                                    <input type="submit" value="Gửi tin nhắn" class="wpcf7-form-control wpcf7-submit">
                                </div>
                            </div>
                            <div class="wpcf7-response-output" aria-hidden="true"></div>
                        </x-forms.post>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
