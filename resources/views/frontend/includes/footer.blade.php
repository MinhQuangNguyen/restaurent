<footer class="commision-padding">
    <div class="footer-bg"><img src="img/fire5.png" alt="fire"></div>
    <div class="footer">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="menu-row">
                        <div class="copyright">
                            <ul>
                                <li><span>© 2021 Studio Minh Tatto Bản quyền thuộc về Media Iconic</span></li>
                            </ul>
                        </div>
                        <div class="footer-menu">
                            <ul>
                                @if(isset($menu))
                                @foreach ($menu as $menuItem)
                                @if ($menuItem->hasChild())
                                <li id="menu-item-73"
                                    class="menu-item menu-item-type-post_type menu-item-object-page menu-item-73">
                                    <a href="{{$menuItem->clickable ? $menuItem->link: '#'}}">{{ $menuItem->name }}</a>
                                </li>
                                @else
                                <?php $link = strlen($menuItem->link) > 1 ? substr($menuItem->link, 1) : $menuItem->link ?>
                                <li id="menu-item-73"
                                    class="menu-item menu-item-type-post_type menu-item-object-page menu-item-73">
                                    <a href="{{ $menuItem->link }}">{{ $menuItem->name }}</a>
                                </li>
                                @endif
                                @endforeach
                                @endif
                            </ul>
                        </div>
                        <div class="redneck">
                            <a href="https://mediaiconic.com/" target="_blank">
                                Chế tác bởi <strong>Media Iconic</strong>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
