<svg xmlns="http://www.w3.org/2000/svg" style="display: none!important">
    <symbol id="facebook" viewBox="0 0 8 15">
        <style></style>
        <g id="LEFT MENU">
            <g id="SOCIAL">
                <path id="f" class="shp0"
                    d="M5.64 2.96H7V.76H5.33v.01C2.72.86 2.18 2.31 2.14 3.84V5.6H.81v2.2h1.33v7.04H4.8V7.8h2l.4-2.2H4.81V4.01c0-.51.35-1.05.84-1.05z">
                </path>
            </g>
        </g>
    </symbol>
    <symbol id="instagram" viewBox="0 0 16 17">
        <style></style>
        <g id="LEFT MENU">
            <g id="SOCIAL">
                <path id="Shape 3" class="shp0"
                    d="M11.06 16.24H4.94c-2.55 0-4.62-2.11-4.62-4.69V4.93C.32 2.34 2.39.24 4.94.24h6.12c2.55 0 4.62 2.1 4.62 4.69v6.62c0 2.58-2.07 4.69-4.62 4.69zm2.95-11.31c0-1.65-1.33-3-2.95-3H4.94c-1.63 0-2.95 1.35-2.95 3v6.62c0 1.65 1.32 2.99 2.95 2.99h6.12c1.62 0 2.95-1.34 2.95-2.99V4.93zm-1.87.03A.95.95 0 0 1 11.2 4c0-.53.42-.96.94-.96s.94.43.94.96-.42.96-.94.96zm.24 3.28a4.56 4.56 0 0 1-1.29 3.12c-.83.84-1.92 1.3-3.09 1.3-2.42 0-4.38-1.99-4.38-4.44 0-1.21.49-2.39 1.34-3.23.83-.82 1.92-1.25 3.07-1.22 1.35.04 2.48.51 3.26 1.36.74.8 1.12 1.88 1.09 3.11zm-4.41-2.4h-.06c-.57 0-1.11.23-1.53.64a2.45 2.45 0 0 0-.72 1.74c0 1.31 1.05 2.37 2.34 2.37 1.28 0 2.31-1.05 2.34-2.4.03-1.45-.83-2.3-2.37-2.35z">
                </path>
            </g>
        </g>
    </symbol>
    <symbol id="whatsapp" viewBox="0 0 448 448">
        <style></style>
        <path id="Layer" fill-rule="evenodd" class="shp0"
            d="M380.9 65.1c41.9 42 67.1 97.7 67.1 157 0 122.4-101.8 222-224.1 222h-.1c-37.2 0-73.7-9.3-106.1-27L0 448l31.5-115C12.1 299.3 1.9 261.1 1.9 222c0-122.4 99.6-222 222-222 59.3 0 115.1 23.1 157 65.1zm-157 341.6c101.7 0 186.6-82.8 186.6-184.6.1-49.3-21.3-95.6-56.1-130.5-34.8-34.9-81.1-54.1-130.4-54.1-101.8 0-184.6 82.8-184.6 184.5 0 34.9 9.7 68.8 28.2 98.2l4.4 7-18.6 68.1 69.8-18.3 6.7 4c28.3 16.8 60.8 25.7 94 25.7zm111.7-131.6c1.4 2.3 1.4 13.4-3.2 26.4-4.6 13-26.7 24.8-37.4 26.4-17.6 2.6-31.4 1.3-66.6-13.9-55.7-24.1-92-80.1-94.8-83.8-2.7-3.7-22.6-30.1-22.6-57.4 0-27.3 14.3-40.7 19.4-46.3 5.1-5.5 11.1-6.9 14.8-6.9 3.7 0 7.4 0 10.6.2 3.4.2 8-1.3 12.5 9.5 4.6 11.1 15.7 38.4 17.1 41.2 1.4 2.8 2.3 6 .5 9.7-10.6 21.2-22 20.5-16.3 30.3 21.5 36.9 42.9 49.7 75.5 66 5.5 2.8 8.8 2.3 12-1.4 3.3-3.8 13.9-16.2 17.6-21.8 3.7-5.6 7.4-4.7 12.5-2.8 5.1 1.8 32.4 15.2 37.9 18 5.5 2.7 9.2 4.1 10.5 6.6z">
        </path>
    </symbol>
    <symbol id="youtube" viewBox="0 0 576 512">
        <path
            d="M549.655 124.083c-6.281-23.65-24.787-42.276-48.284-48.597C458.781 64 288 64 288 64S117.22 64 74.629 75.486c-23.497 6.322-42.003 24.947-48.284 48.597-11.412 42.867-11.412 132.305-11.412 132.305s0 89.438 11.412 132.305c6.281 23.65 24.787 41.5 48.284 47.821C117.22 448 288 448 288 448s170.78 0 213.371-11.486c23.497-6.321 42.003-24.171 48.284-47.821 11.412-42.867 11.412-132.305 11.412-132.305s0-89.438-11.412-132.305zm-317.51 213.508V175.185l142.739 81.205-142.739 81.201z">
        </path>
    </symbol>
</svg>

<div class="menu" id="menu" onclick="handleMenu()">
    <div class="nav-icon">
        <span></span>
        <span></span>
        <span></span>
    </div>
</div>

<div class="mobile-menu">
    <div class="main-menu">
        <ul>
            @if(isset($menu))
              @foreach ($menu as $menuItem)
                  @if ($menuItem->hasChild())
                    <li>
                        <a href="{{$menuItem->clickable ? $menuItem->link: '#'}}" class="{{ request()->is((strlen($menuItem->link) > 1 ? substr($menuItem->link, 1) : $menuItem->link)) || request()->is((strlen($menuItem->link) > 1 ? substr($menuItem->link, 1) : $menuItem->link)."/*") ? 'active': ''}}" onclick="handleMainMenu()">{{ $menuItem->name }}</a>
                    </li>
                  @else
                  <?php $link = strlen($menuItem->link) > 1 ? substr($menuItem->link, 1) : $menuItem->link ?>
                    <li>
                      <a href="{{ $menuItem->link }}" class="{{ request()->is($link) || request()->is($link.'/*') ? 'active': '' }}" onclick="handleMainMenu()">{{ $menuItem->name }}</a>
                    </li>
                  @endif
                @endforeach
            @endif
        </ul>
    </div>
</div>

<div class="navigation">
    <div class="logo">
        <a href="/">
            <img src="img/logo.png" alt="May Portraits">
        </a>
    </div>
    <div class="social">
        <ul>
            <li>
                <a href="" target="_blank">
                    <svg class="whatsapp">
                        <use xlink:href="#whatsapp"></use>
                    </svg>
                </a>
            </li>
            <li>
                <a href="" target="_blank">
                    <svg class="facebook">
                        <use xlink:href="#facebook"></use>
                    </svg>
                </a>
            </li>
            <li>
                <a href="" target="_blank">
                    <svg class="instagram">
                        <use xlink:href="#instagram"></use>
                    </svg>
                </a>
            </li>
            <li>
                <a href="" target="_blank">
                    <svg class="youtube">
                        <use xlink:href="#youtube"></use>
                    </svg>
                </a>
            </li>
        </ul>
    </div>
</div>
