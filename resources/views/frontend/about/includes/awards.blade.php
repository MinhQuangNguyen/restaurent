<section class="awards">
    <div class="container">
        <div class="center">
            <div class="award-title">
                <div class="fire3">
                    <div class="reveal fade-anim-zoom"><img
                           src="img/firebio.png" alt="fire"></div>
                </div>
                <h2 class="reveal text-anim-bottom" data-title="Studio Minh Tatto"><span>Giải thưởng & Thành tích</span></h2>
            </div>
        </div>
        <div class="award">
            <div class="swiper-container swiper-container-initialized swiper-container-horizontal"
                data-method="awardslider">
                <div class="slide-line"></div>
                <div class="swiper-wrapper">
                    <div class="swiper-slide swiper-slide-visible swiper-slide-active"
                        style="width: 263.5px; margin-right: 15px;">
                        <div class="year">2021 </div>
                        <div class="slider-dots">
                            <div class="slider-line"></div>
                        </div>
                        <div class="slider-text">
                            <h3><strong>Cuộc thi Salon ARC lần thứ 14</strong></h3>
                            <p>Lọt vào chung kết, hạng mục tượng hình</p>
                        </div>
                    </div>
                    <div class="swiper-slide swiper-slide-visible swiper-slide-next"
                        style="width: 263.5px; margin-right: 15px;">
                        <div class="year">2013 </div>
                        <div class="slider-dots">
                            <div class="slider-line"></div>
                        </div>
                        <div class="slider-text">
                            <h3><strong>Giải thưởng nổi bật, Strokes of Genius 5: Sách sáng tác</strong>
                            </h3>
                            <p>Sách North Light</p>
                        </div>
                    </div>
                    <div class="swiper-slide swiper-slide-visible"
                        style="width: 263.5px; margin-right: 15px;">
                        <div class="year">2012 </div>
                        <div class="slider-dots">
                            <div class="slider-line"></div>
                        </div>
                        <div class="slider-text">
                            <h3><strong>Giải thưởng công nhận đặc biệt</strong></h3>
                            <p>Tổ chức cuộc thi nghệ thuật quốc tế Artrom</p>
                        </div>
                    </div>
                    <div class="swiper-slide swiper-slide-visible"
                        style="width: 263.5px; margin-right: 15px;">
                        <div class="year">2012 </div>
                        <div class="slider-dots">
                            <div class="slider-line"></div>
                        </div>
                        <div class="slider-text">
                            <h3><strong>Giải thưởng tính năng</strong></h3>
                            <p>Tạp chí hàng quý Still Point Arts, số ra mùa hè</p>
                        </div>
                    </div>
                    <div class="swiper-slide" style="width: 263.5px; margin-right: 15px;">
                        <div class="year">2012 </div>
                        <div class="slider-dots">
                            <div class="slider-line"></div>
                        </div>
                        <div class="slider-text">
                            <h3><strong>Đặc tính</strong></h3>
                            <p>Tạp chí Bluecanvas</p>
                        </div>
                    </div>
                    <div class="swiper-slide" style="width: 263.5px; margin-right: 15px;">
                        <div class="year">2012 </div>
                        <div class="slider-dots">
                            <div class="slider-line"></div>
                        </div>
                        <div class="slider-text">
                            <h3><strong>Người lọt vào chung kết, Hạng mục Tranh, Giải Nghệ thuật Con lừa</strong></h3>
                            <p>Dự án Blindonkey</p>
                        </div>
                    </div>
                    <div class="swiper-slide" style="width: 263.5px; margin-right: 15px;">
                        <div class="year">2012 </div>
                        <div class="slider-dots">
                            <div class="slider-line"></div>
                        </div>
                        <div class="slider-text">
                            <h3><strong>Giải thưởng chân dung</strong></h3>
                            <p>Thư viện trực tuyến Barebrush</p>
                        </div>
                    </div>
                    <div class="swiper-slide" style="width: 263.5px; margin-right: 15px;">
                        <div class="year">2012 </div>
                        <div class="slider-dots">
                            <div class="slider-line"></div>
                        </div>
                        <div class="slider-text">
                            <h3><strong>Đề cử giải thưởng do người quản lý lựa chọn, chân dung của tháng</strong></h3>
                            <p>Thư viện trực tuyến Barebrush</p>
                        </div>
                    </div>
                    <div class="swiper-slide" style="width: 263.5px; margin-right: 15px;">
                        <div class="year">2012 </div>
                        <div class="slider-dots">
                            <div class="slider-line"></div>
                        </div>
                        <div class="slider-text">
                            <h3><strong>Giải thưởng tính năng</strong></h3>
                            <p>Tạp chí Đánh giá Thung lũng Pomona</p>
                        </div>
                    </div>
                    <div class="swiper-slide" style="width: 263.5px; margin-right: 15px;">
                        <div class="year">2012 </div>
                        <div class="slider-dots">
                            <div class="slider-line"></div>
                        </div>
                        <div class="slider-text">
                            <h3><strong>Nơi đầu tiên, Cô đơn</strong></h3>
                            <p>Một cuộc thi sáng tạo số ít</p>
                        </div>
                    </div>
                    <div class="swiper-slide" style="width: 263.5px; margin-right: 15px;">
                        <div class="year">2012 </div>
                        <div class="slider-dots">
                            <div class="slider-line"></div>
                        </div>
                        <div class="slider-text">
                            <h3><strong>Top 300</strong></h3>
                            <p>Giải thưởng chân dung BP</p>
                        </div>
                    </div>
                    <div class="swiper-slide" style="width: 263.5px; margin-right: 15px;">
                        <div class="year">2012 </div>
                        <div class="slider-dots">
                            <div class="slider-line"></div>
                        </div>
                        <div class="slider-text">
                            <h3><strong>Tính năng, Nghệ thuật nổi bật</strong></h3>
                            <p>mediaiconic.com</p>
                        </div>
                    </div>
                    <div class="swiper-slide" style="width: 263.5px; margin-right: 15px;">
                        <div class="year">2012 </div>
                        <div class="slider-dots">
                            <div class="slider-line"></div>
                        </div>
                        <div class="slider-text">
                            <h3><strong>Giải thưởng cho tính năng, cuốn sách Nhận dạng hình khuôn mặt</strong></h3>
                            <p>Sprocketbox Entertainment</p>
                        </div>
                    </div>
                    <div class="swiper-slide" style="width: 263.5px; margin-right: 15px;">
                        <div class="year">2012 </div>
                        <div class="slider-dots">
                            <div class="slider-line"></div>
                        </div>
                        <div class="slider-text">
                            <h3><strong>Tính năng</strong></h3>
                            <p>Tạp chí Art of England</p>
                        </div>
                    </div>
                    <div class="swiper-slide" style="width: 263.5px; margin-right: 15px;">
                        <div class="year">2012 </div>
                        <div class="slider-dots">
                            <div class="slider-line"></div>
                        </div>
                        <div class="slider-text">
                            <h3><strong>Tính năng, Nhóm nghệ thuật tượng hình</strong></h3>
                            <p>Tạp chí nghệ thuật trực tuyến Artreview</p>
                        </div>
                    </div>
                    <div class="swiper-slide" style="width: 263.5px; margin-right: 15px;">
                        <div class="year">2012 </div>
                        <div class="slider-dots">
                            <div class="slider-line"></div>
                        </div>
                        <div class="slider-text">
                            <h3><strong>Lọt vào chung kết, Mọi người</strong></h3>
                            <p>Một cuộc thi sáng tạo số ít</p>
                        </div>
                    </div>
                    <div class="swiper-slide" style="width: 263.5px; margin-right: 15px;">
                        <div class="year">2012 </div>
                        <div class="slider-dots">
                            <div class="slider-line"></div>
                        </div>
                        <div class="slider-text">
                            <h3><strong>Tính năng</strong></h3>
                            <p>Bản tin Nitram Charcoal</p>
                        </div>
                    </div>
                    <div class="swiper-slide" style="width: 263.5px; margin-right: 15px;">
                        <div class="year">2012 </div>
                        <div class="slider-dots">
                            <div class="slider-line"></div>
                        </div>
                        <div class="slider-text">
                            <h3><strong>Giải thưởng cho việc Quảng bá Hạt Sisak-Moslavina thông qua nghệ thuật</strong>
                            </h3>
                            <p>Hạt Sisak-Moslavina, Croatia</p>
                        </div>
                    </div>
                    <div class="swiper-slide" style="width: 263.5px; margin-right: 15px;">
                        <div class="year">2012 </div>
                        <div class="slider-dots">
                            <div class="slider-line"></div>
                        </div>
                        <div class="slider-text">
                            <h3><strong>Tính năng trang chủ</strong></h3>
                            <p>mediaiconic.com</p>
                        </div>
                    </div>
                    <div class="swiper-slide" style="width: 263.5px; margin-right: 15px;">
                        <div class="year">2011 </div>
                        <div class="slider-dots">
                            <div class="slider-line"></div>
                        </div>
                        <div class="slider-text">
                            <h3><strong>Đen và trắng</strong></h3>
                            <p>Một cuộc thi sáng tạo số ít</p>
                        </div>
                    </div>
                    <div class="swiper-slide" style="width: 263.5px; margin-right: 15px;">
                        <div class="year">2011 </div>
                        <div class="slider-dots">
                            <div class="slider-line"></div>
                        </div>
                        <div class="slider-text">
                            <h3><strong>Tính năng</strong></h3>
                            <p>Bản tin Bluecanvas</p>
                        </div>
                    </div>
                    <div class="swiper-slide" style="width: 263.5px; margin-right: 15px;">
                        <div class="year">2011 </div>
                        <div class="slider-dots">
                            <div class="slider-line"></div>
                        </div>
                        <div class="slider-text">
                            <h3><strong>Tính năng, Tin tức về cựu sinh viên</strong></h3>
                            <p>Blog của Học viện Nghệ thuật Thiên thần</p>
                        </div>
                    </div>
                    <div class="swiper-slide" style="width: 263.5px; margin-right: 15px;">
                        <div class="year">2011 </div>
                        <div class="slider-dots">
                            <div class="slider-line"></div>
                        </div>
                        <div class="slider-text">
                            <h3><strong>Lọt vào chung kết, Hạng mục Vẽ</strong></h3>
                            <p>Salon Trung tâm Đổi mới Nghệ thuật Quốc tế 2010/2011</p>
                        </div>
                    </div>
                    <div class="swiper-slide" style="width: 263.5px; margin-right: 15px;">
                        <div class="year">2011 </div>
                        <div class="slider-dots">
                            <div class="slider-line"></div>
                        </div>
                        <div class="slider-text">
                            <h3><strong>Đặc tính</strong></h3>
                            <p>Tạp chí Sống sang trọng</p>
                        </div>
                    </div>
                    <div class="swiper-slide" style="width: 263.5px; margin-right: 15px;">
                        <div class="year">2011 </div>
                        <div class="slider-dots">
                            <div class="slider-line"></div>
                        </div>
                        <div class="slider-text">
                            <h3><strong>Tính năng, Tin tức về cựu sinh viên</strong></h3>
                            <p>Blog của Học viện Nghệ thuật Thiên thần</p>
                        </div>
                    </div>
                    <div class="swiper-slide" style="width: 263.5px; margin-right: 15px;">
                        <div class="year">2010 </div>
                        <div class="slider-dots">
                            <div class="slider-line"></div>
                        </div>
                        <div class="slider-text">
                            <h3><strong>Studio Minh Tatto, Forgotten Beauty book</strong></h3>
                            <p>Bảo tàng thành phố Sisak</p>
                        </div>
                    </div>
                    <div class="swiper-slide" style="width: 263.5px; margin-right: 15px;">
                        <div class="year">2010 </div>
                        <div class="slider-dots">
                            <div class="slider-line"></div>
                        </div>
                        <div class="slider-text">
                            <h3><strong>Đặc tính</strong></h3>
                            <p>Tạp chí Playboy</p>
                        </div>
                    </div>
                    <div class="swiper-slide" style="width: 263.5px; margin-right: 15px;">
                        <div class="year">2010 </div>
                        <div class="slider-dots">
                            <div class="slider-line"></div>
                        </div>
                        <div class="slider-text">
                            <h3><strong>Lọt vào chung kết, Hạng mục Vẽ</strong></h3>
                            <p>Salon Trung tâm Đổi mới Nghệ thuật Quốc tế 2009/2010</p>
                        </div>
                    </div>
                    <div class="swiper-slide" style="width: 263.5px; margin-right: 15px;">
                        <div class="year">2010 </div>
                        <div class="slider-dots">
                            <div class="slider-line"></div>
                        </div>
                        <div class="slider-text">
                            <h3><strong>Đặc tính</strong></h3>
                            <p>Tạp chí Phong cách sống</p>
                        </div>
                    </div>
                    <div class="swiper-slide" style="width: 263.5px; margin-right: 15px;">
                        <div class="year">2010 </div>
                        <div class="slider-dots">
                            <div class="slider-line"></div>
                        </div>
                        <div class="slider-text">
                            <h3><strong>Đặc tính</strong></h3>
                            <p>Tạp chí nghệ thuật đường viền</p>
                        </div>
                    </div>
                    <div class="swiper-slide" style="width: 263.5px; margin-right: 15px;">
                        <div class="year">2010 </div>
                        <div class="slider-dots">
                            <div class="slider-line"></div>
                        </div>
                        <div class="slider-text">
                            <h3><strong>Đặc tính</strong></h3>
                            <p>Báo Sáng Chủ Nhật</p>
                        </div>
                    </div>
                    <div class="swiper-slide" style="width: 263.5px; margin-right: 15px;">
                        <div class="year">2008 </div>
                        <div class="slider-dots">
                            <div class="slider-line"></div>
                        </div>
                        <div class="slider-text">
                            <h3><strong>Đặc tính</strong></h3>
                            <p>chí Nghệ thuật Kinh doanh Ảnh</p>
                        </div>
                    </div>
                    <div class="swiper-slide" style="width: 263.5px; margin-right: 15px;">
                        <div class="year">2008 </div>
                        <div class="slider-dots">
                            <div class="slider-line"></div>
                        </div>
                        <div class="slider-text">
                            <h3><strong>Trang chủ Đặc tính</strong></h3>
                            <p>Trang web Học viện Nghệ thuật Thiên thần</p>
                        </div>
                    </div>
                    <div class="swiper-slide" style="width: 263.5px; margin-right: 15px;">
                        <div class="year">2006 </div>
                        <div class="slider-dots">
                            <div class="slider-line"></div>
                        </div>
                        <div class="slider-text">
                            <h3><strong>Lọt vào chung kết, hạng mục Tĩnh vật</strong></h3>
                            <p>Tiệm Trung tâm Đổi mới Nghệ thuật Quốc tế 2005/2006</p>
                        </div>
                    </div>
                    <div class="swiper-slide" style="width: 263.5px; margin-right: 15px;">
                        <div class="year">2005 </div>
                        <div class="slider-dots">
                            <div class="slider-line"></div>
                        </div>
                        <div class="slider-text">
                            <h3><strong>Đề cập đến Danh dự</strong></h3>
                            <p>Tiệm trung tâm đổi mới nghệ thuật quốc tế 2004/2005</p>
                        </div>
                    </div>
                    <div class="swiper-slide" style="width: 263.5px; margin-right: 15px;">
                        <div class="year">2004 </div>
                        <div class="slider-dots">
                            <div class="slider-line"></div>
                        </div>
                        <div class="slider-text">
                            <h3><strong>Vị trí thứ nhất, Cuộc thi ARC Scolarships</strong></h3>
                            <p>Trung tâm đổi mới nghệ thuật</p>
                        </div>
                    </div>
                    <div class="swiper-slide" style="width: 263.5px; margin-right: 15px;">
                        <div class="year">2004 </div>
                        <div class="slider-dots">
                            <div class="slider-line"></div>
                        </div>
                        <div class="slider-text">
                            <h3><strong>Giải thưởng Mua hàng, Cuộc thi Scolarship ARC</strong></h3>
                            <p>Trung tâm đổi mới nghệ thuật</p>
                        </div>
                    </div>
                </div>
                <span class="swiper-notification" aria-live="assertive" aria-atomic="true"></span>
            </div>
            <div class="left-arrow swiper-button-disabled" tabindex="0" role="button"
                aria-label="Previous slide" aria-disabled="true"><a
                    href=""><img
                       src="img/left-arrow.png" alt="arrow"></a>
            </div>
            <div class="right-arrow" tabindex="0" role="button" aria-label="Next slide"
                aria-disabled="false"><a href=""><img
                       src="img/right-arrow.png" alt="arrow"></a>
            </div>
        </div>
    </div>
</section>
