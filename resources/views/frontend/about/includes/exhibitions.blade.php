<section class="exhibitions">
    <div class="container">
        <div class="center">
            <div class="award-title">
                <div class="fire3">
                    <div class="reveal fade-anim-zoom"><img
                           src="img/firebio2.png" alt="fire"></div>
                </div>
                <h2 class="reveal text-anim-bottom" data-title="Studio Minh Tatto">
                    <span>Triển lãm</span></h2>
            </div>
        </div>
        <div class="exhibition">
            <div class="swiper-container swiper-container-initialized swiper-container-horizontal"
                data-method="exhibitionslider">
                <div class="slide-line"></div>
                <div class="swiper-wrapper">
                    <div class="swiper-slide swiper-slide-visible swiper-slide-active"
                        style="width: 263.5px; margin-right: 15px;">
                        <div class="year">2021</div>
                        <div class="slider-dots">
                            <div class="slider-line"></div>
                        </div>
                        <div class="slider-text">
                            <h3><strong>Studio Minh tatto</strong></h3>
                            <p>Khai mạc Triển lãm</p>
                        </div>
                    </div>
                    <div class="swiper-slide swiper-slide-visible swiper-slide-next"
                        style="width: 263.5px; margin-right: 15px;">
                        <div class="year">2017</div>
                        <div class="slider-dots">
                            <div class="slider-line"></div>
                        </div>
                        <div class="slider-text">
                            <h3><strong>Điều 18, Wien</strong></h3>
                            <p>Kunst Fest Währing</p>
                        </div>
                    </div>
                    <div class="swiper-slide swiper-slide-visible"
                        style="width: 263.5px; margin-right: 15px;">
                        <div class="year">2016</div>
                        <div class="slider-dots">
                            <div class="slider-line"></div>
                        </div>
                        <div class="slider-text">
                            <h3><strong>Hội chợ mùa xuân Birmingham</strong></h3>
                            <p>Với nghệ thuật Buckingham</p>
                        </div>
                    </div>
                    <div class="swiper-slide swiper-slide-visible"
                        style="width: 263.5px; margin-right: 15px;">
                        <div class="year">2015</div>
                        <div class="slider-dots">
                            <div class="slider-line"></div>
                        </div>
                        <div class="slider-text">
                            <h3><strong>Studio Minh tatto</strong></h3>
                            <p>Khai mạc Triển lãm</p>
                        </div>
                    </div>
                    <div class="swiper-slide" style="width: 263.5px; margin-right: 15px;">
                        <div class="year">2012</div>
                        <div class="slider-dots">
                            <div class="slider-line"></div>
                        </div>
                        <div class="slider-text">
                            <h3><strong>Khai mạc Triển lãm</strong></h3>
                            <p>Triển lãm Still Point IV On Line Juried</p>
                        </div>
                    </div>
                    <div class="swiper-slide" style="width: 263.5px; margin-right: 15px;">
                        <div class="year">2012</div>
                        <div class="slider-dots">
                            <div class="slider-line"></div>
                        </div>
                        <div class="slider-text">
                            <h3><strong>Phòng trưng bày Modern Eden, San Francisco</strong></h3>
                            <p>Triển lãm khét tiếng Juried</p>
                        </div>
                    </div>
                    <div class="swiper-slide" style="width: 263.5px; margin-right: 15px;">
                        <div class="year">2012</div>
                        <div class="slider-dots">
                            <div class="slider-line"></div>
                        </div>
                        <div class="slider-text">
                            <h3><strong>Phòng trưng bày nghệ thuật Barebrush</strong></h3>
                            <p>Ảnh chân dung của tháng có thể triển lãm trực tuyến</p>
                        </div>
                    </div>
                    <div class="swiper-slide" style="width: 263.5px; margin-right: 15px;">
                        <div class="year">2012</div>
                        <div class="slider-dots">
                            <div class="slider-line"></div>
                        </div>
                        <div class="slider-text">
                            <h3><strong>Phòng trưng bày Striegl, Sisak</strong></h3>
                            <p>Triển lãm Sacred Art Juried</p>
                        </div>
                    </div>
                    <div class="swiper-slide" style="width: 263.5px; margin-right: 15px;">
                        <div class="year">2012</div>
                        <div class="slider-dots">
                            <div class="slider-line"></div>
                        </div>
                        <div class="slider-text">
                            <h3><strong>Phòng trưng bày W.H.Patterson, London</strong></h3>
                            <p>Triển lãm nhóm</p>
                        </div>
                    </div>
                    <div class="swiper-slide" style="width: 263.5px; margin-right: 15px;">
                        <div class="year">2012</div>
                        <div class="slider-dots">
                            <div class="slider-line"></div>
                        </div>
                        <div class="slider-text">
                            <h3><strong>AAF Battersea, London</strong></h3>
                            <p>Với Phòng trưng bày W.H.Patterson</p>
                        </div>
                    </div>
                    <div class="swiper-slide" style="width: 263.5px; margin-right: 15px;">
                        <div class="year">2012</div>
                        <div class="slider-dots">
                            <div class="slider-line"></div>
                        </div>
                        <div class="slider-text">
                            <h3><strong>Bảo tàng nghệ thuật Georgia</strong></h3>
                            <p>Triển lãm Kress Project On Line</p>
                        </div>
                    </div>
                    <div class="swiper-slide" style="width: 263.5px; margin-right: 15px;">
                        <div class="year">2011</div>
                        <div class="slider-dots">
                            <div class="slider-line"></div>
                        </div>
                        <div class="slider-text">
                            <h3><strong>Phòng trưng bày W.H.Patterson, London</strong></h3>
                            <p>Triển lãm Cá nhân</p>
                        </div>
                    </div>
                    <div class="swiper-slide" style="width: 263.5px; margin-right: 15px;">
                        <div class="year">2011</div>
                        <div class="slider-dots">
                            <div class="slider-line"></div>
                        </div>
                        <div class="slider-text">
                            <h3><strong>Art Chicago, Chicago</strong></h3>
                            <p>Với Phòng trưng bày W.H.Patterson</p>
                        </div>
                    </div>
                    <div class="swiper-slide" style="width: 263.5px; margin-right: 15px;">
                        <div class="year">2011</div>
                        <div class="slider-dots">
                            <div class="slider-line"></div>
                        </div>
                        <div class="slider-text">
                            <h3><strong>AAF Battersea, London</strong></h3>
                            <p>Với Phòng trưng bày W.H.Patterson</p>
                        </div>
                    </div>
                    <div class="swiper-slide" style="width: 263.5px; margin-right: 15px;">
                        <div class="year">2011</div>
                        <div class="slider-dots">
                            <div class="slider-line"></div>
                        </div>
                        <div class="slider-text">
                            <h3><strong>Bảo tàng thành phố Sisak, Sisak</strong></h3>
                            <p>Triển lãm Cá nhân</p>
                        </div>
                    </div>
                    <div class="swiper-slide" style="width: 263.5px; margin-right: 15px;">
                        <div class="year">2011</div>
                        <div class="slider-dots">
                            <div class="slider-line"></div>
                        </div>
                        <div class="slider-text">
                            <h3><strong>AAF Hampstead, Luân Đôn</strong></h3>
                            <p>Với Phòng trưng bày W.H.Patterson</p>
                        </div>
                    </div>
                </div>
                <span class="swiper-notification" aria-live="assertive" aria-atomic="true"></span>
            </div>
            <div class="left-arrow swiper-button-disabled" tabindex="0" role="button"
                aria-label="Previous slide" aria-disabled="true"><a
                    href=""><img
                       src="img/left-arrow.png" alt="arrow"></a>
            </div>
            <div class="right-arrow" tabindex="0" role="button" aria-label="Next slide"
                aria-disabled="false"><a href=""><img
                       src="img/right-arrow.png" alt="arrow"></a>
            </div>
        </div>
    </div>
</section>
