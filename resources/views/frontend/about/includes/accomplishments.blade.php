<section class="accomplishments">
    <div class="container">
        <div class="row">
            <div class="col-12 col-lg-5 full">
                <div class="books">
                    <h2>Sách đã xuất bản</h2>
                    <div class="book-colection">
                        <div class="book">
                            <h3>Sách Tattoo Art, 2021</h3>
                            <p>Nhà xuất bản Parragon Book Service Ltd</p>
                            <div class="book-line"></div>
                        </div>
                        <div class="book">
                            <h3>Trăm Năm Hình Xăm, 2019</h3>
                            <p>Nhà xuất bản Parragon Book Service Ltd</p>
                            <div class="book-line"></div>
                        </div>
                        <div class="book">
                            <h3>Sách - Hình Xăm, 2020</h3>
                            <p>NXB Hà Nội</p>
                            <div class="book-line"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-12 col-lg-7 full">
                <div class="art-collections">
                    <h2>Bộ sưu tập nghệ thuật</h2>
                    <div class="art-collection">
                        <div class="art-content">
                            <h3>Tác phẩm của Studio Minh Tatto có thể được tìm thấy trong các bộ sưu tập trên khắp thế giới như:</h3>
                            <ul>
                                <li>Thiên thần sa ngã của<strong>Sweden</strong></li>
                                <li>Bảy tôi danh chết người của<strong>Nhật Bản</strong></li>
                                <li>Tác phẩm nghệ thuật chân dung tại <strong>Trung tâm Đổi mới Nghệ thuật</strong></li>
                                <li>Học viện Nghệ thuật Thiên thần, <strong> Sự tôn kính</strong></li>
                                <li>Nhà xuất bản Mỹ thuật Buckingham, <strong>UK</strong></li>
                                <li>Bóng đêm tả thực của <strong> Nguyen Quang Minh</strong></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

