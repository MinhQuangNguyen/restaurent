@extends('frontend.layouts.iconic')

@section('title', __('Tiều sử'))
@section('content')
    @include('frontend.home.includes.intro')
    @include('frontend.about.includes.awards')
    @include('frontend.about.includes.accomplishments')
    @include('frontend.about.includes.exhibitions')
@endsection
