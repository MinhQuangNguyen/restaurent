<style>
    
    @font-face {
      font-family: 'Montserrat';
      font-style: normal;
      font-display: swap;
      font-weight: 600;
      src: url('/fonts/Montserrat/Montserrat-SemiBold.ttf')  format('truetype');
    }

    @font-face {
      font-family: 'Montserrat';
      font-style: normal;
      font-display: swap;
      font-weight: 800;
      src: url('/fonts/Montserrat/Montserrat-ExtraBold.ttf')  format('truetype');
    }

    body{
        padding:0;
        margin:0;
    }

    .item-backround-404{
        position: relative;
        width: 100%;
        background-repeat: no-repeat;
        margin: 0 auto;
        background-size: cover;
        height: 100%;
        background-position: center;
    }

    .title{
         font: normal normal bold 42px/34px Montserrat;
        letter-spacing: 0.95px;
        text-transform: uppercase;
        margin-bottom: 10px;
        display: block;
        padding-top: 7%;
        color: white;
        padding-left: 56px;
        text-align: center;
    }

    .container-fluid{
        padding: 0 !important;
    }


    .btn-back-home-404-mqn{
        font: normal normal 600 28px/34px Montserrat;
        text-decoration: none;
        text-transform: uppercase;
        color: white;
        text-shadow: 0px 3px 6px black;
        background: transparent;
        padding: 25px 100px;
        border: 2px solid white;
        box-shadow: 0px 3px 6px black;
        border-radius: 50px;
        position: absolute;
        bottom: 30%;
        left: 24%;
    }
    
    .title-404{
        color: white;
        text-align: center;
        position: relative;
        letter-spacing: 100;
        top: 250px;
        font-size: 260px;
        font-weight: 600;
        font-family: 'Montserrat', sans-serif;
        margin-left: 60px;
        text-shadow: -10px 3px 6px black;
    }
    
    .title-not-found{
        font: normal normal 800 80px/34px Montserrat;
        color: white;
        text-align: center;
        position: relative;
        text-transform: uppercase;
        letter-spacing: 10px;
        top: 300px;
        margin-left: 25px;
        text-shadow: 10px 3px 6px black;
    }

@media (min-width: 768px) {

}

@media (min-width: 992px) {
    .item-backround-404{
        position: relative;
        width: 100%;
        background-repeat: no-repeat;
        margin: 0 auto;
        background-size: cover;
        height: 100%;
        background-position: 0px 50%;
    }
    
     .btn-back-home-404-mqn{
        font: normal normal 600 18px/34px Montserrat;
        text-decoration: none;
        text-transform: uppercase;
        color: white;
        text-shadow: 0px 3px 6px black;
        background: transparent;
        padding: 10px 60px;
        border: 2px solid white;
        box-shadow: 0px 3px 6px black;
        border-radius: 50px;
        position: absolute;
        bottom: 8%;
        left: 40%;
    }
    
     .title-404{
        font: normal normal 600 400px/34px Montserrat;
        color: white;
        text-align: center;
        position: relative;
        letter-spacing: 100;
        top: 180px;
        margin-left: 100px;
        text-shadow: -10px 3px 6px black;
    }
    
       .title-not-found{
        font: normal normal 800 70px/34px Montserrat;
        color: white;
        text-align: center;
        position: relative;
        letter-spacing: 30;
        top: 400px;
        margin-left: 75px;
        text-shadow: 10px 3px 6px black;
    }
   
}

@media (min-width: 1024px) {

}

@media (max-width: 1366px) {
   
}

@media (min-width: 1600px) {
}


</style>

<div class="item-backround-404" style="background-image: url('/img/wallpaperflare.com_wallpaper.jpg');">
    <div class="container-fluid">
        <div class="row">
           <div class="col-12">
                <div class="title-404">404</div>
                <div class="title-not-found">page not found</div>
                <a href="/" class="btn-back-home-404-mqn">
                    Quay lại trang chủ
                 </a>
         </div>
    </div>
</div>