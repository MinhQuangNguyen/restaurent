@inject('model', '\App\Models\Recruitment')

@extends('backend.layouts.app')

@section('title', __('Create Recruitment'))

@section('content')
    <x-forms.post :action="route('admin.recruitment.store')">
        <x-backend.card>
            <x-slot name="header">
                @lang('Create Recruitment')
            </x-slot>

            <x-slot name="headerActions">
                <x-utils.link class="card-header-action" :href="route('admin.recruitment.index')" :text="__('Back')" />
            </x-slot>

            <x-slot name="body">
                <div>
                    <div class="form-group row">
                        <label for="title" class="col-md-2 col-form-label">@lang('Title')</label>

                        <div class="col-md-10">
                            <input type="text" name="title" class="form-control" placeholder="{{ __('Title') }}" value="{{ old('title') }}" maxlength="255" required />
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="title_en" class="col-md-2 col-form-label">@lang('Title (En)')</label>

                        <div class="col-md-10">
                            <input type="text" name="title_en" class="form-control" placeholder="{{ __('Title (En)') }}" value="{{ old('title_en') }}" maxlength="255" required />
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="description" class="col-md-2 col-form-label">@lang('Description Short')</label>

                        <div class="col-md-10">
                            <textarea type="text" name="description" class="form-control" placeholder="{{ __('Description Short') }}">{{ old('description') }}</textarea>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="description_en" class="col-md-2 col-form-label">@lang('Description Short (En)')</label>

                        <div class="col-md-10">
                            <textarea type="text" name="description_en" class="form-control" placeholder="{{ __('Description Short (En)') }}">{{ old('description_en') }}</textarea>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="slug" class="col-md-2 col-form-label">@lang('Slug')</label>

                        <div class="col-md-10">
                            <input type="text" name="slug" class="form-control" placeholder="{{ __('Slug') }}" value="{{ old('slug') }}" maxlength="255" required />
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="slug_en" class="col-md-2 col-form-label">@lang('Slug (En)')</label>

                        <div class="col-md-10">
                            <input type="text" name="slug_en" class="form-control" placeholder="{{ __('Slug (En)') }}" value="{{ old('slug_en') }}" maxlength="255" required />
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="rank" class="col-md-2 col-form-label">@lang('Rank')</label>

                        <div class="col-md-10">
                            <input type="text" name="rank" class="form-control" placeholder="{{ __('Rank') }}" value="{{ old('rank') }}" maxlength="255" />
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="rank_en" class="col-md-2 col-form-label">@lang('Rank (En)')</label>

                        <div class="col-md-10">
                            <input type="text" name="rank_en" class="form-control" placeholder="{{ __('Rank (En)') }}" value="{{ old('rank_en') }}" maxlength="255" />
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="salary_start" class="col-md-2 col-form-label">@lang('Wage')</label>

                        <div class="col-md-5">
                            <input type="number" name="salary_start" class="form-control" placeholder="{{ __('Salary Start') }}" value="{{ old('salary_start') }}" />
                        </div>

                        <div class="col-md-5">
                            <input type="number" name="salary_end" class="form-control" placeholder="{{ __('Salary End') }}" value="{{ old('salary_end') }}" />
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="form" class="col-md-2 col-form-label">@lang('Form')</label>

                        <div class="col-md-4">
                            <select name="form[]" class="form-control" multiple="multiple" value="{{ old('form') }}">
                                @foreach($model::FORMS as $item)
                                    <option value="{{ $item['key'] }}">{{ __($item['value']) }}</option>
                                @endforeach
                            </select>
                        </div>

                        <label for="career_id" class="col-md-2 col-form-label text-center">@lang('Career')</label>

                        <div class="col-md-4">
                            <select name="career_id" class="form-control" value="{{ old('career_id') }}">
                                @foreach($careers as $item)
                                    <option value="{{ $item->id }}">{{ $item->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="degree_id" class="col-md-2 col-form-label">@lang('Degree')</label>

                        <div class="col-md-4">
                            <select name="degree_id" class="form-control" value="{{ old('degree_id') }}">
                                @foreach($degrees as $item)
                                    <option value="{{ $item->id }}">{{ $item->name }}</option>
                                @endforeach
                            </select>
                        </div>

                        <label for="department_id" class="col-md-2 col-form-label text-center">@lang('Department')</label>

                        <div class="col-md-4">
                            <select name="department_id" class="form-control" value="{{ old('department_id') }}">
                                @foreach($departments as $item)
                                    <option value="{{ $item->id }}">{{ $item->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="expired" class="col-md-2 col-form-label">@lang('Expiration Date')</label>

                        <div class="col-md-10">
                            <input type="text" name="expired" class="form-control expired" placeholder="{{ __('Expiration Date') }}" value="{{ old('expired') }}" required />
                        </div>
                    </div><!--form-group-->

                    <div class="form-group row">
                        <label for="address_id" class="col-md-2 col-form-label">@lang('Address')</label>

                        <div class="col-md-10">
                            <select name="address_id" class="form-control" value="{{ old('address_id') }}">
                                @foreach($addresses as $item)
                                    <option value="{{ $item->id }}">{{ $item->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div><!--form-group-->

                    <div class="form-group row">
                        <label for="location" class="col-md-2 col-form-label">@lang('Location')</label>

                        <div class="col-md-10">
                            <textarea type="text" name="location" class="form-control" placeholder="{{ __('Location') }}">{{ old('location') }}</textarea>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="contact" class="col-md-2 col-form-label">@lang('Contact')</label>

                        <div class="col-md-10">
                            <textarea type="text" name="contact" class="form-control" placeholder="{{ __('Contact') }}">{{ old('contact') }}</textarea>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="note" class="col-md-2 col-form-label">@lang('Note')</label>

                        <div class="col-md-10">
                            <textarea type="text" name="note" class="form-control" placeholder="{{ __('Note') }}">{{ old('note') }}</textarea>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="content" class="col-md-2 col-form-label">@lang('Content')</label>

                        <div class="col-md-10">
                            <textarea type="text" name="content" id="content" class="form-control" placeholder="{{ __('Content') }}">{{ old('content') }}</textarea>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="content_en" class="col-md-2 col-form-label">@lang('Content (En)')</label>

                        <div class="col-md-10">
                            <textarea type="text" name="content_en" id="content_en" class="form-control" placeholder="{{ __('Content (En)') }}">{{ old('content_en') }}</textarea>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="sort" class="col-md-2 col-form-label">@lang('Sort')</label>

                        <div class="col-md-10">
                            <input type="number" name="sort" class="form-control" placeholder="{{ __('Sort') }}" value="{{ old('sort') }}" required />
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="active" class="col-md-2 col-form-label">@lang('Status')</label>

                        <div class="col-md-10" style="line-height: 32px;">
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" name="active" id="active1" value="{{ $model::ACTIVE }}" checked>
                                <label class="form-check-label" for="active1">@lang('Active')</label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" name="active" id="active0" value="{{ $model::INACTIVE }}">
                                <label class="form-check-label" for="active0">@lang('Inactive')</label>
                            </div>
                        </div>
                    </div>
                </div>
            </x-slot>

            <x-slot name="footer">
                <button class="btn btn-sm btn-primary float-right" type="submit">@lang('Create')</button>
            </x-slot>
        </x-backend.card>
    </x-forms.post>

    @push('after-scripts')
    <script src="{{asset('js/ckeditor/ckeditor.js')}}"></script>
    <script src="{{asset('js/ckeditor/ckeditor_replace.js')}}"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            $("select").select2({
                placeholder: "{{ __('Select a option') }}",
                allowClear: true
            });

            $('.expired').datepicker({
                format: 'dd/mm/yyyy'
            });
        });
    </script>
    @endpush('after-scripts')
@endsection
