@extends('backend.layouts.app')

@section('title', __('Setting'))

@section('content')
    <x-backend.card>
        <x-slot name="header">
            @lang('Setting')
        </x-slot>

        <x-slot name="body">
            <livewire:backend.settings-table />
        </x-slot>
    </x-backend.card>
@endsection
