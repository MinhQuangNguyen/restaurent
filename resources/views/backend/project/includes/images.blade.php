<div class="form-group row">
    <label for="Images" class="col-md-2 col-form-label">@lang('Images')</label>

    <div class="col-md-10">
        <div class="dropzone" id="{{$type}}" name="{{$type}}"></div>
    </div>
</div>

@push('after-scripts')
<script type="text/javascript">
    var uploadedDocumentMap = '{{ isset($project) }}' ? {} : {}

    Dropzone.options.{{ $type }} = {
        url: '{{ route('admin.image.storeMedia') }}',
        maxFilesize: 2, // MB
        uploadMultiple: true,
        addRemoveLinks: true,
        headers: {
            'X-CSRF-TOKEN': '{!! csrf_token() !!}'
        },
        dictDefaultMessage: '{{ __("Drop files here to upload") }}',
        success: function (file, data) {
            let name_image = "{{ $type.'[images][]' }}"
            let caption_name = "{{ $type.'[caption][]' }}"
            let caption_name_en = "{{ $type.'[caption_en][]' }}"
            let check = '{{ $project }}'
            console.log("data", data.length, file.name)
            let img = data.find(item => item.original_name === file.name)
            if(img) {
                let _imageBox = Dropzone.createElement("<input id='"+img.name+"' type='text' class='form-control images_upload' name='"+name_image+"' value="+img.name+" >");
                file.previewElement.appendChild(_imageBox);
                let _captionBox = Dropzone.createElement("<input id='"+img.name+"' type='text' class='form-control caption' placeholder='Caption' name='"+caption_name+"' >");
                file.previewElement.appendChild(_captionBox);
                if(!check) {
                    let _captionBoxEn = Dropzone.createElement("<input id='"+img.name+"' type='text' class='form-control caption' placeholder='Caption (En)' name='"+caption_name_en+"' >");
                    file.previewElement.appendChild(_captionBoxEn);
                }
                uploadedDocumentMap[img.original_name] = img.name
            }
        },
        removedfile: function (file) {
            var name = uploadedDocumentMap[file.name];
            name = name.replace(/\s+/g, '-').toLowerCase();    /*only spaces*/
            $.ajax({
                type: 'POST',
                url: '{{ route('admin.image.deleteMedia') }}',
                headers: {
                    'X-CSRF-TOKEN': '{!! csrf_token() !!}'
                },
                data: "name="+name,
                dataType: 'html',
                success: function(data) {
                    console.log("data", data)
                }
            });
            var _ref;
            if (file.previewElement) {
                if ((_ref = file.previewElement) != null) {
                    _ref.parentNode.removeChild(file.previewElement);
                    delete uploadedDocumentMap[file.name];
                }
            }
            return;
        },
        init: function () {
            let name_image = "{{ $type.'[images][]' }}"
            let caption_name = "{{ $type.'[caption][]' }}"
            let check = '{{ $project }}'
            let type = '{{ $type }}'

            @if(isset($project) && $project->getMedia('utilities'))
                let caption = {!! json_encode($project[$type]['caption']) !!}
                var files = {!! json_encode($project->getMedia($type)) !!}
                if(files.length) {
                    for (let i in files) {
                        let file = files[i]
                        let captionT = caption[i] ? caption[i] : ""
                        this.options.addedfile.call(this, file)
                        this.options.thumbnail.call(this, file, `/images/${type}/${file.file_name}`);
                        file.previewElement.classList.add('dz-success');
                        file.previewElement.classList.add('dz-complete');
                        let _imageBox = Dropzone.createElement("<input id='"+file.name+"' type='text' class='form-control images_upload' name='"+name_image+"' value="+file.file_name+" >");
                        file.previewElement.appendChild(_imageBox);
                        let _captionBox = Dropzone.createElement("<input id='"+file.name+"' type='text' class='form-control caption' placeholder='Caption' name='"+caption_name+"' value='"+captionT+"' >");
                        file.previewElement.appendChild(_captionBox);
                        if(!check) {
                            let caption_name_en = "{{ $type.'[caption_en][]' }}"
                            let _captionBoxEn = Dropzone.createElement("<input id='"+file.name+"' type='text' class='form-control caption' placeholder='Caption (En)' name='"+caption_name_en+"' value='"+captionT+"' >");
                            file.previewElement.appendChild(_captionBoxEn);
                        }
                        uploadedDocumentMap[file.name] = file.file_name
                    }
                }
            @endif
        }
    }
</script>
@endpush('after-scripts')
