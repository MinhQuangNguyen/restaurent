<div class="card">
    <h5 class="card-header">
        <a data-toggle="collapse" href="#collapse-overview" aria-expanded="true" aria-controls="collapse-overview" id="heading-overview" class="d-block">
            <i class="fa fa-chevron-down float-right"></i>
            @lang('Overview Project')
        </a>
    </h5>
    <div id="collapse-overview" class="collapse show" aria-labelledby="heading-overview">
        <div class="card-body">
            <div class="form-group row">
                <label for="title" class="col-md-2 col-form-label">@lang('Title')</label>

                <div class="col-md-10">
                    <input type="text" name="overviews[title]" class="form-control" placeholder="{{ __('Title') }}" value="{{ old('overviews.title') ?? $project->overviews['title'] ?? __('Overview Project') }}" maxlength="100" required />
                </div>
            </div>

            @if(!isset($project))
            <div class="form-group row">
                <label for="title_en" class="col-md-2 col-form-label">@lang('Title (En)')</label>

                <div class="col-md-10">
                    <input type="text" name="overviews[title_en]" class="form-control" placeholder="{{ __('Title (En)') }}" value="{{ old('overviews.title_en') ?? 'Overview Project' }}" maxlength="100" required />
                </div>
            </div>
            @endif

            <div class="form-group row">
                <label for="content" class="col-md-2 col-form-label">@lang('Content')</label>

                <div class="col-md-10">
                    <textarea type="text" name="overviews[content]" id="content" class="form-control" placeholder="{{ __('Content') }}">{{ old('overviews.content') ?? $project->overviews['content'] ?? null }}</textarea>
                </div>
            </div>

            @if(!isset($project))
            <div class="form-group row">
                <label for="content_en" class="col-md-2 col-form-label">@lang('Content (En)')</label>

                <div class="col-md-10">
                    <textarea type="text" name="overviews[content_en]" id="content_en" class="form-control" placeholder="{{ __('Content (En)') }}">{{ old('overviews.content_en') }}</textarea>
                </div>
            </div>
            @endif
        </div>
    </div>
</div>
