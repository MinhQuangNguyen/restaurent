<div class="card">
    <div class="card-header">
        @lang('Info Project')
    </div>
    <div class="card-body">
        <div class="form-group row">
            <div class="col-md-8">
                <div class="form-group row">
                    <label for="name" class="col-md-2 col-form-label">@lang('Name')</label>

                    <div class="col-md-10">
                        <input type="text" name="name" class="form-control" placeholder="{{ __('Name') }}" value="{{ old('name') ?? $project->name ?? null }}" maxlength="100" required />
                    </div>
                </div>

                @if(!isset($project))
                <div class="form-group row">
                    <label for="name_en" class="col-md-2 col-form-label">@lang('Name (En)')</label>

                    <div class="col-md-10">
                        <input type="text" name="name_en" class="form-control" placeholder="{{ __('Name (En)') }}" value="{{ old('name_en') }}" maxlength="100" required />
                    </div>
                </div>
                @endif

                <div class="form-group row">
                    <label for="address" class="col-md-2 col-form-label">Tác giả</label>

                    <div class="col-md-10">
                        <input type="text" name="address" class="form-control" value="{{ old('address') ?? $project->address ?? null }}" placeholder="Tác giả" maxlength="255">
                    </div>
                </div>

                @if(!isset($project))
                <div class="form-group row">
                    <label for="address_en" class="col-md-2 col-form-label">Tác giả(En)</label>

                    <div class="col-md-10">
                        <input type="text" name="address_en" class="form-control" value="{{ old('address_en') }}" placeholder="Tác giả (En)" maxlength="255"/>
                    </div>
                </div>
                @endif

                <div class="form-group row">
                    <label for="slug" class="col-md-2 col-form-label">@lang('Slug')</label>

                    <div class="col-md-10">
                        <input type="text" name="slug" class="form-control" placeholder="{{ __('Slug') }}" value="{{ old('slug') ?? $project->slug ?? null }}" maxlength="255" required />
                    </div>
                </div>

                @if(!isset($project))
                <div class="form-group row">
                    <label for="slug_en" class="col-md-2 col-form-label">@lang('Slug (En)')</label>

                    <div class="col-md-10">
                        <input type="text" name="slug_en" class="form-control" placeholder="{{ __('Slug (En)') }}" value="{{ old('slug_en') }}" maxlength="255" required />
                    </div>
                </div>
                @endif

                <div class="form-group row">
                    <label for="category_id" class="col-md-2 col-form-label">@lang('Field')</label>

                    <div class="col-md-10">
                        <select name="category_id" class="form-control" x-on:change="categoryId = $event.target.value" required>
                            @foreach($categories as $item)
                                <option value="{{ $item->id }}" {{ isset($project) && $project->category_id === $item->id ? 'selected' : '' }}>{{ $item->name }}</option>
                            @endforeach
                        </select>
                    </div>
                </div><!--form-group-->

                @if(!isset($project))
                <div class="form-group row">
                    <label for="is_highlight" class="col-md-2 col-form-label">@lang('Highlight')</label>

                    <div class="col-md-10">
                        <div class="form-check">
                            <input
                                type="checkbox"
                                name="is_highlight"
                                class="form-check-input checkbox"
                                value="1"
                            />
                        </div>
                    </div>
                </div>

                <div class="form-group row">
                    <label for="banner" class="col-md-2 col-form-label">@lang('Banner')</label>

                    <div class="col-md-10">
                        <div class="custom-file">
                            <input type="file" name="banner" class="custom-file-input" id="customFile" required />
                            <label class="custom-file-label" for="customFile">{{ __('Choose file') }}</label>
                        </div>
                    </div>
                </div>
                @endif

                @if(isset($project))
                <div class="form-group row">
                    <label for="description" class="col-md-2 col-form-label">@lang('Description Short')</label>

                    <div class="col-md-10">
                        <textarea type="text" name="description" class="form-control" placeholder="{{ __('Description Short') }}" maxlength="255">{{ old('description') ?? $project->description ?? null }}</textarea>
                    </div>
                </div>
                @endif

                @if(isset($project))
                <div class="form-group row">
                    <label for="is_highlight" class="col-md-2 col-form-label">@lang('Highlight')</label>

                    <div class="col-md-10">
                        <div class="form-check">
                            <input
                                type="checkbox"
                                name="is_highlight"
                                value="1"
                                class="form-check-input checkbox"
                                {{ $project->is_highlight ? 'checked' : '' }}
                            />
                        </div>
                    </div>
                </div>

                <div class="form-group row">
                    <label for="banner" class="col-md-2 col-form-label">@lang('Banner')</label>

                    <div class="col-md-10">
                        <div class="custom-file">
                            <input type="file" name="banner" class="custom-file-input" id="customFile" />
                            <label class="custom-file-label" for="customFile">{{ $project->banner ? $project->banner : __('Choose file') }}</label>
                        </div>
                    </div>
                </div>
                @endif
            </div>
            <div class="col-md-4">
                <div class="form-group row">
                    <div class="col-md-12">
                        <div class="images-preview-div">
                            <input accept="image/*" type="file" id="images" name="image" style="display: none" />
                            <label for="images" class="w-100 text-center">
                                <img src="{{ isset($project) && $project->image ? $project->image : 'http://www.placehold.it/300x300/EFEFEF/AAAAAA&amp;text=No+Image'}}" alt="No Image" id="image-pre" class="img-thumbnail">
                            </label>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                @if(!isset($project))
                <div class="form-group row">
                    <label for="description" class="col-md-2 col-form-label">@lang('Description Short')</label>

                    <div class="col-md-10">
                        <textarea type="text" name="description" class="form-control" placeholder="{{ __('Description Short') }}" maxlength="255">{{ old('description') ?? $project->description ?? null }}</textarea>
                    </div>
                </div>
                @endif

                @if(!isset($project))
                <div class="form-group row">
                    <label for="description_en" class="col-md-2 col-form-label">@lang('Description Short (En)')</label>

                    <div class="col-md-10">
                        <textarea type="text" name="description_en" class="form-control" placeholder="{{ __('Description Short (En)') }}" maxlength="255">{{ old('description_en') }}</textarea>
                    </div>
                </div>
                @endif
            </div>
        </div>
    </div>
</div>

@push('after-scripts')
    <script type="text/javascript">
        $(document).ready(function() {
            $('input[name="banner"]').change(function(e){
                var fileName = e.target.files[0].name;
                $('.custom-file-label').html(fileName);
            });
        });
    </script>
    @endpush('after-scripts')
