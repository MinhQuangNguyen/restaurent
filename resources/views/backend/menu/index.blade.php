@extends('backend.layouts.app')

@section('title', __('Menu Management'))

@section('content')
    <x-backend.card>
        <x-slot name="header">
            @lang('Menu Management')
        </x-slot>

        @if ($logged_in_user->hasAllAccess())
            <x-slot name="headerActions">
                <x-utils.link
                    icon="c-icon cil-plus"
                    class="card-header-action"
                    :href="route('admin.menu.create')"
                    :text="__('Create Menu')"
                />
            </x-slot>
        @endif

        <x-slot name="body">
            <livewire:backend.menus-table />
        </x-slot>
    </x-backend.card>
@endsection
