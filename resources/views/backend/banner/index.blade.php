@extends('backend.layouts.app')

@section('title', __('Banner Management'))

@section('content')
    <x-backend.card>
        <x-slot name="header">
            @lang('Banner Management')
        </x-slot>

        <x-slot name="body">
            <livewire:backend.banners-table />
        </x-slot>
    </x-backend.card>
@endsection
